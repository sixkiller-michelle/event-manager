﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="EventManager.CeuAveWeb._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<!--
    ============================================
    HOME PAGE "CONTENT AND IMAGE" SLIDER
    ============================================= -->
    <div id="MainSlider">
        <div class="container">
            <div class="row">
                <!-- ----------- Begin:ContentSlides ----------- -->
                <div class="col-md-4 col-sm-12 RemovePaddingRight">
                    <div class="MainSliderContent">
                        <div class="MainSliderEntryWrapper">
                            
                            <a data-slide-index="0" href="">
                                <div class="MainSliderEntry">
                                    <h2></h2>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum scelerisque.
                                    </p>
                                    <p class="AddPaddingTop" style="font-size:11px;">
                                        <i class="fa-clock"></i> 14th April 2013
                                    </p>
                                    <p style="font-size:11px;">
                                        <i class="fa-user"></i> Author Name
                                    </p>
                                </div>
                            </a>
                            <a data-slide-index="1" href="">
                                <div class="MainSliderEntry">
                                    <h2>Print Certificates Quick and Easy</h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <p class="AddMarginLeft" style="font-size:11px;">
                                    </p>
                                </div>
                            </a>
                            <a data-slide-index="2" href="">
                                <div class="MainSliderEntry">
                                    <h2>SENECTUS ET NEYUS ET MALESUADA</h2>
                                    <p>
                                        Vestibulum scelerisque dignissim massa, sed gravida nisi sollicitudin vel. Sed nec erat feugiat orci hendrerit.
                                    </p>
                                    <p class="AddPaddingTop" style="font-size:11px;">
                                        <i class="fa-clock"></i> 14th April 2013
                                    </p>
                                    <p style="font-size:11px;">
                                        <i class="fa-user"></i> Author Name
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- ----------- Finish:ContentSlides ----------- -->

                <!-- ----------- Begin:ImageSlides ----------- -->
                <div class="col-md-8 col-sm-12 RemovePaddingLeft">
                    <div class="MainSliderImages">
                        <a href="#">
                            <img src="./images/slide-show/slide1a.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                        <a href="#">
                            <img src="./images/slide-show/slide2a.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                        <a href="#">
                            <img src="./images/slide-show/slide3.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                    </div>
                </div>
                <!-- ----------- Finish:ImageSlides ----------- -->
            </div>
        </div>
    </div>

    <!--
    ============================================
    ANNOUCEMENT
    ============================================= -->
    <div class="container">
        <div class="AnnounceTable WhiteSkin clearfix">
            <!-- ----------- Begin:AnnounceSlider ----------- -->
            <div class="col-md-9">
                <div class="AnnounceSlider">
                    <li>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi tellus.
                        </p>
                    </li>
                    <li>
                        <p>
                            Pellentesque habitant morbi tristique senectus et netus et malesuada.
                        </p>
                    </li>
                    <li>
                        <p>
                            Vestibulum scelerisque dignissim massa, sed gravida nisi sollicitudin vel.
                        </p>
                    </li>
                </div>
            </div>
            <!-- ----------- Finish:AnnounceSlider ----------- -->

            <!-- ----------- Begin:AnnounceDropdown ----------- -->
            <div class="col-md-3 hidden-sm hidden-xs">
                <ul class="AnnounceDropdown pull-right">
                    <li>WEBSITE DESIGN</li>
                    <li class="dropdown" style="border-right: 1px solid #544b4a;"><a href="#" class="ddown dropdown-toggle" data-toggle="dropdown"><i class="fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">GRAPHIC DESIGN</a>
                            </li>
                            <li><a href="#">PRINT</a>
                            </li>
                            <li><a href="#">MULTIMEDIA</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- ----------- finish:AnnounceDropdown ----------- -->
        </div>
    </div>

    <!--
    ============================================
    SERVICES & NEW PROJECT
    ============================================= -->
    <div class="container" style="margin-bottom: 10px;">
        <div class="row">
            <!-- ----------- Begin:ServicesMosaic ----------- -->
            <div class="col-sm-12">
                <div class="row">
                    <!-- ----------- Begin:1stMosaic ----------- -->
                    <div class="col-sm-4">
                        <asp:HyperLink ID="PrintCertHyperlink" runat="server" NavigateUrl="~/PrintCertificate.aspx">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-doc-text VeryLargeSize"></i>
                                <h3>PRINT CERTIFICATE</h3>
                                <p>Attended an event recently?  Click here 
                                to print your certificate.  </p>
                            </div>
                        </asp:HyperLink>
                    </div>
                    <!-- ----------- Finish:1stMosaic ----------- -->

                    <!-- ----------- Begin:2ndMosaic ----------- -->
                    <div class="col-sm-4">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/about/upcomingevents.aspx">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-calendar VeryLargeSize"></i>
                                <h3>FIND EVENTS</h3>
                                <p>View upcoming events in your area or log in
                                to view your association's events
                                </p>
                            </div>
                        </asp:HyperLink>
                    </div>
                    <!-- ----------- Finish:2ndMosaic ----------- -->
                    
                    <!-- ----------- Begin:3rdMosaic ----------- -->
                    <div class="col-sm-4">
                        <a href="#">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-help VeryLargeSize"></i>
                                <h3>SUPPORT</h3>
                                <p>Contact your association's administrators or the 
                                support staff at ceuavenue.com</p>
                            </div>
                        </a>
                    </div>
                    <!-- ----------- Finish:3rdMosaic ----------- -->
                </div>
            </div>
            <!-- ----------- Finish:ServicesMosaic ----------- -->
        </div>
    </div>
</asp:Content>