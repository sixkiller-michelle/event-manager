INFINITY  |  Flat PSD Template

Infinity is a multipurpose PSD theme suitable for any type of business. The download includes 17 well organized PSD files, 
Flat and clean design with fully editable organized layered. Check out the screenshots to see them all!




Features:

Flexible and Multipurpose
Flat and Clean Design
17 PSD Files – 960+ Grid System
2 Home Pages, 2 Slider Designs
Portfolio Page has 2, 3 or 4 Column Layout
4 Colors
Nexa Fonts used and They are in the “Documentation” File
All Icons are Free for Download From http://fontello.com




Disclaimer:

All images are just used for preview purpose only and  NOT  included in the final purchase files.