﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web;
using System.Web.Security;

namespace EventManager.Qb
{
    public sealed class EventLogger
    {
        // All methods are static, so this can be private
        private EventLogger()
        { }

        // Log an Exception
        public static void LogEvent(string logText)
        {
            // Include enterprise logic for logging exceptions
            // Get the absolute path to the log file
            string logFile = "~/App_Data/QbEventLog.txt";
            logFile = HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            StreamWriter sw = new StreamWriter(logFile, true);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            if (!string.IsNullOrEmpty(logText))
            {
                sw.Write(logText);
                sw.WriteLine();
            }
            sw.Close();
        }

    }
}