﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.Compilation;
using System.Collections;
using System.Xml;
using System.ComponentModel;

namespace EventManager.Qb
{
    /// <summary>
    /// Summary description for EventManagerQbService
    /// </summary>
    [WebService(Namespace = "http://developer.intuit.com/")]
    [WebServiceBinding("EventManagerQbService", "http://developer.intuit.com")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EventManagerQbService : QBWebConnectorSvc
    {
        #region GlobalVariables
        private IContainer components = null;

		System.Diagnostics.EventLog evLog = new System.Diagnostics.EventLog();
        public int assnId = 0;
		public int count=0;
		public ArrayList req=new ArrayList();
		#endregion

        public EventManagerQbService()
        { 
        
        }

        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}


        [WebMethod(EnableSession = true)]
        public override string[] authenticate(string strUserName, string strPassword)
        {
            EventManagerMethods eventManagerMethods = new EventManagerMethods();

            string evLogTxt = "WebMethod: authenticate() has been called by QBWebconnector";
            evLogTxt = evLogTxt + "Parameters received:";
            evLogTxt = evLogTxt + "string strUserName = " + strUserName;
            evLogTxt = evLogTxt + "string strPassword = " + strPassword;

            string[] authReturn = new string[2];
            // Code below uses a random GUID to use as session ticket
            // An example of a GUID is {85B41BEE-5CD9-427a-A61B-83964F1EB426}
            if (System.Web.HttpContext.Current.Session != null)
                authReturn[0] = System.Web.HttpContext.Current.Session.SessionID;
            else
                authReturn[0] = System.Guid.NewGuid().ToString();

            assnId = eventManagerMethods.GetAssociationIdForUser(strUserName, strPassword);
            evLogTxt = evLogTxt + "Retrieved AssnId: " + assnId.ToString();

            if (assnId > 0)
            {   // An empty string for authReturn[1] means asking QBWebConnector 
                // to connect to the company file that is currently openned in QB

                // You could also return "none" to indicate there is no work to do
                // or a company filename in the format C:\full\path o\company.qbw based on your program logic and requirements.
                if (eventManagerMethods.HasWork(assnId))
                {
                    evLogTxt = evLogTxt + "YES! Work to be done. Connect to open company file";
                    authReturn[1] = "";
                }

                else
                {
                    evLogTxt = evLogTxt + "NO WORK TO BE DONE";
                    authReturn[1] = "none";
                }

            }
            else // Not authenticated
            {
                authReturn[1] = "nvu";
            }

            evLogTxt = evLogTxt + "Return values: ";
            evLogTxt = evLogTxt + "string[] authReturn[0] = " + authReturn[0].ToString();
            evLogTxt = evLogTxt + "string[] authReturn[1] = " + authReturn[1].ToString();
            EventLogger.LogEvent(evLogTxt);
            return authReturn;
        }

        [WebMethod]
        public override string sendRequestXML(string ticket, string strHCPResponse, string strCompanyFileName, string qbXMLCountry, int qbXMLMajorVers, int qbXMLMinorVers)
        {
            throw new NotImplementedException();
        }

        [WebMethod]
        public override int receiveResponseXML(string ticket, string response, string hresult, string message)
        {
            string evLogTxt="WebMethod: receiveResponseXML() called by QBWebconnector";
            evLogTxt=evLogTxt+"Parameters received:";
            evLogTxt=evLogTxt+"string ticket = " + ticket;
            evLogTxt=evLogTxt+"string response = " + response;
            evLogTxt=evLogTxt+"string hresult = " + hresult;
            evLogTxt=evLogTxt+"string message = " + message;
            
            int retVal=0;
            if(!hresult.ToString().Equals("")){
                // if error in the response, web service should return a negative int
                evLogTxt=evLogTxt+ "HRESULT = " + hresult;
                evLogTxt=evLogTxt+ "Message = " + message;
                retVal=-101;
            }
            else{
                evLogTxt = evLogTxt + "Length of response received = " + response.Length;
                ArrayList req=buildRequest();
                int total=req.Count;
                int count=Convert.ToInt32(Session["counter"]);
                int percentage=(count*100)/total;
                if (percentage>=100){
                    count=0;
                    Session["counter"]=0;
                }
                retVal=percentage;
            }

            evLogTxt=evLogTxt+"Return values: ";

            evLogTxt = evLogTxt + "int retVal= " + retVal.ToString();
            EventLogger.LogEvent(evLogTxt);
            return retVal;
        }

        [WebMethod]
        public override string connectionError(string ticket, string hresult, string message)
        {
            string evLogTxt="WebMethod: connectionError() has been called by QBWebconnector";
            evLogTxt=evLogTxt+"Parameters received:";
            evLogTxt=evLogTxt+"string ticket = " + ticket;
            evLogTxt=evLogTxt+"string hresult = " + hresult;
            evLogTxt=evLogTxt+"string message = " + message;
           
            string retVal=null;
            // 0x80040400 - QuickBooks found an error when parsing the provided XML text stream.
            const string QB_ERROR_WHEN_PARSING="0x80040400";
            // 0x80040401 - Could not access QuickBooks.
            const string QB_COULDNT_ACCESS_QB="0x80040401";
            // 0x80040402 - Unexpected error. Check the qbsdklog.txt file
            const string QB_UNEXPECTED_ERROR="0x80040402";
            // Add more as you need...
            if(hresult.Trim().Equals(QB_ERROR_WHEN_PARSING)){
                evLogTxt=evLogTxt+ "HRESULT = " + hresult;
                evLogTxt=evLogTxt+ "Message = " + message;
                retVal = "DONE";
            }
            else if(hresult.Trim().Equals(QB_COULDNT_ACCESS_QB)){
                evLogTxt=evLogTxt+ "HRESULT = " + hresult;
                evLogTxt=evLogTxt+ "Message = " + message;
                retVal = "DONE";
            }
            else if(hresult.Trim().Equals(QB_UNEXPECTED_ERROR)){
                evLogTxt=evLogTxt+ "HRESULT = " + hresult;
                evLogTxt=evLogTxt+ "Message = " + message;
                retVal = "DONE";
            }
            else {
                // Depending on various hresults return different value
                // Try again with this company file
                evLogTxt=evLogTxt+ "HRESULT = " + hresult;
                evLogTxt = evLogTxt + "Message = " + message;
                retVal = "";
            }

            evLogTxt=evLogTxt+"Return values: ";
            evLogTxt=evLogTxt+"string retVal = " + retVal;
            EventLogger.LogEvent(evLogTxt);
            return retVal;

        }

        [WebMethod]
        public override string getLastError(string ticket)
        {
            string evLogTxt="WebMethod: getLastError() has been called by QBWebconnector";
            evLogTxt=evLogTxt+"Parameters received:";
            evLogTxt=evLogTxt+"string ticket = " + ticket;
          
            int errorCode=0;
            string retVal=null;
            if(errorCode==-101){
                retVal="QuickBooks was not running!"; // just an example of custom user errors
            }
            else{
                retVal="Error!";
            }
            
            evLogTxt=evLogTxt+"Return values: ";
            evLogTxt = evLogTxt + "string retVal= " + retVal;
            EventLogger.LogEvent(evLogTxt);
            return retVal;
        } 
        
        [WebMethod]
        public override string closeConnection(string ticket)
        {
            string evLogTxt="WebMethod: closeConnection() has been called by QBWebconnector";
            evLogTxt=evLogTxt+"Parameters received:";
            evLogTxt=evLogTxt+"string ticket = " + ticket;
         
            string retVal=null;
            retVal="OK";
           
            evLogTxt=evLogTxt+"Return values: ";
            evLogTxt = evLogTxt + "string retVal= " + retVal;
            EventLogger.LogEvent(evLogTxt);
            return retVal;
        }

        public string clientVersion(string strVersion)
        {
            string evLogTxt="WebMethod: clientVersion() has been called by QBWebconnector";
            evLogTxt=evLogTxt + "Parameters received:";
            evLogTxt=evLogTxt + "string strVersion = " + strVersion;
          
            string retVal=null;
            double recommendedVersion = 1.5;
            double supportedMinVersion = 1.0;
            double suppliedVersion=Convert.ToDouble(strVersion);
            evLogTxt=evLogTxt+"QBWebConnector version = " + strVersion;
            evLogTxt=evLogTxt+"Recommended Version = " + recommendedVersion.ToString();
            evLogTxt=evLogTxt+"Supported Min Version = " + supportedMinVersion.ToString();
            evLogTxt = evLogTxt + "SuppliedVersion = " + suppliedVersion.ToString();
            if(suppliedVersion<recommendedVersion) {
                retVal="W:We recommend that you upgrade your QBWebConnector";
            }
            else if(suppliedVersion<supportedMinVersion){
                retVal="E:You need to upgrade your QBWebConnector";
            }
         
            evLogTxt=evLogTxt+"Return values: ";
            evLogTxt=evLogTxt+"string retVal = " + retVal;
            EventLogger.LogEvent(evLogTxt);
            return retVal;

        }

        public string getInteractiveURL(string wcTicket, string sessionID)
        {
            return "http://sixkiller.dyndns.org/oneticket";
        }

        public string getServerVersion(string ticket)
        {
            Version v = GetHttpApplicationVersion();
            if (v != null)
            {
                return v.Major.ToString() + ":" + v.Minor.ToString() + ":" + v.Build.ToString() + ":" + v.Revision.ToString();
            }
            else
            {
                return "";
            }
        }

        public string interactiveDone(string wcTicket)
        {
            return "Done";
        }

        public string interactiveRejected(string wcTicket, string reason)
        {
            return "Reason for rejection";
        }
       
        private Version GetHttpApplicationVersion()
        {
            Type lBase = typeof(HttpApplication);
            Type lType = BuildManager.GetGlobalAsaxType();

            if (lBase.IsAssignableFrom(lType))
            {
                while (lType.BaseType != lBase) { lType = lType.BaseType; }
                return lType.Assembly.GetName().Version;
            }
            else
            {
                return null;
            }
        }

        #region UtilityMethods
        private void initEvLog()
        {
            try
            {
                string source = "WCWebService";
                if (!System.Diagnostics.EventLog.SourceExists(source))
                    System.Diagnostics.EventLog.CreateEventSource(source, "Application");
                evLog.Source = source;
            }
            catch { };
            return;
        }

        private void logEvent(string logText)
        {
            try
            {
                evLog.WriteEntry(logText);
            }
            catch { };
            return;
        }

        public ArrayList buildRequest()
        {
            string strRequestXML = "";
            XmlDocument inputXMLDoc = null;

            // CustomerQuery
            inputXMLDoc = new XmlDocument();
            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", null, null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbxml", "version=\"4.0\""));

            XmlElement qbXML = inputXMLDoc.CreateElement("QBXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement customerQueryRq = inputXMLDoc.CreateElement("CustomerQueryRq");
            qbXMLMsgsRq.AppendChild(customerQueryRq);
            customerQueryRq.SetAttribute("requestID", "1");
            XmlElement maxReturned = inputXMLDoc.CreateElement("MaxReturned");
            customerQueryRq.AppendChild(maxReturned).InnerText = "1";

            strRequestXML = inputXMLDoc.OuterXml;
            req.Add(strRequestXML);

            // Clean up
            strRequestXML = "";
            inputXMLDoc = null;
            qbXML = null;
            qbXMLMsgsRq = null;
            maxReturned = null;

            // InvoiceQuery
            inputXMLDoc = new XmlDocument();
            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", null, null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbxml", "version=\"4.0\""));

            qbXML = inputXMLDoc.CreateElement("QBXML");
            inputXMLDoc.AppendChild(qbXML);
            qbXMLMsgsRq = inputXMLDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement invoiceQueryRq = inputXMLDoc.CreateElement("InvoiceQueryRq");
            qbXMLMsgsRq.AppendChild(invoiceQueryRq);
            invoiceQueryRq.SetAttribute("requestID", "2");
            maxReturned = inputXMLDoc.CreateElement("MaxReturned");
            invoiceQueryRq.AppendChild(maxReturned).InnerText = "1";

            strRequestXML = inputXMLDoc.OuterXml;
            req.Add(strRequestXML);

            // Clean up
            strRequestXML = "";
            inputXMLDoc = null;
            qbXML = null;
            qbXMLMsgsRq = null;
            maxReturned = null;

            // BillQuery
            inputXMLDoc = new XmlDocument();
            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", null, null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbxml", "version=\"4.0\""));

            qbXML = inputXMLDoc.CreateElement("QBXML");
            inputXMLDoc.AppendChild(qbXML);
            qbXMLMsgsRq = inputXMLDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement billQueryRq = inputXMLDoc.CreateElement("BillQueryRq");
            qbXMLMsgsRq.AppendChild(billQueryRq);
            billQueryRq.SetAttribute("requestID", "3");
            maxReturned = inputXMLDoc.CreateElement("MaxReturned");
            billQueryRq.AppendChild(maxReturned).InnerText = "1";

            strRequestXML = inputXMLDoc.OuterXml;
            req.Add(strRequestXML);

            return req;
        } 
        #endregion

        
    }
}
