﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="features.aspx.cs" Inherits="EventManager.CeuAveWeb._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!------------------------------------
        Features Content
    <------------------------------------>
    <div class="container">
        <div class="row">
            <div class="Testimonials clearfix">
                <!-- ----------- Begin:TestimonialsContent ----------- -->
                <div class="col-sm-8" >
                    <div  id="featuresContainer" class="TestimonialsContent WhiteSkin tab-content">
                        <div class="TestimonialsEntry tab-pane" id="GRAPHICRIVER">
                            <img src="./img/envato-logos/griver.png" title="Image title" alt="alternative information">
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                John Doe, Director at Company.

                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane active" id="ENVATO">
                            <img src="./img/envato-logos/envato.png" title="Image title" alt="alternative information">
                            <blockquote>
                                consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                Lorem ipsum, Founder and Director at Company.
                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane" id="THEMEFOREST">
                            <img src="./img/envato-logos/themeforest.png" title="Image title" alt="alternative information">
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                John Doe, Director at Company.
                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane" id="ACTIVEDEN">
                            <img src="./img/envato-logos/activeden.png" title="Image title" alt="alternative information">
                            <blockquote>
                                consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                Lorem ipsum, Founder and Director at Company.
                            </p>
                        </div>
                    </div>
                </div>
                <!-- ----------- Finish:TestimonialsContent ----------- -->

                <!-- ----------- Begin:TestimonialsBlocks ----------- -->
                <div class="col-sm-4">
                    <div class="TestimonialsBlocksWrapper">
                        <ul id="TestimonialsBlocks">
                            <a href="#GRAPHICRIVER" data-toggle="tab">
                                <li class="WhiteSkin">Online Registration</li>
                            </a>
                            <a href="#ENVATO" data-toggle="tab">
                                <li class="WhiteSkin active">Custom Attendee Badges</li>
                            </a>
                            <a href="#THEMEFOREST" data-toggle="tab">
                                <li class="WhiteSkin">Flexible Pricing</li>
                            </a>
                            <a href="#ACTIVEDEN" data-toggle="tab">
                                <li class="WhiteSkin">Scanned Attendence Tracking</li>
                            </a>
                                <a href="#ACTIVEDEN" data-toggle="tab">
                                <li class="WhiteSkin">Printing Certificates</li>
                            </a>
                                <a href="#ACTIVEDEN" data-toggle="tab">
                                <li class="WhiteSkin">Detailded Reports</li>
                            </a>
                                <a href="#ACTIVEDEN" data-toggle="tab">
                                <li class="WhiteSkin">Quick Books Integrated</li>
                            </a>
                        </ul>
                    </div>
                </div>
                <!-- ----------- Finish:TestimonialsBlocks ----------- -->
            </div>
        </div>
    </div>

</asp:Content>