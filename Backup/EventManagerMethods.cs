﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Qb
{
    public class EventManagerMethods
    {
        public int GetAssociationIdForUser(string username, string password)
        {
            int assnId = 0;
            if (Membership.ValidateUser(username, password))
            {
                MembershipUser user = Membership.GetUser(username);
                if (user != null)
                {
                    AssociationEmployeeMethods orgUserMethods = new AssociationEmployeeMethods();
                    AssociationEmployee emp = orgUserMethods.GetEmployeeByUserId(new Guid(user.ProviderUserKey.ToString()));
                    if (emp != null)
                    {
                        assnId = emp.AssociationId;
                    }
                }
            }

            return assnId;
        }

        public bool HasWork(int orgId)
        {
            //if (GetUnexportedBillingBatches(orgId).Count > 0)
            //    return true;
            //else
            //    return false;
            return false;
        }
    }
}