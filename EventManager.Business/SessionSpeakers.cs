﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class SessionSpeakerMethods : MethodBase<SessionSpeaker>
    {
        public SessionSpeaker GetSessionSpeaker(int id)
        {
            return this.ObjectContext.SessionSpeakers.SingleOrDefault(d => d.Id == id);
        }

        public SessionSpeaker GetSessionSpeaker(int sessionId, int speakerId)
        {
            return this.ObjectContext.SessionSpeakers.SingleOrDefault(d => d.SessionId == sessionId && d.SpeakerId == speakerId);
        }

        public List<SessionSpeaker> GetSpeakersByEvent(int eventId)
        {
            IQueryable<SessionSpeaker> query =

                  from evt in this.ObjectContext.SessionSpeakers
                  where evt.EventSession.EventId == eventId
                  orderby evt.Attendee.LastName, evt.Attendee.FirstName
                  select evt;

            return query.ToList();
        }


        public List<SessionSpeaker> GetSpeakersBySession(int sessionId)
        {
            IQueryable<SessionSpeaker> query =

                  from evt in this.ObjectContext.SessionSpeakers
                  where evt.SessionId == sessionId
                  select evt;

            return query.ToList();
        }

        public List<SessionSpeaker> GetSessionsBySpeaker(int speakerId)
        {
            IQueryable<SessionSpeaker> query =

                  from evt in this.ObjectContext.SessionSpeakers
                  where evt.SpeakerId == speakerId
                  select evt;

            return query.ToList();
        }

        public void Update(SessionSpeaker speaker)
        {
            // Get original object
            SessionSpeaker c = GetSessionSpeaker(speaker.Id);

            // Update the values

        }

    }
}
