﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class EventFlagMethods : MethodBase<EventFlag>
    {
        public EventFlag GetEventFlag(int id)
        {
            return this.ObjectContext.EventFlags.SingleOrDefault(d => d.Id == id);
        }

        public List<EventFlag> GetFlagsByEvent(int eventId)
        {
            IQueryable<EventFlag> query =

                  from evt in this.ObjectContext.EventFlags
                  where evt.EventId == eventId
                  orderby evt.FlagName
                  select evt;

            return query.ToList();
        }

        public List<EventFlag> GetPricedFlagsByEvent(int eventId)
        {
            IQueryable<EventFlag> query =

                  from evt in this.ObjectContext.EventFlags
                  where evt.EventId == eventId && (evt.Period1Price != 0 || evt.Period2Price != 0 || evt.Period3Price != 0)
                  select evt;

            return query.ToList();
        }

        public int GetAttendeeCount(int flagId)
        {
            IQueryable<RegistrationAttendeeFlag> query =

                  from evt in this.ObjectContext.RegistrationAttendeeFlags
                  where evt.EventFlagId == flagId
                  select evt;

            return query.Count();
        }


        public void Update(EventFlag flag)
        {
            // Get original object
            EventFlag c = GetEventFlag(flag.Id);

            // Update the values
            c.FlagCode = flag.FlagCode;
            c.FlagName = flag.FlagName;
            c.PrintOnBadge = flag.PrintOnBadge;
            c.Price = flag.Price;
            c.Period1Price = flag.Period1Price;
            c.Period2Price = flag.Period2Price;
            c.Period3Price = flag.Period3Price;
        }

        public override void Delete(EventFlag flag)
        {
            EventFlag f = GetEventFlag(flag.Id);
            base.Delete(f);
        }
    }
}
