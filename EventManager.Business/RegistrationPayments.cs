﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationPaymentMethods : MethodBase<RegistrationPayment>
    {

        public RegistrationPayment GetRegistrationPayment(int id)
        {
            return this.ObjectContext.RegistrationPayments.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<RegistrationPayment> GetPaymentsByRegistration(int registrationId)
        {
            IQueryable<RegistrationPayment> query =

                  from evt in this.ObjectContext.RegistrationPayments
                  where evt.RegistrationId == registrationId
                  orderby evt.PaymentDate descending
                  select evt;

            return query.ToList();
        }

        public override void Add(RegistrationPayment newPayment)
        {
            base.Add(newPayment);

            // If we've got a 0 balance and payment amt are non-zero, update status to "Paid"
            RegistrationMethods m = new RegistrationMethods();
            decimal totalPayments = m.GetPaymentTotal(newPayment.RegistrationId);
            decimal balance = m.GetBalance(newPayment.RegistrationId);
            if (totalPayments > 0 && balance == 0)
            {
                Registration reg = m.GetRegistration(newPayment.RegistrationId);
                reg.Status = "Paid";
                m.Update(reg);
            }
        }

        public void Update(RegistrationPayment rch)
        {
            RegistrationPayment a = GetRegistrationPayment(rch.Id);
            a.PaymentAmt = rch.PaymentAmt;
            a.PaymentDate = rch.PaymentDate;
            a.PaymentType = rch.PaymentType;
            a.Notes = rch.Notes;

            // If we've got a 0 balance and payment amt are non-zero, update status to "Paid"
            RegistrationMethods m = new RegistrationMethods();
            decimal totalPayments = m.GetPaymentTotal(a.RegistrationId);
            decimal balance = m.GetBalance(a.RegistrationId);
            if (totalPayments > 0 && balance == 0)
            {
                Registration reg = m.GetRegistration(a.RegistrationId);
                reg.Status = "Paid";
                m.Update(reg);
            }

        }
    }
}
