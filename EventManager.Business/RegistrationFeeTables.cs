﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationFeeTableMethods : MethodBase<RegistrationFeeTable>
    {
        public decimal GetRegistrationPrice(int eventId, bool isMember, int? orgFeeCategoryId, int attendeeCount, int speakerCount, int period)
        {
            decimal chargeTotal = 0;
            int nonSpeakerCount = attendeeCount - speakerCount;
            int i = 0;
            decimal basePrice = 0;
            decimal attendeePrice = 0;


            IQueryable<RegistrationFeeTable> query =

                  from fee in this.ObjectContext.RegistrationFeeTables
                  where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == orgFeeCategoryId
                  orderby fee.GroupMinCount
                  select fee;

            // If no rows returned, widen search to include NULL fee category (for Workshops and Training events)
            if (query.Count() == 0)
            {
                query = from fee in this.ObjectContext.RegistrationFeeTables
                  where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == null
                  orderby fee.GroupMinCount
                  select fee;
            }

            foreach (RegistrationFeeTable feeRow in query)
            {
                //if (i == 0)
                //{
                //    basePrice = (period == 1 ? feeRow.Period1GroupPrice : (period == 2 ? feeRow.Period2GroupPrice : feeRow.Period3GroupPrice));
                //    chargeTotal = basePrice;
                //}

                basePrice = (period == 1 ? feeRow.Period1GroupPrice : (period == 2 ? feeRow.Period2GroupPrice : feeRow.Period3GroupPrice));
                attendeePrice = (period == 1 ? feeRow.Period1AttendeePrice : (period == 2 ? feeRow.Period2AttendeePrice : feeRow.Period3AttendeePrice));

                //chargeTotal = chargeTotal + basePrice;
                if (nonSpeakerCount >= feeRow.GroupMaxCount)
                {
                    chargeTotal = chargeTotal + basePrice;
                    chargeTotal = chargeTotal + ((feeRow.GroupMaxCount - feeRow.GroupMinCount + 1) * attendeePrice);
                }
                else if (nonSpeakerCount >= feeRow.GroupMinCount && nonSpeakerCount < feeRow.GroupMaxCount)
                {
                    chargeTotal = chargeTotal + basePrice;
                    chargeTotal = chargeTotal + ((nonSpeakerCount - feeRow.GroupMinCount + 1) * attendeePrice);
                }
                i = i + 1;
            }

            return chargeTotal;

        }

        public string GetPricingCalculation(int eventId, bool isMember, int? orgFeeCategoryId, int attendeeCount, int period)
        {
            string calcString = "";
            int i = 0;
            decimal basePrice = 0;
            decimal attendeePrice = 0;

            IQueryable<RegistrationFeeTable> query =

                  from fee in this.ObjectContext.RegistrationFeeTables
                  where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == orgFeeCategoryId
                  orderby fee.GroupMinCount
                  select fee;

            // If no rows returned, widen search to include NULL fee category (for Workshops and Training events)
            if (query.Count() == 0)
            {
                query = from fee in this.ObjectContext.RegistrationFeeTables
                        where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == null
                        orderby fee.GroupMinCount
                        select fee;
            }

            foreach (RegistrationFeeTable feeRow in query)
            {
                if (i == 0)
                {
                    basePrice = (period == 1 ? feeRow.Period1GroupPrice : (period == 2 ? feeRow.Period2GroupPrice : feeRow.Period3GroupPrice));
                    if (basePrice > 0)
                    { 
                         calcString = "Base: " + basePrice.ToString();
                    }
                }

                attendeePrice = (period == 1 ? feeRow.Period1AttendeePrice : (period == 2 ? feeRow.Period2AttendeePrice : feeRow.Period3AttendeePrice));
                if (attendeeCount >= feeRow.GroupMaxCount)
                {
                    if (string.IsNullOrEmpty(calcString))
                        calcString = (feeRow.GroupMaxCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                    else
                        calcString = calcString + ", " + (feeRow.GroupMaxCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                }
                else if (attendeeCount >= feeRow.GroupMinCount && attendeeCount < feeRow.GroupMaxCount)
                {
                    if (string.IsNullOrEmpty(calcString))
                        calcString = (attendeeCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                    else
                        calcString = calcString + ", " + (attendeeCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                }
                i = i + 1;
            }

            return calcString;

        }

        public List<RegistrationCharge> GetCalculatedRegCharges(int regId, DateTime regDate, int eventId, bool isMember, int? orgFeeCategoryId, int attendeeCount, int period)
        {
            int i = 0;
            int speakerCount = 0;
            int nonSpeakerCount = 0;
            decimal basePrice = 0;
            decimal attendeePrice = 0;
            List<RegistrationCharge> charges = new List<RegistrationCharge>();

            // Get speaker count for registration
            RegistrationMethods rm = new RegistrationMethods();
            speakerCount = rm.GetSpeakerCount(regId);
            nonSpeakerCount = attendeeCount - speakerCount;

            if (speakerCount > 0)
            { 
                RegistrationCharge spch = new RegistrationCharge();
                spch.ChargeAmt = 0;
                spch.ChargeDate = regDate;
                spch.ChargeDesc = "Speakers: " + speakerCount.ToString();
                spch.RegistrationId = regId;
                charges.Add(spch);
            }
            

            // Get fee table
            IQueryable<RegistrationFeeTable> query =

                  from fee in this.ObjectContext.RegistrationFeeTables
                  where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == orgFeeCategoryId
                  orderby fee.GroupMinCount
                  select fee;

            // If no rows returned, widen search to include NULL fee category (for Workshops and Training events)
            if (query.Count() == 0)
            {
                query = from fee in this.ObjectContext.RegistrationFeeTables
                        where fee.EventId == eventId && fee.IsMemberRate == isMember && fee.FeeCategoryId == null
                        orderby fee.GroupMinCount
                        select fee;
            }

            foreach (RegistrationFeeTable feeRow in query)
            {
                RegistrationCharge ch = new RegistrationCharge();

                attendeePrice = (period == 1 ? feeRow.Period1AttendeePrice : (period == 2 ? feeRow.Period2AttendeePrice : feeRow.Period3AttendeePrice));
                basePrice = (period == 1 ? feeRow.Period1GroupPrice : (period == 2 ? feeRow.Period2GroupPrice : feeRow.Period3GroupPrice));

                if (nonSpeakerCount >= feeRow.GroupMaxCount)
                {
                    // Base
                    ch = new RegistrationCharge();
                    ch.ChargeAmt = basePrice;
                    ch.ChargeDate = regDate;
                    ch.ChargeDesc = "Group Price";
                    ch.RegistrationId = regId;
                    charges.Add(ch);

                    // Attendee
                    ch = new RegistrationCharge();
                    ch.ChargeAmt = (feeRow.GroupMaxCount - feeRow.GroupMinCount + 1) * attendeePrice;
                    ch.ChargeDate = regDate;
                    ch.ChargeDesc = "Attendees: " + (feeRow.GroupMaxCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                    ch.RegistrationId = regId;
                    charges.Add(ch);
                }
                else if (nonSpeakerCount >= feeRow.GroupMinCount && nonSpeakerCount < feeRow.GroupMaxCount)
                {
                    // Base
                    ch = new RegistrationCharge();
                    ch.ChargeAmt = basePrice;
                    ch.ChargeDate = regDate;
                    ch.ChargeDesc = "Group Price";
                    ch.RegistrationId = regId;
                    charges.Add(ch);

                    ch = new RegistrationCharge();
                    ch.ChargeAmt = (nonSpeakerCount - feeRow.GroupMinCount + 1) * attendeePrice;
                    ch.ChargeDate = regDate;
                    ch.ChargeDesc = "Attendees: " + (nonSpeakerCount - feeRow.GroupMinCount + 1).ToString() + " @ " + attendeePrice.ToString();
                    ch.RegistrationId = regId;
                    charges.Add(ch);
                }
                i = i + 1;
            }

            return charges;
        }

        public List<RegistrationFeeTable> GetRegistrationFeesByEvent(int eventId)
        {
            IQueryable<RegistrationFeeTable> query =

                  from fee in this.ObjectContext.RegistrationFeeTables
                  where fee.EventId == eventId
                  orderby fee.FeeCategoryId, fee.GroupMinCount, fee.IsMemberRate
                  select fee;

            return query.ToList();
        }

        public RegistrationFeeTable GetRegistrationFee(int id)
        {
            return this.ObjectContext.RegistrationFeeTables.Where(f => f.Id == id).FirstOrDefault();
        }

        public void Update(RegistrationFeeTable regFeeTable)
        {
            if (!regFeeTable.IsValid())
            {
                throw new ArgumentException(regFeeTable.GetRuleViolations().FirstOrDefault().ErrorMessage);
            }

            // Get original object
            RegistrationFeeTable c = GetRegistrationFee(regFeeTable.Id);

            // Update the values
            c.FeeCategoryId = regFeeTable.FeeCategoryId;
            c.GroupMaxCount = regFeeTable.GroupMaxCount;
            c.GroupMinCount = regFeeTable.GroupMinCount;
            c.IsMemberRate = regFeeTable.IsMemberRate;
            c.Period1AttendeePrice = regFeeTable.Period1AttendeePrice;
            c.Period1GroupPrice = regFeeTable.Period1GroupPrice;
            c.Period2AttendeePrice = regFeeTable.Period2AttendeePrice;
            c.Period2GroupPrice = regFeeTable.Period2GroupPrice;
            c.Period3AttendeePrice = regFeeTable.Period3AttendeePrice;
            c.Period3GroupPrice = regFeeTable.Period3GroupPrice;
            
        }

        //public decimal GetRegistrationBasePrice(int regId)
        //{
        //    decimal basePrice = 0;
        //    RegistrationMethods m = new RegistrationMethods();
        //    Registration r = m.GetRegistration(regId);

        //    int eventId = r.EventId;
        //    int? orgId = r.OrganizationId;
        //    DateTime regDate = r.EntryDateTime;

        //    basePrice = GetRegistrationBasePrice(eventId, orgId, regDate);
        //    return basePrice;
        //}

        //public decimal GetRegistrationBasePrice(int eventId, int? orgId, DateTime regDate)
        //{
        //    decimal basePrice = 0;
        //    EventManager.Business.EventFeeScheduleMethods fsm = new EventManager.Business.EventFeeScheduleMethods();

        //    if (orgId.HasValue)
        //    {
        //        OrganizationMethods m = new OrganizationMethods();
        //        int? orgFeeCategoryId = m.GetOrganization((Int32)orgId).OrgFeeCategoryId;

        //        if (orgFeeCategoryId != null)
        //        {
        //            // Get fee period we're currently in for the event
        //            int feePeriod = 0;
        //            feePeriod = fsm.GetRegistrationFeePeriod(eventId, regDate);

        //            try
        //            {
        //                if (feePeriod == 1)
        //                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period1GroupPrice;
        //                else if (feePeriod == 2)
        //                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period2GroupPrice;
        //                else if (feePeriod == 3)
        //                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period3GroupPrice;
        //            }
        //            catch (Exception ex)
        //            {
        //                basePrice = 0;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        basePrice = fsm.GetIndividualPrice(eventId, regDate);
        //    }

        //    return basePrice;
        //}

        //public decimal GetRegistrationExtraAttendeePrice(int regId)
        //{ 
        //    RegistrationMethods m = new RegistrationMethods();
        //    Registration r = m.GetRegistration(regId);

        //    int eventId = r.EventId;
        //    int? orgId = r.OrganizationId;
        //    DateTime regDate = r.EntryDateTime;

        //    return GetRegistrationExtraAttendeePrice(eventId, orgId, regDate);
        //}

        //public decimal GetRegistrationExtraAttendeePrice(int eventId, int? orgId, DateTime regDate)
        //{
        //    decimal extraAttendeePrice = 0;

        //    if (orgId.HasValue)
        //    { 
        //        OrganizationMethods m = new OrganizationMethods();
        //        int? orgFeeCategoryId = m.GetOrganization((Int32)orgId).OrgFeeCategoryId;

        //        if (orgFeeCategoryId != null)
        //        {
        //            // Get fee period we're currently in for the event
        //            int feePeriod = 0;
        //            EventManager.Business.EventFeeScheduleMethods fsm = new EventManager.Business.EventFeeScheduleMethods();
        //            feePeriod = fsm.GetRegistrationFeePeriod(eventId, regDate);

        //            try
        //            {
        //                if (feePeriod == 1)
        //                    extraAttendeePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period1AddAttendeePrice;
        //                else if (feePeriod == 2)
        //                    extraAttendeePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period2AddAttendeePrice;
        //                else if (feePeriod == 3)
        //                    extraAttendeePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period3AddAttendeePrice;
        //            }
        //            catch (Exception ex)
        //            {
        //                extraAttendeePrice = 0;
        //            }
        //        }
        //    }

        //    return extraAttendeePrice;
        //}


    }
}
