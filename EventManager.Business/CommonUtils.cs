﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using EventManager.Model;

namespace EventApp.Common
{
    public class CommonUtils
    {
        public CommonUtils()
        {}

        public static object DeSerializeObjectFromXml(string xmlObject, Type theType)
        {
            object theReturn = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlObject);

            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(theType);
            theReturn = ser.Deserialize(reader);

            return theReturn;
        }

        //public static EventManager.Model.Entities GetEntityDataObject()
        //{
        //    string conString = CommonUtils.GetConnectString("DBEntities");
        //    Entities db = new Entities(conString);

        //    return db;
        //}

        //public static List<State> GetStateList()
        //{
        //    Entities db = CommonUtils.GetEntityDataObject();

        //    return db.States.OrderBy(t => t.StateName).ToList();
        //}

        public static string SerializeObjectToXml(object inputObj)
        {
            string theReturn = "";

            XmlSerializer ser = new XmlSerializer(inputObj.GetType());
            StringBuilder sb = new StringBuilder();
            StringWriter writer = new StringWriter(sb);
            ser.Serialize(writer, inputObj);
            theReturn = sb.ToString();

            return theReturn;
        }

        public static string GetConfigSetting(string theSetting)
        {
            return System.Configuration.ConfigurationManager.AppSettings[theSetting].ToString();
        }

        public static string GetConnectString(string theSetting)
        {
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings[theSetting].ConnectionString.Replace("&quot;", "'");

            cs = cs.Replace("\"", "'");

            return cs;
        }
    }
}