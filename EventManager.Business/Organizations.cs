﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Data;
using EventManager.Model;
using EventApp.Common;

namespace EventManager.Business
{
    public class OrganizationMethods : MethodBase<Organization>
	{
		public Organization GetOrganization(int orgId)
		{
            return this.ObjectContext.Organizations.Where(t => t.Id == orgId).FirstOrDefault();
		}

        public Organization GetOrganizationByName(int associationId, string orgName)
        {
            return this.ObjectContext.Organizations.Where(t => t.AssociationId == associationId && t.OrgName.ToUpper().Trim() == orgName.ToUpper().Trim()).FirstOrDefault();
        }

        public List<Organization> GetOrganizationsByAssociation(int associationId)
        {
            IQueryable<Organization> query =

                    from att in this.ObjectContext.Organizations
                    where att.AssociationId == associationId
                    orderby att.OrgName 
                    select att;

            return query.ToList();
        }

        public List<Organization> GetOrganizationsByAdmin(int attendeeId)
        {
            IQueryable<Organization> query =

                    from att in this.ObjectContext.Organizations
                    where att.AdminAttendeeId == attendeeId
                    orderby att.OrgName
                    select att;

            return query.ToList();
        }

        public Decimal GetBasePrice(int eventId, int orgId)
        {
            Decimal basePrice = 0;

            // Get Fee category ID
            int orgFeeCategoryId;

            try
            {
                Organization o = GetOrganization(orgId);
                orgFeeCategoryId = o.OrganizationFeeCategory.Id;

                // Get fee period we're currently in for the event
                int feePeriod = 0;
                EventManager.Business.EventFeeScheduleMethods om = new EventManager.Business.EventFeeScheduleMethods();
                feePeriod = om.GetRegistrationFeePeriod(eventId, DateTime.Now);

            
                if (feePeriod == 1)
                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period1GroupPrice;
                else if (feePeriod == 2)
                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period2GroupPrice;
                else if (feePeriod == 3)
                    basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period3GroupPrice;
            }
            catch (Exception ex)
            { 
                basePrice = 0;
            }
            
            return basePrice;
        }

        public List<OrganizationFeeCategory> GetFeeCategories(int associationId)
        {
            IQueryable<OrganizationFeeCategory> query =

                    from att in this.ObjectContext.OrganizationFeeCategories 
                    where att.OrganizationType.AssociationId == associationId
                    select att;

            return query.ToList();


        }

        public void MergeDuplicateOrganizations(int keepOrgId, int deleteOrgId)
        {
            // Make sure both orgs have never registered for same event
            RegistrationMethods rm = new RegistrationMethods();
            List<Registration> regs = rm.GetRegistrationHistoryForOrganization(deleteOrgId);
            foreach (Registration r in regs)
            { 
                int eventId = r.EventId;
                Registration r2 = rm.GetRegistrationByOrganization(eventId, keepOrgId);
                if (r2 != null)
                {
                    throw new ApplicationException("Facilities cannot be merged because they have registered for the same event in the past: '" + r2.Event.EventName + "'");
                }
            }

            // Update Attendee table
            AttendeeMethods am = new AttendeeMethods();
            List<Attendee> atts = am.GetAttendeesByOrganization(deleteOrgId);
            foreach (Attendee att in atts)
            {
                att.OrganizationId = keepOrgId;
            }

            // Update Registration table
            foreach (Registration oldReg in regs)
            {
                oldReg.OrganizationId = keepOrgId;
            }

            // Delete org
            OrganizationMethods om = new OrganizationMethods();
            Organization delOrg = om.GetOrganization(deleteOrgId);
            om.Delete(delOrg);

        }

        public OrganizationFeeCategory GetFeeCategory(int id)
        {
            return this.ObjectContext.OrganizationFeeCategories.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<OrganizationFeeCategory> GetFeeCategoriesByOrgType(int orgTypeId)
        {
            IQueryable<OrganizationFeeCategory> query =

                    from att in this.ObjectContext.OrganizationFeeCategories
                    where att.OrganizationTypeId == orgTypeId
                    select att;

            return query.ToList();
        }

        public List<OrganizationType> GetOrganizationTypes(int associationId)
        {
            IQueryable<OrganizationType> query =

                    from att in this.ObjectContext.OrganizationTypes
                    where att.AssociationId == associationId
                    select att;

            return query.ToList();
        }

        public Organization AddOrganization(Organization org)
        {
            // Make sure we don't add a duplicate org
            if (GetOrganizationByName(org.AssociationId, org.OrgName) != null)
            {
                throw new ApplicationException("Facility with name '" + org.OrgName + "' already exists.");
            }

            base.Add(org);
            SaveAllObjectChanges();

            return org;
        }

        public Organization AddOrganization(int associationId, string orgName)
        {
            // Make sure we don't add a duplicate org
            if (GetOrganizationByName(associationId, orgName) != null)
            {
                throw new ApplicationException("Facility with name '" + orgName + "' already exists.");
            }

            Organization o = new Organization();
            o.AssociationId = associationId;
            o.OrgName = orgName;
            o.BedCount = 0;
            base.Add(o);
            SaveAllObjectChanges();

            return o;
        }

        public void AddOrganization(out int id, int AssociationId, string OrgName, int OrgTypeId, int BedCount, string CorporateAffiliation, string Address, string City, string StateCode, string Zip, string Phone, string Fax, string AdminName, string AdminEmail, int OrgFeeCategoryId)
        {
            // Make sure we don't add a duplicate org
            if (GetOrganizationByName(AssociationId, OrgName) != null)
            {
                throw new ApplicationException("Facility with name '" + OrgName + "' already exists.");
            }

            Organization o = new Organization();
            o.AssociationId = AssociationId;
            o.Address = Address;
            o.BedCount = BedCount;
            o.City = City;
            o.CorporateAffiliation = CorporateAffiliation;
            o.Fax = Fax;
            o.OrgFeeCategoryId = OrgFeeCategoryId;
            o.OrgName = OrgName;
            o.OrgTypeId = OrgTypeId;
            o.Phone = Phone;
            o.StateCode = StateCode;
            o.Zip = Zip;
            base.Add(o);
            SaveAllObjectChanges();

            id = o.Id;
        }

        public void Update(Organization org)
        {
            Organization o = GetOrganization(org.Id);

            // If changing org name, make sure we don't create a duplicate org
            if (org.OrgName.ToUpper().Trim() != o.OrgName.ToUpper().Trim())
            { 
                if (GetOrganizationByName(org.AssociationId, org.OrgName) != null)
                {
                    throw new ApplicationException("Facility with name '" + org.OrgName + "' already exists.");
                }
            }
            

            o.Address = org.Address;
            o.AdminAttendeeId = org.AdminAttendeeId;
            o.BedCount = org.BedCount;
            o.City = org.City;
            o.CorporateAffiliation = org.CorporateAffiliation;
            o.Fax = org.Fax;
            o.OrgFeeCategoryId = org.OrgFeeCategoryId;
            o.OrgName = org.OrgName;
            o.OrgTypeId = org.OrgTypeId;
            o.Phone = org.Phone;
            o.StateCode = org.StateCode;
            o.Zip = org.Zip;
        }

        public override void Delete(Organization org)
        {
            var query = from att in this.ObjectContext.Attendees
                        where att.OrganizationId == org.Id
                        select att;

            foreach (Attendee a in query.ToList())
            {
                a.Organization = null;
            }
            Organization o = GetOrganization(org.Id);
            base.Delete(o);
        }

        public void DeleteWithAttendees(Organization org)
        { 
            Organization o = GetOrganization(org.Id);
            foreach(Attendee a in o.Attendees)
            {       
                o.Attendees.Remove(a);
                base.Delete(o);
            }
            
        }
	}
}
