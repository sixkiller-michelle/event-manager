﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Metadata.Edm;
using EventManager.ObjectContextManagement;
using EventManager.Model;
using System.Web.Security;

namespace EventManager.Business
{
    /// <summary>
    /// Generic base class for all other Facade classes.
    /// </summary>
    /// <typeparam name="T">An EntityObject type.</typeparam>
    public abstract class MethodBase<T> where T : System.Data.Objects.DataClasses.EntityObject
    {
        private ObjectContextManager<Entities> _objectContextManager;

        /// <summary>
        /// Returns the current ObjectContextManager instance. Encapsulated the 
        /// _objectContextManager field to show it as an association on the class diagram.
        /// </summary>
        private ObjectContextManager<Entities> ObjectContextManager
        {
            get { return _objectContextManager; }
            set { _objectContextManager = value; }
        }

        /// <summary>
        /// Returns a BariatrakObjectContext object. 
        /// </summary>
        protected internal Entities ObjectContext
        {
            get
            {
                if (ObjectContextManager == null)
                    this.InstantiateObjectContextManager();

                return ObjectContextManager.ObjectContext;
            }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MethodBase()
        { }

        /// <summary>
        /// Instantiates a new ObjectContextManager based on application configuration settings.
        /// </summary>
        private void InstantiateObjectContextManager()
        {
            /* Retrieve ObjectContextManager configuration settings: */
            Hashtable ocManagerConfiguration = ConfigurationManager.GetSection("EventManager.Business.ObjectContext") as Hashtable;
            if (ocManagerConfiguration != null && ocManagerConfiguration.ContainsKey("managerType"))
            {
                string managerTypeName = ocManagerConfiguration["managerType"] as string;
                if (string.IsNullOrEmpty(managerTypeName))
                    throw new ConfigurationErrorsException("The managerType attribute is empty.");
                else
                    managerTypeName = managerTypeName.Trim().ToLower();

                try
                {
                    /* Try to create a type based on it's name: */
                    Assembly frameworkAssembly = Assembly.GetAssembly(typeof(ObjectContextManager<>));
                    /* We have to fix the name, because its a generic class: */
                    Type managerType = frameworkAssembly.GetType(managerTypeName + "`1", true, true);
                    managerType = managerType.MakeGenericType(typeof(Entities));

                    /* Try to create a new instance of the specified ObjectContextManager type: */
                    this.ObjectContextManager = Activator.CreateInstance(managerType) as ObjectContextManager<Entities>;
                }
                catch (Exception e)
                {
                    throw new ConfigurationErrorsException("The managerType specified in the configuration is not valid.", e);
                }
            }
            else
                throw new ConfigurationErrorsException("A EventManager.ObjectContext tag or its managerType attribute is missing in the configuration.");
        }

        public Int32 AssociationId
        {
            get
            {
                int assnId = 0;
                MembershipUser user = Membership.GetUser();
                if (user != null)
                {
                    UserAssociationMethods m = new UserAssociationMethods();
                    assnId = m.GetAssociationIdForUser(new Guid(user.ProviderUserKey.ToString()));
                }
                return assnId;
                
            }
        }

        /// <summary>
        /// Persists all changes to the underlying datastore.
        /// </summary>
        public void SaveAllObjectChanges()
        {
            try
            {
                this.ObjectContext.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        /// <summary>
        /// Adds a new entity object to the context.
        /// </summary>
        /// <param name="newObject">A new object.</param>
        public virtual void Add(T newObject)
        {
            string className = newObject.GetType().Name;
            var container = this.ObjectContext.MetadataWorkspace.GetEntityContainer(this.ObjectContext.DefaultContainerName, DataSpace.CSpace);
            string setName = (from meta in container.BaseEntitySets
                              where meta.ElementType.Name == className
                              select meta.Name).First();

            this.ObjectContext.AddObject(setName, newObject);
        }

        /// <summary>
        /// Adds a new entity object to the context.
        /// </summary>
        /// <param name="newObject">A new object.</param>
        public virtual T Add(T newObject, string entitySetName)
        {
            this.ObjectContext.AddObject(entitySetName, newObject);
            return newObject;
        }

        /// <summary>
        /// Deletes an entity object.
        /// </summary>
        /// <param name="obsoleteObject">An obsolete object.</param>
        public virtual void Delete(T obsoleteObject)
        {
            string className = obsoleteObject.GetType().Name;
            var container = this.ObjectContext.MetadataWorkspace.GetEntityContainer(this.ObjectContext.DefaultContainerName, DataSpace.CSpace);
            string setName = (from meta in container.BaseEntitySets
                              where meta.ElementType.Name == className
                              select meta.Name).First();

            this.ObjectContext.AttachTo(setName, obsoleteObject);
            this.ObjectContext.DeleteObject(obsoleteObject);
        }
    }
}
