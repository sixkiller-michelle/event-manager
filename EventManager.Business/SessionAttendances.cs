﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.Web.Security;

namespace EventManager.Business
{
    public class SessionAttendanceMethods : MethodBase<SessionAttendance>
    {
        public SessionAttendance GetSessionAttendance(int id)
        {
            return this.ObjectContext.SessionAttendances.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<SessionAttendance> GetAttendanceBySession(int sessionId)
        {
            IQueryable<SessionAttendance> query =

                  from evt in this.ObjectContext.SessionAttendances
                  where evt.SessionId == sessionId
                  select evt;

            return query.ToList();
        }

        public List<SessionAttendance> GetAttendanceByEvent(int eventId)
        {
            IQueryable<SessionAttendance> query =

                  from evt in this.ObjectContext.SessionAttendances
                  where evt.EventSession.EventId == eventId
                  select evt;

            return query.ToList();
        }


        public List<SessionAttendance> GetAttendanceByAttendee(int eventId, int attendeeId)
        {
            IQueryable<SessionAttendance> query =

                  from evt in this.ObjectContext.SessionAttendances
                  where evt.RegistrationAttendee.Registration.EventId == eventId && evt.RegistrationAttendee.AttendeeId == attendeeId
                  select evt;

            return query.ToList();
        }

        public List<SessionAttendance> GetAttendanceByRegId(int registrationAttendeeId)
        {
            IQueryable<SessionAttendance> query =

                  from evt in this.ObjectContext.SessionAttendances
                  where evt.RegistrationAttendeeId == registrationAttendeeId
                  select evt;

            return query.ToList();
        }

        public SessionAttendance GetAttendanceByRegIdAndSession(int registrationAttendeeId, int sessionId)
        {
            return this.ObjectContext.SessionAttendances.Where(s => s.RegistrationAttendeeId == registrationAttendeeId && s.SessionId == sessionId).FirstOrDefault();
        }

        public bool ProcessAttendance(int eventId)
        {
            try
            { 
                this.ObjectContext.DoEventAttendance(eventId);
                return true;
            }
            catch(Exception)
            {
                return false;
                throw;
            }
        }

        public void AddSpeaker(int eventId, int sessionId, int attendeeId)
        { 
            // See if there is a registration for the user
            EventManager.Model.RegistrationAttendee attendeeReg = this.ObjectContext.RegistrationAttendees.Where(t => t.AttendeeId == attendeeId && t.Registration.EventId == eventId).FirstOrDefault();

            if (attendeeReg == null)
            {
                // Get attendee info
                EventManager.Model.Attendee a = this.ObjectContext.Attendees.Where(t => t.Id == attendeeId).FirstOrDefault();

                // See if there is a registration for the attendee's facility
                RegistrationMethods erm = new RegistrationMethods();
                EventManager.Model.Registration reg = erm.GetRegistrationByOrganization(eventId, Convert.ToInt32(a.OrganizationId));

                if (reg == null)
                { 
                    // Create the registration 
                    reg = new EventManager.Model.Registration();
                    reg.EventId = eventId;
                    reg.OrganizationId = a.OrganizationId;
                    reg.EnteredByUserId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                    reg.EntryDateTime = DateTime.Now;
                    reg.IsWebRegistration = false;
                } 

                // Create the attendee's registration
                attendeeReg = new EventManager.Model.RegistrationAttendee();
                attendeeReg.AttendeeId = a.Id;
                attendeeReg.Title = a.Title;
                attendeeReg.Price = 0;

                reg.RegistrationAttendees.Add(attendeeReg);
                erm.Add(reg);
                  
            }

            // Insert the session attendance record and flag as speaker
            EventManager.Model.SessionAttendance attend = this.ObjectContext.SessionAttendances.Where(t => t.RegistrationAttendeeId == attendeeReg.Id).FirstOrDefault();
            if (attend != null)
            {
                attend.IsSpeaker = true;
            }
            else
            { 
                attend = new EventManager.Model.SessionAttendance();
                attend.RegistrationAttendeeId = attendeeReg.Id;
                attend.IsSpeaker = true;
                attend.SessionId = sessionId;
                base.Add(attend);
            }
  
        }

        public void Add(int registrationAttendeeId, int sessionId)
        {
            int attCount = this.ObjectContext.SessionAttendances.Where(a => a.RegistrationAttendeeId == registrationAttendeeId && a.SessionId == sessionId).Count();
            if (attCount == 0)
            { 
                SessionAttendance sa = new SessionAttendance();
                sa.RegistrationAttendeeId = registrationAttendeeId;
                sa.SessionId = sessionId;
                sa.IsSpeaker = false;
                base.Add(sa);
            }
            
        }

        public void DeleteAllAttendance(int registrationAttendeeId)
        { 
            foreach(SessionAttendance sa in GetAttendanceByRegId(registrationAttendeeId))
            {
                this.ObjectContext.SessionAttendances.DeleteObject(sa);
            }
        }

        public void Delete(int eventRegistrationId, int sessionId)
        {
            SessionAttendance sa = GetAttendanceByRegIdAndSession(eventRegistrationId, sessionId);
            this.ObjectContext.SessionAttendances.DeleteObject(sa);
        }
    }
}
