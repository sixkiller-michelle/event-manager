﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class ScannerDataMethods : MethodBase<ScannerData>
    {
        public ScannerData GetScannerData(int id)
        {
            return this.ObjectContext.ScannerDatas.SingleOrDefault(d => d.Id == id);
        }

        public List<ScannerData> GetScanDataBySessionAndAttendee(int sessionId, int attendeeId)
        {
            IQueryable<ScannerData> query =

                  from evt in this.ObjectContext.ScannerDatas
                  where evt.SessionId == sessionId && evt.AttendeeId == attendeeId
                  select evt;

            return query.ToList();
        }

        public List<ScannerData> GetScanDataByAttendee(int attendeeId)
        {
            IQueryable<ScannerData> query =

                  from evt in this.ObjectContext.ScannerDatas
                  where evt.AttendeeId == attendeeId
                  select evt;

            return query.ToList();
        }

        public List<RptScanReport_Result> GetScanReport(int eventId, int attendeeId, int sessionId)
        {
            return this.ObjectContext.RptScanReport(eventId, sessionId, attendeeId).ToList();
        }

        public override void Add(ScannerData newObject)
        {
            // Make sure attendee exists before adding
            bool doAdd = true;
            if (newObject.AttendeeId.HasValue)
            {
                int attendeeId = Convert.ToInt32(newObject.AttendeeId);
                Attendee att = this.ObjectContext.Attendees.Where(a => a.Id == attendeeId).FirstOrDefault();
                if (att == null)
                    doAdd = false;
            }
            else
            {
                doAdd = false;
            }
            
            if (!doAdd)
                throw new ArgumentException("Scan cannot be added.");

            base.Add(newObject);
        }
    }
}
