﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationChargeMethods : MethodBase<RegistrationCharge>
    {

        public RegistrationCharge GetRegistrationCharge(int id)
        {
            return this.ObjectContext.RegistrationCharges.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<RegistrationCharge> GetChargesByRegistration(int registrationId)
        {
            //List<RegistrationCharge> charges = new List<RegistrationCharge>();

            //RegistrationMethods m = new RegistrationMethods();
            //Registration reg = m.GetRegistration(registrationId);

            //// Add the base charge
            //RegistrationCharge ch = new RegistrationCharge();
            //ch.ChargeAmt = m.GetPriceForGroup(registrationId);
            //ch.ChargeDate = reg.EntryDateTime;
            //ch.ChargeDesc = "Base Price";

            //// Add any extra people charges
            //ch = new RegistrationCharge();
            //ch.ChargeAmt = m.GetPriceForAttendees(registrationId);
            //ch.ChargeDate = reg.EntryDateTime;
            //ch.ChargeDesc = "Extra Attendees";

            //// Add any flag charges (add-ons)
            //ch = new RegistrationCharge();
            //ch.ChargeAmt = m.GetPriceForFlags(registrationId);
            //ch.ChargeDate = reg.EntryDateTime;
            //ch.ChargeDesc = "Add-Ons";
            //return charges;

            IQueryable<RegistrationCharge> query =

                  from evt in this.ObjectContext.RegistrationCharges
                  where evt.RegistrationId == registrationId
                  orderby evt.ChargeDate descending
                  select evt;

            return query.ToList();
            
        }

        public void Update(RegistrationCharge rch)
        {
            RegistrationCharge a = GetRegistrationCharge(rch.Id);
            a.ChargeAmt = rch.ChargeAmt;
            a.ChargeDate = rch.ChargeDate;
            a.ChargeDesc = rch.ChargeDesc;
            a.Notes = rch.Notes;

        }
    }
}
