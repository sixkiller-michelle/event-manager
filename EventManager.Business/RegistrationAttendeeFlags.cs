﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationAttendeeFlags : MethodBase<RegistrationAttendeeFlag>
    {
        public RegistrationAttendeeFlag GetRegistrationAttendeeFlag(int id)
        {
            return this.ObjectContext.RegistrationAttendeeFlags.Where(t => t.Id == id).FirstOrDefault();
        }

        public RegistrationAttendeeFlag GetRegistrationAttendeeFlag(int registrationAttendeeId, int eventFlagId)
        {
            return this.ObjectContext.RegistrationAttendeeFlags.Where(t => t.RegistrationAttendeeId == registrationAttendeeId && t.EventFlagId == eventFlagId).FirstOrDefault();
        }

        public List<RegistrationAttendeeFlag> GetRegisteredAttendeeFlags(int registrationAttendeeId)
        {
            IQueryable<RegistrationAttendeeFlag> query =

                  from reg in this.ObjectContext.RegistrationAttendeeFlags
                  where reg.RegistrationAttendeeId == registrationAttendeeId
                  orderby reg.EventFlag.FlagName
                  select reg;

            return query.ToList();
        }

        public void DeleteAllFlags(int registrationAttendeeId)
        {
            foreach (RegistrationAttendeeFlag f in GetRegisteredAttendeeFlags(registrationAttendeeId))
            {
                base.Delete(f);
            }
        }

        public void Add(int registrationAttendeeId, int eventFlagId)
        {
            RegistrationAttendeeFlag attendeeFlag = new RegistrationAttendeeFlag();
            attendeeFlag.RegistrationAttendeeId = registrationAttendeeId;
            attendeeFlag.EventFlagId = eventFlagId;

            // See if this flag is already assigned
            if (GetRegistrationAttendeeFlag(registrationAttendeeId, eventFlagId) == null)
            {
                base.Add(attendeeFlag);
            }

            
        }
    }
}
