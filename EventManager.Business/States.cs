﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class StateMethods : MethodBase<State>
    {

        public List<State> GetStates()
        {
            IQueryable<State> query =

                 from evt in this.ObjectContext.States
                 orderby evt.StateName
                 select evt;

            return query.ToList();
        }
    }
}
