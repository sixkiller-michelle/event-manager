using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Collections;
using System.Data.SqlClient;
using System.Xml;
using AuthorizeNet;
using AuthorizeNet.APICore;

namespace EventApp.Common
{
    public class CreditProcessor
    {
        private static string m_PostUrl = "https://gateway.merchantplus.com/cgi-bin/PAWebClient.cgi";
        private static string m_Login = "xx";
        private static string m_TransKey = "xx";

        public static bool ANetTest()
        {
            AuthorizationRequest request = new AuthorizationRequest("4111111111111111", "1216", 10.00M, "Test Transaction");

            //step 2 - create the gateway, sending in your credentials 
            Gateway gate = new Gateway("47qU2M9ZgXdb", "54k6gCjUq42Z8v52", true);

            //step 3 - make some money 
            var response = gate.Send(request);

            return true;
        }

        public static bool ProcessCredit(ref CreditTransaction theTransaction, string email)
        {
            CultureInfo usCulture = CultureInfo.CreateSpecificCulture("en-US");
            NumberFormatInfo nfUs = usCulture.NumberFormat;

            if (theTransaction.Amount < Convert.ToDecimal(.01))
            {
                theTransaction.ResultText = "Amount must be greater than, or equal to .01";
                return false;
            }
            string formattedNumber = Convert.ToDecimal(theTransaction.Amount.ToString(), nfUs).ToString("c", nfUs).Replace("$", "");

            if (theTransaction.CardNumber.Length < 15)
            {
                theTransaction.ResultText = "Invalid card number";
                return false;
            }

            if (theTransaction.ExpireMonth < 1 || theTransaction.ExpireMonth > 12)
            {
                theTransaction.ResultText = "Invalid expire month";
                return false;
            }

            if (theTransaction.ExpireYear < DateTime.Now.Year)
            {
                theTransaction.ResultText = "Invalid expire year";
                return false;
            }
            string exp_month = theTransaction.ExpireMonth.ToString();
            if (exp_month.Length == 1)
                exp_month = "0" + exp_month;
            string exp_date = exp_month + theTransaction.ExpireYear.ToString();

            if (theTransaction.CompanyName.Length == 0 && (theTransaction.FirstName.Length == 0 || theTransaction.LastName.Length == 0))
            {
                theTransaction.ResultText = "Must have a valid first/last name";
                return false;
            }

            if (theTransaction.CardCode.Length == 0)
            {
                theTransaction.ResultText = "Invalid 3-Digit Security Code";
                return false;
            }

            String strPost = "x_login=" + m_Login;
            strPost = strPost + "&x_tran_key=" + m_TransKey;
            strPost = strPost + "&x_method=CC";
            strPost = strPost + "&x_type=AUTH_CAPTURE";
            strPost = strPost + "&x_amount=" + formattedNumber;
            strPost = strPost + "&x_delim_data=TRUE";
            strPost = strPost + "&x_delim_char=|";
            strPost = strPost + "&x_relay_response=FALSE";
            strPost = strPost + "&x_card_num=" + theTransaction.CardNumber;
            strPost = strPost + "&x_exp_date=" + exp_date;

            if (theTransaction.CompanyName.Length > 0)
                strPost = strPost + "&x_company=" + theTransaction.CompanyName;
            else
            {
                strPost = strPost + "&x_first_name=" + theTransaction.FirstName;
                strPost = strPost + "&x_last_name=" + theTransaction.LastName;
            }

            if(theTransaction.Address.Trim().Length > 0)
                strPost = strPost + "&x_address=" + theTransaction.Address;

            if (theTransaction.City.Trim().Length > 0)
                strPost = strPost + "&x_city=" + theTransaction.City;

            if (theTransaction.StateCode.Trim().Length > 0)
                strPost = strPost + "&x_state=" + theTransaction.StateCode;

            if (theTransaction.Zip.Trim().Length > 0)
                strPost = strPost + "&x_zip=" + theTransaction.Zip;

            strPost = strPost + "&x_phone=" + theTransaction.Phone;
            strPost = strPost + "&x_cust_id=0";
            strPost = strPost + "&x_invoice_num=0";

            if(theTransaction.PurchaseDescription.Length > 0)
                strPost = strPost + "&x_description=" + theTransaction.PurchaseDescription;

            if(theTransaction.CardCode.Length > 0)
                strPost = strPost + "&x_card_code=" + theTransaction.CardCode;

            bool isTest = false;
            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("localhost") > -1)
                isTest = true;

            //strPost = strPost + "&";
            if (isTest == false)
                strPost = strPost + "&x_test_request=FALSE";
            else
                strPost = strPost + "&x_test_request=TRUE";

            strPost = strPost + "&x_version=3.1";
            StreamWriter myWriter = null;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(m_PostUrl);
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.UnsafeAuthenticatedConnectionSharing = true;

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost);
            }
            catch (Exception e)
            {
                theTransaction.ResultText = e.Message;
                return false;
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = null;
            try
            {
                objResponse = (HttpWebResponse)objRequest.GetResponse();
            }
            catch (Exception e)
            {
                objRequest.Abort();
                objRequest = null;
                theTransaction.ResultText = e.Message;
                return false;
            }

            using (StreamReader sr =
               new StreamReader(objResponse.GetResponseStream()))
            {
                theTransaction.CreditResponseObject = new CreditResponse(sr.ReadToEnd());

                // Close and clean up the StreamReader
                sr.Close();
                objResponse.Close();
                objResponse = null;
            }

            theTransaction.ResultText = theTransaction.CreditResponseObject.TransResponseReasonText;

            if (theTransaction.CreditResponseObject.TransResponseCode.ToString() == "Approved")
            {
                theTransaction.ResultText = "Approved";
                return true;
            }
            else
            {
                theTransaction.ResultText = theTransaction.CreditResponseObject.TransResponseReasonText;
                return false;
            }
        }
    }

    public enum ResponseCode
    {
        Approved = 1,
        Declined = 2,
        Error = 3
    }

    public class CreditResponse
    {
        private ResponseCode m_ResponseCode;
        private string m_ResponseSubCode = "";
        private string m_ResponseReasonCode = "";
        private string m_ResponseReasonText = "";
        private string m_AVSResultCode = "";
        private string m_ApprovalCode = "";
        private string m_TransactionID = "";
        private string m_CardCodeResponse = "";

        public CreditResponse(string theResponseString)
        {
            string[] theResponse = theResponseString.Split(Convert.ToChar("|"));

            int iCode = Convert.ToInt32(theResponse[0]);
            m_ResponseCode = (ResponseCode)iCode;
            m_ResponseSubCode = theResponse[1];
            m_ResponseReasonCode = theResponse[2];
            m_ResponseReasonText = theResponse[3];
            m_ApprovalCode = theResponse[4];
            m_AVSResultCode = theResponse[5];
            m_TransactionID = theResponse[6];
            m_CardCodeResponse = theResponse[38];
        }

        public ResponseCode TransResponseCode
        {
            get { return m_ResponseCode; }
        }

        public string CardCodeResponse
        {
            get { return m_CardCodeResponse; }
        }

        public string TransResponseSubCode
        {
            get { return m_ResponseSubCode; }
        }

        public string ApprovalCode
        {
            get { return m_ApprovalCode; }
        }

        public string TransactionID
        {
            get { return m_TransactionID; }
        }

        public string AVSResultCode
        {
            get { return m_AVSResultCode; }
        }

        public string TransResponseReasonCode
        {
            get { return m_ResponseReasonCode; }
        }

        public string TransResponseReasonText
        {
            get { return m_ResponseReasonText; }
        }
    }

    public class CreditTransaction : BaseTransaction
    {
        private string m_CardNumber = "";
        private string m_CardType = "";
        private int m_ExpireMonth = 0;
        private int m_ExpireYear = 0;
        private string m_CompanyName = "";
        private string m_FirstName = "";
        private string m_LastName = "";
        private string m_Address = "";
        private string m_City = "";
        private string m_StateCode = "";
        private string m_Zip = "";
        private string m_Phone = "";
        private string m_CardCode = "";
        private CreditResponse m_TheResponse;

        public CreditResponse CreditResponseObject
        {
            get { return m_TheResponse; }
            set { m_TheResponse = value; }
        }

        public string CardNumber
        { 
            get { return m_CardNumber; }
            set { m_CardNumber = value; }
        }

        public string CardType
        {
            get { return m_CardType; }
            set { m_CardType = value; }
        }

        public string CardCode
        {
            get { return m_CardCode; }
            set { m_CardCode = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string StateCode
        {
            get { return m_StateCode; }
            set { m_StateCode = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Zip
        {
            get { return m_Zip; }
            set { m_Zip = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }

        public int ExpireMonth
        {
            get { return m_ExpireMonth; }
            set { m_ExpireMonth = value; }
        }

        public int ExpireYear
        {
            get { return m_ExpireYear; }
            set { m_ExpireYear = value; }
        }
    }

    public abstract class BaseTransaction
    {
        private decimal m_Amount = 0;
        private string m_PurchaseDescription = "";
        private string m_ResultText = "";

        public decimal Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string PurchaseDescription
        {
            get { return m_PurchaseDescription; }
            set { m_PurchaseDescription = value; }
        }

        public string ResultText
        {
            get { return m_ResultText; }
            set { m_ResultText = value; }
        }
    }
}
