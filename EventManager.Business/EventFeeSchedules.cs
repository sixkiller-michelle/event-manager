﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Business
{
    public class EventFeeScheduleMethods : MethodBase<EventFeeSchedule>
    {
        public int GetRegistrationFeePeriod(int eventId, DateTime regDate)
        {
            int period = 0;
            DateTime startRange = new DateTime();
            DateTime endRange = new DateTime();
            IQueryable<EventFeeSchedule> query =

                  from evt in this.ObjectContext.EventFeeSchedules
                  where evt.EventId == eventId
                  orderby evt.StartDateTime
                  select evt;

            int i = 0;
            foreach(EventFeeSchedule s in query.ToList())
            {
                if (i == 0)
                    startRange = s.StartDateTime;
                else if (i == query.Count() - 1)
                    endRange = s.EndDateTime;

                if (regDate >= s.StartDateTime && regDate <= s.EndDateTime)
                {
                    period = i + 1;
                }
                i++;
            }

            if (period == 0)
            {
                // Account for registration being before first time period
                if (query.Count() > 0 && regDate < startRange)
                {
                    period = 1;
                }
                // Account for registration being after last time period
                else if (query.Count() > 0 && regDate > endRange)
                {
                    period = query.Count();
                }
            }

            return period;
        }

        public decimal GetIndividualPrice(int eventId, DateTime regDate)
        {
            IQueryable<EventFeeSchedule> query =

                  from evt in this.ObjectContext.EventFeeSchedules
                  where evt.EventId == eventId && evt.StartDateTime <= regDate && evt.EndDateTime >= regDate
                  select evt;

            if (query.Count() > 0)
                return query.SingleOrDefault().IndividualPrice;
            else
                return 0;
        }

        public List<EventFeeSchedule> GetFeeSchedulesForEvent(int eventId)
        {
            IQueryable<EventFeeSchedule> query =

                 from evt in this.ObjectContext.EventFeeSchedules
                 where evt.EventId == eventId
                 orderby evt.StartDateTime, evt.EndDateTime
                 select evt;

            return query.ToList();
        }

        public EventFeeSchedule GetFeeSchedulePeriod(int id)
        {
            return this.ObjectContext.EventFeeSchedules.Where(f => f.Id == id).FirstOrDefault();
        }

        public void Update(EventFeeSchedule eventFeeSchedule)
        {
            if (!eventFeeSchedule.IsValid())
            {
                throw new ArgumentException(eventFeeSchedule.GetRuleViolations().FirstOrDefault().ErrorMessage);
            }

            // Get original object
            EventFeeSchedule c = GetFeeSchedulePeriod(eventFeeSchedule.Id);

            // Update the values
            c.EndDateTime = eventFeeSchedule.EndDateTime;
            c.IndividualPrice = eventFeeSchedule.IndividualPrice;
            c.PeriodNumber = eventFeeSchedule.PeriodNumber;
            c.StartDateTime = eventFeeSchedule.StartDateTime;
            c.TimeframeDesc = eventFeeSchedule.TimeframeDesc;
            c.TimeframeName = eventFeeSchedule.TimeframeName;
           
        }
    }
}
