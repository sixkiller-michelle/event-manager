﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.IO;

namespace EventManager.Business
{
    public class AspRegRequestNewAssociationMethods : MethodBase<AspRegRequestNewAssociation>
    {
        public AspRegRequestNewAssociation GetAssociationRequest(int id)
        {
            return this.ObjectContext.AspRegRequestNewAssociations.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<AspRegRequestNewAssociation> GetAssociationRequests()
        {
            IQueryable<AspRegRequestNewAssociation> query =

                  from evt in this.ObjectContext.AspRegRequestNewAssociations
                  orderby evt.RequestDateTime descending
                  select evt;

            return query.ToList();
        }

        public List<AspRegRequestNewAssociation> GetUnapprovedAssociationRequests()
        {
            IQueryable<AspRegRequestNewAssociation> query =

                  from evt in this.ObjectContext.AspRegRequestNewAssociations
                  where evt.IsApproved == false
                  orderby evt.RequestDateTime descending 
                  select evt;

            return query.ToList();
        }

        //public override void Add(AspRegRequestNewAssociation newAssociationRequest, AspRegRequestAssociationEmployee assnEmployeeRequest)
        //{


        //    // Send email to developers
        //    Email email = new Email();
        //    email.SendAssociationRequestEmail(newObject.aspnet_Users.aspnet_Membership.Email);

        //    base.Add(newObject);
        //}

     
    }
}
