﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.Web.Services;

namespace EventManager.Business
{
    public class EventCertificateMethods : MethodBase<EventCertificate>
    {

        public EventCertificate GetCertificateInfoByEvent(int eventId)
        {
            return this.ObjectContext.EventCertificates.Where(t => t.EventId == eventId).FirstOrDefault();
        }

        public void Update(EventCertificate cert)
        {
            EventCertificate s = GetCertificateInfoByEvent(cert.EventId);
            s.CertificateHeaderText = cert.CertificateHeaderText;
            s.CertificateFooterText = cert.CertificateFooterText;
        }

        public void Delete(int eventId)
        {
            EventCertificate s = GetCertificateInfoByEvent(eventId);
            base.Delete(s);
        }

    }
}
