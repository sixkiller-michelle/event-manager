﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Data;
using EventApp.Common;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Business
{
    public class AttendeeMethods : MethodBase<Attendee>
	{
		public Attendee GetAttendee(int attendeeId)
		{
            return this.ObjectContext.Attendees.Where(t => t.Id == attendeeId).FirstOrDefault();
		}

        public Attendee GetAttendeeByUserId(Guid userId)
        {
            return this.ObjectContext.Attendees.Where(t => t.UserId == userId).FirstOrDefault();
        }

        public List<Attendee> GetAttendees(int associationId)
        {
            IQueryable<Attendee> query =

                    from att in this.ObjectContext.Attendees
                    where att.AssociationId == associationId
                    orderby att.LastName, att.FirstName
                    select att;

            return query.ToList();
        }


        public List<RptAttendeeCertificateList_Result> GetCertificatePrintList(int attendeeId)
        {
            return this.ObjectContext.RptAttendeeCertificateList(attendeeId).ToList();
        }

        public List<Attendee> GetAttendeesByName(int associationId, string fullName)
        {
            IQueryable<Attendee> query =

                    from att in this.ObjectContext.Attendees
                    where att.AssociationId == associationId && (att.LastName.ToUpper() + ", " + att.FirstName.ToUpper()).StartsWith(fullName.ToUpper()) 
                    orderby att.LastName, att.FirstName
                    select att;

            return query.ToList();
        }

        public List<Attendee> GetAttendeesByEvent(int eventId)
        {
            IQueryable<Attendee> query =

                   from att in this.ObjectContext.RegistrationAttendees
                   where att.Registration.EventId == eventId
                   orderby att.Attendee.LastName, att.Attendee.FirstName
                   select att.Attendee;

            return query.ToList();
        }

     
        public List<Attendee> GetAttendeesByNameAndFacility(int associationId, int? orgId, string firstName, string lastName, bool fromRegHistory)
        {
            IQueryable<Attendee> query; 

            if (orgId.HasValue)
            {
                if (fromRegHistory)
                    
                    query =

                        from att in this.ObjectContext.RegistrationAttendees
                        where att.Registration.Event.AssociationId == associationId && att.Registration.OrganizationId == orgId && att.Attendee.LastName.Trim().ToUpper() == lastName.Trim().ToUpper() && att.Attendee.FirstName.Trim().ToUpper() == firstName.Trim().ToUpper()
                        orderby att.Registration.Event.StartDateTime descending
                        select att.Attendee;
                else
                    query =

                        from att in this.ObjectContext.Attendees
                        where att.AssociationId == associationId && att.OrganizationId == orgId && att.LastName.Trim().ToUpper() == lastName.Trim().ToUpper() && att.FirstName.Trim().ToUpper() == firstName.Trim().ToUpper()
                        orderby att.LastName, att.FirstName
                        select att;
            }
            else
            {
                if (fromRegHistory)
                
                    query =

                        from att in this.ObjectContext.Attendees
                        where att.AssociationId == associationId && att.OrganizationId.HasValue == false && att.LastName.Trim().ToUpper() == lastName.Trim().ToUpper() && att.FirstName.Trim().ToUpper() == firstName.Trim().ToUpper()
                        orderby att.LastName, att.FirstName
                        select att;
                else
                    query =

                       from att in this.ObjectContext.RegistrationAttendees
                       where att.Registration.Event.AssociationId == associationId && att.Registration.OrganizationId.HasValue == false && att.Attendee.LastName.Trim().ToUpper() == lastName.Trim().ToUpper() && att.Attendee.FirstName.Trim().ToUpper() == firstName.Trim().ToUpper()
                       orderby att.Registration.Event.StartDateTime descending
                       select att.Attendee;
            }
            

            return query.Distinct().ToList();
        }

        public List<Attendee> GetAttendeesByOrganization(int? orgId)
        {
            IQueryable<Attendee> query =

                    from att in this.ObjectContext.Attendees
                    where att.OrganizationId == orgId
                    orderby att.LastName, att.FirstName 
                    select att;

            return query.ToList();
        }

        public void MergeDuplicateAttendees(int keepAttendeeId, int deleteAttendeeId)
        { 
            //AttendeeEventSurvey
            //ScannerData
            //SessionSpeaker

            //RegistrationAttendee
            //RegistrationAttendeeFlag
            //SessionAttendance

            // Update AttendeeEventSurvey 
            EventManager.Business.AttendeeEventSurveyMethods m1 = new EventManager.Business.AttendeeEventSurveyMethods();
            List<AttendeeEventSurvey> surveys = m1.GetEventSurveysByAttendee(deleteAttendeeId);
            foreach (AttendeeEventSurvey s in surveys)
            {
                // See if there is already a survey for the new attendee ID
                if (m1.GetEventSurveyByAttendee(s.EventId, keepAttendeeId) == null)
                    s.AttendeeId = keepAttendeeId;
            }

            // Update ScannerData
            EventManager.Business.ScannerDataMethods m2 = new EventManager.Business.ScannerDataMethods();
            List<ScannerData> scans = m2.GetScanDataByAttendee(deleteAttendeeId);
            foreach (ScannerData s in scans)
            {
                s.AttendeeId = keepAttendeeId;
            }
            

            // Update SessionSpeaker
            EventManager.Business.SessionSpeakerMethods m3 = new EventManager.Business.SessionSpeakerMethods();
            List<SessionSpeaker> speakers = m3.GetSessionsBySpeaker(deleteAttendeeId);
            foreach (SessionSpeaker s in speakers)
            {
                // See if there is already a speaker record for new attendee ID
                if (m3.GetSessionSpeaker(s.SessionId, keepAttendeeId) == null)
                    s.SpeakerId = keepAttendeeId;
            }

            // Update Organizations admin
            EventManager.Business.OrganizationMethods m4 = new EventManager.Business.OrganizationMethods();
            List<Organization> orgs = m4.GetOrganizationsByAdmin(deleteAttendeeId);
            foreach (Organization s in orgs)
            {
                s.AdminAttendeeId = keepAttendeeId;
            }
          

            // Update RegistrationAttendee
            EventManager.Business.RegistrationAttendeesMethods m5 = new EventManager.Business.RegistrationAttendeesMethods();
            List<RegistrationAttendee> regAttendees = m5.GetRegistrationsByAttendee(deleteAttendeeId);
            foreach (RegistrationAttendee regAttendee in regAttendees)
            {
                // See if there is already a registration record for new attendee ID
                if (m5.GetRegistrationAttendeeForEvent(regAttendee.Registration.EventId, keepAttendeeId) == null)
                    regAttendee.AttendeeId = keepAttendeeId;
                //else
                //    m5.Delete(regAttendee);
            }

            // Now we need to delete the old attendee
            EventManager.Business.AttendeeMethods m6 = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee deleteAttendee = m6.GetAttendee(deleteAttendeeId);
            EventManager.Model.Attendee keepAttendee = m6.GetAttendee(keepAttendeeId);
            bool movingUser = false;
            if (deleteAttendee.UserId != null && keepAttendee.UserId == null)
            {
                keepAttendee.UserId = deleteAttendee.UserId;
                movingUser = true;
            }

            // Save changes to the "keep" attendee
            m6.SaveAllObjectChanges();

            // Make change to userId of "delete" attendee (if needed)
            if (movingUser)
            { 
                deleteAttendee.UserId = null;
                m6.SaveAllObjectChanges();
            }

            try
            {
                m6.Delete(deleteAttendee);
                m6.SaveAllObjectChanges();
            }
            catch (Exception ex)
            { 
                
            }
        }

        public Attendee AddAttendee(string firstName, string lastName, string title, int assnId)
        {
            // Check for duplicate attendee
            int attendeeCount = 0;
            attendeeCount = this.ObjectContext.Attendees.Where(att => att.AssociationId == assnId && att.FirstName == firstName && att.LastName == lastName).Count();
            if (attendeeCount > 0)
            {
                throw new ArgumentException("Attendee '" + lastName + ", " + firstName + "' already exists.");
            }
            

            Attendee a = new Attendee();
            a.FirstName = firstName;
            a.LastName = lastName;
            a.Title = title;
            a.AssociationId = assnId;
            a.IsApproved = true;
            base.Add(a);
            SaveAllObjectChanges();

            return a;
        }

        public Attendee AddAttendee(Attendee att)
        {
            // do not allow duplicate attendee in an organization
            int attendeeCount = 0;

            if (string.IsNullOrWhiteSpace(att.FirstName) || string.IsNullOrWhiteSpace(att.LastName))
            {
                throw new ArgumentException("First and last name are required");
            }

            if (att.OrganizationId != null)
            {
                attendeeCount = this.ObjectContext.Attendees.Where(a => a.OrganizationId == att.OrganizationId && a.FirstName == att.FirstName && a.LastName == att.LastName).Count();
                if (attendeeCount > 0)
                {
                    throw new ArgumentException("Attendee '" + att.LastName + ", " + att.FirstName + "' is already a staff member of the facility");
                }
            }
                
            base.Add(att);
            SaveAllObjectChanges();

            return att;
        }

        public void Update(int attendeeId, string firstName, string lastName, string middleInitial, string phone, string email)
        {
            Attendee att = GetAttendee(attendeeId);
            att.FirstName = firstName;
            att.LastName = lastName;
            att.MiddleInitial = middleInitial;
            att.PhoneNumber = phone;
            att.Email = email;
            Update(att);
        }

        public void Update(Attendee attendee)
        {
            Attendee a = GetAttendee(attendee.Id);

            // If changing organizations, remove attendee as admin of old organization
            if (attendee.OrganizationId != a.OrganizationId)
            {
                if (a.Organization != null)
                    if (a.Organization.AdminAttendeeId == a.Id)
                        a.Organization.AdminAttendeeId = null;
            }

            a.Address = attendee.Address;
            a.City = attendee.City;
            a.Credentials = attendee.Credentials;
            a.Email = attendee.Email;
            a.FirstName = attendee.FirstName;
            a.LastName = attendee.LastName;
            a.MiddleInitial = (string.IsNullOrWhiteSpace(attendee.MiddleInitial) ? null : attendee.MiddleInitial);
            a.OrganizationId = attendee.OrganizationId;
            a.PhoneNumber = attendee.PhoneNumber;
            a.Prefix = attendee.Prefix;
            a.StateCode = (attendee.StateCode == "" ? null : attendee.StateCode);
            a.Title = attendee.Title;
            a.Zip = attendee.Zip;
            a.Birthdate = attendee.Birthdate;
            a.LicenseExpDate = attendee.LicenseExpDate;
            a.LicenseNumber = attendee.LicenseNumber;
        }
	}
}
