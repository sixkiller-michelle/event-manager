﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.IO;

namespace EventManager.Business
{
    public class AspRegRequestAssociationEmployeeMethods : MethodBase<AspRegRequestAssociationEmployee>
    {
        public AspRegRequestAssociationEmployee GetEmployeeRequest(int id)
        {
            return this.ObjectContext.AspRegRequestAssociationEmployees.Where(t => t.Id == id).FirstOrDefault();
        }

        public AspRegRequestAssociationEmployee GetEmployeeRequestByUserId(Guid userId)
        {
            return this.ObjectContext.AspRegRequestAssociationEmployees.Where(t => t.UserId == userId).FirstOrDefault();
        }

        public List<AspRegRequestAssociationEmployee> GetUnapprovedEmployeeRequestsForExistingAssociation(int associationId)
        {
            IQueryable<AspRegRequestAssociationEmployee> query =

                  from evt in this.ObjectContext.AspRegRequestAssociationEmployees
                  where evt.AssociationId == associationId
                  orderby evt.RequestDateTime descending
                  select evt;

            return query.ToList();
        }

        public List<AspRegRequestAssociationEmployee> GetUnapprovedEmployeeRequestsForNewAssociation(int newAssociationId)
        {
            IQueryable<AspRegRequestAssociationEmployee> query =

                  from evt in this.ObjectContext.AspRegRequestAssociationEmployees
                  where evt.NewAssociationId == newAssociationId
                  orderby evt.RequestDateTime descending
                  select evt;

            return query.ToList();
        }

    }
}
