using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationMethods : MethodBase<Registration>
    {
        public List<Registration> GetRegistrationsForEvent(int eventId)
        {
            IQueryable<Registration> query =

                  from reg in this.ObjectContext.Registrations
                  where reg.EventId == eventId
                  orderby reg.Organization.OrgName
                  select reg;

            return query.ToList();
        }

        public Registration GetExistingRegistration(int eventId, Guid userId)
        { 
            // check for an existing registration for the user's facility
            AttendeeMethods am = new AttendeeMethods();
            Attendee a = am.GetAttendeeByUserId(userId);
            Registration reg = null;

            if (a.OrganizationId != null)
            { 
                reg = GetRegistrationByOrganization(eventId, (int)a.OrganizationId);
            }
            
            if (reg == null)
            {
                reg = GetRegistrationByUser(eventId, userId);
            }

            return reg;
        }

        public Registration GetRegistrationByOrganization(int eventId, int organizationId)
        {
            return this.ObjectContext.Registrations.Where(t => t.EventId == eventId && t.OrganizationId == organizationId).FirstOrDefault();
        }

        public List<Registration> GetRegistrationHistoryForOrganization(int organizationId)
        {
            IQueryable<Registration> query =

                  from reg in this.ObjectContext.Registrations
                  where reg.OrganizationId == organizationId
                  orderby reg.Event.StartDateTime
                  select reg;

            return query.ToList();
        }

        public Registration GetRegistrationByAttendee(int eventId, int attendeeId)
        {
            Registration r = null;
            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            RegistrationAttendee ra = m.GetRegistrationAttendeeForEvent(eventId, attendeeId);

            if (ra != null)
            {
                r = GetRegistration(ra.RegistrationId);
            }
           
            // Return the registration
            return r;
        }

        public List<Registration> GetActiveRegistrationsByUser(Guid UserId)
        {
            List<Registration> activeRegs = new List<Registration>();

            // Get all registrations created by the user
            IQueryable<Registration> query1 =

                  from reg in this.ObjectContext.Registrations
                  where reg.EnteredByUserId == UserId && reg.Event.StartDateTime >= DateTime.Today
                  orderby reg.Event.StartDateTime descending
                  select reg;

            activeRegs = query1.ToList();

            // Get all registrations where the user is an attendee
            //IQueryable<Registration> query2 =

            //      from reg in this.ObjectContext.RegistrationAttendees
            //      where reg.Registration.Event.StartDateTime >= DateTime.Today && reg.Attendee.UserId == UserId && reg.Registration.EnteredByUserId != UserId
            //      orderby reg.Registration.Event.StartDateTime descending
            //      select reg.Registration;

            //activeRegs.AddRange(query2.ToList());

            return activeRegs;
        }

        public Registration GetRegistrationByUser(int eventId, Guid UserId)
        {
            return this.ObjectContext.Registrations.Where(t => t.EventId == eventId && t.EnteredByUserId == UserId).FirstOrDefault();
        }

        public List<RegistrationAttendee> GetRegisteredAttendees(int registrationId)
        {
            IQueryable<RegistrationAttendee> query =

                  from reg in this.ObjectContext.RegistrationAttendees
                  where reg.RegistrationId == registrationId
                  orderby reg.Attendee.LastName, reg.Attendee.FirstName
                  select reg;

            return query.ToList();
        }

        public List<RegistrationAttendee> GetRegisteredAttendeesForEvent(int eventId)
        {
            IQueryable<RegistrationAttendee> query =

                  from reg in this.ObjectContext.RegistrationAttendees
                  where reg.Registration.EventId == eventId
                  orderby reg.Registration.Organization.OrgName, reg.Attendee.LastName, reg.Attendee.FirstName
                  select reg;

            return query.ToList();
        }

        public List<RegistrationAttendeeFlag> GetRegisteredAttendeesWithFlags(int eventId)
        {
            IQueryable<RegistrationAttendeeFlag> query =

                  from reg in this.ObjectContext.RegistrationAttendeeFlags
                  where reg.RegistrationAttendee.Registration.EventId == eventId
                  orderby reg.RegistrationAttendee.Registration.Organization.OrgName, reg.RegistrationAttendee.Attendee.LastName, reg.RegistrationAttendee.Attendee.FirstName
                  select reg;

            return query.ToList();
        }

        public Registration GetRegistration(int registrationId)
        {
            return this.ObjectContext.Registrations.Where(t => t.Id == registrationId).FirstOrDefault();
        }

        public Int32 GetSpeakerCount(int registrationId)
        {
            int count = 0;
            Registration r = GetRegistration(registrationId);
            if (r != null)
            {
                IQueryable<SessionSpeaker> query =

                  from sp in this.ObjectContext.SessionSpeakers
                  where sp.EventSession.EventId == r.EventId
                  select sp;

                List<SessionSpeaker> speakers = query.ToList();

                foreach (RegistrationAttendee att in r.RegistrationAttendees)
                {
                    if (speakers.Where(s => s.SpeakerId == att.Attendee.Id && att.IsCancelled == false).Count() > 0)
                        count++;
                }

            }
            return count;
        }

        public void AddRegistrationWithAttendee(int eventId, Organization org, Attendee att, string title, Guid userId)
        {
            EventManager.Model.Registration r = null;

            // See if the attendee is already registered
            EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
            EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendeeForEvent(eventId, att.Id);
            if (ra != null)
            {
                throw new ArgumentException("Attendee '" + att.FullName + "' is already registered for this event.");
            }

            // Make sure the org is not already registered
            if (org != null)
            {
                r = GetRegistrationByOrganization(eventId, org.Id);
                if (r == null)
                {
                    //add a new FACILITY registration
                    r = new EventManager.Model.Registration();
                    r.Organization = org;
                    r.EnteredByUserId = userId;
                    r.EntryDateTime = DateTime.Now;
                    r.EventId = eventId;
                    r.IsComplete = true;
                    r.IsWebRegistration = false;
                    r.BasePrice = 0;
                }
            }
            else // Look for individual registration
            {
                //add a new INDIVIDUAL registration
                r = new EventManager.Model.Registration();
                r.OrganizationId = null;
                r.EnteredByUserId = userId;
                r.EntryDateTime = DateTime.Now;
                r.EventId = eventId;
                r.IsComplete = true;
                r.IsWebRegistration = false;
                r.BasePrice = 0;
            }

            // Add the attendee to the registration
            ra = new EventManager.Model.RegistrationAttendee();
            ra.Attendee = att;
            ra.Price = 0;
            ra.Title = title;
            ra.Notes = "";
            ra.EntryDateTime = DateTime.Now;
            r.RegistrationAttendees.Add(ra);

            // Now update the attendee's facility/title to be the facility/title for which they just registered
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendee(att.Id);
            a.OrganizationId = r.OrganizationId;
            a.Title = title;

        } 

        public void AddRegistrationAsIndividual(int eventId, int attendeeId, string title, Guid userId)
        {
            //add the registration
            EventManager.Model.Registration r = new EventManager.Model.Registration();
            r.BasePrice = 0;
            r.EnteredByUserId = userId;
            r.EntryDateTime = DateTime.Now;
            r.EventId = eventId;
            r.IsComplete = true;
            r.IsWebRegistration = false;
            r.OrganizationId = new int?();
          

            // Add the attendee to the registration
            EventManager.Model.RegistrationAttendee ra = new EventManager.Model.RegistrationAttendee();
            ra.AttendeeId = attendeeId;
            ra.Price = 0;
            ra.Title = title;
            ra.Notes = "";
            ra.EntryDateTime = DateTime.Now;
            r.RegistrationAttendees.Add(ra);

            // Now update the attendee's facility to be the facility for which they just registered
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendee(attendeeId);
            a.OrganizationId = r.OrganizationId;

            base.Add(r);

        }

        public decimal GetCalculatedChargeTotal(int regId)
        {
            decimal ch = 0;

            List<RegistrationCharge> charges = GetCalculatedCharges(regId);
            ch = charges.Sum(s => s.ChargeAmt);
            return ch;
        }

        public decimal GetCalculatedChargeTotal_old(int regId)
        {
            decimal ch = 0;
            int eventId = 0;
            bool isMember = false;
            int? feeCategoryId = null;
            int attendeeCount = 0;
            int speakerCount = 0;
            int feePeriod = 0;

            // Get the registration record
            Registration reg = GetRegistration(regId);
            if (reg != null)
            {
                eventId = reg.EventId;
                attendeeCount = reg.RegistrationAttendees.Where(a => a.IsCancelled == false).Count();

                // Get speaker count
                RegistrationMethods rm = new RegistrationMethods();
                speakerCount = rm.GetSpeakerCount(regId);

                // Get the time period of the registration
                EventFeeScheduleMethods m1 = new EventFeeScheduleMethods();
                feePeriod = m1.GetRegistrationFeePeriod(eventId, reg.EntryDateTime);

                if (reg.OrganizationId != null)
                {
                    if (reg.Organization.OrganizationFeeCategory != null)
                        isMember = reg.Organization.OrganizationFeeCategory.IsMemberCategory;
                    feeCategoryId = reg.Organization.OrgFeeCategoryId;
                }

                RegistrationFeeTableMethods m2 = new RegistrationFeeTableMethods();
                ch = m2.GetRegistrationPrice(eventId, isMember, feeCategoryId, attendeeCount, speakerCount, feePeriod);
            }

            // Add the flags charges
            ch = ch + GetPriceForFlags(regId);
            
            return ch;
        }

        public string GetPricingCalculation(int regId)
        {
            string calcString = "";
            int eventId = 0;
            bool isMember = false;
            int? feeCategoryId = null;
            int attendeeCount = 0;
            int feePeriod = 0;

            // Get the registration record
            Registration reg = GetRegistration(regId);
            if (reg != null)
            {
                eventId = reg.EventId;
                attendeeCount = reg.RegistrationAttendees.Count();

                // Get the time period of the registration
                EventFeeScheduleMethods m1 = new EventFeeScheduleMethods();
                feePeriod = m1.GetRegistrationFeePeriod(reg.Id, reg.EntryDateTime);

                if (reg.OrganizationId != null)
                {
                    if (reg.Organization.OrganizationFeeCategory != null)
                        isMember = reg.Organization.OrganizationFeeCategory.IsMemberCategory;
                    feeCategoryId = reg.Organization.OrgFeeCategoryId;
                }

                RegistrationFeeTableMethods m2 = new RegistrationFeeTableMethods();
                calcString = m2.GetPricingCalculation(eventId, isMember, feeCategoryId, attendeeCount, feePeriod);
            }

            string flagCalc = GetFlagPricingCalculation(regId);
            if (!string.IsNullOrEmpty(flagCalc.Trim()))
            {
                calcString = calcString + " " + flagCalc;
            }

            return calcString;
        }

        public List<RegistrationCharge> GetCalculatedCharges(int regId)
        {
            int eventId = 0;
            bool isMember = false;
            int? feeCategoryId = null;
            int attendeeCount = 0;
            int feePeriod = 0;
            List<RegistrationCharge> charges = new List<RegistrationCharge>();

            // Get the registration charges
            Registration reg = GetRegistration(regId);
            if (reg != null)
            {
                eventId = reg.EventId;
                attendeeCount = reg.RegistrationAttendees.Where(a => a.IsCancelled == false).Count();

                // Get the time period of the registration
                EventFeeScheduleMethods m1 = new EventFeeScheduleMethods();
                feePeriod = m1.GetRegistrationFeePeriod(eventId, reg.EntryDateTime);

                if (reg.OrganizationId != null)
                {
                    if (reg.Organization.OrganizationFeeCategory != null)
                        isMember = reg.Organization.OrganizationFeeCategory.IsMemberCategory;
                    feeCategoryId = reg.Organization.OrgFeeCategoryId;
                }

                RegistrationFeeTableMethods m2 = new RegistrationFeeTableMethods();
                charges = m2.GetCalculatedRegCharges(regId, reg.EntryDateTime, eventId, isMember, feeCategoryId, attendeeCount, feePeriod);
            }

            // Add the flags charges
            List<RegistrationCharge> flagCharges = GetFlagCharges(regId);
            charges.AddRange(flagCharges);

            return charges;
        }

        public string GetFlagPricingCalculation(int regId)
        {
            //Registration r = GetRegistration(regId);
            //int attendeeFlagCount = 0;
            string flagCalc = "";

            Registration reg = GetRegistration(regId);
            if (reg != null)
            {
                IQueryable<EventFlag> query =

                      from flags in this.ObjectContext.EventFlags
                      where flags.EventId == reg.EventId && flags.Price > 0
                      select flags;

                foreach (EventFlag f in query)
                {
                    IQueryable<RegistrationAttendeeFlag> query2 =

                      from attFlags in this.ObjectContext.RegistrationAttendeeFlags
                      where attFlags.RegistrationAttendee.RegistrationId == regId && attFlags.EventFlagId == f.Id
                      select attFlags;

                    if (query2.Count() > 0)
                        flagCalc = flagCalc + f.FlagCode + " (" + query2.Count().ToString() + " @ " + f.Price.ToString() + ") ";
                }
            }

            return flagCalc;
        }

        public List<RegistrationCharge> GetFlagCharges(int regId)
        {
            List<RegistrationCharge> charges = new List<RegistrationCharge>();

            Registration reg = GetRegistration(regId);
            if (reg != null)
            {
                // Get the time period of the registration
                int period;
                EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                period = m.GetRegistrationFeePeriod(reg.EventId, reg.EntryDateTime);

                IQueryable<EventFlag> query =

                      from flags in this.ObjectContext.EventFlags
                      where flags.EventId == reg.EventId && (flags.Period1Price > 0 || flags.Period2Price > 0 || flags.Period3Price > 0)
                      select flags;

                
                foreach (EventFlag f in query)
                {
                    IQueryable<RegistrationAttendeeFlag> query2 =

                      from attFlags in this.ObjectContext.RegistrationAttendeeFlags
                      where attFlags.RegistrationAttendee.RegistrationId == regId && attFlags.RegistrationAttendee.IsCancelled == false && attFlags.EventFlagId == f.Id
                      select attFlags;

                    if (query2.Count() > 0)
                    {
                        decimal flagPrice = (period == 1 ? f.Period1Price : (period == 2 ? f.Period2Price : f.Period3Price));

                        RegistrationCharge ch = new RegistrationCharge();
                        ch.ChargeAmt = query2.Count() * flagPrice;
                        ch.ChargeDate = reg.EntryDateTime;
                        ch.ChargeDesc = f.FlagCode + ": " + query2.Count().ToString() + " @ " + flagPrice.ToString();
                        ch.RegistrationId = regId;
                        charges.Add(ch);
                    }
                }
            }

            return charges;
        }

        public int GetCancelledAttendeeCount(int regId)
        {
            Registration r = GetRegistration(regId);
            return r.RegistrationAttendees.Where(a => a.IsCancelled == true).Count();
        }

        public decimal GetChargeTotal(int regId)
        {
            Registration r = GetRegistration(regId);
            return r.RegistrationCharges.Sum(ch => ch.ChargeAmt);
        }

        public decimal GetPaymentTotal(int regId)
        {
            Registration r = GetRegistration(regId);
            return r.RegistrationPayments.Sum(ch => ch.PaymentAmt);
        }

        public decimal GetBalance(int eventId, int attendeeId)
        {
            Registration r = GetRegistrationByAttendee(eventId, attendeeId);
            return GetBalance(r.Id);
        }

        public decimal GetBalance(int regId)
        {
            decimal balance = 0;

            balance = (GetCalculatedChargeTotal(regId) + GetChargeTotal(regId)) - GetPaymentTotal(regId);
            return balance;
        }

        //public decimal GetPriceForGroup(int regId)
        //{
        //    decimal basePrice = 0;
        //    RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
        //    basePrice = m.GetRegistrationBasePrice(regId);
        //    return basePrice;
        //}

        //public decimal GetPriceForAttendees(int regId)
        //{ 
        //    // Get max number of attendees for the event registration
        //    int maxAttendees = 15;
        //    int currentAttendees = 0;
        //    decimal extraAttendeePrice = 0;

        //    // Find out how many are currently registered under the facility
        //    Registration r = GetRegistration(regId);
        //    currentAttendees = r.RegistrationAttendees.Count();
            
        //    // Get the price per extra attendee
        //    RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
        //    extraAttendeePrice = m.GetRegistrationExtraAttendeePrice(regId);

        //    if (currentAttendees > maxAttendees)
        //    {
        //        return (currentAttendees - maxAttendees) * extraAttendeePrice;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}

        public decimal GetPriceForFlags(int regId)
        {
            //Registration r = GetRegistration(regId);
            //int attendeeFlagCount = 0;
            decimal flagPrice = 0;
            int period;

            EventFeeScheduleMethods m = new EventFeeScheduleMethods();

            IQueryable<RegistrationAttendeeFlag> query =

                  from reg in this.ObjectContext.RegistrationAttendeeFlags
                  where reg.RegistrationAttendee.Registration.Id == regId && (reg.EventFlag.Period1Price > 0 || reg.EventFlag.Period2Price > 0 || reg.EventFlag.Period3Price > 0)
                  select reg;
            
            foreach (RegistrationAttendeeFlag af in query)
            {
                // Find the period in which the registraton occurred
                period = m.GetRegistrationFeePeriod(af.RegistrationAttendee.Registration.EventId, af.RegistrationAttendee.EntryDateTime);
                flagPrice = flagPrice + (period == 1 ? af.EventFlag.Period1Price : (period == 2 ? af.EventFlag.Period2Price : af.EventFlag.Period3Price));
            }

            // Get flag (add-on) prices 
            //foreach (EventFlag f in r.Event.EventFlags)
            //{
            //    attendeeFlagCount = 0;
            //    if (f.Price != 0)
            //    {
            //        foreach (RegistrationAttendee ra in r.RegistrationAttendees)
            //        {
            //            if (ra.HasFlag(f.Id))
            //                attendeeFlagCount = attendeeFlagCount + 1;
            //        }
            //    }
            //    flagPrice = flagPrice + (attendeeFlagCount * f.Price);
            //}
            return flagPrice;
        }

        public Registration AddRegistration(Registration reg)
        {
            // Make sure the org is not already registered

            base.Add(reg);
            SaveAllObjectChanges();

            return reg;
        }

        public void Update(Registration reg)
        {
            Registration r = GetRegistration(reg.Id);

            // If changing the organization, make sure one doesn't already exist
            if (reg.OrganizationId != r.OrganizationId)
            { 
                Registration existingReg = GetRegistrationByOrganization(r.EventId, Convert.ToInt32(reg.OrganizationId));
                if (existingReg != null)
                {
                    // "Move" all attendees to new facility registration
                    foreach (RegistrationAttendee ra in GetRegisteredAttendees(reg.Id))
                    {
                        ra.RegistrationId = existingReg.Id;

                        // Now update the attendee's facility to be the facility to which they've just been moved
                        EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                        EventManager.Model.Attendee a = am.GetAttendee(ra.AttendeeId);
                        a.OrganizationId = reg.OrganizationId;
                    }
                }
                else
                { 
                    r.OrganizationId = reg.OrganizationId;
                }
            }

            r.BasePrice = reg.BasePrice;
            r.BoothNumber = reg.BoothNumber;
            r.BoothNumberRequest = reg.BoothNumberRequest;
            r.DoorPrize = reg.DoorPrize;
            r.IsComplete = reg.IsComplete;
            r.Status = reg.Status;
            r.Notes = reg.Notes;
          
        }
    }
}
