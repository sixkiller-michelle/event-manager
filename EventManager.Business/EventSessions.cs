﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.Web.Services;

namespace EventManager.Business
{
    public class EventSessionMethods : MethodBase<EventSession>
    {

        public List<EventSession> GetSessionsByEvent(int eventId)
        {
            IQueryable<EventSession> query =

                  from evt in this.ObjectContext.EventSessions
                  where evt.EventId == eventId
                  orderby evt.StartDateTime, evt.EndDateTime 
                  select evt;

            return query.ToList();
        }

        public EventSession GetSession(int id)
        {
            return this.ObjectContext.EventSessions.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<RegistrationAttendee> GetSpeakerRegistrations(int sessionId)
        {
            IQueryable<RegistrationAttendee> query =

                  from s in this.ObjectContext.SessionAttendances
                  where s.SessionId == sessionId && s.IsSpeaker == true
                  orderby s.RegistrationAttendee.Attendee.LastName, s.RegistrationAttendee.Attendee.FirstName
                  select s.RegistrationAttendee;

            return query.ToList();
        }


        public void Update(EventSession session)
        {
            EventSession s = GetSession(session.Id);
            s.Audience = session.Audience;
            s.CEUHours = session.CEUHours;
            s.EndDateTime = session.EndDateTime;
            s.EndDateTimeAlt = session.EndDateTimeAlt;
            s.Location = session.Location;
            s.RequireOneScan = session.RequireOneScan;
            s.Scanner = session.Scanner;
            s.SessionHost = session.SessionHost;
            s.SessionName = session.SessionName;
            s.StartDateTime = session.StartDateTime;
        }

        public void Delete(int sessionId)
        {
            EventSession s = GetSession(sessionId);
            base.Delete(s);
        }

        [WebMethod]
        public string GetScannerFileText(int eventId)
        {
            using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
            {
                List<EventSession> sessions = GetSessionsByEvent(eventId);
                string fileText = "";
                foreach (EventSession s in sessions)
                {
                    fileText += s.Id.ToString() + "," + s.SessionName.Substring(0, 13) + "...," + ((DateTime)s.StartDateTime).DayOfWeek.ToString().Substring(0, 3) + "," + ((DateTime)s.StartDateTime).ToShortTimeString() + "\r\n";
                }
                return fileText;
            }
            
        }

    }
}
