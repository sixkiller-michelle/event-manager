﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class ConfigMethods : MethodBase<Config>
    {
        public Config GetConfigSetting(int assnId, string propertyName)
        {
            return this.ObjectContext.Configs.Where(t => t.AssociationId == assnId && t.PropertyName == propertyName).FirstOrDefault();
        }
    }
}
