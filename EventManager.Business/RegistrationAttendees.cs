﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class RegistrationAttendeesMethods : MethodBase<RegistrationAttendee>
    {
        public RegistrationAttendee GetRegistrationAttendee(int id)
        {
            return this.ObjectContext.RegistrationAttendees.Where(t => t.Id == id).FirstOrDefault();
        }

        public RegistrationAttendee GetRegistrationAttendeeForEvent(int eventId, int attendeeId)
        {
            return this.ObjectContext.RegistrationAttendees.Where(t => t.Registration.EventId == eventId && t.AttendeeId == attendeeId && t.IsCancelled == false).FirstOrDefault();
        }

        public List<RegistrationAttendee> GetRegisteredAttendees(int registrationId, bool includeCancelled = false)
        {
            IQueryable<RegistrationAttendee> query =

                  from reg in this.ObjectContext.RegistrationAttendees
                  where reg.RegistrationId == registrationId && (reg.IsCancelled == false || reg.IsCancelled == includeCancelled)
                  orderby reg.Attendee.LastName, reg.Attendee.FirstName
                  select reg;

            return query.ToList();
        }

        public List<RegistrationAttendee> GetRegistrationsByAttendee(int attendeeId)
        {
            IQueryable<RegistrationAttendee> query =

                  from reg in this.ObjectContext.RegistrationAttendees
                  where reg.AttendeeId == attendeeId && reg.IsCancelled == false
                  orderby reg.Registration.Event.StartDateTime descending
                  select reg;

            return query.ToList();
        }

        public List<RegistrationAttendee> GetRegisteredAttendeesForEvent(int eventId)
        {
            try
            {
                IQueryable<RegistrationAttendee> query =

                  from reg in this.ObjectContext.RegistrationAttendees.Include("Attendee").Include("Registration.Organization")
                  where reg.Registration.EventId == eventId && reg.IsCancelled == false
                  orderby reg.Registration.Organization.OrgName, reg.Attendee.LastName, reg.Attendee.FirstName
                  select reg;

                return query.ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error returning data for registered facilities: 'GetRegisteredAttendeesForEvent'", ex.InnerException);
            }
        }

        public List<RptAttendanceHistory_Result> GetAttendanceHistory(int attendeeId)
        {
            return this.ObjectContext.RptAttendanceHistory(attendeeId).ToList();
        }

        public List<RegAttendeeRow> GetRegisteredAttendeesForGrid(int eventId, bool showOnsiteOnly = false)
        {
            try
            {
                //List<RegAttendeeRow> attendees = new List<RegAttendeeRow>();
                //IQueryable<RegistrationAttendee> query;

                //if (!showOnsiteOnly)
                //    query = from reg in this.ObjectContext.RegistrationAttendees.Include("Attendee").Include("Registration")
                //            where reg.Registration.EventId == eventId && reg.IsCancelled == false
                //            orderby reg.Registration.Organization.OrgName, reg.Attendee.LastName, reg.Attendee.FirstName
                //            select reg;
                //else
                //{
                //    //Event evt = this.ObjectContext.Events.Where(e => e.Id == eventId).FirstOrDefault();
                //    DateTime eventStartOfDay = this.ObjectContext.Events.Where(e => e.Id == eventId).FirstOrDefault().StartDateTime.AddHours(-4);

                //    query = from reg in this.ObjectContext.RegistrationAttendees.Include("Attendee").Include("Registration")
                //            where reg.Registration.EventId == eventId && reg.IsCancelled == false && reg.EntryDateTime >= eventStartOfDay
                //            orderby reg.Registration.Organization.OrgName, reg.Attendee.LastName, reg.Attendee.FirstName
                //            select reg;
                //}
                    

                //foreach (RegistrationAttendee regAttendee in query.ToList())
                //{ 
                //    RegAttendeeRow r = new RegAttendeeRow();
                //    r.AttendeeFullName = regAttendee.Attendee.FullName;
                //    r.AttendeeId = regAttendee.AttendeeId;
                //    r.CeuTotal = regAttendee.CeuTotal;
                //    r.EntryDateTime = regAttendee.EntryDateTime;
                //    r.FlagList = regAttendee.FlagList;
                //    r.Id = regAttendee.Id;
                //    r.IsSpeaker = regAttendee.IsSpeaker;
                //    r.Notes = (regAttendee.Notes == null ? "" : regAttendee.Notes);
                //    r.OrgId = regAttendee.Registration.OrganizationId;
                //    r.OrgName = (r.OrgId == null ? "" : regAttendee.Registration.Organization.OrgName);
                //    r.Title = (regAttendee.Title == null ? "" : regAttendee.Title);
                //    attendees.Add(r);
                //}
                //return attendees;

                return this.ObjectContext.SelRegistrationAttendee(eventId, showOnsiteOnly).ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error returning data for registered facilities: 'GetRegisteredAttendeesForGrid'", ex.InnerException);
            }
        }

        public RegAttendeeRow GetRegisteredAttendeeForGrid(int regAttendeeId)
        {
            try
            {
                return this.ObjectContext.SelRegistrationAttendeeById(regAttendeeId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error returning data for registered facilities: 'GetRegisteredAttendeesForGrid'", ex.InnerException);
            }
        }

        public List<RegistrationAttendeeFlag> GetRegisteredAttendeesWithFlags(int eventId)
        {
            try
            {
                IQueryable<RegistrationAttendeeFlag> query =

                    from reg in this.ObjectContext.RegistrationAttendeeFlags
                    where reg.RegistrationAttendee.Registration.EventId == eventId && reg.RegistrationAttendee.IsCancelled == false
                    orderby reg.RegistrationAttendee.Registration.Organization.OrgName, reg.RegistrationAttendee.Attendee.LastName, reg.RegistrationAttendee.Attendee.FirstName
                    select reg;

                return query.ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error returning data for registered facilities.", ex.InnerException);
            }
            
        }

        public Decimal GetAttendeePrice(Registration reg)
        {
            Decimal basePrice = 0;

            //

            // Get Fee category ID
            //int orgFeeCategoryId;
            //EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
            //Organization o = om.GetOrganization(registrationAttendee);
            //orgFeeCategoryId = o.OrganizationFeeCategory.Id;

            //// Get fee period we're currently in for the event
            //int feePeriod = 0;
            //EventManager.Business.EventFeeScheduleMethods fsm = new EventManager.Business.EventFeeScheduleMethods();
            //feePeriod = fsm.GetRegistrationFeePeriod(eventId);

            //try
            //{
            //    if (feePeriod == 1)
            //        basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period1GroupPrice;
            //    else if (feePeriod == 2)
            //        basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period2GroupPrice;
            //    else if (feePeriod == 3)
            //        basePrice = this.ObjectContext.RegistrationFeeTables.Where(r => r.EventId == eventId && r.FeeCategoryId == orgFeeCategoryId).FirstOrDefault().Period3GroupPrice;
            //}
            //catch (Exception ex)
            //{
            //    basePrice = 0;
            //}

            return basePrice;
        }

        public override void Add(RegistrationAttendee newObject)
        {
            // get the event Id of the new attendee registration
            EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
            Registration r = rm.GetRegistration(newObject.RegistrationId);
            int eventId = r.EventId;

            // Check if person is already registered  for event
            RegistrationAttendee a = GetRegistrationAttendeeForEvent(eventId, newObject.AttendeeId);
            if (a != null)
            {
                throw new ArgumentException("Attendee '" + a.Attendee.FirstName + " " + a.Attendee.LastName + "' is already registered for this event.");
            }
            newObject.EntryDateTime = DateTime.Now;
            base.Add(newObject);
        }

        public void Add(int eventId, RegistrationAttendee newObject)
        {
            // Check if person is already registered  for event
            RegistrationAttendee a = GetRegistrationAttendeeForEvent(eventId, newObject.AttendeeId);
            if (a != null)
            {
                throw new ArgumentException("Attendee '" + a.Attendee.FirstName + " " + a.Attendee.LastName + "' is already registered for this event.");
            }
            newObject.EntryDateTime = DateTime.Now;
 	        base.Add(newObject);
        }

        public void UpdateFacility(int registrationAttendeeId, int? newOrgId, Guid userId)
        {
            RegistrationAttendee attendee = GetRegistrationAttendee(registrationAttendeeId);

            if (newOrgId != attendee.Registration.OrganizationId)
            {
                EventManager.Business.RegistrationMethods m = new EventManager.Business.RegistrationMethods();
                // Store the old registration
                Registration oldReg = m.GetRegistration(attendee.RegistrationId);

                if (newOrgId != null)
                {
                    // Get registration for new facility, if it exists
                    Registration existingReg = m.GetRegistrationByOrganization(attendee.Registration.EventId, Convert.ToInt32(newOrgId));

                    if (existingReg != null)
                    {
                        // Move this attendee to the existing registration (and then update their profile facility to be the facility under which they're registered)
                        attendee.RegistrationId = existingReg.Id;
                        attendee.Attendee.OrganizationId = existingReg.OrganizationId;
                    }
                    else // No existing registration for facility
                    {
                        // Create a new registration for the facility (copying over all fields from old facility registration)
                        EventManager.Model.Registration r = new EventManager.Model.Registration();
                        r.BasePrice = attendee.Registration.BasePrice;
                        r.BoothNumber = attendee.Registration.BoothNumber;
                        r.BoothNumberRequest = attendee.Registration.BoothNumberRequest;
                        r.DoorPrize = attendee.Registration.DoorPrize;
                        r.EnteredByUserId = userId;
                        r.EntryDateTime = oldReg.EntryDateTime;
                        r.EventId = attendee.Registration.EventId;
                        r.IsComplete = true;
                        r.IsWebRegistration = false;
                        r.Notes = attendee.Registration.Notes;
                        r.OrganizationId = newOrgId;
                        m.AddRegistration(r);
                        attendee.Attendee.OrganizationId = newOrgId; // Update facility on attendee's profile as well
                        attendee.Registration = r;
                    }
                }
                else
                {
                    //m.AddRegistrationAsIndividual(attendee.Registration.EventId, attendee.AttendeeId, attendee.Title, userId);

                    // Create a new registration for the facility (copying over all fields from old facility registration)
                    EventManager.Model.Registration r = new EventManager.Model.Registration();
                    r.BasePrice = attendee.Registration.BasePrice;
                    r.BoothNumber = attendee.Registration.BoothNumber;
                    r.BoothNumberRequest = attendee.Registration.BoothNumberRequest;
                    r.DoorPrize = attendee.Registration.DoorPrize;
                    r.EnteredByUserId = userId;
                    r.EntryDateTime = oldReg.EntryDateTime;
                    r.EventId = attendee.Registration.EventId;
                    r.IsComplete = true;
               
                    r.IsWebRegistration = false;
                    r.Notes = attendee.Registration.Notes;
                    r.OrganizationId = null;
                  
                    m.AddRegistration(r);
                    attendee.Registration = r;
                }

                if (oldReg.RegistrationAttendees.Count == 0)
                {
                    m.Delete(oldReg);
                }
            }
        }

        public void CancelRegistration(int regAttendeeId)
        {
            RegistrationAttendee r = GetRegistrationAttendee(regAttendeeId);
            r.IsCancelled = true;
        }

        public void Update(RegistrationAttendee attendee)
        {
            //attendee.At
            //RegistrationAttendee newAttendee = GetRegistrationAttendee(
            RegistrationAttendee r = GetRegistrationAttendee(attendee.Id);
            r.Price = attendee.Price;
            r.Notes = attendee.Notes;
            r.Title = attendee.Title;
            r.IsCancelled = attendee.IsCancelled;

            // If changing title, change on the attendee's profile as well
            if (r.Attendee.Title != attendee.Title)
            {
                r.Attendee.Title = attendee.Title; 
            }
        }

    }
}
