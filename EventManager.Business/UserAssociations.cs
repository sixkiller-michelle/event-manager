﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;

namespace EventManager.Business
{
    public class UserAssociationMethods : MethodBase<UserAssociation>
    {

        public int GetAssociationIdForUser(Guid userId)
        {
            UserAssociation u = this.ObjectContext.UserAssociations.Where(t => t.UserId == userId).FirstOrDefault();
            return Convert.ToInt32(u.AssociationId);
        }

        public Association GetAssociationForUser(Guid userId)
        {
            if (this.ObjectContext.UserAssociations.Where(t => t.UserId == userId).Count() > 0)
            {
                UserAssociation u = this.ObjectContext.UserAssociations.Where(t => t.UserId == userId).First();
                return u.Association;
            }
            else
            {
                return null;
            }
        }

        public List<UserAssociation> GetAllUsers()
        {
            return this.ObjectContext.UserAssociations.ToList();
        }

        public List<UserAssociation> GetUsersByAssociation(int assocId)
        {
            return this.ObjectContext.UserAssociations.Where(a => a.AssociationId == assocId).ToList();
        }

        public List<UserAssociation> GetAllAssociationUsers()
        {
            IQueryable<UserAssociation> query =

                  from usr in this.ObjectContext.UserAssociations
                  join emp in this.ObjectContext.AssociationEmployees on usr.UserId equals emp.UserId
                  orderby emp.Association.AssociationName, emp.LastName, emp.FirstName
                  select usr;

            return query.ToList();

        }

        public List<UserAssociation> GetAllFacilityUsers()
        {
            IQueryable<UserAssociation> query =

                  from usr in this.ObjectContext.UserAssociations
                  join att in this.ObjectContext.Attendees on usr.UserId equals att.UserId
                  orderby att.Association.AssociationName, att.Organization.OrgName, att.LastName, att.FirstName
                  select usr;

            return query.ToList();
        }


        public List<UserAssociation> GetAssociationUsersByAssociation(int assocId)
        {
            IQueryable<UserAssociation> query =

                  from usr in this.ObjectContext.UserAssociations
                  join emp in this.ObjectContext.AssociationEmployees on usr.UserId equals emp.UserId
                  where usr.AssociationId == assocId
                  orderby emp.LastName, emp.FirstName
                  select usr;

            return query.ToList();

        }

        public List<UserAssociation> GetFacilityUsersByAssociation(int assocId)
        {
            IQueryable<UserAssociation> query =

                  from usr in this.ObjectContext.UserAssociations
                  join att in this.ObjectContext.Attendees on usr.UserId equals att.UserId
                  where att.AssociationId == assocId
                  orderby att.Organization.OrgName, att.LastName, att.FirstName
                  select usr;

            return query.ToList();
        }

        public List<UserAssociation> GetFacilityUsersByOrganization(int orgId)
        {
            IQueryable<UserAssociation> query =

                  from usr in this.ObjectContext.UserAssociations
                  join att in this.ObjectContext.Attendees on usr.UserId equals att.UserId
                  where att.OrganizationId == orgId
                  orderby att.LastName, att.FirstName
                  select usr;

            return query.ToList();
        }

        

  
    }
}
