﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Business
{
    public class AttendeeEventSurveyMethods : MethodBase<AttendeeEventSurvey>
    {
        public AttendeeEventSurvey GetEventSurveyByAttendee(int eventId, int attendeeId)
        {
            return this.ObjectContext.AttendeeEventSurveys.Where(t => t.EventId == eventId && t.AttendeeId == attendeeId).FirstOrDefault();   
        }

        public List<AttendeeEventSurvey> GetEventSurveysByAttendee(int attendeeId)
        {
            IQueryable<AttendeeEventSurvey> query =

                    from att in this.ObjectContext.AttendeeEventSurveys
                    where att.AttendeeId == attendeeId
                    select att;

            return query.ToList();
        }

    }
}
