﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using EventManager.Business;
using System.Web.Security;

namespace EventManager.Business
{
    public class AssociationEmployeeMethods : MethodBase<AssociationEmployee>
    {
        public AssociationEmployee GetEmployeeByUserId(Guid userId)
        {
            try
            {
                return this.ObjectContext.AssociationEmployees.Where(t => t.UserId == userId).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public AssociationEmployee GetEmployee(int id)
        {
            return this.ObjectContext.AssociationEmployees.Where(t => t.Id == id).FirstOrDefault();
        }

        public List<AssociationEmployee> GetEmployees(int associationId)
        {
            IQueryable<AssociationEmployee> query =
                    from att in this.ObjectContext.AssociationEmployees
                    where att.AssociationId == associationId
                    orderby att.LastName, att.FirstName
                    select att;

            return query.ToList();
        }

        public List<AssociationEmployee> GetEmployeesWithoutAccounts(int associationId)
        {
            IQueryable<AssociationEmployee> query =
                    from att in this.ObjectContext.AssociationEmployees
                    from u in this.ObjectContext.aspnet_Users
                    where att.AssociationId == associationId && att.UserId != u.UserId
                    orderby att.LastName, att.FirstName
                    select att;

            return query.ToList();
        }

        public List<AssociationEmployee> GetEmployeesByAssociationAndRole(int associationId, string roleName)
        {
            List<AssociationEmployee> empsInRole = new List<AssociationEmployee>();

            IQueryable<AssociationEmployee> employees = from employee in this.ObjectContext.AssociationEmployees
                                                        where employee.AssociationId == associationId && employee.IsActive == true && employee.UserId != null
                                             select employee;

            foreach (AssociationEmployee e in employees)
            {
                string userName = Membership.GetUser(e.UserId).UserName;
                if (Roles.IsUserInRole(userName, roleName))
                {
                    empsInRole.Add(e);
                }
            }
            return empsInRole;
        }

    }
}
