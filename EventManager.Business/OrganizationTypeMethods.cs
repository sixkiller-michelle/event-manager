﻿using EventManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace EventManager.Business
{
    public class OrganizationTypeMethods : MethodBase<OrganizationType>
    {
        public OrganizationType getOrganizationType(int id)
        {
            if (this.ObjectContext.OrganizationTypes.Where(e => e.Id == id).Count() > 0)
                return this.ObjectContext.OrganizationTypes.Where(e => e.Id == id).FirstOrDefault();
            else
                return null;
        }

        public List<OrganizationType> getOrganizationTypeList(int assnId)
        {
            IQueryable<OrganizationType> query = from org in this.ObjectContext.OrganizationTypes
                                                 where org.AssociationId == assnId
                                                    select org;

            return query.ToList();
        }

        public override void Add(OrganizationType newObject)
        {

            base.Add(newObject);
        }

        public override void Delete(OrganizationType obsoleteObject)
        {
            base.Delete(obsoleteObject);
        }

        public void Update(OrganizationType original, OrganizationType modified)
        {
 
            try
            {
                //build entitykey from incoming contactID so that you can Attach
                var ekey = new EntityKey("Entites.OrganizationTypes", "Id", original.Id);
                original.EntityKey = ekey;

                this.ObjectContext.Attach(original);
                this.ObjectContext.ApplyCurrentValues("OrganizationTypes", modified);

                this.SaveAllObjectChanges();
            }
            catch (Exception ex)
            {
                //insert proper exception handling here...
                throw ex;
            }
        }

    }
}
