﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Data;
using EventApp.Common;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Business
{
    public class AttendeeTitleMethods : MethodBase<AttendeeTitle>
	{
        public AttendeeTitle GetTitle(int id)
		{
            return this.ObjectContext.AttendeeTitles.Where(t => t.Id == id).FirstOrDefault();
		}

        public List<AttendeeTitle> GetTitles(int associationId)
        {
            IQueryable<AttendeeTitle> query =

                    from att in this.ObjectContext.AttendeeTitles
                    where att.AssociationId == associationId
                    orderby att.Title
                    select att;

            return query.ToList();
        }

        public AttendeeTitle AddTitle(string title, int assnId)
        {
            AttendeeTitle a = new AttendeeTitle();
            a.Title = title;
            a.AssociationId = assnId;
            base.Add(a);
            SaveAllObjectChanges();

            return a;
        }

        public void Update(AttendeeTitle attendeeTitle)
        {
            AttendeeTitle att = GetTitle(attendeeTitle.Id);
            att.Title = attendeeTitle.Title;
        }

	}
}
