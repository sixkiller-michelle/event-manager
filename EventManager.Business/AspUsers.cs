﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.IO;
using System.Web.Security;

namespace EventManager.Business
{
    public class AspUserMethods : MethodBase<aspnet_Membership>
    {
        private List<MembershipUser> CreateMembershipUserList(List<UserAssociation> users)
        {
            List<MembershipUser> mUsers = new List<MembershipUser>();
            foreach (UserAssociation n in users)
            {
                MembershipUser u = Membership.GetUser(n.UserId);
                if (u != null)
                    mUsers.Add(Membership.GetUser(n.UserId));
            }

            return mUsers;
        }

        public List<MembershipUser> GetAllUsers()
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetAllUsers();
            return CreateMembershipUserList(users);
        }

        public List<MembershipUser> GetAllAssociationUsers()
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetAllAssociationUsers();
            return CreateMembershipUserList(users);
        }

        public List<MembershipUser> GetAllFacilityUsers()
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetAllFacilityUsers();
            return CreateMembershipUserList(users);
        }

        public List<MembershipUser> GetAssociationUsersByAssociation(int assocId)
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetAssociationUsersByAssociation(assocId);
            return CreateMembershipUserList(users);
        }

        public List<MembershipUser> GetFacilityUsersByAssociation(int assnId)
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetFacilityUsersByAssociation(assnId);
            return CreateMembershipUserList(users);
        }

        public List<MembershipUser> GetFacilityUsersByOrganization(int orgId)
        {
            UserAssociationMethods m = new UserAssociationMethods();
            List<UserAssociation> users = m.GetFacilityUsersByOrganization(orgId);
            return CreateMembershipUserList(users);
        }

    }
}
