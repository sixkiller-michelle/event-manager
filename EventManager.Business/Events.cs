﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Data;
using EventApp.Common;
using EventManager.Model;
using EventManager.Business;
using System.Web.Security;

namespace EventManager.Business
{
    public class EventMethods : MethodBase<Event>
    {
        public List<Event> GetEventList(int associationId)
        {
            List<Event> events = new List<Event>();

            IQueryable<Event> query =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == associationId
                  orderby evt.StartDateTime descending
                  select evt;
            events = query.ToList();

            return events.ToList();
        }

        public List<Event> GetEventList(int associationId, Guid userId)
        {
            List<Event> events = new List<Event>();

            IQueryable<Event> publicEventsQuery =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == associationId && evt.Visibility == "Public"
                  select evt;
            events = publicEventsQuery.ToList();

            MembershipUser user = Membership.GetUser(userId);
            if (user != null)
            {
                if (Roles.IsUserInRole(user.UserName, "AssociationAdmin") || Roles.IsUserInRole(user.UserName, "AssociationEmployee") || Roles.IsUserInRole(user.UserName, "Developer"))
                {
                    IQueryable<Event> assnEventsQuery =

                         from evt in this.ObjectContext.Events
                         where evt.AssociationId == associationId && evt.Visibility == "Association" 
                         select evt;
                    events.AddRange(assnEventsQuery.ToList());

                    AssociationEmployeeMethods m = new AssociationEmployeeMethods();
                    AssociationEmployee emp = m.GetEmployeeByUserId(userId);
                    if (emp != null)
                    {
                        IQueryable<Event> creatorEventsQuery =

                               from evt in this.ObjectContext.Events
                               where evt.AssociationId == associationId && evt.Visibility == "Creator" && evt.CreatedByEmployeeId == emp.Id
                               select evt;
                        events.AddRange(creatorEventsQuery.ToList());
                    }
                }
            }

            return events.ToList().OrderByDescending(s => s.StartDateTime).ToList();
        }

        public List<Event> GetUpcomingEvents(int associationId, Guid userId)
        {
            DateTime firstDate = DateTime.Today;
            List<Event> events = GetEventList(associationId, userId);

            events = events.Where(evt => evt.StartDateTime >= firstDate && (evt.EventStatu.StatusDesc == "Open" || evt.EventStatu.StatusDesc == "Future"))
                .OrderByDescending(s => s.StartDateTime)
                .ToList();

            return events;
        }

        public List<Event> GetEventsByMonth(int associationId, DateTime monthStart, Guid userId)
        {
            DateTime monthBegin = new DateTime(monthStart.Year, monthStart.Month, 1);
            DateTime monthEnd = monthBegin.AddMonths(1);

            List<Event> events = GetEventList(associationId, userId);

            events = events.Where(evt => evt.StartDateTime >= monthBegin && evt.StartDateTime < monthEnd)
                .OrderByDescending(s => s.StartDateTime)
                .ToList();

            return events;
        }

        public List<Event> GetEventsByCertificateAvailability(int associationId)
        {
            IQueryable<Event> query =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == associationId && evt.CertificatesAvailableStartDate <= DateTime.Now && evt.CertificatesAvailableEndDate >= DateTime.Now
                  orderby evt.StartDateTime descending
                  select evt;

            return query.ToList();
        }

        public Event GetCurrentConventionEvent(int associationId)
        {
            DateTime lowDate = DateTime.Today.AddMonths(-4);
            DateTime highDate = DateTime.Today.AddMonths(4);
            List<Event> pastEvents = this.ObjectContext.Events.Where(e => e.AssociationId == associationId && e.StartDateTime > lowDate && e.StartDateTime < highDate && e.EventType.TypeName == "Convention").OrderBy(e => e.StartDateTime).ToList();
            if (pastEvents.Count > 0)
            {
                return pastEvents[0];
            }
            else
            {
                return null;
            }
        }

        public Event GetMostRecentEvent(int associationId)
        {
            List<Event> pastEvents = this.ObjectContext.Events.Where(e => e.AssociationId == associationId && e.StartDateTime < DateTime.Today).OrderByDescending(e => e.StartDateTime).ToList();
            if (pastEvents.Count > 0)
            {
                return pastEvents[0];
            }
            else
            {
                return null;
            }
        }

        public Event GetNextEvent(int associationId)
        {
            List<Event> pastEvents = this.ObjectContext.Events.Where(e => e.AssociationId == associationId && e.StartDateTime > DateTime.Today).OrderBy(e => e.StartDateTime).ToList();
            if (pastEvents.Count > 0)
            {
                return pastEvents[0];
            }
            else
            {
                return null;
            }
        }

        public Event GetEvent(int eventId)
        {
            return this.ObjectContext.Events.Where(t => t.Id == eventId && t.AssociationId == this.AssociationId).FirstOrDefault();
        }

        public List<Event> GetEventsByDate(int associationId, DateTime eventDate)
        {
            IQueryable<Event> query =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == associationId && (evt.StartDateTime <= eventDate && eventDate <= evt.EndDateTime)
                  select evt;

            return query.ToList();
        }

        public List<Event> GetEventsByName(int associationId, string eventName)
        {
            IQueryable<Event> query =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == associationId && evt.EventName == eventName
                  select evt;

            return query.ToList();
        }

        public List<Event> GetCertificateListForAttendee(int attendeeId)
        {
            // Get the attendee's association
            int assnId;
            try
            {
                assnId = Convert.ToInt32(this.ObjectContext.Attendees.Where(a => a.Id == attendeeId).FirstOrDefault().AssociationId);
            }
            catch (Exception ex)
            {
                assnId = 0;
                throw new ArgumentException("Attendee ID " + attendeeId.ToString() + " does not exist.", "AttendeeId");
            }

            IQueryable<Event> query =

                  from evt in this.ObjectContext.Events
                  where evt.AssociationId == assnId && evt.CertificatesAvailableStartDate <= DateTime.Now && DateTime.Now <= evt.CertificatesAvailableEndDate
                  select evt;

            return query.ToList();
        }

        public List<SelEventFlagsByEvent_Result> GetEventFlags(int EventId)
        {
            return this.ObjectContext.SelEventFlagsByEvent(EventId).ToList();
        }

        public List<SelEventSession_Result> GetEventSessions(int EventId)
        {
            return this.ObjectContext.SelEventSession(EventId).ToList();
        }

		public List<Attendee> GetAttendeesByLastName(string filter)
		{
			return this.ObjectContext.Attendees.Where(t => t.LastName.StartsWith(filter)).ToList();
		}

        public List<SelOrganization_Result> GetOrganizations(string filter)
        {
            return this.ObjectContext.SelOrganization().Where(t => t.OrgName.Trim().Length > 0).Where(e => e.OrgName.StartsWith(filter, StringComparison.OrdinalIgnoreCase)).ToList();

        }

        public List<SelAttendeeTitle_Result> GetTitles(int associationId, string filter)
        {
            return this.ObjectContext.SelAttendeeTitle(associationId).Where(t => t.Title.Trim().Length > 0).Where(e => e.Title.StartsWith(filter, StringComparison.OrdinalIgnoreCase)) .ToList();
        }


        public void AddEventRegistration(int EventId, int AttendeeId, string Title, string OrgName, string BoothNumber, string Notes, DateTime? EntryDateTime, bool? IsWebReg, string PaymentDate, string BoothNumberRequest, string DoorPrize)
        {
            //// Create the registration
            //EventRegistration er = new EventRegistration();
            //er.EventId = EventId;
            //er.AttendeeId = AttendeeId;
            //er.BoothNumber = BoothNumber;
            //er.BoothNumberRequest = BoothNumberRequest;
            //er.CertPrintDateTime = null;
            //er.DoorPrize = DoorPrize;
            //er.EntryDateTime  = DateTime.Now;
            //er.IsWebRegistration = false;
            //er.Notes = "";
            //er.OrgName = OrgName;
            //er.PaymentDate = null;
            //er.Title = Title;

            //// Get the event
            //Event e = GetEvent(EventId);

            //e.EventRegistrations.Add(er);

            //decimal d = 0;
            //System.Data.Objects.ObjectParameter op = new System.Data.Objects.ObjectParameter("Id", d.GetType());
        
            //return;
      }

        public List<EventType> GetEventTypes()
        {  
            return this.ObjectContext.EventTypes.OrderBy(t => t.TypeName).ToList();
        }

        public List<EventStatus> GetEventStati()
        {
            return this.ObjectContext.EventStati.OrderBy(t => t.SortOrder).ToList();
        }

        public Event AddEvent(Event eventToAdd)
        {
            // Check model for errors
            if (!eventToAdd.IsValid())
            {
                IEnumerable<RuleViolation> v = eventToAdd.GetRuleViolations();
                throw new ArgumentException(v.ElementAt(0).ErrorMessage);
            }

             // Check for an event with the same name for the association
            if (GetEventsByName(eventToAdd.AssociationId, eventToAdd.EventName).Count > 0)
            {
                throw new ArgumentException("An event with this name already exists.", "EventName");
            }

            // Set certificate printing dates to 1 week beyond start date
            eventToAdd.CertificatesAvailableStartDate = eventToAdd.StartDateTime.AddDays(5);
            eventToAdd.CertificatesAvailableEndDate = eventToAdd.StartDateTime.AddYears(1);
        
            base.Add(eventToAdd);
            SaveAllObjectChanges();

            return eventToAdd;
        }

        public void UpdateEvent(Event eventToUpdate)
        {

            Event updEvent = GetEvent(eventToUpdate.Id);
            updEvent.City = eventToUpdate.City;
            updEvent.EndDateTime = eventToUpdate.EndDateTime;
            updEvent.EventName = eventToUpdate.EventName;
            updEvent.Location = eventToUpdate.Location;
            updEvent.StartDateTime = eventToUpdate.StartDateTime;
            updEvent.StateCode = eventToUpdate.StateCode;
            updEvent.TypeId = eventToUpdate.TypeId;
            //updEvent.EventStatusId = eventToUpdate.EventStatusId;
            updEvent.Visibility = eventToUpdate.Visibility;
            updEvent.OnSiteRegistrationDateTime = eventToUpdate.OnSiteRegistrationDateTime;
            updEvent.ShortDesc = eventToUpdate.ShortDesc;
            updEvent.EventDesc = eventToUpdate.EventDesc;
            updEvent.AllowPayByCheck = eventToUpdate.AllowPayByCheck;
            updEvent.BoardMeetingEndDateTime = eventToUpdate.BoardMeetingEndDateTime;
            updEvent.BoardMeetingRoom = eventToUpdate.BoardMeetingRoom;
            updEvent.BoardMeetingStartDateTime = eventToUpdate.BoardMeetingStartDateTime;
            updEvent.CertificatesAvailableEndDate = eventToUpdate.CertificatesAvailableEndDate;
            updEvent.CertificatesAvailableStartDate = eventToUpdate.CertificatesAvailableStartDate;
            updEvent.OnlineRegStartDateTime = eventToUpdate.OnlineRegStartDateTime;
            updEvent.OnlineRegEndDateTime = eventToUpdate.OnlineRegEndDateTime;
            updEvent.OnSiteRegistrationDateTime = eventToUpdate.OnSiteRegistrationDateTime;
            updEvent.RegistrationInfo = eventToUpdate.RegistrationInfo;
            updEvent.CertPrintRequirePayment = eventToUpdate.CertPrintRequirePayment;
            updEvent.CertPrintRequireSurvey = eventToUpdate.CertPrintRequireSurvey;
            
        } 


        public void UpdateEventDates(int eventId, DateTime startDateTime, DateTime endDateTime, DateTime? onlineRegEndDateTime, DateTime? onSiteRegistrationDateTime, DateTime? boardMeetingStartDateTime, DateTime? boardMeetingEndDateTime, string boardMeetingRoom, DateTime? certificatesAvailableStartDate, DateTime? certificatesAvailableEndDate, int id)
        {
            Event e = GetEvent(eventId);
          
            e.EndDateTime = endDateTime;
            e.StartDateTime = startDateTime;
            e.OnSiteRegistrationDateTime = onSiteRegistrationDateTime;
            e.OnlineRegEndDateTime = onlineRegEndDateTime;
            e.BoardMeetingEndDateTime = boardMeetingEndDateTime;
            e.BoardMeetingRoom = boardMeetingRoom;
            e.BoardMeetingStartDateTime = boardMeetingStartDateTime;
            e.CertificatesAvailableEndDate = certificatesAvailableEndDate;
            e.CertificatesAvailableStartDate = certificatesAvailableStartDate;
        }

        //public string GetAttendeeRegistrationFlags(decimal registrationAttendeeId)
        //{
        //    string s = "";
        //    Entities db = CommonUtils.GetEntityDataObject();

        //    using (db)
        //    {
        //        var regFlags = db.RegistrationAttendeeFlags;

        //        var query =
        //        from flag in regFlags
        //        where flag.RegistrationAttendeeId == registrationAttendeeId
        //        select flag;

        //        foreach (RegistrationAttendeeFlag flag in query)
        //        {
        //            if (String.IsNullOrEmpty(s))
        //                s = flag.EventFlag.FlagCode;
        //            else
        //                s = s + ", " + flag.EventFlag.FlagCode;
        //        }
        //    }
        //    return s;
        //}

        //public decimal GetEventRegistrationCeuCount(decimal registrationAttendeeId)
        //{
        //    decimal ceus = 0;
        //    Entities db = CommonUtils.GetEntityDataObject();

        //    using (db)
        //    {
        //        var sessions = db.SessionAttendances;

        //        var query =
        //        from s in sessions
        //        where s.RegistrationAttendeeId == registrationAttendeeId
        //        select s;

        //        foreach (SessionAttendance sa in query)
        //        {
        //            ceus = ceus + (decimal)sa.EventSession.CEUHours;
        //        }
        //    }
        //    return ceus;
        //}
    }
}





