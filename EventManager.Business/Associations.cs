﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.IO;

namespace EventManager.Business
{
    public class AssociationMethods : MethodBase<Association>
    {

        public Association GetAssociation(int associationId)
        {
            return this.ObjectContext.Associations.Where(t => t.AssociationId == associationId).FirstOrDefault();
        }

        public List<Association> GetAssociations()
        {
            IQueryable<Association> query =

                  from evt in this.ObjectContext.Associations
                  where evt.IsApproved == true
                  orderby evt.AssociationName
                  select evt;

            return query.ToList();
        }

        public Association GetAssociationByName(string assnName)
        {
           return this.ObjectContext.Associations.Where(a => a.AssociationName.ToUpper().Trim() == assnName.ToUpper().Trim()).FirstOrDefault();
        }

        public List<Association> GetUnapprovedAssociations()
        {
            IQueryable<Association> query =

                  from evt in this.ObjectContext.Associations
                  where evt.IsApproved == false
                  orderby evt.AssociationName
                  select evt;

            return query.ToList();
        }

        public string GetCertificateFileNameForApplication()
        {
            return "CertificateVertical.rpt";
        }

        public string GetCertificateFileNameForAssociation(int assnId)
        {
            string reportName = "";
            switch (assnId)
            {
                case 1:
                    reportName = "CertificateVerticalIHCA.rpt";
                    break;
                case 2:
                    reportName = "CertificateVerticalUHCA.rpt";
                    break;
            }

            return reportName;
        }

        public string GetCertificateFileNameForEvent(int eventId)
        {
            /*
            StreamReader sr;
            string appFilePath = Server.MapPath("~/Association/Documents/AttendeeSingleBadge.label");
            string assnfilePath = Server.MapPath("~/Association/Documents/Associations/" + assnId.ToString() + "/AttendeeSingleBadge.label");
            string eventFilePath = "";
            if (regsToFind2.Count > 0)
            {
                RegistrationAttendee attendee = ctx.RegistrationAttendees.Where(r => r.Id == firstRegId).FirstOrDefault();
                if (attendee != null)
                {
                    eventFilePath = Server.MapPath("~/Association/Documents/Events/" + attendee.Registration.EventId.ToString() + "/AttendeeSingleBadge.label");
                }
            }

            if (eventFilePath != "" && File.Exists(eventFilePath))
                sr = File.OpenText(eventFilePath);
            else if (File.Exists(assnfilePath))
                sr = File.OpenText(assnfilePath);
            else
                sr = File.OpenText(appFilePath);
            */

            return "CertificateVertical" + eventId.ToString() + ".rpt";
        }

        public Association AddAssociation(string assnName, string address, string city, string stateCode, string zip, string phone, string fax, bool isApproved)
        {
            Association newAssn = new Association();
            newAssn.AssociationName = assnName;
            newAssn.Address = address;
            newAssn.City = city;
            newAssn.StateCode = stateCode;
            newAssn.Zip = zip;
            newAssn.PhoneNumber = phone;
            newAssn.FaxNumber = fax;
            newAssn.IsApproved = isApproved;
            base.Add(newAssn);
            SaveAllObjectChanges();

            return newAssn;
        }

        public void UpdateAssociation(Association assn)
        {
            Association a = GetAssociation(assn.AssociationId);
            a.Address = assn.Address;
            a.AssociationName = assn.AssociationName;
            a.City = assn.City;
            a.FaxNumber = assn.FaxNumber;
            a.HeaderImage = assn.HeaderImage;
            a.PhoneNumber = assn.PhoneNumber;
            a.StateCode = assn.StateCode;
            a.Zip = assn.Zip;

        }
    }
}
