﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.IO;

namespace EventManager.Business
{
    public class AspRegRequestFacilityEmployeeMethods : MethodBase<AspRegRequestFacilityEmployee>
    {
        public AspRegRequestFacilityEmployee GetEmployeeRequest(int id)
        {
            return this.ObjectContext.AspRegRequestFacilityEmployees.Where(t => t.Id == id).FirstOrDefault();
        }

        public AspRegRequestFacilityEmployee GetEmployeeRequestByUserId(Guid userId)
        {
            return this.ObjectContext.AspRegRequestFacilityEmployees.Where(t => t.UserId == userId).FirstOrDefault();
        }

        public List<AspRegRequestFacilityEmployee> GetUnapprovedEmployeeRequestsForFacility(int orgId)
        {
            IQueryable<AspRegRequestFacilityEmployee> query =

                  from evt in this.ObjectContext.AspRegRequestFacilityEmployees
                  where evt.OrgId == orgId
                  orderby evt.RequestDateTime descending
                  select evt;

            return query.ToList();
        }
    }
}
