﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.Data;

namespace EventManager.Business
{
    public class OrganizationFeeCategoryMethods : MethodBase<OrganizationFeeCategory>
    {
        public OrganizationFeeCategory GetFeeCategory(int id)
        {
            return this.ObjectContext.OrganizationFeeCategories.SingleOrDefault(d => d.Id == id);
        }

        public List<OrganizationFeeCategory> GetFeeCategoriesByOrgType(int orgTypeId)
        {
            IQueryable<OrganizationFeeCategory> query =

                  from evt in this.ObjectContext.OrganizationFeeCategories
                  where evt.OrganizationTypeId == orgTypeId
                  orderby evt.CategoryDesc
                  select evt;

            return query.ToList();

        }

        public List<OrganizationFeeCategory> GetFeeCategoriesByDescAndId(string desc, int orgTypeId)
        {
            IQueryable<OrganizationFeeCategory> query =
                 from evt in this.ObjectContext.OrganizationFeeCategories
                 where evt.CategoryDesc == desc && evt.OrganizationTypeId == orgTypeId
                 select evt;

            return query.ToList();
        }

        public List<OrganizationFeeCategory> GetFeeCategories(int associationId)
        {
            IQueryable<OrganizationFeeCategory> query =

                  from evt in this.ObjectContext.OrganizationFeeCategories
                  where evt.OrganizationType.AssociationId == associationId
                  orderby evt.OrganizationType.OrgTypeName, evt.CategoryDesc
                  select evt;

            return query.ToList();
        }

        public override void Add(OrganizationFeeCategory newObject)
        {
            List<OrganizationFeeCategory> cases = GetFeeCategoriesByOrgType(newObject.OrganizationTypeId);

            if (String.IsNullOrEmpty(newObject.CategoryDesc))
            {
                throw new Exception("Category Description cannot be blank.");
            }

            base.Add(newObject);
        }

        public override void Delete(OrganizationFeeCategory obsoleteObject)
        {
            base.Delete(obsoleteObject);
        }

        public void Update(OrganizationFeeCategory original, OrganizationFeeCategory modified)
        {
            List<OrganizationFeeCategory> cases = GetFeeCategoriesByOrgType(modified.OrganizationTypeId);

            if(String.IsNullOrEmpty(modified.CategoryDesc)) {
                throw new Exception("Category Description cannot be blank.");
            } else {
                List<OrganizationFeeCategory> tests = GetFeeCategoriesByDescAndId(modified.CategoryDesc, original.OrganizationTypeId);
                if (tests.Count > 0)
                {
                    if(tests[0].Id != modified.Id) {
                        throw new Exception("\"" + modified.CategoryDesc + "\" is taken, please choose another.");
                    }
                }
            }

            try
            {   
                //build entitykey from incoming contactID so that you can Attach
                var ekey = new EntityKey("Entities.OrganizationFeeCategories", "Id", original.Id);
                original.EntityKey = ekey;

                this.ObjectContext.Attach(original);
                this.ObjectContext.ApplyCurrentValues("OrganizationFeeCategories", modified);

                //originalCse.ParticipantId = modifiedCse.ParticipantId;
                this.SaveAllObjectChanges();
            }
            catch (Exception ex)
            {
                //insert proper exception handling here...
                throw ex;
            }
        }
    }
}
