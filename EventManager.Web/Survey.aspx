﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.master" AutoEventWireup="true" CodeBehind="Survey.aspx.cs" Inherits="EventManager.Web.Survey" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content" class="box">
	<h2 class="page-title">Event Survey</h2>
	<div class="breadcrumb"><a href="#" id="home">Home</a>  »  <a href="#">Certificates</a></div>
	
      <%--  <asp:DropDownList ID="EventsDropDownList" runat="server" 
            DataSourceID="EventsDataSource" DataTextField="EventName" DataValueField="Id" 
            Width="400px">
        </asp:DropDownList>--%>
	
    <asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource">
        <ItemTemplate>
            <table>
            <tr><td colspan="2"><b><asp:Label ID="EventNameLabel" runat="server" Text='<%# Bind("EventName") %>' /></b></td></tr>
            <tr>
                <td>Location:</td>
                <td><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Bind("FullLocation") %>' /></td>
            </tr>
            <tr>
                <td>Date:</td>
                <td><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Bind("StartDateTime") %>' /></td>
            </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <br />

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" CssClass="failureNotification" ValidationGroup="ValidationGroup1" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" Display="None" ValidationGroup="ValidationGroup1"></asp:CustomValidator>

    <asp:FormView ID="FormView2" runat="server" DataSourceID="AttendeeSurveyDataSource" 
            oniteminserted="FormView2_ItemInserted" DefaultMode="Insert" 
            DataKeyNames="Id" oniteminserting="FormView2_ItemInserting"  >
        <InsertItemTemplate>
            <table>
                <tr><td><h3>Which sessions were MOST helpful to you? </h3></td></tr>
                <tr><td><i>Please explain why.</i></td></tr>
                <tr><td><telerik:radtextbox ID="MostRadTextBox" runat="server" Height="150px" TextMode="MultiLine" Width="600px" Text='<%# Bind("MostHelpfulText") %>'></telerik:radtextbox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please tell us which were the MOST HELPFUL sessions." ControlToValidate="MostRadTextBox" ValidationGroup="ValidationGroup1">*</asp:RequiredFieldValidator></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td><h3>Which sessions were LEAST helpful to you? </h3></td></tr>
                <tr><td><i>Please explain why.</i></td></tr>
                <tr><td><telerik:radtextbox ID="LeastRadTextBox" runat="server" Height="150px" TextMode="MultiLine" Width="600px" Text='<%# Bind("LeastHelpfulText") %>' />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please tell us which were the LEAST HELPFUL sessions." ControlToValidate="LeastRadTextBox" ValidationGroup="ValidationGroup1">*</asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>
                    <telerik:RadButton ID="InsertRadButton" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
                    <telerik:RadButton ID="InsertCancelRadButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                </td></tr>
            </table>
        </InsertItemTemplate>
        <ItemTemplate>
            <table>
                <tr><td>
                    <p style="color:Green; font-size:large; margin-bottom:0px;">Thank you for your feedback!</p>
                    <asp:HyperLink ID="CertificateHyperLink" runat="server" NavigateUrl="~/Association/Reports2/ReportViewer.aspx">Print your certificate</asp:HyperLink><br />
                    <asp:HyperLink ID="AttendanceHistoryHyperlink" runat="server" NavigateUrl="~/PrintCertHistory.aspx">Return to your attendance history.</asp:HyperLink>
                </td></tr>
                <tr><td><h3>Which sessions were MOST helpful to you? </h3></td></tr>
                <tr><td><i>Please explain why.</i></td></tr>
                <tr><td><telerik:radtextbox ID="MostRadTextBox" runat="server" Height="150px" 
                        TextMode="MultiLine" Width="600px" Text='<%# Bind("MostHelpfulText") %>' 
                        BackColor="#CCCCCC" ReadOnly="True"></telerik:radtextbox></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td><h3>Which sessions were LEAST helpful to you? </h3></td></tr>
                <tr><td><i>Please explain why.</i></td></tr>
                <tr><td><telerik:radtextbox ID="LeastRadTextBox" runat="server" Height="150px" 
                        TextMode="MultiLine" Width="600px" Text='<%# Bind("LeastHelpfulText") %>' 
                        BackColor="#CCCCCC" ReadOnly="True"></telerik:radtextbox></td></tr>
            </table>
        </ItemTemplate>
    </asp:FormView>

    <asp:ObjectDataSource ID="AttendeeSurveyDataSource" runat="server" 
        DataObjectTypeName="EventManager.Model.AttendeeEventSurvey" 
        DeleteMethod="Delete" InsertMethod="Add" 
        SelectMethod="GetEventSurveyByAttendee" 
        TypeName="EventManager.Business.AttendeeEventSurveyMethods" 
            onselecting="AttendeeSurveyDataSource_Selecting">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="attendeeId" QueryStringField="AttendeeId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="EventDataSource" runat="server" 
        SelectMethod="GetEvent" TypeName="EventManager.Business.EventMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</div><!-- end #content -->
<div class="clear"></div>

    

</asp:Content>
