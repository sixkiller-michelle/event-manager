﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Web.Facility;
using System.IO;

namespace EventManager.Web
{
    public partial class Survey : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null || Request.QueryString["AttendeeId"] == null)
            {
                Response.Redirect("~/PrintCertSearch.aspx");
            }
            else
            {
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                int attendeeId = Convert.ToInt32(Request.QueryString["AttendeeId"]);
                int assnId = Convert.ToInt32(Request.QueryString["AssnId"]);

                if (!IsPostBack)
                {
                    // See if a survey exists
                    EventManager.Business.AttendeeEventSurveyMethods m = new EventManager.Business.AttendeeEventSurveyMethods();
                    if (m.GetEventSurveyByAttendee(eventId, attendeeId) != null)
                    {
                        FormView2.ChangeMode(FormViewMode.ReadOnly);
                    }
                    else
                    {
                        FormView2.ChangeMode(FormViewMode.Insert);
                    }
                }

                // Set Hyperlink url
                HyperLink h1 = (HyperLink)FormView2.FindControl("CertificateHyperLink");
                HyperLink h2 = (HyperLink)FormView2.FindControl("AttendanceHistoryHyperLink");
                if (h1 != null && h2 != null)
                {
                    // See if payment has been made (to show certificate print link)
                    EventManager.Business.RegistrationMethods m3 = new EventManager.Business.RegistrationMethods();
                    EventManager.Model.Registration r = m3.GetRegistrationByAttendee(eventId, attendeeId);
                    decimal balance = m3.GetBalance(eventId, attendeeId);

                    if (r != null)
                    {
                        if (balance <= 0)
                        {
                            string certificateReportName = "";
                            EventManager.Business.AssociationMethods m2 = new EventManager.Business.AssociationMethods();
                            if (File.Exists(Server.MapPath("~/Association/Reports2/" + m2.GetCertificateFileNameForEvent(eventId))))
                                certificateReportName = m2.GetCertificateFileNameForEvent(eventId);
                            else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m2.GetCertificateFileNameForAssociation(r.Event.AssociationId))))
                                certificateReportName = m2.GetCertificateFileNameForAssociation(r.Event.AssociationId);
                            else
                                certificateReportName = m2.GetCertificateFileNameForApplication();

                            h1.NavigateUrl = "~/Association/Reports2/ReportViewer.aspx?FileName=" + certificateReportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId;
                            h1.Visible = true;
                        }
                        else
                        {
                            h1.Visible = false;
                        }
                    }
                    else
                    {
                        h1.Visible = false;
                    }

                    // Set link back to event history page
                    h2.NavigateUrl = "~/PrintCertHistory.aspx?id=" + attendeeId;
                }
            }
        }

        protected void EventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters[0] = Request.QueryString["AssnId"];
        }

        protected void FormView2_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    EventManager.Business.AttendeeEventSurveyMethods m = new EventManager.Business.AttendeeEventSurveyMethods();
                    m.SaveAllObjectChanges();
                    int assnId = Convert.ToInt32(Request.QueryString["AssnId"]);
                    int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                    int attendeeId = Convert.ToInt32(Request.QueryString["AttendeeId"]);
                    Response.Redirect("~/Survey.aspx?AssnId=" + assnId + "&EventId=" + eventId + "&AttendeeId=" + attendeeId);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    else
                        CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                }
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void AttendeeSurveyDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }

        protected void FormView2_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["EventId"] = Convert.ToInt32(Request.QueryString["EventId"]);
            e.Values["AttendeeId"] = Convert.ToInt32(Request.QueryString["AttendeeId"]);
        }
    }
}