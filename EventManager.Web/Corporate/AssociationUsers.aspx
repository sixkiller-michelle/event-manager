﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="AssociationUsers.aspx.cs" Inherits="EventManager.Web.Association.Corporate.AssociationUsersForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Horizontal">
    <telerik:RadPane ID="CriteriaRadPane" runat="server" Height="100">
        
        <asp:Panel ID="CriteriaPanel" runat="server" Style="padding:10px;">

        <table>
        <tr>
            <td>Association:</td>
            <td>
                <telerik:RadComboBox ID="AssociationRadComboBox" Runat="server" AutoPostBack="True"  
                    DataTextField="AssociationName" 
                    DataValueField="AssociationId"  Width="400px" MarkFirstMatch="true" 
                    OnSelectedIndexChanged="AssociationRadComboBox_SelectedIndexChanged" 
                    OnDataBound="AssociationRadComboBox_DataBound" >
                </telerik:RadComboBox>
            </td>
        </tr>
        </table>
        
        </asp:Panel>

    </telerik:RadPane>
    <telerik:RadPane ID="GridRadPane" runat="server">

        <telerik:RadSplitter ID="RadSplitter2" runat="server" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Vertical">
        <telerik:RadPane ID="AssnUsersRadPane" runat="server" Height="100">
            
            <telerik:RadGrid ID="AssnUsersRadGrid" runat="server" CellSpacing="0" AllowSorting="true"
            GridLines="None" OnNeedDataSource="AssnUsersRadGrid_NeedDataSource" OnItemDataBound="AssnUsersRadGrid_ItemDataBound"
            OnItemCommand="AssnUsersRadGrid_ItemCommand">
            <MasterTableView DataKeyNames="UserName" AutoGenerateColumns="False">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                 
                    <telerik:GridTemplateColumn HeaderText="Association" UniqueName="AssociationGridTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="AssociationNameLabel" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Name" UniqueName="NameGridTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="FullNameLabel" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Email" UniqueName="EmailGridTemplateColumn">
                        <ItemTemplate>
                            <asp:HyperLink ID="EmailHyperlink" runat="server" NavigateUrl='<%# "mailto:" + Eval("Email") %>' Text='<%# Eval("Email") %>' /><br />
                            <asp:Label ID="UsernameLabel" runat="server" Text='<%# "Username: " + Eval("UserName") %>' ></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridCheckBoxColumn DataField="IsActive" DataType="System.Boolean" FilterControlAltText="Filter IsActive column"
                        HeaderText="Email Validated" SortExpression="IsActive" UniqueName="IsActive">
                    </telerik:GridCheckBoxColumn>

                    <telerik:GridTemplateColumn HeaderText="Locked Out" UniqueName="LockedOutGridTemplateColumn" SortExpression="IsLockedOut">
                        <ItemTemplate>
                            <asp:Panel ID="LockedOutPanel" runat="server" Visible='<%# (bool)Eval("IsLockedOut") %>'>
                                <asp:Label ID="LockedOutLabel" runat="server" Text="LOCKED OUT" ForeColor="Red" ></asp:Label>&nbsp;
                                <telerik:RadButton ID="UnlockRadButton" runat="server" Text="Unlock" CommandName="Unlock" />
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="CreationDate" DataType="System.DateTime" FilterControlAltText="Filter CreationDate column"
                        HeaderText="Creation" ReadOnly="True" SortExpression="CreationDate" UniqueName="CreationDate" ItemStyle-HorizontalAlign="Right">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastLoginDate" DataType="System.DateTime" FilterControlAltText="Filter LastLoginDate column"
                        HeaderText="Last Login" SortExpression="LastLoginDate" UniqueName="LastLoginDate" ItemStyle-HorizontalAlign="Right">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        </telerik:RadPane>

        <telerik:RadPane ID="FacilityUsersRadPane" runat="server" Height="100">

            <asp:Panel ID="SelectFacilityPanel" runat="server" style="padding:10px; background-color:#cccccc;">
            <table>
                <tr>
                <td>Facility:</td>
                <td>
                    <telerik:RadComboBox ID="OrgRadComboBox" Runat="server" AutoPostBack="True"  
                        DataTextField="OrgName" Height="300"
                        DataValueField="Id"  Width="400px" MarkFirstMatch="true" 
                        OnSelectedIndexChanged="OrgRadComboBox_SelectedIndexChanged" 
                        OnDataBound="OrgRadComboBox_DataBound" >
                    </telerik:RadComboBox>
                </td>
            </tr>
            </table>
            </asp:Panel>

            <telerik:RadGrid ID="FacilityUsersRadGrid" runat="server" CellSpacing="0"
            GridLines="None" OnNeedDataSource="FacilityUsersRadGrid_NeedDataSource" OnItemDataBound="FacilityUsersRadGrid_ItemDataBound"
            OnItemCommand="FacilityUsersRadGrid_ItemCommand">
            <MasterTableView DataKeyNames="UserName" AutoGenerateColumns="False">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                 
                    <telerik:GridTemplateColumn HeaderText="Association" UniqueName="AssociationGridTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="AssociationNameLabel" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Facility" UniqueName="FacilityGridTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="FacilityNameLabel" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Name" UniqueName="NameGridTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="FullNameLabel" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Email" UniqueName="EmailGridTemplateColumn">
                        <ItemTemplate>
                            <asp:HyperLink ID="EmailHyperlink" runat="server" NavigateUrl='<%# "mailto:" + Eval("Email") %>' Text='<%# Eval("Email") %>' /><br />
                            <asp:Label ID="UsernameLabel" runat="server" Text='<%# "Username: " + Eval("UserName") %>' ></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridCheckBoxColumn DataField="IsApproved" DataType="System.Boolean" FilterControlAltText="Filter IsApproved column"
                        HeaderText="Email Validated" SortExpression="IsApproved" UniqueName="IsApproved">
                    </telerik:GridCheckBoxColumn>

                    <telerik:GridTemplateColumn HeaderText="Locked Out" UniqueName="LockedOutGridTemplateColumn" SortExpression="IsLockedOut">
                        <ItemTemplate>
                            <asp:Panel ID="LockedOutPanel" runat="server" Visible='<%# (bool)Eval("IsLockedOut") %>'>
                                <asp:Label ID="LockedOutLabel" runat="server" Text="LOCKED OUT" ForeColor="Red" ></asp:Label>&nbsp;
                                <telerik:RadButton ID="UnlockRadButton" runat="server" Text="Unlock" CommandName="Unlock" />
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="CreationDate" DataType="System.DateTime" FilterControlAltText="Filter CreationDate column"
                        HeaderText="Creation" ReadOnly="True" SortExpression="CreationDate" UniqueName="CreationDate" ItemStyle-HorizontalAlign="Right">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastLoginDate" DataType="System.DateTime" FilterControlAltText="Filter LastLoginDate column"
                        HeaderText="Last Login" SortExpression="LastLoginDate" UniqueName="LastLoginDate" ItemStyle-HorizontalAlign="Right">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>

        </telerik:RadPane>
        </telerik:RadSplitter>

        
    </telerik:RadPane>
</telerik:RadSplitter>
<%--    
<asp:ObjectDataSource ID="AssociationsDataSource" runat="server" 
        SelectMethod="GetAssociations" 
        TypeName="EventManager.Business.AssociationMethods"></asp:ObjectDataSource>--%>


</asp:Content>
