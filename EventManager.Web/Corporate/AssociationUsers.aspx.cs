﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using EventManager.Model;
using Telerik.Web.UI;
using EventManager.Business;

namespace EventManager.Web.Association.Corporate
{
    public partial class AssociationUsersForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAssociations();
            }
        }

        private void LoadAssociations()
        {
            AssociationMethods m = new AssociationMethods();
            AssociationRadComboBox.DataSource = m.GetAssociations();
            AssociationRadComboBox.DataBind();
            AssociationRadComboBox.Items.Insert(0, new RadComboBoxItem("[All]", ""));
        }

        protected void AssnUsersRadGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            AspUserMethods m = new AspUserMethods();

            if (AssociationRadComboBox.SelectedValue != "")
            {
                AssnUsersRadGrid.DataSource = m.GetAssociationUsersByAssociation(Convert.ToInt32(AssociationRadComboBox.SelectedValue));
                AssnUsersRadGrid.MasterTableView.GetColumnSafe("AssociationGridTemplateColumn").Visible = false;
            }
            else
            { 
                AssnUsersRadGrid.DataSource = m.GetAllAssociationUsers();
                AssnUsersRadGrid.MasterTableView.GetColumnSafe("AssociationGridTemplateColumn").Visible = true;
            }
                
        }

        protected void AssnUsersRadGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                string userName = ((GridDataItem)e.Item).GetDataKeyValue("UserName").ToString();
                MembershipUser u = Membership.GetUser(userName);
                Guid userId = new Guid(u.ProviderUserKey.ToString());

                UserAssociationMethods m = new UserAssociationMethods();
                EventManager.Model.Association assn = m.GetAssociationForUser(userId);

                AssociationEmployeeMethods am = new AssociationEmployeeMethods();
                AssociationEmployee emp = am.GetEmployeeByUserId(userId);

                // Put assocation name in the grid
                if (assn != null)
                {
                    Label assnNameLabel = (Label)e.Item.FindControl("AssociationNameLabel");
                    assnNameLabel.Text = assn.AssociationName;
                }

                // Put person's name in the grid
                if (emp != null)
                {
                    Label fullNameLabel = (Label)e.Item.FindControl("FullNameLabel");
                    fullNameLabel.Text = emp.FullName;
                }

                // Hide email if username and email are same
                Label usernameLabel = (Label)e.Item.FindControl("UsernameLabel");
                usernameLabel.Visible = (u.Email != u.UserName);
            }
        }

        protected void AssnUsersRadGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Unlock")
            {
                
                GridItem item = (GridItem)e.Item;

                string UserName = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"].ToString();

                MembershipUser um = Membership.GetUser(UserName);
                
                bool locked = um.IsLockedOut;

                if (locked)
                {
                    um.UnlockUser();
                    AssnUsersRadGrid.Rebind();
                }
                
            }
        }

        protected void FacilityUsersRadGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            AspUserMethods m = new AspUserMethods();

            if (AssociationRadComboBox.SelectedValue != "")
            {
                if (OrgRadComboBox.SelectedIndex > 0)
                    FacilityUsersRadGrid.DataSource = m.GetFacilityUsersByOrganization(Convert.ToInt32(OrgRadComboBox.SelectedValue));
                else
                    FacilityUsersRadGrid.DataSource = m.GetFacilityUsersByAssociation(Convert.ToInt32(AssociationRadComboBox.SelectedValue));

                FacilityUsersRadGrid.MasterTableView.GetColumnSafe("AssociationGridTemplateColumn").Visible = false;
            }
                
            else
            { 
                FacilityUsersRadGrid.DataSource = m.GetAllFacilityUsers();
                FacilityUsersRadGrid.MasterTableView.GetColumnSafe("AssociationGridTemplateColumn").Visible = true;
            }
                

        }

        protected void FacilityUsersRadGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                string userName = ((GridDataItem)e.Item).GetDataKeyValue("UserName").ToString();
                MembershipUser u = Membership.GetUser(userName);
                Guid userId = new Guid(u.ProviderUserKey.ToString());

                UserAssociationMethods m = new UserAssociationMethods();
                EventManager.Model.Association assn = m.GetAssociationForUser(userId);

                AttendeeMethods am = new AttendeeMethods();
                Attendee att = am.GetAttendeeByUserId(userId);

                // Put assocation name in the grid
                if (assn != null)
                { 
                    Label assnNameLabel = (Label)e.Item.FindControl("AssociationNameLabel");
                    assnNameLabel.Text = assn.AssociationName;
                }
                
                // Put facility name and person's name in the grid
                if (att != null)
                { 
                    Label facilityNameLabel = (Label)e.Item.FindControl("FacilityNameLabel");
                    facilityNameLabel.Text = (att.Organization != null ? att.Organization.OrgName : "");

                    Label fullNameLabel = (Label)e.Item.FindControl("FullNameLabel");
                    fullNameLabel.Text = att.FullName;
                }
                
                // Hide email if username and email are same
                Label usernameLabel = (Label)e.Item.FindControl("UsernameLabel");
                usernameLabel.Visible = (u.Email != u.UserName);
            }
        }

        protected void FacilityUsersRadGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Unlock")
            {

                GridItem item = (GridItem)e.Item;

                string UserName = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"].ToString();

                MembershipUser um = Membership.GetUser(UserName);

                bool locked = um.IsLockedOut;

                if (locked)
                {
                    um.UnlockUser();
                    FacilityUsersRadGrid.Rebind();
                }

            }
        }

        protected void AssociationRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (Request.QueryString["assnId"] != null)
            { 
                int associationId = 0;
                if (Int32.TryParse(Request.QueryString["assnId"], out associationId))
                { 
                    AssociationRadComboBox.SelectedValue = associationId.ToString();
                    LoadOrgCombo();
                    AssnUsersRadGrid.Rebind();
                }
            }
        }

        private void LoadOrgCombo()
        {
            OrganizationMethods m = new OrganizationMethods();
            if (AssociationRadComboBox.SelectedValue != "")
            {
                OrgRadComboBox.DataSource = m.GetOrganizationsByAssociation(Convert.ToInt32(AssociationRadComboBox.SelectedValue));
                OrgRadComboBox.DataBind();
                OrgRadComboBox.Items.Insert(0, new RadComboBoxItem("[All]", ""));
                OrgRadComboBox.Enabled = true;
            }
            else
            {
                OrgRadComboBox.DataSource = null;
                OrgRadComboBox.Items.Insert(0, new RadComboBoxItem("[All]", ""));
                OrgRadComboBox.SelectedIndex = 0;
                OrgRadComboBox.Enabled = false;
            }
        }

        protected void AssociationRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            LoadOrgCombo();
            AssnUsersRadGrid.Rebind();
            FacilityUsersRadGrid.Rebind();
        }

        protected void OrgRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (Request.QueryString["orgId"] != null)
            {
                int orgId = 0;
                if (Int32.TryParse(Request.QueryString["orgId"], out orgId))
                    OrgRadComboBox.SelectedValue = orgId.ToString();
            }
        }

        protected void OrgRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            FacilityUsersRadGrid.Rebind();
        }
    }
}