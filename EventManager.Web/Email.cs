﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using EventManager.Business;

namespace EventManager.Web
{
    public partial class Email
    {
        public void SendMail(string to, string from, string subject, string body)
        {
            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("admin@ceuavenue.com", to);
            Email.Body = body;
            Email.IsBodyHtml = true;
            Email.Subject = subject;

            System.Net.NetworkCredential creds = new System.Net.NetworkCredential("kisner.michelle@gmail.com", "oihadaball12");
            System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            mailClient.EnableSsl = true;
            mailClient.Credentials = creds;
            try
            {
                mailClient.Send(Email);
            }
            catch (Exception ex)
            {

            }
            
        }

        public void SendMailUsingConfig(string to, string from, string subject, string body)
        {
            // UPDATE THIS VALUE TO YOUR EMAIL ADDRESS
            string ToAddress = to;

            // (1) Create the MailMessage instance
            MailMessage mm = new MailMessage(from, ToAddress);

            // (2) Assign the MailMessage's properties
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;

            // (3) Create the SmtpClient object
            SmtpClient smtp = new SmtpClient();
            smtp.Timeout = 30000;
            smtp.EnableSsl = true;

            // (4) Send the MailMessage (will use the Web.config settings)
            smtp.Send(mm);

        }

        public string GetBodyText_ApproveAssociation(string assnName, string firstName, string lastName, string email)
        {
            string body = "A new association request was just registered on ceuavenue.com.";
            body += "Association name: " + assnName + "<br />";
            body += "Name: " + firstName + " " + lastName + "<br /><br />";
            body += "Go to <a href='http://www.ceuavenue.com/Association/Admin/ApproveRegistrations.aspx'>Approve Registrations</a>";
            return body;
            
        }

        public string GetBodyText_ApproveAssociationEmployee(string assnName, string firstName, string lastName, string email)
        {
            string body = "A person just registered as a staff member at your association.  The person's information is listed below.  You will ";
            body +=  "need to grant role permissions to this person in ceuavenue.com (see link below) before the person will be granted access to the application.<br /><br />";
            body += "Association name: " + assnName + "<br />";
            body += "Name: " + firstName + " " + lastName + "<br />";
            body += "Email: " + email + "<br /><br />";
            body += "Go to <a href='http://www.ceuavenue.com/Association/Admin/Employees.aspx'>Approve Registrations</a> to approve or reject this registration.";
            return body;

        }

        public string GetBodyText_FacilityRegistration(string assnName, int? orgId, string facilityName, string firstName, string lastName, string email)
        {
            string body = "A person just registered as a facility employee.  The person's information is listed below. <br /><br />";
            body += "Association name: " + assnName + "<br />";
            body += "Facility name: " + facilityName + (orgId.HasValue ? "" : " (NEW)") + "<br />";
            body += "Name: " + firstName + " " + lastName + "<br />";
            body += "Email: " + email + "<br /><br />";
            return body;

        }

        public void SendActivateAccountEmail(int associationId, Guid userId, string userName, string email)
        {
            string logoPath = "";
            string assnName = "";
            if (associationId > 0)
            {
                AssociationMethods assnMethods = new AssociationMethods();
                EventManager.Model.Association assn = assnMethods.GetAssociation(associationId);
                if (assn != null)
                {
                    //logoPath = "http://www.ceuavenue.com/Association/Documents/Associations/" + associationId.ToString() + "/logo.png";
                    logoPath = "http://www.ceuavenue.com" + assn.LogoPath.Substring(1);
                    assnName = assn.AssociationName;
                }
            }

            StringBuilder emailMessage = new StringBuilder();
            if (!string.IsNullOrEmpty(assnName))
            {
                emailMessage.Append("<img src='" + logoPath + "' /><br>");
                emailMessage.Append("Thank you for creating an account with " + assnName + " on ceuavenue.");
            }
            else
            {
                emailMessage.Append("Thank you for creating an account with on ceuavenue.");
            }
                
            emailMessage.Append("<br />");
            emailMessage.Append("Please click the below link to activate your account: <br /><br />");
            emailMessage.Append(string.Format("<a href='http://www.ceuavenue.com/Account/ActivateUser.aspx?userName={0}&Id={1}' style='font-size:14px;'>Activate {0} </a>", userName, userId.ToString()));

            EventManager.Web.Email mail = new EventManager.Web.Email();
            mail.SendMail(email, "noreply@ceuavenue.com", "Activate your ceuavenue.com account", emailMessage.ToString());
        }

        public void SendNewAccountNotificationToDev(string regType, string assnName, string orgName, string firstName, string lastName, string email)
        {
            string logoPath = "";
            string subject = "";
            if (regType == "Association")
            {
                subject = "New ceuavenue.com account request: New Association";
            }
            else if (regType == "AssociationStaff")
            {
                subject = "New ceuavenue.com account request: Association Staff";
            }
            else
            {
                subject = "New ceuavenue.com account request: Facility Staff";
            }

            StringBuilder emailMessage = new StringBuilder();
            emailMessage.Append("<table>");
            emailMessage.Append("<tr><td>Name:</td><td>" + firstName + " " + lastName + "</td></tr>");
            emailMessage.Append("<tr><td>Email:</td><td>" + email + "</td></tr>");
            emailMessage.Append("<tr><td>Association:</td><td>" + assnName + "</td></tr>");
            emailMessage.Append("<tr><td>Facility Name:</td><td>" + orgName + "</td></tr>");
            emailMessage.Append("</table>");
            
            emailMessage.Append("<br />");
            emailMessage.Append("Approve or reject registration:<br />");
            emailMessage.Append(string.Format("<a href='http://www.ceuavenue.com/Association/Admin/ApprovedRegistrations.aspx'>Approve Registrations</a>"));

            EventManager.Web.Email mail = new EventManager.Web.Email();
            mail.SendMail("michelle@sixkillersoftware.com", email, subject, emailMessage.ToString());
        }
    }
}