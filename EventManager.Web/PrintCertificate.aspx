﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.master" AutoEventWireup="true" CodeBehind="PrintCertificate.aspx.cs" Inherits="EventManager.Web.Facility.PrintCertificateForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
table td {padding:3px; vertical-align:middle;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content" class="box">
	<h2 class="page-title">Print Certificate</h2>
	<div class="breadcrumb"><a href="Default.aspx" id="home">Home</a>  »  <a href="#">Certificates</a></div>

    <asp:ValidationSummary ID="PageValidationSummary" runat="server" ForeColor="Red" ValidationGroup="PageValidationGroup" />
    <asp:CustomValidator ID="PageCustomValidator" runat="server" ErrorMessage="An error has occurred" Display="None" ValidationGroup="PageValidationGroup"></asp:CustomValidator>

    <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" DisplaySideBar="False" 
            onactivestepchanged="Wizard1_ActiveStepChanged"
            onnextbuttonclick="Wizard1_NextButtonClick">
        <WizardSteps>
            
            <asp:WizardStep ID="AttendeeSearchWizardStep" runat="server" Title="Enter First Name and Last Name" >
                <asp:ValidationSummary ID="AttendeeSearchValidationSummary" runat="server" ValidationGroup="AttendeeSearchValidationGroup" ForeColor="Red" />
                <asp:RequiredFieldValidator ID="FirstNameRequiredValidator" runat="server" ErrorMessage="First name is required" ValidationGroup="AttendeeSearchValidationGroup" ControlToValidate="FirstNameRadTextBox" Display="None"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="AttendeeSearchCustomValidator" runat="server" ErrorMessage="" ForeColor="Red" ValidationGroup="AttendeeSearchValidationGroup" Display="None"></asp:CustomValidator><br />
            <table>
            <tr>
                <td>Association:</td>
                <td>
                    <telerik:RadComboBox ID="AssociationRadComboBox" Runat="server" AutoPostBack="true"  
                        DataTextField="AssociationName" AppendDataBoundItems="true" 
                        DataValueField="AssociationId"  Width="400px" 
                        OnSelectedIndexChanged="AssociationRadComboBox_SelectedIndexChanged" 
                        OnDataBound="AssociationRadComboBox_DataBound">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr> 
            <tr>
                <td>Facility:</td>
                <td>
                    <telerik:RadComboBox ID="FacilityRadComboBox" Runat="server" AllowCustomText="true" MarkFirstMatch="true"  
                        DataTextField="OrgName" 
                        DataValueField="Id" Width="400px" AppendDataBoundItems="True">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>First Name:</td>
                <td><telerik:RadTextBox ID="FirstNameRadTextBox" runat="server" EmptyMessage="First name" CausesValidation="true" />
                </td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><telerik:RadTextBox ID="LastNameRadTextBox" runat="server" EmptyMessage="Last name" /></td>
            </tr>
          <%--  <tr>
                <td>&nbsp;</td>
                <td><telerik:RadButton ID="AttendeeSearchRadButton" runat="server" Text="Find Me" CausesValidation="true" onclick="AttendeeSearchRadButton_Click" UseSubmitBehavior="true" /></td>
            </tr>--%>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="AttendeeIdLabel" runat="server" Text=""></asp:Label></td>
            </tr>
            </table>
            </asp:WizardStep>

            <asp:WizardStep ID="EnterAttendeeId" runat="server" Title="Enter Your Attendee ID">
            
            <asp:ValidationSummary ID="AttendeeIdValidationSummary" runat="server" ForeColor="Red" ValidationGroup="AttendeeIdValidationGroup" />
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Attendee ID does not exist" Display="None" ValidationGroup="AttendeeIdValidationGroup"></asp:CustomValidator>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="AttendeeIdRadTextBox" ErrorMessage="You must enter your attendee ID" ValidationGroup="AttendeeIdValidationGroup" Display="None"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" Operator="DataTypeCheck" Type="Integer"  ControlToValidate="AttendeeIdRadTextBox" ErrorMessage="Attendee ID must be a whole number" ValidationGroup="AttendeeIdValidationGroup" Display="None" />
   
            <table>
            <tr>
                <td>Enter your attendee ID:&nbsp;</td>
                <td style="width:50px;"><telerik:RadTextBox ID="AttendeeIdRadTextBox" runat="server" EmptyMessage="Attendee ID" /></td>
                <td style="text-align:left;">
                  <%--  <telerik:RadButton ID="ViewCertificateRadButton" runat="server" Text="View Event History" CausesValidation="true" onclick="ViewCertificateRadButton_Click" UseSubmitBehavior="true" />--%></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">
                    <p class="instructions">Note: your attendee ID can be found on your badge, under the barcode</p>
                    <p>If you do not know your attendee ID, you can 
                        <asp:Button ID="SearchNameButton" runat="server" Text="enter your name instead" /></p>
                </td>
            </tr>
            </table>
            </asp:WizardStep>

            <asp:WizardStep ID="SelectNameWizardStep" runat="server" Title="Select Your Name">
                <telerik:radgrid id="AttendeesRadGrid" runat="server" autogeneratecolumns="False"
                    cellspacing="0" datasourceid="AttendeesDataSource" gridlines="None">
                    <mastertableview datasourceid="AttendeesDataSource">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>

            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>

                <Columns>
                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" 
                        FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" 
                        UniqueName="Id">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FullName" 
                        FilterControlAltText="Filter FullName column" HeaderText="Name" ReadOnly="True" 
                        SortExpression="FullName" UniqueName="FullName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Organization.OrgName" 
                        FilterControlAltText="Filter OrgName column" HeaderText="Facility" 
                        SortExpression="Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Title" 
                        FilterControlAltText="Filter Title column" HeaderText="Title" 
                        SortExpression="Title" UniqueName="Title">
                    </telerik:GridBoundColumn>
                </Columns>

                <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                </EditFormSettings>
                </mastertableview>
                <filtermenu enableimagesprites="False"></filtermenu>
                </telerik:radgrid>

            </asp:WizardStep>

            <asp:WizardStep ID="AttendanceHistoryWizardStep" runat="server" Title="Select Event">
            <asp:Panel ID="EventsPanel" runat="server">
                <asp:Label ID="AttendeeNameLabel" runat="server" Text="Name" Font-Size="Large" ForeColor="#D2912C"></asp:Label><br />
                 <asp:Label ID="AttendeeTitleLabel" runat="server" Text="Title"></asp:Label><br /><br />
            <h3 style="font-weight:bold;">Attendance History</h3>
            <p style="font-size:smaller;">In order to print your certificate, ALL of the following must be true:</p>
            <ul style="font-size:smaller;">
                <li>Your registration must be fully <b>paid</b></li>
                <li>You must have completed the event <b>survey</b></li>
                <li>Today's date must fall in the range of the "Certificate Available" dates</li>
                <li style="color:red;">Note: Your certificate may be more than one page- to view multiple pages, use the toolbar menu at the top of the report</li>
            </ul>

            <telerik:RadGrid id="EventHistoryRadGrid" runat="server" autogeneratecolumns="False" cellspacing="0"
                gridlines="None"
                onitemcommand="RadGrid1_ItemCommand" Width="900px" 
                onitemdatabound="RadGrid1_ItemDataBound">
                <mastertableview DataKeyNames="AssociationId, EventId, AttendeeId">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>

                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>

                        <Columns>
                            <telerik:GridBoundColumn DataField="EventStartDate" DataFormatString="{0:d}" 
                                DataType="System.DateTime" FilterControlAltText="Filter StartDateTime column" 
                                HeaderText="Date" SortExpression="StartDateTime" UniqueName="StartDateTime" ItemStyle-Width="80">
<ItemStyle Width="80px"></ItemStyle>
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="EventName" 
                                FilterControlAltText="Filter EventName column" HeaderText="Event" 
                                SortExpression="EventName" UniqueName="EventName" ItemStyle-Width="300">
<ItemStyle Width="300px"></ItemStyle>
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CeuHours" 
                                FilterControlAltText="Filter CeuHours column" HeaderText="CEUs" 
                                SortExpression="CeuHours" UniqueName="CeuHoursBoundColumn" ItemStyle-Width="50">
<ItemStyle Width="50px"></ItemStyle>
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="CertificateDateRange" 
                                FilterControlAltText="Filter CertificateDateRange column" HeaderText="Certificate Available" 
                                SortExpression="CertificateDateRange" UniqueName="CertificateDateRangeBoundColumn" ItemStyle-Width="150">
<ItemStyle Width="150px"></ItemStyle>
                            </telerik:GridBoundColumn>

                            <telerik:GridTemplateColumn UniqueName="IsPaidTemplateColumn" DataField="IsPaid" HeaderText="Paid" ItemStyle-Width="50">
                              <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToBoolean(Eval("IsPaid")) == true ? "Yes" : "No" %>'></asp:Label>
                                </ItemTemplate>

<ItemStyle Width="50px"></ItemStyle>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn UniqueName="IsSurveyCompleteTemplateColumn" DataField="IsSurveyComplete" HeaderText="Survey" ItemStyle-Width="50">
                              <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "Yes" : "No" %>'></asp:Label>
                                </ItemTemplate>

<ItemStyle Width="50px"></ItemStyle>
                            </telerik:GridTemplateColumn>

                            <telerik:GridHyperLinkColumn UniqueName="SurveyHyperlinkColumn"
                                    HeaderText=""
                                    DataType="System.String"
                                    Text="Take Survey..."
                                    DataNavigateUrlFields="AssociationId, EventId, AttendeeId"
                                    DataNavigateUrlFormatString="Survey.aspx?AssnId={0}&EventId={1}&AttendeeId={2}">
                            </telerik:GridHyperLinkColumn>

                            <%-- <telerik:GridTemplateColumn UniqueName="IsSurveyCompleteTemplateColumn" DataField="IsSurveyComplete" HeaderText="Survey" ItemStyle-Width="50">
                              <ItemTemplate>
                                    <asp:HyperLink ID="IsSurveyCompleteHyperlink" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "" : "Take Survey..." %>'></asp:HyperLink>
                                    <asp:Label ID="IsSurveyCompleteLabel" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "Yes" : "" %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
        --%>
                            <telerik:GridButtonColumn UniqueName="PrintCertGridButtonColumn" CommandName="PrintCertificate" HeaderText="Print" Text="Print Certificate" ItemStyle-Width="150">
<ItemStyle Width="150px"></ItemStyle>
                            </telerik:GridButtonColumn>
                        </Columns>

                    <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                    </EditFormSettings>
            
                </mastertableview>
                <filtermenu enableimagesprites="False"></filtermenu>
            </telerik:RadGrid>

            </asp:Panel>

            </asp:WizardStep>

        </WizardSteps>

     <StartNavigationTemplate>
        <telerik:RadButton ID="NextButton" runat="server" Text="View Your Attendance History"
                    CausesValidation="true"
                    CommandName="MoveNext"  />&nbsp;
    </StartNavigationTemplate>

    <StepNavigationTemplate>
        `<hr style="color:#e99719;width:5px; line-height:5px;" />
        <telerik:RadButton ID="PreviousButton" runat="server" Text="<< Previous"
                    CausesValidation="false" 
                    CommandName="MovePrevious"  />&nbsp;

        <telerik:RadButton ID="NextButton" runat="server" Text="Next >>"
                    CausesValidation="true"
                    CommandName="MoveNext"  />&nbsp;
      
    </StepNavigationTemplate>
    <FinishNavigationTemplate>
        <br />
        <telerik:RadButton ID="PreviousButton" runat="server" Text="<< Previous"
                    CausesValidation="false" 
                    CommandName="MovePrevious"  />
    </FinishNavigationTemplate>
    </asp:Wizard>

    <p>&nbsp;</p>
    <p>&nbsp;</p>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
            onselecting="AttendeesDataSource_Selecting" SelectMethod="GetAttendeesByName" 
            TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
            <asp:Parameter Name="fullName" Type="String" />
        </SelectParameters>
        </asp:ObjectDataSource>

   <%-- <asp:ObjectDataSource ID="EventsDataSource" runat="server"
        SelectMethod="GetCertificatePrintList" 
        TypeName="EventManager.Business.AttendeeMethods" 
        onselecting="EventsDataSource_Selecting" 
        onselected="EventsDataSource_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="AttendeeIdRadTextBox" Name="attendeeId" PropertyName="Text" Type="Int32" DefaultValue="0" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>

    <%--<asp:ObjectDataSource ID="AssociationsDataSource" runat="server" 
            SelectMethod="GetAssociations" 
            TypeName="EventManager.Business.AssociationMethods"></asp:ObjectDataSource>--%>
    
      <%--<asp:ObjectDataSource ID="FacilitiesDataSource" runat="server" 
                SelectMethod="GetOrganizationsByAssociation" 
                TypeName="EventManager.Business.OrganizationMethods">
                 <SelectParameters>
                     <asp:ControlParameter ControlID="AssociationRadComboBox" Name="associationId" 
                         PropertyName="SelectedValue" Type="Int32" />
                 </SelectParameters>
            </asp:ObjectDataSource>--%>

</div><!-- end #content -->
<div class="clear"></div>

</asp:Content>
