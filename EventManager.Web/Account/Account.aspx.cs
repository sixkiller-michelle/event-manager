﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Profile;
using System.Web.Security;

namespace Bariatrak20.Web.Account
{
    public partial class Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get user info
                MembershipUser user = Membership.GetUser();
                if (user != null)
                { 
                    Guid userId = new Guid(user.ProviderUserKey.ToString());
                    EmailTextBox.Text = user.Email;
                    UserNameTextBox.Text = user.UserName;

                    if (!user.IsApproved)
                    {
                        this.EmailNotVerified.Visible = true;
                    }
                    else
                    {
                        this.EmailNotVerified.Visible = false;

                        // See if they are an assn employee
                        EventManager.Business.AssociationEmployeeMethods m = new EventManager.Business.AssociationEmployeeMethods();
                        EventManager.Model.AssociationEmployee emp = m.GetEmployeeByUserId(userId);
                        if (emp != null)
                        {
                            if (!emp.IsActive)
                                this.AccountNotApproved.Visible = true;
                        }
                        else
                        {
                       
                            // See if the person is an unapproved attendee
                            EventManager.Business.AttendeeMethods m3 = new EventManager.Business.AttendeeMethods();
                            EventManager.Model.Attendee att = m3.GetAttendeeByUserId(userId);
                            if (att != null)
                            {
                                if (!att.IsApproved)
                                    this.AccountNotApproved.Visible = true;
                            }
                        }
                    }
                }
            }
        }

        protected void UpdateProfileButton_Click(object sender, EventArgs e)
        {
            //ProfileBase profile = HttpContext.Current.Profile;

            //profile.SetPropertyValue("Registration.FirstName", FirstNameTextBox.Text);
            //profile.SetPropertyValue("Registration.LastName", LastNameTextBox.Text);
        }

    }
}