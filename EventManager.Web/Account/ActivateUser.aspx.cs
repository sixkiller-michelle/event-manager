﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Profile;
using EventManager.Model;
using EventManager.Business;

namespace Bariatrak20.Web.Account
{
    public partial class ActivateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userid = "";
            string verificationType = "";
            int? associationId = 0;
            string assnName = "";
            string facilityName = "";
            string firstName = "";
            string lastName = "";
            string email = "";

            // Get the user ID from the request string
            try
            {
                userid = Request.QueryString["Id"].ToString();
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Default.aspx");
            }
           
            if (userid != null && userid != "")
            {
                try
                {
                    // Validate we have a good userId (that there was no tampering)
                    Guid userGuid = new Guid(userid);

                    // Get the user from membership
                    MembershipUser mu = Membership.GetUser(userGuid);

                    EventManager.Web.Email oEmail = new EventManager.Web.Email();
               
                    // See if the person is requesting  employee access
                    EventManager.Business.AspRegRequestAssociationEmployeeMethods m = new EventManager.Business.AspRegRequestAssociationEmployeeMethods();
                    EventManager.Model.AspRegRequestAssociationEmployee emp = m.GetEmployeeRequestByUserId(userGuid);
                    if (emp != null && emp.AssociationId != null)
                    {
                       ActivateAssociationEmployee(emp);
                       StaffSuccessPanel.Visible = true;

                    }
                    // Person is requesting new association access
                    else if (emp != null && emp.AssociationId == null && emp.NewAssociationId != null)
                    {
                       ActivateNewAssociation(emp.Association.AssociationName, emp.LastName, emp.FirstName, emp.aspnet_Users.aspnet_Membership.Email);
                       AssociationSuccessPanel.Visible = true;
                    }
                    else
                    {
                        // See if the person is requesting facility access
                        EventManager.Business.AspRegRequestFacilityEmployeeMethods m3 = new EventManager.Business.AspRegRequestFacilityEmployeeMethods();
                        EventManager.Model.AspRegRequestFacilityEmployee att = m3.GetEmployeeRequestByUserId(userGuid);
                        if (att != null)
                        {
                           ActivateFacilityEmployee(att);
                           FacilitySuccessPanel.Visible = true;
                        }
                      
                    }

                 
                    if (!mu.IsApproved)
                    {
                        mu.IsApproved = true;
                        Membership.UpdateUser(mu);
                    }
                    
                }
                

                catch (FormatException ex)
                {
                    //thrown by new Guid(userid), the given userid is incorrectly formatted, sign of tampering.
                    FailurePanel.Visible = true;
                }
                catch (ArgumentNullException ex)
                {
                    //thrown by both Membership.GetUser(userGuid) and Membership.UpdateUser(mu)
                    FailurePanel.Visible = true;
                }
                catch (Exception ex)
                {
                    FailurePanel.Visible = true;
                }
            }
            else
            {
                // Provide appropriate user feedback
                FailurePanel.Visible = true;
            }
        }

        private void ActivateNewAssociation(string assnName, string lastName, string firstName, string email)
        {
            EventManager.Web.Email oEmail = new EventManager.Web.Email();

            // Send email to michelle, tom to notify them they need to approve new association
            string bodyText = oEmail.GetBodyText_ApproveAssociation(assnName, firstName, lastName, email);
            oEmail.SendMail("michelle@sixkillersoftware.com", email, "New ceuavenue.com registration (association)", bodyText);
        }

        private void ActivateAssociationEmployee(AspRegRequestAssociationEmployee emp)
        {
             // transfer to AssociationEmployee table
            AssociationEmployeeMethods assnEmpMethods = new AssociationEmployeeMethods();
            AssociationEmployee ae = new AssociationEmployee();
            ae.AssociationId = Convert.ToInt32(emp.AssociationId);
            ae.Email = emp.aspnet_Users.aspnet_Membership.Email;
            ae.FirstName = emp.FirstName;
            ae.IsActive = true;
            ae.LastName = emp.LastName;
            ae.PhoneNumber = emp.PhoneNumber;
            ae.UserId = emp.UserId;
            assnEmpMethods.Add(ae);
            
            // Add to UserAssociation table
            UserAssociationMethods userAssnMethods = new UserAssociationMethods();
            Association assn = userAssnMethods.GetAssociationForUser(emp.UserId);
            if (assn == null)
            { 
                UserAssociation userAssn = new UserAssociation();
                userAssn.UserId = emp.UserId;
                userAssn.AssociationId = ae.AssociationId;
                userAssnMethods.Add(userAssn);
            }

            userAssnMethods.SaveAllObjectChanges();

            string assnName = emp.Association.AssociationName;
                        
            // Send email to association admins to approve registrations
            EventManager.Web.Email oEmail = new EventManager.Web.Email();
            string bodyText = oEmail.GetBodyText_ApproveAssociationEmployee(assnName, ae.FirstName, ae.LastName, ae.Email);
            foreach (EventManager.Model.AssociationEmployee assnEmp in assnEmpMethods.GetEmployeesByAssociationAndRole(Convert.ToInt32(ae.AssociationId), "AssociationAdmin"))
            {
                oEmail.SendMail(assnEmp.aspnet_Users.aspnet_Membership.Email, ae.Email, "New ceuavenue.com registration (staff): " + ae.FirstName + " " + ae.LastName, bodyText);
            }
            oEmail.SendMail("michelle@sixkillersoftware.com", ae.Email, "New ceuavenue.com registration (staff): " + ae.FirstName + " " + ae.LastName, bodyText);

           
        }

        private void ActivateFacilityEmployee(AspRegRequestFacilityEmployee att)
        {
             // transfer to Attendee table
            AttendeeMethods m = new AttendeeMethods();
            Attendee newAtt = new Attendee();
            newAtt.AssociationId = Convert.ToInt32(att.AssociationId);
            newAtt.OrganizationId = att.OrgId;
            newAtt.Title = att.Title;
            newAtt.Email = att.aspnet_Users.aspnet_Membership.Email;
            newAtt.FirstName = att.FirstName;
            newAtt.IsApproved = true;
            newAtt.LastName = att.LastName;
            newAtt.PhoneNumber = att.PhoneNumber;
            newAtt.UserId = att.UserId;
            m.Add(newAtt);

            // Add to UserAssociation table
            UserAssociationMethods userAssnMethods = new UserAssociationMethods();
            Association assn = userAssnMethods.GetAssociationForUser(att.UserId);
            if (assn == null)
            {  
                UserAssociation userAssn = new UserAssociation();
                userAssn.UserId = att.UserId;
                userAssn.AssociationId = newAtt.AssociationId;
                userAssnMethods.Add(userAssn);
            }
           
            m.SaveAllObjectChanges();

            // Grant 'FacilityEmployee" role
            if (!Roles.IsUserInRole(att.aspnet_Users.UserName, "FacilityEmployee"))
                Roles.AddUserToRole(att.aspnet_Users.UserName, "FacilityEmployee");

            // Send email to michelle to notify of facility reg
            EventManager.Web.Email oEmail = new EventManager.Web.Email();
            string bodyText = oEmail.GetBodyText_FacilityRegistration(att.Association.AssociationName, att.OrgId, att.OrgName, att.FirstName, att.LastName, att.aspnet_Users.aspnet_Membership.Email);
            oEmail.SendMail("michelle@sixkillersoftware.com", att.aspnet_Users.aspnet_Membership.Email, "New ceuavenue.com registration (facility)", bodyText);
        }

    }
}