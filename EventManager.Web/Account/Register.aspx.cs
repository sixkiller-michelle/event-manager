﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Profile;
using Telerik.Web.UI;

namespace EventManager.Web.Account
{
    public partial class Register : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];

            if (!IsPostBack)
            {
                RegisterUser.ActiveStepIndex = 0;
                RegTypeRadioButtonList.SelectedValue = "FacilityStaff";
                SetInstructionsForSelectedRegType();
            }
        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {

            string regType = "";
            MembershipUser user = Membership.GetUser(RegisterUser.UserName);
            Guid UserID = new Guid(user.ProviderUserKey.ToString());
            int assnId = 0;

            user.Email = RegisterUser.UserName;
            Membership.UpdateUser(user);

            try
            {
                // Get the create user wizard step
                CreateUserWizardStep createUserStep = RegisterUser.CreateUserStep;

                // Get the registration type (Association or Facility)
                regType = RegTypeRadioButtonList.SelectedValue;

                // If association request, create the user request
                if (regType == "Association" || regType == "AssociationStaff")
                {
                    EventManager.Business.AspRegRequestAssociationEmployeeMethods assnEmpReqMethods = new EventManager.Business.AspRegRequestAssociationEmployeeMethods();
                    EventManager.Model.AspRegRequestAssociationEmployee assnEmpRequest = new EventManager.Model.AspRegRequestAssociationEmployee();
                    assnEmpRequest.UserId = UserID;
                    assnEmpRequest.FirstName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("FirstNameTextBox")).Text;
                    assnEmpRequest.LastName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("LastNameTextBox")).Text;
                    assnEmpRequest.PhoneNumber = ((RadMaskedTextBox)createUserStep.ContentTemplateContainer.FindControl("PhoneTextBox")).Text;
                    assnEmpRequest.RequestDateTime = DateTime.Now;
                    assnEmpRequest.IsApproved = false;

                    // If user is adding a NEW association, create the asssociation request
                    if (regType == "Association")
                    {
                        // Add the association request
                        EventManager.Model.AspRegRequestNewAssociation newAssn = new EventManager.Model.AspRegRequestNewAssociation();
                        newAssn.Address = AssociationStreetTextBox.Text;
                        newAssn.AssociationName = AssociationNameTextBox.Text;
                        newAssn.City = AssociationCityTextBox.Text;
                        newAssn.StateCode = AssociationStateComboBox.SelectedValue;
                        newAssn.Zip = AssociationZipTextBox.Text;
                        newAssn.FaxNumber = AssociationFaxTextBox.Text;
                        newAssn.PhoneNumber = AssociationPhoneTextBox.Text;
                        newAssn.IsApproved = false;
                        newAssn.RequestedByUserId = UserID;
                        newAssn.RequestDateTime = DateTime.Now;

                        assnEmpRequest.AspRegRequestNewAssociation = newAssn;
                    }
                    else
                    {
                        assnId = Convert.ToInt32(AssnDropDownList.SelectedValue);
                        assnEmpRequest.AssociationId = assnId;
                    }

                    assnEmpReqMethods.Add(assnEmpRequest);
                    assnEmpReqMethods.SaveAllObjectChanges();
                }
                else if (regType == "FacilityStaff" && AssnDropDownList.SelectedIndex >= 0)
                {
                    int? orgId = 0;

                    EventManager.Business.AspRegRequestFacilityEmployeeMethods facEmpReqMethods = new EventManager.Business.AspRegRequestFacilityEmployeeMethods();
                    EventManager.Model.AspRegRequestFacilityEmployee facEmpRequest = new EventManager.Model.AspRegRequestFacilityEmployee();
                    facEmpRequest.AssociationId = Convert.ToInt32(AssnDropDownList.SelectedValue);
                    facEmpRequest.UserId = UserID;
                    facEmpRequest.FirstName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("FirstNameTextBox")).Text;
                    facEmpRequest.LastName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("LastNameTextBox")).Text;
                    facEmpRequest.Title = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("TitleTextBox")).Text;
                    facEmpRequest.PhoneNumber = ((RadMaskedTextBox)createUserStep.ContentTemplateContainer.FindControl("PhoneTextBox")).Text;
                    facEmpRequest.RequestDateTime = DateTime.Now;
                    facEmpRequest.IsApproved = false;

                    if (OrgDropDownList.SelectedIndex == 0)
                    {
                        facEmpRequest.OrgId = null;
                        facEmpRequest.OrgName = "";
                    }
                    else if (OrgDropDownList.SelectedIndex == 1)
                    {
                        facEmpRequest.OrgId = null;
                        facEmpRequest.OrgName = OrgNameRadTextBox.Text;
                    }
                    else
                    {
                        facEmpRequest.OrgId = Convert.ToInt32(OrgDropDownList.SelectedValue);
                        facEmpRequest.OrgName = OrgDropDownList.Text;
                    }
                    facEmpReqMethods.Add(facEmpRequest);
                    facEmpReqMethods.SaveAllObjectChanges();
                }

                // Set the UserId cookie
                FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);
            }
            catch (Exception)
            {
                Membership.DeleteUser(user.UserName, true);
            }
        }

        protected void RegisterUser_SendingMail(object sender, MailMessageEventArgs e)
        {
            e.Cancel = true;
            string userName = RegisterUser.UserName;
            string emailAddress = RegisterUser.Email;
            Guid userId = (Guid)Membership.GetUser(userName).ProviderUserKey;
            int assnId = 0;
            string assnName;
            string orgName;
            string firstName;
            string lastName;

            if (RegTypeRadioButtonList.SelectedValue != "Association")
            {
                assnId = Convert.ToInt32(AssnDropDownList.SelectedValue);
                assnName = AssnDropDownList.Text;
            }
            else
            {
                assnName = AssociationNameTextBox.Text;
            }

            // Get the create user wizard step
            CreateUserWizardStep createUserStep = RegisterUser.CreateUserStep;
            orgName = (OrgDropDownList.SelectedIndex == 1 ? OrgNameRadTextBox.Text + " (NEW)" : OrgDropDownList.Text);
            lastName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("LastNameTextBox")).Text;
            firstName = ((RadTextBox)createUserStep.ContentTemplateContainer.FindControl("FirstNameTextBox")).Text;

            // Send email to user to validate email
            Email email = new Email();
            email.SendActivateAccountEmail(assnId, userId, userName, emailAddress);

            // Send notification email to admin
            email.SendNewAccountNotificationToDev(RegTypeRadioButtonList.SelectedValue, assnName, orgName, firstName, lastName, emailAddress);
        }

        protected void OrganizationsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (AssnDropDownList.SelectedIndex > 0)
                e.InputParameters["associationId"] = AssnDropDownList.SelectedValue;
        }

        protected void RegisterUser_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // Select Association
            if (RegisterUser.ActiveStepIndex == 0)
            {
                if (RegTypeRadioButtonList.SelectedIndex == 0) // Select registration type
                    RegisterUser.ActiveStepIndex = 2;
                else
                    RegisterUser.ActiveStepIndex = 1;
            }
            else if (RegisterUser.ActiveStepIndex == 1) // Select association
            {
                if (AssnDropDownList.SelectedIndex == 0)
                {
                    e.Cancel = true;
                    SelectAssociationCustomValidator.ErrorMessage = "Please select an association";
                    SelectAssociationCustomValidator.IsValid = false;
                }
                else
                {
                    if (RegTypeRadioButtonList.SelectedIndex == 1)
                        RegisterUser.ActiveStepIndex = 4;
                    else
                        RegisterUser.ActiveStepIndex = 3;
                }
                
            }
            else if (RegisterUser.ActiveStepIndex == 2) // Enter new association info
            {
                RegisterUser.ActiveStepIndex = 4;
            }
            else if (RegisterUser.ActiveStepIndex == 3) // Select facility
            {
                if (OrgDropDownList.SelectedIndex == 0 && string.IsNullOrWhiteSpace(OrgNameRadTextBox.Text))
                {
                    e.Cancel = true;
                    SelectFacilityCustomValidator.ErrorMessage = "Please select a facility (or the option 'My facility is not listed').";
                    SelectFacilityCustomValidator.IsValid = false;
                }
                else 
                    RegisterUser.ActiveStepIndex = 4;
            }
        }

        private bool AssociationNameExists(string assnName)
        {
            EventManager.Business.AssociationMethods am = new EventManager.Business.AssociationMethods();
            if (am.GetAssociationByName(assnName) != null)
                return true;
            else
                return false;
        }

        private bool FacilityNameExists(int associationId, string facilityName)
        {
            EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
            if (am.GetOrganizationByName(associationId, facilityName) != null)
                return true;
            else
                return false;
        }

        protected void RegisterUser_ActiveStepChanged(object sender, EventArgs e)
        {
            Telerik.Web.UI.RadButton nextButton = (Telerik.Web.UI.RadButton)RegisterUser.ActiveStep.FindControl("NextRadButton");            

            if (RegisterUser.ActiveStepIndex == 3)
            {
                // Reload the facility list (in case user changed associations on the first step)
                //OrgDropDownList.DataBind();
                if (OrgDropDownList.SelectedIndex < 0)
                { 
                    OrgDropDownList.SelectedIndex = 0;
                }
                SetOrgNameVisibility();
            }
            else if (RegisterUser.ActiveStepIndex == 4) // Create user step
            {
                CreateUserWizardStep createUserStep = RegisterUser.CreateUserStep;
                RadTextBox firstNameTextBox = (RadTextBox)createUserStep.ContentTemplateContainer.FindControl("FirstNameTextBox");
                firstNameTextBox.Focus();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void RegisterUser_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            CustomValidator c = (CustomValidator)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("DuplicateUserNameCustomValidator");
            if (c != null)
            {
                c.IsValid = false;
                c.ErrorMessage = "This email is already taken.  Please enter a different one.";
            }
        }

        protected void RegTypeRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetInstructionsForSelectedRegType();
        }

        private void SetInstructionsForSelectedRegType()
        { 
            if (RegTypeRadioButtonList.SelectedValue == "Association")
            {
                NewAssocationInstructPanel.Visible = true;
                AssocationStaffInstructPanel.Visible = false;
                FacilityStaffInstructPanel.Visible = false;
                AssocationStaffInstructPanel2.Visible = false;
                FacilityStaffInstructPanel2.Visible = false;
            }
            else if (RegTypeRadioButtonList.SelectedValue == "AssociationStaff")
            {
                NewAssocationInstructPanel.Visible = false;
                AssocationStaffInstructPanel.Visible = true;
                FacilityStaffInstructPanel.Visible = false;
                AssocationStaffInstructPanel2.Visible = true;
                FacilityStaffInstructPanel2.Visible = false;
            }
            else if (RegTypeRadioButtonList.SelectedValue == "FacilityStaff")
            {
                NewAssocationInstructPanel.Visible = false;
                AssocationStaffInstructPanel.Visible = false;
                FacilityStaffInstructPanel.Visible = true;
                AssocationStaffInstructPanel2.Visible = false;
                FacilityStaffInstructPanel2.Visible = true;
            }
        }

        private void SetOrgNameVisibility()
        {
            if (OrgDropDownList.SelectedIndex == 1)
            {
                OrgNamePanel.Visible = true;
                OrgNameRadTextBox.Focus();
            }
            else
            {
                OrgNamePanel.Visible = false;
            }
        }

        protected void OrgDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SetOrgNameVisibility();
        }

        protected void AssnDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (AssnDropDownList.SelectedIndex > 0)
            {
                OrgDropDownList.Items.Clear();
                OrgDropDownList.DataBind();
            }
                
        }

        protected void OrgDropDownList_DataBound(object sender, EventArgs e)
        {
            OrgDropDownList.Items.Insert(0, new RadComboBoxItem("* My facility is not listed *", ""));
            OrgDropDownList.Items.Insert(0, new RadComboBoxItem("", ""));
        }

        private void Page_Error(object sender, EventArgs e)
        {

            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc is UnauthorizedAccessException)
            {
                
                // Clear the error from the server
                Server.ClearError();

            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

        protected void SendEmailButton_Click(object sender, EventArgs e)
        {
            try
            {
                Email email = new Email();
                email.SendMail("michelle@sixkillersoftware.com", "michelle@sixkillersoftware.com", "Testing smtp email", "This is the body");
            }
            catch (Exception ex)
            { 
                
            }
            
        }

        protected void SendEmailConfigButton_Click(object sender, EventArgs e)
        {
            try
            {
                Email email = new Email();
                email.SendMailUsingConfig("michelle@sixkillersoftware.com", "michelle@sixkillersoftware.com", "Testing smtp email", "This is the body");
            }
            catch (Exception ex)
            {

            }
            
        }

        protected void RegisterUser_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {

        }

        protected void UserName_TextChanged(object sender, EventArgs e)
        {
            // Get the create user wizard step
            CreateUserWizardStep createUserStep = RegisterUser.CreateUserStep;
            RadTextBox userTextBox = (RadTextBox)createUserStep.ContentTemplateContainer.FindControl("UserName");
            RadTextBox emailTextBox = (RadTextBox)createUserStep.ContentTemplateContainer.FindControl("Email");
            if (emailTextBox != null && userTextBox != null)
            {
                emailTextBox.Text = userTextBox.Text;
                RegisterUser.Email = userTextBox.Text;
                RadTextBox pwTextBox = (RadTextBox)createUserStep.ContentTemplateContainer.FindControl("Password");
                pwTextBox.Focus();
            }
        }

        protected void RegisterUser_CreatingUser(object sender, LoginCancelEventArgs e)
        {

        }


    }
}
