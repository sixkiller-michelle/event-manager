﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="EventManager.Web.Account.Register" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .failureNotification {color:Red;}
        .instructions {color:Blue; font-style:italic;}
    </style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    <telerik:RadScriptManager Runat="server"></telerik:RadScriptManager>



   <%-- <telerik:RadButton ID="SendEmailButton" runat="server" Text="Send Email" 
    AutoPostBack="true" onclick="SendEmailButton_Click">
    </telerik:RadButton>

    <telerik:RadButton ID="SendEmailConfigButton" runat="server" Text="Send Email Using Config" 
    AutoPostBack="true" onclick="SendEmailConfigButton_Click">
    </telerik:RadButton>--%>

    <h2>Create a New Account</h2>

    <asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="True" Width="800px"
        OnNextButtonClick="RegisterUser_NextButtonClick" OnFinishButtonClick="RegisterUser_FinishButtonClick" 
        OnActiveStepChanged="RegisterUser_ActiveStepChanged" OnCreatingUser="RegisterUser_CreatingUser"
        OnCreatedUser="RegisterUser_CreatedUser" 
        OnSendingMail="RegisterUser_SendingMail" DisableCreatedUser="true"  
        ForeColor="Black" OnCreateUserError="RegisterUser_CreateUserError" 
        ActiveStepIndex="0" >
        <MailDefinition  Subject="Activate your account" From="noreply@ceuavenue.com" Priority="High" BodyFileName="~/Account/ValidateEmailBody.txt" />
        <WizardSteps>

            <%--ACTIVE STEP INDEX: 0--%>
            <asp:WizardStep ID="WizardStep1" runat="server" StepType="Start">
            
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="failureNotification" ValidationGroup="SelectAssociationValidationGroup"/>

                <h3>What type of account are you registering for?</h3>
                        <table id="Table2" border="0" class="formview-table-layout" style="float:left;">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="RegTypeRadioButtonList" runat="server" AutoPostBack="true" 
                                    TextAlign="Right" RepeatDirection="Vertical" RepeatLayout="Table" 
                                    OnSelectedIndexChanged="RegTypeRadioButtonList_SelectedIndexChanged">
                                    <asp:ListItem Value="Association">&nbsp;&nbsp;New Association</asp:ListItem>
                                    <asp:ListItem Value="AssociationStaff">&nbsp;&nbsp;Association Staff Member</asp:ListItem>
                                    <asp:ListItem Value="FacilityStaff">&nbsp;&nbsp;Attendee</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        </table>
                        <div style="float:left; width:500px;height:150px; margin-left:20px;margin-top:20px;" class="instructions">
                        <asp:Panel runat="server" ID="NewAssocationInstructPanel">
                        Select this option if you are the <b>director or administrator of an association</b> that provides continuing education training to its
                        member and non-member facilities by <b>hosting </b>conventions, workshops, training events, and/or trade shows. By selecting this option,
                        you are requesting more information about purchasing a license to ceuavenue.com in order to manage registration and attendance for 
                        your events.  After completing registration, you will be contacted with further information, including how to sign up for a free trial 
                        of the software.
                        </asp:Panel>
                        <asp:Panel runat="server" ID="AssocationStaffInstructPanel">
                        Select this option if you are a <b>staff member </b>of an association that hosts conventions, workshops, and/or trade shows, and your 
                        association already uses ceuavenue.com to manage your event registrations and attendance.
                        Your registration will need to be approved by an administrator at your association.
                        </asp:Panel>
                        <asp:Panel runat="server" ID="FacilityStaffInstructPanel">
                        Select this option if you <b>attend events to receive continuing education credits</b>.  By registering with ceuavenue.com, you 
                        can manage your profile, view your event attendance history, and print certificates.
                        </asp:Panel>
                        </div>
                
                    <div style="clear:both;"></div>

            </asp:WizardStep>

            <%--ACTIVE STEP INDEX: 1--%>
            <asp:WizardStep ID="SelectAssociationWizardStep" runat="server" StepType="Step">
            
                <asp:ValidationSummary ID="SelectAssociationValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="SelectAssociationValidationGroup"/>
                <asp:CustomValidator ID="SelectAssociationCustomValidator" runat="server" ErrorMessage="" Display="None" ForeColor="Red" ValidationGroup="SelectAssociationValidationGroup"></asp:CustomValidator>
                        <table id="assnTable" border="0" class="formview-table-layout" style="float:left;">
                        <tr>
                            <td>
                                Please select your association from the drop down list:<br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadComboBox ID="AssnDropDownList" runat="server" AllowCustomText="false" AppendDataBoundItems="true"
                                    DataSourceID="AssociationsDataSource" DataTextField="AssociationName" MarkFirstMatch="true"
                                    DataValueField="AssociationId" Width="358px" DropDownWidth="300" AutoPostBack="true" 
                                    OnSelectedIndexChanged="AssnDropDownList_SelectedIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                    </Items>
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        </table>
                    

                        <div style="float:left; margin-left:20px;margin-top:20px; width:200px;" class="instructions">
                            <asp:Panel runat="server" ID="AssocationStaffInstructPanel2">
                            Select the association at which you are employed.
                            </asp:Panel>
                            <asp:Panel runat="server" ID="FacilityStaffInstructPanel2">
                            Select the assocation which usually hosts the events that you attend.
                            </asp:Panel>
                        </div>
                
                    <div style="clear:both;"></div>
                    <br /><br />

            </asp:WizardStep>

            <%--ACTIVE STEP INDEX: 2--%>
            <asp:WizardStep ID="CreateAssociationWizardStep" runat="server" StepType="Step">
                    <asp:ValidationSummary ID="CreateAssociationValidationSummary" runat="server" CssClass="failureNotification" />
                    <asp:CustomValidator ID="CreateAssociationCustomValidator" runat="server" ErrorMessage="" ForeColor="Red" Display="None" ></asp:CustomValidator>

                   <h3>Tell us about your association</h3>
                        <table class="formview-table-layout">
                            <tr>
                                <td align="right"><asp:label runat="server" associatedcontrolid="AssociationNameTextBox" id="Label2" >Association Name:</asp:label></td>
                                <td><telerik:RadTextBox ID="AssociationNameTextBox" runat="server" Text='' Width="300" MaxLength="100" />
                                
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AssociationNameTextBox" 
                                CssClass="failureNotification" ErrorMessage="Association name is required." ToolTip="Association name is required.">*</asp:RequiredFieldValidator>
                                
                                <asp:CustomValidator ID="ExistingAssociationValidator" runat="server" CssClass="failureNotification" Display="Dynamic"     
                                ValidateEmptyText="false"  ControlToValidate="AssociationNameTextBox"  
                                ErrorMessage="An association with this name already exists">*</asp:CustomValidator> </td>
                            </tr>
                            <tr>
					            <td  align="right">Street:</td>
					            <td><telerik:RadTextBox ID="AssociationStreetTextBox" runat="server" Text='' TextMode="MultiLine" Rows="2" MaxLength="100" CausesValidation="true"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="AssociationStreetTextBox" 
                                CssClass="failureNotification" ErrorMessage="Street is required." ToolTip="Street is required.">*</asp:RequiredFieldValidator></td>
				            </tr>
				            <tr>
					            <td align="right">City:</td>
					            <td><telerik:RadTextBox ID="AssociationCityTextBox" runat="server" Text='' MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="AssociationCityTextBox" 
                                CssClass="failureNotification" ErrorMessage="City is required." ToolTip="City is required.">*</asp:RequiredFieldValidator></td>
				            </tr>
				            <tr>
					            <td align="right" >State:</td>
					            <td><telerik:RadComboBox
                                            ID="AssociationStateComboBox" runat="server" DataSourceID="StatesDataSource" 
                                            DataTextField="StateName" DataValueField="StateCode" Width="130" AllowCustomText="false"
                                            DropDownWidth="200">
                                        </telerik:RadComboBox></td>
				            </tr>
				            <tr>
					            <td align="right">Zip:</td>
					            <td><telerik:RadTextBox ID="AssociationZipTextBox" runat="server" Text='' MaxLength="10" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="AssociationZipTextBox" 
                                CssClass="failureNotification" ErrorMessage="Zip code is required." ToolTip="Zip code is required.">*</asp:RequiredFieldValidator></td>
				            </tr>
				            <tr>
					            <td align="right">Phone:</td>
					            <td><telerik:RadMaskedTextBox ID="AssociationPhoneTextBox" runat="server" Mask="(###) ###-####" ></telerik:RadMaskedTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="AssociationPhoneTextBox" 
                                CssClass="failureNotification" ErrorMessage="Phone number is required." ToolTip="Phone number is required.">*</asp:RequiredFieldValidator>
					            </td>
				            </tr>
				            <tr>
					            <td align="right">Fax:</td>
					            <td><telerik:RadMaskedTextBox ID="AssociationFaxTextBox" runat="server" Mask="(###) ###-####" ></telerik:RadMaskedTextBox>
                                </td>
				            </tr>
                        </table>
                    <br />
            </asp:WizardStep>

            <%--ACTIVE STEP INDEX: 3--%>
            <asp:WizardStep ID="SelectFacilityWizardStep" runat="server" StepType="Step">
            
                <asp:ValidationSummary ID="SelectFacilityValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="SelectFacilityValidationGroup"/>
                <asp:CustomValidator ID="SelectFacilityCustomValidator" runat="server" ErrorMessage="" Display="None" ForeColor="Red" ValidationGroup="SelectFacilityValidationGroup"></asp:CustomValidator>
                Please select your facility from the drop down list.  If your facility is not listed, select the option "My facility is not listed" and type the 
                name of your facility in the text box.
               
                    <table id="Table1" border="0" class="formview-table-layout">
                    <tr>
                        <td>
                            <telerik:RadComboBox ID="OrgDropDownList" runat="server"
                                DataSourceID="OrganizationsDataSource" DataTextField="OrgName" AutoPostBack="true"
                                MarkFirstMatch="true" AllowCustomText="false" 
                                DataValueField="Id" Width="400px" 
                                OnSelectedIndexChanged="OrgDropDownList_SelectedIndexChanged" 
                                OnDataBound="OrgDropDownList_DataBound">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <div class="instructions">
                                Select the facility where you are employed.
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel runat="server" ID="OrgNamePanel" style="margin-top:20px;">
                                <div class="instructions" style="margin-top:10px;margin-bottom:10px;">
                                Enter the name of your facility.<br />
                                Note: leave this field blank if you are <b>not employed at a facility</b>, and usually
                                register for events as an individual.
                                </div>
                                <asp:Label runat="server" ID="OrgNameLabel" AssociatedControlID="OrgNameRadTextBox" Text="Facility Name:"  ></asp:Label>
                                <telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Width="400" MaxLength="100"></telerik:RadTextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    </table>
             <br />
            </asp:WizardStep>

            <%--ACTIVE STEP INDEX: 4--%>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep" runat="server" >
           
                <ContentTemplate>
                    <asp:Panel runat="server" ID="CreateUserWizardStepPanel">
                    (Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length).</p>
                    <asp:ValidationSummary ID="CreateUserValidationSummary" runat="server" CssClass="failureNotification" ForeColor="Red" ValidationGroup="CreateUserValidationGroup"/>
                    <asp:CustomValidator ID="DuplicateUserNameCustomValidator" runat="server" ErrorMessage="This username already exists." ForeColor="Red" Display="None" ValidationGroup="CreateUserValidationGroup"></asp:CustomValidator>

                    <div class="accountInfo2" style="width:400px;">

                    <fieldset id="account_fieldset" class="register">
                    <legend>Account Information</legend>
                    <table class="formview-table-layout">
                      <tr>
                        <td align="right"><asp:Label ID="Label3" runat="server" AssociatedControlID="FirstNameTextBox">First Name:</asp:Label></td>
                        <td><telerik:RadTextBox ID="FirstNameTextBox" runat="server" SkinId="LoginTextBox" MaxLength="30"></telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="FirstNameTextBox" 
                                CssClass="failureNotification" ErrorMessage="First name is required." ToolTip="First name is required." 
                                ValidationGroup="CreateUserValidationGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><asp:Label ID="Label4" runat="server" AssociatedControlID="LastNameTextBox">Last Name:</asp:Label></td>
                        <td><telerik:RadTextBox ID="LastNameTextBox" runat="server" SkinId="LoginTextBox" MaxLength="30"></telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="LastNameTextBox" 
                                CssClass="failureNotification" ErrorMessage="Last name is required." ToolTip="Last name is required." 
                                ValidationGroup="CreateUserValidationGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><asp:Label ID="Label5" runat="server" AssociatedControlID="TitleTextBox" MaxLength="50">Title:</asp:Label></td>
                        <td><telerik:RadTextBox ID="TitleTextBox" runat="server" SkinId="LoginTextBox"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
					    <td align="right">Phone:</td>
					    <td><telerik:RadMaskedTextBox ID="PhoneTextBox" runat="server" Mask="(###) ###-####" ></telerik:RadMaskedTextBox>
                            <asp:requiredfieldvalidator runat="server" controltovalidate="PhoneTextBox" tooltip="Phone number is required."
                            id="Requiredfieldvalidator7" validationgroup="CreateUserValidationGroup" errormessage="Phone number is required.">
                            *</asp:requiredfieldvalidator>
					    </td>
				    </tr>
                            <tr>
                                <td align="right" >
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email:</asp:Label>     
                                </td>
                                <td><telerik:RadTextBox ID="UserName" runat="server"  SkinId="LoginTextBox" 
                                        Width="300" MaxLength="100" ontextchanged="UserName_TextChanged" AutoPostBack="true"></telerik:RadTextBox>
                                    <telerik:RadTextBox ID="Email" runat="server" Width="300" Visible="false"></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                         CssClass="failureNotification" ErrorMessage="Email is required." ToolTip="Email is required." 
                                         ValidationGroup="CreateUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr style="margin:3px; padding:0px;" /></td>
                            </tr>
                            <tr>
                                <td align="right"><asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label></td>
                                <td><telerik:RadTextBox ID="Password" runat="server"  SkinId="LoginTextBox" TextMode="Password" MaxLength="50"></telerik:RadTextBox><asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                     CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                                     ValidationGroup="CreateUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label></td>
                                <td><telerik:RadTextBox ID="ConfirmPassword" runat="server"  SkinId="LoginTextBox" TextMode="Password" MaxLength="50"></telerik:RadTextBox>
                                
                                <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic" 
                                     ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server" 
                                     ToolTip="Confirm Password is required." ValidationGroup="CreateUserValidationGroup">*</asp:RequiredFieldValidator>
                                
                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                                     CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                     ValidationGroup="CreateUserValidationGroup">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr style="margin:3px; padding:0px;" /></td>
                            </tr>
                            <tr>
                                <td align="right">
                                  <asp:label runat="server" associatedcontrolid="Question" id="QuestionLabel">
                                    Security Question:</asp:label></td>
                                <td>
                                  <telerik:RadTextBox runat="server" id="Question" Width="300" MaxLength="100"></telerik:RadTextBox>
                                  <asp:requiredfieldvalidator runat="server" controltovalidate="Question" tooltip="Security question is required."
                                    id="QuestionRequired" validationgroup="CreateUserValidationGroup" errormessage="Security question is required.">
                                    *</asp:requiredfieldvalidator>
                                  </td>
                            </tr>
                            <tr>
                            <td align="right">
                              <asp:label runat="server" associatedcontrolid="Answer" id="AnswerLabel">Security Answer:</asp:label></td>
                            <td>
                              <telerik:RadTextBox runat="server" id="Answer" MaxLength="100" ></telerik:RadTextBox>
                              <asp:requiredfieldvalidator runat="server" controltovalidate="Answer" tooltip="Security answer is required."
                                    id="AnswerRequired" validationgroup="CreateUserValidationGroup" errormessage="Security answer is required.">*</asp:requiredfieldvalidator>
                            </td>
                          </tr>
                            </table>
                        </fieldset>
                        
                    </div>
                    <div class="accountInfo" style="float:left; width:500px; margin-left:20px;">
                        
                        
                    </div>
                   </asp:Panel>
                </ContentTemplate>
                <CustomNavigationTemplate>
                    <div style="margin: 1em 1em; float:left;">
                        <telerik:RadButton ID="StepPreviousButton" runat="server" Text="<< Previous" CommandName="MovePrevious" />&nbsp;
                        <telerik:RadButton ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Complete Registration" CausesValidation="true" UseSubmitBehavior="true"
                                 ValidationGroup="CreateUserValidationGroup" />
                    </div>
                </CustomNavigationTemplate>

            </asp:CreateUserWizardStep>

            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" >
                <ContentTemplate>
                Thank you for registering with CEU Avenue! 
                <h2 style="color:Red;">&nbsp;</h2>
                <h2 style="color:Red;">Important Next Step</h2>
                <h3>Email Verification</h3>
                <p>Before your account can be created, we need to verify your email address. An 
                    email has been sent to the address you provided. <em>Click on the link contained 
                    in the email to complete your account registration.</em>  </p>
                </ContentTemplate>
             </asp:CompleteWizardStep>

        </WizardSteps>

        <StartNavigationTemplate>
            <div style="float:left;">
            <telerik:RadButton ID="NextRadButton" runat="server" Text="Next >>"
                         CausesValidation="true"
                         CommandName="MoveNext"  />&nbsp;

                <telerik:RadButton ID="CancelRadButton" runat="server" Text="Cancel"
                         CausesValidation="false" 
                         OnClientClick="return confirm('Are you sure you want to cancel');" 
                         OnClick="btnCancel_Click"   /></div>
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <div style="float:left;">
             <telerik:RadButton ID="PreviousRadButton" runat="server" Text="<< Previous"
                         CausesValidation="false"  
                         CommandName="MovePrevious"  />&nbsp;

                <telerik:RadButton ID="NextRadButton" runat="server" Text="Next >>"
                         CausesValidation="true"
                         CommandName="MoveNext"  />&nbsp;

                <telerik:RadButton ID="CancelRadButton" runat="server" Text="Cancel"
                         CausesValidation="false" 
                         OnClientClick="return confirm('Are you sure you want to cancel');" 
                         OnClick="btnCancel_Click"   /></div>
        </StepNavigationTemplate>
        <FinishNavigationTemplate>
        
        </FinishNavigationTemplate>

    </asp:CreateUserWizard>
    <br /><br />

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" 
        SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AssociationsDataSource" runat="server" 
        SelectMethod="GetAssociations" 
        TypeName="EventManager.Business.AssociationMethods"></asp:ObjectDataSource>

    <asp:ObjectDataSource ID="OrganizationsDataSource" runat="server" 
        onselecting="OrganizationsDataSource_Selecting" 
        SelectMethod="GetOrganizationsByAssociation" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
