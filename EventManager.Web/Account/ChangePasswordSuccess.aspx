﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="EventManager.Web.Account.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="MenuContent" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<telerik:RadScriptManager ID="ScriptManager1" runat="server" />
    <h2>
        Change Password
    </h2>
    <p style="color:Green; font-weight:bold;">
        Your password has been changed successfully.
    </p>
    <p>
        Back to <telerik:RadButton runat="server" ID="DashboardRadButton" Text="Dashboard" AutoPostBack="true" PostBackUrl="~/Association/Dashboard.aspx"></telerik:RadButton>
    </p>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
</asp:Content>
