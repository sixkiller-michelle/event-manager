﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);


        }

        public void LoginUser_OnLoggedIn(object sender, EventArgs e)
        {
            MembershipUser u = Membership.GetUser(LoginUser.UserName);
            Guid userId = new Guid(u.ProviderUserKey.ToString());

            //Check first if user is association employee
            EventManager.Business.AssociationEmployeeMethods em = new EventManager.Business.AssociationEmployeeMethods();
            EventManager.Model.AssociationEmployee emp = em.GetEmployeeByUserId(userId);

            if (emp != null)
            {
                if (emp.IsActive)
                {
                    if (Request.QueryString["ReturnURL"] != null)
                        Response.Redirect(Request.QueryString["ReturnURL"].ToString(), true);
                    else
                        Response.Redirect("~/Association/Dashboard.aspx");
                }
            }
            else
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee att = am.GetAttendeeByUserId(userId);

                if (att != null)
                {
                    if (Request.QueryString["ReturnURL"] != null)
                        Response.Redirect(Request.QueryString["ReturnURL"]);
                    else
                        Response.Redirect("~/Facility/Events/EventList.aspx");
                }
                else
                {
                    Literal failureText = (Literal)LoginUser.FindControl("FailureText");
                    failureText.Text = "An attendee profile cannot be found for user " + u.UserName;
                }
            }

        }

        private void Page_Error(object sender, EventArgs e)
        {

            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc is UnauthorizedAccessException)
            {
                errorMsg = exc.Message;

                LoginUser.FailureText = errorMsg;
                LoginUser.FailureAction = LoginFailureAction.Refresh;

                // Log the exception and notify system operators
                //ExceptionUtility.LogException(exc, "Login.aspx");
                ExceptionUtility.NotifySystemOps(exc);

                // Clear the error from the server
                Server.ClearError();

            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

        protected void LoginUser_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            MembershipUser u = Membership.GetUser(LoginUser.UserName);
            
            if (u != null)
            { 
                Guid userId = new Guid(u.ProviderUserKey.ToString());
                string failureText = "";
                Literal failureControl = (Literal)LoginUser.FindControl("FailureText");

                //Check first if user is association employee
                EventManager.Business.AssociationEmployeeMethods em = new EventManager.Business.AssociationEmployeeMethods();
                EventManager.Model.AssociationEmployee emp = em.GetEmployeeByUserId(userId);

                if (emp != null)
                {
                    if (!emp.IsActive)
                    {
                        failureText = "The association account for '" + emp.FirstName + " " + emp.LastName + "' is not active.";
                    }
                }
                else // Check if user is facility employee (in Attendee table with UserId)
                {
                    EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                    EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

                    if (a == null)
                    {
                        failureText = "An application profile for '" + emp.FirstName + " " + emp.LastName + "' could not be found.";
                    }
                }

                if (failureText != "")
                {
                    failureControl.Text = failureText;
                    e.Cancel = true;

                    // Send notification to dev
                    UnauthorizedAccessException ex = new UnauthorizedAccessException(failureText);
                    ExceptionUtility.NotifySystemOps(ex, "Login.aspx");
                }

            }
            
           
        }

        protected void LoginUser_LoginError(object sender, EventArgs e)
        {

        }
    }
}
