﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeBehind="ActivateUser.aspx.cs" Inherits="Bariatrak20.Web.Account.ActivateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
.success
{ color:Green;
  font-size:14px;
  font-weight:bold;}
.failure
{ color:red;
  font-size:14px;
  font-weight:bold;}
</style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <asp:Panel ID="StaffSuccessPanel" runat="server" Visible="false">
       <asp:Label ID="Label1" runat="server" Text="Your email has been verified.  Thank you for your request for access to Event Manager on ceuavenue.com" CssClass="success"></asp:Label><br /><br />
       <asp:Label ID="Label5" runat="server" Text="Your request has been sent to your association's admin for review.  You will be notified via email once your request has been approved."></asp:Label>
    </asp:Panel>
    <asp:Panel ID="AssociationSuccessPanel" runat="server" Visible="false">
       <asp:Label ID="Label3" runat="server" Text="Your email has been verified.  Thank you for your request for access to Event Manager on ceuavenue.com" CssClass="success"></asp:Label><br /><br />
       <asp:Label ID="Label4" runat="server" Text="Your request has been sent to IHCA and Sixkiller Software for review.  You will be notified via email once your request has been approved."></asp:Label>
    </asp:Panel>
    <asp:Panel ID="FacilitySuccessPanel" runat="server" Visible="false">
       <asp:Label ID="Label6" runat="server" Text="Your email has been verified.  Thank you for registering with ceuavenue.com" CssClass="success"></asp:Label><br /><br />
       <p>You can <asp:HyperLink ID="LoginHyperLink" runat="server" NavigateUrl="~/account/login.aspx">LOGIN</asp:HyperLink> to register for events or view your past attendance history.</p>
        
    </asp:Panel>
    <asp:Panel ID="FailurePanel" runat="server" Visible="false">
        <asp:Label ID="Label2" runat="server" Text="I'm sorry, but there was a problem activating your account." CssClass="failure"></asp:Label>
    </asp:Panel>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    
</asp:Content>
