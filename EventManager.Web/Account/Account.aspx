﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="Bariatrak20.Web.Account.Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">

<div id="content" class="box">
	<h2 class="page-title">Account</h2>
	<div class="breadcrumb"><a href="../Default.aspx" id="home">Home</a>  »  <a href="#">Account</a></div>
	<p>&nbsp;</p>

    <table style="vertical-align:top;">
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="User Name:"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="UserNameTextBox" ReadOnly="true" Width="300" Style="background-color:#cccccc;" /></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Email:"></asp:Label></td>
            <td><asp:TextBox runat="server" ID="EmailTextBox" ReadOnly="true" Width="300" Style="background-color:#cccccc;"   /></td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Label ID="EmailNotVerified" runat="server" Text="Your email address is not verified." Visible="false" CssClass="failureNotification" /><br />
            <asp:Label ID="AccountNotApproved" runat="server" Text="Your account has not yet been approved." Visible="false" CssClass="failureNotification" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Account/ChangePassword.aspx" Text="Change your password"></asp:HyperLink>
            </td>
        </tr>
    </table>
    <%--<asp:Button runat="server" ID="UpdateProfileButton" 
                Text="Save Preferences" 
                OnClick="UpdateProfileButton_Click" />--%>

</div><!-- end #content -->
<br />

</asp:Content>
