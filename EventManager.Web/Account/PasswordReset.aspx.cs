﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace EventManager.Web.Account
{
    public partial class PasswordResetForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RecoverPwd_SendingMail_Old(object sender, MailMessageEventArgs e)
        {
            e.Message.CC.Add("michelle@sixkillersoftware.com");
        }

        protected void RecoverPwd_SendingMail(object sender, MailMessageEventArgs e)
        {
            //MailMessage mm = new MailMessage();
            //mm.CC.Add("michelle@sixkillersoftware.com");
            //mm.From = e.Message.From;
            //mm.Subject = e.Message.Subject.ToString();
            //mm.To.Add(e.Message.To[0]);

            //mm.Body = e.Message.Body;
            //SmtpClient smtp = new SmtpClient();
            //smtp.EnableSsl = true;

            //smtp.Send(mm);

            EventManager.Web.Email mail = new EventManager.Web.Email();
            mail.SendMail(e.Message.To[0].Address, "admin@ceuavenue.com", "CEUAVENUE.COM Password Reset", e.Message.Body);
            e.Cancel = true; 
        }
    }
}