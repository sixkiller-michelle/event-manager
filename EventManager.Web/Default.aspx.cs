﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetViewForAuthenticatedUser();
        }

        private void SetViewForAuthenticatedUser()
        { 
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                EventManager.Business.AssociationEmployeeMethods aem = new EventManager.Business.AssociationEmployeeMethods();
                EventManager.Model.AssociationEmployee emp = aem.GetEmployeeByUserId(new Guid(user.ProviderUserKey.ToString()));
                if (emp != null)
                {
                    AssociationNameLabel.Text = emp.Association.AssociationName;
                    UserNameLabel.Text = "Welcome, " + emp.FirstName + "!";
                }
                else
                {
                    EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                    EventManager.Model.Attendee a = am.GetAttendeeByUserId(new Guid(user.ProviderUserKey.ToString()));
                    if (a != null)
                    {
                        AssociationNameLabel.Text = a.Association.AssociationName;
                        UserNameLabel.Text = "Welcome, " + a.FirstName + "!";
                        DashboardHyperLink.NavigateUrl = "~/Facility/Events/EventList.aspx";
                        DashboardHyperLink.Text = "Upcoming Events";
                    }
                }

                loginbox.Style.Value = "display:none;";
                associationbox.Style.Value = "display:block;";
            }
        }

        private void NavigateToDefaultPage(string username)
        {
            MembershipUser u = Membership.GetUser(username);
            Guid userId = new Guid(u.ProviderUserKey.ToString());

            EventManager.Business.UserAssociationMethods uam = new EventManager.Business.UserAssociationMethods();

            Session["UserId"] = userId;
            Session["AssociationId"] = uam.GetAssociationIdForUser(userId);

            //Check first if user is association employee
            EventManager.Business.AssociationEmployeeMethods em = new EventManager.Business.AssociationEmployeeMethods();
            EventManager.Model.AssociationEmployee emp = em.GetEmployeeByUserId(userId);
            if (emp != null)
            {
                if (Request.QueryString["ReturnURL"] != null)
                    Response.Redirect(Request.QueryString["ReturnURL"].ToString(), true);
                else
                    Response.Redirect("~/Association/Dashboard.aspx");
            }
            else
            {
                if (Request.QueryString["ReturnURL"] != null)
                    Response.Redirect(Request.QueryString["ReturnURL"]);
                else
                    Response.Redirect("~/Default.aspx");
            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            string userName = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;
            if (Membership.ValidateUser(userName, password))
            {
                MembershipUser user = Membership.GetUser(userName);
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(user.UserName, true);
                    NavigateToDefaultPage(userName);
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Register.aspx");
        }

    }
}
