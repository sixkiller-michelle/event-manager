﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace Inclusion.Web
{
    public partial class Error404Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                LoginPanel.Visible = false;
                ProfilePanel.Visible = true;

                if (Roles.IsUserInRole("Permbooks"))
                    PermbooksPanel.Visible = true;
                if (Roles.IsUserInRole("HR"))
                    HrPanel.Visible = true;
                if (Roles.IsUserInRole("Billing"))
                    BillingPanel.Visible = true;
            }
            else
            {
                LoginPanel.Visible = true;
                ProfilePanel.Visible = false;
                PermbooksPanel.Visible = false;
                HrPanel.Visible = false;
                BillingPanel.Visible = false;
            }
        }
    }
}