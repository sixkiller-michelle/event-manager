﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web.Association
{
    public partial class AssociationMaster3 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetSelectedMenuItem();
        }

        protected void SetSelectedMenuItem()
        {
            Menu mainMenu = this.MainMenu;

            if (Request.ServerVariables["Path_Info"].Contains("Association/Dashboard"))
                mainMenu.Items[1].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Event") || Request.ServerVariables["Path_Info"].Contains("/Association/NewEvent") || Request.ServerVariables["Path_Info"].Contains("/Association/EditEvent"))
                mainMenu.Items[2].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Org"))
                mainMenu.Items[3].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Attendee"))
                mainMenu.Items[4].Selected = true;
        }

        public Guid UserId
        {
            get
            {
                Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                return g;
            }
        }

        public int AssociationId
        {
            get
            {
                EventManager.Business.AssociationEmployeeMethods ef = new EventManager.Business.AssociationEmployeeMethods();
                return ef.GetEmployeeByUserId(this.UserId).AssociationId;
            }
        }
    }
}