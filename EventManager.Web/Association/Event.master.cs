﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class Event : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
             

                // Add EventId to all links (1st and second level)
                if (Request.QueryString["EventId"] != "" && Request.QueryString["EventId"] != null) // check to see if querystring exists
                {
                    foreach (Telerik.Web.UI.RadMenuItem m in EventRadMenu.Items)
                    {
                        if (m.NavigateUrl != "")
                        {
                            m.NavigateUrl = m.NavigateUrl + "?EventId=" + Request.QueryString["EventId"];
                        }
                        foreach (Telerik.Web.UI.RadMenuItem m2 in m.Items)
                        {
                            m2.NavigateUrl = m2.NavigateUrl + "?EventId=" + Request.QueryString["EventId"];
                        }
                    }
                    HyperLink hyperlink1 = (HyperLink)RadPane1.FindControl("SpeakersReportHyperLink");
                    hyperlink1.NavigateUrl = hyperlink1.NavigateUrl + Request.QueryString["EventId"];
                    HyperLink hyperlink2 = (HyperLink)RadPane1.FindControl("EventSurveyResultsHyperLink");
                    hyperlink2.NavigateUrl = hyperlink2.NavigateUrl + Request.QueryString["EventId"];
                }

                EventRadMenu.ClickToOpen = false;
                EventRadMenu.Flow = (ItemFlow)Enum.Parse(typeof(ItemFlow), "Vertical", true);

                SetSelectedEventDetails();
                SetSelectedMenuItem();
            }
        }

        private void SetSelectedMenuItem()
        {
            //<telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventList.aspx" Text="Event List" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventOverview.aspx" Text="Overview" Value="Overview" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EditEvent.aspx" Text="Details" Value="Event Details" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDates.aspx" Text="Dates" Value="Dates" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDesc.aspx" Text="Description" Value="Description" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventRegInstruct.aspx" Text="Registration Instructions" Value="Reg Instructions" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDocuments.aspx" Text="Documents" Value="Documents" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventSessions.aspx" Text="Sessions" Value="Sessions" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventReg.aspx" Text="Registrations" Value="Registrations" Flow="Vertical">
            //    <GroupSettings ExpandDirection="Down"></GroupSettings>
            //    <Items>
            //        <telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventReg.aspx" Text="Facility Summary">
            //        </telerik:RadMenuItem>
            //        <telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventRegAttendees.aspx" Text="Attendees">
            //        </telerik:RadMenuItem>
            //    </Items>
            //    <groupsettings flow="Horizontal" repeatdirection="Horizontal" />
            //</telerik:RadMenuItem>
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventFlags.aspx" Text="Flags" Value="Flags" />

            if (Request.ServerVariables["Path_Info"].Contains("/Association/EventList.aspx"))
                this.EventRadMenu.Items[0].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventOverview") ||
                    Request.ServerVariables["Path_Info"].Contains("/Association/EditEvent") || 
                    Request.ServerVariables["Path_Info"].Contains("/Association/EventDates") ||
                    Request.ServerVariables["Path_Info"].Contains("/Association/EventDesc") ||
                    Request.ServerVariables["Path_Info"].Contains("/Association/EventRegInstruct") ||
                    Request.ServerVariables["Path_Info"].Contains("/Association/EventDocuments"))
                this.EventRadMenu.Items[1].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventSessions"))
                this.EventRadMenu.Items[2].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventFlags"))
                this.EventRadMenu.Items[3].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventRegAttendees2.aspx"))
                this.EventRadMenu.Items[4].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventReg.aspx"))
                this.EventRadMenu.Items[5].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventAttendance.aspx"))
                this.EventRadMenu.Items[6].Selected = true;
        }

        private void SetSelectedEventDetails()
        {
            if (Request.QueryString["EventId"] != null)
            {
                int eventID = int.Parse(Request.QueryString["EventId"]);

                EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
                EventManager.Model.Event e = em.GetEvent(eventID);
                HeaderEventNameLabel.Text = e.EventName;
                EventTypeLabel.Text = e.EventType.TypeName;
                EventStartDateLabel.Text = e.StartDateTime.ToShortDateString() + " " + e.StartDateTime.ToShortTimeString();
                EventStatusLabel.Text = e.EventStatu.StatusDesc;
                EventVisibilityLabel.Text = e.Visibility;
               
            }
        }
    }
}