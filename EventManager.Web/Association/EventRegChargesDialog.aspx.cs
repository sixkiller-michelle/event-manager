﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventRegChargesDialog : System.Web.UI.Page
    {
        public string RadGrid1PanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }   
	
	    protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
	    {
		    if (e.Updated.ID == "RadGrid1")
		    {
			    this.RadGrid1PanelClientID = e.UpdatePanel.ClientID;
		    }
	    }
        

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int regId = Convert.ToInt32(Request.QueryString["RegId"]);
                decimal chargeTotal = 0;

                // Get the registration record
                RegistrationMethods m1 = new RegistrationMethods();
                //chargeTotal = m1.GetChargeTotal(regId);
                chargeTotal = m1.GetCalculatedChargeTotal(regId);

            }
            catch (Exception ex)
            {
                //CustomValidator1.ErrorMessage = ex.Message;
                //CustomValidator1.IsValid = false;
            }
        }

        protected void MasterTableView_DataBinding(object sender, EventArgs e)
        {
            GridNumericColumn chargeAmtCol = RadGrid1.MasterTableView.GetColumnSafe("ChargeAmt") as GridNumericColumn;
            chargeAmtCol.FooterAggregateFormatString = chargeAmtCol.Aggregate.ToString() + ": {0:C}";
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);

            RegistrationChargeMethods m = new RegistrationChargeMethods();
            RadGrid1.DataSource = m.GetChargesByRegistration(regId);
        }

        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;

            try
            {
                RegistrationChargeMethods m = new RegistrationChargeMethods();
                RegistrationCharge ch = new RegistrationCharge();
                ch.RegistrationId = Convert.ToInt32(Request.QueryString["RegId"]);
                item.UpdateValues(ch);

                m.Add(ch);
                m.SaveAllObjectChanges();
                RadGrid1.Rebind();
              
            }
            catch (ArgumentException ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.InnerException.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int regChargeId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            RegistrationChargeMethods am = new RegistrationChargeMethods();
            RegistrationCharge ch = am.GetRegistrationCharge(regChargeId);

            item.UpdateValues(ch);
            am.SaveAllObjectChanges();
            RadGrid1.Rebind();

            // Hide all edit items
            RadGrid1.MasterTableView.ClearEditItems();
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int regChargeId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            EventManager.Business.RegistrationChargeMethods am = new EventManager.Business.RegistrationChargeMethods();

            RegistrationCharge ch = am.GetRegistrationCharge(regChargeId);
            am.Delete(ch);
            am.SaveAllObjectChanges();

            RadGrid1.Rebind();
        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                RadGrid1.MasterTableView.ClearEditItems();
              
                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert  
                newValues["RegistrationId"] = Convert.ToInt32(Request.QueryString["RegId"]);
                newValues["ChargeAmt"] = 0;
                newValues["ChargeDesc"] = "";
                newValues["ChargeDate"] = DateTime.Now;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);
            }
            else if (e.CommandName == RadGrid.EditCommandName)
            {
                e.Item.OwnerTableView.IsItemInserted = false;
            }
        }

    }
}