﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventRegAttendeesForm : System.Web.UI.Page
    {
        bool isPdfExport = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

            if (!IsPostBack)
            {
                ActionsRadMenu.Items[0].Text = "Event Registrations - Attendees";
                NewRegPanel.Style["display"] = "none";

                LinkButton newAttendeeButton = NewAttendeeLinkButton;
                if (newAttendeeButton != null)
                    newAttendeeButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}');", "0", "-1");

                LinkButton newOrgButton = NewOrgLinkButton;
                if (newOrgButton != null)
                    newOrgButton.Attributes["onclick"] = String.Format("return ShowOrgDialog('{0}');", "0");

                // Get the "Single Badge" DYMO file and save the text to hidden field
                //string filePath = Server.MapPath("~/Association/Documents/Attendee Single Badge.label");
                //StreamReader sr = File.OpenText(filePath);
                //SingleBadgeTextBox.Text = sr.ReadToEnd();
                //sr.Close();
            }
            else
            {
                Control ctl = CommonWebUtils.GetPostBackControl(Page);
                if (ctl != null)
                {
                    if (ctl.ID != null)
                        if (!(ctl.ID.Contains("NewRegAddButton") || ctl.ID == "NewAttendeeRadComboBox"))
                        {
                            NewRegPanel.Style["display"] = "none";

                            if (ctl.ID == "NewAttendeeRadComboBox")
                            {
                                //NewRegFacilityRadComboBox.Focus();
                            }
                        }
                }
            }
        }

        protected void RegistrationsRadGrid_Init(object sender, EventArgs e)
        {
            GridFilterMenu menu = RegistrationsRadGrid.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "NoFilter" || menu.Items[i].Text == "Contains" || menu.Items[i].Text == "StartsWith" || menu.Items[i].Text == "EqualTo" || menu.Items[i].Text == "GreaterThan" || menu.Items[i].Text == "LessThan")
                {
                    i++;
                }
                else
                {
                    menu.Items.RemoveAt(i);
                }
            }

        }

        protected void RegistrationsRadGrid_ItemDeleted(object sender, GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                { 
                    EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
                    m.SaveAllObjectChanges();
                    RegistrationsRadGrid.DataBind();
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }
                
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void RegistrationsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem)
            {
                if (e.Item.IsInEditMode)
                {
                    int regAttendeeId = ((EventManager.Model.RegistrationAttendee)e.Item.DataItem).Id;

                    // Get all flags for the selected attendee
                    EventManager.Business.RegistrationAttendeeFlags ram = new EventManager.Business.RegistrationAttendeeFlags();
                    foreach (EventManager.Model.RegistrationAttendeeFlag af in ram.GetRegisteredAttendeeFlags(regAttendeeId))
                    {
                        // find the matching item in check box list and check it
                        CheckBoxList flagCheckBoxList = (CheckBoxList)e.Item.FindControl("AttendeeFlagsCheckBoxList");
                        if (flagCheckBoxList != null)
                        {
                            foreach (ListItem checkBox in flagCheckBoxList.Items)
                            {
                                if (Convert.ToInt32(checkBox.Value) == af.EventFlagId)
                                    checkBox.Selected = true;
                            }
                        }

                    }

                }
            }
            
        }

        protected void RegistrationsRadGrid_ItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    int regAttendeeId = Convert.ToInt32((e.Item as GridEditableItem).GetDataKeyValue("Id"));
                    EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
                    EventManager.Business.RegistrationAttendeeFlags m2 = new EventManager.Business.RegistrationAttendeeFlags();

                    CheckBoxList flagCheckBoxList = (CheckBoxList)e.Item.FindControl("AttendeeFlagsCheckBoxList");
                    m2.DeleteAllFlags(regAttendeeId);
                    m.SaveAllObjectChanges();

                    foreach (ListItem checkbox in flagCheckBoxList.Items)
                    {
                        if (checkbox.Selected)
                        {
                            m2.Add(regAttendeeId, Convert.ToInt32(checkbox.Value));
                        }
                    }

                    m.SaveAllObjectChanges();
                    RegistrationsRadGrid.DataBind();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    }
                    else
                    { 
                        CustomValidator1.ErrorMessage = ex.Message;
                    } 
                    CustomValidator1.IsValid = false;
                    e.KeepInEditMode = true;
                }

            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void PrintRadButton_Click(object sender, EventArgs e)
        {
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);

                Response.Redirect("~/Reports/ReportViewer.aspx?FileName=AttendeeList.rpt&ParmNames=EventName|SortField|@EventId&ParmValues=2012 Spring Workshop|Organization|" + eventId);
        }

        protected void PrintButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            string eventName = "2012 Spring Workshop";
            string sortOrder = "";

            RadioButtonList sortOrderList = (RadioButtonList)((LinkButton)sender).NamingContainer.FindControl("SortOrderRadioButtonList");
            sortOrder = sortOrderList.SelectedValue;

            Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=AttendeeList.rpt&ParmNames=EventName|SortField|@EventId&ParmValues=" + eventName + "|" + sortOrder + "|" + eventId);
        }

        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                //EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                //IQueryable<EventManager.Model.Attendee> query =

                //    from att in ctx.Attendees
                //    where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text)
                //    orderby att.LastName, att.FirstName
                //    select att;

                //if (query.Count() > 0)
                if (Session["AttendeesList"] == null)
                { 
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    Session["AttendeesList"] = m.GetAttendees(assnId);
                }
                List<EventManager.Model.Attendee> attendees = ((List<EventManager.Model.Attendee>)Session["AttendeesList"]).Where(l => (l.LastName + ", " + l.FirstName).StartsWith(e.Text, true, null)).OrderBy(l => l.LastName).ThenBy(l => l.FirstName).ToList();
                if (attendees.Count > 0)
                {
                    NewAttendeeRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Attendee a in attendees)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                        NewAttendeeRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewAttendeeRadComboBox.ClearSelection();
                }

            }

        }

        protected void NewAttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.Text))
            {
                try
                {
                    if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                    {
                        //NewAttendeePanel.Visible = false;
                        int attendeeId = Convert.ToInt32(e.Value);

                        // Get the selected attendee from drop down
                        EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                        EventManager.Model.Attendee a = am.GetAttendee(attendeeId);

                        // Pre-fill their org and title (if available)
                        if (a.Organization != null && !String.IsNullOrEmpty(a.Organization.OrgName))
                        {
                            NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Text = "";
                            RadComboBoxItem item = new RadComboBoxItem(a.Organization.OrgName, a.Organization.Id.ToString());
                            NewRegFacilityRadComboBox.Items.Insert(0, item);
                            NewRegFacilityRadComboBox.SelectedIndex = 0;
                            NewRegFacilityRadComboBox.Focus();
                            //NewFacilityPanel.Visible = false;
                        }
                        else
                        {
                            NewRegFacilityRadComboBox.Text = "";
                            NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Focus();
                            //NewFacilityPanel.Visible = true;
                        }

                        NewRegTitleTextBox.Text = a.Title.Trim();
                    }
                    else
                    {
                        NewRegFacilityRadComboBox.Items.Clear();
                        NewRegFacilityRadComboBox.Text = "";
                        NewRegTitleTextBox.Text = "";
                        NewRegFacilityRadComboBox.Focus();
                    }

                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }

            }
            else
            {
                //NewAttendeePanel.Visible = false;
            }
        }

        protected void NewRegFacilityRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                //EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                //IQueryable<EventManager.Model.Attendee> query =

                //    from att in ctx.Attendees
                //    where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text)
                //    orderby att.LastName, att.FirstName
                //    select att;

                //if (query.Count() > 0)
                List<EventManager.Model.Organization> orgs = ((List<EventManager.Model.Organization>)Session["OrganizationsList"]).Where(l => l.OrgName.StartsWith(e.Text)).ToList();
                if (orgs.Count > 0)
                {
                    NewRegFacilityRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Organization a in orgs)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.OrgName, a.Id.ToString());
                        NewRegFacilityRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewRegFacilityRadComboBox.ClearSelection();
                }

            }
        }

        protected void NewRegFacilityRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.Text))
            {
                try
                {
                    if (String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        //NewFacilityPanel.Visible = true;
                        //NewFacilityNameTextBox.Text = NewRegFacilityRadComboBox.Text.Trim();
                    }
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }

            }
        }

        private string GetLastName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0)
            {
                n = fullName.Substring(0, fullName.IndexOf(","));
            }
            return n.Trim();
        }

        private string GetFirstName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0 && fullName.IndexOf(",") < fullName.Length - 1)
            {
                n = fullName.Substring(fullName.IndexOf(",") + 1);
            }
            return n;
        }

        protected void NewRegAddButton_Click(object sender, EventArgs e)
        {
            try
            {
                int assnId = UserInfo.AssociationId;
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                Guid userId = UserInfo.UserId;
                EventManager.Model.Organization org = null;
                EventManager.Model.Attendee att = null;
                string title = "";

                EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                
                // Find the attendee (or create, if one is not selected)
                if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                {
                    att = am.GetAttendee(Convert.ToInt32(NewAttendeeRadComboBox.SelectedValue));
                }
                else if (NewAttendeeRadComboBox.Text.Trim().Length > 3)
                {
                    List<Attendee> matchingAttendees = am.GetAttendeesByName(assnId, NewAttendeeRadComboBox.Text.Trim());
                    if (matchingAttendees.Count > 0)
                        att = matchingAttendees[0];

                    if (att == null)
                    { 
                        string firstName = GetFirstName(NewAttendeeRadComboBox.Text);
                        string lastName = GetLastName(NewAttendeeRadComboBox.Text);
                        att = am.AddAttendee(firstName, lastName, "", assnId);
                        // Refresh session 
                        if (Session["AttendeesList"] != null && att != null)
                        { 
                            //Session["AttendeesList"] = am.GetAttendees(assnId);
                            List<Attendee> attList = (List<Attendee>)Session["AttendeesList"];
                            attList.Add(att);
                            Session["AttendeesList"] = attList;
                        }
                    }
                }
               
                if (att != null)
                {
                    // Get the selected org (or 'null' for individual registration)
                    if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        org = om.GetOrganization(Convert.ToInt32(NewRegFacilityRadComboBox.SelectedValue));
                    }
                    else if (NewRegFacilityRadComboBox.Text.Trim().Length > 1)
                    {
                        org = om.GetOrganizationByName(assnId, NewRegFacilityRadComboBox.Text.Trim());
                        if (org == null)
                        { 
                            org = om.AddOrganization(assnId, NewRegFacilityRadComboBox.Text.Trim());
                            if (Session["OrganizationsList"] != null && org != null)
                            {
                                //Session["AttendeesList"] = am.GetAttendees(assnId);
                                List<Organization> orgList = (List<Organization>)Session["OrganizationsList"];
                                orgList.Add(org);
                                Session["OrganizationsList"] = orgList;
                            }
                        }
                            
                    }

                    // Get the title
                    title = NewRegTitleTextBox.Text;
                
                    EventManager.Business.RegistrationMethods efm = new EventManager.Business.RegistrationMethods();
                    efm.AddRegistrationWithAttendee(eventId, org, att, title, userId);

                    efm.SaveAllObjectChanges();

                    // Show success message
                    NewRegSuccessValidator.ErrorMessage = "'" + att.LastName + ", " + att.FirstName + "' successfully registered.";
                    NewRegSuccessValidator.IsValid = false;

                    // Reset all inputs
                    ResetNewRegistrationPanel();

                    // Rebind the registrations to show the new attendee registration
                    RegistrationsRadGrid.DataBind();
                }
                

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    NewRegCustomValidator.ErrorMessage = ex.InnerException.Message;
                else
                    NewRegCustomValidator.ErrorMessage = ex.Message;

                NewRegCustomValidator.IsValid = false;
            }
        }

        protected void PrintBadgesButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string assnName = evt.Association.AssociationName;
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet ds = new EventManager.Web.Association.Reports2.AttendeeBadgesDataSet();
                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable dt = (EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable)ds.Tables[0];
                
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    
                    EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                    EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r = dt.NewAttendeeRow();
                    r.AssnName = assnName;
                    r.AttendeeFirstName = ra.Attendee.FirstName;
                    r.AttendeeId = ra.Attendee.Id;
                    r.AttendeeNameFNF = ra.Attendee.FullNameFNF;
                    r.EventEndDate = endDate;
                    r.EventName = eventName;
                    r.EventStartDate = startDate;
                    r.FlagString = ra.FlagListForBadge;
                    if (ra.Registration.Organization != null && ra.Registration.Organization.OrgName != null)
                        r.OrgName = ra.Registration.Organization.OrgName;
                    else
                        r.OrgName = "";
                    r.Title = ra.Title;

                    dt.AddAttendeeRow(r);
                }

                Session["AttendeeBadgesDs"] = ds;
                Response.Redirect("~/Association/Reports2/ReportViewer2.aspx?FileName=AttendeeBadges.rpt&Ds=AttendeeBadgesDs");
            }
        }

        protected void ActionsRadMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Text == "New")
            { 
                // Fill up the attendees and org lists
                int assnId = UserInfo.AssociationId;

                //---------------------------------------------------
                // Load the attendee combo
                //---------------------------------------------------
                //NewAttendeeRadComboBox.Items.Clear();
                // Look for attendees list in session, if not there, get from database
                if (Session["AttendeesList"] == null)
                {
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    List<EventManager.Model.Attendee> attendees = m.GetAttendees(assnId);
                    Session["AttendeesList"] = attendees;
                    //NewAttendeeRadComboBox.DataSource = attendees;
                }
                else
                {
                    //NewAttendeeRadComboBox.DataSource = Session["AttendeesList"];
                }
                //NewAttendeeRadComboBox.DataBind();

                //---------------------------------------------------
                // Load the facilities combo
                // --------------------------------------------------
                //NewRegFacilityRadComboBox.Items.Clear();
                if (Session["OrganizationsList"] == null)
                {
                    EventManager.Business.OrganizationMethods m2 = new EventManager.Business.OrganizationMethods();
                    List<EventManager.Model.Organization> orgs = m2.GetOrganizationsByAssociation(assnId);
                    Session["OrganizationsList"] = orgs;
                    //NewRegFacilityRadComboBox.DataSource = orgs;
                }
                else
                {
                    //NewRegFacilityRadComboBox.DataSource = Session["OrganizationsList"];
                }
                //NewRegFacilityRadComboBox.DataBind();

                // Clear out all fields of "new reg" panel and show it
                ResetNewRegistrationPanel();
                NewRegPanel.Visible = true;
                NewRegPanel.Style.Add(HtmlTextWriterStyle.Display, "block");
            }
        }

        private void ResetNewRegistrationPanel()
        {
            NewAttendeeRadComboBox.Items.Clear();
            NewAttendeeRadComboBox.Text = "";
            NewRegFacilityRadComboBox.Items.Clear();
            NewRegFacilityRadComboBox.Text = "";
            NewRegTitleTextBox.Text = "";

            NewAttendeeRadComboBox.Focus();
        }

        private void PrintSingleBadge(EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r)
        {
            Dymo.DymoAddIn DymoAddIn = new Dymo.DymoAddIn();
            Dymo.DymoLabels DymoLabels = new Dymo.DymoLabels();
            string FilePath = Server.MapPath("~/Association/Documents/Attendee Single Badge.label");
            string PrtNames = "";
            string PrinterName = "";

            if (DymoAddIn.Open(FilePath))
            {
                DymoLabels.SetField("EventStartDate", r.EventStartDate.ToShortDateString());
                DymoLabels.SetField("EventName", r.EventName);
                DymoLabels.SetField("AttendeeId", r.AttendeeId.ToString());
                DymoLabels.SetField("AttendeeFirstName", r.AttendeeFirstName);
                DymoLabels.SetField("FullNameAndTitle", r.AttendeeNameFNF.Trim() + ", " + r.Title.Trim());
                DymoLabels.SetField("OrgName", r.OrgName);
                DymoLabels.SetField("FlagString", r.FlagString);
                DymoLabels.SetField("AssociationName", "IHCA-ICAL");
            }

            // Find attached DYMO printer
            PrtNames = DymoAddIn.GetDymoPrinters();

            if (PrtNames != null)
            {
                PrinterName = PrtNames; 
                DymoAddIn.SelectPrinter(PrinterName);
                DymoAddIn.Print2(1, false, 0);
            }
        }

        protected void PrintSingleBadgesButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                Dymo.DymoAddIn DymoAddIn = new Dymo.DymoAddIn();
                Dymo.DymoLabels DymoLabels = new Dymo.DymoLabels();
                string FilePath = Server.MapPath("~/Association/Documents/Attendee Single Badge.label");
                string PrtNames = "";
                string PrinterName = "";

                // Find attached DYMO printer
                PrtNames = DymoAddIn.GetDymoPrinters();

                if (PrtNames != null)
                {
                    PrinterName = PrtNames;
                    DymoAddIn.SelectPrinter(PrinterName);
                }

                if (DymoAddIn.Open(FilePath) && PrinterName != null)
                { 
                    foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                    {
                        int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                        EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                        EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                        DymoLabels.SetField("EventStartDate", startDate.ToShortDateString());
                        DymoLabels.SetField("EventName", eventName);
                        DymoLabels.SetField("AttendeeId", ra.AttendeeId.ToString());
                        DymoLabels.SetField("AttendeeFirstName", ra.Attendee.FirstName);
                        DymoLabels.SetField("FullNameAndTitle", ra.Attendee.FullNameFNF.Trim() + ", " + ra.Title.Trim());
                        DymoLabels.SetField("OrgName", ra.Registration.Organization.OrgName);
                        DymoLabels.SetField("FlagString", ra.FlagListForBadge);
                        DymoLabels.SetField("AssociationName", "IHCA-ICAL");

                        DymoAddIn.Print2(1, false, 1);
                    }
                }
            }
        }



        protected void FlagMenuDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList flagList = (DropDownList)sender;

            if (flagList.SelectedIndex > 0)
            {
                EventManager.Business.RegistrationAttendeeFlags m2 = new EventManager.Business.RegistrationAttendeeFlags();

                // Assign flag for each selected row
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    int eventFlagId = Convert.ToInt32(flagList.SelectedValue);

                    m2.Add(regAttendeeId, eventFlagId); 
                }

                flagList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                RegistrationsRadGrid.DataBind();
            }
            
        }

        protected void SessionsMenuDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList sessionList = (DropDownList)sender;

            if (sessionList.SelectedIndex > 0)
            {
                EventManager.Business.SessionAttendanceMethods m2 = new EventManager.Business.SessionAttendanceMethods();

                // Assign flag for each selected row
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    int sessionId = Convert.ToInt32(sessionList.SelectedValue);

                    m2.Add(regAttendeeId, sessionId);
                }

                sessionList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                RegistrationsRadGrid.DataBind();
            }

        }

        protected void RegistrationsRadGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (!IsPostBack)
            {
                if (e.Item is GridDataItem)
                {
                    string eventId = Request.QueryString["EventId"];
                    GridDataItem item = (GridDataItem)e.Item;
                
                    if (item.DataItem != null)
                    {
                        if (!isPdfExport)
                        { 
                            RadButton editBtn = (RadButton)item.FindControl("EditAttendanceRadButton");
                            editBtn.Attributes["onclick"] = String.Format("return ShowAttendanceDialog('{0}', '{1}', '{2}');", eventId, ((RegistrationAttendee)item.DataItem).Id.ToString(), item.ItemIndex);

                            // Set the click event for "Show Attendee Dialog"
                            LinkButton attendeeNameButton = (LinkButton)item.FindControl("AttendeeNameLinkButton");
                            attendeeNameButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}');", ((RegistrationAttendee)item.DataItem).AttendeeId.ToString(), item.ItemIndex);

                            // Set the click event for "Show Attendee Dialog"
                            string reportName = "";
                            EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
                            if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForEvent(Convert.ToInt32(eventId)))))
                                reportName = m.GetCertificateFileNameForEvent(Convert.ToInt32(eventId));
                            else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForAssociation(UserInfo.AssociationId))))
                                reportName = m.GetCertificateFileNameForAssociation(UserInfo.AssociationId);
                            else
                                reportName = m.GetCertificateFileNameForApplication();

                            LinkButton printCertButton = (LinkButton)item.FindControl("PrintCertLinkButton");
                            printCertButton.Attributes["onclick"] = String.Format("return ShowCertificateReport('{0}', '{1}', '" + reportName + "');", Request.QueryString["EventId"], ((RegistrationAttendee)item.DataItem).AttendeeId.ToString());
                        }
                    
                    }
                }
            }
        }

        protected void AttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }

        protected void AttendeesDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {

        }

        protected void RegistrationsRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                isPdfExport = true;
                //RegistrationsRadGrid.PageSize = RegistrationsRadGrid.MasterTableView.VirtualItemCount;
                RegistrationsRadGrid.ExportSettings.IgnorePaging = true;
                RegistrationsRadGrid.ExportSettings.OpenInNewWindow = true;
                RegistrationsRadGrid.ExportSettings.ExportOnlyData = false;
                RegistrationsRadGrid.MasterTableView.GetColumn("CheckboxSelectColumn").Visible = false;
                RegistrationsRadGrid.MasterTableView.GetColumn("DeleteRegistrationColumn").Visible = false;
                RegistrationsRadGrid.MasterTableView.GetColumn("PrintCertificateTemplateColumn").Visible = false;
                RegistrationsRadGrid.MasterTableView.GetColumn("EditCommandColumn").Visible = false;
                RegistrationsRadGrid.MasterTableView.AllowFilteringByColumn = false;
                RegistrationsRadGrid.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
                PrepareGridForExport();
                RegistrationsRadGrid.MasterTableView.ExportToExcel();
            }
            else if (e.CommandName == "ExportToPdf")
            {
                isPdfExport = true;
            }
                
        }

        private void PrepareGridForExport()
        {
            foreach (GridDataItem gi in RegistrationsRadGrid.MasterTableView.Items)
            {
                //var hypAttendeeName = (LinkButton)gi.FindControl("AttendeeNameLinkButton");
                //gi["AttendeeNameTemplateColumn"].Text = hypAttendeeName.Text;

                // Hide the edit button on CEU total column
                var btnEditAttendance = (RadButton)gi.FindControl("EditAttendanceRadButton");
                btnEditAttendance.Visible = false;
            } 

        }
    }
}