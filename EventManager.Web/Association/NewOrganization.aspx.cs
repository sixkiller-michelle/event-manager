﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web.Association
{
    public partial class NewOrganization : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OrgTypeDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters[0] = this.AssociationId;
        }

        private Guid UserId
        {
            get
            {
                Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                return g;
            }
        }

        private int AssociationId
        {
            get
            {
                EventManager.Business.AssociationEmployeeMethods ef = new EventManager.Business.AssociationEmployeeMethods();
                return ef.GetEmployeeByUserId(this.UserId).AssociationId;
            }
        }

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            // try to insert the organization
            EventManager.Model.Organization o = new EventManager.Model.Organization();
            o.AssociationId = this.AssociationId;
            o.OrgName = OrgNameRadTextBox.Text;
            o.BedCount = (int)BedsRadNumericTextBox.Value;
            o.OrgTypeId = Convert.ToInt32(OrgTypeRadComboBox.SelectedValue);

            try
            {
                EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                o = om.AddOrganization(o);
                om.SaveAllObjectChanges();

                // Store the new ID in the hidden field
                OrgIdLabel.Text = o.Id.ToString();
            }
            catch (Exception ex)
            {
                CustomValidator1.ErrorMessage = ex.Message;
                CustomValidator1.IsValid = false;
            }
            
        }
    }
}