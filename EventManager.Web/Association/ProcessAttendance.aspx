﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="ProcessAttendance.aspx.cs" Inherits="EventManager.Web.Association.ProcessAttendanceForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<style type="text/css">
ul.ceuList {
  list-style-type:disc;
}
 
ul.ceuList li {
  position: relative;
  font: italic 12px/1.5 Helvetica, Verdana, sans-serif;
  margin-bottom: 20px;
}
 
ul.ceuList li p {
  font: 12px/1.5 Helvetica, sans-serif;
  padding-left: 40px;
}


</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="MainPanel" runat="server" Width="700" Style="padding:20px; padding-top:0px; height:90%; overflow:auto;">

<div style="width:600px;">

    <h2>Granting CEUs</h2>

    <p>Clicking the "Grant CEUs" button below will compile all scans for all attendees at the event and compare 
    them to the start / end times for each session. An attendee will receive credit based on the following rules:</p>

    <b>For sessions requiring 2 scans ("Only 1 scan required" UNCHECKED)</b>
    <ul class="ceuList">
        <li>The "IN" scan must fall in the acceptable range: between 1 hour before session start and 20 minutes after session start.</li>
        <li>The "OUT" scan must fall in the acceptable range: between 20 minutes prior to session end and 1 hour after session end</li>
    </ul>

    <b>For sessions requiring only 1 scan ("Only 1 scan required" checked)</b>
    <ul class="ceuList">
        <li>The attendee must have at least one scan between 1 hour before session start and 1 hour after session end.</li>
    </ul>
</div>

<asp:Panel ID="Panel1" runat="server" Width="600" Style="padding:20px; background-color:#eae8e8; border: 1 solid;">

    <p style="color:Red;">Before granting CEUs, <i>be sure that you have done the following:</i></p>

    <ul class="ceuList">
        <li>Adjusted the start and end times of each session to be accurate.  For example, if a session 
        was supposed to start at 9:00 AM, but started 30 minutes late, update the start time of the session to be 9:30 AM. 
        This will ensure that attendees get credit for an "IN" scan if they scanned at 9:25 AM. Go to the 
        <asp:HyperLink ID="SessionPageHyperLink" runat="server" Text="Sessions" NavigateUrl="~/Association/EventSessions.aspx?EventId=" /> to update session times.
         </li>
         <li>If you used the offline scanner application to scan attendees, be sure to upload all scan files from the
         laptops that collected the scans.
         </li>
    </ul>

    <telerik:RadButton ID="ProcessAttendanceRadButton" runat="server" Text="Grant CEUs" onclick="ProcessAttendanceRadButton_Click"></telerik:RadButton>&nbsp;&nbsp;

    <asp:Label ID="SuccessLabel" runat="server" Text="CEUs granted successfully" ForeColor="Green" Visible="false"></asp:Label>
    <asp:Label ID="FailureLabel" runat="server" Text="An error occurred while granting CEUs" ForeColor="Red" Visible="false"></asp:Label>

</asp:Panel>

</asp:Panel>

</asp:Content>
