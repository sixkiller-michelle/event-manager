﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventAttendance.aspx.cs" Inherits="EventManager.Web.Association.EventSessionAttendanceForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="content">

<h2>Event Attendance</h2>


    <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" 
        CellSpacing="0" DataSourceID="AttendanceDataSource" GridLines="None">
        <groupingsettings casesensitive="False"></groupingsettings>
<MasterTableView datasourceid="AttendanceDataSource" DataKeyNames="Id">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="RegistrationAttendeeId" 
            DataType="System.Int32" 
            FilterControlAltText="Filter RegistrationAttendeeId column" 
            HeaderText="RegistrationAttendeeId" SortExpression="RegistrationAttendeeId" 
            UniqueName="RegistrationAttendeeId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="SessionId" DataType="System.Int32" 
            FilterControlAltText="Filter SessionId column" HeaderText="SessionId" 
            SortExpression="SessionId" UniqueName="SessionId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="EventSession.SessionName" DataType="System.String" 
            FilterControlAltText="Filter SessionName column" HeaderText="Session" 
            SortExpression="EventSession.SessionName" UniqueName="SessionNameGridBoundColumn">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="RegistrationAttendee.Attendee.FullName" DataType="System.String" 
            FilterControlAltText="Filter AttendeeName column" HeaderText="Attendee" 
            SortExpression="RegistrationAttendee.Attendee." UniqueName="AttendeeNameGridBoundColumn">
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn DataField="IsSpeaker" DataType="System.Boolean" 
            FilterControlAltText="Filter IsSpeaker column" HeaderText="IsSpeaker" 
            SortExpression="IsSpeaker" UniqueName="IsSpeaker">
        </telerik:GridCheckBoxColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>


    <asp:ObjectDataSource ID="AttendanceDataSource" runat="server" 
        SelectMethod="GetAttendanceByEvent" 
        TypeName="EventManager.Business.SessionAttendanceMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
</asp:Content>
