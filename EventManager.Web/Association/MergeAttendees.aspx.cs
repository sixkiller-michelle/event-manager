﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class MergeAttendeesForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label l = (Label)DeleteAttendeeFormView.FindControl("EmptyDataLabel");
                if (l != null)
                    l.Visible = false;
            }
            else
            {
               
                if (Request.Form["__EVENTTARGET"].Contains("KeepAttendeeRadTextBox"))
                    if (Request.Form["__EVENTARGUMENT"] != "")
                    {
                        KeepAttendeeFormView.DataBind();
                    }
            }
        }

        protected void MergeButton_Click(object sender, EventArgs e)
        {
            try
            {
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                m.MergeDuplicateAttendees(Convert.ToInt32(KeepAttendeeRadTextBox.Text), Convert.ToInt32(DeleteAttendeeRadTextBox.Text));
                //m.SaveAllObjectChanges();

                DeleteAttendeeFormView.DataBind();
                KeepAttendeeFormView.DataBind();
                DeleteRadGrid.DataBind();
                KeepRadGrid.DataBind();
            }
            catch (Exception ex)
            { 
                
            }

        }

        protected void DeleteAttendeeFormView_DataBound(object sender, EventArgs e)
        {
            if (DeleteAttendeeFormView.DataItemCount > 0 && KeepAttendeeFormView.DataItemCount > 0)
            {
                MergeButton.Enabled = true;
            }
            else
            {
                MergeButton.Enabled = false;
            }
        }

        protected void KeepAttendeeFormView_DataBound(object sender, EventArgs e)
        {
            if (DeleteAttendeeFormView.DataItemCount > 0 && KeepAttendeeFormView.DataItemCount > 0)
            {
                MergeButton.Enabled = true;
            }
            else
            {
                MergeButton.Enabled = false;
            }
        }
    }
}