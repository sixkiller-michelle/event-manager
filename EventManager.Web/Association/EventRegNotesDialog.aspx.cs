﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class EventRegNotesDialog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                if (RegistrationId > 0)
                { 
                    RegistrationMethods m = new RegistrationMethods();
                    Registration reg = m.GetRegistration(RegistrationId);
                    if (reg != null)
                    {
                        RegNoteRadTextBox.Text = reg.Notes;
                        OrgNameLabel.Text = reg.RegistrationDesc;
                    }
                }
            }
            
        }

        private int RegistrationId
        {
            get
            { 
                int regId;
                if (Int32.TryParse(Request.QueryString["RegId"], out regId))
                {
                    return regId;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void RegAttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["registrationId"] = RegistrationId;
        }

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            RegistrationMethods m = new RegistrationMethods();
            Registration reg = m.GetRegistration(RegistrationId);
            if (reg != null)
            {
                reg.Notes = RegNoteRadTextBox.Text;
                m.SaveAllObjectChanges();
            }

            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind(" + RegistrationId.ToString() + ");", true);
        }

        protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            RadGrid1.DataSource = m.GetRegisteredAttendees(RegistrationId, true).OrderBy(r => r.EntryDateTime);
        }

        protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                int regAttendeeId = Convert.ToInt32(dataBoundItem.GetDataKeyValue("Id"));

                RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                RegistrationAttendee att = m.GetRegistrationAttendee(regAttendeeId);
                if (att != null)
                {
                    if (att.IsCancelled == true)
                    {
                        dataBoundItem["AttendeeNameGridBoundColumn"].BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        dataBoundItem["AttendeeNameGridBoundColumn"].BackColor = RadGrid1.ItemStyle.BackColor;
                    }
                }
            }
        }
    }
}