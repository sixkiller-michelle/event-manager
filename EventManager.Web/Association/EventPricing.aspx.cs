﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using Telerik.Web.UI;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventPricingForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private int EventId
        {
            get
            {
                int eventId = 0;
                if (Int32.TryParse(Request.QueryString["EventId"], out eventId))
                {
                    return eventId;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void FeeScheduleRadGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            int eventId = EventId;
            if (eventId > 0)
            {
                EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                FeeScheduleRadGrid.DataSource = m.GetFeeSchedulesForEvent(eventId);
            }
        }

        protected void RegFeesRadGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            int eventId = EventId;
            if (eventId > 0)
            {
                RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
                RegFeesRadGrid.DataSource = m.GetRegistrationFeesByEvent(eventId);
            }
        }

       
        protected void FeeScheduleRadGrid_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            try
            {
                EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                EventFeeSchedule sch = new EventFeeSchedule();
                sch.EventId = Convert.ToInt32(Request.QueryString["EventId"]);
                item.UpdateValues(sch);
                sch.StartDateTime = new DateTime(sch.StartDateTime.Year, sch.StartDateTime.Month, sch.StartDateTime.Day);
                sch.EndDateTime = new DateTime(sch.EndDateTime.Year, sch.EndDateTime.Month, sch.EndDateTime.Day, 23, 59, 59);

                m.Add(sch);
                m.SaveAllObjectChanges();
                FeeScheduleRadGrid.MasterTableView.IsItemInserted = false;
                //FeeScheduleRadGrid.Rebind();

            }
            catch (ArgumentException ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("FormTemplateCustomValidator");
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("FormTemplateCustomValidator");
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void FeeScheduleRadGrid_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;

            try
            {
                int feeScheduleId = Convert.ToInt32(item.GetDataKeyValue("Id"));

                EventManager.Business.EventFeeScheduleMethods esm = new EventManager.Business.EventFeeScheduleMethods();
                EventFeeSchedule s = esm.GetFeeSchedulePeriod(feeScheduleId);
                item.UpdateValues(s);

                esm.Update(s);
                esm.SaveAllObjectChanges();

                FeeScheduleRadGrid.MasterTableView.ClearEditItems();
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("FormTemplateCustomValidator");
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            
        } 
        
        protected void FeeScheduleRadGrid_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                int feeScheduleId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

                EventManager.Business.EventFeeScheduleMethods esm = new EventManager.Business.EventFeeScheduleMethods();
                EventFeeSchedule s = esm.GetFeeSchedulePeriod(feeScheduleId);
                esm.Delete(s);
                esm.SaveAllObjectChanges();

                FeeScheduleRadGrid.Rebind();
            }
            catch (Exception ex)
            {
                CustomValidator v = CustomValidator1;
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
            }
            
        }

        protected void RegFeesRadGrid_Init(object sender, EventArgs e)
        {
            SetColumnHeaderText();
        }


        protected void RegFeesRadGrid_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            try
            {
                RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
                RegistrationFeeTable fee = new RegistrationFeeTable();
                item.UpdateValues(fee);

                fee.EventId = EventId;
                RadComboBox orgFeeCatCombo = (RadComboBox)item.FindControl("OrgFeeCategoryRadComboBox");
                if (orgFeeCatCombo.SelectedIndex == 0)
                {
                    fee.FeeCategoryId = null;
                }
            
                m.Add(fee);
                m.SaveAllObjectChanges();
                RegFeesRadGrid.MasterTableView.IsItemInserted = false;
                //RegFeesRadGrid.Rebind();

            }
            catch (ArgumentException ex)
            {
                CustomValidator v = CustomValidator1;
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = CustomValidator1;
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void RegFeesRadGrid_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;

            try
            {
                int regFeeTableId = Convert.ToInt32(item.GetDataKeyValue("Id"));

                RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
                RegistrationFeeTable fee = new RegistrationFeeTable();
                item.UpdateValues(fee);
                fee.Id = regFeeTableId;

                m.Update(fee);
                m.SaveAllObjectChanges();

                RegFeesRadGrid.MasterTableView.ClearEditItems();
            }
            catch (Exception ex)
            {
                CustomValidator v = CustomValidator1;
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }

        }

        protected void RegFeesRadGrid_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                int regFeeTableId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

                RegistrationFeeTableMethods m = new RegistrationFeeTableMethods();
                RegistrationFeeTable fee = m.GetRegistrationFee(regFeeTableId);
                m.Delete(fee);
                m.SaveAllObjectChanges();

                RegFeesRadGrid.Rebind();
            }
            catch (Exception ex)
            {
                CustomValidator v = CustomValidator1;
                if (ex.InnerException != null)
                    v.ErrorMessage = ex.InnerException.Message;
                else
                    v.ErrorMessage = ex.Message;
                v.IsValid = false;
            }

        }


        private void SetColumnHeaderText()
        {
            // Get all reg time periods to update column headers
            if (EventId > 0)
            {
                EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                List<EventFeeSchedule> timeframes = m.GetFeeSchedulesForEvent(EventId);

                GridTemplateColumn grpPriceCol1 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period1GroupPriceTemplateColumn");
                GridTemplateColumn attPriceCol1 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period1AttendeePriceTemplateColumn");

                GridTemplateColumn grpPriceCol2 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period2GroupPriceTemplateColumn");
                GridTemplateColumn attPriceCol2 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period2AttendeePriceTemplateColumn");

                GridTemplateColumn grpPriceCol3 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period3GroupPriceTemplateColumn");
                GridTemplateColumn attPriceCol3 = (GridTemplateColumn)RegFeesRadGrid.MasterTableView.GetColumnSafe("Period3AttendeePriceTemplateColumn");

                if (timeframes.Count > 0)
                {
                    grpPriceCol1.HeaderText = "'" + timeframes[0].TimeframeName + "' Group Price";
                    attPriceCol1.HeaderText = "'" + timeframes[0].TimeframeName + "' Attendee Price";

                    if (timeframes.Count > 1)
                    {
                        grpPriceCol2.HeaderText = "'" + timeframes[1].TimeframeName + "' Group Price";
                        attPriceCol2.HeaderText = "'" + timeframes[1].TimeframeName + "' Attendee Price";

                        if (timeframes.Count > 2)
                        {
                            grpPriceCol3.HeaderText = "'" + timeframes[2].TimeframeName + "' Group Price";
                            attPriceCol3.HeaderText = "'" + timeframes[2].TimeframeName + "' Attendee Price";
                        }
                        else
                        {
                            grpPriceCol3.Visible = false;
                            attPriceCol3.Visible = false;
                        }
                    }
                    else
                    {
                        grpPriceCol2.Visible = false;
                        attPriceCol2.Visible = false;
                        grpPriceCol3.Visible = false;
                        attPriceCol3.Visible = false;
                    }
                }
                else
                {
                    grpPriceCol1.Visible = false;
                    attPriceCol1.Visible = false;
                    grpPriceCol2.Visible = false;
                    attPriceCol2.Visible = false;
                    grpPriceCol3.Visible = false;
                    attPriceCol3.Visible = false;
                }

            }
        }

        protected void OrgFeeCategoryRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox feeCatCombo = (RadComboBox)sender;
           
            GridDataItem item = (GridDataItem)feeCatCombo.Parent.Parent;
            CheckBox memberCheckBox = (CheckBox)item.FindControl("IsMemberCheckBox");
            Label isMemberLabel = (Label)item.FindControl("IsMemberLabel");
            Label isNonMemberLabel = (Label)item.FindControl("IsNonMemberLabel");

            // Set the 'Member' column based on selection
            if (feeCatCombo.SelectedIndex == 0)
            {
                memberCheckBox.Checked = false;
                memberCheckBox.Enabled = true;
                isMemberLabel.Visible = false;
                isNonMemberLabel.Visible = false;
            }
            else if (feeCatCombo.SelectedIndex > 0)
            { 
                // Get the fee category that is selected
                int orgFeeCatId = Convert.ToInt32(feeCatCombo.SelectedValue);
                OrganizationFeeCategoryMethods m = new OrganizationFeeCategoryMethods();
                OrganizationFeeCategory feeCat = m.GetFeeCategory(orgFeeCatId);
                if (feeCat != null)
                {
                    if (feeCat.IsMemberCategory)
                    {
                        memberCheckBox.Checked = true;
                        isMemberLabel.Visible = true;
                        isNonMemberLabel.Visible = false;
                    }
                    else
                    {
                        memberCheckBox.Checked = false;
                        isMemberLabel.Visible = false;
                        isNonMemberLabel.Visible = true;
                    } 
                    memberCheckBox.Enabled = false;
                    memberCheckBox.Visible = false;

                }
            }
        }

        protected void OrgFeeCategoriesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void RegFeesRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                RegFeesRadGrid.MasterTableView.ClearEditItems();

                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert  
                newValues["EventId"] = EventId;
                newValues["FeeCategoryId"] = null;
                newValues["IsMemberRate"] = false;
                newValues["GroupMinCount"] = 1;
                newValues["GroupMaxCount"] = 1;
                newValues["Period1GroupPrice"] = 0;
                newValues["Period1AttendeePrice"] = 0;
                newValues["Period2GroupPrice"] = 0;
                newValues["Period2AttendeePrice"] = 0;
                newValues["Period3GroupPrice"] = 0;
                newValues["Period3AttendeePrice"] = 0;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

    }
}