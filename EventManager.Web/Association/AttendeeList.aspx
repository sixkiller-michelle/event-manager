﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="AttendeeList.aspx.cs" Inherits="EventManager.Web.Association.AttendeeListForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <style type="text/css">
        #<%= RadGrid1GridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>

    <script type="text/javascript" id="telerikClientEvents1">
//<![CDATA[

	
//]]>
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[
        var rowIndex;

        function NewAttendeeRadButton_Clicked(sender, args) {
            ShowAttendeeDialog(0, 0);
        }

        function ActionsRadMenu_ItemClicking(sender, args) {

            if (args.get_item().get_text() == "New") {
                ShowAttendeeDialog(0, null);
            }
            else if (args.get_item().get_text() == "Print Attendee List") {
                var assnId = args.get_item().get_value();
                ShowReportViewerDialog(assnId)
                //args.set_cancel(true);
            }

        }

        function ShowOrgDialog(orgId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "EditOrgDialog");
            window.setSize(850, 450);
            return false;
        }

        function ShowAttendeeDialog(attendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "EditAttendeeDialog");
            window.setSize(850, 630);
            return false;
        }

        function ShowReportViewerDialog(associationId) {

            window.radopen("Reports2/ReportViewer.aspx?FileName=Attendees.rpt&ParmNames=@AssociationId&ParmValues=" + associationId, "ReportViewerDialog");
            window.setSize(850, 600);
            return false;
        }

        function AttendeeAdded(attendeeId, fullName, orgId, orgName, title) {

            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(attendeeId);
        }

        function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(attendeeId);
        }

        //]]>
    </script>

</telerik:RadCodeBlock>


<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxManager runat="server" ID="RadAjaxManager1" 
        DefaultLoadingPanelID="RadAjaxLoadingPanel1" 
        OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" 
        onajaxrequest="RadAjaxManager1_AjaxRequest">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGrid1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadSplitter runat="server" ID="EventRadSplitter" Width="100%" Height="100%" BorderStyle="None" BorderSize="0" Orientation="Horizontal" >
    
    <telerik:RadPane ID="TopRadPane" runat="server" Scrolling="None" Height="80" >

         <telerik:RadMenu ID="ActionsRadMenu" runat="server" Width="100%" 
            OnItemClick="ActionsRadMenu_ItemClick" Skin="Default" 
            OnClientItemClicking="ActionsRadMenu_ItemClicking">
        <Items>
            <telerik:RadMenuItem runat="server" Text="New" PostBack="false"></telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Print Attendee List" Value="1" Enabled="true"></telerik:RadMenuItem>
        </Items>
        </telerik:RadMenu>

        <asp:Panel ID="Panel1" runat="server" style="padding:10px;">
            <h2 style="margin-top:30px;">Attendee List</h2>
        </asp:Panel>

        <asp:Panel ID="HeaderPanel" runat="server" Style="margin:10px;">
            <telerik:RadButton ID="NewAttendeeRadButton" runat="server" AutoPostBack="false" UseSubmitBehavior="false"  
                Text="New Attendee..." onclientclicked="NewAttendeeRadButton_Clicked"></telerik:RadButton>
        </asp:Panel>

    </telerik:RadPane>
    <telerik:RadPane ID="BottomRadPane" runat="server" Scrolling="None">

        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
            AllowPaging="True" AllowSorting="False" Height="100%" Width="100%" PageSize="20"  
        AutoGenerateColumns="False" CellSpacing="0" DataSourceID="AttendeesDataSource" 
            GridLines="None" PagerStyle-Position="TopAndBottom" 
            onitemcreated="RadGrid1_ItemCreated">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings>
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        </ClientSettings>
        <MasterTableView datasourceid="AttendeesDataSource" DataKeyNames="Id" AllowFilteringByColumn="True"  >
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" AllowFiltering="true" HeaderText="Id" 
                    SortExpression="Id" UniqueName="Id" HeaderStyle-Width="100" ItemStyle-Width="100">
                    <FilterTemplate>Clear filters
                        <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/Images/filterCancel.gif" AlternateText="Show All" ToolTip="Show All" OnClick="btnShowAll_Click"
                            Style="vertical-align: middle" />
                    </FilterTemplate>
                </telerik:GridBoundColumn>

                 <telerik:GridTemplateColumn UniqueName="AttendeeNameTemplateColumn" DataField="FullName" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" 
                        AllowFiltering="true" FilterControlWidth="90px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                        SortExpression="FullName" FilterControlAltText="Search by attendee name" 
                        HeaderStyle-Width="150px" ItemStyle-Width="150">
                        <ItemTemplate>
                            <asp:LinkButton ID="AttendeeNameLinkButton" runat="server" Text='<%# Eval("FullName") %>' CommandName="ViewAttendeeDetails" />
                        </ItemTemplate>
                        <HeaderStyle Width="150px"></HeaderStyle>
                    </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="Organization.OrgName" HeaderText="Facility" SortExpression="Organization.OrgName" UniqueName="OrgNameColumn"
                    AllowFiltering="true" ShowFilterIcon="true" CurrentFilterFunction="StartsWith" AutoPostBackOnFilter="true" 
                    ItemStyle-Width="250" HeaderStyle-Width="250" FilterControlWidth="200">
                </telerik:GridBoundColumn>

               <%-- <telerik:GridTemplateColumn UniqueName="OrgTemplateColumn" DataField="Organization.OrgName" HeaderText="Facility" 
                    AutoPostBackOnFilter="true" SortExpression="Organization.OrgName"  HeaderStyle-Width="250px" ItemStyle-Width="250"
                    FilterControlAltText="Filter by facility" FilterControlWidth="200" CurrentFilterFunction="StartsWith" ShowFilterIcon="true" DataType="System.String">
                  <ItemTemplate>
                        <%# Eval("Organization.OrgName")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridTemplateColumn>--%>

                <telerik:GridBoundColumn DataField="Title"   HeaderText="Title" SortExpression="Title" UniqueName="TitleColumn" 
                    ItemStyle-Width="200" HeaderStyle-Width="200" 
                    AllowFiltering="true" ShowFilterIcon="true" FilterControlWidth="150" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Email"  AllowFiltering="false" HeaderText="Email" SortExpression="Email" UniqueName="EmailColumn" 
                    ItemStyle-Width="200" HeaderStyle-Width="200">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PhoneNumber"  AllowFiltering="false" HeaderText="Phone" UniqueName="column" >
                </telerik:GridBoundColumn>

            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>
        <PagerStyle Position="TopAndBottom"></PagerStyle>
        <FilterMenu EnableImageSprites="False"></FilterMenu>

        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>


    </telerik:RadPane>
</telerik:RadSplitter>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetAttendees" TypeName="EventManager.Business.AttendeeMethods" onselecting="AttendeesDataSource_Selecting">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" EnableShadow="false">
    <Windows>
        <telerik:RadWindow ID="EditAttendeeDialog" runat="server" Title="Edit Attendee" Height="500px"
            Width="700px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
        <telerik:RadWindow ID="EditOrgDialog" runat="server" Title="Edit Facility" Height="460px"
            Width="850px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
        <telerik:RadWindow ID="ReportViewerDialog" runat="server" Title="Report" Height="600px"
            Width="850px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
    </Windows>
</telerik:RadWindowManager>

</asp:Content>
