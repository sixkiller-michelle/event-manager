﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class AttendeeDetailsDialog : System.Web.UI.Page
    {
        EventManager.Model.Attendee newAttendee;
        int newAttendeeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["AttendeeId"] == null || Request.QueryString["AttendeeId"] == "0")
                {
                    FormView1.ChangeMode(FormViewMode.Insert);
                }

                if (FormView1.CurrentMode == FormViewMode.Insert)
                { 
                    Telerik.Web.UI.RadTextBox firstName = (Telerik.Web.UI.RadTextBox)FormView1.FindControl("FirstNameTextBox");
                    if (firstName != null)
                        firstName.Focus();
                }
            }
            
           

        }

        private void RefreshAttendeeSessionList()
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            List<EventManager.Model.Attendee> attendees = m.GetAttendees(UserInfo.AssociationId);
            Session["AttendeesList"] = attendees;
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    RefreshAttendeeSessionList();
                    AttendeeDataSource.DataBind();
                    FormView1.DataBind();
                    CloseForm("insert");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    else
                        CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInInsertMode = true;
                }
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    //AttendeeDataSource.DataBind();
                    //FormView1.DataBind();
                    CloseForm("update");
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInEditMode = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
        }

        private void CloseForm(string action)
        {
            EventManager.Business.AttendeeMethods m;

            // Get the new attendee ID and name
            if (action == "insert")
            { 
                if (newAttendee != null)
                {
                    int AttendeeId = newAttendee.Id;
                    string AttendeeFullName = newAttendee.FullNameWithId;
                    string Title = newAttendee.Title;

                    int? OrgId = newAttendee.OrganizationId;
                    string OrgName = "";
                
                    // Get the org name
                    if (OrgId != null && OrgId != 0)
                    { 
                        OrgName = newAttendee.Organization.OrgName;
                    }

                    // Refill the session variable to include new attendee
                    m = new EventManager.Business.AttendeeMethods();
                    Session["AttendeesList"] = m.GetAttendees(GetAssociationId());

                    ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('insert', '" + AttendeeId.ToString() + "', '" + AttendeeFullName + "', '" + OrgId.ToString() + "', '" + OrgName + "', '" + Title + "');", true);
                }
            }
            else if (action == "update")
            {
                m = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a  = m.GetAttendee(Convert.ToInt32(Request.QueryString["AttendeeId"]));

                int? OrgId = a.OrganizationId;
                string OrgName = "";

                // Get the org name
                if (OrgId != null && OrgId != 0)
                {
                    OrgName = a.Organization.OrgName;
                }

                if (a != null)
                {
                    //ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('update', '" + a.Id.ToString() + "', '" + a.FullName.Replace(",", "\\,") + "', '" + OrgId.ToString() + "', '" + OrgName + "', '" + a.Title + "');", true);
                    ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('update', " + a.Id.ToString() + ", '" + a.FullName + "', '" + OrgId.ToString() + "', '" + OrgName.Replace("'", "\\'")  + "', '" + (String.IsNullOrEmpty(a.Title) ? "" : a.Title) + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind(0, '');", true);
            }
            
        }

        protected void OrgDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = GetAssociationId();
        }

        private int GetAssociationId()
        {
            // Get the user ID
            Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());

            // Get the association ID
            EventManager.Business.AssociationEmployeeMethods ef = new EventManager.Business.AssociationEmployeeMethods();
            return ef.GetEmployeeByUserId(g).AssociationId;
        }

        protected void AttendeeDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            newAttendee = (EventManager.Model.Attendee)e.ReturnValue;
            if (newAttendee != null)
                newAttendeeId = ((EventManager.Model.Attendee)e.ReturnValue).Id;
        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('cancel', '', '', '', '', '');", true);
            }
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["IsApproved"] = true;
            e.Values["AssociationId"] = GetAssociationId();

            RadComboBox orgCombo = (RadComboBox)FormView1.FindControl("OrganizationRadComboBox");
            if (orgCombo != null && (orgCombo.SelectedValue == "" || orgCombo.SelectedValue == null))
            {
                if (string.IsNullOrEmpty(orgCombo.Text))
                {
                    e.Values["OrganizationId"] = null;
                }
                else
                {
                    e.Values["OrganizationId"] = null;
                }
            }

        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Edit)
            { 
                RadComboBox orgCombo = (RadComboBox)FormView1.FindControl("OrganizationRadComboBox");
                if (orgCombo.SelectedValue == "")
                {
                    e.NewValues["OrganizationId"] = null;
                }
            }
        }

        protected void StateRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            {
                RadComboBox c = (RadComboBox)sender;
                if (c.Items.Count > 1)
                {
                    c.SelectedValue = UserInfo.DefaultStateCode;
                }
            }
            
        }

        protected void OrganizationRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                RadComboBox combo = (RadComboBox)sender;
                int assnId = GetAssociationId();

                if (Session["OrganizationsList"] == null)
                {
                    EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();
                    Session["OrganizationsList"] = m.GetOrganizationsByAssociation(assnId);
                }
                List<EventManager.Model.Organization> orgs = ((List<EventManager.Model.Organization>)Session["OrganizationsList"]).Where(l => l.OrgName.StartsWith(e.Text)).ToList();
                if (orgs.Count > 0)
                {
                    combo.Items.Clear();
                    foreach (EventManager.Model.Organization a in orgs)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.OrgName, a.Id.ToString());
                        combo.Items.Add(item);
                    }
                    combo.Items.Insert(0, new RadComboBoxItem("", ""));
                }
                else
                {
                    combo.ClearSelection();
                }

            }
        }

    }
}