﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="AttendeeDetails.aspx.cs" Inherits="EventManager.Web.Association.AttendeeDetailsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content">

    Back to: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Association/AttendeeList.aspx">Attendee List</asp:HyperLink>
    <br />
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="errorMessage" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>

    <table style="width:100%;">
    <tr>
    <td style="vertical-align:top; width:400px;">
        <asp:FormView ID="FormView1" runat="server" DataSourceID="AttendeeDataSource" DataKeyNames="Id" 
                Width="100%"
                oniteminserted="FormView1_ItemInserted" 
                onitemupdated="FormView1_ItemUpdated" 
                onitemdeleted="FormView1_ItemDeleted" 
                oniteminserting="FormView1_ItemInserting" 
            onitemupdating="FormView1_ItemUpdating">
            <EditItemTemplate>
                <table style="width: 100%;" class="formview-table-layout">
                   <tr>
                        <td colspan="2"><h1><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullNameFNF") %>' ViewStateMode="Disabled"></asp:Label></h1></td>
                    </tr>
                    <tr>
                        <td>Prefix:</td>
                        <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind("Prefix") %>' /></td>
                    </tr>
                    <tr>
                        <td>First Name:</td>
                        <td><telerik:RadTextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                    </tr>
                     <tr>
                        <td>Last Name:</td>
                        <td> <telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' /></td>
                    </tr>
                    <tr>
                        <td>MI:</td>
                        <td><telerik:RadTextBox ID="MiddleInitialTextBox" runat="server" Text='<%# Bind("MiddleInitial") %>' /></td>
                    </tr>
                    <tr>
                        <td>Credentials:</td>
                        <td><telerik:RadTextBox ID="CredentialsTextBox" runat="server" Text='<%# Bind("Credentials") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Facility</h3></td>
                    </tr>
                    <tr>
                        <td>Facility Name:</td>
                        <td>                  
                      
                            <telerik:RadComboBox
                            ID="OrganizationRadComboBox" runat="server" DataSourceId="OrganizationsDataSource"
                            Width="200px" DropDownWidth="300px" AppendDataBoundItems="true"
                            EmptyMessage="Facility..."
                            MarkFirstMatch="True"
                            DataTextField="OrgName" 
                            DataValueField="Id"
                            SelectedValue='<%# Bind("OrganizationId") %>'>
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Address</h3></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><telerik:RadTextBox ID="FullAddressTextBox" runat="server" Text='<%# Bind("Address") %>' Wrap="true" Rows="3" Width="200" /></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><telerik:RadTextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' /></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="300" ondatabound="StateRadComboBox_DataBound">
                            </telerik:RadComboBox></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td><telerik:RadTextBox ID="TextBox14" runat="server" Text='<%# Bind("Zip") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Contact</h3></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><telerik:RadTextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="Update" />&nbsp;
                <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="True" CommandName="Cancel" />
            </EditItemTemplate>

            <InsertItemTemplate>
            <h2>New Attendee</h2>
                <table style="width: 100%;" class="formview-table-layout">
                    <tr>
                        <td colspan="4"><h3>Name</h3></td>
                    </tr>
                    <tr>
                        <td>Prefix:</td>
                        <td colspan="3"><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind("Prefix") %>' MaxLength="5" Width="50" /></td>
                    </tr>
                    <tr>
                        <td>First Name:</td>
                        <td colspan="3"><telerik:RadTextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                    </tr>
                     <tr>
                        <td>Last Name:</td>
                        <td><telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' /></td>
                        <td>MI:</td>
                        <td><telerik:RadTextBox ID="MiddleInitialTextBox" runat="server" Text='<%# Bind("MiddleInitial") %>' Width="30" /></td>
                    </tr>
                    <tr>
                        <td>Credentials:</td>
                        <td colspan="3"><telerik:RadTextBox ID="CredentialsTextBox" runat="server" Text='<%# Bind("Credentials") %>' MaxLength="30" Width="50" /></td>
                    </tr>
                    <tr>
                        <td colspan="4"><h3>Facility</h3></td>
                    </tr>
                    <tr>
                        <td>Facility Name:</td>
                        <td colspan="3">                  
                        <telerik:RadComboBox
                            ID="OrganizationRadComboBox" runat="server" DataSourceId="OrganizationsDataSource"
                            Width="200px" DropDownWidth="300px" AppendDataBoundItems="true"
                            EmptyMessage="Facility..."
                            MarkFirstMatch="True"
                            DataTextField="OrgName" 
                            DataValueField="Id"
                            SelectedValue='<%# Bind("OrganizationId") %>'>
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td colspan="3"><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="4"><h3>Address</h3></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td colspan="3"><telerik:RadTextBox ID="FullAddressTextBox" runat="server" Text='<%# Bind("Address") %>' Wrap="true" Rows="3" Width="200" TextMode="MultiLine" /></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td colspan="3"><telerik:RadTextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' /></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td colspan="3"><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode"
                               DropDownWidth="300" ondatabound="StateRadComboBox_DataBound">
                            </telerik:RadComboBox></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td colspan="3"><telerik:RadTextBox ID="TextBox14" runat="server" Text='<%# Bind("Zip") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Contact</h3></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td colspan="3"><telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td colspan="3"><telerik:RadTextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <br />
                <telerik:RadButton ID="InsertButton" runat="server" Text="Insert" CausesValidation="True"  CommandName="Insert" />&nbsp;
                <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
            </InsertItemTemplate>

            <ItemTemplate>
                <table style="width: 100%;" class="formview-table-layout">
                    <tr>
                        <td colspan="2"><h1><asp:Label ID="Label1" runat="server" Text='<%# Bind("FullNameFNF") %>'></asp:Label></h1></td>
                    </tr>
                    <tr>
                        <td>Id:</td>
                        <td><telerik:RadTextBox ID="IdRadTextBox" runat="server" Text='<%# Bind("Id") %>' ReadOnly="true" BorderStyle="None" Width="50" /></td>
                    </tr>
                    <tr>
                        <td>Facility:</td>
                        <td>
                        <asp:HyperLink ID="OrgHyperLink" runat="server" 
                            Text='<%# Bind("Organization.OrgName") %>' 
                            NavigateUrl='<%# String.Format("~/Association/OrgDetails.aspx?OrgId={0}", Eval("OrganizationId"))%>'></asp:HyperLink>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' ReadOnly="true" Width="100%" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Address</h3></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind("FullAddress") %>' 
                            Height="50" Rows="3" TextMode="MultiLine" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Contact</h3></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><telerik:RadTextBox Width="195px" ID="EmailRadTextBox" runat="server" Label="" Text='<%# Bind("Email") %>'
                            EmptyMessage="" InvalidStyleDuration="100" AutoPostBack="false" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><telerik:RadTextBox Width="195px" ID="PhoneRadTextBox" runat="server" Label="" Text='<%# Bind("PhoneNumber") %>'
                            EmptyMessage="" InvalidStyleDuration="100" AutoPostBack="false" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <br />

                <telerik:RadButton ID="EditButton" runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />&nbsp;
                <telerik:RadButton ID="DeleteButton" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete" />&nbsp;
                <telerik:RadButton ID="NewButton" runat="server" CausesValidation="false" CommandName="New" Text="New" />

            </ItemTemplate>
        </asp:FormView>
    </td>
    <td style="vertical-align:top; padding-left:20px;">
        
        <h3>Attendance History</h3>
         <telerik:RadGrid ID="AttendanceHistoryRadGrid" runat="server" CellSpacing="0" 
                GridLines="None" Width="100%" 
                AutoGenerateColumns="False">
            <MasterTableView>
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>

            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>

                <Columns>
                    <telerik:GridBoundColumn DataField="EventDate" HeaderText="Event Date"
                        FilterControlAltText="Filter column column" UniqueName="column" 
                        DataFormatString="{0:d}" ItemStyle-Width="70" HeaderStyle-Width="70">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EventName"  HeaderText="Event"
                        FilterControlAltText="Filter column column" UniqueName="column">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CeuHours"  HeaderText="CEUs"
                        FilterControlAltText="Filter column column" UniqueName="column" ItemStyle-Width="50" HeaderStyle-Width="50">
                    </telerik:GridBoundColumn>
                </Columns>

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False">
            <WebServiceSettings>
            <ODataSettings InitialContainerName=""></ODataSettings>
            </WebServiceSettings>
            </FilterMenu>

            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
            <WebServiceSettings>
            <ODataSettings InitialContainerName=""></ODataSettings>
            </WebServiceSettings>
            </HeaderContextMenu>
            </telerik:RadGrid>

            <br />

        <div style="margin-top:5px; background-color:#d5d5d5; width:100%">
        <asp:FormView ID="UserFormView" runat="server" DataSourceID="AttendeeDataSource">
            <ItemTemplate>
                <table class="formview-table-layout" style="margin:5px; width:100%;">
                <tr>
                    <td>User ID:</td>
                    <td><telerik:RadTextBox ID="UserIdRadTextBox" runat="server" Text='<%# Bind("UserId") %>' ReadOnly="true" BorderStyle="None" /></td>
                </tr>
                <tr>
                    <td>Last Activity:</td>
                    <td><telerik:RadTextBox ID="LastActivityRadTextBox" runat="server" Text='<%# Bind("aspnet_Users.LastActivityDate") %>' ReadOnly="true" BorderStyle="None" /></td>
                </tr>
                </table>
            </ItemTemplate>
        </asp:FormView>
        </div>

    </td>
    </tr>
    </table>

    <asp:ObjectDataSource ID="OrganizationsDataSource" runat="server" 
        onselecting="OrganizationsDataSource_Selecting" 
        SelectMethod="GetOrganizationsByAssociation" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeeDataSource" runat="server" 
        SelectMethod="GetAttendee" TypeName="EventManager.Business.AttendeeMethods" 
            DataObjectTypeName="EventManager.Model.Attendee" DeleteMethod="Delete" 
            InsertMethod="AddAttendee" UpdateMethod="Update" oninserted="AttendeeDataSource_Inserted">
        <SelectParameters>
            <asp:QueryStringParameter Name="attendeeId" QueryStringField="AttendeeId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="StatesDataSource" runat="server" 
            SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
        </asp:ObjectDataSource>

</div> <!--end #content-->

</asp:Content>
