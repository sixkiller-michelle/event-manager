﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EditEvent.aspx.cs" Inherits="EventManager.Web.Association.EditEventForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
h3 {margin-top: 20px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="100%" Height="100%" Style="margin:0px; padding:0px;">
        
    <div style="width:100%; height:100%; margin:10px;">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" CssClass="errorMessage" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ValidationGroup="ValidationGroup1" ForeColor="Red" Display="None" />

    <h2>Event Details</h2>
            <asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource" 
            DataKeyNames="Id" Width="900px" DefaultMode="Edit"
            oniteminserted="FormView1_ItemInserted" 
            onitemupdated="FormView1_ItemUpdated" 
            onitemdeleted="FormView1_ItemDeleted" 
            oniteminserting="FormView1_ItemInserting" 
            OnModeChanged="FormView1_ModeChanged"
            onitemcreated="FormView1_ItemCreated"  >
            <EditItemTemplate>
                <table cellpadding="3" class="formview-table-layout">
                <tr>
                    <td colspan="4">
                         <asp:Button ID="Button3" runat="server" Text="Update" CausesValidation="True" CommandName="Update" />&nbsp
                         <asp:Button ID="Button4" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" /><br /><br />
                    </td>
                </tr>
                <tr>
                    <td>Event Type:</td>
                    <td colspan="3">
                            <telerik:RadComboBox ID="EventTypesDropDownList" runat="server" DataSourceID="EventTypesDataSource" 
                                DataTextField="TypeName" DataValueField="Id" SelectedValue='<%# Bind("TypeId") %>'>
                            </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>Title:</td>
                    <td colspan="3"><telerik:RadTextBox ID="EventNameTextBox" runat="server"  Text='<%# Bind("EventName") %>' Width="483px"   /></td>
                </tr>
                <tr>
                    <td>Start:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker1" Runat="server" SelectedDate='<%# Bind("StartDateTime") %>' Width="180">
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>End:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker2" Runat="server" SelectedDate='<%# Bind("EndDateTime") %>' Width="180">
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
            </table>

            <table cellpadding="3" class="formview-table-layout">
                <tr>
                    <td class="style1" colspan="4"><h3>Location</h3></td>
                </tr>
                <tr>
                    <td>Venue:</td>
                    <td colspan="3">
                        <telerik:RadTextBox ID="TextBox2" runat="server" Width="100%" Text='<%# Bind("Location") %>' /> </td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>
                        <telerik:RadTextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' Width="100" />
                    </td>
                    <td>State:</td>
                    <td align="right"><telerik:RadComboBox ID="StatesDropDownList" runat="server" DataSourceID="StatesDataSource" SelectedValue='<%# Bind("StateCode") %>'
                            DataTextField="StateName" DataValueField="StateCode" Width="150">
                        </telerik:RadComboBox></td>
                </tr>
           </table>
           
            <table cellpadding="3" class="formview-table-layout">
                <tr>
                    <td colspan="5"><h3>Certificate Printing</h3></td>
                </tr>
                 <tr>
                    <td>From:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker7" Runat="server"  Width="180"
                            DbSelectedDate='<%# Bind("CertificatesAvailableStartDate") %>' SharedCalendarID="sharedCalendar">
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>To:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker8" Runat="server" Width="180" SharedCalendarID="sharedCalendar"
                            DbSelectedDate='<%# Bind("CertificatesAvailableEndDate") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:CheckBox ID="SurveyRequiredCheckBox" Text="Survey is required" runat="server" Checked='<%# Bind("CertPrintRequireSurvey") %>'  />&nbsp;&nbsp;
                        <asp:CheckBox ID="PaymentRequiredCheckBox" Text="Payment is required" runat="server" Checked='<%# Bind("CertPrintRequirePayment") %>' /></td>
                </tr>
            </table>

            <table cellpadding="3" class="formview-table-layout">
               <tr>
                    <td class="style1" colspan="4"><h3>Registration Dates</h3></td>
                </tr>
                <tr>
                    <td>Online Registration Opens:</td>
                    <td>
                         <telerik:RadDateTimePicker ID="RadDateTimePicker3" Runat="server" Width="180"  SharedCalendarID="sharedCalendar"
                             DbSelectedDate='<%# Bind("OnlineRegStartDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>Closes:</td>
                    <td>
                         <telerik:RadDateTimePicker ID="RadDateTimePicker9" Runat="server"  Width="180" SharedCalendarID="sharedCalendar"
                             DbSelectedDate='<%# Bind("OnlineRegEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
                <tr>
                    <td>On-Site Registration Opens:</td>
                    <td>
                         <telerik:RadDateTimePicker ID="RadDateTimePicker4" Runat="server"  Width="180" SharedCalendarID="sharedCalendar"
                             DbSelectedDate='<%# Bind("OnSiteRegistrationDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" colspan="4"><h3>Board Meeting</h3></td>
                </tr>
                <tr>
                    <td>Start:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker5" Runat="server"  Width="180" SharedCalendarID="sharedCalendar"
                            DbSelectedDate='<%# Bind("BoardMeetingStartDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>End:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker6" Runat="server"  Width="180" SharedCalendarID="sharedCalendar"
                            DbSelectedDate='<%# Bind("BoardMeetingEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
                <tr>
                    <td>Room:</td>
                    <td colspan="4">
                        <telerik:RadTextBox ID="TextBox5" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' />
                    </td>
                </tr>
            </table>

             <table cellpadding="3" class="formview-table-layout">
                 <tr>
                    <td><h3>Payment</h3></td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="PayByCheckCheckBox" runat="server" Text="Allow payment by check" Checked='<%# Bind("AllowPayByCheck") %>' />
                    </td>
                </tr>
            </table>

            <table cellpadding="3" class="formview-table-layout">
                 <tr>
                    <td><h3>Visibility</h3></td>
                </tr>
                <tr>
                    <td>
                       <asp:DropDownList ID="EventVisibilityDropDownList" runat="server" Width="100" SelectedValue='<%# Bind("Visibility") %>'>
                            <asp:ListItem Text="Just Me" Value="Creator" />
                            <asp:ListItem Text="Association" Value="Association" />
                            <asp:ListItem Text="Public" Value="Public" />
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>

             <table cellpadding="3" class="formview-table-layout">
                 <tr>
                    <td><h3>Description</h3></td>
                </tr>
                <tr>
                    <td>Short Description (shown to facilities in event list)</td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="ShortDescRadTextBox" runat="server" Text='<%# Bind("ShortDesc") %>' MaxLength="255" EmptyMessage="Enter a short description" Width="500" Height="50" TextMode="MultiLine" Wrap="true" />
                    </td>
                </tr>
            </table>

           Full Description (shown to facilities on event details page)
               
            <telerik:radeditor ID="EventDescRadEditor" runat="server" 
                SkinID="DefaultSetOfTools" ToolbarMode="Default" Skin="Default" Height="600" Width="90%" 
                BackColor="White" ContentAreaCssFile="" ContentAreaMode="Div" 
                Content='<%# Bind(Container.DataItem, "EventDesc") %>' 
                oninit="EventDescRadEditor_Init">
            </telerik:radeditor>

            <div style="clear:both;"></div><br />
                <telerik:RadButton ID="UpdateRadButton" runat="server" Text="Update" CausesValidation="True" CommandName="Update" />&nbsp;
                <telerik:RadButton ID="CancelRadButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                <br /><br />
                <br />
                <telerik:RadButton ID="DeleteRadButton" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" ConfirmText="Are you sure you want to delete this event?" ConfirmTitle="Delete?" ConfirmDialogType="RadWindow" />

                 <telerik:RadCalendar ID="sharedCalendar" runat="server" EnableMultiSelect="false"
                    RangeMinDate="2006/01/01">
                </telerik:RadCalendar>

            </EditItemTemplate>
    
            <InsertItemTemplate>
                <table cellpadding="3" class="formview-table-layout">
                <tr>
                    <td>Event Type:</td>
                    <td>
                            <asp:DropDownList ID="EventTypesDropDownList" runat="server" DataSourceID="EventTypesDataSource" 
                                DataTextField="TypeName" DataValueField="Id" SelectedValue='<%# Bind("TypeId") %>'>
                            </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Title:</td>
                    <td><asp:TextBox ID="TextBox1" runat="server"  Text='<%# Bind("EventName") %>' Width="483px"   /></td>
                </tr>
                <telerik:radeditor ID="EventDescRadEditor" runat="server" SkinID="DefaultSetOfTools" ToolbarMode="Default" Skin="Default" Height="300" Width="90%" 
                        BackColor="White" ContentAreaCssFile="" ContentAreaMode="Div" Content='<%# Bind(Container.DataItem, "EventDesc") %>' oninit="EventDescRadEditor_Init">
                    </telerik:radeditor>
            </table>

            <table cellpadding="3" style="float:left;" class="formview-table-layout">
                 <tr>
                    <td colspan="2"><h3>Dates</h3></td>
                </tr>
                <tr>
                    <td>Start:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker1" Runat="server" 
                            SelectedDate='<%# Bind("StartDateTime") %>'
                            Culture="en-US">
                            <TimeView CellSpacing="-1" Interval="00:15:00" Width="300px">
                            </TimeView>
                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            <Calendar UseColumnHeadersAsSelectors="False" 
                                UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                            </Calendar>
                            <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                            </DateInput>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
                <tr>
                    <td>End:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker2" Runat="server" 
                            SelectedDate='<%# Bind("EndDateTime") %>' Culture="en-US">
                            <TimeView CellSpacing="-1" Interval="00:15:00">
                            </TimeView>
                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                ViewSelectorText="x">
                            </Calendar>
                            <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                            </DateInput>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        </telerik:RadDateTimePicker>
                    </td>
                </tr>
                 <tr>
                    <td class="style1" style="width: 161px">&nbsp;</td>
                    <td class="style2">Start:</td>
                    <td>&nbsp;</td>
                    <td>End:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">Online Registration Opens:</td>
                    <td class="style2">
                         <telerik:RadDateTimePicker ID="RadDateTimePicker3" Runat="server" 
                             DbSelectedDate='<%# Bind("OnlineRegStartDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td class="style1" style="width: 161px">Closes:</td>
                    <td class="style2">
                         <telerik:RadDateTimePicker ID="RadDateTimePicker9" Runat="server" 
                             DbSelectedDate='<%# Bind("OnlineRegEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">On-Site Registration Opens:</td>
                    <td class="style2">
                         <telerik:RadDateTimePicker ID="RadDateTimePicker4" Runat="server" 
                             DbSelectedDate='<%# Bind("OnSiteRegistrationDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Start:</td>
                    <td>&nbsp;</td>
                    <td>End:</td>
                    <td>Room:</td>
                </tr>
                <tr>
                    <td>Board Meeting:&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker5" Runat="server" 
                            DbSelectedDate='<%# Bind("BoardMeetingStartDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker6" Runat="server" 
                            DbSelectedDate='<%# Bind("BoardMeetingEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' />
                    </td>
                </tr>
                <tr>
                    <td>Certificates Available Online:</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker7" Runat="server" 
                            DbSelectedDate='<%# Bind("CertificatesAvailableStartDate") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker8" Runat="server" 
                            DbSelectedDate='<%# Bind("CertificatesAvailableEndDate") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">To Print Certificate:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="SurveyRequiredCheckBox" Text="Survey is required" runat="server" Checked='<%# Bind("CertPrintRequireSurvey") %>'   />&nbsp;&nbsp;
                        <asp:CheckBox ID="PaymentRequiredCheckBox" Text="Payment is required" runat="server" Checked='<%# Bind("CertPrintRequirePayment") %>'  /></td>
                </tr>
            </table>

            <table cellpadding="3" style="float:left; margin-left:10px;" class="formview-table-layout">
                <tr>
                    <td colspan="4"><h3>Location</h3></td>
                </tr>
                <tr>
                    <td>Venue:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox2" runat="server" Width="100%" Text='<%# Bind("Location") %>' /> </td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' />
                    </td>
                    <td>State:</td>
                    <td align="right"><asp:DropDownList ID="StatesDropDownList" runat="server" DataSourceID="StatesDataSource" SelectedValue='<%# Bind("StateCode") %>'
                            DataTextField="StateName" DataValueField="StateCode">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="style1">&nbsp;</td>
                    <td class="style2">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
           </table>
           <table cellpadding="3" style="float:left; margin-left:10px;" class="formview-table-layout">
                <tr>
                    <td colspan="2"><h3>Status & Visibility</h3></td>
                </tr>
                <tr>
                    <td class="style1">Status:</td>
                    <td class="style2">
                        <asp:DropDownList ID="EventStatusDropDownList" runat="server" Width="100"  SelectedValue='<%# Bind("EventStatusId") %>'>
                            <asp:ListItem Text="Future" Value="1" />
                            <asp:ListItem Text="Open" Value="2" />
                            <asp:ListItem Text="Closed" Value="3" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Visibility:</td>
                    <td>
                       <asp:DropDownList ID="EventVisibilityDropDownList" runat="server" Width="100" SelectedValue='<%# Bind("Visibility") %>'>
                            <asp:ListItem Text="Creator" Value="Creator" />
                            <asp:ListItem Text="Association" Value="Assocation" />
                            <asp:ListItem Text="Registered User" Value="Registered User" />
                            <asp:ListItem Text="Public" Value="Public" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <%--<tr>
                    <!--These are filled from RadEditor controls on FormView1_ItemUpdating -->
                    <td><asp:TextBox ID="EventDescTextBox" runat="server" Width="81px" Text='<%# Bind("EventDesc") %>' /></td>
                     <td><asp:TextBox ID="RegistrationInfoTextBox" runat="server" Width="81px" Text='<%# Bind("RegistrationInfo") %>' /></td>
                </tr>--%>
            </table>
                <div style="clear:both;"></div><br />
                <telerik:RadButton ID="InsertRadButton" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
                <telerik:RadButton ID="InsertCancelRadButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
            </InsertItemTemplate>
            
            <ItemTemplate>
            <table cellpadding="3" class="formview-table-layout">
                <tr>
                    <td class="style1">Event Type:</td>
                    <td><asp:TextBox ID="TextBox4" runat="server" Height="23px" Text='<%# Bind("EventType.TypeName") %>' Width="483px" ReadOnly="true"  />
                    </td>
                </tr>
                <tr>
                    <td class="style1">Title:</td>
                    <td><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EventName") %>' Width="483px" ReadOnly="true" /></td>
                </tr>
                <telerik:radeditor ID="EventDescRadEditor" runat="server" SkinID="DefaultSetOfTools" ToolbarMode="Default" Skin="Default" Height="300" Width="90%" 
                    BackColor="White" ContentAreaCssFile="" ContentAreaMode="Div" Content='<%# DataBinder.Eval(Container.DataItem, "EventDesc") %>'>
                    </telerik:radeditor>
            </table>

            <table cellpadding="3" style="float:left;" class="formview-table-layout">
                 <tr>
                    <td colspan="2"><h4>Dates</h4></td>
                </tr>
                <tr>
                    <td>Start:</td>
                    <td><asp:TextBox ID="TextBox7" runat="server" Height="23px" Text='<%# Bind("StartDateTime", "{0:g}") %>' Width="170px" ReadOnly="true"  />
                    </td>
                </tr>
                <tr>
                    <td>End:</td>
                    <td><asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("EndDateTime", "{0:g}") %>' Width="170px" ReadOnly="true"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">To Print Certificate:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="SurveyRequiredCheckBox" Text="Survey is required" runat="server" Checked='<%# Bind("CertPrintRequireSurvey") %>' Enabled="false"   />&nbsp;&nbsp;
                        <asp:CheckBox ID="PaymentRequiredCheckBox" Text="Payment is required" runat="server" Checked='<%# Bind("CertPrintRequirePayment") %>' Enabled="false"  /></td>
                </tr>
                <tr>
                <td class="style1" style="width: 161px">Online Registration Opens:</td>
                <td class="style2">
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("OnlineRegStartDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
               <td class="style1" style="width: 161px">Online Registration Closes:</td>
                <td class="style2">
                    <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("OnlineRegEndDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="style1" style="width: 161px">On-Site Registration Opens:</td>
                <td class="style2">
                   <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("OnSiteRegistrationDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="style1" style="width: 161px">&nbsp;</td>
                <td class="style2">Start:</td>
                <td>&nbsp;</td>
                <td>End:</td>
                <td>Room:</td>
            </tr>
            <tr>
            <td class="style1" style="width: 161px">Board Meeting:&nbsp;</td>
                <td class="style2"><asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("BoardMeetingStartDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
                <td><asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("BoardMeetingEndDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>
                    <asp:TextBox ID="TextBox15" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' ReadOnly="true" CssClass="readonly" />
                </td>
            </tr>
            <tr>
                <td class="style1" style="width: 161px">Certificates Available Online:</td>
                <td class="style2"><asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("CertificatesAvailableStartDate", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
                <td><asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("CertificatesAvailableEndDate", "{0:g}") %>' ReadOnly="true" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
            </tr>
            </table>

            <table cellpadding="3" style="float:left; margin-left:10px;" class="formview-table-layout">
                <tr>
                    <td class="style1" colspan="4"><h4>Location</h4></td>
                </tr>
                <tr>
                    <td class="style1">Venue:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox2" runat="server" Width="292px" Text='<%# Bind("Location") %>' ReadOnly="true" /> &nbsp;</td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' ReadOnly="true" /></td>
                    <td align="right">State:</td>
                    <td><asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("StateCode") %>' Width="50" ReadOnly="true" /></td>
                </tr>
            </table>
            <table cellpadding="3" style="float:left; margin-left:10px;" class="formview-table-layout">
                <tr>
                    <td colspan="2"><h4>Status & Visibility</h4></td>
                </tr>
                <tr>
                    <td class="style1">Status:</td>
                    <td class="style2"><asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("EventStatu.StatusDesc") %>' ReadOnly="true" />
                    </td>
                   
                </tr>
                <tr>
                    <td>Visibility:</td>
                    <td><asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("Visibility") %>' ReadOnly="true" />
                    </td>
                </tr>
            </table>
            <div style="clear:both;"></div><br />
            <asp:Button ID="EditButton" runat="server" Text="Edit"  CausesValidation="false" CommandName="Edit"/>&nbsp;
            </ItemTemplate>
           
            </asp:FormView>

        </div>
</asp:Panel>

    <asp:ObjectDataSource ID="EventDataSource" runat="server" DataObjectTypeName="EventManager.Model.Event" 
        DeleteMethod="Delete" SelectMethod="GetEvent" 
        TypeName="EventManager.Business.EventMethods" UpdateMethod="UpdateEvent" InsertMethod="AddEvent"
        oninserted="EventDataSource_Inserted" 
        onupdating="EventDataSource_Updating">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="EventTypesDataSource" runat="server" SelectMethod="GetEventTypes" 
        TypeName="EventManager.Business.EventMethods">
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" SelectMethod="GetStates" 
        TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>

</asp:Content>

