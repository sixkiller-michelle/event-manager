﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class EditAttendanceDialog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null || Request.QueryString["RegAttendeeId"] == null)
            {
                //Response.Redirect("~/Association/EventList.aspx");
            }

            if (!IsPostBack)
            {
                CeuTotalLabel.Text = "0";
                SessionCountLabel.Text = "0";
            }
        }

        protected void SessionsRadGrid_DataBound(object sender, EventArgs e)
        {
            int attendeeId = Convert.ToInt32(Request.QueryString["AttendeeId"]);
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            int regAttendeeId = Convert.ToInt32(Request.QueryString["RegAttendeeId"]);

            // Get session attendance for the attendee
            EventManager.Business.SessionAttendanceMethods m = new EventManager.Business.SessionAttendanceMethods();
            foreach (EventManager.Model.SessionAttendance af in m.GetAttendanceByRegId(regAttendeeId))
            {
                foreach (Telerik.Web.UI.GridDataItem dgi in SessionsRadGrid.Items)
                {
                    int sessionId = (int)dgi.OwnerTableView.DataKeyValues[dgi.ItemIndex]["Id"];
                    if (sessionId == af.SessionId)
                    { 
                        dgi.Selected = true;
                        break;
                    }
                        
                }
            }

            // Update the totals at top
            UpdateTotals();

        }

        protected void SessionsRadGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                e.Item.PreRender += new EventHandler(SessionsRadGrid_ItemPreRender);
            }
        }

        private void SessionsRadGrid_ItemPreRender(object sender, EventArgs e)
        {
            ((sender as GridDataItem)["CheckBoxTemplateColumn"].FindControl("CheckBox1") as System.Web.UI.WebControls.CheckBox).Checked = (sender as GridDataItem).Selected;
        }


        protected void SessionsRadGrid_PreRender(object sender, EventArgs e)
        {
            GridHeaderItem headerItem = SessionsRadGrid.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("headerChkbox") as System.Web.UI.WebControls.CheckBox).Checked = SessionsRadGrid.SelectedItems.Count == SessionsRadGrid.Items.Count;
        }

        private void UpdateTotals()
        {
            CeuTotalLabel.Text = GetCeuTotal().ToString("G");
            SessionCountLabel.Text = SessionsRadGrid.SelectedItems.Count.ToString();
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as System.Web.UI.WebControls.CheckBox).NamingContainer as Telerik.Web.UI.GridItem).Selected = (sender as System.Web.UI.WebControls.CheckBox).Checked;
            UpdateTotals();
        }

        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.CheckBox headerCheckBox = (sender as System.Web.UI.WebControls.CheckBox);
            foreach (GridDataItem dataItem in SessionsRadGrid.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as System.Web.UI.WebControls.CheckBox).Checked = headerCheckBox.Checked;
                dataItem.Selected = headerCheckBox.Checked;
            }
            UpdateTotals();
        }

        private decimal GetCeuTotal()
        {
            decimal total = 0;

            foreach (Telerik.Web.UI.GridDataItem dgi in SessionsRadGrid.Items)
            {
                int sessionId = (int)dgi.OwnerTableView.DataKeyValues[dgi.ItemIndex]["Id"];
                if (dgi.Selected)
                {
                    total = total + Convert.ToDecimal(dgi.Cells[5].Text);
                }
            }
            return total;
        }

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            // try to insert the attendance records
            try
            {
                int regAttendeeId = Convert.ToInt32(Request.QueryString["RegAttendeeId"]);

                // Get the registration attendee record
                //EventManager.Business.RegistrationAttendeesMethods m1 = new EventManager.Business.RegistrationAttendeesMethods();
                //EventManager.Model.RegistrationAttendee regAttendee = m1.GetRegistrationAttendee(regAttendeeId);

                EventManager.Business.SessionAttendanceMethods m = new EventManager.Business.SessionAttendanceMethods();

                m.DeleteAllAttendance(regAttendeeId);
                foreach (Telerik.Web.UI.GridDataItem dgi in SessionsRadGrid.Items)
                {
                    if (dgi.Selected == true)
                    {
                        EventManager.Model.SessionAttendance sa = new EventManager.Model.SessionAttendance();
                        sa.RegistrationAttendeeId = regAttendeeId;
                        sa.SessionId = (int)dgi.OwnerTableView.DataKeyValues[dgi.ItemIndex]["Id"];
                        sa.IsSpeaker = false;
                        m.Add(sa);
                    }
                        
                }
                m.SaveAllObjectChanges();

                string ceuTotal = CeuTotalLabel.Text;
                CeuTotalLabel.Text = "0";
                SessionCountLabel.Text = "0";
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('" + ceuTotal + "');", true);

                // Store the new ID in the hidden field
                //OrgIdLabel.Text = o.Id.ToString();
            }
            catch (Exception ex)
            {
                //CustomValidator1.ErrorMessage = ex.Message;
                //CustomValidator1.IsValid = false;
            }
        }

    }
}