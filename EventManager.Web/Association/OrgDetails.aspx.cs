﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Model;
using EventManager.Business;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class OrgDetails : System.Web.UI.Page
    {
        int newOrgId;
        Organization newOrg;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                if (Request.QueryString["OrgId"] == null)
                {
                    FormView1.DefaultMode = FormViewMode.Insert;
                    // Set focus to org name
                    Telerik.Web.UI.RadTextBox t = (Telerik.Web.UI.RadTextBox)FormView1.FindControl("OrgNameRadTextBox");
                    t.Focus();
                }
                else
                {
                    FormView1.DefaultMode = FormViewMode.Edit;
                }
            }
           
        }

        protected void FeeCategoriesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            // Find the selected org Type
            //int orgTypeId = Convert.ToInt32(((Telerik.Web.UI.RadComboBox)FormView1.FindControl("OrgTypeRadComboBox")).SelectedValue);
            //e.InputParameters["orgTypeId"] = orgTypeId;
            e.InputParameters[0] = UserInfo.AssociationId;
        }

        protected void OrgTypesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["AssociationId"] = UserInfo.AssociationId;
            e.Values["AdminAttendeeId"] = new Nullable<System.Int32>();

            RadTextBox bedsTextBox = (RadTextBox)FormView1.FindControl("BedCountRadTextBox");
            if (bedsTextBox != null && bedsTextBox.Text == "")
                e.Values["BedCount"] = 0;

            RadComboBox orgTypeCombo = (RadComboBox)FormView1.FindControl("OrgTypeRadComboBox");
            if (orgTypeCombo != null && String.IsNullOrEmpty(orgTypeCombo.SelectedValue))
            {
                e.Values["OrgTypeId"] = null;
            }

            RadComboBox feeCatCombo = (RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
            if (feeCatCombo != null && String.IsNullOrEmpty(feeCatCombo.SelectedValue))
            {
                e.Values["OrgFeeCategoryId"] = null;
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    OrgDataSource.DataBind();
                    FormView1.DataBind();
                }
                catch (Exception ex)
                {
                    e.KeepInEditMode = true;
                    e.ExceptionHandled = true;
                }
            }
            else
            {
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    Response.Redirect("~/Association/OrgDetails.aspx?OrgId=" + newOrgId.ToString());
                    //OrgDataSource.DataBind();
                    //e.KeepInInsertMode = true;
                    //FormView1.ChangeMode(FormViewMode.ReadOnly);
                    //FormView1.DataBind();
                }
                catch (Exception ex)
                {
                    e.KeepInInsertMode = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                CustomValidator1.IsValid = false;
                e.KeepInInsertMode = true;
            }
        }

        protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                am.SaveAllObjectChanges();
                Response.Redirect("~/Association/OrgList.aspx");
            }
            else
            {
               
            }
        }

        protected void OrgDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                newOrg = (Organization)e.ReturnValue;
                newOrgId = ((Organization)e.ReturnValue).Id;
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                CustomValidator1.IsValid = false;
            }
           
        }

        protected void NewAttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
            Organization o = om.GetOrganization(Convert.ToInt32(Request.QueryString["OrgId"]));

            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendee(Convert.ToInt32(e.Value));

            try
            {
                o.Attendees.Add(a);
                om.SaveAllObjectChanges();

                // Refresh the attendee grid
                AttendeesRadGrid.DataBind();

                // Reload the admin combo
                Telerik.Web.UI.RadComboBox c = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("AdminRadComboBox");
                FormView1.DataBind();
            }
            catch (Exception ex)
            { 
                
            }
            
        }

        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                //EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                //List<EventManager.Model.Attendee> attendees = am.GetAttendeesByName(assId, NewSpeakerRadComboBox.Text);
           
                EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                IQueryable<EventManager.Model.Attendee> query =

                    from att in ctx.Attendees
                    where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text) && att.IsApproved == true
                    orderby att.LastName, att.FirstName
                    select att;

                foreach (EventManager.Model.Attendee a in query)
                {
                    RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                    NewAttendeeRadComboBox.Items.Add(item);
                }
            }
            
        }

        protected void AttendeesRadGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int attendeeId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();

            Attendee a = am.GetAttendee(attendeeId);
            a.OrganizationId = null;
            am.SaveAllObjectChanges();

            AttendeesRadGrid.DataBind();
            FormView1.DataBind();
        }

        protected void OrgTypeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // Find the selected org Type
            int orgTypeId = Convert.ToInt32(e.Value);
            LoadFeeCategoryCombo(orgTypeId);
        }

        protected void FeeCategoryDropDownList_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            // Get the selected org type
            int orgTypeId = Convert.ToInt32(((Telerik.Web.UI.RadComboBox)FormView1.FindControl("OrgTypeRadComboBox")).SelectedValue);
            LoadFeeCategoryCombo(orgTypeId);
        }

        private void LoadFeeCategoryCombo(int orgTypeId)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();

            Telerik.Web.UI.RadComboBox feeCategoryCombo = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
            //feeCategoryCombo.DataSource = om.GetFeeCategoriesByOrgType(orgTypeId);
            feeCategoryCombo.DataBind();
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            // Make sure to set Admin (from combo)
            Telerik.Web.UI.RadComboBox adminCombo = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("AdminRadComboBox");
            if (adminCombo.SelectedIndex > 0)
                e.NewValues["AdminAttendeeId"] = adminCombo.SelectedValue;
            else
                e.NewValues["AdminAttendeeId"] = new System.Int32?();
        }

        protected void FormView2_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["AssociationId"] = UserInfo.AssociationId;
            e.Values["OrganizationId"] = Convert.ToInt32(Request.QueryString["OrgId"]);
            e.Values["IsApproved"] = true;
        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {

        }


    }
}