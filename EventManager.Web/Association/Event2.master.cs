﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class EventMaster2 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
             
                // Add EventId to all links (1st and second level)
                if (Request.QueryString["EventId"] != "" && Request.QueryString["EventId"] != null) // check to see if querystring exists
                {
                    foreach (Telerik.Web.UI.RadPanelItem m in EventMenuPanelBar.Items)
                    {
                        if (m.NavigateUrl != "")
                        {
                            m.NavigateUrl = m.NavigateUrl + "?EventId=" + Request.QueryString["EventId"];
                        }
                        foreach (Telerik.Web.UI.RadPanelItem m2 in m.Items)
                        {
                            if (m.Text == "Reports")
                                if (m2.Text != "Attendee Barcodes" && m2.Text != "Session Attendance")
                                    m2.NavigateUrl = m2.NavigateUrl + Request.QueryString["EventId"];
                                else
                                {
                                    m2.Items[0].NavigateUrl = m2.Items[0].NavigateUrl + Request.QueryString["EventId"];
                                    m2.Items[1].NavigateUrl = m2.Items[1].NavigateUrl + Request.QueryString["EventId"];
                                }
                            else
                                m2.NavigateUrl = m2.NavigateUrl + "?EventId=" + Request.QueryString["EventId"];
                        }
                    }
                }

                EventOverviewRadButton.NavigateUrl = EventOverviewRadButton.NavigateUrl + "?EventId=" + Request.QueryString["EventId"];
                EventOverviewRadButton.PostBackUrl = EventOverviewRadButton.PostBackUrl + "?EventId=" + Request.QueryString["EventId"];


                SetSelectedEventDetails();
                SetSelectedMenuItem();
            }
        }

        private void SetSelectedMenuItem()
        {
            //<telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventList.aspx" Text="Event List" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventOverview.aspx" Text="Overview" Value="Overview" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EditEvent.aspx" Text="Details" Value="Event Details" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDates.aspx" Text="Dates" Value="Dates" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDesc.aspx" Text="Description" Value="Description" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventRegInstruct.aspx" Text="Registration Instructions" Value="Reg Instructions" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventDocuments.aspx" Text="Documents" Value="Documents" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventSessions.aspx" Text="Sessions" Value="Sessions" />
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventReg.aspx" Text="Registrations" Value="Registrations" Flow="Vertical">
            //    <GroupSettings ExpandDirection="Down"></GroupSettings>
            //    <Items>
            //        <telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventReg.aspx" Text="Facility Summary">
            //        </telerik:RadMenuItem>
            //        <telerik:RadMenuItem runat="server" NavigateUrl="~/Association/EventRegAttendees.aspx" Text="Attendees">
            //        </telerik:RadMenuItem>
            //    </Items>
            //    <groupsettings flow="Horizontal" repeatdirection="Horizontal" />
            //</telerik:RadMenuItem>
            //<telerik:RadMenuItem NavigateUrl="~/Association/EventFlags.aspx" Text="Flags" Value="Flags" />

            //if (Request.ServerVariables["Path_Info"].Contains("/Association/EventList.aspx"))
            //    this.EventRadMenu.Items[0].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventOverview") ||
            //        Request.ServerVariables["Path_Info"].Contains("/Association/EditEvent") || 
            //        Request.ServerVariables["Path_Info"].Contains("/Association/EventDates") ||
            //        Request.ServerVariables["Path_Info"].Contains("/Association/EventDesc") ||
            //        Request.ServerVariables["Path_Info"].Contains("/Association/EventRegInstruct") ||
            //        Request.ServerVariables["Path_Info"].Contains("/Association/EventDocuments"))
            //    this.EventRadMenu.Items[1].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventSessions"))
            //    this.EventRadMenu.Items[2].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventFlags"))
            //    this.EventRadMenu.Items[3].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventRegAttendees.aspx"))
            //    this.EventRadMenu.Items[4].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventReg.aspx"))
            //    this.EventRadMenu.Items[5].Selected = true;
            //else if (Request.ServerVariables["Path_Info"].Contains("/Association/EventAttendance.aspx"))
            //    this.EventRadMenu.Items[6].Selected = true;
        }

        private void SetSelectedEventDetails()
        {
            if (Request.QueryString["EventId"] != null)
            {
                int eventID = 0;
                if (int.TryParse(Request.QueryString["EventId"], out eventID))
                {
                    EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
                    EventManager.Model.Event e = em.GetEvent(eventID);
                    if (e != null)
                    {
                        HeaderEventNameLabel.Text = e.EventName;
                        EventTypeLabel.Text = e.EventType.TypeName;
                        EventStartDateLabel.Text = e.StartDateTime.ToShortDateString() + " " + e.StartDateTime.ToShortTimeString();
                        EventStatusLabel.Text = e.EventStatu.StatusDesc;
                        EventVisibilityLabel.Text = e.Visibility;
                    }
                    else
                    {
                        //throw new ArgumentException("Event " + eventID.ToString() + " does not exist.");
                        EventCustomValidator.ErrorMessage = "Event ID " + eventID.ToString() + " does not exist.";
                        EventCustomValidator.IsValid = false;
                        EventMenuPanelBar.Enabled = false;
                        EventOverviewRadButton.Enabled = false;
                        RadPane2.Visible = false;
                    }
                }
                else
                {
                    //throw new ArgumentException(Request.QueryString["EventId"] + " is not a valid event ID");
                    EventCustomValidator.ErrorMessage = Request.QueryString["EventId"] + " is not a valid event ID";
                    EventCustomValidator.IsValid = false;
                    EventMenuPanelBar.Enabled = false;
                    EventOverviewRadButton.Enabled = false;
                    RadPane2.Visible = false;
                }
            }
        }
    }
}