﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using EventManager.Model;
using Telerik.Web.UI;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class OrgDetailsDialog : System.Web.UI.Page
    {
        Organization newOrg;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["OrgId"] == null || Request.QueryString["OrgId"] == "0")
            {
                FormView1.ChangeMode(FormViewMode.Insert);
            }
            try
            { 
                Telerik.Web.UI.RadTextBox orgName = (Telerik.Web.UI.RadTextBox)FormView1.FindControl("OrgNameRadTextBox");
                if (orgName != null)
                { 
                    orgName.Focus();
                }
            }
            catch(Exception)
            {
                
            }
            
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["AssociationId"] = UserInfo.AssociationId;

            RadTextBox bedCountTextBox = (RadTextBox)FormView1.FindControl("BedCountRadTextBox");
            if (bedCountTextBox != null && String.IsNullOrEmpty(bedCountTextBox.Text))
            {
                e.Values["BedCount"] = 0;
            }

            RadComboBox orgTypeCombo = (RadComboBox)FormView1.FindControl("OrgTypeRadComboBox");
            if (orgTypeCombo != null && String.IsNullOrEmpty(orgTypeCombo.SelectedValue))
            {
                e.Values["OrgTypeId"] = null;
            }

            RadComboBox feeCatCombo = (RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
            if (feeCatCombo != null && String.IsNullOrEmpty(feeCatCombo.SelectedValue))
            {
                e.Values["OrgFeeCategoryId"] = null;
            }
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                try
                {
                    am.SaveAllObjectChanges();

                    int orgId = newOrg.Id;
                    string orgName = newOrg.OrgName;

                    // Refill the session variable to include new org
                    EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();
                    Session["OrganizationsList"] = m.GetOrganizationsByAssociation(UserInfo.AssociationId);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "EditOrgDialog", "OrgAdded('" + orgId.ToString() + "', '" + orgName.Replace("'", "\\'") + "');", true);
                }
                catch (Exception ex)
                {
                    e.KeepInInsertMode = true;
                }
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.KeepInInsertMode = true;
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();

                    OrganizationMethods m = new OrganizationMethods();
                    Organization org = m.GetOrganization(Convert.ToInt32(Request.QueryString["OrgId"]));

                    // Refill the session variable to include new org
                    Session["OrganizationsList"] = m.GetOrganizationsByAssociation(UserInfo.AssociationId);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mykey", "OrgUpdated('" + org.Id.ToString() + "', '" + org.OrgName.Replace("'", "\\'") + "');", true);
                }
                catch (System.Data.UpdateException ex)
                {
                    if (ex.InnerException != null)
                    {
                        if (ex.InnerException.Message.Contains("Cannot insert duplicate key row"))
                        {
                            CustomValidator1.ErrorMessage = "There is already a facility with this name.";
                            CustomValidator1.IsValid = false;
                            e.ExceptionHandled = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    CustomValidator1.IsValid = false;
                    e.KeepInEditMode = true;
                    e.ExceptionHandled = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = (e.Exception.InnerException != null ? e.Exception.InnerException.Message : e.Exception.Message);
                CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }
        }

        

        protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();
                m.SaveAllObjectChanges();

                int orgId = Convert.ToInt32(Request.QueryString["OrgId"]);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mykey", "OrgDeleted('" + orgId.ToString() + "');", true);
            }
            else
            {

            }
        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseWindow();", true);
            }
        }

        protected void FeeCategoriesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void OrgTypesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void OrgDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            newOrg = (Organization)e.ReturnValue;
        }

        protected void OrgTypeRadComboBox_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // Find the selected org Type
            int orgTypeId = Convert.ToInt32(e.Value);

            if (FormView1.CurrentMode == FormViewMode.Insert)
            { 
                 // Reset the fee category combo
                Telerik.Web.UI.RadComboBox feeCategoryCombo = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
                feeCategoryCombo.DataSource = null;
                feeCategoryCombo.SelectedValue = null;
                LoadFeeCategoryCombo(orgTypeId);
            }
           
        }

        private void LoadFeeCategoryCombo(int orgTypeId)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();

            Telerik.Web.UI.RadComboBox feeCategoryCombo = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
            feeCategoryCombo.DataSource = om.GetFeeCategoriesByOrgType(orgTypeId);
            //feeCategoryCombo.DataBind();
        }

        protected void FeeCategoryDropDownList_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            // Get the selected org type
            int orgTypeId = Convert.ToInt32(((Telerik.Web.UI.RadComboBox)FormView1.FindControl("OrgTypeRadComboBox")).SelectedValue);
            LoadFeeCategoryCombo(orgTypeId);
        }

        protected void StateRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            { 
                RadComboBox c = (RadComboBox)sender;
                if (c.Items.Count > 1)
                {
                    c.SelectedValue = UserInfo.DefaultStateCode;
                }
            }
            
        }

        protected void FeeCategoryDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // Hide/show the member label
            RadComboBox feeCatCombo = (RadComboBox)sender;
            Label memberLabel = (Label)FormView1.FindControl("IsMemberLabel");
            Label nonMemberLabel = (Label)FormView1.FindControl("IsNonMemberLabel");

            if (feeCatCombo.SelectedItem != null)
            {
                int orgFeeCatId = Convert.ToInt32(feeCatCombo.SelectedValue);
                OrganizationMethods m = new OrganizationMethods();
                OrganizationFeeCategory cat = m.GetFeeCategory(orgFeeCatId);
                if (cat != null)
                {
                    memberLabel.Visible = cat.IsMemberCategory;
                    nonMemberLabel.Visible = !cat.IsMemberCategory;
                }
                else
                {
                    memberLabel.Visible = false;
                    nonMemberLabel.Visible = true;
                }
            }
            else
            {
                memberLabel.Visible = false;
                nonMemberLabel.Visible = true;
            }

        }

        protected void OrgDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            RadTextBox bedsTextBox = (RadTextBox)FormView1.FindControl("BedCountRadTextBox");

            Organization org = (Organization)e.InputParameters[0];
            if (org != null && bedsTextBox != null)
            {
                if (string.IsNullOrEmpty(bedsTextBox.Text.Trim()))
                    org.BedCount = 0;
            }
        }

        protected void OrgDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            RadTextBox bedsTextBox = (RadTextBox)FormView1.FindControl("BedCountRadTextBox");

            Organization org = (Organization)e.InputParameters[0];
            if (org != null && bedsTextBox != null)
            {
                if (string.IsNullOrEmpty(bedsTextBox.Text.Trim()))
                    org.BedCount = 0;
            }
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            RadTextBox bedCountTextBox = (RadTextBox)FormView1.FindControl("BedCountRadTextBox");
            if (bedCountTextBox != null && String.IsNullOrEmpty(bedCountTextBox.Text))
            {
                e.NewValues["BedCount"] = 0;
            }

            RadComboBox typeCombo = (RadComboBox)FormView1.FindControl("OrgTypeRadComboBox");
            if (typeCombo.SelectedIndex == 0)
            {
                e.NewValues["OrgTypeId"] = null;
            }

            RadComboBox feeCatCombo = (RadComboBox)FormView1.FindControl("FeeCategoryDropDownList");
            if (feeCatCombo.SelectedIndex == 0)
            {
                e.NewValues["OrgFeeCategoryId"] = null;
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc is System.Web.HttpException || exc is System.ArgumentOutOfRangeException)
            {
                if (exc is System.ArgumentOutOfRangeException)
                    errorMsg = "There is a problem with one of the properties of this facililty.";
                else
                    errorMsg = "An error occurred.";

                errorMsg = errorMsg + " The system administrator has been notified, and it will be fixed ASAP!";

                CustomValidator1.ErrorMessage = errorMsg;
                CustomValidator1.IsValid = false;

                // Log the exception and notify system operators
                ExceptionUtility.LogException(exc, "OrgDetailsDialog.aspx?" + Request.QueryString);
                ExceptionUtility.NotifySystemOps(exc, "OrgDetailsDialog.aspx?" + Request.QueryString);

                // Clear the error from the server
                Server.ClearError();
            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

    }
}