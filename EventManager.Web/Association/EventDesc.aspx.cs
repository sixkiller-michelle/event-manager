﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class EventDescForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

            if (!IsPostBack)
            {
                GetData();
            }
        }

        protected void GetData()
        {
            Telerik.Web.UI.RadEditor editor1 = RadEditor1;
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = em.GetEvent(Convert.ToInt32(Request.QueryString["EventId"]));
            if (evt != null)
            {
                editor1.Content = evt.EventDesc;
            }
        }

        protected void SaveRadButton_Click(object sender, EventArgs e)
        {
            Telerik.Web.UI.RadEditor editor1 = RadEditor1;
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = em.GetEvent(Convert.ToInt32(Request.QueryString["EventId"]));
            if (evt != null)
            {
                evt.EventDesc = editor1.Content;
                em.SaveAllObjectChanges();
            }
        }
    }
}