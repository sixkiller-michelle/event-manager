﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class NewEventForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorLabel.Visible = false;
        }

        protected void CreateEventRadButton_Click(object sender, EventArgs e)
        {
            EventManager.Model.Event ev = new EventManager.Model.Event();

            DateTime? selectedDate = EventEndDateTimePicker.SelectedDate;

            ev.AssociationId = UserInfo.AssociationId;
            ev.TypeId = Convert.ToInt32(EventTypesDropDownList.SelectedValue);
            ev.EventName = EventNameTextBox.Text;
            ev.StartDateTime = Convert.ToDateTime(EventStartDateTimePicker.SelectedDate);
            ev.EndDateTime = EventEndDateTimePicker.SelectedDate.GetValueOrDefault();
            ev.EventDesc = "";
            ev.EventStatusId = 2; // "Open"
            ev.Location = LocationTextBox.Text;
            ev.City = CityTextBox.Text;
            ev.StateCode = StatesDropDownList.SelectedValue.ToString();
            ev.Visibility = EventVisibilityDropDownList.SelectedValue;
            ev.CertPrintRequireSurvey = true;
            ev.CertPrintRequirePayment = true;

            EventManager.Business.AssociationEmployeeMethods am = new EventManager.Business.AssociationEmployeeMethods();
            AssociationEmployee emp = am.GetEmployeeByUserId(UserInfo.UserId);
            ev.CreatedByEmployeeId = emp.Id;
            ev.CreatedDateTime = DateTime.Now;

            // Add the event
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            try
            {
                ev = em.AddEvent(ev);
                em.SaveAllObjectChanges();
                Response.Redirect("~/Association/EditEvent.aspx?EventId=" + ev.Id.ToString());
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
                ErrorLabel.Visible = true;
            }

        }

        protected void StatesDropDownList_DataBound(object sender, EventArgs e)
        {
            RadComboBox statesCombo = (RadComboBox)sender;
            statesCombo.SelectedValue = UserInfo.DefaultStateCode;
        }
    }
}