﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class ScannerDataForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void FindAttendeeRadButton_Click(object sender, EventArgs e)
        {
            SessionsRadComboBox.SelectedIndex = 0;

            EventManager.Business.ScannerDataMethods m = new EventManager.Business.ScannerDataMethods();
            RadGrid1.DataSource = m.GetScanReport(Convert.ToInt32(Request.QueryString["EventId"]), Convert.ToInt32(AttendeeRadComboBox.SelectedValue), 0);
            RadGrid1.DataBind();
        }

        protected void FindSessionRadButton_Click(object sender, EventArgs e)
        {
            AttendeeRadComboBox.SelectedIndex = 0;

            EventManager.Business.ScannerDataMethods m = new EventManager.Business.ScannerDataMethods();
            RadGrid1.DataSource = m.GetScanReport(Convert.ToInt32(Request.QueryString["EventId"]), 0, Convert.ToInt32(SessionsRadComboBox.SelectedValue));
            RadGrid1.DataBind();
        }

        protected void GetResultsRadButton_Click(object sender, EventArgs e)
        {
            int attendeeId = 0;
            int sessionId = 0;

            if (AttendeeRadComboBox.SelectedIndex > 0)
                attendeeId = Convert.ToInt32(AttendeeRadComboBox.SelectedValue);

            if (SessionsRadComboBox.SelectedIndex > 0)
                sessionId = Convert.ToInt32(SessionsRadComboBox.SelectedValue);

            EventManager.Business.ScannerDataMethods m = new EventManager.Business.ScannerDataMethods();
            RadGrid1.DataSource = m.GetScanReport(Convert.ToInt32(Request.QueryString["EventId"]), attendeeId, sessionId);
            RadGrid1.DataBind();
        }

        protected void ClearFilterRadButton_Click(object sender, EventArgs e)
        {
            AttendeeRadComboBox.SelectedIndex = 0;
            SessionsRadComboBox.SelectedIndex = 0;

            RadGrid1.DataSource = null;
            RadGrid1.DataBind();

            //ClearAllFilters();
        }

        private void ClearAllFilters()
        {
            RadGrid1.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGrid1.MasterTableView.RenderColumns)
            {
                if (column.SupportsFiltering())
                {
                    column.CurrentFilterValue = string.Empty;
                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                }
            }
            RadGrid1.MasterTableView.Rebind();
        }

        protected void AttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (AttendeeRadComboBox.SelectedIndex != 0)
                ReportHyperlink.NavigateUrl = "~/Association/Reports2/ReportViewer.aspx?FileName=ScanReportGroupAttendee.rpt&ParmNames=@EventId|@SessionId|@AttendeeId&ParmValues=" + Request.QueryString["EventId"] + "|" + SessionsRadComboBox.SelectedValue + "|" + AttendeeRadComboBox.SelectedValue;
            else
                ReportHyperlink.NavigateUrl = "~/Association/Reports2/ReportViewer.aspx?FileName=ScanReportGroupSession.rpt&ParmNames=@EventId|@SessionId|@AttendeeId&ParmValues=" + Request.QueryString["EventId"] + "|" + SessionsRadComboBox.SelectedValue + "|" + AttendeeRadComboBox.SelectedValue;
        }

        protected void SessionsRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (AttendeeRadComboBox.SelectedIndex != 0)
                ReportHyperlink.NavigateUrl = "~/Association/Reports2/ReportViewer.aspx?FileName=ScanReportGroupAttendee.rpt&ParmNames=@EventId|@SessionId|@AttendeeId&ParmValues=" + Request.QueryString["EventId"] + "|" + SessionsRadComboBox.SelectedValue + "|" + AttendeeRadComboBox.SelectedValue;
            else
                ReportHyperlink.NavigateUrl = "~/Association/Reports2/ReportViewer.aspx?FileName=ScanReportGroupSession.rpt&ParmNames=@EventId|@SessionId|@AttendeeId&ParmValues=" + Request.QueryString["EventId"] + "|" + SessionsRadComboBox.SelectedValue + "|" + AttendeeRadComboBox.SelectedValue;
        }
    }
}