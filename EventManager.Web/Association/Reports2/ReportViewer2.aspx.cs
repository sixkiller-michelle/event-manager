using System;
using System.Data;
using System.ComponentModel;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Specialized;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using EventManager.Model;
using EventManager.Business;
using EventApp.Common;
using System.Reflection;

public partial class ReportViewer2 : System.Web.UI.Page
{
    private string rptFileName = "";
    private StringCollection _parameterNames = new StringCollection();
    private StringCollection _parameterValues = new StringCollection();
    private ReportDocument cryRpt = new ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Get the dataset from session
            string datasetName = Request.QueryString["Ds"].ToString();
            DataSet ds = (DataSet)Session[datasetName];

            // Get the Crystal Report filename from query string
            rptFileName = Request.QueryString["FileName"];

            // Get report params from query string
            if (Request.QueryString["ParmNames"] != null && Request.QueryString["ParmNames"] != "")
            { 
                string[] parmNames = Request.QueryString["ParmNames"].ToString().Split(char.Parse("|"));
                string[] parmValues = Request.QueryString["ParmValues"].ToString().Split(char.Parse("|"));

                foreach (string s in parmNames)
                    _parameterNames.Add(s);

                foreach (string s in parmValues)
                    _parameterValues.Add(s);
            }
           
            // Create the report
            string reportPath = HttpContext.Current.Server.MapPath(String.Format("~/Association/reports2/{0}", rptFileName));
            
            cryRpt.Load(reportPath);
            cryRpt.SetDataSource(ds);
            SetReportParameters();

            CrystalDecisions.Web.CrystalReportViewer crviewer = (CrystalDecisions.Web.CrystalReportViewer)this.FindControl("CrystalReportViewer1");
            crviewer.ReportSource = cryRpt;
            crviewer.PrintMode = CrystalDecisions.Web.PrintMode.Pdf;
            Session.Add("report", cryRpt);
        }
        else
        {
            CrystalDecisions.Web.CrystalReportViewer crviewer = (CrystalDecisions.Web.CrystalReportViewer)this.FindControl("CrystalReportViewer1");
            crviewer.PrintMode = CrystalDecisions.Web.PrintMode.Pdf;
            crviewer.ReportSource = Session["report"];
        }
    }

    public DataSet ToDataSet<T>(IList<T> list)
    {         
        Type elementType = typeof(T);         
        DataSet ds = new DataSet();         
        DataTable t = new DataTable();         
        
        ds.Tables.Add(t);          
        
        //add a column to table for each public property on T         
        foreach (var propInfo in elementType.GetProperties())
        {
            Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
            t.Columns.Add(propInfo.Name, ColType);
        }
        
        //go through each property on T and add each value to the table
        foreach (T item in list)
        {
            DataRow row = t.NewRow();
            foreach (var propInfo in elementType.GetProperties())
            {
                row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
            }
            t.Rows.Add(row);
        }
        
        return ds;
    } 

    private void LaunchReport()
    {
        System.IO.Stream st = null;

        try
        {
            st = ReportAsStream();

            byte[] b = new byte[st.Length];
            st.Read(b, 0, (int)st.Length);
            this.LaunchReport(b);
        }
        finally
        {
            if (st != null)
            {
                st.Dispose();
                st = null;
            }
        }
    }

    public System.IO.Stream ReportAsStream()
    {
        ReportDocument crReportDocument = ReportAsCrystalDocument();
        System.IO.Stream st = null;

        try
        {
            ExportOptions exp = new ExportOptions();
            exp.ExportFormatType = ExportFormatType.PortableDocFormat;
            PdfRtfWordFormatOptions myPdfFormatOptions = new PdfRtfWordFormatOptions();
            exp.FormatOptions = myPdfFormatOptions;

            //crReportDocument.FormatEngine.PrintOptions.PrinterName = "??";
            ExportRequestContext req = new ExportRequestContext();
            req.ExportInfo = exp;

            st = crReportDocument.FormatEngine.ExportToStream(req);            
        }
        catch (Exception ex)
        {
            st = null;
        }
        finally
        {
            if (crReportDocument != null)
            {
                crReportDocument.Dispose();
                crReportDocument = null;
            }
        }

        return st;
    }

    private ReportDocument ReportAsCrystalDocument()
    {
        try
        {
            string reportPath = HttpContext.Current.Server.MapPath(String.Format("~/reports/{0}", rptFileName));

            var reportDocument = new ReportDocument();
            reportDocument.Load(reportPath);

            //Setup the connection information structure to be used
            //to log onto the datasource for the report
            var conn = new ConnectionInfo();

            conn.ServerName = EventApp.Common.CommonUtils.GetConfigSetting("ReportDBServer");
            conn.DatabaseName = EventApp.Common.CommonUtils.GetConfigSetting("ReportDBCatalog");
            conn.UserID = EventApp.Common.CommonUtils.GetConfigSetting("ReportDBUser");
            conn.Password = EventApp.Common.CommonUtils.GetConfigSetting("ReportDBPassword");
            conn.IntegratedSecurity = false;

            //// Reattach the tables in the MAIN REPORT first.
            SetDBLogonForReport(conn, reportDocument);

            //SetReportParameters(reportDocument);

            // --Login to all tables in all subreports--
            SetDBLoginForSubreports(conn, reportDocument);

            return reportDocument;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument reportDocument)
    {
        try
        {
            Tables tables = reportDocument.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


    private void SetDBLoginForSubreports(ConnectionInfo connectionInfo, ReportDocument reportDocument)
    {
        try
        {
            Sections crSections = reportDocument.ReportDefinition.Sections;

            foreach (Section crSection in crSections)
            {
                ReportObjects crReportObjects = crSection.ReportObjects;

                foreach (ReportObject crReportObject in crReportObjects)
                {
                    if (crReportObject.Kind == ReportObjectKind.SubreportObject)
                    {
                        // If you find a subreport, typecast the reportobject to a subreport object 
                        SubreportObject crSubreportObject = (SubreportObject)crReportObject;

                        // Open the subreport 
                        ReportDocument subReportDocument = crSubreportObject.OpenSubreport(crSubreportObject.SubreportName);

                        SetDBLogonForReport(connectionInfo, subReportDocument);
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }

    }

    private void SetReportParameters()
    {
        try
        {
            ParameterFieldDefinitions crParameterFieldDefinitions;
            ParameterFieldDefinition crParameterFieldDefinition;
            ParameterValues crParameterValues;
            ParameterDiscreteValue crParameterDiscreteValue;

            //Get the collection of parameters from the report
            crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;

            for (int i = 0; i < _parameterNames.Count; i++)
            {
                string paramName = _parameterNames[i];
                string paramValue = _parameterValues[i];

                //Access the specified parameter from the collection
                crParameterFieldDefinition = crParameterFieldDefinitions[paramName];

                //Get the current values from the parameter field.  At this point
                //there are zero values set.
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                //Set the current values for the parameter field
                crParameterDiscreteValue = new ParameterDiscreteValue();

                switch (crParameterFieldDefinition.ParameterValueKind)
                {
                    case ParameterValueKind.StringParameter:
                        crParameterDiscreteValue.Value = paramValue;
                        break;
                    case ParameterValueKind.BooleanParameter:
                        crParameterDiscreteValue.Value = bool.Parse(paramValue);
                        break;
                    case ParameterValueKind.CurrencyParameter:
                        crParameterDiscreteValue.Value = decimal.Parse(paramValue);
                        break;
                    case ParameterValueKind.DateParameter:
                        crParameterDiscreteValue.Value = DateTime.Parse(paramValue);
                        break;
                    case ParameterValueKind.DateTimeParameter:
                        crParameterDiscreteValue.Value = DateTime.Parse(paramValue);
                        break;
                    case ParameterValueKind.NumberParameter:
                        crParameterDiscreteValue.Value = double.Parse(paramValue);
                        break;
                    case ParameterValueKind.TimeParameter:
                        crParameterDiscreteValue.Value = DateTime.Parse(paramValue);
                        break;
                    default:
                        break;
                }

                //Add the first current value for the parameter field
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);
            }

        }
        catch (Exception)
        {
            throw;
        }
    }


    private void LaunchReport(byte[] report)
    {
        Response.Buffer = true;
        Response.BufferOutput = true;
        Response.ClearHeaders();
        Response.ClearContent();
        Response.ContentType = "application/pdf";
        Response.BinaryWrite(report);
        Response.End();
    }

    protected void CrystalReportViewer1_Init(object sender, EventArgs e)
    {
     
    }
}