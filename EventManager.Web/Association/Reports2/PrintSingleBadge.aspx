﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintSingleBadge.aspx.cs" Inherits="EventManager.Web.Association.Reports2.PrintSingleBadge" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script src="http://labelwriter.com/software/dls/sdk/js/DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"> </script>
    <script src="DYMO.Label.Framework_2.0Beta.js" type="text/javascript" charset="UTF-8"> </script>

    <script type="text/javascript">
     //<![CDATA[
        window.onload = frameworkInitShim;
        }

        function startupCode() {
            initiatePrint();
        }
        function frameworkInitShim() {
            dymo.label.framework.init(startupCode); //init, then invoke a callback
        }

        //if (window.attachEvent) { window.attachEvent('onload', initiatePrint); }
        //else if (window.addEventListener) { window.addEventListener('load', initiatePrint, false); }
        //else { document.addEventListener('load', initiatePrint, false); }

        var printers;

        function initiatePrint() {

            //alert("getting printers...");
            printers = dymo.label.framework.getLabelWriterPrinters();

            // Check for printers installed
            if (printers.length == 0) {
                alert("No DYMO printers installed.");
                window.close();
            }
            else {

                fillPrinterList();
                // If one printer, just print and close.  Else, fill printer combo and wait for button press
//                if (printers.length == 1) {
//                    doPrint(printers[0].name);
//                }
//                else { 
//                    fillPrinterList();
//                }

            }
        }

        function doPrint(printerName) {

            var labelText = document.all("<%=SingleBadgeTextBox.ClientID %>").value;
            var label = dymo.label.framework.openLabelXml(labelText);
            var labelSet = new dymo.label.framework.LabelSetBuilder();
            
            var gridView = $find("<%=AttendeesRadGrid.MasterTableView.ClientID %>"); 

            for (i = 0; i < gridView.get_dataItems().length; i++) {
                var row = gridView.get_dataItems()[i];

                // Create a new label and add it to the label set
                var record = labelSet.addRecord();
             
                // Set label values (for checked row)
                record.setText("AssociationName", gridView.getCellByColumnUniqueName(row, "AssociationName").innerText);
                record.setText("EventStartDate", gridView.getCellByColumnUniqueName(row, "EventStartDate").innerText);
                record.setText("EventName", gridView.getCellByColumnUniqueName(row, "EventName").innerText);
                record.setText("AttendeeFirstName", gridView.getCellByColumnUniqueName(row, "AttendeeFirstName").innerText);
                record.setText("FullNameAndTitle", gridView.getCellByColumnUniqueName(row, "FullNameAndTitle").innerText);
                record.setText("OrgName", gridView.getCellByColumnUniqueName(row, "OrgName").innerText);
                record.setText("AttendeeId", gridView.getCellByColumnUniqueName(row, "AttendeeId").innerText);
                record.setText("FlagString", gridView.getCellByColumnUniqueName(row, "FlagString").innerText);
            }
                
            if (printerName.search("Twin") == "0") {
                label.print(printerName, '', labelSet);
            }
            else {
                var params;
                if (document.all("<%=LeftRadioButton.ClientID %>").checked) {
                    params = dymo.label.framework.createLabelWriterPrintParamsXml({ twinTurboRoll: dymo.label.framework.TwinTurboRoll.Left });
                }
                else {
                    params = dymo.label.framework.createLabelWriterPrintParamsXml({ twinTurboRoll: dymo.label.framework.TwinTurboRoll.Right });
                }
                label.print(printerName, params, labelSet);
            }

            //window.close();
        }

        function fillPrinterList() {

            var printerName = "";
            for (var i = 0; i < printers.length; ++i) {

                var printer = printers[i];

                var combo = $find("<%=PrintersRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(printer.name);
                combo.get_items().add(comboItem);
                combo.commitChanges();
            }
            if (printers.length > 0) {
                comboItem.select();
                combo.commitChanges();
            }
        }


        function PrintButtonClicked(sender, eventArgs) {
            // Print to the printer selected in the combo
            var combo = $find("<%=PrintersRadComboBox.ClientID %>");
            //alert("Printing to: " + combo.get_text());
            doPrint(combo.get_text());
        }

        function CloseButtonClicked(sender, eventArgs) {
            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.PrintBadgesWindowClosed();
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

     //]]>

    </script>

</head>
<body>
    <form id="form1" runat="server">

    <telerik:RadScriptManager ID="ScriptManager1" runat="server" />
    <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" />

    <asp:Label ID="MessageLabel" runat="server" Text="Please select a label printer:"></asp:Label>
  
    <br />
    <br />
    <telerik:RadComboBox ID="PrintersRadComboBox" runat="server" Width="300">
    </telerik:RadComboBox>
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Roll (for twin turbo printers):"></asp:Label>&nbsp;&nbsp;
     <asp:RadioButton ID="LeftRadioButton" runat="server" Text="Left" GroupName="PrinterRoll" Checked="true" />&nbsp;&nbsp;
    <asp:RadioButton ID="RightRadioButton" runat="server" Text="Right" GroupName="PrinterRoll" />
    <br />

    <br />

    <telerik:RadButton ID="btnStandard" runat="server" Text="Print Badge" AutoPostBack="false" UseSubmitBehavior="false" BackColor="LightGreen"
        OnClientClicked="PrintButtonClicked" Icon-PrimaryIconUrl="~/Images/print16x16.png"></telerik:RadButton>&nbsp;&nbsp;
    
    <telerik:RadButton ID="btnClose" runat="server" Text="Close" AutoPostBack="false" UseSubmitBehavior="false"
        OnClientClicked="CloseButtonClicked"></telerik:RadButton>

        <br /><br />
       

    <telerik:RadGrid ID="AttendeesRadGrid" runat="server" CellSpacing="0"
        GridLines="None" onitemdatabound="AttendeesRadGrid_ItemDataBound">
        <MasterTableView DataKeyNames="RegistrationAttendeeId"></MasterTableView>
    </telerik:RadGrid> 
    <br />

    <asp:TextBox ID="SingleBadgeTextBox" runat="server" TextMode="MultiLine" 
        Height="30px" Width="443px" />

    </form>
</body>
</html>
