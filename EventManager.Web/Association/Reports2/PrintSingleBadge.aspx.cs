﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EventManager.Model;
using Telerik.Web.UI;

namespace EventManager.Web.Association.Reports2
{
    public partial class PrintSingleBadge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetAttendeeBadgeInfo();
        }

        private void GetAttendeeBadgeInfo()
        {
            // Get the attendee registrations from the list
            EventManager.Model.Entities ctx = new EventManager.Model.Entities();

            List<int> regsToFind2 = new List<int>();
            List<string> ids = new List<string>(Request.QueryString["RegAttendeeId"].Split(','));
            foreach (string id in ids)
            {
                regsToFind2.Add(Convert.ToInt32(id));
            }

            AttendeesRadGrid.DataSource = from att in ctx.RegistrationAttendees
                                          where regsToFind2.Contains(att.Id)
                                          select new
                                          {
                                              AssociationName = att.Registration.Event.Association.AssociationName,
                                              RegistrationAttendeeId = att.Id,
                                              AttendeeId = att.AttendeeId,
                                              AttendeeFirstName = att.Attendee.FirstName,
                                              OrgName = (att.Registration.Organization == null ? "" : att.Registration.Organization.OrgName),
                                              Title = att.Title,
                                              AttendeeNameFNF = att.Attendee.FirstName.Trim() + " " + att.Attendee.LastName.Trim(),
                                              FullNameAndTitle = att.Attendee.FirstName.Trim() + " " +  att.Attendee.LastName.Trim() + ", " + att.Title.Trim(),
                                              EventId = att.Registration.EventId,
                                              EventName = att.Registration.Event.EventName,
                                              EventStartDate = att.Registration.Event.StartDateTime,
                                              EventEndDate = att.Registration.Event.EndDateTime,
                                              FlagString = ""
                                          };

            AttendeesRadGrid.DataBind();

            // Find the badge to use (look for it under the event directory.  If not there, grab the one under the assn directory)
            // Find the event ID of the registrations
            StreamReader sr;
            int firstRegId = (regsToFind2.Count > 0 ? regsToFind2[0] : 0);
            int assnId = UserInfo.AssociationId;
            string appFilePath = Server.MapPath("~/Association/Documents/AttendeeSingleBadge.label");
            string assnfilePath = Server.MapPath("~/Association/Documents/Associations/" + assnId.ToString() + "/AttendeeSingleBadge.label");
            string eventFilePath = "";
            if (regsToFind2.Count > 0)
            {
                RegistrationAttendee attendee = ctx.RegistrationAttendees.Where(r => r.Id == firstRegId).FirstOrDefault();
                if (attendee != null)
                {
                    eventFilePath = Server.MapPath("~/Association/Documents/Events/" + attendee.Registration.EventId.ToString() + "/AttendeeSingleBadge.label");
                }
            }
            
            if (eventFilePath != "" && File.Exists(eventFilePath))
                sr = File.OpenText(eventFilePath);
            else if (File.Exists(assnfilePath))
                sr = File.OpenText(assnfilePath);
            else
                sr = File.OpenText(appFilePath);

            // Get the "Single Badge" DYMO file and save the text to hidden field
            SingleBadgeTextBox.Text = sr.ReadToEnd();
            sr.Close();
        }

        private string GetFlagString(int regAttendeeId)
        {
            string s = "";
            using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
            {
                var regFlags = entities.RegistrationAttendeeFlags;

                var query =
                from flag in regFlags
                where flag.RegistrationAttendeeId == regAttendeeId && flag.EventFlag.PrintOnBadge == true
                select flag;

                foreach (RegistrationAttendeeFlag flag in query)
                {
                    s = (s == "" ? flag.EventFlag.FlagCode.Trim() : s + ", " + flag.EventFlag.FlagCode.Trim());
                }
            }
            return s;
        }

        protected void AttendeesRadGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is Telerik.Web.UI.GridDataItem)
            { 
                int regAttendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["RegistrationAttendeeId"]);
                e.Item.Cells[14].Text = GetFlagString(regAttendeeId);
            }
        }
    }
}