﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportTemp.aspx.cs" Inherits="ReportTemp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="font-size: medium;">
        <a href="ReportViewer.aspx?FileName=CertificateVertical.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=70|2460" target="_blank">Certificate Vertical (IHCA)</a><br />
        <a href="ReportViewer.aspx?FileName=CertificateVerticalUHCA2.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=92|8501" target="_blank">Certificate Vertical (UHCA)</a><br />
        <a href="ReportViewer.aspx?FileName=EventSessions.rpt&ParmNames=@EventId&ParmValues=70" target="_blank">Event Sessions</a><br />
        <a href="ReportViewer.aspx?FileName=AttendeeList.rpt&ParmNames=EventName|SortField|@EventId&ParmValues=2012 Spring Workshop|Organization|84" target="_blank">Attendee List</a><br />
        <a href="ReportViewer.aspx?FileName=EventSpeakers.rpt&ParmNames=@EventId&ParmValues=84" target="_blank">Event Speakers</a><br />
        <a href="ReportViewer.aspx?FileName=EventSurveyResults.rpt&ParmNames=@EventId&ParmValues=84" target="_blank">Event Survey Results</a><br />
        <a href="ReportViewer.aspx?FileName=SessionAttendance.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=84|0" target="_blank">Session Attendance</a><br />

        <br />
        <a href="ReportViewer.aspx?FileName=AttendeeBadges.rpt&ParmNames=EventName|EventStartDate|EventEndDate|@EventId|@AttendeeId|@OrgName&ParmValues=My Event|9-1-2011|9-5-2011|2|0|" target="_blank">Attendee Badge Report</a><br />
        <a href="ReportViewer.aspx?FileName=AttendeeList.rpt&ParmNames=EventName|SortField|@EventId|@AttendeeId|@OrgName&ParmValues=My Event||2|0|" target="_blank">Attendee List</a><br />
        <a href="ReportViewer.aspx?FileName=AttendeeSingleBadge.rpt&ParmNames=EventName|AttendeeId|AttendeeName|OrgName|FlagString|EventStartDate|EventEndDate&ParmValues=Dave Event|57|Dave Hopper|Apex Corp.|438849576|1/1/2000|1/1/2099" target="_blank">Attendee Single Badge</a><br />
        <a href="ReportViewer.aspx?FileName=AttendeeSingleBadgeIII.rpt&ParmNames=EventName|AttendeeId|AttendeeName|OrgName|FlagString|EventStartDate|EventEndDate&ParmValues=Dave Event|57|Dave Hopper|Apex Corp.|438849576|1/1/2000|1/1/2099" target="_blank">Attendee Single Badge III</a><br />
        <a href="ReportViewer.aspx?FileName=Certificate.rpt&ParmNames=EventName|EventStartDate|EventEndDate|EventLocation|@EventId&ParmValues=Dave Event|1/1/2000|1/1/2099|Vegas|2" target="_blank">Certificate</a><br />
        <a href="ReportViewer.aspx?FileName=EventContactSheet.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=2|0" target="_blank">Event Contact Sheet</a><br />
        <a href="ReportViewer.aspx?FileName=EventContactSheetLabels.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=2|0" target="_blank">Event Contact Sheet Labels</a><br />
        <a href="ReportViewer.aspx?FileName=Registrations.rpt&ParmNames=IsTradeShow|@EventId|@AttendeeId|@OrgName&ParmValues=False|13|0|" target="_blank">Registrations</a><br />
        <a href="ReportViewer.aspx?FileName=SessionAttendance.rpt&ParmNames=EventName|EventStartDate|EventEndDate|@SessionId&ParmValues=Dave Event|1/1/2000|1/1/2099|91" target="_blank">Session Attendance</a><br /><br />
        <a href="ReportViewer.aspx?FileName=CertificateReport.rpt&ParmNames=@EventId&ParmValues=2" target="_blank">Certificate Report</a><br />
        <a href="ReportViewer.aspx?FileName=ScanReport.rpt&ParmNames=@EventId&ParmValues=71" target="_blank">Scan Report</a>
    </div>
</asp:Content>

