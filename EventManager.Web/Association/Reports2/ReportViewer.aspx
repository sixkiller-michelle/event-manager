﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="EventManager.Web.Association.Reports2.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
    //<![CDATA[
        function CloseAndRebind(errorMsg) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.ReportViewerErrorOccurred(errorMsg);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CloseWindow() {
            GetRadWindow().close();
        }

        function CloseButton_Clicked(sender, args) {
            CloseWindow();
        }

        //]]>
    </script>
</head>

<body>
  
    <form id="form1" runat="server"> 
    
    <div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="An error has occurred" ForeColor="Red" Display="Dynamic" ValidationGroup="ValidationGroup1" ></asp:CustomValidator>

        <CR:CrystalReportViewer ID="CrystalReportViewer1"  runat="server" Width="100%" Height="100%"  
            ReportSourceID="CrystalReportSource1" oninit="CrystalReportViewer1_Init" 
            ToolPanelView="None" onunload="CrystalReportViewer1_Unload" 
            onerror="CrystalReportViewer1_Error" />

        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
