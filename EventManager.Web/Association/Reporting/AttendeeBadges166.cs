namespace EventManager.Web.Association.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AttendeeBadges166
    /// </summary>
    public partial class AttendeeBadges166 : Telerik.Reporting.Report
    {
        public AttendeeBadges166()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public static object[] ConvertStringArray(string value)
        {
            return value.Split(',');
        }

    }
}