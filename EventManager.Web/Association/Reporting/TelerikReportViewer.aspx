﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TelerikReportViewer.aspx.cs" Inherits="EventManager.Web.Association.Reporting.TelerikReportViewer" %>


<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.1.12.820, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    html, body, form
        {
            margin: 0px;
            padding: 0;
            height: 100%;
            width:100%;
        }
   </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height:100%; width:100%;">
    
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="98%" BorderStyle="None" BorderWidth="0"  
            Width="98%" ZoomMode="PageWidth" ZoomPercent="75" Style="margin:0 auto;">
            <typereportsource 
                typename="EventManager.Web.Association.Reporting.AttendeeBadges117, EventManager.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null">
            </typereportsource>
        </telerik:ReportViewer>
    
    </div>
    </form>
</body>
</html>
