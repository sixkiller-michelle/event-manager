namespace EventManager.Web.Association.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for AttendeeList.
    /// </summary>
    public partial class AttendeeList : Telerik.Reporting.Report
    {
        public AttendeeList()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            
        }

        private void AttendeeList_ItemDataBound(object sender, EventArgs e)
        {
           
        }

        private void AttendeeList_ItemDataBinding(object sender, EventArgs e)
        {
            Telerik.Reporting.Sorting sorting1 = new Telerik.Reporting.Sorting();
            if (ReportParameters["SortField"].Value.ToString() == "Facility")
                sorting1.Expression = "=Fields.Registration.Organization.OrgName";
            else
                sorting1.Expression = "=Fields.Attendee.FullName";
            sorting1.Direction = Telerik.Reporting.SortDirection.Asc;

            if (Report.Sortings.Count > 0)
                this.Report.Sortings.RemoveAt(0);

            this.Report.Sortings.Add(sorting1);
        }

    }
}