namespace EventManager.Web.Association.Reporting
{
    partial class ExhibitorBadges
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detail = new Telerik.Reporting.DetailSection();
            this.attendeeFirstNameDataTextBox = new Telerik.Reporting.TextBox();
            this.orgNameDataTextBox = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.ColumnSpacing = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.attendeeFirstNameDataTextBox,
            this.orgNameDataTextBox,
            this.textBox2,
            this.textBox1,
            this.textBox3,
            this.textBox4,
            this.textBox5});
            this.detail.Name = "detail";
            // 
            // attendeeFirstNameDataTextBox
            // 
            this.attendeeFirstNameDataTextBox.CanGrow = false;
            this.attendeeFirstNameDataTextBox.CanShrink = false;
            this.attendeeFirstNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D));
            this.attendeeFirstNameDataTextBox.Name = "attendeeFirstNameDataTextBox";
            this.attendeeFirstNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.39999982714653015D));
            this.attendeeFirstNameDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.attendeeFirstNameDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.attendeeFirstNameDataTextBox.Value = "=Fields.AttendeeFirstName";
            // 
            // orgNameDataTextBox
            // 
            this.orgNameDataTextBox.CanGrow = false;
            this.orgNameDataTextBox.CanShrink = false;
            this.orgNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.8001575469970703D));
            this.orgNameDataTextBox.Name = "orgNameDataTextBox";
            this.orgNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.orgNameDataTextBox.Style.Font.Bold = true;
            this.orgNameDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.orgNameDataTextBox.Value = "=Fields.OrgName";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6000787019729614D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9898195266723633D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "= IIF (Trim(Fields.Title) = \"\", Fields.AttendeeNameFNF, Fields.AttendeeNameFNF + " +
                "\", \" + Trim(Fields.Title))";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.01037724781781435D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9896228313446045D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "=Fields.AssnName";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.70000004768371582D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9896228313446045D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Value = "= IIF (Fields.EventStartDate.Date <> Fields.EventEndDate.Date, Fields.EventStartD" +
                "ate.Date + \" - \" + Fields.EventEndDate.Date, CDate(Fields.EventStartDate).ToShor" +
                "tDateString())";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.50007873773574829D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0001969337463379D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Value = "=Fields.EventName";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.01037724781781435D), Telerik.Reporting.Drawing.Unit.Inch(2.4000003337860107D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.97924542427063D), Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D));
            this.textBox5.Style.Color = System.Drawing.Color.Red;
            this.textBox5.Style.Font.Name = "Stencil";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Value = "EXHIBITOR";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "Attendee";
            this.objectDataSource1.DataSource = typeof(EventManager.Web.Association.Reports2.AttendeeBadgesDataSet);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // ExhibitorBadges
            // 
            this.DataSource = this.objectDataSource1;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "ExhibitorBadges";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.99960631132125854D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.25D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.24960629642009735D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(4.0001969337463379D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox attendeeFirstNameDataTextBox;
        private Telerik.Reporting.TextBox orgNameDataTextBox;
        private Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;

    }
}