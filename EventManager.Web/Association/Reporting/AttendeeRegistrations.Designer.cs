namespace EventManager.Web.Association.Reporting
{
    partial class AttendeeRegistrations
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter2 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.ReportParameter reportParameter3 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.notesCaptionTextBox = new Telerik.Reporting.TextBox();
            this.titleCaptionTextBox = new Telerik.Reporting.TextBox();
            this.orgNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.isSpeakerCaptionTextBox = new Telerik.Reporting.TextBox();
            this.flagsCaptionTextBox = new Telerik.Reporting.TextBox();
            this.createdDateCaptionTextBox = new Telerik.Reporting.TextBox();
            this.ceusCaptionTextBox = new Telerik.Reporting.TextBox();
            this.attendeeNameCaptionTextBox = new Telerik.Reporting.TextBox();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.titleTextBox = new Telerik.Reporting.TextBox();
            this.reportFooter = new Telerik.Reporting.ReportFooterSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.attendeeNameDataTextBox = new Telerik.Reporting.TextBox();
            this.ceusDataTextBox = new Telerik.Reporting.TextBox();
            this.createdDateDataTextBox = new Telerik.Reporting.TextBox();
            this.flagsDataTextBox = new Telerik.Reporting.TextBox();
            this.isSpeakerDataTextBox = new Telerik.Reporting.TextBox();
            this.notesDataTextBox = new Telerik.Reporting.TextBox();
            this.titleDataTextBox = new Telerik.Reporting.TextBox();
            this.orgNameDataTextBox = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.notesCaptionTextBox,
            this.titleCaptionTextBox,
            this.orgNameCaptionTextBox,
            this.isSpeakerCaptionTextBox,
            this.flagsCaptionTextBox,
            this.createdDateCaptionTextBox,
            this.ceusCaptionTextBox,
            this.attendeeNameCaptionTextBox});
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // notesCaptionTextBox
            // 
            this.notesCaptionTextBox.CanGrow = true;
            this.notesCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.notesCaptionTextBox.Name = "notesCaptionTextBox";
            this.notesCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.notesCaptionTextBox.StyleName = "Caption";
            this.notesCaptionTextBox.Value = "Notes";
            // 
            // titleCaptionTextBox
            // 
            this.titleCaptionTextBox.CanGrow = true;
            this.titleCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.titleCaptionTextBox.Name = "titleCaptionTextBox";
            this.titleCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.titleCaptionTextBox.StyleName = "Caption";
            this.titleCaptionTextBox.Value = "Title";
            // 
            // orgNameCaptionTextBox
            // 
            this.orgNameCaptionTextBox.CanGrow = true;
            this.orgNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.orgNameCaptionTextBox.Name = "orgNameCaptionTextBox";
            this.orgNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.orgNameCaptionTextBox.StyleName = "Caption";
            this.orgNameCaptionTextBox.Value = "Facility";
            // 
            // isSpeakerCaptionTextBox
            // 
            this.isSpeakerCaptionTextBox.CanGrow = true;
            this.isSpeakerCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.isSpeakerCaptionTextBox.Name = "isSpeakerCaptionTextBox";
            this.isSpeakerCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999967217445374D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.isSpeakerCaptionTextBox.StyleName = "Caption";
            this.isSpeakerCaptionTextBox.Value = "Spkr";
            // 
            // flagsCaptionTextBox
            // 
            this.flagsCaptionTextBox.CanGrow = true;
            this.flagsCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.400001049041748D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.flagsCaptionTextBox.Name = "flagsCaptionTextBox";
            this.flagsCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98385363817214966D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.flagsCaptionTextBox.StyleName = "Caption";
            this.flagsCaptionTextBox.Value = "Flags";
            // 
            // createdDateCaptionTextBox
            // 
            this.createdDateCaptionTextBox.CanGrow = true;
            this.createdDateCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.5000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.createdDateCaptionTextBox.Name = "createdDateCaptionTextBox";
            this.createdDateCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78385418653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.createdDateCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.createdDateCaptionTextBox.StyleName = "Caption";
            this.createdDateCaptionTextBox.Value = "Created";
            // 
            // ceusCaptionTextBox
            // 
            this.ceusCaptionTextBox.CanGrow = true;
            this.ceusCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(9.40000057220459D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.ceusCaptionTextBox.Name = "ceusCaptionTextBox";
            this.ceusCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49996042251586914D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.ceusCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.ceusCaptionTextBox.StyleName = "Caption";
            this.ceusCaptionTextBox.Value = "CEUs";
            // 
            // attendeeNameCaptionTextBox
            // 
            this.attendeeNameCaptionTextBox.CanGrow = true;
            this.attendeeNameCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.attendeeNameCaptionTextBox.Name = "attendeeNameCaptionTextBox";
            this.attendeeNameCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.attendeeNameCaptionTextBox.StyleName = "Caption";
            this.attendeeNameCaptionTextBox.Value = "Attendee";
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.43518003821372986D);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.textBox4});
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Style.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pageFooter.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pageFooter.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(2D);
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.13517983257770538D));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1979167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.currentTimeTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(9.1999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.13517983257770538D));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69996070861816406D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.pageInfoTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.13517983257770538D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7999998927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox4.Style.Font.Name = "Georgia";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Page:";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(9.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.31867137551307678D));
            this.titleTextBox.StyleName = "Title";
            this.titleTextBox.Value = "Registered Attendees";
            // 
            // reportFooter
            // 
            this.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28125D);
            this.reportFooter.Name = "reportFooter";
            this.reportFooter.Style.Visible = false;
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.41851344704627991D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.attendeeNameDataTextBox,
            this.ceusDataTextBox,
            this.createdDateDataTextBox,
            this.flagsDataTextBox,
            this.isSpeakerDataTextBox,
            this.notesDataTextBox,
            this.titleDataTextBox,
            this.orgNameDataTextBox});
            this.detail.Name = "detail";
            this.detail.Style.BorderColor.Default = System.Drawing.SystemColors.ControlLight;
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.detail.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.detail.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            // 
            // attendeeNameDataTextBox
            // 
            this.attendeeNameDataTextBox.CanGrow = true;
            this.attendeeNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.attendeeNameDataTextBox.Name = "attendeeNameDataTextBox";
            this.attendeeNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7791666984558106D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.attendeeNameDataTextBox.StyleName = "Data";
            this.attendeeNameDataTextBox.Value = "=Fields.AttendeeName";
            // 
            // ceusDataTextBox
            // 
            this.ceusDataTextBox.CanGrow = true;
            this.ceusDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(9.40000057220459D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.ceusDataTextBox.Name = "ceusDataTextBox";
            this.ceusDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49996042251586914D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.ceusDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.ceusDataTextBox.StyleName = "Data";
            this.ceusDataTextBox.Value = "=Fields.Ceus";
            // 
            // createdDateDataTextBox
            // 
            this.createdDateDataTextBox.CanGrow = true;
            this.createdDateDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.5000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.createdDateDataTextBox.Name = "createdDateDataTextBox";
            this.createdDateDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78385418653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.createdDateDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.createdDateDataTextBox.StyleName = "Data";
            this.createdDateDataTextBox.Value = "=Fields.CreatedDate";
            // 
            // flagsDataTextBox
            // 
            this.flagsDataTextBox.CanGrow = true;
            this.flagsDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.flagsDataTextBox.Name = "flagsDataTextBox";
            this.flagsDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98385429382324219D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.flagsDataTextBox.StyleName = "Data";
            this.flagsDataTextBox.Value = "=Fields.Flags";
            // 
            // isSpeakerDataTextBox
            // 
            this.isSpeakerDataTextBox.CanGrow = true;
            this.isSpeakerDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.isSpeakerDataTextBox.Name = "isSpeakerDataTextBox";
            this.isSpeakerDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999967217445374D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.isSpeakerDataTextBox.StyleName = "Data";
            this.isSpeakerDataTextBox.Value = "=Fields.IsSpeaker";
            // 
            // notesDataTextBox
            // 
            this.notesDataTextBox.CanGrow = true;
            this.notesDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.notesDataTextBox.Name = "notesDataTextBox";
            this.notesDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.notesDataTextBox.StyleName = "Data";
            this.notesDataTextBox.Value = "=Fields.Notes";
            // 
            // titleDataTextBox
            // 
            this.titleDataTextBox.CanGrow = true;
            this.titleDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.titleDataTextBox.Name = "titleDataTextBox";
            this.titleDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.titleDataTextBox.StyleName = "Data";
            this.titleDataTextBox.Value = "=Fields.Title";
            // 
            // orgNameDataTextBox
            // 
            this.orgNameDataTextBox.CanGrow = true;
            this.orgNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.11051511764526367D));
            this.orgNameDataTextBox.Name = "orgNameDataTextBox";
            this.orgNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.orgNameDataTextBox.StyleName = "Data";
            this.orgNameDataTextBox.Value = "=Fields.OrgName";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.010416666977107525D), Telerik.Reporting.Drawing.Unit.Inch(0.41875004768371582D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.4166669845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Value = "= Parameters.EventName.Value";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.010416666977107525D), Telerik.Reporting.Drawing.Unit.Inch(0.61882871389389038D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.3958334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox2.Value = "= Parameters.Location.Value";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.010416666977107525D), Telerik.Reporting.Drawing.Unit.Inch(0.81890767812728882D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.3958334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.Value = "= Parameters.CityState.Value";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "RegisteredAttendees";
            this.objectDataSource1.DataSource = typeof(EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.1082348823547363D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.titleTextBox,
            this.textBox1,
            this.textBox2,
            this.textBox3});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // AttendeeRegistrations
            // 
            this.DataSource = this.objectDataSource1;
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageFooter,
            this.reportFooter,
            this.detail,
            this.pageHeaderSection1});
            this.Name = "AttendeeRegistrations";
            this.PageSettings.Landscape = true;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "EventName";
            reportParameter2.Name = "Location";
            reportParameter3.Name = "CityState";
            this.ReportParameters.Add(reportParameter1);
            this.ReportParameters.Add(reportParameter2);
            this.ReportParameters.Add(reportParameter3);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(97)))), ((int)(((byte)(74)))));
            styleRule1.Style.Font.Name = "Georgia";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(174)))), ((int)(((byte)(173)))));
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(168)))), ((int)(((byte)(212)))));
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Dotted;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(238)))), ((int)(((byte)(243)))));
            styleRule2.Style.Font.Name = "Georgia";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Font.Name = "Georgia";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Font.Name = "Georgia";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(9.8999996185302734D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
        private Telerik.Reporting.TextBox attendeeNameCaptionTextBox;
        private Telerik.Reporting.TextBox ceusCaptionTextBox;
        private Telerik.Reporting.TextBox createdDateCaptionTextBox;
        private Telerik.Reporting.TextBox flagsCaptionTextBox;
        private Telerik.Reporting.TextBox isSpeakerCaptionTextBox;
        private Telerik.Reporting.TextBox notesCaptionTextBox;
        private Telerik.Reporting.TextBox orgNameCaptionTextBox;
        private Telerik.Reporting.TextBox titleCaptionTextBox;
        private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
        private Telerik.Reporting.Group labelsGroup;
        private Telerik.Reporting.PageFooterSection pageFooter;
        private Telerik.Reporting.TextBox currentTimeTextBox;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.TextBox titleTextBox;
        private Telerik.Reporting.ReportFooterSection reportFooter;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox attendeeNameDataTextBox;
        private Telerik.Reporting.TextBox ceusDataTextBox;
        private Telerik.Reporting.TextBox createdDateDataTextBox;
        private Telerik.Reporting.TextBox flagsDataTextBox;
        private Telerik.Reporting.TextBox isSpeakerDataTextBox;
        private Telerik.Reporting.TextBox notesDataTextBox;
        private Telerik.Reporting.TextBox orgNameDataTextBox;
        private Telerik.Reporting.TextBox titleDataTextBox;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;

    }
}