namespace EventManager.Web.Association.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for FacilityList.
    /// </summary>
    public partial class FacilityList : Telerik.Reporting.Report
    {
        public FacilityList()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            this.ID.Name = "ID";

        }
    }
}