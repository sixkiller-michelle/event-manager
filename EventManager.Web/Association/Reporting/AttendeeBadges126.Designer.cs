namespace EventManager.Web.Association.Reporting
{
    partial class AttendeeBadges126
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule9 = new Telerik.Reporting.Drawing.FormattingRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttendeeBadges126));
            this.detail = new Telerik.Reporting.DetailSection();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            this.objectDataSource2 = new Telerik.Reporting.ObjectDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.ColumnSpacing = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.barcode1,
            this.textBox1,
            this.textBox3,
            this.textBox2,
            this.textBox4,
            this.pictureBox1});
            this.detail.Name = "detail";
            // 
            // barcode1
            // 
            this.barcode1.Checksum = false;
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.barcode1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.barcode1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.barcode1.Symbology = Telerik.Reporting.Barcode.SymbologyType.Code39;
            this.barcode1.Value = "= Fields.AttendeeId";
            // 
            // textBox1
            // 
            this.textBox1.CanShrink = true;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8000788688659668D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.099921703338623D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Italic = true;
            this.textBox1.Style.Font.Name = "Goudy Old Style";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(36D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "= Fields.AttendeeFirstName";
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = false;
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.2999211847782135D));
            this.textBox3.Style.Font.Italic = true;
            this.textBox3.Style.Font.Name = "Goudy Old Style";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox3.TextWrap = false;
            this.textBox3.Value = "= Fields.AttendeeNameFNF";
            // 
            // textBox2
            // 
            this.textBox2.CanGrow = false;
            this.textBox2.CanShrink = true;
            formattingRule1.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Owner,Administrator,Corp. Officer\") ")});
            formattingRule1.Style.Color = System.Drawing.Color.DarkGray;
            formattingRule1.Style.Font.Name = "Goudy Old Style";
            formattingRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule2.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Nursing,CNA\")")});
            formattingRule2.Style.Color = System.Drawing.Color.Red;
            formattingRule2.Style.Font.Name = "Goudy Old Style";
            formattingRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule3.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Building Engineer,Maintenance\")")});
            formattingRule3.Style.Color = System.Drawing.Color.Brown;
            formattingRule3.Style.Font.Name = "Goudy Old Style";
            formattingRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule4.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Social Worker,Resident Advocate\")")});
            formattingRule4.Style.Color = System.Drawing.Color.Blue;
            formattingRule4.Style.Font.Name = "Goudy Old Style";
            formattingRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule5.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Recreation Therapy\")")});
            formattingRule5.Style.Color = System.Drawing.Color.Purple;
            formattingRule5.Style.Font.Name = "Goudy Old Style";
            formattingRule5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule6.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Admissions,Marketing\")")});
            formattingRule6.Style.Color = System.Drawing.Color.Green;
            formattingRule6.Style.Font.Name = "Goudy Old Style";
            formattingRule6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule7.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Dietary\")")});
            formattingRule7.Style.Color = System.Drawing.Color.Orange;
            formattingRule7.Style.Font.Name = "Goudy Old Style";
            formattingRule7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule8.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"Housekeeping\")")});
            formattingRule8.Style.Color = System.Drawing.Color.HotPink;
            formattingRule8.Style.Font.Name = "Goudy Old Style";
            formattingRule8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            formattingRule9.Filters.AddRange(new Telerik.Reporting.Filter[] {
            new Telerik.Reporting.Filter("=Fields.Title", Telerik.Reporting.FilterOperator.In, "= ConvertStringArray(\"ICF/ID\")")});
            formattingRule9.Style.Color = System.Drawing.Color.DarkBlue;
            formattingRule9.Style.Font.Name = "Goudy Old Style";
            formattingRule9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox2.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1,
            formattingRule2,
            formattingRule3,
            formattingRule4,
            formattingRule5,
            formattingRule6,
            formattingRule7,
            formattingRule8,
            formattingRule9});
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.5000001192092896D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.40000009536743164D));
            this.textBox2.Style.Color = System.Drawing.Color.Black;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Goudy Old Style";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.TextWrap = false;
            this.textBox2.Value = "= Fields.Title";
            // 
            // textBox4
            // 
            this.textBox4.CanGrow = false;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.9000792503356934D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.29992136359214783D));
            this.textBox4.Style.Font.Name = "Goudy Old Style";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.TextWrap = false;
            this.textBox4.Value = "= Fields.OrgName";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "Attendee";
            this.objectDataSource1.DataSource = typeof(EventManager.Web.Association.Reports2.AttendeeBadgesDataSet);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.Name = "objectDataSource2";
            // 
            // AttendeeBadges123
            // 
            this.DataSource = this.objectDataSource1;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "AttendeeBadges126";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.99960631132125854D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.25D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.24960629642009735D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(4.0103774070739746D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.ObjectDataSource objectDataSource2;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.PictureBox pictureBox1;

    }
}