﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;

namespace EventManager.Web.Association.Reporting
{
    public partial class TelerikReportViewer : System.Web.UI.Page
    {
        Telerik.Reporting.Report report;
        private StringCollection paramNames = new StringCollection();
        private StringCollection paramValues = new StringCollection();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["method"] != "push")
            {
                string typeName = "EventManager.Web.Association.Reporting." + Request.QueryString["FileName"];
              
                string[] parmNames = Request.QueryString["ParmNames"].ToString().Split(char.Parse("|"));
                string[] parmValues = Request.QueryString["ParmValues"].ToString().Split(char.Parse("|"));

                foreach (string s in parmNames)
                    paramNames.Add(s);

                foreach (string s in parmValues)
                    paramValues.Add(s);

                // Create the report using reflection, and set parameters
                Type t = Type.GetType(typeName + ", EventManager.Web");
                report = (Telerik.Reporting.Report)Activator.CreateInstance(t);

                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = report;

                SetReportParameters();
                ReportViewer1.ReportSource = instanceReportSource;
            }
            else
            { 
                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = (Telerik.Reporting.Report)Session["Report"];

                // Assigning the report to the report viewer.
                ReportViewer1.ReportSource = instanceReportSource;
                Session.Remove("Report");

                // Calling the RefreshReport method (only in WinForms applications).
                ReportViewer1.RefreshReport();
            }

        }

        private void SetReportParameters()
        {
            try
            {
                //ParameterFieldDefinitions crParameterFieldDefinitions;
                //ParameterFieldDefinition crParameterFieldDefinition;
                //ParameterValues crParameterValues;
                //ParameterDiscreteValue crParameterDiscreteValue;

                //Get the collection of parameters from the report
                //crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields;

                for (int i = 0; i < paramNames.Count; i++)
                {
                    string paramName = paramNames[i];
                    string paramValue = paramValues[i];

                    //Access the specified parameter from the collection
                    //crParameterFieldDefinition = crParameterFieldDefinitions[paramName];

                    //Get the current values from the parameter field.  At this point
                    //there are zero values set.
                    //crParameterValues = crParameterFieldDefinition.CurrentValues;

                    //Set the current values for the parameter field
                    //crParameterDiscreteValue = new ParameterDiscreteValue();

                    switch (report.ReportParameters[paramName].Type)
                    {
                        case Telerik.Reporting.ReportParameterType.String:
                            report.ReportParameters[paramName].Value = paramValue;
                            break;
                        case Telerik.Reporting.ReportParameterType.Boolean:
                            report.ReportParameters[paramName].Value = bool.Parse(paramValue);
                            break;
                        case Telerik.Reporting.ReportParameterType.DateTime:
                            report.ReportParameters[paramName].Value = (paramValue == "" ? DateTime.MinValue : DateTime.Parse(paramValue));
                            break;
                        case Telerik.Reporting.ReportParameterType.Integer:
                            report.ReportParameters[paramName].Value = Int32.Parse(paramValue);
                            break;
                        case Telerik.Reporting.ReportParameterType.Float:
                            report.ReportParameters[paramName].Value = Double.Parse(paramValue);
                            break;
                        default:
                            break;
                    }

                    //Add the first current value for the parameter field
                    //crParameterValues.Add(crParameterDiscreteValue);
                    //crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}