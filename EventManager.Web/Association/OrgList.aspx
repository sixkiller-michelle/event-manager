﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="OrgList.aspx.cs" Inherits="EventManager.Web.Association.OrgListForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <style type="text/css">
        #<%= RadGrid1GridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>

    <script type="text/javascript" id="telerikClientEvents1">
//<![CDATA[

	
//]]>
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        <!--
        var rowIndex;

        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("Insert") >= 0 ||
                    args.get_eventTarget().indexOf("Update") >= 0) {
                args.set_enableAjax(false);
            }
        }

        function ActionsRadMenu_ItemClicking(sender, args) {
        
            if (args.get_item().get_text() == "New") {
                ShowOrgDialog(0, null);
            }
            else if (args.get_item().get_text() == "Print Facility List") {
                var assnId = args.get_item().get_value();
                ShowReportViewerDialog(assnId)
                //args.set_cancel(true);
            }
            
        }

        function ShowOrgDialog(orgId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "EditOrgDialog");
            window.setSize(700, 450);
            return false;
        }

        function ShowStaffDialog(orgId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("OrgStaffDialog.aspx?OrgId=" + orgId, "EditStaffDialog");
            window.setSize(850, 450);
            return false;
        }

        function ShowReportViewerDialog(associationId) {

            window.radopen("Reporting/TelerikReportViewer.aspx?FileName=FacilityList&ParmNames=AssociationId&ParmValues=" + associationId, "ReportViewerDialog");
            window.setSize(850, 600);
            return false;
        }

        function RefreshOrgGrid(speakerString) {

            // Update the session row with the new speaker string
            var editedDataItem = $find("<%= RadGrid1.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            //editedDataItem.get_cell("SpeakerStringTemplateColumn").children[0].innerText = speakerString;
        }

        function OrgAdded(orgId, orgName) {

            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(orgId);
        }

        function OrgUpdated(orgId, orgName) {

            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(orgId);
        }

        function StaffUpdated(orgId, staffCount) {

            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(orgId);
        }

        -->
    </script>

</telerik:RadCodeBlock>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxManager runat="server" ID="RadAjaxManager1" 
        DefaultLoadingPanelID="RadAjaxLoadingPanel1" 
        OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" 
        onajaxrequest="RadAjaxManager1_AjaxRequest">
<ClientEvents OnRequestStart="onRequestStart" />
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGrid1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ActionsRadMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ActionsRadMenu" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>


<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderSize="0" Orientation="Horizontal">
    <telerik:RadPane ID="TopRadPane" runat="server" Scrolling="None" Height="70">
    
        <telerik:RadMenu ID="ActionsRadMenu" runat="server" Width="100%" 
            OnItemClick="ActionsRadMenu_ItemClick" Skin="Default" 
            OnClientItemClicking="ActionsRadMenu_ItemClicking">
        <Items>
            <telerik:RadMenuItem runat="server" Text="New" PostBack="false"></telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Print Facility List" Value="1" Enabled="true"></telerik:RadMenuItem>
        </Items>
        </telerik:RadMenu>

        <asp:Panel ID="HeaderPanel" runat="server" style="padding:10px;">
            <h2 style="margin-top:30px;">Facilities List</h2>
        </asp:Panel>
        
    </telerik:RadPane>
   <%-- <telerik:RadSplitBar ID="RadSplitBar1" runat="server" Enabled="false"  ></telerik:RadSplitBar>--%>
    <telerik:RadPane ID="BottomRadPane" runat="server" Scrolling="None">
    
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
            AllowPaging="True" AllowSorting="False" Height="100%" PageSize="20" 
            AutoGenerateColumns="False" CellSpacing="0" DataSourceID="OrgDataSource" 
            GridLines="None" PagerStyle-Position="TopAndBottom" 
            onitemcreated="RadGrid1_ItemCreated" >
            <GroupingSettings CaseSensitive="false" />
            <ClientSettings>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <MasterTableView datasourceid="OrgDataSource" DataKeyNames="Id">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>

            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>

                <Columns>
                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" AllowFiltering="true" HeaderText="Id" SortExpression="Id" UniqueName="Id" 
                        HeaderStyle-Width="80" ItemStyle-Width="80">
                        <FilterTemplate>Clear filters
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/Images/filterCancel.gif" AlternateText="Show All" ToolTip="Show All" OnClick="btnShowAll_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridBoundColumn>

                  <%--  <telerik:GridHyperLinkColumn FilterControlAltText="Search by facility name" FilterControlWidth="150" ItemStyle-Width="180" HeaderStyle-Width="180"
                                UniqueName="OrgNameHyperlink" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                                HeaderText="Facility"
                                DataType="System.String"
                                DataTextField="OrgName"
                                DataNavigateUrlFields="Id"
                                DataNavigateUrlFormatString="OrgDetails.aspx?OrgId={0}">
                        </telerik:GridHyperLinkColumn>
--%>

                    <telerik:GridTemplateColumn UniqueName="OrgNameTemplateColumn" DataField="OrgName" HeaderText="Facility" HeaderStyle-HorizontalAlign="Left" 
                            AllowFiltering="true" FilterControlWidth="150px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                            SortExpression="OrgName" 
                            HeaderStyle-Width="180px" ItemStyle-Width="180">
                            <ItemTemplate>
                                <asp:LinkButton ID="OrgNameLinkButton" runat="server" Text='<%# Eval("OrgName") %>' CommandName="ViewOrgDetails" />
                            </ItemTemplate>
                            <HeaderStyle Width="180px"></HeaderStyle>
                        </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="StaffCountTemplateColumn" DataField="StaffCount" HeaderText="Staff" HeaderStyle-HorizontalAlign="Right" 
                            AllowFiltering="false" SortExpression="StaffCount" HeaderStyle-Width="50px" ItemStyle-Width="50">
                            <ItemTemplate>
                                <asp:LinkButton ID="StaffCountLinkButton" runat="server" Text='<%# Eval("StaffCount") %>' CommandName="ViewStaffDialog" />
                            </ItemTemplate>
                            <HeaderStyle Width="50px"></HeaderStyle>
                        </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="OrgTypeTemplateColumn" DataField="OrgTypeId" HeaderText="Type" SortExpression="OrgTypeId"  HeaderStyle-Width="150px" ItemStyle-Width="150">
                      <ItemTemplate>
                            <%# Eval("OrganizationType.OrgTypeName")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                        <FilterTemplate>
                            <telerik:RadComboBox ID="RadComboBoxOrgType" DataSourceID="OrgTypeDataSource" DataTextField="OrgTypeName"
                                DataValueField="Id" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("OrgTypeTemplateColumn").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="OrgTypeIndexChanged" Width="120" DropDownWidth="200">
                                <Items>
                                    <telerik:RadComboBoxItem Text="All" />
                                </Items>
                            </telerik:RadComboBox>
                            <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

                                <script type="text/javascript">
                                    function OrgTypeIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("OrgTypeTemplateColumn", args.get_item().get_value(), "EqualTo");

                                    }
                                </script>

                            </telerik:RadScriptBlock>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridCheckBoxColumn DataField="IsMember" DataType="System.Boolean" FilterControlAltText="Filter IsMember column" HeaderText="Member" 
                        HeaderStyle-Width="60" ItemStyle-Width="200" SortExpression="IsMember" UniqueName="IsMember"
                        CurrentFilterFunction="NoFilter" ShowFilterIcon="true" AutoPostBackOnFilter="true">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="AdminName" AllowFiltering="false" HeaderText="Admin Name" SortExpression="AdminName" UniqueName="AdminName" ItemStyle-Width="120" HeaderStyle-Width="120">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AdminEmail"  AllowFiltering="false" HeaderText="Admin Email" SortExpression="AdminEmail" UniqueName="AdminEmail" ItemStyle-Width="150" HeaderStyle-Width="150">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AdminPhone"  AllowFiltering="false" HeaderText="Phone" UniqueName="AdminPhoneGridBoundColumn" HeaderStyle-Width="80" ItemStyle-Width="80">
                    </telerik:GridBoundColumn>
                

                    <telerik:GridTemplateColumn UniqueName="FeeCategoryTemplateColumn" DataField="OrgFeeCategoryId" HeaderText="Fee Category" SortExpression="OrgFeeCategoryId"  HeaderStyle-Width="150px" ItemStyle-Width="150">
                      <ItemTemplate>
                            <%# Eval("OrganizationFeeCategory.CategoryDesc")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                        <FilterTemplate>
                            <telerik:RadComboBox ID="RadComboBoxFeeCategory" DataSourceID="FeeCategoryDataSource" DataTextField="CategoryDesc"
                                DataValueField="Id" Width="120" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("FeeCategoryTemplateColumn").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="FeeCategoryIndexChanged" DropDownWidth="200">
                                <Items>
                                    <telerik:RadComboBoxItem Text="All" />
                                </Items>
                            </telerik:RadComboBox>
                            <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                                <script type="text/javascript">
                                    function FeeCategoryIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("FeeCategoryTemplateColumn", args.get_item().get_value(), "EqualTo");

                                    }
                                </script>

                            </telerik:RadScriptBlock>
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>

                </Columns>

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>
            <PagerStyle Position="TopAndBottom"></PagerStyle>
            <FilterMenu EnableImageSprites="False"></FilterMenu>

            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
        </telerik:RadGrid>

    </telerik:RadPane>
</telerik:RadSplitter>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" EnableShadow="false">
    <Windows>
        <telerik:RadWindow ID="EditOrgDialog" runat="server" Title="Edit Facility" Height="400px"
            Width="650px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
        <telerik:RadWindow ID="EditStaffDialog" runat="server" Title="Edit Staff" Height="460px"
            Width="850px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
        <telerik:RadWindow ID="ReportViewerDialog" runat="server" Title="Report" Height="600px"
            Width="850px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="false"
            Modal="true" />
    </Windows>
</telerik:RadWindowManager>
    
<asp:ObjectDataSource ID="OrgDataSource" runat="server" onselecting="OrgDataSource_Selecting" SelectMethod="GetOrganizationsByAssociation" TypeName="EventManager.Business.OrganizationMethods" OldValuesParameterFormatString="original_{0}">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="FeeCategoryDataSource" runat="server" SelectMethod="GetFeeCategories" TypeName="EventManager.Business.OrganizationMethods" OldValuesParameterFormatString="original_{0}" onselecting="FeeCategoryDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="OrgTypeDataSource" runat="server" SelectMethod="GetOrganizationTypes" TypeName="EventManager.Business.OrganizationMethods" OldValuesParameterFormatString="original_{0}" onselecting="OrgTypeDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>


</asp:Content>
