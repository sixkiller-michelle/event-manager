﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class EventRegAttendeesList : System.Web.UI.Page
    {
        public string RegRadGridPanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
        {
            if (e.Updated.ID == "GridListView")
            {
                this.RegRadGridPanelClientID = e.UpdatePanel.ClientID;
            }
        }

        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                List<EventManager.Model.Attendee> attendees = GetAttendeesList(assnId).Where(l => (l.LastName + ", " + l.FirstName).StartsWith(e.Text, true, null)).OrderBy(l => l.LastName).ThenBy(l => l.FirstName).ToList();
                //List<EventManager.Model.Attendee> attendees = GetAttendeesList(assnId, false);
                if (attendees.Count > 0)
                {
                    NewAttendeeRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Attendee a in attendees)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                        NewAttendeeRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewAttendeeRadComboBox.ClearSelection();
                }
            }
        }

        protected void NewAttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.Text))
            {
                try
                {
                    if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                    {
                        //NewAttendeePanel.Visible = false;
                        int attendeeId = Convert.ToInt32(e.Value);

                        // Get the selected attendee from drop down
                        EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                        EventManager.Model.Attendee a = am.GetAttendee(attendeeId);

                        // Pre-fill their org and title (if available)
                        if (a.Organization != null && !String.IsNullOrEmpty(a.Organization.OrgName))
                        {
                            //RadComboBoxItem comboItem = NewRegFacilityRadComboBox.FindItemByValue(a.OrganizationId.ToString());
                            //NewRegFacilityRadComboBox.SelectedValue = a.OrganizationId.ToString();
                            //NewRegFacilityRadComboBox.Focus();

                            NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Text = "";
                            RadComboBoxItem item = new RadComboBoxItem(a.Organization.OrgName, a.Organization.Id.ToString());
                            NewRegFacilityRadComboBox.Items.Insert(0, item);
                            NewRegFacilityRadComboBox.SelectedIndex = 0;
                            NewRegFacilityRadComboBox.Focus();
                           
                        }
                        else
                        {
                            NewRegFacilityRadComboBox.Text = "";
                            //NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Focus();
                            //NewFacilityPanel.Visible = true;
                        }

                        NewRegTitleTextBox.Text = a.Title.Trim();
                    }
                    else
                    {
                        //NewRegFacilityRadComboBox.Items.Clear();
                        NewRegFacilityRadComboBox.Text = "";
                        NewRegTitleTextBox.Text = "";
                        NewRegFacilityRadComboBox.Focus();
                    }

                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }

            }
            else
            {
                //NewAttendeePanel.Visible = false
            }

        }

        protected void NewRegFacilityRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                List<EventManager.Model.Organization> orgs = GetFacilitiesList(assnId).Where(l => l.OrgName.StartsWith(e.Text)).ToList();
                if (orgs.Count > 0)
                {
                    NewRegFacilityRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Organization a in orgs)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.OrgName, a.Id.ToString());
                        NewRegFacilityRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewRegFacilityRadComboBox.ClearSelection();
                }

            }
        }

        protected void NewRegFacilityRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.Text))
            {
                try
                {
                    if (String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        //NewFacilityPanel.Visible = true;
                        //NewFacilityNameTextBox.Text = NewRegFacilityRadComboBox.Text.Trim();
                    }
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }
            }
        }


        private List<Attendee> GetAttendeesList(int assnId, bool refresh = false)
        {
            List<Attendee> attendees;
            if (Session["AttendeesList"] == null || refresh == true)
            {
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                attendees = m.GetAttendees(assnId);
                Session["AttendeesList"] = attendees;
            }
            else
            {
                attendees = (List<Attendee>)Session["AttendeesList"];
            }
            return attendees.OrderBy(a => a.LastName).ToList();
        }

        private List<Organization> GetFacilitiesList(int assnId, bool refresh = true)
        {
            List<Organization> facilities;
            if (Session["OrganizationsList"] == null || refresh == true)
            {
                EventManager.Business.OrganizationMethods m2 = new EventManager.Business.OrganizationMethods();
                facilities = m2.GetOrganizationsByAssociation(assnId);
                Session["OrganizationsList"] = facilities;
            }
            else
            {
                facilities = (List<Organization>)Session["OrganizationsList"];
            }
            return facilities.OrderBy(f => f.OrgName).ToList();
        }

        protected void NewRegAddButton_Click(object sender, EventArgs e)
        {
            try
            {
                int assnId = UserInfo.AssociationId;
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                Guid userId = UserInfo.UserId;
                EventManager.Model.Organization org = null;
                EventManager.Model.Attendee att = null;
                string title = "";

                EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();

                // Find the attendee (or create, if one is not selected)
                if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                {
                    att = am.GetAttendee(Convert.ToInt32(NewAttendeeRadComboBox.SelectedValue));
                }
                else if (NewAttendeeRadComboBox.Text.Trim().Length > 3)
                {
                    List<Attendee> matchingAttendees = am.GetAttendeesByName(assnId, NewAttendeeRadComboBox.Text.Trim());
                    if (matchingAttendees.Count > 0)
                        att = matchingAttendees[0];

                    if (att == null)
                    {
                        string firstName = GetFirstName(NewAttendeeRadComboBox.Text);
                        string lastName = GetLastName(NewAttendeeRadComboBox.Text);
                        att = am.AddAttendee(firstName, lastName, "", assnId);
                        if (att != null)
                        {
                            // Add the newly entered attendee to the session list
                            List<Attendee> attendeeList = (List<Attendee>)Session["AttendeesList"];
                            attendeeList.Add(att);
                            Session["AttendeesList"] = attendeeList;

                            //FillAttendeeCombo();
                        }
                    }
                }

                if (att != null)
                {
                    // Get the selected org (or 'null' for individual registration)
                    if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        org = om.GetOrganization(Convert.ToInt32(NewRegFacilityRadComboBox.SelectedValue));
                    }
                    else if (NewRegFacilityRadComboBox.Text.Trim().Length > 1)
                    {
                        org = om.GetOrganizationByName(assnId, NewRegFacilityRadComboBox.Text.Trim());
                        if (org == null)
                        {
                            org = om.AddOrganization(assnId, NewRegFacilityRadComboBox.Text.Trim());
                            if (org != null)
                            {
                                List<Organization> orgList = (List<Organization>)Session["OrganizationsList"];
                                orgList.Add(org);
                                Session["OrganizationsList"] = orgList;
                                //FillFacilityCombo();
                            }
                        }

                    }

                    // Get the title
                    title = NewRegTitleTextBox.Text;

                    EventManager.Business.RegistrationMethods efm = new EventManager.Business.RegistrationMethods();
                    efm.AddRegistrationWithAttendee(eventId, org, att, title, userId);

                    efm.SaveAllObjectChanges();

                    // Show success message
                    NewRegSuccessValidator.ErrorMessage = "'" + att.LastName + ", " + att.FirstName + "' successfully registered.";
                    NewRegSuccessValidator.IsValid = false;

                    // Reset all inputs
                    ResetNewRegistrationPanel();

                    // Filter the grid to show the newly inserted person 
                    // Rebind the registrations to show the new attendee registration
                    FilterListView("AttendeeFullName", att.LastName + ", " + att.FirstName);

                }


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    NewRegCustomValidator.ErrorMessage = ex.InnerException.Message;
                else
                    NewRegCustomValidator.ErrorMessage = ex.Message;

                NewRegCustomValidator.IsValid = false;
            }
        }

        private void FilterListView(string field, string value)
        {
            if (field == "AttendeeId")
            { 
                GridListView.FilterExpressions.BuildExpression(expression => expression .EqualTo("AttendeeId", Convert.ToInt32(value)));
            }
            else if (field == "AttendeeFullName")
            {
                GridListView.FilterExpressions.BuildExpression(expression => expression.EqualTo("AttendeeFullName", value));
            }
        }

        private void ResetNewRegistrationPanel()
        {
            //NewAttendeeRadComboBox.Items.Clear();
            NewAttendeeRadComboBox.Text = "";
            NewAttendeeRadComboBox.SelectedIndex = -1;
            //NewRegFacilityRadComboBox.Items.Clear();
            NewRegFacilityRadComboBox.Text = "";
            NewRegFacilityRadComboBox.SelectedIndex = -1;
            NewRegTitleTextBox.Text = "";

            NewAttendeeRadComboBox.Focus();
        }

        private string GetLastName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0)
            {
                n = fullName.Substring(0, fullName.IndexOf(",")).Trim();
            }
            return n.Trim();
        }

        private string GetFirstName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0 && fullName.IndexOf(",") < fullName.Length - 1)
            {
                n = fullName.Substring(fullName.IndexOf(",") + 1).Trim();
            }
            return n;
        }

        protected void PrintMenuRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox printCombo = (RadComboBox)sender;
            //if (printCombo.SelectedValue == "PrintBadgeSheet")
            //    PrintBadgeSheetForSelectedAttendees();
            //else if (printCombo.SelectedValue == "PrintRegistrations")
            //    PrintRegistrationsForSelectedAttendees();
            //else if (printCombo.SelectedValue == "PrintContactSheet")
            //    PrintContactSheetForSelectedAttendees();
            //else if (printCombo.SelectedValue == "PringSingleBadge")
            //{
            //    ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "ShowPrintBadgeForm();", true);
            //}
        }

        protected void PrintMenuDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);
            DropDownList printMenuDropDown = (DropDownList)sender;

            if (evt != null && printMenuDropDown.SelectedValue != "")
            {
                string assnName = evt.Association.AssociationName;
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                if (printMenuDropDown.SelectedValue == "PrintBadgeSheet")
                {
                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet ds = new EventManager.Web.Association.Reports2.AttendeeBadgesDataSet();
                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable dt = (EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable)ds.Tables[0];

                    foreach (RadListViewDataItem gi in GridListView.SelectedItems)
                    {
                        int regAttendeeId = Convert.ToInt32(gi.GetDataKeyValue("Id"));

                        EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                        EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                        EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r = dt.NewAttendeeRow();
                        r.AssnName = assnName;
                        r.AttendeeFirstName = ra.Attendee.FirstName;
                        r.AttendeeId = ra.Attendee.Id;
                        r.AttendeeNameFNF = ra.Attendee.FullNameFNF;
                        r.EventEndDate = endDate;
                        r.EventName = eventName;
                        r.EventStartDate = startDate;
                        r.FlagString = ra.FlagListForBadge;
                        if (ra.Registration.Organization != null && ra.Registration.Organization.OrgName != null)
                            r.OrgName = ra.Registration.Organization.OrgName;
                        else
                            r.OrgName = "";
                        r.Title = ra.Title;

                        dt.AddAttendeeRow(r);
                    }

                    Session["AttendeeBadgesDs"] = ds;
                    printMenuDropDown.SelectedIndex = 0;
                    Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");

                }

            }

        }

        protected void FlagMenuDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox flagList = (RadComboBox)sender;

            if (flagList.SelectedIndex > 0)
            {
                EventManager.Business.RegistrationAttendeeFlags m2 = new EventManager.Business.RegistrationAttendeeFlags();

                // Assign flag for each selected row
                foreach (RadListViewDataItem gi in GridListView.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.GetDataKeyValue("Id"));
                    int eventFlagId = Convert.ToInt32(flagList.SelectedValue);

                    m2.Add(regAttendeeId, eventFlagId);
                }

                flagList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                GridListView.Rebind();
            }

        }

        protected void SessionsMenuDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox sessionList = (RadComboBox)sender;

            if (sessionList.SelectedIndex > 0)
            {
                EventManager.Business.SessionAttendanceMethods m2 = new EventManager.Business.SessionAttendanceMethods();

                // Assign flag for each selected row
                foreach (RadListViewDataItem gi in GridListView.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.GetDataKeyValue("Id"));
                    int sessionId = Convert.ToInt32(sessionList.SelectedValue);

                    m2.Add(regAttendeeId, sessionId);
                }

                sessionList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                GridListView.Rebind();
            }



        }

        protected void ActionsRadMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Text == "New")
            {
                // Fill up the attendees and org lists
                int assnId = UserInfo.AssociationId;

                //---------------------------------------------------
                // Load the attendee combo
                //---------------------------------------------------
                //NewAttendeeRadComboBox.Items.Clear();
                // Look for attendees list in session, if not there, get from database
                if (Session["AttendeesList"] == null)
                {
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    List<EventManager.Model.Attendee> attendees = m.GetAttendees(assnId);
                    Session["AttendeesList"] = attendees;
                    //NewAttendeeRadComboBox.DataSource = attendees;
                }
                else
                {
                    //NewAttendeeRadComboBox.DataSource = Session["AttendeesList"];
                }
                //NewAttendeeRadComboBox.DataBind();

                //---------------------------------------------------
                // Load the facilities combo
                // --------------------------------------------------
                //NewRegFacilityRadComboBox.Items.Clear();
                if (Session["OrganizationsList"] == null)
                {
                    EventManager.Business.OrganizationMethods m2 = new EventManager.Business.OrganizationMethods();
                    List<EventManager.Model.Organization> orgs = m2.GetOrganizationsByAssociation(assnId);
                    Session["OrganizationsList"] = orgs;
                    //NewRegFacilityRadComboBox.DataSource = orgs;
                }
                else
                {
                    //NewRegFacilityRadComboBox.DataSource = Session["OrganizationsList"];
                }
                //NewRegFacilityRadComboBox.DataBind();

                // Clear out all fields of "new reg" panel and show it
                ResetNewRegistrationPanel();
                NewRegRadPane.Visible = true;
                NewRegRadPane.Collapsed = false;
                //NewRegPanel.Style.Add(HtmlTextWriterStyle.Display, "block");
            }
            else if (e.Item.Text == "Clear Filters")
            {
                // Clear all filters
                GridListView.FilterExpressions.Clear();

            }
            if (e.Item.Text == "Cancel Registration")
            {
                // Cancel all selected registrations
                RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                foreach (RadListViewDataItem gi in GridListView.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.GetDataKeyValue("Id"));
                    m.CancelRegistration(regAttendeeId);
                }
                m.SaveAllObjectChanges();
                GridListView.Rebind();
            }
        }

        protected void GridListView_NeedDataSource(object sender, RadListViewNeedDataSourceEventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);

            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            GridListView.DataSource = m.GetRegisteredAttendeesForGrid(eventId);
        }

    }
}