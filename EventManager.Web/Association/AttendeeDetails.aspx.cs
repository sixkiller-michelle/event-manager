﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class AttendeeDetailsForm : System.Web.UI.Page
    {
        EventManager.Model.Attendee newAttendee;
        int newAttendeeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["AttendeeId"] == null)
                {
                    FormView1.ChangeMode(FormViewMode.Insert);
                }
                LoadAttendeeAttendanceHistory();
            }
        }

        private void LoadAttendeeAttendanceHistory()
        {
            EventManager.Model.Entities ctx = new EventManager.Model.Entities();

            int attendeeId = Convert.ToInt32(Request.QueryString["AttendeeId"]);
            AttendanceHistoryRadGrid.DataSource = (from reg in ctx.RegistrationAttendees
                                                  join sa in ctx.SessionAttendances on reg.Id equals sa.RegistrationAttendeeId
                                                   where reg.AttendeeId == attendeeId
                                                  select new
                                                  {
                                                      EventName = reg.Registration.Event.EventName,
                                                      EventDate = reg.Registration.Event.StartDateTime,
                                                      CeuHours = ctx.SessionAttendances.Where(i => i.RegistrationAttendeeId == reg.Id).Sum(i => i.EventSession.CEUHours)
                                                  }).Distinct();

            AttendanceHistoryRadGrid.DataBind();
        }

        protected void OrganizationsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        private void RefreshAttendeeSessionList()
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            List<EventManager.Model.Attendee> attendees = m.GetAttendees(UserInfo.AssociationId);
            Session["AttendeesList"] = attendees;
        }

        protected void StateRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            {
                RadComboBox c = (RadComboBox)sender;
                if (c.Items.Count > 1)
                {
                    c.SelectedValue = UserInfo.DefaultStateCode;
                }
            }

        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["AssociationId"] = UserInfo.AssociationId;
            e.Values["IsApproved"] = true;

            // set State
            RadComboBox stateCombo = (RadComboBox)FormView1.FindControl("StateRadComboBox");
            e.Values["StateCode"] = stateCombo.SelectedValue;

            // set org
            RadComboBox orgCombo = (RadComboBox)FormView1.FindControl("OrganizationRadComboBox");
            if (orgCombo.SelectedValue == "")
                e.Values["OrganizationId"] = null;
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    RefreshAttendeeSessionList();
                    Response.Redirect("~/Association/AttendeeDetails.aspx?AttendeeId=" + newAttendeeId.ToString());
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    else
                        CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInInsertMode = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    AttendeeDataSource.DataBind();
                    FormView1.DataBind();
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInEditMode = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
        }

        protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                    am.SaveAllObjectChanges();
                    Response.Redirect("~/Association/AttendeeList.aspx");
                }
                catch (Exception ex)
                {
                    throw new Exception("Attendee cannot be deleted.", ex.InnerException);
                    e.ExceptionHandled = true;
                }
                
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void AttendeeDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            newAttendee = (EventManager.Model.Attendee)e.ReturnValue;
            newAttendeeId = ((EventManager.Model.Attendee)e.ReturnValue).Id;
        }


        private void Page_Error(object sender, EventArgs e)
        {
      
            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc.Message == "Attendee cannot be deleted.")
            {
                if (exc.InnerException.Message.Contains("FK_RegistrationAttendee_Attendee"))
                {
                    CustomValidator1.ErrorMessage = "This attendee has registered for one or more events.";
                }
                else
                {
                    CustomValidator1.ErrorMessage = "An error occurred while trying to delete attendee.";
                }
                CustomValidator1.ErrorMessage = exc.Message;
                CustomValidator1.IsValid = false;
              
                // Log the exception and notify system operators
                ExceptionUtility.LogException(exc, "AttendeeDetails.aspx");
                ExceptionUtility.NotifySystemOps(exc);

                // Clear the error from the server
                Server.ClearError();
            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            // set org
            RadComboBox orgCombo = (RadComboBox)FormView1.FindControl("OrganizationRadComboBox");
            if (orgCombo.SelectedValue == "")
                e.NewValues["OrganizationId"] = null;
        }

    }
}