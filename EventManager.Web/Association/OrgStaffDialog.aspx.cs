﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using EventManager.Model;
using Telerik.Web.UI;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class OrgStaffDialog : System.Web.UI.Page
    {
        Organization newOrg;

        protected void Page_Load(object sender, EventArgs e)
        {
            int orgId = Convert.ToInt32(Request.QueryString["OrgId"]);
            OrganizationMethods m = new OrganizationMethods();
            Organization org = m.GetOrganization(orgId);
            if (org != null)
            {
                OrgNameLabel.Text = org.OrgName;
                StaffCountLabel.Text = org.StaffCount.ToString();
            }
        }

        protected void AttendeesRadGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int attendeeId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();

            Attendee a = am.GetAttendee(attendeeId);
            a.OrganizationId = null;
            am.SaveAllObjectChanges();

            AttendeesRadGrid.DataBind();
        }

        protected void NewAttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
            Organization o = om.GetOrganization(Convert.ToInt32(Request.QueryString["OrgId"]));

            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            if (e.Value != "")
            {
                EventManager.Model.Attendee a = am.GetAttendee(Convert.ToInt32(e.Value));

                try
                {
                    o.Attendees.Add(a);
                    om.SaveAllObjectChanges();

                    // Refresh the attendee grid
                    AttendeesRadGrid.DataBind();

                    // Reload the admin combo
                    //Telerik.Web.UI.RadComboBox c = (Telerik.Web.UI.RadComboBox)FormView1.FindControl("AdminRadComboBox");
                    //FormView1.DataBind();
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                CustomValidator1.ErrorMessage = "Attendee name cannot be found.  Click 'New Attendee' to add a new person to the database.";
                CustomValidator1.IsValid = false;
                
            }
            

        }

        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                //EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                //List<EventManager.Model.Attendee> attendees = am.GetAttendeesByName(assId, NewSpeakerRadComboBox.Text);

                EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                IQueryable<EventManager.Model.Attendee> query =

                    from att in ctx.Attendees
                    where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text) && att.IsApproved == true
                    orderby att.LastName, att.FirstName
                    select att;

                foreach (EventManager.Model.Attendee a in query)
                {
                    RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                    NewAttendeeRadComboBox.Items.Add(item);
                }
            }

        }

        protected void AttendeesRadGrid_DataBound(object sender, EventArgs e)
        {
            StaffCountLabel.Text = AttendeesRadGrid.MasterTableView.Items.Count.ToString();
        }

        protected void SaveCloseRadButton_Click(object sender, EventArgs e)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
            Organization o = om.GetOrganization(Convert.ToInt32(Request.QueryString["OrgId"]));
            if (AdminRadComboBox.SelectedIndex > 0)
            {
                o.AdminAttendeeId = Convert.ToInt32(AdminRadComboBox.SelectedValue);
            }
            else
            {
                o.AdminAttendeeId = null;
            }
            om.SaveAllObjectChanges();
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "StaffUpdated('" + o.Id.ToString() + "', '" + AttendeesRadGrid.Items.Count.ToString() + "');", true);
        }

        protected void AdminRadComboBox_DataBound(object sender, EventArgs e)
        {
            EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
            Organization o = om.GetOrganization(Convert.ToInt32(Request.QueryString["OrgId"]));
            if (AdminRadComboBox.Items.Count > 1)
            {
                if (AdminRadComboBox.FindItemByValue(o.AdminAttendeeId.ToString()) != null)
                    AdminRadComboBox.SelectedValue = o.AdminAttendeeId.ToString();
            }
        }

    }
}