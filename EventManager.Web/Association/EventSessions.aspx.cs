﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Model;
using EventManager.Business;
using System.IO;

namespace EventManager.Web.Association
{
    public partial class EventSessions : System.Web.UI.Page
    {
        int eventId;
        public string SessionsRadGridPanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }
            else
            {
                eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            }

            if (!IsPostBack)
            {
                NewSessionRadPane.Collapsed = true;
                LoadAttendees();
                ResetNewSessionFields();

                LinkButton newAttendeeButton = NewAttendeeLinkButton;
                if (newAttendeeButton != null)
                    newAttendeeButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}');", "0", "-1");
            }
            else
            {
                Control ctl = CommonWebUtils.GetPostBackControl(Page);
                string display = "none";
                //if (ctl != null && ctl.ID != null)
                //{
                //    if (ctl.ID.Contains("CreateSessionRadButton") || ctl.ID.Contains("InsertSpeakerRadButton"))
                //    {
                //        display = "block";
                //    }
                //}
                //if (display == "block")
                //    NewSessionRadPane.Collapsed = false;
                //else
                //    NewSessionRadPane.Collapsed = true;
                if (ctl != null && ctl.ID != null && ctl.ID.Contains("NewRegDoneRadButton"))
                {
                    NewSessionRadPane.Collapsed = false;
                }
                //else
                //{
                //    NewSessionRadPane.Collapsed = true;
                //}
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
        {
            if (e.Updated.ID == "SessionsRadGrid")
            {
                this.SessionsRadGridPanelClientID = e.UpdatePanel.ClientID;
            }
        }

        protected void InsertSpeakerRadButton_Click(object sender, EventArgs e)
        {
            if (NewSpeakerRadComboBox.SelectedItem != null && NewSpeakerRadComboBox.SelectedValue != "")
            {
                // Add the attendee to the ListBox
                RadListBoxItem newSpeakerItem = new RadListBoxItem();
                newSpeakerItem.Value = NewSpeakerRadComboBox.SelectedValue;
                newSpeakerItem.Text = NewSpeakerRadComboBox.SelectedItem.Text;
                SpeakersRadListBox.Items.Add(newSpeakerItem);

                NewSpeakerRadComboBox.SelectedIndex = 0;

                //EventManager.Business.SessionSpeakerMethods m = new EventManager.Business.SessionSpeakerMethods();
                //EventManager.Model.SessionSpeaker s = new EventManager.Model.SessionSpeaker();
                //s.SessionId = Convert.ToInt32(Request.QueryString["SessionId"]);
                //s.SpeakerId = Convert.ToInt32(NewSpeakerRadComboBox.SelectedValue);
                //m.Add(s);
                //m.SaveAllObjectChanges();

                //SpeakersRadGrid.DataBind();
            }
        }

        protected void CreateSessionRadButton_Click(object sender, EventArgs e)
        {
            EventManager.Business.EventSessionMethods esm = new EventManager.Business.EventSessionMethods();
            EventManager.Model.EventSession s = new EventManager.Model.EventSession();
            s.Audience = AudienceTextBox1.Text;
            s.CEUHours = Convert.ToDecimal(CeusTextBox1.Value);
            s.EndDateTime = (DateTime)EndDateRadDateTimePicker1.SelectedDate;
            s.EndDateTimeAlt = AltEndDateRadDateTimePicker1.SelectedDate;
            s.EventId = Convert.ToInt32(Request.QueryString["EventId"]);
            s.Location = LocationTextBox1.Text;
            s.RequireOneScan = OneScanCheckBox1.Checked;
            s.Scanner = ScannerTextBox1.Text;
            s.SessionHost = HostTextBox1.Text;
            s.SessionName = SessionNameTextBox1.Text;
            s.StartDateTime = (DateTime)StartDateRadDateTimePicker1.SelectedDate;

            // Add the speakers
            foreach (RadListBoxItem speakerItem in SpeakersRadListBox.Items)
            {
                SessionSpeaker newSpeaker = new SessionSpeaker();
                newSpeaker.SpeakerId = Convert.ToInt32(speakerItem.Value);
                s.SessionSpeakers.Add(newSpeaker);
            }

            esm.Add(s);
            
            try
            {
                esm.SaveAllObjectChanges();
                ResetNewSessionFields();
                NewSessionRadPane.Collapsed = false;
                SessionsRadGrid.Rebind();
            }
            catch (Exception ex)
            {

            }

            
        }

        private void LoadAttendees()
        {
            // Get the association ID
            int assnId = UserInfo.AssociationId;

            NewSpeakerRadComboBox.Items.Clear();

            // Look for attendees list in session, if not there, get from database
            if (Session["AttendeesList"] == null)
            {
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                List<EventManager.Model.Attendee> attendees = m.GetAttendees(assnId);
                Session["AttendeesList"] = attendees;
                NewSpeakerRadComboBox.DataSource = attendees;

               
            }
            else
            {
                NewSpeakerRadComboBox.DataSource = Session["AttendeesList"];
            }

            // Add a blank row
            NewSpeakerRadComboBox.Items.Insert(0, new RadComboBoxItem("", ""));
            NewSpeakerRadComboBox.DataBind();

        }

        protected void CancelSessionRadButton_Click(object sender, EventArgs e)
        {
            ResetNewSessionFields();
        }

        private void ResetNewSessionFields()
        {
            // Get the default values for start/end times
            DateTime startDate;
            DateTime endDate;
            EventManager.Business.EventMethods esm = new EventManager.Business.EventMethods();
            EventManager.Model.Event em = esm.GetEvent(Convert.ToInt32(Request.QueryString["EventId"]));
            startDate = em.LatestSessionStartDateTime;
            endDate = startDate.AddHours(1);

            EventManager.Model.EventSession s = new EventManager.Model.EventSession();

            SessionNameTextBox1.Text = "";
            CeusTextBox1.Text = "";
            EndDateRadDateTimePicker1.SelectedDate = endDate;
            AltEndDateRadDateTimePicker1.Clear();
            LocationTextBox1.Text = "";
            OneScanCheckBox1.Checked = false;
            ScannerTextBox1.Text = "";
            HostTextBox1.Text = "";
            StartDateRadDateTimePicker1.SelectedDate = startDate;
            AudienceTextBox1.Text = "";
            SpeakersRadListBox.Items.Clear();

            SessionNameTextBox1.Focus();
        }

        protected void DoneRadButton_Click(object sender, EventArgs e)
        {
            //SessionDataSource.DataBind();
            //SessionsRadGrid.DataBind();
            //LeftPane.Collapsed = true;
            //Response.Redirect("~/Association/EventSessions.aspx?EventId=" + Request.QueryString["EventId"].ToString());
        }

        protected void StartDateRadDateTimePicker_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            // 
            //if ((RadDateTimePicker)UpdatePanel2.FindControl("StartDateRadDateTimePicker1")).SelectedDate != null)
            //{ 
            //    EndDateRadDateTimePicker.SelectedDate = ((DateTime)StartDateRadDateTimePicker.SelectedDate).AddHours(1);
            //}
        }

        protected void SessionsRadGrid_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ExportSessionsRadButton_Click(object sender, EventArgs e)
        {

        }

        protected void PrintSessionsRadButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=EventSessions.rpt&ParmNames=@EventId&ParmValues=" + eventId);
        }

        protected void SessionsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem)
            {
                if (e.Item.IsInEditMode)
                {
               
                }
            }
        }

        protected void SessionsRadGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                LinkButton editBtn = (LinkButton)item.FindControl("EditSpeakersLinkButton");
                if (item.DataItem != null)
                    editBtn.Attributes["onclick"] = String.Format("return ShowSpeakersDialog('{0}', '{1}');", ((EventSession)item.DataItem).Id.ToString(), item.ItemIndex);
            } 
        }

        private void PrintSessionList()
        {
            string eventId = Request.QueryString["EventId"];
            Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?FileName=EventSessions&ParmNames=EventId&ParmValues=" + eventId);
        }

        

        protected void ActionsRadMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Text == "Print Session List")
            {
                PrintSessionList();
            }
            else if (e.Item.Text == "New")
            {
                NewSessionRadPane.Collapsed = false;
                SessionNameTextBox1.Focus();
            }
            else if (e.Item.Text == "Print Speaker Badges")
            {
                PrintSpeakerBadges();
            }
        }

        protected void SessionsRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            EventSessionMethods m = new EventSessionMethods();
            SessionsRadGrid.DataSource = m.GetSessionsByEvent(eventId);
        }

        protected void SessionsRadGrid_InsertCommand(object sender, GridCommandEventArgs e)
        {

        }

        protected void SessionsRadGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int sessionId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            EventManager.Business.EventSessionMethods esm = new EventManager.Business.EventSessionMethods();
            EventSession s = esm.GetSession(sessionId);
            item.UpdateValues(s);

            esm.Update(s);
            esm.SaveAllObjectChanges();

            SessionsRadGrid.MasterTableView.ClearEditItems();
        }

        protected void SessionsRadGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int sessionId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            EventSessionMethods am = new EventSessionMethods();
            EventSession s = am.GetSession(sessionId);
            am.Delete(s);
            am.SaveAllObjectChanges();

            SessionsRadGrid.Rebind();
        }

        private void PrintSpeakerBadges()
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string assnName = evt.Association.AssociationName;
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                // Set up the dataset to pass to Crystal Report
                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet ds = new EventManager.Web.Association.Reports2.AttendeeBadgesDataSet();
                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable dt = (EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable)ds.Tables[0];

                // Get the speakers
                SessionSpeakerMethods m2 = new SessionSpeakerMethods();
                List<SessionSpeaker> speakers = m2.GetSpeakersByEvent(eventId);

                foreach (SessionSpeaker sp in speakers)
                {
                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r = dt.NewAttendeeRow();
                    r.AssnName = assnName;
                    r.AttendeeFirstName = sp.Attendee.FirstName;
                    r.AttendeeId = sp.Attendee.Id;
                    r.AttendeeNameFNF = sp.Attendee.FullNameFNF;
                    r.EventEndDate = endDate;
                    r.EventName = eventName;
                    r.EventStartDate = startDate;
                    r.Title = sp.Attendee.Title;
                    if (sp.Attendee.Organization != null && sp.Attendee.Organization.OrgName != null)
                        r.OrgName = sp.Attendee.Organization.OrgName;
                    else
                        r.OrgName = "";

                    dt.AddAttendeeRow(r);
                }

                // Create the report object
                // Get which report to use (event-based or generic badge)
                string typeName = "EventManager.Web.Association.Reporting.SpeakerBadges";
                if (File.Exists(Server.MapPath("~/Association/Reporting/SpeakerBadges" + eventId.ToString() + ".resx")))
                    typeName = "EventManager.Web.Association.Reporting.SpeakerBadges" + eventId.ToString();
                else if (File.Exists(Server.MapPath("~/Association/Reporting/SpeakerBadges_" + evt.AssociationId.ToString() + ".resx")))
                    typeName = "EventManager.Web.Association.Reporting.SpeakerBadges_" + evt.AssociationId.ToString();

                // Create the report using reflection, set data source, and store in session
                Type t = Type.GetType(typeName);
                Telerik.Reporting.Report report = (Telerik.Reporting.Report)Activator.CreateInstance(t);
                report.DataSource = ds;
                Session["Report"] = report;

                // Show report form
                Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");

            }
        }

    }
}