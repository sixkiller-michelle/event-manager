﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Qb.aspx.cs" Inherits="EventManager.Web.Association.Qb.QbForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<h2>QuickBooks Web Connector</h2>

The QuickBooks Web Connector is a software application that runs on Microsoft Windows that enables specially designed web-based applications to exchange data with QuickBooks products.

 If you've been directed to install the QuickBooks Web Connector in order to use a web-based application designed to work with it, click on the link below to download the Web Connector installer. If a dialog box appears with the option to run or save, click run. Follow the prompts to install.
 <asp:HyperLink ID="QbWebConnectorHyperLink" runat="server" NavigateUrl="http://da.developer.intuit.com/uploadedFiles/QBSM/Webconnector/QBWebConnectorInstaller(8).exe" Target="_blank">&nbsp;
 Download and install the QuickBooks Web Connector</asp:HyperLink> &nbsp;Version 2.0.0.139 Size on disk: 12.4 MB (13,058,048 bytes)
 <br />
Click <asp:HyperLink ID="WebConnectorChangesHyperlink" runat="server" NavigateUrl="http://da.developer.intuit.com/uploadedFiles/QBSM/Webconnector/QBWC_Changes_200139.doc" Target="_blank">here</asp:HyperLink>&nbsp;(links to a word document .doc file) to view bugs fixed in a release of QuickBooks Web Connector.

<h2>Event Manager QWC file</h2>
Download the QWC file <asp:HyperLink ID="QwcFileHyperLink" runat="server" NavigateUrl="~/Association/Qb/EventManagerHttpsService.qwc" Target="_blank">here</asp:HyperLink>

</asp:Content>
