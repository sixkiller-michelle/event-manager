﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="EditAssociationInfo.aspx.cs" Inherits="EventManager.Web.Association.EditAssociationForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content">

    <asp:FormView ID="FormView1" runat="server" DataSourceID="AssociationDataSource" OnItemUpdating="FormView1_ItemUpdating">
    <EditItemTemplate>
        <h2><asp:Label ID="AssociationNameHeaderLabel" runat="server" Text='<%# Bind("AssociationName") %>' /></h2>
        <table style="width: 100%;">
            <tr>
                <td>Logo:</td>
                 <td>
                    Allowed extensions are: .gif, .jpg, .jpeg, .png<br /> &nbsp;
                    <telerik:RadUpload ID="RadUpload1" 
                    runat="server" 
                    InitialFileInputsCount="1" ControlObjectsVisibility="None" 
                    AllowedFileExtensions=".gif, .jpg, .jpeg, .png" Height="24px" />
             
                <asp:Button id="buttonSubmit" runat="server" cssclass="RadUploadSubmit" text="Submit" style="MARGIN-TOP: 6px" />                
                <asp:CustomValidator ID="Customvalidator1" runat="server" Display="Dynamic" OnServerValidate="Customvalidator1_ServerValidate">
                    <span style="FONT-SIZE: 11px;">Invalid extensions.</span>
                </asp:CustomValidator>
             
                <telerik:RadProgressArea id="progressArea1" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>Association ID:&nbsp;</td>
                <td><asp:Label ID="AssociationIdTextBox" runat="server" Text='<%# Bind("AssociationId") %>' />&nbsp;</td>
               
            </tr>
            <tr>
                <td>Association Name:&nbsp;</td>
                <td><asp:TextBox ID="AssociationNameTextBox" runat="server" Text='<%# Bind("AssociationName") %>' />&nbsp;</td>
                
            </tr>
            <tr>
                <td colspan="2"><h3>Address</h3></td>
            </tr>
            <tr>
                <td>Street:&nbsp;</td>
                <td><asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' Rows="3" Height="50px" style="height: 22px" />&nbsp;</td>
                
            </tr>
            <tr>
                <td>City:</td>
                <td><asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' /></td>
            </tr>
            <tr>
                <td>State:</td>
                <td><asp:DropDownList ID="StatesDropDownList" runat="server" DataSourceID="StatesDataSource" 
                        DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("StateCode") %>'>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Zip:</td>
                <td><asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' /></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td><asp:TextBox ID="FaxNumberTextBox" runat="server" Text='<%# Bind("FaxNumber") %>' /></td>
            </tr>

        </table>
    
    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />

<%--        <script type="text/javascript">
            function validateRadUpload1(source, arguments) {
                arguments.IsValid = $find('<%= RadUpload1.ClientID %>').validateExtensions();
            }
        </script>--%>

    </EditItemTemplate>

    <InsertItemTemplate>
        AssociationId:
        <asp:TextBox ID="AssociationIdTextBox" runat="server" Text='<%# Bind("AssociationId") %>' />
        <br />
        AssociationName:
        <asp:TextBox ID="AssociationNameTextBox" runat="server" Text='<%# Bind("AssociationName") %>' />
        <br />
        Address:
        <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' />
        <br />
        City:
        <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
        <br />
        StateCode:
        <asp:TextBox ID="StateCodeTextBox" runat="server" Text='<%# Bind("StateCode") %>' />
        <br />
        Zip:
        <asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' />
        <br />
        PhoneNumber:
        <asp:TextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' />
        <br />
        FaxNumber:
        <asp:TextBox ID="FaxNumberTextBox" runat="server" Text='<%# Bind("FaxNumber") %>' />
        <br />
        HeaderImage:
        <asp:TextBox ID="HeaderImageTextBox" runat="server" Text='<%# Bind("HeaderImage") %>' />
        <br />
        Events:
        <asp:TextBox ID="EventsTextBox" runat="server" Text='<%# Bind("Events") %>' />
        <br />
        AssociationEmployees:
        <asp:TextBox ID="AssociationEmployeesTextBox" runat="server" Text='<%# Bind("AssociationEmployees") %>' />
        <br />
        EntityState:
        <asp:TextBox ID="EntityStateTextBox" runat="server" Text='<%# Bind("EntityState") %>' />
        <br />
        EntityKey:
        <asp:TextBox ID="EntityKeyTextBox" runat="server" Text='<%# Bind("EntityKey") %>' />
        <br />
        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
        &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
    </InsertItemTemplate>
    <ItemTemplate>
    <h2><asp:Label ID="AssociationNameHeaderLabel" runat="server" Text='<%# Bind("AssociationName") %>' /></h2>
        <table style="width: 100%;">
            <tr>
                <td>Logo:</td>
                 <td><asp:TextBox ID="HeaderImageTextBox" runat="server" Text='<%# Bind("HeaderImage") %>' /> 
                 <asp:Image ID="imgPicture" runat="server" 
                    ImageUrl='<%# Eval("HeaderImage","Image.aspx?PersonID={0}") %>' Width="128px" Height="128px" /></td>
            </tr>
            <tr>
                <td>Association ID:&nbsp;</td>
                <td><asp:Label ID="AssociationIdTextBox" runat="server" Text='<%# Bind("AssociationId") %>' />&nbsp;</td>
               
            </tr>
            <tr>
                <td>Association Name:&nbsp;</td>
                <td><asp:Label ID="AssociationNameTextBox" runat="server" Text='<%# Bind("AssociationName") %>' />&nbsp;</td>
                
            </tr>
            <tr>
                <td colspan="2"><h3>Address</h3></td>
            </tr>
            <tr>
                <td>Street:&nbsp;</td>
                <td><asp:Label ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' Rows="3" Height="50px" style="height: 22px" />&nbsp;</td>
                
            </tr>
            <tr>
                <td>City:</td>
                <td><asp:Label ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' /></td>
            </tr>
            <tr>
                <td>State:</td>
                <td><asp:Label ID="StateCodeTextBox" runat="server" Text='<%# Bind("StateCode") %>' /></td>
            </tr>
            <tr>
                <td>Zip:</td>
                <td><asp:Label ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' /></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><asp:Label ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td><asp:Label ID="FaxNumberTextBox" runat="server" Text='<%# Bind("FaxNumber") %>' /></td>
            </tr>

        </table>
    <br />
    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
    </ItemTemplate>
    </asp:FormView>
</div>

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" SelectMethod="GetStates" 
        TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="AssociationDataSource" runat="server" DataObjectTypeName="EventManager.Model.Association" SelectMethod="GetAssociation" TypeName="EventManager.Business.AssociationMethods" UpdateMethod="UpdateAssociation">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
