﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using EventManager.Model;
using Telerik.Web.UI;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class EventRegDetailsForm : System.Web.UI.Page
    {
        EventManager.Model.Attendee newAttendee;
        int _regId = 0;
        int _eventId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["RegId"] == null || Request.QueryString["RegId"] == "0")
            {
                if (Request.QueryString["EventId"] == null || Request.QueryString["EventId"] == "0")
                {
                    Response.Redirect("~/Association/EventList.aspx");
                }
                else
                {
                    RegistrationFormView.ChangeMode(FormViewMode.Insert);
                    _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                }
            }
            else
            {
                _regId = Convert.ToInt32(Request.QueryString["RegId"]);
                EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                EventManager.Model.Registration r = rm.GetRegistration(_regId);
                _eventId = r.EventId;
            }

            SetTradeShowVisibility();
        }

        private void SetTradeShowVisibility()
        {
            int eventId;
            if (Request.QueryString["EventId"] != null && Request.QueryString["EventId"] != "0")
            {
                eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
                EventManager.Model.Event evt = em.GetEvent(eventId);
                Panel p = (Panel)RegistrationFormView.FindControl("TradeShowPanel");
                if (p != null)
                {
                    if (evt.EventType.TypeName == "Trade Show")
                    {
                        p.Visible = true;
                        p.Style["display"] = "block";
                    }
                    else
                    {
                        p.Visible = false;
                        p.Style["display"] = "none";
                    }
                }

            }
        }

        protected void OrgRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                RadComboBox combo = (RadComboBox)sender;
                int assnId = UserInfo.AssociationId;

                EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                IQueryable<EventManager.Model.Organization> query =

                    from att in ctx.Organizations
                    where att.AssociationId == assnId && att.OrgName.StartsWith(e.Text)
                    orderby att.OrgName
                    select att;

                if (query.Count() > 0)
                {
                    foreach (EventManager.Model.Organization o in query)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(o.OrgName, o.Id.ToString());
                        combo.Items.Add(item);
                    }
                }
                else
                {
                    combo.ClearSelection();
                }

            }

        }

        protected void OrgRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                RadComboBox combo = (RadComboBox)sender;
                if (!String.IsNullOrEmpty(combo.SelectedValue))
                {
                    // Fill in the base price 
                    if (RegistrationFormView.CurrentMode == FormViewMode.Insert)
                    {

                        EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                        Decimal basePrice = om.GetBasePrice(_eventId, Convert.ToInt32(combo.SelectedValue));

                        RadTextBox baseTextBox = (RadTextBox)RegistrationFormView.FindControl("BasePriceTextBox");
                        baseTextBox.Text = basePrice.ToString();

                    }
                }
                else
                {
                    //TextBox firstNameTextBox = (TextBox)NewAttendeeFormView.FindControl("FirstNameLabel");
                    //TextBox lastNameTextBox = (TextBox)NewAttendeeFormView.FindControl("LastNameLabel");
                    //TextBox titleTextBox = (TextBox)NewAttendeeFormView.FindControl("TitleLabel");
                    //firstNameTextBox.Text = NewAttendeeRadComboBox.Text.Substring(NewAttendeeRadComboBox.Text.IndexOf(",") + 1).Trim();
                    //lastNameTextBox.Text = NewAttendeeRadComboBox.Text.Substring(0, NewAttendeeRadComboBox.Text.IndexOf(","));
                    //titleTextBox.Focus();
                }

            }
            catch (Exception ex)
            {
                CustomValidator1.ErrorMessage = ex.Message;
                CustomValidator1.IsValid = false;
            }
        }


        protected void OrganizationsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void OrganizationRadComboBox_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            // Fill in the base price 
            if (RegistrationFormView.CurrentMode == FormViewMode.Insert)
            {
                Telerik.Web.UI.RadComboBox c = (Telerik.Web.UI.RadComboBox)RegistrationFormView.FindControl("OrganizationRadComboBox");
                if (c != null)
                {
                    if (c.SelectedIndex > 0)
                    {
                        EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                        Decimal basePrice = om.GetBasePrice(_eventId, Convert.ToInt32(c.SelectedValue));

                        Telerik.Web.UI.RadTextBox baseTextBox = (Telerik.Web.UI.RadTextBox)RegistrationFormView.FindControl("BasePriceTextBox");
                        baseTextBox.Text = basePrice.ToString();
                    }
                }
            }
        }

        protected void RegistrationFormView_ModeChanged(object sender, EventArgs e)
        {
            if (RegistrationFormView.CurrentMode == FormViewMode.Insert)
            {
                RegistrantsPanel.Visible = false;
            }
            else
            {
                RegistrantsPanel.Visible = true;
            }

            SetTradeShowVisibility();
        }

        protected void RegistrationFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            // Get the current user ID
            Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());

            // Get the selected org ID
            int? orgId = -1;
            RadComboBox c = (RadComboBox)RegistrationFormView.FindControl("OrgRadComboBox");
            if (c != null)
            {
                if (!String.IsNullOrEmpty(c.SelectedValue))
                    orgId = Convert.ToInt32(c.SelectedValue);
                else
                {
                    if (!String.IsNullOrEmpty(c.Text))
                    {
                        // Get association ID
                        int assnId = UserInfo.AssociationId;
                        EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                        orgId = om.GetOrganizationByName(assnId, c.Text).Id;
                    }

                }
            }

            // Get the base price for the registration (based on timeframe and org fee category)
            e.Values["EventId"] = Request.QueryString["EventId"];
            e.Values["OrganizationId"] = orgId;
            e.Values["EnteredByUserId"] = g;
            e.Values["EntryDateTime"] = DateTime.Now;
            e.Values["IsWebRegistration"] = false;
            //e.Values["BasePrice"] = 0.00;
            e.Values["IsComplete"] = true;
            e.Values["IsPaid"] = false;
            e.Values["IsComplete"] = true;
            e.Values["PaymentAmount"] = Convert.ToDecimal(0);
        }

        protected void RegistrationFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                rm.SaveAllObjectChanges();
                RegistrationFormView.DataBind();
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.KeepInInsertMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void RegistrationFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                rm.SaveAllObjectChanges();
                RegistrationFormView.DataBind();
            }
            else
            {
                e.KeepInEditMode = true;
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void RegistrationFormView_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                rm.SaveAllObjectChanges();
                RegistrationFormView.DataBind();
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        private int OrgId()
        {
            int orgId = 0;
            if (RegistrationFormView.CurrentMode == FormViewMode.ReadOnly)
            {
                Label orgIdLabel = (Label)RegistrationFormView.FindControl("OrgIdLabel");
                if (orgIdLabel != null)
                    orgId = Convert.ToInt32(orgIdLabel.Text);
            }
            else
            {
                RadComboBox orgCombo = (RadComboBox)RegistrationFormView.FindControl("OrgRadComboBox");
                if (orgCombo != null)
                    if (orgCombo.SelectedIndex > 0)
                    {
                        orgId = Convert.ToInt32(orgCombo.SelectedValue);
                    }
            }
            return orgId;
        }


        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                EventManager.Model.Entities ctx = new EventManager.Model.Entities();
                IQueryable<EventManager.Model.Attendee> query =

                    from att in ctx.Attendees
                    where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text)
                    orderby att.LastName, att.FirstName
                    select att;

                if (query.Count() > 0)
                {
                    foreach (EventManager.Model.Attendee a in query)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                        NewAttendeeRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewAttendeeRadComboBox.ClearSelection();
                }

            }

        }

        protected void NewAttendeeFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            int orgId = OrgId();
            if (orgId > 0)
            {
                e.Values["OrganizationId"] = orgId;
                e.Values["IsApproved"] = true;
            }
            else
            {
                e.Cancel = true;
            }

        }

        protected void NewAttendeeFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                // Get the current registration id
                int regId = (int)RegistrationFormView.DataKey.Value;

                if (regId != 0)
                {
                    EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                    EventManager.Model.Registration r = rm.GetRegistration(regId);

                    // Create the new attendee registration
                    EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                    EventManager.Model.RegistrationAttendee ra = new EventManager.Model.RegistrationAttendee();
                    newAttendee.IsApproved = true;
                    ra.Attendee = newAttendee;
                    ra.Price = 0;
                    ra.Title = newAttendee.Title;
                    ra.Notes = "";

                    r.RegistrationAttendees.Add(ra);

                    rm.SaveAllObjectChanges();
                    RadGrid1.DataBind();
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void NewAttendeeFormView_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                //// Get the newly added attendee and add them to the registration
                //int regId = (int)RegistrationFormView.DataKey.Value;

                //if (regId != 0)
                //{ 
                //    EventManager.Business.RegistrationMethods rm = new EventManager.Business.RegistrationMethods();
                //    EventManager.Model.Registration r = rm.GetRegistration(regId);

                //    // Insert the new attendee
                //    EventManager.Model.Attendee a = new EventManager.Model.Attendee();
                //    a.AssociationId = ((AssociationMaster2)Master.Master).AssociationId;
                //    a.Email = ((TextBox)NewAttendeeFormView.FindControl("EmailLabel")).Text;
                //    a.PhoneNumber = ((TextBox)NewAttendeeFormView.FindControl("PhoneLabel")).Text;
                //    a.FirstName = ((TextBox)NewAttendeeFormView.FindControl("FirstNameLabel")).Text;
                //    a.LastName = ((TextBox)NewAttendeeFormView.FindControl("LastNameLabel")).Text;
                //    a.Title = ((TextBox)NewAttendeeFormView.FindControl("TitleLabel")).Text;

                //    // Create the new attendee registration
                //    EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                //    EventManager.Model.RegistrationAttendee ra = new EventManager.Model.RegistrationAttendee();
                //    ra.Attendee = a;
                //    ra.Price = 0;
                //    ra.Title = a.Title;
                //    ra.Notes = "";

                //    r.RegistrationAttendees.Add(ra);

                //    rm.SaveAllObjectChanges();
                //    RadGrid1.DataBind();
                //}

            }

        }

        protected void RadGrid1_ItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.RegistrationAttendeesMethods rm = new EventManager.Business.RegistrationAttendeesMethods();
                rm.SaveAllObjectChanges();
                RadGrid1.DataBind();
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void RadGrid1_ItemDeleted(object sender, GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.RegistrationAttendeesMethods rm = new EventManager.Business.RegistrationAttendeesMethods();
                rm.SaveAllObjectChanges();
                RadGrid1.DataBind();
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void AttendeeDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            newAttendee = (EventManager.Model.Attendee)e.ReturnValue;
        }

        protected void RegistrationDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            EventManager.Model.Registration r = (EventManager.Model.Registration)e.ReturnValue;
            if (r != null)
            {
                Response.Redirect("~/Association/EventRegDetails.aspx?RegId=" + r.Id.ToString());
            }
        }

        private void AddAttendeeToRegistration()
        {
            int orgId = OrgId();

            if (orgId > 0)
            {
                try
                {
                    if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                    {
                        // Get the selected attendee from drop down
                        EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                        EventManager.Model.Attendee a = am.GetAttendee(Convert.ToInt32(NewAttendeeRadComboBox.SelectedValue));

                        // Get the reg id
                        int regId = Convert.ToInt32(Request.QueryString["RegId"]);

                        EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                        EventManager.Model.RegistrationAttendee ra = new EventManager.Model.RegistrationAttendee();
                        ra.RegistrationId = regId;
                        ra.AttendeeId = a.Id;
                        ra.Notes = "";
                        ra.Price = 0;
                        ra.Title = a.Title;

                        ram.Add(ra);
                        ram.SaveAllObjectChanges();

                        // Refresh the registrations grid
                        RadGrid1.DataBind();
                    }
                    else
                    {
                        TextBox firstNameTextBox = (TextBox)NewAttendeeFormView.FindControl("FirstNameLabel");
                        TextBox lastNameTextBox = (TextBox)NewAttendeeFormView.FindControl("LastNameLabel");
                        TextBox titleTextBox = (TextBox)NewAttendeeFormView.FindControl("TitleLabel");
                        firstNameTextBox.Text = NewAttendeeRadComboBox.Text.Substring(NewAttendeeRadComboBox.Text.IndexOf(",") + 1).Trim();
                        lastNameTextBox.Text = NewAttendeeRadComboBox.Text.Substring(0, NewAttendeeRadComboBox.Text.IndexOf(","));
                        titleTextBox.Focus();
                    }

                }
                catch (Exception ex)
                {
                    AttendeesCustomValidator.ErrorMessage = ex.Message;
                    AttendeesCustomValidator.IsValid = false;
                }

            }
        }

        protected void AddAttendeeRadButton_Click(object sender, EventArgs e)
        {
            AddAttendeeToRegistration();
        }

        protected void RegistrationFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            e.NewValues["EventId"] = _eventId;
            if (e.NewValues["OrganizationId"] == "")
            {
                e.NewValues["OrganizationId"] = null;
            }

            Label orderNumberLabel = (Label)RegistrationFormView.FindControl("OrderNumberLabel");
            if (orderNumberLabel != null)
            {
                if (orderNumberLabel.Text == "")
                    e.NewValues["OrderNumber"] = null;
            }
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                int regAttendeeId = Convert.ToInt32(dataBoundItem.GetDataKeyValue("Id"));
              
                RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                RegistrationAttendee att = m.GetRegistrationAttendee(regAttendeeId);
                if (att != null)
                {
                    if (att.IsCancelled == true)
                    {
                        dataBoundItem["AttendeeNameHyperlink"].BackColor = System.Drawing.Color.Red;
                        //e.Item.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        dataBoundItem["AttendeeNameHyperlink"].BackColor = System.Drawing.Color.White;
                    }
                }
                
                //Check the formatting condition
                //if (balance != 0)
                //{
                //    dataBoundItem["BalanceGridBoundColumn"].BackColor = System.Drawing.Color.Red;
                //    dataBoundItem["BalanceGridBoundColumn"].ForeColor = System.Drawing.Color.White;
                //}
            }
        }



    }
}