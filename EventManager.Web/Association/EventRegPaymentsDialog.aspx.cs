﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using Telerik.Web.UI;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventRegPaymentsDialog : System.Web.UI.Page
    {
        public string RadGrid1PanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
        {
            if (e.Updated.ID == "RadGrid1")
            {
                this.RadGrid1PanelClientID = e.UpdatePanel.ClientID;
            }
        }

        protected void CloseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int regId = Convert.ToInt32(Request.QueryString["RegId"]);
                decimal paymentTotal = 0;
                decimal balance = 0;
                string paymentMethod = "";
                string paymentNumber = "";

                // Get the registration record
                RegistrationMethods m1 = new RegistrationMethods();
                Registration reg = m1.GetRegistration(regId);
                paymentTotal = reg.PaymentTotal;
                balance = m1.GetBalance(regId);
                paymentMethod = reg.PaymentType;
                paymentNumber = reg.PaymentNumber;
                
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('" + paymentTotal + "', '" + paymentMethod + "', '" + paymentNumber + "', '" + balance + "');", true);
            }
            catch (Exception ex)
            {
                //CustomValidator1.ErrorMessage = ex.Message;
                //CustomValidator1.IsValid = false;
            }
        }

        protected void MasterTableView_DataBinding(object sender, EventArgs e)
        {
            GridNumericColumn paymentAmtCol = RadGrid1.MasterTableView.GetColumnSafe("PaymentAmt") as GridNumericColumn;
            paymentAmtCol.FooterAggregateFormatString = paymentAmtCol.Aggregate.ToString() + ": {0:C}";
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);

            RegistrationPaymentMethods m = new RegistrationPaymentMethods();
            RadGrid1.DataSource = m.GetPaymentsByRegistration(regId);
        }

        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;

            try
            {
                int regId = Convert.ToInt32(Request.QueryString["RegId"]);
                RegistrationPaymentMethods m = new RegistrationPaymentMethods();
                RegistrationPayment pay = new RegistrationPayment();
                pay.RegistrationId = regId;
                item.UpdateValues(pay);

                m.Add(pay);
                m.SaveAllObjectChanges();

                // Close the window and update the totals (payment, balance)
                decimal paymentTotal = 0;
                decimal balance = 0;
                string paymentMethod = "";
                string paymentNumber = "";

                RegistrationMethods m1 = new RegistrationMethods();
                Registration reg = m1.GetRegistration(regId);
                paymentTotal = reg.PaymentTotal;
                balance = m1.GetBalance(regId);
                paymentMethod = reg.PaymentType;
                paymentNumber = reg.PaymentNumber;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CloseDialog", "CloseAndRebind('" + paymentTotal + "', '" + paymentMethod + "', '" + paymentNumber + "', '" + balance + "', '" + reg.Status + "');", true);

            }
            catch (ArgumentException ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.InnerException.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int regPaymentId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            RegistrationPaymentMethods am = new RegistrationPaymentMethods();
            RegistrationPayment pay = am.GetRegistrationPayment(regPaymentId);

            item.UpdateValues(pay);
            am.Update(pay);
            am.SaveAllObjectChanges();

            decimal paymentTotal = 0;
            decimal balance = 0;
            string paymentMethod = "";
            string paymentNumber = "";

            RegistrationMethods m1 = new RegistrationMethods();
            Registration reg = m1.GetRegistration(pay.RegistrationId);
            paymentTotal = reg.PaymentTotal;
            balance = m1.GetBalance(pay.RegistrationId);
            paymentMethod = reg.PaymentType;
            paymentNumber = reg.PaymentNumber;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CloseDialog", "CloseAndRebind('" + paymentTotal + "', '" + paymentMethod + "', '" + paymentNumber + "', '" + balance + "', '" + reg.Status + "');", true);

            //RadGrid1.Rebind();

            // Hide all edit items
            //RadGrid1.MasterTableView.ClearEditItems();
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int regPaymentId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            RegistrationPaymentMethods am = new RegistrationPaymentMethods();
            RegistrationPayment ch = am.GetRegistrationPayment(regPaymentId);
            am.Delete(ch);
            am.SaveAllObjectChanges();

            RadGrid1.Rebind();
        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                RadGrid1.MasterTableView.ClearEditItems();

                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert  
                newValues["RegistrationId"] = Convert.ToInt32(Request.QueryString["RegId"]);
                newValues["PaymentAmt"] = 0;
                newValues["PaymentType"] = "Check";
                newValues["PaymentNumber"] = "";
                newValues["PaymentDate"] = DateTime.Now;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);
            }
            else if (e.CommandName == RadGrid.EditCommandName)
            {
                e.Item.OwnerTableView.IsItemInserted = false;
            }
        }
    }
}