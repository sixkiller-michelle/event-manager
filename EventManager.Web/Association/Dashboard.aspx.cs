﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillEvents();
            }
        }

        private void FillEvents()
        {
            EventMethods m = new EventMethods();
            EventManager.Model.Event convention = m.GetCurrentConventionEvent(UserInfo.AssociationId);
            if (convention != null)
            {
                ConventionRadPane.Collapsed = false;
                ConventionNameLabel.Text = convention.EventName;
                ConventionDateLabel.Text = convention.StartDateTime.ToLongDateString();
                OverviewRadButton1.PostBackUrl = OverviewRadButton1.PostBackUrl + "?EventId=" + convention.Id.ToString();
                AttendeesRadButton1.PostBackUrl = AttendeesRadButton1.PostBackUrl + "?EventId=" + convention.Id.ToString();
                FacilitiesRadButton1.PostBackUrl = FacilitiesRadButton1.PostBackUrl + "?EventId=" + convention.Id.ToString();
                if (convention.LagDays <= 0)
                    ConventionLagTimeLabel.Text = convention.LagTimeString + " ago";
                else
                    ConventionLagTimeLabel.Text = convention.LagTimeString + " from now";
                
            }
            else
            {
                ConventionRadPane.Collapsed = true;
            }

            EventManager.Model.Event nextEvent = m.GetNextEvent(UserInfo.AssociationId);
            if (nextEvent != null && (convention == null || (nextEvent.Id != convention.Id)))
            {
                NextEventRadPane.Collapsed = false;
                NextEventNameLabel.Text = nextEvent.EventName;
                NextEventDateLabel.Text = nextEvent.StartDateTime.ToLongDateString();
                OverviewRadButton2.PostBackUrl = OverviewRadButton2.PostBackUrl + "?EventId=" + nextEvent.Id.ToString();
                AttendeesRadButton2.PostBackUrl = AttendeesRadButton2.PostBackUrl + "?EventId=" + nextEvent.Id.ToString();
                FacilitiesRadButton2.PostBackUrl = FacilitiesRadButton2.PostBackUrl + "?EventId=" + nextEvent.Id.ToString();
                if (nextEvent.LagDays <= 0)
                    NextEventLagTimeLabel.Text = nextEvent.LagTimeString + " ago";
                else
                    NextEventLagTimeLabel.Text = nextEvent.LagTimeString + " from now";
            }
            else
            {
                NextEventRadPane.Collapsed = true;
            }

            EventManager.Model.Event lastEvent = m.GetMostRecentEvent(UserInfo.AssociationId);
            if (lastEvent != null && (convention == null || (lastEvent.Id != convention.Id)))
            {
                LastEventRadPane.Collapsed = false;
                LastEventNameLabel.Text = lastEvent.EventName;
                LastEventDateLabel.Text = lastEvent.StartDateTime.ToLongDateString();
                OverviewRadButton3.PostBackUrl = OverviewRadButton3.PostBackUrl + "?EventId=" + lastEvent.Id.ToString();
                AttendeesRadButton3.PostBackUrl = AttendeesRadButton3.PostBackUrl + "?EventId=" + lastEvent.Id.ToString();
                FacilitiesRadButton3.PostBackUrl = FacilitiesRadButton3.PostBackUrl + "?EventId=" + lastEvent.Id.ToString();
                if (lastEvent.LagDays <= 0)
                    LastEventLagTimeLabel.Text = lastEvent.LagTimeString + " ago";
                else
                    LastEventLagTimeLabel.Text = lastEvent.LagTimeString + " from now";
            }
            else
            {
                LastEventRadPane.Collapsed = true;
            }


        }

        protected void GetUpcomingEvents()
        {
            int associationId = ((AssociationMaster2)Master).AssociationId;
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            UpcomingEventsRadGrid.DataSource = em.GetUpcomingEvents(associationId, UserInfo.UserId);
            UpcomingEventsRadGrid.DataBind();
        }

        protected void UpcomingEventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
            e.InputParameters["userId"] = UserInfo.UserId;
        }
    }
}