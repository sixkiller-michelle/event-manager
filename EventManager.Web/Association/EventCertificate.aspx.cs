﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class EventCertificate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                HeaderTextRadEditor.ImageManager.UploadPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"];
                HeaderTextRadEditor.ImageManager.ViewPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"];
                //FooterTextRadEditor.ImageManager.UploadPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"];
                //FooterTextRadEditor.ImageManager.ViewPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"];
                GetCertificateInfo();
            }
        }

        private void GetCertificateInfo()
        {
            EventManager.Business.EventCertificateMethods m = new EventManager.Business.EventCertificateMethods();
            EventManager.Model.EventCertificate c = m.GetCertificateInfoByEvent(Convert.ToInt32(Request.QueryString["EventId"]));
            if (c != null)
            {
                HeaderTextRadEditor.Content = c.CertificateHeaderText;
                FooterTextRadEditor.Content = c.CertificateFooterText;
            }
        }

        protected void SaveRadButton_Click(object sender, EventArgs e)
        {
            EventManager.Business.EventCertificateMethods m = new EventManager.Business.EventCertificateMethods();
            EventManager.Model.EventCertificate c = m.GetCertificateInfoByEvent(Convert.ToInt32(Request.QueryString["EventId"]));
            if (c == null)
            {
                c = new EventManager.Model.EventCertificate();
            }

            c.EventId = Convert.ToInt32(Request.QueryString["EventId"]);
            c.CertificateHeaderText = HeaderTextRadEditor.Content;
            c.CertificateFooterText = FooterTextRadEditor.Content;
            try
            { m.SaveAllObjectChanges();}
            catch (Exception ex)
            { }
            
        }
    }
}