﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.PointOfService;
using System.Text;

namespace EventManager.Web.Association
{
    public partial class ScanAttendees2 : System.Web.UI.Page
    {
        int eventId = 0;
        private PosExplorer explorer;
        private Scanner scanner;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            int assnId = UserInfo.AssociationId;
            try
            {
                AssociationLogoImage.ImageUrl = "~/Association/Documents/Associations/" + assnId.ToString() + "/logo.png";
            }
            finally { }

            //explorer = new PosExplorer(this);
            explorer = new PosExplorer();
            explorer.DeviceAddedEvent += new DeviceChangedEventHandler(explorer_DeviceAddedEvent);


            if (Request.QueryString["EventId"] != null)
            {
                eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                if (!IsPostBack)
                {
                    AttendeeNameContainer.Visible = false;
                    var sessionMethods = new EventManager.Business.EventSessionMethods();
                    SessionsRadComboBox.DataSource = sessionMethods.GetSessionsByEvent(eventId);
                    SessionsRadComboBox.DataBind();
                }
            }
        }

        void explorer_DeviceAddedEvent(object sender, DeviceChangedEventArgs e)
        {
            if (e.Device.Type == "Scanner")
            {
                scanner = (Scanner)explorer.CreateInstance(e.Device);
                scanner.Open();
                scanner.Claim(1000);
                scanner.DeviceEnabled = true;
                scanner.DataEvent += new DataEventHandler(scanner_DataEvent);
                scanner.DataEventEnabled = true;
                scanner.DecodeData = true;
            }


        }

        void scanner_DataEvent(object sender, DataEventArgs e)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            scanner.DataEventEnabled = true;
            //listBox1.Items.Add(encoding.GetString(scanner.ScanDataLabel));
            AttendeeIdRadNumericTextBox.Text = encoding.GetString(scanner.ScanDataLabel);
        }

        private void LogScan(string attendeeId, int sessionId)
        {
            int attId = 0;

            if (Int32.TryParse(attendeeId, out attId))
            {

                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = m.GetAttendee(Convert.ToInt32(attId));
                if (a != null)
                {
                    AttendeeNameLabel.Text = a.FullNameFNF;

                    // Log a scan for this session
                    EventManager.Business.ScannerDataMethods m2 = new EventManager.Business.ScannerDataMethods();
                    EventManager.Model.ScannerData scan = new EventManager.Model.ScannerData();
                    scan.EventId = eventId;
                    scan.SessionId = sessionId;
                    scan.AttendeeId = a.Id;
                    scan.ScanDate = DateTime.Now.Date.ToShortDateString();
                    scan.ScanTime = DateTime.Now.ToString("H:mm:ss");

                    m2.Add(scan);
                    m2.SaveAllObjectChanges();

                    AttendeeIdRadNumericTextBox.Text = "";
                    AttendeeIdRadNumericTextBox.Focus();
                }
                else
                {
                    AttendeeIdRadNumericTextBox.Text = "";
                    AttendeeNameLabel.Text = "";
                    AttendeeIdRadNumericTextBox.Focus();
                }
            }
            else
            {
                // Send email to michelle
                Email mail = new Email();
                mail.SendMail("michelle@sixkillersoftware.com", "admin@ceuavenue.com", "Invalid scan value: " + attendeeId, "");
                AttendeeIdRadNumericTextBox.Text = "";
                AttendeeNameLabel.Text = "";
                AttendeeIdRadNumericTextBox.Focus();
            }
        }
            
        protected void SessionsRadComboBox_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
          
            var sessionMethods = new EventManager.Business.EventSessionMethods();
            SessionFormView.DataSource = sessionMethods.GetSessionsByEvent(eventId).Where(x => x.Id == Convert.ToInt32(SessionsRadComboBox.SelectedValue));
            SessionFormView.DataBind();
            ResetEntry();
        }

        private void ResetEntry()
        {
            AttendeeIdRadNumericTextBox.Text = "";
            AttendeeNameLabel.Text = "";
            //AttendeeIdTextBox.SelectionOnFocus = Telerik.Web.UI.SelectionOnFocus.SelectAll;

        }
    }
}