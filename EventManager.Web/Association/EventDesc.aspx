﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventDesc.aspx.cs" Inherits="EventManager.Web.Association.EventDescForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div id="content">
    
    <h2>Event Description</h2>

    <p>Enter the event description here.  This description will be shown to all users on the event details page.</p>
    
    <telerik:RadButton ID="SaveRadButton" runat="server" Text="Save" onclick="SaveRadButton_Click">
    </telerik:RadButton>
    
    <telerik:radeditor ID="RadEditor1" runat="server" SkinID="DefaultSetOfTools" ToolbarMode="Default" Skin="Default" BackColor="White" ContentAreaCssFile="" ContentAreaMode="Div">
    <Content>
        This is a test.
    </Content>
    <ImageManager ViewPaths="~/Association/Images"
                            UploadPaths="~/Association/Images"
                            DeletePaths="~/Association/Images">
                        </ImageManager>

    </telerik:radeditor>
     </div>

</asp:Content>
