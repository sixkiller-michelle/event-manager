﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventDocuments.aspx.cs" Inherits="EventManager.Web.Association.EventDocumentsForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content">
        <h2>
            Event Documents</h2>
        <p>&nbsp;</p>
        
        <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />

        <telerik:RadUpload ID="RadUpload1" Runat="server"  InitialFileInputsCount="3" 
            oninit="RadUpload1_Init"  >
        </telerik:RadUpload>
        <br /><br />

        <telerik:RadButton runat="server" ID="SubmitButton" Text="Upload files" onclick="SubmitButton_Click" Width="150" Height="30" />
        
        <telerik:RadProgressArea runat="server" ID="ProgressArea1"></telerik:RadProgressArea>

        <br /><br />
         <asp:Repeater ID="repeaterResults" runat="server" Visible="False">
                                <HeaderTemplate>
                                    <div class="title">Uploaded files in the target folder:</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                    <%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
        <br /><br />

        
        <h3 style="margin-bottom:3px;">Uploaded Documents</h3>
        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
         OnItemCommand="RadGrid1_ItemCommand" AllowSorting="false">
        <MasterTableView DataKeyNames="Name">

        <Columns>
            <telerik:GridButtonColumn CommandName="Download" Text="Download" ButtonType="PushButton" UniqueName="DownloadColumn" HeaderStyle-Width="80">
            </telerik:GridButtonColumn>
            <telerik:GridHyperLinkColumn DataNavigateUrlFields="FullName" DataTextField="Name" 
               HeaderText="File Name" NavigateUrl="{0}"  HeaderStyle-Width="200">
            </telerik:GridHyperLinkColumn>
            <telerik:GridBoundColumn DataField="CreationTime" HeaderText="Created" ItemStyle-Width="140"
                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:g}" HeaderStyle-Width="140" >
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Length" HeaderText="File Size"
		        DataFormatString="{0:#,### bytes}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
            </telerik:GridBoundColumn>
           <telerik:GridButtonColumn CommandName="Delete" Text="Delete" ButtonType="ImageButton" ImageUrl="~/Images/filterCancel.gif" ItemStyle-Width="50"></telerik:GridButtonColumn>
        </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>

        <FilterMenu EnableImageSprites="False"></FilterMenu>

        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
        </telerik:RadGrid>

        <br />
        <br />

    </div>
</asp:Content>
