﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventOverviewForm : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EventMethods m = new EventMethods();
                EventManager.Model.Event evt = m.GetEvent(EventId);
                if (evt != null)
                {
                    RadPanelBar timeframePanelBar = (RadPanelBar)EventTimeframeRadPane.FindControl("RadPanelBar1");
                    RadPanelItem beforePanelItem = (RadPanelItem)timeframePanelBar.Items[0];
                    RadPanelItem afterPanelItem = (RadPanelItem)timeframePanelBar.Items[1];
                    if (evt.LagDays >= 0)
                    {
                        beforePanelItem.Expanded = true;
                        afterPanelItem.Expanded = false;
                        Label beforeDaysLabel = (Label)beforePanelItem.FindControl("BeforeEventDaysLabel");
                        beforeDaysLabel.Text = "Event is in " + evt.LagTimeString;
                        Panel over2WkPanel = (Panel)beforePanelItem.FindControl("Over2WeeksPanel");
                        Panel under2WkPanel = (Panel)beforePanelItem.FindControl("Under2WeeksPanel");
                        if (evt.LagDays > 14)
                        {
                            over2WkPanel.Visible = true;
                            under2WkPanel.Visible = false;
                        }
                        else
                        {
                            over2WkPanel.Visible = false;
                            under2WkPanel.Visible = true;

                        }
                    }
                    else
                    {
                        Label afterDaysLabel = (Label)afterPanelItem.FindControl("AfterEventDaysLabel");
                        afterDaysLabel.Text = "Event was " + evt.LagTimeString + " ago";

                        beforePanelItem.Expanded = false;
                        afterPanelItem.Expanded = true;
                    }
                }
            }
        }

        private int EventId
        {
            get
            {
                int eventId = 0;
                if (Int32.TryParse(Request.QueryString["EventId"], out eventId))
                {
                    return eventId;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void FlagsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            if (dataItem is GridDataItem)
            {
                EventFlagMethods m = new EventFlagMethods();
                int flagId = Convert.ToInt32(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["Id"]);
                
                Label attCountLabel = (Label)dataItem.FindControl("AttendeeCountLabel");
                attCountLabel.Text = m.GetAttendeeCount(flagId).ToString();
            }
        }

    }
}