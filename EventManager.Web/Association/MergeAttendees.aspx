﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="MergeAttendees.aspx.cs" Inherits="EventManager.Web.Association.MergeAttendeesForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
.content {width:95%; margin: 0 auto;}
.EditAttendeeButton {margin-top:170px;}
.checklist.ul li {margin-top:5px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal" Width="100%" Height="100%">
    <telerik:RadPane runat="server" Height="160">
    
    <div style="margin:10px;">
        <p><i>Before merging attendees</i>:</p>
        <ul class="checklist">
            <li>VERIFY THE PROFILE: Make sure the profile of the attendee on the right is correct.  The profile on the left will be deleted permanently - information will NOT be copied from the left side to the right side.</li>
            <li>CHECK FOR DUPLICATE EVENT REGISTRATIONS: Make sure that the two attendees were never registered for the same event (with different attendee IDs).  If this is the case, the event registration on the left (all information associated with it - flags, attendance) will be deleted. </li>
        </ul>
            <telerik:RadButton ID="MergeButton" runat="server" Text="Merge Attendees" 
                Enabled="false" onclick="MergeButton_Click">
            </telerik:RadButton>
    </div>

    </telerik:RadPane>
    <telerik:RadSplitBar runat="server"></telerik:RadSplitBar>
     <telerik:RadPane ID="RadPane1" runat="server">
     
        <telerik:RadSplitter ID="RadSplitter2" runat="server" Orientation="Vertical">
        <telerik:RadPane ID="DeleteAttendeeRadPane" runat="server">
    
        <div class="content">
            <table class="formview-table-layout">
            <tr>
                <td colspan="2">
                    <h2 style="color:Red;">This attendee will be DELETED</h2>
                </td>
            </tr>
            <tr>
                <td>Attendee ID:</td>
                <td>
                    <telerik:RadTextBox ID="DeleteAttendeeRadTextBox" runat="server" AutoPostBack="true">
                    </telerik:RadTextBox></td>
            </tr>
            </table>

            <asp:FormView ID="DeleteAttendeeFormView" runat="server" 
                DataSourceID="OldAttendeeDataSource"  DataKeyNames="Id"
                ondatabound="DeleteAttendeeFormView_DataBound">
                
                <ItemTemplate>
                    <table class="formview-table-layout">
                    <tr>
                        <td colspan="2"><h3>Attendee Profile</h3><hr /></td>
                    </tr>
                    <tr>
                        <td class="label">Name: </td>
                        <td> <asp:Label ID="Label1" runat="server" Text='<%# Bind("FullName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Facility:</td>
                        <td><asp:Label ID="Label2" runat="server" Text='<%# Bind("Organization.OrgName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Title:</td>
                        <td><asp:Label ID="Label3" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Address:</td>
                        <td><asp:Label ID="Label4" runat="server" Text='<%# Bind("FullAddress") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><asp:Label ID="Label5" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                     <tr>
                        <td class="label">Email:</td>
                        <td><asp:Label ID="Label6" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    </table>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <asp:Label ID="EmptyDataLabel" runat="server" Text='This attendee does not exist.' ForeColor="Red" />
                </EmptyDataTemplate>
            </asp:FormView>
            <br />
            <h3>Attendance History</h3><hr />
            <telerik:RadGrid ID="DeleteRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="OldRegHistoryDataSource" GridLines="None">
                <MasterTableView DataSourceID="OldRegHistoryDataSource">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Registration.Event.EventName" 
                            FilterControlAltText="Filter Event column" HeaderText="Event" 
                            SortExpression="Registration.Event.EventName" UniqueName="EventNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Registration.Organization.OrgName" 
                            FilterControlAltText="Filter Facility column" HeaderText="Facility" 
                            SortExpression="Registration.Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Title" 
                            FilterControlAltText="Filter Title column" HeaderText="Title" 
                            SortExpression="Title" UniqueName="Title">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CeuTotal" DataType="System.Decimal" 
                            FilterControlAltText="Filter CeuTotal column" HeaderText="CeuTotal" 
                            ReadOnly="True" SortExpression="CeuTotal" UniqueName="CeuTotal">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </div>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>
        <telerik:RadPane ID="KeepAttendeeRadPane" runat="server">
        <div class="content">
         <table class="formview-table-layout">
         <tr>
                <td colspan="2">
                    <h2 style="color:Green;">Keep this attendee</h2>
                </td>
            </tr>
            <tr>
                <td>Attendee ID:</td>
                <td>
                    <telerik:RadTextBox ID="KeepAttendeeRadTextBox" runat="server" AutoPostBack="true">
                    </telerik:RadTextBox></td>
            </tr>
            </table>

            <asp:FormView ID="KeepAttendeeFormView" runat="server" 
                DataSourceID="NewAttendeeDataSource" DataKeyNames="Id" 
                ondatabound="KeepAttendeeFormView_DataBound">
                
                <ItemTemplate>
                    <table class="formview-table-layout" style="float:left;">
                    <tr>
                        <td colspan="2"><h3>Attendee Profile</h3><hr /></td>
                    </tr>
                    <tr>
                        <td class="label">Name: </td>
                        <td> <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("FullName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Facility:</td>
                        <td><asp:Label ID="OrgLabel" runat="server" Text='<%# Bind("Organization.OrgName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Title:</td>
                        <td><asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Address:</td>
                        <td><asp:Label ID="AddressLabel" runat="server" Text='<%# Bind("FullAddress") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                     <tr>
                        <td class="label">Email:</td>
                        <td><asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    </table>
                    <telerik:RadButton ID="EditAttendeeRadButton" runat="server" Text="Edit attendee profile" AutoPostBack="false" OnClientClicked="ShowAttendeeDialog" CssClass="EditAttendeeButton">
                    </telerik:RadButton>

                </ItemTemplate>
            </asp:FormView>
            <div style="clear:both;"></div>
            <br />
            <h3>Attendance History</h3><hr />
            <telerik:RadGrid ID="KeepRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="NewRegHistoryDataSource" GridLines="None">
                <MasterTableView DataSourceID="NewRegHistoryDataSource">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Registration.Event.EventName" 
                            FilterControlAltText="Filter Event column" HeaderText="Event" 
                            SortExpression="Registration.Event.EventName" UniqueName="EventNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Registration.Organization.OrgName" 
                            FilterControlAltText="Filter Facility column" HeaderText="Facility" 
                            SortExpression="Registration.Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Title" 
                            FilterControlAltText="Filter Title column" HeaderText="Title" 
                            SortExpression="Title" UniqueName="Title">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CeuTotal" DataType="System.Decimal" 
                            FilterControlAltText="Filter CeuTotal column" HeaderText="CeuTotal" 
                            ReadOnly="True" SortExpression="CeuTotal" UniqueName="CeuTotal">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>

        </div>
        </telerik:RadPane>
        </telerik:RadSplitter>

     </telerik:RadPane>
    </telerik:RadSplitter>

    
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[
        function ShowAttendeeDialog() {


            // Get the attendeeID from textbox
            var attendeeTextBox = $find("<%= KeepAttendeeRadTextBox.ClientID %>");
            var attendeeId = attendeeTextBox.get_value();
            var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

            var attendeeTextBox = document.all("<%=KeepAttendeeRadTextBox.ClientID %>");
            __doPostBack('KeepAttendeeRadTextBox', attendeeTextBox.value);
      
        }
      //]]>
    </script>
</telerik:RadCodeBlock>

    <telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="Attendee ID..."
            Type="Number">
            <TargetControls>
                <telerik:TargetInput ControlID="DeleteAttendeeRadTextBox" />
                <telerik:TargetInput ControlID="KeepAttendeeRadTextBox" />
            </TargetControls>
        </telerik:NumericTextBoxSetting>
    </telerik:RadInputManager>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" 
        EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="OldAttendeeDataSource" runat="server" 
        SelectMethod="GetAttendee" TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="DeleteAttendeeRadTextBox" Name="attendeeId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="NewAttendeeDataSource" runat="server" 
        SelectMethod="GetAttendee" TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="KeepAttendeeRadTextBox" Name="attendeeId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="OldRegHistoryDataSource" runat="server" 
        SelectMethod="GetRegistrationsByAttendee" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="DeleteAttendeeRadTextBox" Name="attendeeId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="NewRegHistoryDataSource" runat="server" 
        SelectMethod="GetRegistrationsByAttendee" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="KeepAttendeeRadTextBox" Name="attendeeId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
