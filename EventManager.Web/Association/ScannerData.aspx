﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="ScannerData.aspx.cs" Inherits="EventManager.Web.Association.ScannerDataForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadSplitter runat="server" ID="EventRadSplitter" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Horizontal" >
    
    <telerik:RadPane ID="NewRegRadPane" runat="server" Scrolling="Both" Height="150" Width="100%">
        
        <h2 style="margin-left:20px;">Scan Report</h2>
        <table class="formview-table-layout" style="margin-left:20px;">
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Find attendee:"></asp:Label></td>
            <td><telerik:RadComboBox ID="AttendeeRadComboBox" runat="server" Width="300" Height="300" AutoPostBack="true" 
                    DataSourceID="AttendeesDataSource" DataTextField="FullNameWithId" AppendDataBoundItems="true" 
                    DataValueField="Id" 
                    onselectedindexchanged="AttendeeRadComboBox_SelectedIndexChanged">
                    <Items>
                    <telerik:RadComboBoxItem runat="server" Text="[All]" Value="0" />
                    </Items>
                </telerik:RadComboBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label2" runat="server" Text="Find session:"></asp:Label></td>
            <td><telerik:RadComboBox ID="SessionsRadComboBox" runat="server" AppendDataBoundItems="true"  AutoPostBack="true" 
                    DataSourceID="SessionsDataSource" DataTextField="SessionName" Width="300" 
                    DropDownWidth="500"  Height="300"
                    DataValueField="Id" 
                    onselectedindexchanged="SessionsRadComboBox_SelectedIndexChanged">
                    <Items>
                    <telerik:RadComboBoxItem runat="server" Text="[All]" Value="0" />
                    </Items>
                </telerik:RadComboBox></td>
        </tr>
        <tr>
            <td></td>
            <td><telerik:RadButton ID="GetResultsRadButton" runat="server" Text="Find" onclick="GetResultsRadButton_Click" />&nbsp;&nbsp;
               <%-- <telerik:RadButton ID="ClearFilterRadButton" runat="server" Text="Clear Filter" onclick="ClearFilterRadButton_Click" />--%></td>
            <td>
                <asp:HyperLink ID="ReportHyperlink" runat="server" Target="_blank" Text="Print Report"></asp:HyperLink>&nbsp;
            </td>
        </tr>
        </table>
      
    </telerik:RadPane>
    
    <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>

    <telerik:RadPane ID="RegGridRadPane" runat="server" EnableViewState="true" Scrolling="Both">
    
    <!--
    AllowPaging="True" 
        PageSize="50" PagerStyle-Position="TopAndBottom"
    -->
        <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="0"
        GridLines="None" Width="100%"
        AutoGenerateColumns="False">
         <groupingsettings casesensitive="False"></groupingsettings>
         <ClientSettings Scrolling-AllowScroll="false"></ClientSettings>
        <MasterTableView DataKeyNames="AttendeeId, SessionId" PagerStyle-Position="TopAndBottom">
            <CommandItemSettings ExportToPdfText="Export to PDF">
            </CommandItemSettings>
            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                            <telerik:GridBoundColumn DataField="AttendeeId" FilterControlAltText="Filter AttendeeId column" HeaderStyle-Width="30" FilterControlWidth="40"
                                HeaderText="Attendee ID" SortExpression="AttendeeId" UniqueName="AttendeeIdGridBoundColumn" CurrentFilterFunction="EqualTo"  ItemStyle-Width="50">
<HeaderStyle Width="30px"></HeaderStyle>

<ItemStyle Width="70px"></ItemStyle>
                            </telerik:GridBoundColumn>
                             
                            <telerik:GridHyperLinkColumn FilterControlAltText="Filter attendee name" HeaderStyle-Width="150px" HeaderText="Attendee"
                                AllowFiltering="true" FilterControlWidth="100px" AutoPostBackOnFilter="true" ShowFilterIcon="true"
                                UniqueName="AttendeeNameHyperlink" CurrentFilterFunction="StartsWith" 
                                DataType="System.String"
                                DataTextField="AttendeeName"
                                DataNavigateUrlFields="AttendeeId"
                                DataNavigateUrlFormatString="AttendeeDetails.aspx?AttendeeId={0}">
                                <HeaderStyle Width="150px"></HeaderStyle>
                                <ItemStyle Width="150px" />
                            </telerik:GridHyperLinkColumn>

                            <telerik:GridBoundColumn DataField="SessionName" FilterControlAltText="Filter SessionName column" CurrentFilterFunction="StartsWith" FilterControlWidth="100" ItemStyle-Width="150" 
                                HeaderText="Session" SortExpression="SessionName" UniqueName="SessionNameGridBoundColumn">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SessionStart" DataType="System.DateTime" FilterControlAltText="Filter SessionStart column" 
                                HeaderText="Date" SortExpression="SessionStart" ItemStyle-Width="70" DataFormatString="{0:d}"  
                                UniqueName="SessionStartGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SessionStart" DataType="System.DateTime" FilterControlAltText="Filter SessionStart column" 
                                HeaderText="Start" SortExpression="SessionStart" ItemStyle-Width="60" DataFormatString="{0:t}"  
                                UniqueName="SessionStartGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SessionEnd" DataType="System.DateTime" FilterControlAltText="Filter SessionEnd column" 
                                HeaderText="End" SortExpression="SessionEnd" ItemStyle-Width="60" DataFormatString="{0:t}"  
                                UniqueName="SessionEndGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReceivedCredit" DataType="System.String" FilterControlAltText="Filter ReceivedCredit column" CurrentFilterFunction="EqualTo" 
                                HeaderText="Credit?" SortExpression="ReceivedCredit" UniqueName="ReceivedCreditGridBoundColumn" ItemStyle-Width="60" FilterControlWidth="40">
<ItemStyle Width="60px"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InScanTime" DataType="System.DateTime" FilterControlAltText="Filter InScanTime column"
                                HeaderText="In" SortExpression="InScanTime" 
                                UniqueName="InScanTimeGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OutScanTime" DataType="System.DateTime" FilterControlAltText="Filter OutScanTime column"
                                HeaderText="Out" SortExpression="OutScanTime" 
                                UniqueName="OutScanTimeGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ScanString" DataType="System.String" FilterControlAltText="Filter ScanString column"
                                HeaderText="All Scans" SortExpression="ScanString" ItemStyle-Width="200" 
                                UniqueName="ScanStringGridBoundColumn" AllowFiltering="False">
                            </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <PagerStyle AlwaysVisible="True" />
        </MasterTableView>
        <HeaderStyle HorizontalAlign="Left" />
        <pagerstyle position="TopAndBottom" ></pagerstyle>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>
    </telerik:RadPane>

    </telerik:RadSplitter>

    
    <script type="text/javascript">
                //<![CDATA[

        function CollapseNewRegPane() {
            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
            var isCollapseSuccess = pane.collapse();

        }

        function RegGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var regGrid = $find("<%= RadGrid1.ClientID %>");
            regGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            regGrid.repaint();
        }

                 //]]>
    </script>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetAttendeesByEvent" 
        TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SessionsDataSource" runat="server" 
        SelectMethod="GetSessionsByEvent" 
        TypeName="EventManager.Business.EventSessionMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

  <%--  <asp:ObjectDataSource ID="ScanReportDataSource" runat="server" 
        SelectMethod="GetScanReport" TypeName="EventManager.Business.ScannerDataMethods" >
		<SelectParameters>
			<asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
		    <asp:ControlParameter ControlID="AttendeeRadComboBox" DefaultValue="0" 
                Name="attendeeId" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="SessionsRadComboBox" DefaultValue="0" 
                Name="sessionId" PropertyName="SelectedValue" Type="Int32" />
		</SelectParameters>
	</asp:ObjectDataSource>--%>

</asp:Content>
