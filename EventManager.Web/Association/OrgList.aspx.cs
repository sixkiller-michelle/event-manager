﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class OrgListForm : System.Web.UI.Page
    {
        public string RadGrid1GridPanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Set the click event for "Print Facility List" menu button
            //LinkButton orgNameButton = (LinkButton)item.FindControl("OrgNameLinkButton");
            //orgNameButton.Attributes["onclick"] = String.Format("return ShowOrgDialog('{0}', '{1}');", orgId, item.ItemIndex);

        }

        protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
        {
            if (e.Updated.ID == "RadGrid1")
            {
                this.RadGrid1GridPanelClientID = e.UpdatePanel.ClientID;
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            GridDataItem item = RadGrid1.MasterTableView.FindItemByKeyValue("Id", Convert.ToInt32(e.Argument));
            if (item != null)
            {
                OrganizationMethods m = new OrganizationMethods();
                Organization org = m.GetOrganization(Convert.ToInt32(e.Argument));
                if (org != null)
                {
                    item.DataItem = org;
                    item.DataBind();
                }
            }
            else
            {
                RadGrid1.DataBind();
            }
        }

        protected void ActionsRadMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Text == "Print Facility List")
            {
                Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?FileName=FacilityList&ParmNames=AssociationId&ParmValues=" + UserInfo.AssociationId.ToString());
            }
            
        }

        protected void OrgDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void FeeCategoryDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void OrgTypeDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void btnShowAll_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            RadGrid1.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGrid1.MasterTableView.RenderColumns)
            {
                if (column.SupportsFiltering())
                {
                    column.CurrentFilterValue = string.Empty;
                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                }
            }
            RadGrid1.MasterTableView.Rebind();

        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string orgId = item.GetDataKeyValue("Id").ToString();
          
                if (item.DataItem != null)
                {
                    // Set the click event for "Show Facility Dialog"
                    LinkButton orgNameButton = (LinkButton)item.FindControl("OrgNameLinkButton");
                    orgNameButton.Attributes["onclick"] = String.Format("return ShowOrgDialog('{0}', '{1}');", orgId, item.ItemIndex);

                    // Set the click event for "Show Staff Dialog"
                    LinkButton staffCountButton = (LinkButton)item.FindControl("StaffCountLinkButton");
                    staffCountButton.Attributes["onclick"] = String.Format("return ShowStaffDialog('{0}', '{1}');", orgId, item.ItemIndex);

                }
            }
        }

        
    }
}