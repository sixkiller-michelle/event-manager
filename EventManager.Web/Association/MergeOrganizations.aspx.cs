﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class MergeOrganizationsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label l = (Label)DeleteOrgFormView.FindControl("EmptyDataLabel");
                if (l != null)
                    l.Visible = false;
            }
            else
            {
               
                if (Request.Form["__EVENTTARGET"].Contains("KeepOrgRadTextBox"))
                    if (Request.Form["__EVENTARGUMENT"] != "")
                    {
                        KeepOrgFormView.DataBind();
                    }
            }
        }

        protected void MergeButton_Click(object sender, EventArgs e)
        {
            try
            {
                EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();
                m.MergeDuplicateOrganizations(Convert.ToInt32(KeepOrgRadTextBox.Text), Convert.ToInt32(DeleteOrgRadTextBox.Text));
                m.SaveAllObjectChanges();

                DeleteOrgFormView.DataBind();
                KeepOrgFormView.DataBind();
                //DeleteRadGrid.DataBind();
                //KeepRadGrid.DataBind();
            }
            catch (Exception ex)
            {
                CustomValidator1.ErrorMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                CustomValidator1.IsValid = false;
            }

        }

        protected void DeleteOrgFormView_DataBound(object sender, EventArgs e)
        {
            if (DeleteOrgFormView.DataItemCount > 0 && KeepOrgFormView.DataItemCount > 0)
            {
                MergeButton.Enabled = true;
            }
            else
            {
                MergeButton.Enabled = false;
            }
        }

        protected void KeepOrgFormView_DataBound(object sender, EventArgs e)
        {
            if (DeleteOrgFormView.DataItemCount > 0 && KeepOrgFormView.DataItemCount > 0)
            {
                MergeButton.Enabled = true;
            }
            else
            {
                MergeButton.Enabled = false;
            }
        }
    }
}