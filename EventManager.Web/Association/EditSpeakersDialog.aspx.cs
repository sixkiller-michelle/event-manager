﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EditSpeakersDialog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the session name by the passed in ID
            if (Request.QueryString["SessionId"] != null)
            {
                int sessionId = Convert.ToInt32(Request.QueryString["SessionId"]);
                EventManager.Business.EventSessionMethods m = new EventManager.Business.EventSessionMethods();
                EventManager.Model.EventSession s = m.GetSession(sessionId);
                if (s != null)
                {
                    SessionNameLabel.Text = s.SessionName;
                    SessionStartTimeLabel.Text = s.StartDateTime.ToString();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }

            if (!IsPostBack)
            {
                NewAttendeePanel.Visible = false;
                InsertSpeakerRadButton.Visible = true;
                LoadAttendees();
            }
        }

        private void LoadAttendees()
        {
           int assnId = UserInfo.AssociationId;

            NewSpeakerRadComboBox.Items.Clear();

            // Look for attendees list in session, if not there, get from database
            if (Session["AttendeesList"] == null)
            {
                RefreshAttendeeSessionList();
            }
            NewSpeakerRadComboBox.DataSource = Session["AttendeesList"];
            NewSpeakerRadComboBox.DataBind();
        
        }

        protected void InsertSpeakerRadButton_Click(object sender, EventArgs e)
        {
            if (NewSpeakerRadComboBox.SelectedValue != null)
            {
                InsertSpeaker(Convert.ToInt32(NewSpeakerRadComboBox.SelectedValue));
                SpeakersRadGrid.DataBind();
            }
        }

        private void InsertSpeaker(int attendeeId)
        {
            EventManager.Business.SessionSpeakerMethods m = new EventManager.Business.SessionSpeakerMethods();
            EventManager.Model.SessionSpeaker s = new EventManager.Model.SessionSpeaker();
            s.SessionId = Convert.ToInt32(Request.QueryString["SessionId"]);
            s.SpeakerId = attendeeId;
            m.Add(s);
            m.SaveAllObjectChanges();
        }

        protected void SpeakersRadGrid_ItemDeleted(object sender, Telerik.Web.UI.GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            { 
                EventManager.Business.SessionSpeakerMethods m = new EventManager.Business.SessionSpeakerMethods();
                m.SaveAllObjectChanges();
                SpeakersRadGrid.DataBind();
            }
           
        }

        protected void CloseWindowRadButton_Click(object sender, EventArgs e)
        {
            // Get the new speakers string
            EventManager.Business.EventSessionMethods m = new EventManager.Business.EventSessionMethods();
            EventManager.Model.EventSession session = m.GetSession(Convert.ToInt32(Request.QueryString["SessionId"]));
            string speakerString = session.SpeakerString;

            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('" + speakerString.ToString() + "');", true);
        }

        protected void NewAttendeeRadButton_Click(object sender, EventArgs e)
        {
            NewAttendeeRadButton.Visible = false;
            NewAttendeePanel.Visible = true;
            NewAttendeeFirstNameRadTextBox.Focus();

        }

        protected void AddAttendeeRadButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(NewAttendeeLastNameRadTextBox.Text) && !string.IsNullOrEmpty(NewAttendeeFirstNameRadTextBox.Text))
            {
                try
                {
                    AttendeeMethods m = new AttendeeMethods();
                    Attendee newAtt = m.AddAttendee(NewAttendeeFirstNameRadTextBox.Text, NewAttendeeLastNameRadTextBox.Text, NewAttendeeTitleRadTextBox.Text, UserInfo.AssociationId);
                    if (newAtt != null)
                    {
                        // Insert the speaker
                        InsertSpeaker(newAtt.Id);
                        NewAttendeeFirstNameRadTextBox.Text = "";
                        NewAttendeeLastNameRadTextBox.Text = "";
                        NewAttendeeTitleRadTextBox.Text = "";
                        NewAttendeePanel.Visible = false;
                        NewAttendeeRadButton.Visible = true;
                        SpeakersRadGrid.DataBind();

                        // Refresh the attendee list and combo
                        RefreshAttendeeSessionList();
                        LoadAttendees();
                    }
                }
                catch (Exception ex)
                {
                    NewAttendeeCustomValidator.ErrorMessage = ex.Message;
                    NewAttendeeCustomValidator.IsValid = false;
                }
                
            }
        }

        private void RefreshAttendeeSessionList()
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            List<EventManager.Model.Attendee> attendees = m.GetAttendees(UserInfo.AssociationId);
            Session["AttendeesList"] = attendees;
        }
    }
}