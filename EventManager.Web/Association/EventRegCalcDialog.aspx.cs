﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventRegCalcDialog : System.Web.UI.Page
    {
        public string AutoChargesRadGridPanelClientID;
        public string ManualChargesRadGridPanelClientID;

        protected void Page_Load(object sender, EventArgs e)
        {

        }   
	
	    protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
	    {
            if (e.Updated.ID == "AutoChargesRadGrid")
		    {
                this.AutoChargesRadGridPanelClientID = e.UpdatePanel.ClientID;
		    }
            if (e.Updated.ID == "RadGrid1")
            {
                this.ManualChargesRadGridPanelClientID = e.UpdatePanel.ClientID;
            }
	    }
        

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int regId = Convert.ToInt32(Request.QueryString["RegId"]);
                decimal chargeTotal = 0;

                // Get the registration record
                RegistrationMethods m1 = new RegistrationMethods();
                chargeTotal = m1.GetCalculatedChargeTotal(regId) + m1.GetChargeTotal(regId);


                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('" + chargeTotal + "');", true);
            }
            catch (Exception ex)
            {
                //CustomValidator1.ErrorMessage = ex.Message;
                //CustomValidator1.IsValid = false;
            }
        }

        protected void AutoChargesMasterTableView_DataBinding(object sender, EventArgs e)
        {
            GridNumericColumn chargeAmtCol = AutoChargesRadGrid.MasterTableView.GetColumnSafe("ChargeAmt") as GridNumericColumn;
            chargeAmtCol.FooterAggregateFormatString = chargeAmtCol.Aggregate.ToString() + ": {0:C}";
        }

        protected void AutoChargesRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);

            RegistrationMethods m = new RegistrationMethods();
            AutoChargesRadGrid.DataSource = m.GetCalculatedCharges(regId);
        }



        protected void MasterTableView_DataBinding(object sender, EventArgs e)
        {
            GridNumericColumn chargeAmtCol = RadGrid1.MasterTableView.GetColumnSafe("ChargeAmt") as GridNumericColumn;
            chargeAmtCol.FooterAggregateFormatString = chargeAmtCol.Aggregate.ToString() + ": {0:C}";
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);

            RegistrationChargeMethods m = new RegistrationChargeMethods();
            RadGrid1.DataSource = m.GetChargesByRegistration(regId);
            
        }

        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;

            try
            {
                RegistrationChargeMethods m = new RegistrationChargeMethods();
                RegistrationCharge ch = new RegistrationCharge();
                ch.RegistrationId = Convert.ToInt32(Request.QueryString["RegId"]);
                item.UpdateValues(ch);

                m.Add(ch);
                m.SaveAllObjectChanges();

                decimal chargeTotal = 0;
                RegistrationMethods m1 = new RegistrationMethods();
                chargeTotal = m1.GetCalculatedChargeTotal(ch.RegistrationId) + m1.GetChargeTotal(ch.RegistrationId);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CloseDialog", "CloseAndRebind('" + chargeTotal + "');", true);
                //RadGrid1.Rebind();
            }
            catch (ArgumentException ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)item.FindControl("CustomValidator1");
                v.ErrorMessage = ex.InnerException.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int regChargeId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            RegistrationChargeMethods am = new RegistrationChargeMethods();
            RegistrationCharge ch = am.GetRegistrationCharge(regChargeId);

            item.UpdateValues(ch);
            am.SaveAllObjectChanges();
            RadGrid1.Rebind();

            // Hide all edit items
            RadGrid1.MasterTableView.ClearEditItems();
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int regChargeId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            EventManager.Business.RegistrationChargeMethods am = new EventManager.Business.RegistrationChargeMethods();

            RegistrationCharge ch = am.GetRegistrationCharge(regChargeId);
            am.Delete(ch);
            am.SaveAllObjectChanges();

            RadGrid1.Rebind();
        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                RadGrid1.MasterTableView.ClearEditItems();

                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert  
                newValues["RegistrationId"] = Convert.ToInt32(Request.QueryString["RegId"]);
                newValues["ChargeAmt"] = 0;
                newValues["ChargeDesc"] = "";
                newValues["ChargeDate"] = DateTime.Now;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);
            }
            else if (e.CommandName == RadGrid.EditCommandName)
            {
                e.Item.OwnerTableView.IsItemInserted = false;
            }
            else if (e.CommandName == "PerformInsert")             
            {                 
                //make sure the insert user control closes automatically after inserted
                //e.Item.OwnerTableView.IsItemInserted = false;             
            } 
        }

        protected void AutoChargesRadGrid_DataBound(object sender, EventArgs e)
        {
            UpdateChargeTotal();
        }

        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            UpdateChargeTotal();
        }

        private void UpdateChargeTotal()
        {
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);
            decimal chargeTotal = 0;

            // Get the registration record
            RegistrationMethods m1 = new RegistrationMethods();
            chargeTotal = m1.GetCalculatedChargeTotal(regId) + m1.GetChargeTotal(regId);

            TotalLabel.Text = "Total: " + chargeTotal.ToString("C");
        }

        private void Page_Error(object sender, EventArgs e)
        {

            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc is System.Runtime.InteropServices.COMException)
            {
                // Show user the error
                if (exc.Message.Contains("Connection failed"))
                {
                    errorMsg = "The report server is unable to connect to the database. Try refreshing this page, or try again later.";
                }
                else
                {
                    errorMsg = "The report server is currently experiencing high traffic.  Try refreshing this page, or try again later.";
                }

                // Log the exception and notify system operators
                ExceptionUtility.LogException(exc, "EventReg.aspx");
                ExceptionUtility.NotifySystemOps(exc);

                // Clear the error from the server
                Server.ClearError();

            }
            else
            {
                // Pass the error on to the default global handler
            }
        }
        
    }
}