﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;
using Telerik.Web.UI;

namespace EventManager.Web.Association
{
    public partial class EventFlagsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

            
            if (!IsPostBack)
            {
               
            }
          
        }

        private int EventId
        {
            get
            {
                int eventId = 0;
                if (Int32.TryParse(Request.QueryString["EventId"], out eventId))
                {
                    return eventId;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void RadGrid1_ItemUpdated(object sender, Telerik.Web.UI.GridUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.EventFlagMethods efm = new EventManager.Business.EventFlagMethods();
                efm.SaveAllObjectChanges();

                RadGrid1.DataBind();
            }
        }

        protected void RadGrid1_ItemDeleted(object sender, Telerik.Web.UI.GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.EventFlagMethods efm = new EventManager.Business.EventFlagMethods();
                efm.SaveAllObjectChanges();

                RadGrid1.DataBind();
            }
        }

        protected void NewFlagAddButton_Click(object sender, EventArgs e)
        {
            EventManager.Business.EventFlagMethods efm = new EventManager.Business.EventFlagMethods();
            EventManager.Model.EventFlag f = new EventManager.Model.EventFlag();

            f.EventId = EventId;
            f.FlagCode = NewFlagCodeTextBox.Text;
            f.FlagName = NewFlagNameTextBox.Text;
            f.PrintOnBadge = NewFlagPrintBadgeCheckBox.Checked;
            efm.Add(f);

            try
            {
                efm.SaveAllObjectChanges();
                NewFlagNameTextBox.Text = "";
                NewFlagCodeTextBox.Text = "";
                NewFlagPrintBadgeCheckBox.Checked = false;
                NewFlagNameTextBox.Focus();

                RadGrid1.DataBind();
            }
            catch (Exception ex)
            { 
            
            }
        }

        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            //SetColumnHeaderText();
        }

        private void SetColumnHeaderText()
        {
            // Get all reg time periods to update column headers
            if (EventId > 0)
            {
                EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                List<EventFeeSchedule> timeframes = m.GetFeeSchedulesForEvent(EventId);

                GridBoundColumn priceCol1 = (GridBoundColumn)RadGrid1.MasterTableView.GetColumnSafe("Period1PriceGridBoundColumn");
                GridBoundColumn priceCol2 = (GridBoundColumn)RadGrid1.MasterTableView.GetColumnSafe("Period2PriceGridBoundColumn");
                GridBoundColumn priceCol3 = (GridBoundColumn)RadGrid1.MasterTableView.GetColumnSafe("Period3PriceGridBoundColumn");

                if (timeframes.Count > 0)
                {
                    priceCol1.HeaderText = timeframes[0].TimeframeName;

                    if (timeframes.Count > 1)
                    {
                        priceCol2.HeaderText = timeframes[1].TimeframeName;

                        if (timeframes.Count > 2)
                        {
                            priceCol3.HeaderText = timeframes[2].TimeframeName;
                        }
                        else
                        {
                            priceCol3.Visible = false;
                        }
                    }
                    else
                    {
                        priceCol2.Visible = false;
                        priceCol3.Visible = false;
                    }
                }
                else
                {
                    priceCol1.Visible = false;
                    priceCol2.Visible = false;
                    priceCol3.Visible = false;
                }

            }
        }

        protected void RadGrid1_Init(object sender, EventArgs e)
        {
            SetColumnHeaderText();
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            //SetColumnHeaderText();
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem)
            {
                if (e.Item.IsInEditMode)
                {
                    // Get all reg time periods to hide/show price textboxes
                    if (EventId > 0)
                    {
                        int flagId = Convert.ToInt32(((GridEditFormItem)e.Item).GetDataKeyValue("Id"));

                        Label price1Label = (Label)e.Item.FindControl("Period1PriceLabel");
                        RadNumericTextBox price1TextBox = (RadNumericTextBox)e.Item.FindControl("Period1PriceRadNumericTextBox");

                        Label price2Label = (Label)e.Item.FindControl("Period2PriceLabel");
                        RadNumericTextBox price2TextBox = (RadNumericTextBox)e.Item.FindControl("Period2PriceRadNumericTextBox");

                        Label price3Label = (Label)e.Item.FindControl("Period3PriceLabel");
                        RadNumericTextBox price3TextBox = (RadNumericTextBox)e.Item.FindControl("Period3PriceRadNumericTextBox");

                        EventFeeScheduleMethods m = new EventFeeScheduleMethods();
                        List<EventFeeSchedule> timeframes = m.GetFeeSchedulesForEvent(EventId);

                        if (timeframes.Count > 0)
                        {
                            price1Label.Text = timeframes[0].TimeframeName;

                            if (timeframes.Count > 1)
                            {
                                price2Label.Text = timeframes[1].TimeframeName;

                                if (timeframes.Count > 2)
                                {
                                    price3Label.Text = timeframes[2].TimeframeName;
                                }
                                else
                                {
                                    price3Label.Visible = false;
                                    price3TextBox.Enabled = false;
                                }
                            }
                            else
                            {
                                price2Label.Visible = false;
                                price3Label.Visible = false;
                                price2TextBox.Enabled = false;
                                price3TextBox.Enabled = false;
                            }
                        }
                        else
                        {
                            price1Label.Visible = false;
                            price2Label.Visible = false;
                            price3Label.Visible = false;
                            price1TextBox.Enabled = false;
                            price2TextBox.Enabled = false;
                            price3TextBox.Enabled = false;
                        }
                    }

                }
            }
        }
    }
}