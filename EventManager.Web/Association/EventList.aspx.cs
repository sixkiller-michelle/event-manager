﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using System.Web.Security;

namespace EventManager.Web.Association
{
    public partial class EventListForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void EventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void ShowAllFutureEventsImageButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            FutureEventsRadGrid.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in FutureEventsRadGrid.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
                if (column is GridTemplateColumn)
                {
                    GridTemplateColumn templateColumn = column as GridTemplateColumn;
                    templateColumn.CurrentFilterValue = string.Empty;
                }
                if (column is GridHyperLinkColumn)
                {
                    GridHyperLinkColumn hyperlinkColumn = column as GridHyperLinkColumn;
                    hyperlinkColumn.CurrentFilterValue = string.Empty;
                }
            }

            FutureEventsRadGrid.MasterTableView.Rebind();

        }

        protected void ShowAllPastEventsImageButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            PastEventsRadGrid.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in PastEventsRadGrid.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
                if (column is GridTemplateColumn)
                {
                    GridTemplateColumn templateColumn = column as GridTemplateColumn;
                    templateColumn.CurrentFilterValue = string.Empty;
                }
                if (column is GridHyperLinkColumn)
                {
                    GridHyperLinkColumn hyperlinkColumn = column as GridHyperLinkColumn;
                    hyperlinkColumn.CurrentFilterValue = string.Empty;
                }
            }

            PastEventsRadGrid.MasterTableView.Rebind();

        }

        protected void FutureEventsRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            Guid userId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            EventMethods m = new EventMethods();
            FutureEventsRadGrid.DataSource = m.GetEventList(UserInfo.AssociationId, userId).Where(evt => evt.StartDateTime >= DateTime.Today.Date).OrderBy(evt => evt.StartDateTime).ToList();
        }

        protected void FutureEventsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hyplnk = (HyperLink)item["EventNameHyperlink"].Controls[0];
                hyplnk.ForeColor = System.Drawing.Color.Blue;
            }
        }

        protected void PastEventsRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            EventMethods m = new EventMethods();
            PastEventsRadGrid.DataSource = m.GetEventList(UserInfo.AssociationId).Where(evt => evt.StartDateTime < DateTime.Today.Date).OrderByDescending(evt => evt.StartDateTime).ToList();
        }

        protected void PastEventsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HyperLink hyplnk = (HyperLink)item["EventNameHyperlink"].Controls[0];
                hyplnk.ForeColor = System.Drawing.Color.Blue;
            } 
        }



    }
}