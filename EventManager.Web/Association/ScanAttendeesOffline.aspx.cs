﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class ScanAttendeesOffline : System.Web.UI.Page
    {
        int eventId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            int assnId = UserInfo.AssociationId;
            try
            {
                AssociationLogoImage.ImageUrl = "~/Association/Documents/Associations/" + assnId.ToString() + "/logo.png";
            }
            finally { }

            if (Request.QueryString["EventId"] != null)
            {
                eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            } 
            
            // Hide / Show the attendee info (name and scan box)
           if (SessionsRadComboBox.SelectedIndex <= 0)
            {
                AttendeeNameContainer.Visible = false;
            }
            else
            {
                AttendeeNameContainer.Visible = true;
                AttendeeIdTextBox2.Focus();
            }

            // If post back, log the scan (if valid ID entered)
            if (IsPostBack)
            {
                if (Request.Form["__EVENTTARGET"] == "AttendeeIdTextBox2")
                    if (Request.Form["__EVENTARGUMENT"] != "")
                        LogScan(Request.Form["__EVENTARGUMENT"]);

            }
           

           
        }

        private void LogScan(string attendeeId)
        {
            // Get the attendee
            if (!String.IsNullOrEmpty(attendeeId))
            {
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = m.GetAttendee(Convert.ToInt32(attendeeId));
                if (a != null)
                {
                    AttendeeNameLabel.Text = a.FullNameFNF;

                    // Log a scan for this session
                    EventManager.Business.ScannerDataMethods m2 = new EventManager.Business.ScannerDataMethods();
                    EventManager.Model.ScannerData scan = new EventManager.Model.ScannerData();
                    scan.EventId = eventId;
                    scan.SessionId = Convert.ToInt32(SessionsRadComboBox.SelectedValue);
                    scan.AttendeeId = a.Id;
                    scan.ScanDate = DateTime.Now.Date.ToShortDateString();
                    scan.ScanTime = DateTime.Now.ToString("H:mm:ss");

                    m2.Add(scan);
                    m2.SaveAllObjectChanges();

                    AttendeeIdTextBox2.Text = "";
                    AttendeeIdTextBox2.Focus();
                }
                else
                {
                    AttendeeIdTextBox2.Text = "";
                    AttendeeNameLabel.Text = "";
                    AttendeeIdTextBox2.Focus();
                }
            }
        }

        protected void AttendeeIdTextBox_TextChanged(object sender, EventArgs e)
        {
            //LogScan();
        }

            
        protected void SessionsRadComboBox_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ResetEntry();
        }

        private void ResetEntry()
        {
            AttendeeIdTextBox2.Text = "";
            AttendeeNameLabel.Text = "";
            //AttendeeIdTextBox.SelectionOnFocus = Telerik.Web.UI.SelectionOnFocus.SelectAll;

        }
    }
}