﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="OrgDetails.aspx.cs" Inherits="EventManager.Web.Association.OrgDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 71px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="content">

Back to: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Association/OrgList.aspx">Facility List</asp:HyperLink>
<br />
        

<table style="width:100%;">
<tr>
    <td style="width:400px; vertical-align:top;">
    <asp:ValidationSummary ID="OrgValidationSummary" runat="server" ValidationGroup="EditOrg" ForeColor="Red" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="None" ValidationGroup="EditOrg" ForeColor="Red"></asp:CustomValidator>

    <asp:FormView ID="FormView1" runat="server" DataSourceID="OrgDataSource" 
            DataKeyNames="Id" DefaultMode="Edit" 
            oniteminserted="FormView1_ItemInserted" 
            onitemupdated="FormView1_ItemUpdated" 
            onitemdeleted="FormView1_ItemDeleted" 
            oniteminserting="FormView1_ItemInserting" 
            onitemupdating="FormView1_ItemUpdating" >
    <EditItemTemplate>
        <table cellspacing="0" cellpadding="0" class="formview-table-layout nowrap">
            <tr>
                <td colspan="2"><h1><asp:Label ID="Label1" runat="server" Text='<%# Eval("OrgName") %>' ViewStateMode="Disabled"></asp:Label></h1></td>
            </tr>
            <tr>
                <td colspan="2">
                <telerik:RadButton ID="RadButton11" runat="server" Text="Update" CausesValidation="True" CommandName="Update" ValidationGroup="Insert" />&nbsp;
                <telerik:RadButton ID="RadButton12" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                </td>
            </tr>
				<tr>
					<td>Name:</td>
					<td><telerik:RadTextBox ID="OrgNameDynamicControl" runat="server" Text='<%# Bind("OrgName") %>' />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorOrgName" runat="server" ControlToValidate="OrgNameDynamicControl"                         
                            ErrorMessage="Facility name is Required" ForeColor="Red" ValidationGroup="EditOrg">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
					<td>Type:</td>
					<td><telerik:RadComboBox
                            ID="OrgTypeRadComboBox" runat="server" DataSourceID="OrgTypesDataSource" 
                            DataTextField="OrgTypeName" DataValueField="Id" SelectedValue='<%# Bind("OrgTypeId") %>'
                            DropDownWidth="300" AutoPostBack="true" 
                            onselectedindexchanged="OrgTypeRadComboBox_SelectedIndexChanged">
                       </telerik:RadComboBox>
					</td>
				</tr>
                <tr>
						<td style="width: 89px">
							Fee Category:</td>
						<td>
                        <telerik:RadComboBox
                                ID="FeeCategoryDropDownList" runat="server" DataSourceID="FeeCategoriesDataSource"
                                DataTextField="CategoryDesc" DataValueField="Id" EnableViewState="true"
                                SelectedValue='<%# Bind("OrgFeeCategoryId") %>' 
                                DropDownWidth="300" onitemsrequested="FeeCategoryDropDownList_ItemsRequested">
                            </telerik:RadComboBox>
						</td>
					</tr>
                <tr>
					<td>Corporate Affiliation:</td>
					<td>
						<telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' />
					</td>
				</tr>
                <tr>
					<td>Beds:</td>
					<td><telerik:RadTextBox ID="BedCountRadTextBox" runat="server" Text='<%# Bind("BedCount") %>' Width="50"  />
                    <asp:CompareValidator ID="BedCountCompareFieldValidator" runat="server" ControlToValidate="BedCountRadTextBox" 
                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Input valid bed count" ValidationGroup="EditOrg">  
                    </asp:CompareValidator>
                    &nbsp;<asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMember") %>' Text="Member" /></td>
				</tr>
                <tr>
                    <td colspan="2"><h3>Facility Contact Info</h3></td>
                </tr>
				<tr>
					<td >
						Street:</td>
					<td>
						<telerik:RadTextBox ID="AddressRadTextBox" runat="server" Text='<%# Bind("Address") %>' TextMode="MultiLine" />
					</td>
				</tr>
				<tr>
					<td>
						City:</td>
					<td>
						<telerik:RadTextBox ID="CityRadTextBox" runat="server" Text='<%# Bind("City") %>' />
					</td>
				</tr>
				<tr>
					<td >
						State:</td>
					<td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode" Width="130" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="200">
                            </telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td>
						Zip:
					</td>
					<td>
						<telerik:RadTextBox ID="ZipRadTextBox" runat="server" Text='<%# Bind("Zip") %>' />
					</td>
				</tr>
				<tr>
					<td >
						Phone:</td>
					<td>
						<telerik:RadTextBox ID="PhoneRadTextBox" runat="server" Text='<%# Bind("Phone") %>' />
					</td>
				</tr>
				<tr>
					<td >
						Fax:</td>
					<td>
						<telerik:RadTextBox ID="FaxRadTextBox" runat="server" Text='<%# Bind("Fax") %>' />
					</td>
				</tr>
			    <tr>
                    <td colspan="2"><h3>Administrative Contact</h3></td>
                </tr>
				<tr>
					<td colspan="2">
						<telerik:RadComboBox
                                ID="AdminRadComboBox" runat="server" DataSourceID="AttendeesDataSource"
                                DataTextField="FullName" DataValueField="Id" EnableViewState="true" AppendDataBoundItems="true"
                                SelectedValue='<%# Bind("AdminAttendeeId") %>' 
                                DropDownWidth="150">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="System.DBNull.Value" />
                                </Items>
                            </telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:Label ID="AdminEmailLabel" runat="server" Text='<%# Eval("Attendee.Email") %>' />
					</td>
				</tr>
					
				</table>
			</div>
			<div style="clear:both"></div>
            <br />
			 <telerik:RadButton ID="RadButton13" runat="server" Text="Update" CausesValidation="True" CommandName="Update" ValidationGroup="EditOrg" />&nbsp;
             <telerik:RadButton ID="RadButton14" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />

    </EditItemTemplate>

    <InsertItemTemplate>
        <table cellspacing="0" cellpadding="0" class="formview-table-layout">
            <tr>
                <td colspan="2"><h1>New Facility</h1></td>
            </tr>
            <tr>
                <td colspan="2">
                   <telerik:RadButton ID="RadButton9" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
                    <telerik:RadButton ID="RadButton10" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                </td>
            </tr>
				<tr>
					<td>Name:</td>
					<td><telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Text='<%# Bind("OrgName") %>' Width="300" /></td>
                </tr>
                <tr>
					<td>Type:</td>
					<td><telerik:RadComboBox
                                ID="OrgTypeRadComboBox" runat="server" DataSourceID="OrgTypesDataSource" 
                                DataTextField="OrgTypeName" DataValueField="Id" Width="300" SelectedValue='<%# Bind("OrgTypeId") %>'
                                DropDownWidth="300">
                            </telerik:RadComboBox>
					</td>
				</tr>
                <tr>
						<td>
							Fee Category:</td>
						<td>
                        <telerik:RadComboBox
                                ID="FeeCategoryDropDownList" runat="server" DataSourceID="FeeCategoriesDataSource" 
                                DataTextField="CategoryDesc" DataValueField="Id"  Width="300"
                                SelectedValue='<%# Bind("OrgFeeCategoryId") %>' DropDownWidth="300">
                            </telerik:RadComboBox>
						</td>
					</tr>
                <tr>
					<td>Corporate Affiliation:</td>
					<td>
						<telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' Width="300" />
					</td>
				</tr>
                <tr>
					<td>Beds:</td>
					<td><telerik:RadTextBox ID="BedCountRadTextBox" runat="server" Text='<%# Bind("BedCount") %>' Width="50"  /></td>
				</tr>
				
				<tr>
					<td>Member:</td>
					<td>
						<asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMember") %>' Text="Member" />
					</td>
				</tr>
                <tr>
                    <td colspan="2"><h3>Contact Information</h3></td>
                </tr>
				<tr>
					<td >
						Street:</td>
					<td>
						<telerik:RadTextBox ID="AddressRadTextBox" runat="server" Text='<%# Bind("Address") %>' TextMode="MultiLine" Rows="2" />
					</td>
				</tr>
				<tr>
					<td >
						City:</td>
					<td>
						<telerik:RadTextBox ID="CityRadTextBox" runat="server" Text='<%# Bind("City") %>' />
					</td>
				</tr>
				<tr>
					<td >
						State:</td>
					<td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode" Width="100" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="300">
                            </telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td>
						Zip:
					</td>
					<td>
						<telerik:RadTextBox ID="ZipRadTextBox" runat="server" Text='<%# Bind("Zip") %>' />
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;</td>
					<td>
						&nbsp;</td>
				</tr>
				<tr>
					<td>
						Phone:</td>
					<td>
						<telerik:RadTextBox ID="PhoneRadTextBox" runat="server" Text='<%# Bind("Phone") %>' />
					</td>
				</tr>
				<tr>
					<td>
						Fax:</td>
					<td>
						<telerik:RadTextBox ID="FaxRadTextBox" runat="server" Text='<%# Bind("Fax") %>' />
					</td>
				</tr>
			    <tr>
                    <td colspan="2"><h3>Administrative Contact</h3></td>
                </tr>
				<tr>
					<td colspan="2">
						<telerik:RadComboBox
                                ID="AdminRadComboBox" runat="server" DataSourceID="AttendeesDataSource"
                                DataTextField="FullName" DataValueField="Id" EnableViewState="true" AppendDataBoundItems="true"
                                SelectedValue='<%# Bind("AdminAttendeeId") %>' 
                                DropDownWidth="150">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="System.DBNull.Value" />
                                </Items>
                            </telerik:RadComboBox>
					</td>
					
				</tr>
					
				</table><br />
            <telerik:RadButton ID="RadButton7" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
            <telerik:RadButton ID="RadButton8" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />

    </InsertItemTemplate>

    <ItemTemplate>
        <table cellspacing="0" cellpadding="0" class="formview-table-layout">
            <tr>
                <td colspan="3"><h1><asp:Label ID="Label1" runat="server" Text='<%# Eval("OrgName") %>' ViewStateMode="Disabled"></asp:Label></h1></td>
            </tr>
            <tr>
                <td colspan="3">
                <telerik:RadButton ID="RadButton4" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" />&nbsp;
                <telerik:RadButton ID="RadButton5" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" />&nbsp;
                <telerik:RadButton ID="RadButton6" runat="server" Text="New" CausesValidation="False" CommandName="New" />
                </td>
            </tr>
            <tr>
					<td>Id:</td>
					<td colspan="2"><telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind("Id") %>' ReadOnly="true" Width="40" /></td>
                </tr>
				<tr>
					<td>Name:</td>
					<td colspan="2"><telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Text='<%# Bind("OrgName") %>' ReadOnly="true" Width="100%" /></td>
                </tr>
                <tr>
					<td>Type:</td>
					<td colspan="2"><telerik:RadTextBox ID="OrgTypeNameRadTextBox" runat="server" Text='<%# Bind("OrganizationType.OrgTypeName") %>' ReadOnly="true" Width="100%" />
					</td>
				</tr>
                <tr>
					<td>Corporate Affiliation:</td>
					<td colspan="2">
						<telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' ReadOnly="true" Width="100%" />
					</td>
				</tr>
                <tr>
					<td>Beds:</td>
					<td><telerik:RadTextBox ID="BedCountDynamicControl" runat="server" Text='<%# Bind("BedCount") %>' ReadOnly="true" Width="50"  /></td>
				    <td align="right" class="style1"><asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMember") %>' Text="Member" /></td>
                </tr>
                <tr>
                    <td colspan="3"><h3>Facility Contact Info</h3></td>
                </tr>
				<tr>
					<td >
						Address:</td>
					<td colspan="2">
						<telerik:RadTextBox ID="AddressDynamicControl" runat="server" Text='<%# Bind("FullAddress") %>' TextMode="MultiLine" ReadOnly="true" />
					</td>
				</tr>
				
				<tr>
					<td >
						Phone:</td>
					<td colspan="2">
						<telerik:RadTextBox ID="PhoneDynamicControl" runat="server" Text='<%# Bind("Phone") %>' ReadOnly="true" />
					</td>
				</tr>
				<tr>
					<td >
						Fax:</td>
					<td colspan="2">
						<telerik:RadTextBox ID="FaxDynamicControl" runat="server" Text='<%# Bind("Fax") %>' ReadOnly="true" />
					</td>
				</tr>
			    <tr>
                    <td colspan="3"><h3>Administrative Contact</h3></td>
                </tr>
				<tr>
					<td>
						Name:</td>
					<td>
						<telerik:RadTextBox ID="AdminNameRadTextBox" runat="server" Text='<%# Bind("Attendee.FullName") %>' />
					</td>
					
				</tr>
				<tr>
					<td>
						Email:</td>
					<td>
						<telerik:RadTextBox ID="AdminEmailRadTextBox" runat="server" Text='<%# Bind("Attendee.Email") %>' />
					</td>
				</tr>
					<tr>
						<td>
							Fee Category:</td>
						<td colspan="2"><telerik:RadTextBox ID="RadTextBox3" runat="server" Text='<%# Bind("OrganizationFeeCategory.CategoryDesc") %>' ReadOnly="true" />
						</td>
					</tr>
				</table>
        <br />
        <telerik:RadButton ID="RadButton1" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" />&nbsp;
        <telerik:RadButton ID="RadButton2" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" />&nbsp;
        <telerik:RadButton ID="RadButton3" runat="server" Text="New" CausesValidation="False" CommandName="New" />
   
    </ItemTemplate>
    </asp:FormView>
    </td>
    <td style="vertical-align:top; padding-left:20px;">
        <br />
        <h3>Staff Members</h3>
        <telerik:RadGrid ID="AttendeesRadGrid" runat="server"  
            AutoGenerateColumns="False" CellSpacing="0" DataSourceID="AttendeesDataSource" 
            OnDeleteCommand="AttendeesRadGrid_DeleteCommand"
            GridLines="None" AllowPaging="True" PageSize="5" PagerStyle-Position="Bottom">
        <MasterTableView DataSourceID="AttendeesDataSource" DataKeyNames="Id" >
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" 
                    FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" 
                    UniqueName="Id">
                </telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn FilterControlAltText="Filter attendee name" HeaderStyle-Width="150px" HeaderText="Attendee"
                                FilterControlWidth="120px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                                UniqueName="AttendeeNameHyperlink"
                                DataType="System.String"
                                DataTextField="FullName"
                                DataNavigateUrlFields="Id"
                                DataNavigateUrlFormatString="AttendeeDetails.aspx?AttendeeId={0}">
                            </telerik:GridHyperLinkColumn>
                <telerik:GridBoundColumn DataField="Title" 
                    FilterControlAltText="Filter Title column" HeaderText="Title" 
                    SortExpression="Title" UniqueName="Title">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Email" 
                    FilterControlAltText="Filter Email column" HeaderText="Email" 
                    SortExpression="Email" UniqueName="Email">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PhoneNumber" 
                    FilterControlAltText="Filter PhoneNumber column" HeaderText="Phone" 
                    SortExpression="PhoneNumber" UniqueName="PhoneNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UserId" DataType="System.Guid" 
                    FilterControlAltText="Filter UserId column" HeaderText="User Id" 
                    SortExpression="UserId" UniqueName="UserId">
                </telerik:GridBoundColumn>
                 <telerik:GridButtonColumn ConfirmText="Remove this staff member?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Remove" ButtonType="ImageButton" CommandName="Delete" ConfirmDialogHeight="100px"
                            ConfirmDialogWidth="220px" />

            </Columns>

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>
            <br />
            <telerik:RadComboBox
                 ID="NewAttendeeRadComboBox" runat="server"
                 Width="300px" Height="140px"
                 EmptyMessage="Type attendee name..."
                 MarkFirstMatch="true"
                 AllowCustomText="true"
                 EnableLoadOnDemand="true"
                AutoPostBack="true"
                onselectedindexchanged="NewAttendeeRadComboBox_SelectedIndexChanged" onitemsrequested="NewAttendeeRadComboBox_ItemsRequested">
             </telerik:RadComboBox>

             <br /><br />
        <h3>Create New Staff Member</h3>

        <asp:FormView ID="FormView2" runat="server" DataSourceID="AttendeeDataSource" 
            DefaultMode="Insert" oniteminserting="FormView2_ItemInserting" >
            <InsertItemTemplate>
                <table>
                    <tr>
                       <td>First:</td><td><asp:TextBox ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                       <td>Last:</td><td><asp:TextBox ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' /></td>
                       <td>MI:</td><td><asp:TextBox ID="MiddleInitialLabel" runat="server" Text='<%# Bind("MiddleInitial") %>' Width="30" /></td>
                    </tr>
                    <tr>
                        <td>Title:</td><td colspan="5"><asp:TextBox ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' Width="200" /></td>
                    </tr>
                    <tr>
                        <td>Email:</td><td colspan="5"><asp:TextBox ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' Width="200" /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td><td colspan="5"><asp:TextBox ID="Label2" runat="server" Text='<%# Bind("PhoneNumber") %>' Width="200" /></td>
                    </tr>
                </table><br />
                <telerik:RadButton ID="InsertButton" runat="server" CausesValidation="True"  
                    CommandName="Insert" Text="Insert" onclick="InsertButton_Click" /> &nbsp;
                <telerik:RadButton ID="InsertCancelButton" runat="server"   CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                
                <table>
                    <tr>
                       <td>First:</td><td><asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                       <td>Last:</td><td><asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' /></td>
                       <td>MI:</td><td><asp:Label ID="MiddleInitialLabel" runat="server" Text='<%# Bind("MiddleInitial") %>' /></td>
                    </tr>
                    <tr>
                        <td>Title:</td><td colspan="5"><asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td>Email:</td><td colspan="5"><asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td><td colspan="5"><asp:Label ID="Label2" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                </table>
                           
                <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>

    </td>
    
</tr>
</table>
    
    <asp:ObjectDataSource ID="OrgTypesDataSource" runat="server" 
        onselecting="OrgTypesDataSource_Selecting" SelectMethod="GetOrganizationTypes" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="FeeCategoriesDataSource" runat="server" 
        onselecting="FeeCategoriesDataSource_Selecting" SelectMethod="GetFeeCategories" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeeDataSource" runat="server" 
            DataObjectTypeName="EventManager.Model.Attendee" InsertMethod="AddAttendee"
            SelectMethod="GetAttendee" TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:Parameter Name="attendeeId" Type="Int32" />
        </SelectParameters>
        </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="OrgDataSource" runat="server" 
            SelectMethod="GetOrganization" TypeName="EventManager.Business.OrganizationMethods" 
            DataObjectTypeName="EventManager.Model.Organization" InsertMethod="AddOrganization" DeleteMethod="Delete" 
            UpdateMethod="Update" 
            oninserted="OrgDataSource_Inserted">
        <SelectParameters>
            <asp:QueryStringParameter Name="orgId" QueryStringField="OrgId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" 
        SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetAttendeesByOrganization" 
        TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="orgId" QueryStringField="OrgId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
</asp:Content>
