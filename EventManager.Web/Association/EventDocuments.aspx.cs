﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace EventManager.Web.Association
{
    public partial class EventDocumentsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

            if (!IsPostBack)
                GetDocumentsForEvent();
            
        }

        private void GetDocumentsForEvent()
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            string folderPath = "~/Association/Documents/Events/" + eventId.ToString();

            DirectoryInfo myDir = new DirectoryInfo(Server.MapPath(folderPath));

            FileSystemInfo[] fileInfos = myDir.GetFileSystemInfos();

            // sort them by creation time 
            Array.Sort<FileSystemInfo>(fileInfos, delegate(FileSystemInfo a, FileSystemInfo b)
            {
                return a.LastWriteTime.CompareTo(b.LastWriteTime);
            });

            RadGrid1.DataSource = fileInfos.Reverse();
            RadGrid1.DataBind();
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            string folderPath = "~/Association/Documents/Events/" + eventId.ToString();

            // Make sure a folder exists for the event
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                // Create the folder
                Directory.CreateDirectory(Server.MapPath(folderPath));
            }


            if (RadUpload1.UploadedFiles.Count > 0)
            {
                RadUpload1.TargetFolder = folderPath;

                repeaterResults.DataSource = RadUpload1.UploadedFiles;
                repeaterResults.DataBind();
                repeaterResults.Visible = true;


                GetDocumentsForEvent();

            }

        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                string fileName = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Name"].ToString();
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                string filePath = "~/Association/Documents/Events/" + eventId.ToString() + "/" + fileName;

                Response.ContentType = "text/plain";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();
            }
            else if (e.CommandName == "Delete")
            { 
                string fileName = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Name"].ToString();
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                string filePath = "~/Association/Documents/Events/" + eventId.ToString() + "/" + fileName;
                try
                {
                    File.Delete(Server.MapPath(filePath));
                    GetDocumentsForEvent();
                }
                finally 
                { 
                    
                }
            }
        }

        protected void RadUpload1_Init(object sender, EventArgs e)
        {
            // Set the upload directory
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            string folderPath = "~/Association/Documents/Events/" + eventId.ToString();

            // Make sure a folder exists for the event
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                // Create the folder
                Directory.CreateDirectory(Server.MapPath(folderPath));
            }
        }

    }
}