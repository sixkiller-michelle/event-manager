﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;

namespace EventManager.Web.Association
{
    public partial class EditEventForm : System.Web.UI.Page
    {
        int newEventId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                if (Request.QueryString["EventId"] == null || Request.QueryString["EventId"].ToString() == "0")
                {
                    //Response.Redirect("~/Association/EventList.aspx");
                    FormView1.ChangeMode(FormViewMode.Insert);
                }
            }
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["AssociationId"] = UserInfo.AssociationId;
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    EventDataSource.DataBind();
                    FormView1.DataBind();
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    CustomValidator1.IsValid = false;
                    e.KeepInEditMode = true;
                    e.ExceptionHandled = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    Response.Redirect("~/Association/EditEvent.aspx?EventId=" + newEventId.ToString());
                    //OrgDataSource.DataBind();
                    //e.KeepInInsertMode = true;
                    //FormView1.ChangeMode(FormViewMode.ReadOnly);
                    //FormView1.DataBind();
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    CustomValidator1.IsValid = false;
                    e.KeepInInsertMode = true;
                    e.ExceptionHandled = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                CustomValidator1.IsValid = false;
                e.KeepInInsertMode = true;
                e.ExceptionHandled = true;
            }
        }

        protected void FormView1_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.EventMethods am = new EventManager.Business.EventMethods();
                am.SaveAllObjectChanges();
                Response.Redirect("~/Association/EventList.aspx");
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void EventDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue != null)
                newEventId = ((EventManager.Model.Event)e.ReturnValue).Id;
            else
                newEventId = 0;
        }

        protected void FormView1_ModeChanged(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            { 
                // Find the "Print Certificate" checkboxes and set defaults
                //CheckBox surveyCheckBox = (CheckBox)FormView1.FindControl("RequireSurveyCheckBox");
                //surveyCheckBox.Checked = true;

                //CheckBox paymentCheckBox = (CheckBox)FormView1.FindControl("RequirePaymentCheckBox");
                //paymentCheckBox.Checked = true;
            }
        }

        protected void FormView1_ItemCreated(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            {
                // Find the "Print Certificate" checkboxes and set defaults
                CheckBox surveyCheckBox = (CheckBox)FormView1.FindControl("SurveyRequiredCheckBox");
                surveyCheckBox.Checked = true;

                CheckBox paymentCheckBox = (CheckBox)FormView1.FindControl("PaymentRequiredCheckBox");
                paymentCheckBox.Checked = true;
            }
        }

        protected void EventDescRadEditor_Init(object sender, EventArgs e)
        {
            RadEditor editor = (RadEditor)sender;

            string[] viewPaths = new string[2];
            viewPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"].ToString();
            viewPaths[1] = "~/Association/Documents/Associations/" + UserInfo.AssociationId.ToString();
            
            string[] uploadPaths = new string[1];
            uploadPaths[0] = "~/Association/Documents/Events/" + Request.QueryString["EventId"].ToString();

            // Make sure both folders exists for the event/association
            if (!Directory.Exists(Server.MapPath(viewPaths[0])))
            {
                // Create the folder
                Directory.CreateDirectory(Server.MapPath(viewPaths[0]));
            }
            if (!Directory.Exists(Server.MapPath(viewPaths[1])))
            {
                // Create the folder
                Directory.CreateDirectory(Server.MapPath(viewPaths[1]));
            }

            editor.ImageManager.ViewPaths = viewPaths;
            editor.ImageManager.UploadPaths = uploadPaths;
        }

        protected void EventDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            EventManager.Model.Event evt = (EventManager.Model.Event)e.InputParameters[0];
            evt.EventStatusId = 2; // "Open"
        }

    }
}