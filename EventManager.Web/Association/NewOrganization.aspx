﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewOrganization.aspx.cs" Inherits="EventManager.Web.Association.NewOrganization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Facility</title>
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/inner.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table {margin:auto; padding:5px;}
        table td {padding:5px;}
       
        html, body, form
        {
            margin: 10px;
            padding: 0;
            height: 100%;
            background: #f2f2de;
        }
        
        body
        {
            font: normal 11px Arial, Verdana, Sans-serif;
        }
        
        fieldset
        {
            height: 150px;
        }
        
        * + html fieldset
        {
            height: 300px;
            width: 400px;
        }
       
    </style>
    
</head>
<body onload="AdjustRadWidow();">

    <form id="form1" runat="server">
    <div>
    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        
    <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" />

    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function openWin2() {
            var parentPage = GetRadWindow().BrowserWindow;
            var parentRadWindowManager = parentPage.GetRadWindowManager();
            var oWnd2 = parentRadWindowManager.open("Dialog2.aspx", "RadWindow2");
            window.setTimeout(function () {
                oWnd2.setActive(true);
            }, 0);
        }

        function populateCityName(arg) {
            var cityName = document.getElementById("cityName");
            cityName.value = arg;
        }

        function AdjustRadWidow() {
            var oWindow = GetRadWindow();
            setTimeout(function () { oWindow.autoSize(true); if ($telerik.isChrome || $telerik.isSafari) ChromeSafariFix(oWindow); }, 500);
        }

        //fix for Chrome/Safari due to absolute positioned popup not counted as part of the content page layout
        function ChromeSafariFix(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            setTimeout(function () {
                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();

            }, 310);
        }

        function returnToParent() {
            //create the argument that will be returned to the parent page
            var oArg = new Object();

            //get the org ID
            oArg.OrgId = document.getElementById("OrgIdLabel").text;

            //get the org name
            var orgNameTextBox = $find("<%= OrgNameRadTextBox.ClientID %>");
            oArg.OrgName = orgNameTextBox.get_value();

            //get the org type 
            var orgTypeRadComboBox = $find("<%= OrgTypeRadComboBox.ClientID %>");
            var orgTypeRadComboItem = orgTypeRadComboBox.get_selectedItem();
            oArg.OrgTypeId = orgTypeRadComboItem.get_value();

            //get the beds
            var bedsRadNumericTextBox = $find("<%= BedsRadNumericTextBox.ClientID %>");
            oArg.BedCount = bedsRadNumericTextBox.get_value();

            //get member/non member
            oArg.IsMember = document.getElementById("IsMemberCheckBox").checked;
           
            //get a reference to the current RadWindow
            var oWnd = GetRadWindow();

            //Close the RadWindow and send the argument to the parent page
            if (oArg.OrgName && oArg.OrgTypeId) {
                oWnd.close(oArg);
            }
            else {
                alert("Please fill in all required fields.");
            }
        }
    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="ValidationGroup1" ForeColor="Red" Display="None" ></asp:CustomValidator>

        <table style="width:350px;">
            <tr>
                <td class="style1">
                    Name:</td>
                <td>
                    <telerik:RadTextBox ID="OrgNameRadTextBox" Runat="server" width="300">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Type:</td>
                <td class="style3">
                    <telerik:radcombobox ID="OrgTypeRadComboBox" runat="server" DataSourceID="OrgTypeDataSource" width="300"
                        DataTextField="OrgTypeName" DataValueField="Id">
                    </telerik:radcombobox></td>
            </tr>
            <tr>
                <td class="style1">
                    Beds:</td>
                <td>
                    <telerik:radnumerictextbox id="BedsRadNumericTextBox" runat="server" width="50" value="0">
                        <numberformat zeropattern="n" decimaldigits="0"></numberformat>
                    </telerik:radnumerictextbox>
                </td>
            </tr>
            <tr>
                <td class="style1"></td>
                <td><asp:CheckBox ID="IsMemberCheckBox" runat="server" Text="Member" />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="OrgIdLabel" runat="server" Text="" Visible="true"></asp:Label>
                    <button title="Submit" id="close" onclick="returnToParent(); return false;">Submit</button>

                    <telerik:radbutton runat="server" ID="OkRadButton" text="Ok" width="50" 
                        onclick="OkRadButton_Click">
                    </telerik:radbutton>&nbsp;
                    <telerik:radbutton runat="server" ID="CancelRadButton" text="Cancel" width="80">
                    </telerik:radbutton>
                </td>
            </tr>
        </table>
        <br /><br />
    </div>
    </form>

    <asp:objectdatasource ID="OrgTypeDataSource" runat="server" 
        SelectMethod="GetOrganizationTypes" 
        TypeName="EventManager.Business.OrganizationMethods" 
        onselecting="OrgTypeDataSource_Selecting">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:objectdatasource>

</body>
</html>
