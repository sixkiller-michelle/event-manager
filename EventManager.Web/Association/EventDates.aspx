﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventDates.aspx.cs" Inherits="EventManager.Web.Association.EventDatesForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<div id="content">
    <h3>&nbsp;</h3>
    <h3>Event Dates</h3>
    <asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource" Width="800px" DataKeyNames="Id" DefaultMode="Edit">
    <EditItemTemplate>
            <table cellpadding="3" class="formview-table-layout">
               
                <tr>
                    <td class="style1" style="width: 161px">&nbsp;</td>
                    <td class="style2">Start:</td>
                    <td>&nbsp;</td>
                    <td>End:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">Event Dates:&nbsp;</td>
                    <td class="style2">
                        <telerik:RadDateTimePicker ID="EventStartDateTimePicker" Runat="server" 
                            DbSelectedDate='<%# Bind("StartDateTime") %>' Culture="en-US" MinDate="">
                            <TimeView CellSpacing="-1" Columns="4" runat="server">
                            </TimeView>
                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x" runat="server">
                            </Calendar>
                            <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" runat="server">
                            </DateInput>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="EventEndDateTimePicker" Runat="server" DbSelectedDate='<%# Bind("EndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">Online Registration Closes:</td>
                    <td class="style2">
                         <telerik:RadDateTimePicker ID="RadDateTimePicker1" Runat="server" 
                             DbSelectedDate='<%# Bind("OnlineRegEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td> &nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">On-Site Registration Opens:</td>
                    <td class="style2">
                         <telerik:RadDateTimePicker ID="RadDateTimePicker2" Runat="server" 
                             DbSelectedDate='<%# Bind("OnSiteRegistrationDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">&nbsp;</td>
                    <td class="style2">Start:</td>
                    <td>&nbsp;</td>
                    <td>End:</td>
                    <td>Room:</td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">Board Meeting:&nbsp;</td>
                    <td class="style2">
                        <telerik:RadDateTimePicker ID="RadDateTimePicker3" Runat="server" 
                            DbSelectedDate='<%# Bind("BoardMeetingStartDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker4" Runat="server" 
                            DbSelectedDate='<%# Bind("BoardMeetingEndDateTime") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' />
                    </td>
                </tr>
                <tr>
                    <td class="style1" style="width: 161px">Certificates Available Online:</td>
                    <td class="style2">
                        <telerik:RadDateTimePicker ID="RadDateTimePicker5" Runat="server" 
                            DbSelectedDate='<%# Bind("CertificatesAvailableStartDate") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePicker6" Runat="server" 
                            DbSelectedDate='<%# Bind("CertificatesAvailableEndDate") %>'>
                        </telerik:RadDateTimePicker>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                <td colspan="5">
                <br />
                    <telerik:RadButton ID="UpdateRadButton" runat="server" Text="Update" CausesValidation="True" CommandName="Update" />
                    <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                </td>
                </tr>
            </table>
    </EditItemTemplate>


    <ItemTemplate>
    <table cellpadding="3" class="formview-table-layout">
        <tr>
            <td class="style1" style="width: 161px">&nbsp;</td>
            <td class="style2">Start:</td>
            <td>&nbsp;</td>
            <td>End:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1" style="width: 161px">Event Dates:&nbsp;</td>
            <td class="style2">
                <asp:TextBox ID="Label1" runat="server" Text='<%# Bind("StartDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="Label2" runat="server" Text='<%# Bind("EndDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1" style="width: 161px">Online Registration Closes:</td>
            <td class="style2">
                <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("OnlineRegEndDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1" style="width: 161px">On-Site Registration Opens:</td>
            <td class="style2">
               <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("OnSiteRegistrationDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
            </td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1" style="width: 161px">&nbsp;</td>
            <td class="style2">Start:</td>
            <td>&nbsp;</td>
            <td>End:</td>
            <td>Room:</td>
        </tr>
        <tr>
            <td class="style1" style="width: 161px">Board Meeting:&nbsp;</td>
                <td class="style2"><asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("BoardMeetingStartDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
                <td><asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("BoardMeetingEndDateTime", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' ReadOnly="true" CssClass="readonly" />
                </td>
            </tr>
            <tr>
                <td class="style1" style="width: 161px">Certificates Available Online:</td>
                <td class="style2"><asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("CertificatesAvailableStartDate", "{0:g}") %>' ReadOnly="True" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
                <td><asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("CertificatesAvailableEndDate", "{0:g}") %>' ReadOnly="true" CssClass="readonly" />
                </td>
                <td>&nbsp;</td>
            </tr>
        <tr>
            <td colspan="5">
            <telerik:RadButton ID="EditButton" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" />
        
            </td>
        </tr>
    </table>
    </ItemTemplate>
    </asp:FormView>
</div>

    <asp:ObjectDataSource ID="EventDataSource" runat="server" SelectMethod="GetEvent" TypeName="EventManager.Business.EventMethods" 
        UpdateMethod="UpdateEventDates" OnUpdating="EventDataSource_Updating" OnUpdated="EventDataSource_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="eventId" Type="Int32" />
            <asp:Parameter Name="startDateTime" Type="DateTime" />
            <asp:Parameter Name="endDateTime" Type="DateTime" />
            <asp:Parameter Name="onlineRegEndDateTime" Type="DateTime" />
            <asp:Parameter Name="onSiteRegistrationDateTime" Type="DateTime" />
            <asp:Parameter Name="boardMeetingStartDateTime" Type="DateTime" />
            <asp:Parameter Name="boardMeetingEndDateTime" Type="DateTime" />
            <asp:Parameter Name="boardMeetingRoom" Type="String" />
            <asp:Parameter Name="certificatesAvailableStartDate" Type="DateTime" />
            <asp:Parameter Name="certificatesAvailableEndDate" Type="DateTime" />
        </UpdateParameters>
    </asp:ObjectDataSource>

</asp:Content>
