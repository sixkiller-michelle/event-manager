﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="NewEvent.aspx.cs" Inherits="EventManager.Web.Association.NewEventForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #content {margin:5px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="content">

    <h2>New Event</h2>
    
    <asp:Label ID="ErrorLabel" runat="server" Visible="false" Style="color:Red;"></asp:Label>

    <table cellpadding="3">
        <tr>
            <td colspan="4"><h3>Event Details</h3></td>
        </tr>
        <tr>
            <td class="style1">Event Type:</td>
            <td colspan="3"><telerik:RadComboBox ID="EventTypesDropDownList" runat="server" DataSourceID="EventTypesDataSource" 
                                DataTextField="TypeName" DataValueField="Id">
                            </telerik:RadComboBox></td>
           
        </tr>
        <tr>
            <td class="style1">Title:</td>
            <td colspan="3"><telerik:radtextbox ID="EventNameTextBox" runat="server" 
                    Height="23px" Width="483px" MaxLength="100"  /></td>
          
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style2">Start:</td>
            <td>End:</td>
        </tr>
        <tr>
            <td class="style1">Event Date:</td>
            <td class="style2">
                <telerik:RadDateTimePicker ID="EventStartDateTimePicker" Runat="server">
                </telerik:RadDateTimePicker>
            </td>
            <td>
                <telerik:RadDateTimePicker ID="EventEndDateTimePicker" Runat="server">
                </telerik:RadDateTimePicker>
            </td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1" colspan="4"><h3>Location</h3></td>
          
        </tr>
        <tr>
            <td class="style1">Venue:</td>
            <td colspan="3">
                <telerik:radtextbox ID="LocationTextBox" runat="server" Width="292px" MaxLength="50" /> &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">City:</td>
            <td class="style2">
                <telerik:RadTextBox ID="CityTextBox" runat="server" MaxLength="40" Width="150" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">State:</td>
            <td class="style2">
               <telerik:RadComboBox ID="StatesDropDownList" runat="server" DataSourceID="StatesDataSource"
                            DataTextField="StateName" DataValueField="StateCode" Width="150" 
                    ondatabound="StatesDropDownList_DataBound">
                        </telerik:RadComboBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">Visibility:</td>
            <td class="style2">
               <telerik:RadComboBox ID="EventVisibilityDropDownList" runat="server" Width="100">
                    <Items>
                    <telerik:RadComboBoxItem Text="Just Me" Value="Creator" />
                    <telerik:RadComboBoxItem Text="Association" Value="Association" />
                    <telerik:RadComboBoxItem Text="Public" Value="Public" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <telerik:RadButton ID="CreateEventRadButton" runat="server" onclick="CreateEventRadButton_Click" Text="Save and Continue...">
                </telerik:RadButton>&nbsp;
                <telerik:RadButton ID="CancelEventRadButton" runat="server" Text="Cancel">
                </telerik:RadButton>
            </td>
           
        </tr>
    </table>

    <asp:ObjectDataSource ID="EventTypesDataSource" runat="server" SelectMethod="GetEventTypes" 
        TypeName="EventManager.Business.EventMethods">
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>

    <br />
    </div>
</asp:Content>
