﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAttendanceDialog.aspx.cs" Inherits="EventManager.Web.Association.EditAttendanceDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Session Attendance</title>
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/inner.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table {margin:auto; padding:5px;}
        table td {padding:5px;}
       
        html, body, form
        {
            margin: 10px;
            padding: 0;
            height: 100%;
            background: #f2f2de;
        }
        
        body
        {
            font: normal 11px Arial, Verdana, Sans-serif;
        }
        
        fieldset
        {
            height: 150px;
        }
        
        * + html fieldset
        {
            height: 300px;
            width: 400px;
        }
       
    </style>

    <script type="text/javascript">
        //<![CDATA[
        function CloseAndRebind(ceuCount) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.RefreshRegistrationsGrid(ceuCount);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }

        function CancelRadButton_Clicked(sender, args) {
            CancelEdit();
        }

         //]]>
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
         <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
         <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" />
        
        <telerik:radbutton runat="server" ID="OkRadButton" text="Ok" width="50" onclick="OkRadButton_Click"></telerik:radbutton>&nbsp;
        <telerik:radbutton runat="server" ID="CancelRadButton" text="Cancel" width="80" onclientclicked="CancelRadButton_Clicked"></telerik:radbutton>
        <br /><br />

         <asp:Label ID="Label1" runat="server" Text="Sessions Attended: " 
             Font-Size="Large"></asp:Label>
         <asp:Label ID="SessionCountLabel" runat="server" Text="0" Font-Size="Large" 
             ForeColor="#0099FF"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

         <asp:Label ID="Label2" runat="server" Text="CEU Total: " 
             Font-Size="Large"></asp:Label>
         <asp:Label ID="CeuTotalLabel" runat="server" Text="0" Font-Size="Large" 
             ForeColor="#0099FF"></asp:Label>
             <br /><br />
        
        
        <telerik:RadGrid ID="SessionsRadGrid" runat="server" AutoGenerateColumns="False" 
             CellSpacing="0" DataSourceID="SessionsDataSource" GridLines="None" 
            AllowMultiRowSelection="true" ondatabound="SessionsRadGrid_DataBound" 
             onitemcreated="SessionsRadGrid_ItemCreated" 
             onprerender="SessionsRadGrid_PreRender">
            <clientsettings>
                <selecting allowrowselect="True" />
            </clientsettings>
            <MasterTableView DataSourceID="SessionsDataSource" DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    
                    <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                        <HeaderTemplate>
                            <asp:CheckBox id="headerChkbox" OnCheckedChanged="ToggleSelectedState" AutoPostBack="True" runat="server"></asp:CheckBox>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox id="CheckBox1" OnCheckedChanged="ToggleRowSelection" AutoPostBack="True" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="SessionName" FilterControlAltText="Filter SessionName column" HeaderText="SessionName" SortExpression="SessionName" UniqueName="SessionName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDateTime" DataFormatString="{0:g}" DataType="System.DateTime" FilterControlAltText="Filter StartDateTime column" HeaderText="Start" SortExpression="StartDateTime" UniqueName="StartDateTime">
                        <HeaderStyle Width="120px" />
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CEUHours" DataType="System.Decimal" FilterControlAltText="Filter CEUHours column" HeaderText="CEUs" SortExpression="CEUHours" UniqueName="CEUHours">
                        <HeaderStyle Width="40px" />
                        <ItemStyle Width="40px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
                <WebServiceSettings>
                    <ODataSettings InitialContainerName="">
                    </ODataSettings>
                </WebServiceSettings>
            </FilterMenu>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                <WebServiceSettings>
                    <ODataSettings InitialContainerName="">
                    </ODataSettings>
                </WebServiceSettings>
            </HeaderContextMenu>
        </telerik:RadGrid>
        <br />

      

        <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods">
            <SelectParameters>
                <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
