﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class ProcessAttendanceForm : System.Web.UI.Page
    {
        int eventId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
                if (Int32.TryParse(Request.QueryString["EventId"], out eventId))
                    SessionPageHyperLink.NavigateUrl += Request.QueryString["EventId"];

        }

        protected void ProcessAttendanceRadButton_Click(object sender, EventArgs e)
        {
            SessionAttendanceMethods m = new SessionAttendanceMethods();
            if (m.ProcessAttendance(eventId))
            {
                SuccessLabel.Visible = true;
                FailureLabel.Visible = false;
            }
            else
            {
                SuccessLabel.Visible = false;
                FailureLabel.Visible = true;
            }
        }
    }
}