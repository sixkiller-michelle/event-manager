﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;

namespace EventManager.Web.Association
{
    public partial class EventSessionDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["SessionId"] == null || Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

        }

        protected void SessionsRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["SessionId"] != null)
                    SessionsRadComboBox.SelectedValue = Request.QueryString["SessionId"].ToString();
                else
                    SessionsRadComboBox.SelectedIndex = 0;
            }
        }

        protected void SpeakersRadGrid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int regId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
                EventManager.Business.SessionAttendanceMethods sam = new EventManager.Business.SessionAttendanceMethods();
                sam.Delete(regId, Convert.ToInt32(SessionsRadComboBox.SelectedValue));
                sam.SaveAllObjectChanges();

                SpeakersRadGrid.DataBind();
            }
        }

        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] GetAttendeeNameCompletionListWithId(int associationId, string prefixText, int count)
        {
            List<string> attendeeList = new List<string>();

            //EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            //List<EventManager.Model.Attendee> attendees = am.GetAttendeesByName(associationId, prefixText);

            //foreach (EventManager.Model.Attendee a in attendees)
            //    attendeeList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(a.FullName, a.Id.ToString()));

            return attendeeList.ToArray();
        }


        protected void NewSpeakerRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            EventManager.Business.SessionAttendanceMethods sam = new EventManager.Business.SessionAttendanceMethods();

            sam.AddSpeaker(Convert.ToInt32(Request.QueryString["EventId"]), Convert.ToInt32(SessionsRadComboBox.SelectedValue), Convert.ToInt32(e.Value));
            sam.SaveAllObjectChanges();

            SpeakersRadGrid.DataBind();
        }

        protected void NewSpeakerRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {

            int assnId = UserInfo.AssociationId;

            //EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            //List<EventManager.Model.Attendee> attendees = am.GetAttendeesByName(assId, NewSpeakerRadComboBox.Text);

            EventManager.Model.Entities ctx = new EventManager.Model.Entities();
            IQueryable<EventManager.Model.Attendee> query =

                from att in ctx.Attendees
                where att.AssociationId == assnId && (att.LastName + ", " + att.FirstName).StartsWith(e.Text)
                orderby att.LastName, att.FirstName
                select att;

            foreach (EventManager.Model.Attendee a in query)
            {
                RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName, a.Id.ToString());
                NewSpeakerRadComboBox.Items.Add(item);
            }
           

        }


    }
}