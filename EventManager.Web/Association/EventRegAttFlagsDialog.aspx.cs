﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Association
{
    public partial class EventRegAttFlagsDialog : System.Web.UI.Page
    {
        int eventRegAttendeeId = 0;
        int eventId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Int32.TryParse(Request.QueryString["id"], out eventRegAttendeeId))
            {
                RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                RegistrationAttendee regAtt = m.GetRegistrationAttendee(eventRegAttendeeId);
                eventId = regAtt.Registration.EventId;
            }
        }

        protected void EventFlagsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (eventId != null && eventId > 0)
            {
                e.InputParameters["eventId"] = eventId;
            }
        }

        protected void AttendeeFlagsCheckBoxList_DataBound(object sender, EventArgs e)
        {
            // Check all of the flags that attendee has
            EventManager.Business.RegistrationAttendeeFlags ram = new EventManager.Business.RegistrationAttendeeFlags();
            foreach (EventManager.Model.RegistrationAttendeeFlag af in ram.GetRegisteredAttendeeFlags(eventRegAttendeeId))
            {
                // find the matching item in check box list and check it
                CheckBoxList flagCheckBoxList = AttendeeFlagsCheckBoxList;
                if (flagCheckBoxList != null)
                {
                    foreach (ListItem checkBox in flagCheckBoxList.Items)
                    {
                        if (Convert.ToInt32(checkBox.Value) == af.EventFlagId)
                            checkBox.Selected = true;
                    }
                }

            }

        }

        protected void OkRadButton_Click(object sender, EventArgs e)
        {
            RegistrationAttendeeFlags m2 = new RegistrationAttendeeFlags();
            CheckBoxList flagCheckBoxList = AttendeeFlagsCheckBoxList;
            m2.DeleteAllFlags(eventRegAttendeeId);
            m2.SaveAllObjectChanges();

            foreach (ListItem checkbox in flagCheckBoxList.Items)
            {
                if (checkbox.Selected)
                {
                    m2.Add(eventRegAttendeeId, Convert.ToInt32(checkbox.Value));
                }
            }
            m2.SaveAllObjectChanges();

            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            RegistrationAttendee regAtt = m.GetRegistrationAttendee(eventRegAttendeeId);
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('" + regAtt.FlagList + "', " + eventRegAttendeeId + ");", true);
        }
    }
}