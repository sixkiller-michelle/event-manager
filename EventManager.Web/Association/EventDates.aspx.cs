﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class EventDatesForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }


            //this.EventStartDateTimePicker.NullText = "";
            //this.EventStartDateTimePicker.NullDate = DateTime.MinValue;
        }

        protected void EventDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["EventId"] = Request.QueryString["EventId"];
        }

        protected void EventDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            em.SaveAllObjectChanges();
            EventDataSource.DataBind();
            FormView1.DataBind();
        }

 
    }
}