﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using EventManager.Business;

namespace EventManager.Web.Association
{
    public partial class AssociationMaster2 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetSelectedMenuItem();
            bool switchFacModuleVisible = false;

            if (!IsPostBack)
            { 
                Guid userId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                if (userId != null)
                {
                    UserAssociationMethods m = new UserAssociationMethods();
                    AssnNameLabel.Text = m.GetAssociationForUser(userId).AssociationName.ToUpper();
                }

                // Check if logged in user is a member of an association
                if (Membership.GetUser() != null)
                {
                    if (Roles.IsUserInRole("FacilityAdmin") || Roles.IsUserInRole("FacilityEmployee"))
                    {
                        switchFacModuleVisible = true;
                    }

                }

                Panel facModulePanel = (Panel)LoginView1.FindControl("FacilityModulePanel");
                facModulePanel.Visible = switchFacModuleVisible;
               
            }
        }

        protected void SetSelectedMenuItem()
        {
            RadMenu mainMenu = this.MainRadMenu;

            if (Request.ServerVariables["Path_Info"].Contains("Association/Dashboard"))
                mainMenu.Items[1].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Event") || Request.ServerVariables["Path_Info"].Contains("/Association/NewEvent") || Request.ServerVariables["Path_Info"].Contains("/Association/EditEvent"))
                mainMenu.Items[2].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Org"))
                mainMenu.Items[3].Selected = true;
            else if (Request.ServerVariables["Path_Info"].Contains("/Association/Attendee"))
                mainMenu.Items[4].Selected = true;
        }

        public Guid UserId
        {
            get
            {
                Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                return g;
            }
        }

        public int AssociationId
        {
            get
            {
                EventManager.Business.AssociationEmployeeMethods ef = new EventManager.Business.AssociationEmployeeMethods();
                return ef.GetEmployeeByUserId(this.UserId).AssociationId;
            }
        }
    }
}