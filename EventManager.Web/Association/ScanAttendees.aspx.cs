﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Association
{
    public partial class ScanAttendees : System.Web.UI.Page
    {
        int eventId = 0;

        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            int assnId = UserInfo.AssociationId;
            try
            {
                AssociationLogoImage.ImageUrl = "~/Association/Documents/Associations/" + assnId.ToString() + "/logo.png";
            }
            finally { }


            if (Request.QueryString["EventId"] != null)
            {
                eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                if (!IsPostBack)
                {
                    LoadSessionList(0);
                }
                else if (Request.Form["__EVENTTARGET"] == "AttendeeIdRadNumericTextBox")
                {
                    AttendeeNameContainer.Visible = true;
                    if (Request.Form["__EVENTARGUMENT"] != "")
                    { 
                        // Assume attendeeId_sessionId
                        var value = Request.Form["__EVENTARGUMENT"];
                        string attendeeId = value.Substring(0, value.IndexOf("_"));
                        int sessionId = Convert.ToInt32(value.Substring(value.ToString().IndexOf("_") + 1));

                        if (sessionId > -1)
                            LogScan(attendeeId, sessionId);

                        if (SessionsRadComboBox.SelectedValue == "-1")
                        { 
                            LoadSessionList(sessionId);
                            SelectSession(sessionId);
                            if (SessionsRadComboBox.SelectedIndex > 0)
                            {
                                AttendeeNameContainer.Visible = true;
                                AttendeeIdRadNumericTextBox.Focus();
                            }
                        }
                            
                    }
                        
                }
                else if (Request.Form["__EVENTTARGET"] == "SessionsRadComboBox")
                {
                    int sessionId = Convert.ToInt32(SessionsRadComboBox.SelectedValue);
                    SelectSession(sessionId);
                    if (SessionsRadComboBox.SelectedIndex > 0)
                    { 
                        AttendeeNameContainer.Visible = true;
                        AttendeeIdRadNumericTextBox.Focus();
                    }
                }
                else if (SessionsRadComboBox.SelectedIndex > 0)
                {
                    AttendeeNameContainer.Visible = true;
                    AttendeeIdRadNumericTextBox.Focus();
                }
            } 
        }


        private void LoadSessionList(int selectedSessionId)
        {
            AttendeeNameContainer.Visible = false;
            var sessionMethods = new EventManager.Business.EventSessionMethods();
            SessionsRadComboBox.DataSource = sessionMethods.GetSessionsByEvent(eventId);
            SessionsRadComboBox.DataBind();

            if (selectedSessionId == 0)
                SessionsRadComboBox.SelectedIndex = 0;
            else
                SessionsRadComboBox.SelectedValue = selectedSessionId.ToString();
        }

        private void LogScan(string attendeeId, int sessionId)
        {
            int attId = 0;

            if (Int32.TryParse(attendeeId, out attId))
            {

                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = m.GetAttendee(Convert.ToInt32(attId));
                if (a != null)
                {
                    AttendeeNameLabel.Text = a.FullNameFNF;

                    // Log a scan for this session
                    EventManager.Business.ScannerDataMethods m2 = new EventManager.Business.ScannerDataMethods();
                    EventManager.Model.ScannerData scan = new EventManager.Model.ScannerData();
                    scan.EventId = eventId;
                    scan.SessionId = sessionId;
                    scan.AttendeeId = a.Id;
                    scan.ScanDate = DateTime.Now.Date.ToShortDateString();
                    scan.ScanTime = DateTime.Now.ToString("H:mm:ss");

                    m2.Add(scan);
                    m2.SaveAllObjectChanges();

                    AttendeeIdRadNumericTextBox.Text = "";
                    AttendeeIdRadNumericTextBox.Focus();
                }
                else
                {
                    AttendeeIdRadNumericTextBox.Text = "";
                    AttendeeNameLabel.Text = "";
                    AttendeeIdRadNumericTextBox.Focus();
                }
            }
            else
            {
                // Send email to michelle
                Email mail = new Email();
                mail.SendMail("michelle@sixkillersoftware.com", "admin@ceuavenue.com", "Invalid scan value: " + attendeeId, "");
                AttendeeIdRadNumericTextBox.Text = "";
                AttendeeNameLabel.Text = "";
                AttendeeIdRadNumericTextBox.Focus();
            }
        }
            

        private void SelectSession(int sessionId)
        {
            var sessionMethods = new EventManager.Business.EventSessionMethods();
            var session = sessionMethods.GetSessionsByEvent(eventId).Where(x => x.Id == sessionId);
           
            SessionFormView.DataSource = session;
            SessionFormView.DataBind();
            ResetEntry();
        }

        private void ResetEntry()
        {
            AttendeeIdRadNumericTextBox.Text = "";
            AttendeeNameLabel.Text = "";
        }
    }
}