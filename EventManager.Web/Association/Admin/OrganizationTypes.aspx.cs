﻿using EventManager.Business;
using EventManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Settings
{
    public partial class OrganizationTypesForm : System.Web.UI.Page
    {
        private int employeeId;

        private int m_AssocId
        {
            get
            {
                int empId = 0;
                if (Int32.TryParse(Request.QueryString["id"], out empId))
                {
                    return empId;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = m_AssocId;

        }

        protected void RadGrid1_ItemUpdated(object sender, Telerik.Web.UI.GridUpdatedEventArgs e)
        {
            int RateId = Convert.ToInt32((e.Item as GridEditFormItem).GetDataKeyValue("Id"));


            if (e.Exception == null)
            {
                OrganizationFeeCategoryMethods cm = new OrganizationFeeCategoryMethods();
                cm.SaveAllObjectChanges();
                RadGrid1.Rebind();
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;

                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
                CustomValidator1.IsValid = false;
            }
        }

        protected void RadGrid1_ItemInserted(object sender, Telerik.Web.UI.GridInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                OrganizationFeeCategoryMethods cm = new OrganizationFeeCategoryMethods();
                cm.SaveAllObjectChanges();
                RadGrid1.DataBind();
            }
            else
            {
                String str = e.Exception.Message;
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                {
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                }

                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
                CustomValidator1.IsValid = false;
            }
        }

        protected void RadGrid1_ItemDeleted(object sender, Telerik.Web.UI.GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                int categoryId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));
                
                OrganizationFeeCategoryMethods om = new OrganizationFeeCategoryMethods();
                OrganizationFeeCategory old = om.GetFeeCategory(categoryId);

                //om.Delete(old);
                om.SaveAllObjectChanges();

                RadGrid1.Rebind();
            }
            else
            {
                String str = e.Exception.Message;
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;

                e.ExceptionHandled = true;
                CustomValidator1.IsValid = false;
            }
        }

        protected void orgFeeCategoryDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            ((OrganizationFeeCategory)e.InputParameters[0]).OrganizationTypeId = Int32.Parse( orgTypeComboBox.SelectedValue );
        }

        protected void orgFeeCategoryDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters[0] = Int32.Parse(orgTypeComboBox.SelectedValue);
        }

        protected void orgTypeDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters[0] = UserInfo.AssociationId;
        }

        protected void orgTypeComboBox_DataBound(object sender, EventArgs e)
        {
            if (orgTypeComboBox.Items.Count > 0)
                orgTypeComboBox.SelectedIndex = 0;
        }

        protected void orgTypeComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            NewOrg_FormView.PageIndex = orgTypeComboBox.SelectedIndex;
            NewOrg_FormView.DataBind();

            RadGrid1.Rebind();
        }

        protected void NewOrg_FormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                OrganizationTypeMethods otm = new OrganizationTypeMethods();
                otm.SaveAllObjectChanges();

                // Reload the combo box with new value, and select it
                orgTypeComboBox.DataBind();
                string cat = e.Values[0].ToString();
                orgTypeComboBox.SelectedIndex = orgTypeComboBox.FindItemByText(cat).Index;

                // Rebind the form view 
                NewOrg_FormView.DataBind();
                NewOrg_FormView.PageIndex = orgTypeComboBox.SelectedIndex;
              
                // Rebind the grid of fee categories
                RadGrid1.Rebind();

            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;

                CustomValidator1.IsValid = false;
                e.KeepInInsertMode = true;
            }
        }

        protected void NewOrg_FormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                OrganizationTypeMethods otm = new OrganizationTypeMethods();

                otm.SaveAllObjectChanges();
                NewOrg_FormView.DataBind();
                NewOrg_FormView.ChangeMode(FormViewMode.Edit);
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
            }
        }

        protected void NewOrg_FormView_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                OrganizationTypeMethods otm = new OrganizationTypeMethods();

                try
                {
                    otm.SaveAllObjectChanges();
                    orgTypeComboBox.DataBind();
                 
                    // Rebind the form view 
                    NewOrg_FormView.DataBind();
                    NewOrg_FormView.PageIndex = orgTypeComboBox.SelectedIndex;

                    // Rebind the grid of fee categories
                    RadGrid1.Rebind();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    else
                        CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void orgTypeDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            ((OrganizationType)e.InputParameters[0]).AssociationId = UserInfo.AssociationId;
        }

        protected void NewOrg_FormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {

        }

    }
}