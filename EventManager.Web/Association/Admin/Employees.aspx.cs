﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;
using System.Web.Security;
using System.IO;

namespace EventManager.Web
{
    public partial class EmployeesForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.IsInRole("AssociationAdmin"))
            {
                EnableEdit();
            }
        }

        protected void EnableEdit()
        {
            RadGrid1.Enabled = true;
        }

        
        protected void InactiveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            RadGrid1.Rebind();
        }

        protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            AssociationEmployeeMethods m = new EventManager.Business.AssociationEmployeeMethods();
            List<AssociationEmployee> emps = m.GetEmployees(UserInfo.AssociationId);
            if (InactiveCheckBox.Checked)
                RadGrid1.DataSource = m.GetEmployees(UserInfo.AssociationId);
            else
                RadGrid1.DataSource = m.GetEmployees(UserInfo.AssociationId).Where(emp => emp.IsActive == true).ToList();
        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                RadGrid1.MasterTableView.ClearEditItems();

                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert
                newValues["IsActive"] = true;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);

                // Hide the roles checkboxes
                GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
                Panel rolesPanel = (Panel)insertedItem.FindControl("RolesPanel");
                rolesPanel.Visible = false;

                // Set focus to first name textbox
                RadTextBox fistNameTextBox = (RadTextBox)insertedItem.FindControl("FirstNameTextBox");
                fistNameTextBox.Focus();

            }
            else if (e.CommandName == RadGrid.EditCommandName)
            {
                e.Item.OwnerTableView.IsItemInserted = false;
            }
            else if (e.CommandName == "Invite")
            {
                Email invite = new Email();

                int id = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));
                string email;
                string username;
                string password;

                AssociationEmployeeMethods m = new AssociationEmployeeMethods();
                AssociationEmployee employee = m.GetEmployee(id);
               
                username = employee.Email;
                password = createPassword(6, 0, 0);
                email = username; // Set email equal to username
                if (Request.ServerVariables["HTTP_HOST"].Contains("localhost"))
                    email = "michelle@sixkillersoftware.com";

                //Create User
                try
                {
                    MembershipUser newUser = Membership.CreateUser(username, password, email);
                    Roles.AddUserToRole(newUser.UserName, "AssociationEmployee");

                    // Update employee info
                    employee.UserId = new Guid(newUser.ProviderUserKey.ToString());
                    employee.IsActive = true;

                    //Add user to UserAssociation
                    UserAssociationMethods assocMethods = new UserAssociationMethods();
                    UserAssociation userAssoc = new UserAssociation();
                    userAssoc.UserId = new Guid(newUser.ProviderUserKey.ToString());
                    userAssoc.AssociationId = employee.AssociationId;
                    assocMethods.Add(userAssoc);

                    m.SaveAllObjectChanges();

                    // Send email to employee informing them of their new account on ceuavenue.com
                    string httpHost = Request.ServerVariables["HTTP_HOST"];
                    string fileText = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/AssociationEmployeeInvite.htm"));
                    fileText = fileText.Replace("<%Username%>", username);
                    fileText = fileText.Replace("<%Password%>", password);
                    fileText = fileText.Replace("<%CreatedDateTime%>", DateTime.Now.ToString());
                    fileText = fileText.Replace("<%CreatedByUsername%>", UserInfo.UserName);

                    invite.SendMail(email, "noReplys@ceuavenue.com", "CEU Avenue Account", fileText);
                    RadGrid1.Rebind();
                }
                catch (Exception ex)
                {
                    if (ex is MembershipCreateUserException)
                    {
                        CustomValidator1.ErrorMessage = (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                    }
                    else
                    {
                        CustomValidator1.ErrorMessage = "Unable to invite user " + username;
                        ExceptionUtility.NotifySystemOps(ex, "Employees.aspx", "Error when inviting employee");
                    }
                    CustomValidator1.IsValid = false;
                }

                
            }

        }

        public String createPassword(int numCharacters, int minCharacters, int maxAlphaNumeric)
        {

            Random random = new Random();
            int rand;
            String genString = "";
            String[] values = new String[2];
            values[0] = "123456789";
            values[1] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

            int i = numCharacters;

            while (i > minCharacters)
            {
                rand = random.Next(0, 2);
                genString += values[rand][random.Next(0, values[rand].Length)];
                i--;
            }

            return genString;
        }

        protected void RadGrid1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void RadGrid1_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridEditFormInsertItem insertedItem = (GridEditFormInsertItem)e.Item;

            try
            {
                AssociationEmployeeMethods m = new AssociationEmployeeMethods();
                AssociationEmployee emp = new AssociationEmployee();

                insertedItem.UpdateValues(emp);
                emp.AssociationId = UserInfo.AssociationId;
                emp.IsActive = true;
                m.Add(emp);
                m.SaveAllObjectChanges();
            }
            catch (ArgumentException ex)
            {
                CustomValidator v = (CustomValidator)insertedItem.FindControl("CustomValidator1");
                v.ErrorMessage = ex.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
            catch (Exception ex)
            {
                CustomValidator v = (CustomValidator)insertedItem.FindControl("CustomValidator1");
                v.ErrorMessage = ex.InnerException.Message;
                v.IsValid = false;
                e.Canceled = true;
            }
        }

        protected void RadGrid1_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int employeeId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            AssociationEmployeeMethods m = new AssociationEmployeeMethods();
            AssociationEmployee employee = m.GetEmployee(employeeId);

            // Add the roles that were selected
            if (employee != null)
            {
                CheckBox adminCheckBox = (CheckBox)item.FindControl("IsAdminCheckBox");
                CheckBox employeeCheckBox = (CheckBox)item.FindControl("IsEmployeeCheckBox");

                // Add checked roles
                if (adminCheckBox.Checked && employee.aspnet_Users != null && !Roles.IsUserInRole(employee.aspnet_Users.UserName, "AssociationAdmin"))
                    Roles.AddUserToRole(employee.aspnet_Users.UserName, "AssociationAdmin");
                if (employeeCheckBox.Checked && employee.aspnet_Users != null && !Roles.IsUserInRole(employee.aspnet_Users.UserName, "AssociationEmployee"))
                    Roles.AddUserToRole(employee.aspnet_Users.UserName, "AssociationEmployee");

                // Remove unchecked roles
                if (!adminCheckBox.Checked && employee.aspnet_Users != null && Roles.IsUserInRole(employee.aspnet_Users.UserName, "AssociationAdmin"))
                    Roles.RemoveUserFromRole(employee.aspnet_Users.UserName, "AssociationAdmin");
                if (!employeeCheckBox.Checked && employee.aspnet_Users != null && Roles.IsUserInRole(employee.aspnet_Users.UserName, "AssociationEmployee"))
                    Roles.RemoveUserFromRole(employee.aspnet_Users.UserName, "AssociationEmployee");
            }
            item.UpdateValues(employee);
            m.SaveAllObjectChanges();
            RadGrid1.Rebind();

            // Hide all edit items
            RadGrid1.MasterTableView.ClearEditItems();

        }


        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                GridDataItem item = (GridDataItem)e.Item;

                if (item.DataItem != null)
                {
                    //HyperLink subjectHyperlink = (HyperLink)item.FindControl("SubjectHyperLink");
                    //subjectHyperlink.Attributes["onclick"] = String.Format("return ShowNotesDialog('{0}', '{1}');", ((Task)item.DataItem).Id.ToString(), item.ItemIndex);

                    //HyperLink durationHyperlink = (HyperLink)item.FindControl("DurationHyperLink");
                    //durationHyperlink.Attributes["onclick"] = String.Format("return ShowActivityDialog('{0}', '{1}');", ((Task)item.DataItem).Id.ToString(), item.ItemIndex);
                }
            }
        }

        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            GridEditCommandColumn editCol = (GridEditCommandColumn)RadGrid1.Columns.FindByUniqueName("EditCommandColumn");
            //GridButtonColumn deleteCol = (GridButtonColumn)RadGrid1.Columns.FindByUniqueName("DeleteCommandColumn");

            // Disable edit if user is not admin 
            if (!(User.IsInRole("AssociationAdmin")))
            {
                editCol.Visible = false;
                //deleteCol.Visible = false;
            }
            else
            {
                editCol.Visible = true;
                //deleteCol.Visible = true;
            }
        }

     
        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode && !(e.Item is GridEditFormInsertItem))
            {
                if (e.Item.DataItem != null)
                {
                    // Get the ID of the employee
                    bool isAdmin = false;
                    bool isEmployee = false;

                    CheckBox adminCheckBox = e.Item.FindControl("IsAdminCheckBox") as CheckBox;
                    CheckBox empCheckBox = e.Item.FindControl("IsEmployeeCheckBox") as CheckBox;

                    Panel rolesPanel = e.Item.FindControl("RolesPanel") as Panel;

                    int employeeId = ((AssociationEmployee)e.Item.DataItem).Id;

                    if (((AssociationEmployee)e.Item.DataItem).aspnet_Users != null)
                    {
                        string userName = ((AssociationEmployee)e.Item.DataItem).aspnet_Users.UserName;


                        if (Roles.IsUserInRole(userName, "AssociationAdmin"))
                            isAdmin = true;
                        if (Roles.IsUserInRole(userName, "AssociationEmployee"))
                            isEmployee = true;
                    }
                    rolesPanel.Visible = true;


                    adminCheckBox.Checked = isAdmin;
                    empCheckBox.Checked = isEmployee;
                }

            }
            //Is it a GridDataItem
            else if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;

                if (e.Item.DataItem != null)
                {
                    AssociationEmployee emp = (AssociationEmployee)e.Item.DataItem;
                    //Check the formatting condition

                    if (((AssociationEmployee)e.Item.DataItem).IsActive == false)
                    {
                        dataBoundItem["FullName"].ForeColor = System.Drawing.Color.Red;
                        dataBoundItem["Id"].ForeColor = System.Drawing.Color.Red;
                    }

                    Label label = (Label)e.Item.FindControl("EmailLabel");
                    RadButton button = (RadButton)e.Item.FindControl("InviteButton");

                    if (emp.UserId != null)
                    {
                        label.Visible = true;
                        button.Visible = false;
                    }
                    else
                    {
                        label.Visible = false;
                        button.Visible = true;
                    }

                }

            }

        }
    }
}