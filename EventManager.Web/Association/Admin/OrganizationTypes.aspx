﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="OrganizationTypes.aspx.cs" Inherits="EventManager.Web.Settings.OrganizationTypesForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:Panel runat="server" ID="MainPanel" Style="padding:10px;">
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ForeColor="Red" Display="Dynamic"></asp:CustomValidator>

    <telerik:RadComboBox 
        ID="orgTypeComboBox" 
        Runat="server" 
        DataSourceID="orgTypeDataSource" 
        DataTextField="OrgTypeName" 
        DataValueField="Id"
        AutoPostBack="True" OnDataBound="orgTypeComboBox_DataBound" OnSelectedIndexChanged="orgTypeComboBox_SelectedIndexChanged" 
        >

    </telerik:RadComboBox>

    <asp:FormView ID="NewOrg_FormView" runat="server" DataKeyNames="Id"  
        DataSourceID="orgTypeDataSource" Height="41px" 
        OnItemDeleted="NewOrg_FormView_ItemDeleted" 
        OnItemInserted="NewOrg_FormView_ItemInserted" 
        OnItemUpdated="NewOrg_FormView_ItemUpdated" 
        OnItemUpdating="NewOrg_FormView_ItemUpdating">
        <EditItemTemplate>
            <br />
            <telerik:RadTextBox ID="OrgTypeNameTextBox" runat="server" Text='<%# Bind("OrgTypeName") %>' Font-Size="Large" Width="300" MaxLength="50" /><br /><br />
            <telerik:RadButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
            &nbsp;<telerik:RadButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            <br />
            New facility type:
            <telerik:RadTextBox ID="OrgTypeNameTextBox" runat="server" Text='<%# Bind("OrgTypeName") %>' Width="300" MaxLength="50" /><br /><br />
           
            <telerik:RadButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />&nbsp;
            <telerik:RadButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
          
            <br />
            <asp:Label ID="OrgTypeNameLabel" runat="server" Text='<%# Eval("OrgTypeName") %>' Font-Size="Large" /><br /><br />
          
            <telerik:RadButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            &nbsp;<telerik:RadButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" />
            &nbsp;<telerik:RadButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>

</asp:Panel>

    <telerik:RadGrid 
        ID="RadGrid1" 
        runat="server" 
        CellSpacing="0" 
        DataSourceID="orgFeeCategoryDataSource" 
        GridLines="None" 
        OnItemDeleted="RadGrid1_ItemDeleted" 
        OnItemInserted="RadGrid1_ItemInserted" 
        OnItemUpdated="RadGrid1_ItemUpdated"
        AutoGenerateColumns="False"
        AllowAutomaticDeletes="True" 
        AllowAutomaticInserts="True" 
        AllowAutomaticUpdates="True"
        >

<MasterTableView 
    DataSourceID="orgFeeCategoryDataSource" 
    CommandItemDisplay="Top"
    DataKeyNames="Id"
    >
<CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="New fee category..."></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="CategoryDesc" FilterControlAltText="Filter CategoryDesc column" HeaderText="Category Description" SortExpression="CategoryDesc" UniqueName="CategoryDesc">
        </telerik:GridBoundColumn>

        <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ItemStyle-Width="30" HeaderStyle-Width="30"
            ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
            UniqueName="DeleteCommandColumn" ConfirmTextFields="CategoryDesc" ConfirmTextFormatString="Are you sure you want to delete the fee category '{0}'?">
            <HeaderStyle Width="30px"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
        </telerik:GridButtonColumn>


        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
            <ItemStyle CssClass="MyImageButton"></ItemStyle>
        </telerik:GridEditCommandColumn>
    </Columns>

<EditFormSettings EditFormType="Template">
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
                <FormTemplate>
                
                    <div style="clear: both; width: 100%; height: 100%; padding:5px;">
                        <table class="formview-table-layout">
                            <tr>
                                <td colspan="2">
                                    <asp:ValidationSummary ID="FormTemplateValidationSummary" runat="server" ValidationGroup="FormTemplateValidationGroup"
                                        ForeColor="Red" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ValidationGroup="FormTemplateValidationGroup"
                                        Display="None"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Description:</td>
                                <td>
                                    <h3 style="margin-top: 0px;">
                                        <telerik:RadTextBox ID="CategoryDescTextBox" runat="server" Text='<%# Bind("CategoryDesc") %>' MaxLength="50" Width="300" /></h3>
                                </td>
                               
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 3px;">
                                    <telerik:RadButton ID="btnUpdate" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                    &nbsp;
                                    <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancel" />
                                </td>
                            </tr>
                        </table>

                    </div>
                </FormTemplate>
            </EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>



    <asp:ObjectDataSource 
        ID="orgFeeCategoryDataSource" 
        runat="server" 
        DataObjectTypeName="EventManager.Model.OrganizationFeeCategory" 
        DeleteMethod="Delete" 
        InsertMethod="Add"
        SelectMethod="GetFeeCategoriesByOrgType" 
        TypeName="EventManager.Business.OrganizationFeeCategoryMethods" 
        UpdateMethod="Update" 
        OnInserting="orgFeeCategoryDataSource_Inserting" 
        OnSelecting="orgFeeCategoryDataSource_Selecting" 
        OldValuesParameterFormatString="original" 
        ConflictDetection="CompareAllValues"
        >

        <SelectParameters>
            <asp:Parameter Name="orgTypeId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="original" Type="Object" />
            <asp:Parameter Name="modified" Type="Object" />
        </UpdateParameters>
    </asp:ObjectDataSource>


    <asp:ObjectDataSource 
        ID="orgTypeDataSource" 
        runat="server" 
        DataObjectTypeName="EventManager.Model.OrganizationType" 
        DeleteMethod="Delete" 
        InsertMethod="Add" 
        SelectMethod="getOrganizationTypeList" 
        TypeName="EventManager.Business.OrganizationTypeMethods" 
        UpdateMethod="Update" 
        OnSelecting="orgTypeDataSource_Selecting" OldValuesParameterFormatString="original_{0}" OnInserting="orgTypeDataSource_Inserting"
        >

        <SelectParameters>
            <asp:Parameter Name="assnId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="original" Type="Object" />
            <asp:Parameter Name="modified" Type="Object" />
        </UpdateParameters>
    </asp:ObjectDataSource>





</asp:Content>
