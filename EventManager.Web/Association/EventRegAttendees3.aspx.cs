﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Model;
using System.Web.Profile;
using EventManager.Business;
using System.IO;

namespace EventManager.Web.Association
{
    public partial class EventRegAttendees3 : System.Web.UI.Page
    {
        bool isPdfExport = false;
        bool isEdit = false;
        public string RegRadGridPanelClientID;
        string certificateReportName;
        int eventId = 0;
        int assnId = 0;

        protected override void OnInit(EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.MinValue);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Association/EventList.aspx");
            }
            else if (Int32.TryParse(Request.QueryString["EventId"], out eventId))
            {
                // Set the click event for "Print Certificate" dialog
                EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
                if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForEvent(Convert.ToInt32(eventId)))))
                    certificateReportName = m.GetCertificateFileNameForEvent(Convert.ToInt32(eventId));
                else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForAssociation(UserInfo.AssociationId))))
                    certificateReportName = m.GetCertificateFileNameForAssociation(UserInfo.AssociationId);
                else
                    certificateReportName = m.GetCertificateFileNameForApplication();
            }
            else
            {
                Response.Redirect("~/Association/EventList.aspx");
            }

            if (!IsPostBack)
            {

                NewRegRadPane.Collapsed = true;
                assnId = UserInfo.AssociationId;

                // Load the attendee combo
                GetAttendeesList(assnId, false);

                // Load the facility combo
                GetFacilitiesList(assnId, false);

                LinkButton newAttendeeButton = NewAttendeeLinkButton;
                if (newAttendeeButton != null)
                    newAttendeeButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}');", "0", "-1");

                LinkButton newOrgButton = NewOrgLinkButton;
                if (newOrgButton != null)
                    newOrgButton.Attributes["onclick"] = String.Format("return ShowOrgDialog('{0}');", "0");

                // Set the Registrations grid pager size based on user's stored preferences
                int pageSize = AccountProfile.CurrentUser.RegAttendeesGridPageSize;
                if (pageSize > 0)
                    RegistrationsRadGrid.PageSize = pageSize;
                else
                    RegistrationsRadGrid.PageSize = 50;
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreated(object sender, AjaxSettingCreatedEventArgs e)
        {
            if (e.Updated.ID == "RegistrationsRadGrid")
            {
                this.RegRadGridPanelClientID = e.UpdatePanel.ClientID;
            }
        }

        private void FillAttendeeCombo()
        {
            // Load the attendee combo
            AttendeeMethods attMethods = new AttendeeMethods();
            NewAttendeeRadComboBox.DataSource = attMethods.GetAttendees(UserInfo.AssociationId);
            NewAttendeeRadComboBox.DataBind();
        }

        private void FillFacilityCombo()
        {
            // Load the facility combo
            OrganizationMethods orgMethods = new OrganizationMethods();
            NewRegFacilityRadComboBox.DataSource = orgMethods.GetOrganizationsByAssociation(UserInfo.AssociationId);
            NewRegFacilityRadComboBox.DataBind();
        }

        protected void PrintButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            string eventName = "2012 Spring Workshop";
            string sortOrder = "";

            RadioButtonList sortOrderList = (RadioButtonList)((LinkButton)sender).NamingContainer.FindControl("SortOrderRadioButtonList");
            sortOrder = sortOrderList.SelectedValue;

            Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=AttendeeList.rpt&ParmNames=EventName|SortField|@EventId&ParmValues=" + eventName + "|" + sortOrder + "|" + eventId);
        }

        private void PrintBadgeSheetForSelectedAttendees()
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string assnName = evt.Association.AssociationName;
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet ds = new EventManager.Web.Association.Reports2.AttendeeBadgesDataSet();
                EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable dt = (EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable)ds.Tables[0];

                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                    EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                    EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r = dt.NewAttendeeRow();
                    r.AssnName = assnName;
                    r.AttendeeFirstName = ra.Attendee.FirstName;
                    r.AttendeeId = ra.Attendee.Id;
                    r.AttendeeNameFNF = ra.Attendee.FullNameFNF;
                    r.EventEndDate = endDate;
                    r.EventName = eventName;
                    r.EventStartDate = startDate;
                    r.FlagString = ra.FlagListForBadge;
                    if (ra.Registration.Organization != null && ra.Registration.Organization.OrgName != null)
                        r.OrgName = ra.Registration.Organization.OrgName;
                    else
                        r.OrgName = "";
                    r.Title = ra.Title;

                    dt.AddAttendeeRow(r);
                }

                // Get which report to use (event-based or generic badge)
                string typeName = "EventManager.Web.Association.Reporting.AttendeeBadges";
                if (File.Exists(Server.MapPath("~/Association/Reporting/AttendeeBadges" + eventId.ToString() + ".resx")))
                    typeName = "EventManager.Web.Association.Reporting.AttendeeBadges" + eventId.ToString();

                // Create the report using reflection, set data source, and store in session
                Type t = Type.GetType(typeName);
                Telerik.Reporting.Report report = (Telerik.Reporting.Report)Activator.CreateInstance(t);
                report.DataSource = ds;
                Session["Report"] = report;

                // Show report form
                Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "BadgeReportDialog", "ShowBadgeReport();", true);


            }
        }

        private void PrintRegistrationsForSelectedAttendees()
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string eventName = evt.EventName;
                string location = evt.Location;
                string cityState = evt.City.Trim() + ", " + evt.StateCode.Trim();
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;
                string paramNames = "";
                string paramValues = "";

                EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet ds = new EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet();
                EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet.RegisteredAttendeesDataTable dt = (EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet.RegisteredAttendeesDataTable)ds.Tables[0];

                foreach (GridDataItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                    //EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                    //EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                    EventManager.Web.Association.Reports2.AttendeeRegistrationsDataSet.RegisteredAttendeesRow r = dt.NewRegisteredAttendeesRow();
                    r.AttendeeName = ((LinkButton)gi["AttendeeNameTemplateColumn"].Controls[1]).Text;
                    r.Ceus = ((Label)gi["CeusTemplateColumn"].Controls[1]).Text;
                    r.CreatedDate = gi["EntryDateTime"].Text;
                    r.Flags = ((Label)gi["FlagListTemplateColumn"].Controls[1]).Text;
                    r.IsSpeaker = ((Label)gi["IsSpeakerTemplateColumn"].Controls[1]).Text;
                    r.Notes = gi["Notes"].Text.Replace("&nbsp;", "");
                    r.OrgName = ((System.Web.UI.WebControls.HyperLink)gi["OrgNameHyperlink"].Controls[0]).Text;
                    r.Title = gi["TitleGridBoundColumn"].Text.Trim().Replace("&nbsp;", "");

                    dt.AddRegisteredAttendeesRow(r);
                }

                // Create the report object
                EventManager.Web.Association.Reporting.AttendeeRegistrations report = new EventManager.Web.Association.Reporting.AttendeeRegistrations();
                report.ReportParameters["EventName"].Value = eventName;
                report.ReportParameters["Location"].Value = location;
                report.ReportParameters["CityState"].Value = cityState;
                report.DataSource = ds;
                Session["Report"] = report;

                //paramNames = "EventName|Location|CityState";
                //paramValues = eventName + "|" + location + "|" + cityState;
                //Session["AttendeeRegistrationsDs"] = ds;
                Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");
            }
        }

        private void PrintContactSheetForSelectedAttendees()
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string eventName = evt.EventName;
                string location = evt.Location;
                string city = evt.City.Trim();
                string stateCode = evt.StateCode;
                string cityState = evt.City.Trim() + ", " + evt.StateCode.Trim();
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                EventManager.Web.Association.Reporting.ContactSheetDataSet ds = new EventManager.Web.Association.Reporting.ContactSheetDataSet();
                EventManager.Web.Association.Reporting.ContactSheetDataSet.RptEventContactSheetDataTable dt = (EventManager.Web.Association.Reporting.ContactSheetDataSet.RptEventContactSheetDataTable)ds.Tables[0];

                foreach (GridDataItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                    EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                    EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                    EventManager.Web.Association.Reporting.ContactSheetDataSet.RptEventContactSheetRow r = dt.NewRptEventContactSheetRow();
                    r.EventName = eventName;
                    r.StartDateTime = startDate;
                    r.EndDateTime = endDate;
                    r.Location = location;
                    r.City = city;
                    r.StateCode = stateCode;
                    r.AttendeeName = ((LinkButton)gi["AttendeeNameTemplateColumn"].Controls[1]).Text;
                    r.AttendeeNameFNF = ra.Attendee.FullNameFNF;
                    r.OrgName = ((System.Web.UI.WebControls.HyperLink)gi["OrgNameHyperlink"].Controls[0]).Text;
                    r.Title = gi["TitleGridBoundColumn"].Text.Trim().Replace("&nbsp;", "");
                    r.FullAddress = ra.Attendee.FullAddress;
                    r.PhoneNumber = ra.Attendee.PhoneNumber;
                    r.Email = ra.Attendee.Email;
                    r.BoothNumber = ra.Registration.BoothNumber;
                    r.DoorPrize = ra.Registration.DoorPrize;

                    dt.AddRptEventContactSheetRow(r);
                }

                // Create the report object
                EventManager.Web.Association.Reporting.EventContactSheet report = new EventManager.Web.Association.Reporting.EventContactSheet();
                report.DataSource = ds;
                Session["Report"] = report;
                Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");
            }
        }

        protected void PrintBadgesButton_Click(object sender, EventArgs e)
        {
            PrintBadgeSheetForSelectedAttendees();
        }

        protected void PrintSingleBadgesButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);

            if (evt != null)
            {
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                Dymo.DymoAddIn DymoAddIn = new Dymo.DymoAddIn();
                Dymo.DymoLabels DymoLabels = new Dymo.DymoLabels();
                string FilePath = Server.MapPath("~/Association/Documents/Attendee Single Badge.label");
                string PrtNames = "";
                string PrinterName = "";

                // Find attached DYMO printer
                PrtNames = DymoAddIn.GetDymoPrinters();

                if (PrtNames != null)
                {
                    PrinterName = PrtNames;
                    DymoAddIn.SelectPrinter(PrinterName);
                }

                if (DymoAddIn.Open(FilePath) && PrinterName != null)
                {
                    foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                    {
                        int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                        EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                        EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                        DymoLabels.SetField("EventStartDate", startDate.ToShortDateString());
                        DymoLabels.SetField("EventName", eventName);
                        DymoLabels.SetField("AttendeeId", ra.AttendeeId.ToString());
                        DymoLabels.SetField("AttendeeFirstName", ra.Attendee.FirstName);
                        DymoLabels.SetField("FullNameAndTitle", ra.Attendee.FullNameFNF.Trim() + ", " + ra.Title.Trim());
                        DymoLabels.SetField("OrgName", ra.Registration.Organization.OrgName);
                        DymoLabels.SetField("FlagString", ra.FlagListForBadge);
                        DymoLabels.SetField("AssociationName", "IHCA-ICAL");

                        DymoAddIn.Print2(1, false, 1);
                    }
                }
            }
        }

        protected void PrintMenuDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = m.GetEvent(eventId);
            DropDownList printMenuDropDown = (DropDownList)sender;

            if (evt != null && printMenuDropDown.SelectedValue != "")
            {
                string assnName = evt.Association.AssociationName;
                string eventName = evt.EventName;
                DateTime startDate = evt.StartDateTime;
                DateTime endDate = evt.EndDateTime;

                if (printMenuDropDown.SelectedValue == "PrintBadgeSheet")
                {
                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet ds = new EventManager.Web.Association.Reports2.AttendeeBadgesDataSet();
                    EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable dt = (EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeDataTable)ds.Tables[0];

                    foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                    {
                        int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);

                        EventManager.Business.RegistrationAttendeesMethods ram = new EventManager.Business.RegistrationAttendeesMethods();
                        EventManager.Model.RegistrationAttendee ra = ram.GetRegistrationAttendee(regAttendeeId);

                        EventManager.Web.Association.Reports2.AttendeeBadgesDataSet.AttendeeRow r = dt.NewAttendeeRow();
                        r.AssnName = assnName;
                        r.AttendeeFirstName = ra.Attendee.FirstName;
                        r.AttendeeId = ra.Attendee.Id;
                        r.AttendeeNameFNF = ra.Attendee.FullNameFNF;
                        r.EventEndDate = endDate;
                        r.EventName = eventName;
                        r.EventStartDate = startDate;
                        r.FlagString = ra.FlagListForBadge;
                        if (ra.Registration.Organization != null && ra.Registration.Organization.OrgName != null)
                            r.OrgName = ra.Registration.Organization.OrgName;
                        else
                            r.OrgName = "";
                        r.Title = ra.Title;

                        dt.AddAttendeeRow(r);
                    }

                    Session["AttendeeBadgesDs"] = ds;
                    printMenuDropDown.SelectedIndex = 0;
                    Response.Redirect("~/Association/Reporting/TelerikReportViewer.aspx?method=push");

                }

            }

        }

        protected void FlagMenuDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox flagList = (RadComboBox)sender;

            if (flagList.SelectedIndex > 0)
            {
                EventManager.Business.RegistrationAttendeeFlags m2 = new EventManager.Business.RegistrationAttendeeFlags();

                // Assign flag for each selected row
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    int eventFlagId = Convert.ToInt32(flagList.SelectedValue);

                    m2.Add(regAttendeeId, eventFlagId);
                }

                flagList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                RegistrationsRadGrid.Rebind();
            }

        }

        protected void SessionsMenuDropDownList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox sessionList = (RadComboBox)sender;

            if (sessionList.SelectedIndex > 0)
            {
                EventManager.Business.SessionAttendanceMethods m2 = new EventManager.Business.SessionAttendanceMethods();

                // Assign flag for each selected row
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    int sessionId = Convert.ToInt32(sessionList.SelectedValue);

                    m2.Add(regAttendeeId, sessionId);
                }

                sessionList.SelectedIndex = 0;
                m2.SaveAllObjectChanges();
                RegistrationsRadGrid.Rebind();
            }

        }

        protected void ActionsRadMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (e.Item.Text == "New")
            {
                // Fill up the attendees and org lists
                int assnId = UserInfo.AssociationId;

                //---------------------------------------------------
                // Load the attendee combo
                //---------------------------------------------------
                //NewAttendeeRadComboBox.Items.Clear();
                // Look for attendees list in session, if not there, get from database
                if (Session["AttendeesList"] == null)
                {
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    List<EventManager.Model.Attendee> attendees = m.GetAttendees(assnId);
                    Session["AttendeesList"] = attendees;
                    //NewAttendeeRadComboBox.DataSource = attendees;
                }
                else
                {
                    //NewAttendeeRadComboBox.DataSource = Session["AttendeesList"];
                }
                //NewAttendeeRadComboBox.DataBind();

                //---------------------------------------------------
                // Load the facilities combo
                // --------------------------------------------------
                //NewRegFacilityRadComboBox.Items.Clear();
                if (Session["OrganizationsList"] == null)
                {
                    EventManager.Business.OrganizationMethods m2 = new EventManager.Business.OrganizationMethods();
                    List<EventManager.Model.Organization> orgs = m2.GetOrganizationsByAssociation(assnId);
                    Session["OrganizationsList"] = orgs;
                    //NewRegFacilityRadComboBox.DataSource = orgs;
                }
                else
                {
                    //NewRegFacilityRadComboBox.DataSource = Session["OrganizationsList"];
                }
                //NewRegFacilityRadComboBox.DataBind();

                // Clear out all fields of "new reg" panel and show it
                ResetNewRegistrationPanel();
                NewRegRadPane.Visible = true;
                NewRegRadPane.Collapsed = false;
                //NewRegPanel.Style.Add(HtmlTextWriterStyle.Display, "block");
            }
            else if (e.Item.Text == "Clear Filters")
            {
                // Clear all filters
                ClearAllFilters();

            }
            if (e.Item.Text == "Cancel Registration")
            {
                // Cancel all selected registrations
                RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                foreach (GridItem gi in RegistrationsRadGrid.SelectedItems)
                {
                    int regAttendeeId = Convert.ToInt32(gi.OwnerTableView.DataKeyValues[gi.ItemIndex]["Id"]);
                    m.CancelRegistration(regAttendeeId);
                }
                m.SaveAllObjectChanges();
                RegistrationsRadGrid.Rebind();
            }
        }

        protected void NewRegFacilityRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                List<EventManager.Model.Organization> orgs = GetFacilitiesList(assnId).Where(l => l.OrgName.StartsWith(e.Text)).ToList();
                if (orgs.Count > 0)
                {
                    NewRegFacilityRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Organization a in orgs)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.OrgName, a.Id.ToString());
                        NewRegFacilityRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewRegFacilityRadComboBox.ClearSelection();
                }

            }
        }

        protected void NewRegFacilityRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.Text))
            {
                try
                {
                    if (String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        //NewFacilityPanel.Visible = true;
                        //NewFacilityNameTextBox.Text = NewRegFacilityRadComboBox.Text.Trim();
                    }
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }
            }
        }

        private List<Attendee> GetAttendeesList(int assnId, bool refresh = false)
        {
            List<Attendee> attendees;
            if (Session["AttendeesList"] == null || refresh == true)
            {
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                attendees = m.GetAttendees(assnId);
                Session["AttendeesList"] = attendees;
            }
            else
            {
                attendees = (List<Attendee>)Session["AttendeesList"];
            }
            return attendees.OrderBy(a => a.LastName).ToList();
        }

        private List<Organization> GetFacilitiesList(int assnId, bool refresh = true)
        {
            List<Organization> facilities;
            if (Session["OrganizationsList"] == null || refresh == true)
            {
                EventManager.Business.OrganizationMethods m2 = new EventManager.Business.OrganizationMethods();
                facilities = m2.GetOrganizationsByAssociation(assnId);
                Session["OrganizationsList"] = facilities;
            }
            else
            {
                facilities = (List<Organization>)Session["OrganizationsList"];
            }
            return facilities.OrderBy(f => f.OrgName).ToList();
        }

        protected void NewAttendeeRadComboBox_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            if (e.Text.Length >= 2)
            {
                int assnId = UserInfo.AssociationId;

                List<EventManager.Model.Attendee> attendees = GetAttendeesList(assnId).Where(l => (l.LastName + ", " + l.FirstName).StartsWith(e.Text, true, null)).OrderBy(l => l.LastName).ThenBy(l => l.FirstName).ToList();
                //List<EventManager.Model.Attendee> attendees = GetAttendeesList(assnId, false);
                if (attendees.Count > 0)
                {
                    NewAttendeeRadComboBox.Items.Clear();
                    foreach (EventManager.Model.Attendee a in attendees)
                    {
                        RadComboBoxItem item = new RadComboBoxItem(a.LastName + ", " + a.FirstName + " (" + a.Id.ToString() + ")", a.Id.ToString());
                        NewAttendeeRadComboBox.Items.Add(item);
                    }
                }
                else
                {
                    NewAttendeeRadComboBox.ClearSelection();
                }
            }
        }

        protected void NewAttendeeRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.Text))
            {
                try
                {
                    if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                    {
                        //NewAttendeePanel.Visible = false;
                        int attendeeId = Convert.ToInt32(e.Value);

                        // Get the selected attendee from drop down
                        EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                        EventManager.Model.Attendee a = am.GetAttendee(attendeeId);

                        // Pre-fill their org and title (if available)
                        if (a.Organization != null && !String.IsNullOrEmpty(a.Organization.OrgName))
                        {
                            //RadComboBoxItem comboItem = NewRegFacilityRadComboBox.FindItemByValue(a.OrganizationId.ToString());
                            //NewRegFacilityRadComboBox.SelectedValue = a.OrganizationId.ToString();
                            //NewRegFacilityRadComboBox.Focus();

                            NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Text = "";
                            RadComboBoxItem item = new RadComboBoxItem(a.Organization.OrgName, a.Organization.Id.ToString());
                            NewRegFacilityRadComboBox.Items.Insert(0, item);
                            NewRegFacilityRadComboBox.SelectedIndex = 0;
                            NewRegFacilityRadComboBox.Focus();

                        }
                        else
                        {
                            NewRegFacilityRadComboBox.Text = "";
                            //NewRegFacilityRadComboBox.Items.Clear();
                            NewRegFacilityRadComboBox.Focus();
                            //NewFacilityPanel.Visible = true;
                        }

                        NewRegTitleTextBox.Text = a.Title.Trim();
                    }
                    else
                    {
                        //NewRegFacilityRadComboBox.Items.Clear();
                        NewRegFacilityRadComboBox.Text = "";
                        NewRegTitleTextBox.Text = "";
                        NewRegFacilityRadComboBox.Focus();
                    }

                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }

            }
            else
            {
                //NewAttendeePanel.Visible = false
            }

        }

        protected void NewRegAddButton_Click(object sender, EventArgs e)
        {
            try
            {
                int assnId = UserInfo.AssociationId;
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                Guid userId = UserInfo.UserId;
                EventManager.Model.Organization org = null;
                EventManager.Model.Attendee att = null;
                string title = "";

                EventManager.Business.OrganizationMethods om = new EventManager.Business.OrganizationMethods();
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();

                // Find the attendee (or create, if one is not selected)
                if (!String.IsNullOrEmpty(NewAttendeeRadComboBox.SelectedValue))
                {
                    att = am.GetAttendee(Convert.ToInt32(NewAttendeeRadComboBox.SelectedValue));
                }
                else if (NewAttendeeRadComboBox.Text.Trim().Length > 3)
                {
                    List<Attendee> matchingAttendees = am.GetAttendeesByName(assnId, NewAttendeeRadComboBox.Text.Trim());
                    if (matchingAttendees.Count > 0)
                        att = matchingAttendees[0];

                    if (att == null)
                    {
                        string firstName = GetFirstName(NewAttendeeRadComboBox.Text);
                        string lastName = GetLastName(NewAttendeeRadComboBox.Text);
                        att = am.AddAttendee(firstName, lastName, "", assnId);
                        if (att != null)
                        {
                            // Add the newly entered attendee to the session list
                            List<Attendee> attendeeList = (List<Attendee>)Session["AttendeesList"];
                            attendeeList.Add(att);
                            Session["AttendeesList"] = attendeeList;

                            //FillAttendeeCombo();
                        }
                    }
                }

                if (att != null)
                {
                    // Get the selected org (or 'null' for individual registration)
                    if (!String.IsNullOrEmpty(NewRegFacilityRadComboBox.SelectedValue))
                    {
                        org = om.GetOrganization(Convert.ToInt32(NewRegFacilityRadComboBox.SelectedValue));
                    }
                    else if (NewRegFacilityRadComboBox.Text.Trim().Length > 1)
                    {
                        org = om.GetOrganizationByName(assnId, NewRegFacilityRadComboBox.Text.Trim());
                        if (org == null)
                        {
                            org = om.AddOrganization(assnId, NewRegFacilityRadComboBox.Text.Trim());
                            if (org != null)
                            {
                                List<Organization> orgList = (List<Organization>)Session["OrganizationsList"];
                                orgList.Add(org);
                                Session["OrganizationsList"] = orgList;
                                //FillFacilityCombo();
                            }
                        }

                    }

                    // Get the title
                    title = NewRegTitleTextBox.Text;

                    EventManager.Business.RegistrationMethods efm = new EventManager.Business.RegistrationMethods();
                    efm.AddRegistrationWithAttendee(eventId, org, att, title, userId);

                    efm.SaveAllObjectChanges();

                    // Show success message
                    NewRegSuccessValidator.ErrorMessage = "'" + att.LastName + ", " + att.FirstName + "' successfully registered.";
                    NewRegSuccessValidator.IsValid = false;

                    // Reset all inputs
                    ResetNewRegistrationPanel();

                    // Filter the grid to show the newly inserted person 
                    // Rebind the registrations to show the new attendee registration
                    //RegistrationsRadGrid.Rebind();
                    GridColumn attNameCol = RegistrationsRadGrid.MasterTableView.GetColumnSafe("AttendeeNameTemplateColumn");
                    RegistrationsRadGrid.MasterTableView.FilterExpression = "([AttendeeFullName] LIKE '" + att.LastName + ", " + att.FirstName + "%')";
                    attNameCol.CurrentFilterFunction = GridKnownFunction.StartsWith;
                    attNameCol.CurrentFilterValue = att.LastName + ", " + att.FirstName;
                    RegistrationsRadGrid.MasterTableView.Rebind();

                    //RegistrationsRadGrid.Rebind();
                    //RegGridUpdatePanel.Update();
                }


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    NewRegCustomValidator.ErrorMessage = ex.InnerException.Message;
                else
                    NewRegCustomValidator.ErrorMessage = ex.Message;

                NewRegCustomValidator.IsValid = false;
            }
        }

        private string GetLastName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0)
            {
                n = fullName.Substring(0, fullName.IndexOf(",")).Trim();
            }
            return n.Trim();
        }

        private string GetFirstName(string fullName)
        {
            string n = "";
            fullName = fullName.Trim();
            if (fullName.Length > 3 && fullName.IndexOf(",") > 0 && fullName.IndexOf(",") < fullName.Length - 1)
            {
                n = fullName.Substring(fullName.IndexOf(",") + 1).Trim();
            }
            return n;
        }

        private void ResetNewRegistrationPanel()
        {
            //NewAttendeeRadComboBox.Items.Clear();
            NewAttendeeRadComboBox.Text = "";
            NewAttendeeRadComboBox.SelectedIndex = -1;
            //NewRegFacilityRadComboBox.Items.Clear();
            NewRegFacilityRadComboBox.Text = "";
            NewRegFacilityRadComboBox.SelectedIndex = -1;
            NewRegTitleTextBox.Text = "";

            NewAttendeeRadComboBox.Focus();
        }

        protected void RegistrationsRadGrid_Init(object sender, EventArgs e)
        {
            //GridFilterMenu menu = RegistrationsRadGrid.FilterMenu;
            //int i = 0;
            //while (i < menu.Items.Count)
            //{
            //    if (menu.Items[i].Text == "NoFilter" || menu.Items[i].Text == "Contains" || menu.Items[i].Text == "StartsWith" || menu.Items[i].Text == "EqualTo" || menu.Items[i].Text == "GreaterThan" || menu.Items[i].Text == "LessThan")
            //    {
            //        i++;
            //    }
            //    else
            //    {
            //        menu.Items.RemoveAt(i);
            //    }
            //}

        }

        protected void RegistrationsRadGrid_ItemDeleted(object sender, GridDeletedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
                    m.SaveAllObjectChanges();
                    RegistrationsRadGrid.Rebind();
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                }

            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void RegistrationsRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem)
            {
                if (e.Item.IsInEditMode)
                {
                    int regAttendeeId = Convert.ToInt32(((GridEditFormItem)e.Item).GetDataKeyValue("Id"));

                    // Get all flags for the selected attendee
                    //EventManager.Business.RegistrationAttendeeFlags ram = new EventManager.Business.RegistrationAttendeeFlags();
                    //foreach (EventManager.Model.RegistrationAttendeeFlag af in ram.GetRegisteredAttendeeFlags(regAttendeeId))
                    //{
                    //    // find the matching item in check box list and check it
                    //    CheckBoxList flagCheckBoxList = (CheckBoxList)e.Item.FindControl("AttendeeFlagsCheckBoxList");
                    //    if (flagCheckBoxList != null)
                    //    {
                    //        foreach (ListItem checkBox in flagCheckBoxList.Items)
                    //        {
                    //            if (Convert.ToInt32(checkBox.Value) == af.EventFlagId)
                    //                checkBox.Selected = true;
                    //        }
                    //    }

                    //}

                    //// Fill the facility combo
                    ////EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();
                    //RadComboBox facilitiesCombo = (RadComboBox)e.Item.FindControl("FacilitiesRadComboBox");
                    ////facilitiesCombo.DataSource = m.GetOrganizationsByAssociation(UserInfo.AssociationId);
                    //facilitiesCombo.DataSource = GetFacilitiesList(UserInfo.AssociationId, false);
                    //facilitiesCombo.AppendDataBoundItems = true;
                    //facilitiesCombo.Items.Insert(0, new RadComboBoxItem("", ""));
                    //facilitiesCombo.DataBind();

                    //// Select the facility
                    //int? orgId = ((EventManager.Model.RegAttendeeRow)e.Item.DataItem).OrgId;
                    //if (facilitiesCombo.Items.Count > 0)
                    //    if (orgId != null)
                    //        facilitiesCombo.SelectedValue = orgId.ToString();
                    //    else
                    //        facilitiesCombo.SelectedIndex = 0;



                }
            }

        }

        protected void RegistrationsRadGrid_ItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    int regAttendeeId = Convert.ToInt32((e.Item as GridEditableItem).GetDataKeyValue("Id"));
                    EventManager.Business.RegistrationAttendeeFlags m2 = new EventManager.Business.RegistrationAttendeeFlags();

                    CheckBoxList flagCheckBoxList = (CheckBoxList)e.Item.FindControl("AttendeeFlagsCheckBoxList");
                    m2.DeleteAllFlags(regAttendeeId);
                    m2.SaveAllObjectChanges();

                    foreach (ListItem checkbox in flagCheckBoxList.Items)
                    {
                        if (checkbox.Selected)
                        {
                            m2.Add(regAttendeeId, Convert.ToInt32(checkbox.Value));
                        }
                    }

                    // If facility has changed, do the update
                    RadComboBox orgCombo = (RadComboBox)e.Item.FindControl("FacilitiesRadComboBox");
                    if (orgCombo != null)
                    {
                        EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
                        if (orgCombo.SelectedIndex == 0)
                            m.UpdateFacility(regAttendeeId, null, UserInfo.UserId);
                        else
                            m.UpdateFacility(regAttendeeId, Convert.ToInt32(orgCombo.SelectedValue), UserInfo.UserId);
                    }


                    m2.SaveAllObjectChanges();
                    RegistrationsRadGrid.Rebind();
                    //RegGridUpdatePanel.Update();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    }
                    else
                    {
                        CustomValidator1.ErrorMessage = ex.Message;
                    }
                    CustomValidator1.IsValid = false;
                    e.KeepInEditMode = true;
                }

            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }


        }

        protected void RegistrationsRadGrid_ItemCreated(object sender, GridItemEventArgs e)
        {
            //Control ctl = null;

            //if (IsPostBack)
            //{ 
            //    ctl = CommonWebUtils.GetPostBackControl(Page);
            //}

            //if (!IsPostBack || (IsPostBack && (ctl != null && ctl.ID == "RegistrationsRadGrid")))
            //{
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                string eventId = Request.QueryString["EventId"];
                string regAttendeeId = item.GetDataKeyValue("Id").ToString();
                string attendeeId = item.GetDataKeyValue("AttendeeId").ToString();

                if (item.DataItem != null)
                {
                    if (!isPdfExport && !isEdit)
                    {
                        RadButton editBtn = (RadButton)item.FindControl("EditAttendanceRadButton");
                        editBtn.Attributes["onclick"] = String.Format("return ShowAttendanceDialog('{0}', '{1}', '{2}');", eventId, regAttendeeId, item.ItemIndex);

                        // Set the click event for "Show Attendee Dialog"
                        LinkButton attendeeNameButton = (LinkButton)item.FindControl("AttendeeNameLinkButton");
                        attendeeNameButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}');", attendeeId, item.ItemIndex);

                        // Set the click event for "Print Certifiocate" dialog
                        HyperLink printCertHyperlink = (HyperLink)item.FindControl("PrintCertHyperlink");
                        printCertHyperlink.Attributes["onclick"] = String.Format("return ShowCertificateReport('{0}', '{1}', '" + certificateReportName + "');", Request.QueryString["EventId"], attendeeId);
                    }

                }
            }
            else if (e.Item is GridPagerItem)
            {
                //RadComboBox combo = (e.Item as GridPagerItem).FindControl("PageSizeComboBox") as RadComboBox;
                //RadComboBoxItem item = new RadComboBoxItem("All", int.MaxValue.ToString());
                //item.Attributes.Add("ownerTableViewId", e.Item.OwnerTableView.ClientID);
                //combo.Items.Add(item);
                //if (RegistrationsRadGrid.PageSize == int.MaxValue)
                //{
                //    combo.Items.FindItemByText("All").Selected = true;
                //}
                //else
                //{
                //    combo.Items.FindItemByValue(RegistrationsRadGrid.PageSize.ToString()).Selected = true;
                //}
                //RadComboBoxItem extraItem = combo.Items.FindItemByText(int.MaxValue.ToString());
                //if (extraItem != null)
                //{
                //    combo.Items.Remove(extraItem);
                //} 

                // METHOD 1
                //int allItemsCount = (e.Item as GridPagerItem).Paging.DataSourceCount;
                //RadComboBox combo = (e.Item as GridPagerItem).FindControl("PageSizeComboBox") as RadComboBox;

                //RadComboBoxItem item = new RadComboBoxItem("All", allItemsCount.ToString());
                //item.Attributes.Add("ownerTableViewId", e.Item.OwnerTableView.ClientID);
                //combo.Items.Add(item);
                //combo.Items.FindItemByValue(RegistrationsRadGrid.PageSize.ToString()).Selected = true;


                // METHOD 2
                GridPagerItem gpi = e.Item as GridPagerItem;

                //Give pager drop down an 'All' option.
                RadComboBox combo = gpi.FindControl("PageSizeComboBox") as RadComboBox;
                if (combo != null)
                {
                    RadComboBoxItem item = new RadComboBoxItem("All", int.MaxValue.ToString());
                    item.Attributes.Add("ownerTableViewId", gpi.OwnerTableView.ClientID);
                    combo.Items.Add(item);
                    if (RegistrationsRadGrid.PageSize == int.MaxValue) { combo.Items.FindItemByText("All").Selected = true; }
                    else { combo.Items.FindItemByValue(RegistrationsRadGrid.PageSize.ToString()).Selected = true; }
                    if (combo.Items.FindItemByText(int.MaxValue.ToString()) != null) { combo.Items.Remove(combo.Items.FindItemByText(int.MaxValue.ToString())); }
                }

            }
            //}
        }

        protected void RegistrationsRadGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            //AccountProfile.CurrentUser.RegAttendeesGridPageSize = e.NewPageSize;
            //RegistrationsRadGrid.Height = RegGridRadPane.Height.ToString().Substring(0, RegGridRadPane.Height.ToString().Length - 2) - 30;
        }

        protected void RegistrationsRadGrid_ItemEvent(object sender, GridItemEventArgs e)
        {
            if (e.EventInfo is GridInitializePagerItem)
            {
                //TotalItemCount = (e.EventInfo as GridInitializePagerItem).PagingManager.DataSourceCount;
            }
        }

        protected void btnShowAll_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ClearAllFilters();
        }

        private void ClearAllFilters()
        {
            RegistrationsRadGrid.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RegistrationsRadGrid.MasterTableView.RenderColumns)
            {
                if (column.SupportsFiltering())
                {
                    column.CurrentFilterValue = string.Empty;
                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                }
            }
            RegistrationsRadGrid.MasterTableView.Rebind();
        }

        protected void RegistrationsRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Filter")
            {
                ActionsRadMenu.Items[0].Text = RegistrationsRadGrid.MasterTableView.FilterExpression;
            }
            else if (e.CommandName == "Edit")
            {
                isEdit = true;
            }
            else if (e.CommandName == "Update" || e.CommandName == "Cancel")
            {
                isEdit = false;
            }
        }

        protected void RegistrationsRadGrid_NeedDataSource_old(object sender, GridNeedDataSourceEventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);

            EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
            RegistrationsRadGrid.DataSource = m.GetRegisteredAttendeesForEvent(eventId);
        }

        protected void RegistrationsRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);

            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            RegistrationsRadGrid.DataSource = m.GetRegisteredAttendeesForGrid(eventId);

        }


        protected void RegistrationsRadGrid_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            int regAttendeeId = Convert.ToInt32((e.Item as GridDataItem).GetDataKeyValue("Id"));

            RegistrationAttendeesMethods am = new RegistrationAttendeesMethods();
            RegistrationAttendee ra = am.GetRegistrationAttendee(regAttendeeId);
            am.Delete(ra);
            am.SaveAllObjectChanges();

            //RegistrationsRadGrid.Rebind();
        }

        protected void RegistrationsRadGrid_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            int regAttendeeId = Convert.ToInt32(item.GetDataKeyValue("Id"));

            RegistrationAttendeesMethods am = new RegistrationAttendeesMethods();
            RegistrationAttendee ra = am.GetRegistrationAttendee(regAttendeeId);

            RegistrationAttendeeFlags m2 = new RegistrationAttendeeFlags();

            CheckBoxList flagCheckBoxList = (CheckBoxList)e.Item.FindControl("AttendeeFlagsCheckBoxList");
            m2.DeleteAllFlags(regAttendeeId);
            m2.SaveAllObjectChanges();

            foreach (ListItem checkbox in flagCheckBoxList.Items)
            {
                if (checkbox.Selected)
                {
                    m2.Add(regAttendeeId, Convert.ToInt32(checkbox.Value));
                }
            }

            // If facility has changed, do the update
            RadComboBox orgCombo = (RadComboBox)e.Item.FindControl("FacilitiesRadComboBox");
            if (orgCombo != null)
            {
                EventManager.Business.RegistrationAttendeesMethods m = new EventManager.Business.RegistrationAttendeesMethods();
                if (orgCombo.SelectedIndex == 0)
                    m.UpdateFacility(regAttendeeId, null, UserInfo.UserId);
                else
                    m.UpdateFacility(regAttendeeId, Convert.ToInt32(orgCombo.SelectedValue), UserInfo.UserId);
            }

            item.UpdateValues(ra);
            am.SaveAllObjectChanges();
            //RegistrationsRadGrid.Rebind();

            // Hide all edit items
            RegistrationsRadGrid.MasterTableView.ClearEditItems();
        }

        private void Page_Error(object sender, EventArgs e)
        {
            // Get last error from the server
            Exception exc = Server.GetLastError();
            string errorMsg = "";

            // Handle exceptions generated by Button 1
            if (exc is System.Web.HttpException)
            {
                if (exc.InnerException.Message.Contains("Request timed out"))
                {
                    CustomValidator1.ErrorMessage = "This attendee has registered for one or more events.";
                }
                else
                {
                    CustomValidator1.ErrorMessage = "An error occurred.";
                }
                CustomValidator1.ErrorMessage = exc.Message;
                CustomValidator1.IsValid = false;

                // Log the exception and notify system operators
                ExceptionUtility.LogException(exc, "EventRegAttendees2.aspx");
                ExceptionUtility.NotifySystemOps(exc, "EventRegAttendees2.aspx");

                // Clear the error from the server
                Server.ClearError();
            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

        protected void PrintMenuRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox printCombo = (RadComboBox)sender;
            if (printCombo.SelectedValue == "PrintBadgeSheet")
                PrintBadgeSheetForSelectedAttendees();
            else if (printCombo.SelectedValue == "PrintRegistrations")
                PrintRegistrationsForSelectedAttendees();
            else if (printCombo.SelectedValue == "PrintContactSheet")
                PrintContactSheetForSelectedAttendees();
            else if (printCombo.SelectedValue == "PringSingleBadge")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "ShowPrintBadgeForm();", true);
            }
        }
    }
}