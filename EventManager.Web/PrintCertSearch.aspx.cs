﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Model;

namespace EventManager.Web
{
    public partial class PrintCertSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadAssociations();
            }
        }

        private void LoadAssociations()
        {
            EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
            AssociationRadComboBox.DataSource = m.GetAssociations();
            AssociationRadComboBox.DataBind();
        }

        protected void AssociationRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (AssociationRadComboBox.Items.Count > 0)
            {
                // Look for cookie stored for assn
                if (Request.Cookies["assnId"] != null && !string.IsNullOrWhiteSpace(Request.Cookies["assnId"].Value))
                {
                    string assnId = Request.Cookies["assnId"].Value;
                    AssociationRadComboBox.SelectedValue = assnId;
                }
                else
                {
                    AssociationRadComboBox.SelectedIndex = 0;
                }

                LoadFacilitiesforAssociation();
            }
        }

        protected void AssociationRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (AssociationRadComboBox.SelectedIndex > 0)
            {
                FirstNameRadTextBox.Enabled = true;
                LastNameRadTextBox.Enabled = true;
                LoadFacilitiesforAssociation();
            }
            else
            {
                FirstNameRadTextBox.Enabled = false;
                LastNameRadTextBox.Enabled = false;
                FacilityRadComboBox.Items.Clear();
                FacilityRadComboBox.Enabled = false;
            }

        }

        private void LoadFacilitiesforAssociation()
        {
            if (AssociationRadComboBox.SelectedIndex > 0)
            {
                EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();

                FacilityRadComboBox.Items.Clear();
                FacilityRadComboBox.Text = "";
                FacilityRadComboBox.DataSource = m.GetOrganizationsByAssociation(Convert.ToInt32(AssociationRadComboBox.SelectedValue));
                FacilityRadComboBox.DataBind();
                FacilityRadComboBox.Items.Insert(0, new RadComboBoxItem("", ""));
                FacilityRadComboBox.Enabled = true;
            }
            else
            {
                FacilityRadComboBox.Items.Clear();
            }
        }

        protected void AttendeeSearchRadButton_Click(object sender, EventArgs e)
        {
            AttendeesRadGrid.Visible = false;
            int assnId = 0;
            int? orgId = 0; 
            string lastName = LastNameRadTextBox.Text.Trim();
            string firstName = FirstNameRadTextBox.Text.Trim();

            if (!Int32.TryParse(AssociationRadComboBox.SelectedValue, out assnId))
            {
                CustomValidator1.ErrorMessage = "Please select a valid association";
                CustomValidator1.IsValid = false;
            }

            if (FacilityRadComboBox.SelectedIndex > 0)
            {
                int tempOrgId;
                if (!Int32.TryParse(FacilityRadComboBox.SelectedValue, out tempOrgId))
                {
                    CustomValidator1.ErrorMessage = "Please select a valid facility";
                    CustomValidator1.IsValid = false;
                }
                else
                {
                    orgId = tempOrgId;
                }
            }
            else
            {
                orgId = null;
            }

            if (string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(firstName))
            {
                CustomValidator1.ErrorMessage = "Please enter your first and last name";
                CustomValidator1.IsValid = false;
            }

            if (CustomValidator1.IsValid)
            { 
                // Store assnId in session for next time
                Response.Cookies["assnId"].Value = AssociationRadComboBox.SelectedValue;
                Response.Cookies["assnId"].Expires = DateTime.Now.AddDays(365);

                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                Attendee att;
                if (orgId == 0)
                    orgId = null;

                List<EventManager.Model.Attendee> attendees = m.GetAttendeesByNameAndFacility(assnId, orgId, firstName, lastName, true);
                if (attendees.Count == 1)
                {
                    att = attendees[0];
                    Response.Redirect("~/PrintCertHistory.aspx?id=" + att.Id.ToString());
                }
                else if (attendees.Count == 0)
                {
                    CustomValidator1.ErrorMessage = "No matching person can be found.";
                    CustomValidator1.IsValid = false;
                }
                else if (attendees.Count > 1)
                {
                    AttendeesRadGrid.DataSource = attendees;
                    AttendeesRadGrid.DataBind();
                    AttendeesRadGrid.Visible = true;

                    CustomValidator1.ErrorMessage = "There were " + attendees.Count.ToString() + " profiles found matching search criteria.  Click the 'This is me' button on the profile that matches your most recent information.   If more than one listed profile is yours, contact your association and have them merge your profiles into one.";
                    CustomValidator1.IsValid = false;
                }
            }
            

        }

        protected void AttendeesRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "SelectAttendee")
            {
                GridDataItem item = (GridDataItem)e.Item;
                Response.Redirect("~/PrintCertHistory.aspx?id=" + item["Id"].Text);            
            }
        }
    }
}