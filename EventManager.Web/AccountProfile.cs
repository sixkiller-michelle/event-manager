﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;

namespace EventManager.Web
{
    public class AccountProfile : ProfileBase
    {
        static public AccountProfile CurrentUser
        {
            get
            {
                return (AccountProfile)
                       (ProfileBase.Create(Membership.GetUser().UserName));
            }
        }

        public string FullName
        {
            get { return ((string)(base["FullName"])); }
            set { base["FullName"] = value; Save(); }
        }

        public int RegAttendeesGridPageSize
        {
            get { return ((int)(base["RegAttendeesGridPageSize"])); }
            set { base["RegAttendeesGridPageSize"] = value; Save(); }
        }

        // add additional properties here 
    } 

}