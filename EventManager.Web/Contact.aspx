﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EventManager.Web.ContactForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 93px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div id="mainleft">
		<div id="content" class="box">
			<h2 class="page-title">Contact</h2>
			<div class="breadcrumb"><a href="#" id="home">Home</a>  »  <a href="#">Contact</a></div>
			<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nunc odio, faucibus id molestie non, congue id dolor.</strong><br />Duis congue faucibus leo at convallis. Nunc elit tellus, imperdiet adipiscing hendrerit id, vehicula quis nisi. Sed lobortis ipsum vel nunc commodo tempor. In eget risus nec felis lacinia egestas  in scelerisque felis. </p>
			<div id="contact_form">
				
                <asp:TextBox ID="ConfirmationTextBox" runat="server" Text="" Visible="false" BorderStyle="None" BorderWidth="0" Font-Size="Medium" Width="500" />
    
                <table style="width: 400px;">
                    <tr>
                        <td class="style1">Name:&nbsp;</td>
                        <td><asp:TextBox ID="NameTextBox" runat="server" Text=""   />&nbsp;</td>
           
                    </tr>
                    <tr>
                        <td class="style1">Email:&nbsp;</td>
                        <td><asp:TextBox ID="EmailTextBox" runat="server" Text=""  />&nbsp;</td>
           
                    </tr>
                    <tr>
                        <td class="style1">Message:&nbsp;</td>
                        <td><asp:TextBox ID="MessageTextBox" runat="server" Text="" TextMode="MultiLine" Rows="10" Columns="50" Width="300" Height="100" />&nbsp;</td>
           
                    </tr>
                    <tr>
                        <td colspan="2">
                        <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" UseSubmitBehavior="true" CssClass="input-submit" onclick="SubmitButton_Click" />
                        </td>
                    </tr>
                </table>       
			</div><!-- end of #contact_form -->
			<br/>
		</div><!-- end #content -->
	</div><!-- end #mainleft -->
	<div id="mainright">
		<div class="box">
		<ul>
			<li class="widget-container"><h2 class="widget-title">Head Office</h2>
				<div class="widget-text">
					<h5>SoftLayer DC</h5>
					<a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Granville+Island,+Vancouver&amp;sll=49.293533,-123.13189&amp;sspn=0.010454,0.032637&amp;ie=UTF8&amp;hq=&amp;hnear=Granville+Island,+Canada&amp;ll=49.279565,-123.129787&amp;spn=0.020917,0.065274&amp;t=h&amp;z=14&amp;iwloc=A&amp;output=embed?iframe=true&amp;width=640&amp;height=480" rel="prettyPhoto" ><img src="images/icon-services4.png" alt="" class="alignright" /></a>
								

					1234 Address City, TS 56789<br />
					City, Country<br />
					<div class="contact-text">
						<span class="lefttext">Telephone</span><span class="dot">:</span><span class="righttext">0800-123456</span> <br />
						<span class="lefttext">Faximillie</span><span class="dot">:</span><span class="righttext">0800-123457</span> <br />
						<span class="lefttext">Email</span><span class="dot">:</span><span class="righttext"><a href="mailto:testmail@templatesquare.com">testmail@templatesquare.com</a></span>
					</div>
					<div class="clear"></div>
				</div>
			</li>
			<li class="widget-container"><h2 class="widget-title">Branch Office</h2>
				<div class="widget-text">
					<h5>The Planet</h5>
					<a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Granville+Island,+Vancouver&amp;sll=49.293533,-123.13189&amp;sspn=0.010454,0.032637&amp;ie=UTF8&amp;hq=&amp;hnear=Granville+Island,+Canada&amp;ll=49.279565,-123.129787&amp;spn=0.020917,0.065274&amp;t=h&amp;z=14&amp;iwloc=A&amp;output=embed?iframe=true&amp;width=640&amp;height=480" rel="prettyPhoto" ><img src="images/icon-services4.png" alt="" class="alignright" /></a>
					1234 Address City, TS 56789<br />
					City, Country<br />
					<div class="contact-text">
						<span class="lefttext">Telephone</span><span class="dot">:</span><span class="righttext">0800-123456</span> <br />
						<span class="lefttext">Faximillie</span><span class="dot">:</span><span class="righttext">0800-123457</span> <br />
						<span class="lefttext">Email</span><span class="dot">:</span><span class="righttext"><a href="mailto:testmail@templatesquare.com">testmail@templatesquare.com</a></span>
					</div>
					<div class="clear"></div>
				</div>
				<br />
				<div class="widget-text">
					<h5>Liquid Web</h5>
					<a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Granville+Island,+Vancouver&amp;sll=49.293533,-123.13189&amp;sspn=0.010454,0.032637&amp;ie=UTF8&amp;hq=&amp;hnear=Granville+Island,+Canada&amp;ll=49.279565,-123.129787&amp;spn=0.020917,0.065274&amp;t=h&amp;z=14&amp;iwloc=A&amp;output=embed?iframe=true&amp;width=640&amp;height=480" rel="prettyPhoto" ><img src="images/icon-services4.png" alt="" class="alignright" /></a>
					1234 Address City, TS 56789<br />
					City, Country<br />
					<div class="contact-text">
						<span class="lefttext">Telephone</span><span class="dot">:</span><span class="righttext">0800-123456</span> <br />
						<span class="lefttext">Faximillie</span><span class="dot">:</span><span class="righttext">0800-123457</span> <br />
						<span class="lefttext">Email</span><span class="dot">:</span><span class="righttext"><a href="mailto:testmail@templatesquare.com">testmail@templatesquare.com</a></span>
					</div>
					<div class="clear"></div>
				</div>
			</li>
		</ul>
		<br/>
		</div>
	</div><!-- end #mainright -->
	<div class="clear"></div><!-- clear float -->


     

</asp:Content>
