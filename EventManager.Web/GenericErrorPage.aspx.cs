﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web
{
    public partial class GenericErrorPage : System.Web.UI.Page
    {
        protected Exception ex = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the last error from the server
            Exception ex = Server.GetLastError();

            // Create a safe message
            string safeMsg = "A problem has occurred in the web site. System administrators have been notified of this error and will get it fixed ASAP.";

            // Show Inner Exception fields for local access
            if (ex != null && ex.InnerException != null)
            {
                innerMessage.Text = ex.InnerException.Message;
                innerTrace.Text = ex.InnerException.StackTrace;
                InnerErrorPanel.Visible = Request.IsLocal;
            }

            // Show exception fields for local access
            if (ex != null)
            {
                exMessage.Text = ex.Message;
                exTrace.Text = ex.StackTrace;
                ErrorPanel.Visible = Request.IsLocal;
            }

            SafeErrorLabel.Text = safeMsg;

            if (Request.Path != "/GenericErrorPage.aspx")
            {
                ReturnHyperlink.NavigateUrl = "~" + Request.Path + "?" + Request.QueryString;
                ReturnHyperlink.Text = "~" + Request.Path + "?" + Request.QueryString;
            }
            else
            {
                ReturnHyperlink.NavigateUrl = "";
                ReturnHyperlink.Text = "[not available]";
            }

            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                if (Roles.IsUserInRole("AssociationAdmin") || Roles.IsUserInRole("AssociationEmployee") || Roles.IsUserInRole("Developer"))
                    DashboardHyperlink.NavigateUrl = "~/Association/Dashboard.aspx";
                else if (Roles.IsUserInRole("FacilityAdmin") || Roles.IsUserInRole("FacilityEmployee"))
                    DashboardHyperlink.NavigateUrl = "~/Facility/Dashboard.aspx";
                else
                    DashboardHyperlink.Enabled = false;
            }
            else
            {
                DashboardHyperlink.Enabled = false;
            }

            // Clear the error from the server
            Server.ClearError();

        }
    }
}