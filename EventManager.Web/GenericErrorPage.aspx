﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GenericErrorPage.aspx.cs" Inherits="EventManager.Web.GenericErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<%--
The following example shows the GenericErrorPage.aspx page. 
This page creates a safe message that it displays to remote users. For local users (typically developers and testers of the application), 
the page displays a complete exception report. The Page_Error handler redirects InvalidOperationException errors to this page.
--%>
<div style="min-height:300px;">

    <h2 style="color:Red;">Oops! We're sorry, but an error has occurred.</h2>

    <asp:Label ID="SafeErrorLabel" runat="server" Font-Bold="true" Font-Size="Large" />
    <br /><br />
    You can try returning to the page that caused the error: <asp:HyperLink ID="ReturnHyperlink" runat="server" NavigateUrl="" /><br /><br />
    If that doesn't work, return to your <asp:HyperLink ID="DashboardHyperlink" runat="server" NavigateUrl="~/Association/Dashboard.aspx">dashboard</asp:HyperLink> OR 
    the ceuavenue.com <asp:HyperLink ID="HomePageHyperlink" runat="server" NavigateUrl="~/Default.aspx">home page</asp:HyperLink>.
    <br /><br />

    <asp:Panel ID="InnerErrorPanel" runat="server" Visible="false">
        
          <hr />Inner Error Message:<br />
            
            <asp:Label ID="innerMessage" runat="server" Font-Bold="true" Font-Size="Medium" /><br />
          <pre>
            <asp:Label ID="innerTrace" runat="server" />
          </pre>
    </asp:Panel>

    <asp:Panel ID="ErrorPanel" runat="server" Visible="false">
        
          <hr />Error Message:<br />
          
          <asp:Label ID="exMessage" runat="server" Font-Bold="true" 
            Font-Size="Large" />
        
        <pre>
          <asp:Label ID="exTrace" runat="server" Visible="false" />
        </pre>
    </asp:Panel>

  </div>


</asp:Content>
