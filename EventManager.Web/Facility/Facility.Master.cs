﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;

namespace EventManager.Web.Facility
{
    public partial class FacilityMaster2 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool switchAssnModuleVisible = false;

            // Check if logged in user is a member of an association
            if (Membership.GetUser() != null)
            {
                if (Roles.IsUserInRole("AssociationAdmin") || Roles.IsUserInRole("AssociationEmployee") || Roles.IsUserInRole("Developer"))
                {
                    switchAssnModuleVisible = true;
                }

            }
                
            Panel assnModulePanel = (Panel)LoginView1.FindControl("AssociationModulePanel");
            if (assnModulePanel != null)
                assnModulePanel.Visible = switchAssnModuleVisible;

            SetSelectedMenuItem();
            LoadAssnInfo();

        }

        protected void SetSelectedMenuItem()
        {
            RadMenu mainMenu = RadMenu1;

            if (mainMenu != null)
            {
                if (Request.ServerVariables["Path_Info"].Contains("/Facility/Events/EventList.aspx"))
                    mainMenu.Items[0].Selected = true;
                else if (Request.ServerVariables["Path_Info"].Contains("/Facility/Events/Attendance.aspx"))
                    mainMenu.Items[1].Selected = true;
                //else if (Request.ServerVariables["Path_Info"].Contains("/Facility/Org"))
                //    mainMenu.Items[2].Selected = true;
            }

        }

        private void LoadAssnInfo()
        {
            EventManager.Model.Association a = null;
            if (Membership.GetUser() != null)
            {
                EventManager.Business.UserAssociationMethods m = new EventManager.Business.UserAssociationMethods();
                a = m.GetAssociationForUser(UserInfo.UserId);
            }

            if (a == null)
            {
                LogoImage.ImageUrl = "";
                AssnNameLabel.Text = "";
                AssnAddressLabel.Text = "";
                AssnCityLabel.Text = "";
                AssnPhoneLabel.Text = "";
                AssnFaxLabel.Text = "";
            }
            else
            {
                LogoImage.ImageUrl = a.LogoPath;
                AssnNameLabel.Text = a.AssociationName;
                AssnAddressLabel.Text = a.Address;
                AssnCityLabel.Text = a.City + ", " + a.StateCode + " " + a.Zip;
                AssnPhoneLabel.Text = a.PhoneNumber;
                AssnFaxLabel.Text = a.FaxNumber;
            }
        }
    }
}