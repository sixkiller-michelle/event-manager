﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility
{
    public partial class RegWizForm : System.Web.UI.Page
    {
        private int _eventId;
        private int _regId;
        private Guid _userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                RegistrationMethods m = new RegistrationMethods();

                if (Request.QueryString["RegId"] == null && Request.QueryString["EventId"] != null)
                {
                    // See if there is an existing reg for user for this event
                    _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                    Registration reg = m.GetRegistrationByUser(_eventId, UserInfo.UserId);
                    if (reg == null) // No registration, create new one
                    {
                        RegistrationFormView.ChangeMode(FormViewMode.Insert);
                        _regId = 0;
                    }
                    else if (!reg.IsComplete) // Registration exists and it not complete, put it edit mode
                    {
                        RegistrationFormView.ChangeMode(FormViewMode.Edit);
                        _regId = reg.Id;
                    }
                    else // Registration exists and is complete, navigate to details page
                    {
                        //Response.Redirect("~/Facility/RegDetails.aspx?Id=" + _regId.ToString());
                        RegistrationFormView.ChangeMode(FormViewMode.ReadOnly);
                        _regId = reg.Id;
                    }
                }
                else if (Request.QueryString["RegId"] != null && Request.QueryString["EventId"] == null)
                {
                    _regId = Convert.ToInt32(Request.QueryString["RegId"]);
                    RegistrationFormView.ChangeMode(FormViewMode.Edit);
                    Registration reg = m.GetRegistration(_regId);
                    if (reg != null)
                    {
                        _eventId = reg.EventId;
                    }

                }
                else if (Request.QueryString["RegId"] == null && Request.QueryString["EventId"] == null)
                {
                    Response.Redirect("~/Facility/Dashboard.aspx");
                }
                else
                {
                    _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                    _regId = Convert.ToInt32(Request.QueryString["RegId"]);
                    _userId = UserInfo.UserId;
                }

                // Always start at first tab (verify contact)
                Wizard1.ActiveStepIndex = 0;
            }
            else
            {
                _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                _regId = Convert.ToInt32(Request.QueryString["RegId"]);
                _userId = UserInfo.UserId;
            }
      
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            AttendeesRadGrid.DataBind();
        }

        private void LoadDeleteAttendeesDropDown()
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a.OrganizationId != null)
            {
                AttendeeRadComboBox.Items.Clear();
                AttendeeRadComboBox.Items.Add(new RadComboBoxItem("", ""));
                AttendeeRadComboBox.DataSource = am.GetAttendeesByOrganization((int)a.OrganizationId);
                AttendeeRadComboBox.DataBind();

            }
            
        }

        protected void AttendeeDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["userId"] = UserInfo.UserId;
        }

        protected void OrganizationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a != null)
                if (a.OrganizationId != null)
                    e.InputParameters["orgId"] = a.OrganizationId;
        }

        protected void RegistrationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (_regId == null || _regId == 0)
            {
                e.InputParameters["registrationId"] = null;
                if (RegistrationFormView.CurrentMode != FormViewMode.Insert)
                    RegistrationFormView.ChangeMode(FormViewMode.Insert);
            }
            else
            {
                e.InputParameters["registrationId"] = _regId;
            } 
        }

        protected void EventDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (_eventId != null)
                e.InputParameters["eventId"] = _eventId;
        }

        protected void AttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a.OrganizationId != null)
                e.InputParameters["orgId"] = a.OrganizationId;
        }

        protected void UserFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            { 
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                am.SaveAllObjectChanges();

                UserFormView.DataBind();
                UserInfoUpdatePanel.Update();
            }
        }

        protected void OrganizationFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                am.SaveAllObjectChanges();

                OrganizationFormView.DataBind();
            }
        }

        protected void AttendeesRadGrid_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                ////the dropdown list will be the first control in the Controls collection of the corresponding cell
                //RadTextBox list = (e.Item as GridEditableItem)["FullName"].Controls[0] as RadTextBox;

                ////attach SelectedIndexChanged event for the combobox control
                //list.AutoPostBack = true;
                //list.TextChanged += new RadTextBoxTextChangedEventHandler(list_SelectedIndexChanged);
            }

        }

        protected void CheckedChanged(object sender, System.EventArgs e)
        {
            CheckBox chkBox = (sender as CheckBox);
            Panel myPanel = chkBox.Parent as Panel;
            GridDataItem dataItem = myPanel.NamingContainer as GridDataItem;

            int attendeeId = Convert.ToInt32(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["Id"]);

            // Add the attendee to the registration
            //RegistrationMethods m = new RegistrationMethods();
            //Registration reg = m.GetRegistration(_regId);

            //finally assign a CSS class to the table cell depending on the item type (item, alternatingItem)
            if (chkBox.Checked)
            {
                if (dataItem.ItemType == GridItemType.Item)
                {
                    dataItem["FullName"].CssClass = "itemHighlight";
                }
                else //Alternating item
                {
                    dataItem["FullName"].CssClass = "altItemHighlight";
                }

                RegistrationAttendeesMethods m2 = new RegistrationAttendeesMethods();
                RegistrationAttendee ra = new RegistrationAttendee();
                ra.AttendeeId = attendeeId;
                ra.EntryDateTime = DateTime.Now;
                ra.Title = dataItem["Title"].Text;
                ra.RegistrationId = _regId;
                m2.Add(ra);
                m2.SaveAllObjectChanges();

                AttendeesRadGrid.DataBind();
                RegistrationFormView.DataBind();
            }
            else
            {
                dataItem["FullName"].CssClass = ""; //Remove the CSS class when unchecked

                RegistrationAttendeesMethods m2 = new RegistrationAttendeesMethods();
                RegistrationAttendee ra = m2.GetRegistrationAttendeeForEvent(_eventId, attendeeId);
                m2.Delete(ra);
                m2.SaveAllObjectChanges();

                AttendeesRadGrid.DataBind();
                RegistrationFormView.DataBind();
            }

            // Add up the total number of checked items
            //int checkedCount = 0;
            //foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
            //{
            //    CheckBox chk = (CheckBox)r.FindControl("SelectedAttendeeCheckBox"); 
            //    if (chk.Checked == true)
            //        checkedCount++;
            //}
            //SelectedAttendeesCount.Text = checkedCount.ToString();
        }

        protected void AttendeesRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            int attendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
            if (e.CommandName == "Delete")
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = am.GetAttendee(attendeeId);
                a.OrganizationId = null;
                am.SaveAllObjectChanges();

                int[] checkedAttendees = GetCheckedAttendees();
                AttendeesRadGrid.DataBind();
                CheckAttendees(checkedAttendees);
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {

        }


        private int[] GetCheckedAttendees()
        {
            int attendeeCount = 0;
            int[] checkedAttendees = new int[AttendeesRadGrid.Items.Count];
            foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
            {
                CheckBox chk = (CheckBox)r.FindControl("SelectedAttendeeCheckBox");
                if (chk.Checked == true)
                {
                    checkedAttendees[attendeeCount] = (int)r.OwnerTableView.DataKeyValues[r.ItemIndex]["Id"];
                    attendeeCount++;
                }
            }
            return checkedAttendees;
        }

        private void CheckAttendees(int[] checkedAttendees)
        {
            for (int i = 0; i < checkedAttendees.Count(); i++)
            {
                foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
                {
                    if ((int)r.OwnerTableView.DataKeyValues[r.ItemIndex]["Id"] == checkedAttendees[i])
                    {
                        CheckBox chk = (CheckBox)r.FindControl("SelectedAttendeeCheckBox");
                        chk.Checked = true;
                    }
                }
            }
        }

        protected void DeleteAttendeeButton_Click(object sender, EventArgs e)
        {
            if (AttendeeRadComboBox.SelectedIndex > 0)
            { 
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = am.GetAttendee(Convert.ToInt32(AttendeeRadComboBox.SelectedValue));
                a.OrganizationId = null;
                am.SaveAllObjectChanges();

                LoadDeleteAttendeesDropDown();
                int[] checkedAttendees = GetCheckedAttendees();
                AttendeesRadGrid.DataBind();
                CheckAttendees(checkedAttendees);
            }
           
        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (e.CurrentStepIndex == 0 && e.NextStepIndex == 1 && RegistrationFormView.CurrentMode == FormViewMode.Insert)
            { 
                int? orgId = null;
                AttendeeMethods m = new AttendeeMethods();
                Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);
                if (att != null && att.Organization != null)
                {
                    orgId = att.OrganizationId;
                }

                // If user gets beyond step 1 (verifying contact info), insert a new registration for them.
                Registration reg = new Registration();
                reg.EnteredByUserId = UserInfo.UserId;
                reg.EntryDateTime = DateTime.Now;
                reg.EventId = _eventId;
                reg.IsComplete = false;
                reg.IsWebRegistration = true;
                reg.OrganizationId = orgId;
                reg.Status = null;

                RegistrationMethods m2 = new RegistrationMethods();
                m2.Add(reg);
                m2.SaveAllObjectChanges();
            }
        }

        protected void RegistrationFormView_DataBound(object sender, EventArgs e)
        {
            Registration reg = (Registration)RegistrationFormView.DataItem;
            if (reg != null)
            {
                RegistrationMethods m = new RegistrationMethods();
                Label attCountLabel = (Label)RegistrationFormView.FindControl("AttendeeCountLabel");
                attCountLabel.Text = reg.NumberOfRegistrants.ToString();

                Label regCostLabel = (Label)RegistrationFormView.FindControl("RegCostLabel");
                regCostLabel.Text = String.Format("{0:C}", m.GetCalculatedChargeTotal(reg.Id));

                Label orgNameLabel = (Label)RegistrationFormView.FindControl("OrgNameLabel");
                if (reg.OrganizationId != null)
                {
                    orgNameLabel.Text = reg.Organization.OrgName;
                }
                else
                {
                    orgNameLabel.Text = "N/A";
                }

            }
            else
            {
                AttendeeMethods m = new AttendeeMethods();
                Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);
                Label orgNameLabel = (Label)RegistrationFormView.FindControl("OrgNameLabel");
                if (att != null && att.Organization != null)
                {
                    orgNameLabel.Text = att.Organization.OrgName;
                }
                else
                {
                    orgNameLabel.Text = "N/A";    
                }
            }

        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {

        }

        protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
        {
            if (Wizard1.ActiveStepIndex == 1) // Add attendees
            {
                int? orgId = null;
                orgId = GetOrgForRegistration();

                AttendeesRadGrid.DataBind();
                LoadDeleteAttendeesDropDown();

                // Set the click link for "Add Attendee" button
                NewAttendeeRadButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}', '{2}');", "0", orgId, -1);
            }
            else if (Wizard1.ActiveStepIndex == 2) // Add Flags
            {
                RegisteredAttendeesListView.DataBind();
            }
            else if (Wizard1.ActiveStepIndex == 3) // Review
            {
                ReviewAttendeesRadListView.DataBind(); // List of attendees
                RadGrid1.Rebind();  // Charge summary
            }
        }

        private int? GetOrgForRegistration()
        {
            AttendeeMethods m = new AttendeeMethods();
            Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);
            if (att != null && att.Organization != null)
            {
                return Convert.ToInt32(att.OrganizationId);
            }
            else
            {
                return null;
            }
        }

        protected void AttendeesRadGrid_DataBound(object sender, EventArgs e)
        {
            // Get all registered attendees and check them in the grid
            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            List<RegistrationAttendee> atts = m.GetRegisteredAttendeesForEvent(_eventId);

            int attendeeId;
            foreach (GridDataItem item in AttendeesRadGrid.MasterTableView.Items)
            {
                attendeeId = Convert.ToInt32(item.OwnerTableView.DataKeyValues[item.ItemIndex]["Id"]);
                if (atts.Find(a => a.AttendeeId == attendeeId) != null)
                {
                    item.Selected = true;
                    CheckBox selectedCheckBox = (CheckBox)item.FindControl("SelectedAttendeeCheckBox");
                    if (selectedCheckBox != null)
                        selectedCheckBox.Checked = true;
                }
            }

            SelectedAttendeesCount.Text = AttendeesRadGrid.SelectedItems.Count.ToString();

        }

        private void AttendeesRadGrid_ItemPreRender(object sender, EventArgs e)
        {
            ((sender as GridDataItem)["SelectAttendeeTemplateColumn"].FindControl("SelectedAttendeeCheckBox") as CheckBox).Checked = (sender as GridDataItem).Selected;
        }

        protected void RegisteredAttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["registrationId"] = _regId;
        }

        protected void EventFlagsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["eventId"] = _eventId;
        }

        protected void RegisteredAttendeesListView_ItemDataBound(object sender, RadListViewItemEventArgs e)
        {
            // Get all flags assigned to attendee
            if (e.Item is RadListViewDataItem)
            {
                EventFlagMethods m = new EventFlagMethods();
                RadListViewDataItem item = (RadListViewDataItem)e.Item;

                // Load all flags in list
                CheckBoxList attFlagsList = (CheckBoxList)item.FindControl("AttendeeFlagsCheckBoxList");
                attFlagsList.DataSource = m.GetFlagsByEvent(_eventId);
                attFlagsList.DataBind();
                // Now check all that are assigned to user
                int regAttendeeId = Convert.ToInt32(item.GetDataKeyValue("Id"));
                RegistrationAttendeeFlags m2 = new RegistrationAttendeeFlags();
                List<RegistrationAttendeeFlag> attFlags = m2.GetRegisteredAttendeeFlags(regAttendeeId);
                foreach (RegistrationAttendeeFlag f in attFlags)
                {
                    
                    ListItem chkBoxItem = attFlagsList.Items.FindByValue(f.EventFlagId.ToString());
                    chkBoxItem.Selected = true;

                }
             
            }
        }

      
        protected void AttendeeFlagsCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckBoxList chkList = (CheckBoxList)sender;

            // Find the parent list view item (RegistrationAttendeeId)
            RadListViewDataItem regAttendeeItem = (RadListViewDataItem)((CheckBoxList)sender).NamingContainer;
            int regAttendeeId = Convert.ToInt32(regAttendeeItem.GetDataKeyValue("Id"));

            // Delete all flags for the attendee, and then re-insert checked ones
            RegistrationAttendeeFlags m2 = new RegistrationAttendeeFlags();
            m2.DeleteAllFlags(regAttendeeId);

            foreach (ListItem chkItem in chkList.Items)
            { 
                if (chkItem.Selected == true)
                {
                    RegistrationAttendeeFlag attFlag = new RegistrationAttendeeFlag();
                    attFlag.EventFlagId = Convert.ToInt32(chkItem.Value);
                    attFlag.RegistrationAttendeeId = regAttendeeId;
                    m2.Add(attFlag);
                }
            }
            

            m2.SaveAllObjectChanges();

            // Refresh the registration totals at top
            RegistrationFormView.DataBind();
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RegistrationMethods m = new RegistrationMethods();
            List<RegistrationCharge> charges = m.GetCalculatedCharges(_regId);

            RegistrationChargeMethods m2 = new RegistrationChargeMethods();
            charges.AddRange(m2.GetChargesByRegistration(_regId));

            RadGrid1.DataSource = charges;
        }

    }
}