﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManager.Web.Facility.Model
{
    public class Registration
    {
        public int RegistrationId;
        public DateTime RegistrationDate;
        public Guid RegisteredByUserId;
        public string RegisteredByFullName;


        public string AuthorizeNetOrderNumber;


    }

    public class RegistrationCharge
    {
        public string Desc;
        public int Qty;
        public decimal Price;

    }
}