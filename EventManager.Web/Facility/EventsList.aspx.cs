﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Model;
using EventManager.Business;
using Telerik.Web.UI.Calendar.View;
using Telerik.Web.UI.Calendar;
using System.Web.Security;

namespace EventManager.Web.Facility
{
    public partial class EventsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // RadScheduler1.OnClientAppointmentClick = "onAppointmentClick";

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //if (!IsPostBack)
               // RadScheduler1.DataSource = GetUpcomingEvents();
            
        }

        private List<Event> GetUpcomingEvents()
        {
            int associationId = UserInfo.AssociationId;
            Guid userId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            List<EventManager.Model.Event> events;

            // Bind to month calendar
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            events = em.GetUpcomingEvents(associationId, userId);

            // Fill multi view calendar with special days
            //foreach (EventManager.Model.Event e in events)
            //{ 
            //    Telerik.Web.UI.RadCalendarDay specialDay = new Telerik.Web.UI.RadCalendarDay();
            //    specialDay.Date = e.StartDateTime;
            //    specialDay.IsSelectable = true;
            //    specialDay.ToolTip = e.EventName;
            //    RadCalendar1.SpecialDays.Add(specialDay);
            //}

            return events;
        }

        
        protected void RadCalendar1_PreRender(object sender, System.EventArgs e)
        {
           // foreach (CalendarView view in RadCalendar1.CalendarView.ChildViews)
           // {
           //     ((MonthView)view).TitleFormat = "MMM yy";
           // }
        }

        private bool DateIsEventDate(DateTime calendarDay)
        {
            int associationId = UserInfo.AssociationId;

            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            if (em.GetEventsByDate(associationId, calendarDay).Count > 0)
                return true;
            else
                return false;
        }

        protected void CustomizeDay(object sender, Telerik.Web.UI.Calendar.DayRenderEventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime CurrentDate = e.Day.Date;
                if (DateIsEventDate(CurrentDate))
                {
                    TableCell currentCell = e.Cell;
                    currentCell.Style["background-color"] = "Orange";
                    currentCell.Style["text-align"] = "center";

                    currentCell.Text = " E ";
                }
            }
        }

        protected void SelectedDateChange(object sender, SelectedDatesEventArgs e)
        {
            //string date = e.SelectedDates.Count - 1 >= 0 ? e.SelectedDates[e.SelectedDates.Count - 1].Date.ToString() : "none";
            DateTime selectedDate = new DateTime();
            if (e.SelectedDates.Count > 0)
            {
                selectedDate = e.SelectedDates[0].Date;

                int associationId = UserInfo.AssociationId;

                EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
                //EventsRadGrid.DataSource = em.GetEventsByDate(associationId, selectedDate);
                EventsRadGrid.DataSource = em.GetEventsByDate(associationId, new DateTime(2012, 1, 13));
                EventsRadGrid.DataBind();
               // UpdatePanel2.Update();
            }
        }

        protected void EventsRadGrid_RowCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            // Get the row
            int eventId = 70;
            //int eventId = Convert.ToInt32(EventsRadGrid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);

            if (e.CommandName == "EventDetails")
            {
                Response.Redirect("~/Facility/EventDetails.aspx?EventId=" + eventId.ToString());
            }
        }

    }
}