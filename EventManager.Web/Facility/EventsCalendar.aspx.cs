﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Model;
using EventManager.Business;
using Telerik.Web.UI.Calendar.View;
using Telerik.Web.UI.Calendar;
using System.Web.Security;

namespace EventManager.Web.Facility
{
    public partial class EventsCalendarForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RadScheduler1.OnClientAppointmentClick = "onAppointmentClick";

            if (!IsPostBack)
            {
                LoadEventsList();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //if (!IsPostBack)
            List<EventManager.Model.Event> events = GetUpcomingEvents();

            RadScheduler1.DataSource = events;
            RadScheduler1.DataBind();

            //EventsRadGrid.DataSource = GetEventsByMonth();
            //EventsRadGrid.DataBind();
            //UpdatePanel2.Update();

        }

        private List<Event> GetUpcomingEvents()
        {
            Guid userId = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            int associationId = UserInfo.AssociationId;
            List<EventManager.Model.Event> events;

            // Bind to month calendar
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            events = em.GetUpcomingEvents(associationId, userId);

            return events;
        }

        private List<Event> GetEventsByMonth(DateTime dayInMonth)
        {
            int associationId = UserInfo.AssociationId;
            List<EventManager.Model.Event> events;

            // Bind to month calendar
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            events = em.GetEventsByMonth(associationId, dayInMonth, UserInfo.UserId);

            return events;
        }

        protected void EventsRadGrid_RowCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            // Get the row
            int eventId = 70;
            //int eventId = Convert.ToInt32(EventsRadGrid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value);

            if (e.CommandName == "EventDetails")
            {
                Response.Redirect("~/Facility/EventDetails.aspx?EventId=" + eventId.ToString());
            }
        }

        protected void RadScheduler1_NavigationComplete(object sender, Telerik.Web.UI.SchedulerNavigationCompleteEventArgs e)
        {
            if (e.Command == Telerik.Web.UI.SchedulerNavigationCommand.NavigateToNextPeriod || e.Command == Telerik.Web.UI.SchedulerNavigationCommand.NavigateToPreviousPeriod || e.Command == Telerik.Web.UI.SchedulerNavigationCommand.NavigateToSelectedDate)
            {
                LoadEventsList();
            }
        }

        private void LoadEventsList()
        {
            // Grad a date from the middle of the month
            if (RadScheduler1.SelectedView == Telerik.Web.UI.SchedulerViewType.MonthView)
            {
                DateTime firstVisibleDay = RadScheduler1.VisibleRangeStart;
                DateTime middleOfMonthDay = firstVisibleDay.AddDays(15);
                EventsRadGrid.DataSource = GetEventsByMonth(middleOfMonthDay);

                // Set label for selected month
                MonthLabel.Text = "Events for " + middleOfMonthDay.ToString("MMMM") + ", " + middleOfMonthDay.Year.ToString();
                EventsRadGrid.DataBind();
                UpdatePanel2.Update();
            }
        }
    }
}