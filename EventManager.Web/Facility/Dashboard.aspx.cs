﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;
using Telerik.Web.UI;

namespace EventManager.Web.Facility
{
    public partial class FacilityDashboardForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StaffDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            AttendeeMethods m = new AttendeeMethods();
            Attendee a = m.GetAttendeeByUserId(userId);
            
            if (a != null)
            {
                e.InputParameters["orgId"] = a.OrganizationId;
            }
            
        }

        protected void UpcomingEventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void RegistrationsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["UserId"] = UserInfo.UserId;
        }

        protected void RegistrationsRadGrid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;
                int regId = Convert.ToInt32(dataBoundItem.GetDataKeyValue("Id"));
                decimal chargeTotal = 0;
                decimal paymentTotal = 0;
                decimal balance = 0;

                RegistrationMethods m = new RegistrationMethods();
                chargeTotal = m.GetCalculatedChargeTotal(regId) + m.GetChargeTotal(regId);
                paymentTotal = m.GetPaymentTotal(regId);
                balance = chargeTotal - paymentTotal;

                LinkButton editBtn = (LinkButton)dataBoundItem.FindControl("TotalChargesLinkButton");
                editBtn.Text = String.Format("{0:C}", chargeTotal);

                dataBoundItem["BalanceGridBoundColumn"].Text = String.Format("{0:C}", balance);

                //Check the formatting condition
                if (balance != 0)
                {
                    dataBoundItem["BalanceGridBoundColumn"].BackColor = System.Drawing.Color.Red;
                    dataBoundItem["BalanceGridBoundColumn"].ForeColor = System.Drawing.Color.White;
                }

                Registration reg = m.GetRegistration(regId);
                if (reg != null)
                {
                    RadButton editButton = (RadButton)dataBoundItem.FindControl("EditRegRadButton");
                    RadButton viewButton = (RadButton)dataBoundItem.FindControl("ViewRegRadButton");
                    Label note = (Label)dataBoundItem.FindControl("StatusNote");
                    if (reg.IsComplete)
                    {
                        note.Text = "Your registration is complete";
                        note.ForeColor = System.Drawing.Color.Green;
                        editButton.Visible = false;
                        viewButton.Visible = true;
                        viewButton.PostBackUrl = "~/Facility/EventRegistration.aspx?RegId=" + regId.ToString();
                    }
                    else
                    {
                        note.Text = "Your registration is INCOMPLETE";
                        note.ForeColor = System.Drawing.Color.Red;
                        editButton.Visible = true;
                        viewButton.Visible = false;
                        editButton.PostBackUrl = "~/Facility/RegWiz.aspx?RegId=" + regId.ToString();
                    }
                }
            }
        }

        protected void RegNowButton_Click(object sender, EventArgs e)
        {
            Button regButton = (Button)sender;
            RadListViewDataItem item = (RadListViewDataItem)regButton.Parent;

            string eventId = item.GetDataKeyValue("Id").ToString();
            Response.Redirect("~/Facility/RegWiz.aspx?EventId=" + eventId.ToString());
        }
    }
}