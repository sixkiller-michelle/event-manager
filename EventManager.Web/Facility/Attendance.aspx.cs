﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility
{
    public partial class AttendanceForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AttendanceDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            AttendeeMethods am = new AttendeeMethods();
            Attendee att = am.GetAttendeeByUserId(UserInfo.UserId);

            if (att != null)
                e.InputParameters["attendeeId"] = att.Id;
        }
    }
}