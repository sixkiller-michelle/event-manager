﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web.Facility
{
    public partial class FacilityMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public Guid UserId
        {
            get
            {
                Guid g = new Guid();
                if (Membership.GetUser() != null)
                {
                    g = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                    return g;
                }
                return g;
            }
        }

        public int AssociationId
        {
            get
            {
                EventManager.Business.UserAssociationMethods uam = new EventManager.Business.UserAssociationMethods();
                return uam.GetAssociationIdForUser(this.UserId);
            }
        }
    }
}