﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="EventManager.Web.Facility.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<div id="mainleft">
		<div id="content" class="box">
			<h2 class="page-title">Registration Wizard</h2>
			<div class="breadcrumb"><a href="#" id="home">Home</a>  »  <a href="#">Registration</a>  »  <a href="#">Wizard</a></div>
			<p>
            
            </p>
		</div><!-- end #content -->
	</div><!-- end #mainleft -->
	<div id="mainright">
		<div class="box">
		<ul>
			<li class="widget-container">
            <h3>Registration in</h3>
            <h2 class="widget-title">3 Easy Steps</h2>
			<ul>
				<li><a href="#">Verify your contact information</a></li>
				<li><a href="#">Select attendees</a></li>
				<li><a href="#">Select add-ons</a></li>
				<li><a href="#">Complete Payment</a></li>
			</ul>
			</li>
		</ul>
		</div>
	</div><!-- end #mainright -->
	<div class="clear"></div><!-- clear float -->

</asp:Content>
