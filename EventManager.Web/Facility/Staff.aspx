﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.Master" AutoEventWireup="true" CodeBehind="Staff.aspx.cs" Inherits="EventManager.Web.Facility.StaffForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Staff Members
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:Panel runat="server" ID="StaffPanel" style="margin:10px;">
            <asp:Label ID="OrgNameLabel" runat="server" Text="" />
            <telerik:RadListView ID="StaffRadListView" runat="server" DataSourceID="StaffDataSource">
                <LayoutTemplate>
                    <div class="RadListView RadListView_Default">
                        <div ID="itemPlaceholder" runat="server">
                        </div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="rlvI staffTable">
                        <table>
                        <tr>
                            <td><b><asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' /></b></td>
                            <td><b><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>' /></b></td>
                        </tr>
                        <tr>
                            <td colspan="2"><i><asp:Label ID="Label2" runat="server" Text='<%# Eval("Title") %>' /></i></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Email") %>' /></td>
                        </tr>
                        </table>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div class="rlvA staffTable">
                        <table>
                        <tr>
                            <td><b><asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' /></b></td>
                            <td><b><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>' /></b></td>
                        </tr>
                        <tr>
                            <td colspan="2"><i><asp:Label ID="Label2" runat="server" Text='<%# Eval("Title") %>' /></i></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Email") %>' /></td>
                        </tr>
                        </table>
                    </div>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div class="RadListView RadListView_Default">
                        <div class="rlvEmpty">
                            There are no staff members.</div>
                    </div>
                </EmptyDataTemplate>
            </telerik:RadListView>
            </asp:Panel>

     <asp:ObjectDataSource ID="StaffDataSource" runat="server" 
        DataObjectTypeName="EventManager.Model.Attendee" DeleteMethod="Delete" 
        InsertMethod="Add" onselecting="StaffDataSource_Selecting" 
        SelectMethod="GetAttendeesByOrganization" 
        TypeName="EventManager.Business.AttendeeMethods" UpdateMethod="Update">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
    </asp:ObjectDataSource>


</asp:Content>
