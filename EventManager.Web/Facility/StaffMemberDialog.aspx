﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffMemberDialog.aspx.cs" Inherits="EventManager.Web.Facility.StaffMemberDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Attendee</title>
    <link href="site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    //<![CDATA[
        //function CloseAndRebind(action, attendeeId, fullNameWithId, orgId, orgName, title) {
        function CloseAndRebind(action, attendeeId, fullName, orgId, orgName, title) {

            var oWindow = GetRadWindow();
            if (action == "insert") {
                //oWindow.BrowserWindow.AttendeeAdded(attendeeId, fullName, orgId, orgName, title);
                oWindow.BrowserWindow.AttendeeAdded(attendeeId, fullName, orgId, orgName, title);
            }
            else if (action != "cancel") {
                //oWindow.BrowserWindow.AttendeeUpdated(attendeeId, fullNameWithId, orgId, orgName, title);
                oWindow.BrowserWindow.AttendeeUpdated(attendeeId, fullName, orgId, orgName, title);
            }
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CloseWindow() {
            GetRadWindow().close();
        }

        function CloseButton_Clicked(sender, args) {
            CloseWindow();
        }

        function CancelRadButton_Clicked(sender, args) {
            CloseWindow();
        }
        //]]>
    </script>
    <style type="text/css">
        .style1
        {
            width: 296px;
        }
        html, body, form
        {
            margin: 0px;
            padding: 0;
            height: 100%;
            background: #f2f2de;
        }
        
        table td {white-space:nowrap; padding:3px;}
        input[readonly] {border:none; background-color:Gray;}
        
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
    <div>
        <telerik:radscriptmanager ID="Radscriptmanager1" runat="server" />
        <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server"  />

        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="errorMessage" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>

        <asp:FormView ID="FormView1" runat="server" DataSourceID="AttendeeDataSource" 
            Width="98%" onitemcommand="FormView1_ItemCommand" DataKeyNames="Id" DefaultMode="Edit"
            oniteminserted="FormView1_ItemInserted" CssClass="AttendeeForm"
            onitemupdated="FormView1_ItemUpdated" 
            oniteminserting="FormView1_ItemInserting"  >
            <EditItemTemplate>
                <table class="formview-table-layout" style="float:left; margin-right:20px;">
                    <tr>
                        <td colspan="2">
                        <telerik:RadButton ID="UpdateButton1" runat="server" Text="Save &amp; Close" CausesValidation="True"  CommandName="Update" />&nbsp;
                        <telerik:radbutton runat="server" ID="UpdateCancelRadButton" text="Cancel" width="80" onclientclicked="CancelRadButton_Clicked"></telerik:radbutton>
                        </td>
                    </tr>
                   <tr>
                        <td colspan="2"><h1 style="margin:3px;"><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullNameFNF") %>' ViewStateMode="Disabled"></asp:Label></h1></td>
                    </tr>
                    <tr>
                        <td>First Name:</td>
                        <td><telerik:RadTextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                    </tr>
                     <tr>
                        <td>Last Name:</td>
                        <td> <telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' /></td>
                    </tr>
                    <tr>
                        <td>MI:</td>
                        <td><telerik:RadTextBox ID="MiddleInitialTextBox" runat="server" Text='<%# Bind("MiddleInitial") %>' /></td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                   <%-- <tr>
                        <td colspan="2">
                        <telerik:RadButton ID="UpdateButton2" runat="server" Text="Update" CausesValidation="True"  CommandName="Update" />&nbsp;
                        <telerik:RadButton ID="UpdateCancelButton2" runat="server" Text="Cancel" CausesValidation="True" CommandName="Cancel" />
                        </td>
                    </tr>--%>
                </table>
                <table class="formview-table-layout"  style="float:left; margin-top:20px;">
                    <tr>
                        <td colspan="2"><h3>Address</h3></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><telerik:RadTextBox ID="FullAddressTextBox" runat="server" Text='<%# Bind("Address") %>' Wrap="true" Rows="3" Width="200" /></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><telerik:RadTextBox ID="TextBox3" runat="server" Text='<%# Bind("City") %>' /></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" AppendDataBoundItems="true" 
                                DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="300px" Width="125px" 
                                ondatabound="StateRadComboBox_DataBound">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="" Value="" />
                                </Items>
                            </telerik:RadComboBox></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td><telerik:RadTextBox ID="TextBox14" runat="server" Text='<%# Bind("Zip") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Contact</h3></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><telerik:RadTextBox ID="EmailTextBox" runat="server" 
                                Text='<%# Bind("Email") %>' Width="200px" /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><telerik:RadTextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                

            </EditItemTemplate>
            <InsertItemTemplate>

                <table class="formview-table-layout" style="float:left; margin:10px; margin-right:20px;">
                    <tr>
                        <td colspan="2"> 
                            <telerik:radbutton runat="server" ID="InsertButton1" CausesValidation="True" CommandName="Insert" Text="Insert" />&nbsp;
                            <telerik:radbutton runat="server" ID="InsertCancelButton1" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h1 style="margin:2px;"><asp:Label ID="Label1" runat="server" Text='New Attendee' ViewStateMode="Disabled"></asp:Label></h1></td>
                    </tr>
                   
                    <tr>
                        <td>First Name:</td>
                        <td >
                            <telerik:RadTextBox ID="FirstNameTextBox" runat="server" 
                                Text='<%# Bind("FirstName") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:</td>
                        <td >
                            <telerik:RadTextBox ID="LastNameTextBox" runat="server" 
                                Text='<%# Bind("LastName") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Middle Initial:</td>
                        <td>
                            <telerik:RadTextBox ID="MiddleInitialTextBox" runat="server" 
                                Text='<%# Bind("MiddleInitial") %>' Width="30px" />
                        </td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td>
                            <telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                     <tr>
                        <td colspan="2">
                            <telerik:radbutton runat="server" ID="InsertButton2" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />&nbsp;
                            <telerik:radbutton runat="server" ID="InsertCancelButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                       </td>
                    </tr>
                </table>
                <br />
                <table class="formview-table-layout" style="float:left; margin-top:55px;">
                    <tr>
                        <td>
                            Address:</td>
                        <td>
                            <telerik:RadTextBox ID="AddressTextBox" runat="server" Height="47px" Width="150" 
                                Text='<%# Bind("Address") %>' TextMode="MultiLine"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            City:</td>
                        <td>
                            <telerik:RadTextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            State:</td>
                        <td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode"
                                DropDownWidth="200">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Zip:</td>
                        <td>
                            <telerik:RadTextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' Width="100" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Email:</td>
                        <td>
                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' 
                                Width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone Number:</td>
                        <td>
                            <asp:TextBox ID="PhoneNumberTextBox" runat="server" 
                                Text='<%# Bind("PhoneNumber") %>' />
                        </td>
                    </tr>
                </table>
                <br />
                <div style="clear:both;"></div>

                
            </InsertItemTemplate>
            <ItemTemplate>
                <table style="width: 100%;" class="formview-table-layout">
                    <tr>
                        <td colspan="2"><h1 style="margin:2px;"><asp:Label ID="Label1" runat="server" Text='<%# Bind("FullNameFNF") %>'></asp:Label></h1></td>
                    </tr>
                    <tr>
                        <td>Id:</td>
                        <td><telerik:RadTextBox ID="IdRadTextBox" runat="server" Text='<%# Bind("Id") %>' ReadOnly="true" BorderStyle="None" Width="50" /></td>
                    </tr>
                    <tr>
                        <td>Facility:</td>
                        <td><telerik:RadTextBox ID="OrgRadTextBox" runat="server" Text='<%# Bind("Organization.OrgName") %>' ReadOnly="true" BorderStyle="None" Width="200" />
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' ReadOnly="true" Width="200" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Contact</h3></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td><telerik:RadTextBox Width="195px" ID="EmailRadTextBox" runat="server" Label="" Text='<%# Bind("Email") %>'
                            EmptyMessage="" InvalidStyleDuration="100" AutoPostBack="false" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td><telerik:RadTextBox Width="195px" ID="PhoneRadTextBox" runat="server" Label="" Text='<%# Bind("PhoneNumber") %>'
                            EmptyMessage="" InvalidStyleDuration="100" AutoPostBack="false" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td><telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind("FullAddress") %>' 
                            Height="50" Rows="3" TextMode="MultiLine" ReadOnly="true">
                        </telerik:RadTextBox></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <br />

            </ItemTemplate>
        </asp:FormView>

        <asp:ObjectDataSource ID="AttendeeDataSource" runat="server" 
            SelectMethod="GetAttendee" 
            TypeName="EventManager.Business.AttendeeMethods" 
            DataObjectTypeName="EventManager.Model.Attendee" InsertMethod="AddAttendee" 
            UpdateMethod="Update" oninserted="AttendeeDataSource_Inserted" 
            oninserting="AttendeeDataSource_Inserting">
            <SelectParameters>
                <asp:QueryStringParameter Name="attendeeId" QueryStringField="AttendeeId" 
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

         <asp:ObjectDataSource ID="StatesDataSource" runat="server" 
            SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
        </asp:ObjectDataSource>


    </div>
    </form>
</body>
</html>
