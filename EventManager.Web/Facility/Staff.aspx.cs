﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility
{
    public partial class StaffForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StaffDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            AttendeeMethods m = new AttendeeMethods();
            Attendee a = m.GetAttendeeByUserId(userId);

            if (a != null)
            {
                e.InputParameters["orgId"] = a.OrganizationId;
            }

        }
    }
}