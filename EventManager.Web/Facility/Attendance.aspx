﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.Master" AutoEventWireup="true" CodeBehind="Attendance.aspx.cs" Inherits="EventManager.Web.Facility.AttendanceForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Attendance History
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Panel runat="server" ID="MainPanel" style="padding:10px;">

    <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" 
        CellSpacing="0" DataSourceID="AttendanceDataSource" GridLines="None">
        <MasterTableView DataSourceID="AttendanceDataSource">
            <Columns>
                <telerik:GridTemplateColumn DataField="EventStartDate"
                    FilterControlAltText="Filter EventStartDate column" HeaderText="Event" 
                    SortExpression="EventStartDate" UniqueName="EventGridTemplateColumn">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="EventDateLabel" Text='<%# Eval("EventStartDate", "{0:d}") %>'></asp:Label><br />
                        <asp:Label runat="server" ID="Label1" Text='<%# Eval("EventName") %>' Font-Size="Large"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                
                <telerik:GridTemplateColumn DataField="OrgName"
                    FilterControlAltText="Filter EventStartDate column" HeaderText="Your Credentials" 
                    SortExpression="EventStartDate" UniqueName="OrgNameGridTemplateColumn">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="OrgNameLabel" Text='<%# Eval("OrgName") %>'></asp:Label><br />
                        <asp:Label runat="server" ID="TitleLabel" Text='<%# Eval("Title") %>' Font-Italic="true"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="CeuTotal" DataType="System.Decimal" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"  ItemStyle-Width="80" HeaderStyle-Width="80"
                    FilterControlAltText="Filter CeuTotal column" HeaderText="CEUs" ItemStyle-Font-Size="Large" 
                    SortExpression="CeuTotal" UniqueName="CeuTotal">
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Certificate" UniqueName="CertificateGridTemplateColumn">
                    <ItemTemplate>
                        <telerik:RadButton ID="PrintCertRadButton" runat="server" Text="View Certificate" CommandName="ViewCertificate" CommandArgument='<%# Eval("EventId") %>'>
                        </telerik:RadButton>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

    <asp:ObjectDataSource ID="AttendanceDataSource" runat="server" 
        onselecting="AttendanceDataSource_Selecting" 
        SelectMethod="GetAttendanceHistory" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods">
        <SelectParameters>
            <asp:Parameter Name="attendeeId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Panel>

</asp:Content>
