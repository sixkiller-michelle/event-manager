﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility
{
    public partial class StaffMemberDialog : System.Web.UI.Page
    {
        EventManager.Model.Attendee newAttendee;
        int newAttendeeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["AttendeeId"] == null || Request.QueryString["AttendeeId"] == "0")
                {
                    FormView1.ChangeMode(FormViewMode.Insert);
                }

                if (FormView1.CurrentMode == FormViewMode.Insert)
                {
                    Telerik.Web.UI.RadTextBox firstName = (Telerik.Web.UI.RadTextBox)FormView1.FindControl("FirstNameTextBox");
                    if (firstName != null)
                        firstName.Focus();
                }
            }



        }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    AttendeeDataSource.DataBind();
                    FormView1.DataBind();
                    CloseForm("insert");
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    else
                        CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInInsertMode = true;
                }
            }
            else
            {
                if (e.Exception.InnerException != null)
                    CustomValidator1.ErrorMessage = e.Exception.InnerException.Message;
                else
                    CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                try
                {
                    am.SaveAllObjectChanges();
                    CloseForm("update");
                }
                catch (Exception ex)
                {
                    CustomValidator1.ErrorMessage = ex.Message;
                    CustomValidator1.IsValid = false;
                    e.ExceptionHandled = true;
                    e.KeepInEditMode = true;
                }
            }
            else
            {
                CustomValidator1.ErrorMessage = e.Exception.Message;
                CustomValidator1.IsValid = false;
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
        }

        private void CloseForm(string action)
        {
            EventManager.Business.AttendeeMethods m;

            // Get the new attendee ID and name
            if (action == "insert")
            {
                if (newAttendee != null)
                {
                    int AttendeeId = newAttendee.Id;
                    string AttendeeFullName = newAttendee.FullNameWithId;
                    string Title = newAttendee.Title;

                    int? OrgId = newAttendee.OrganizationId;
                    string OrgName = "";

                    // Get the org name
                    if (OrgId != null && OrgId != 0)
                    {
                        OrgName = newAttendee.Organization.OrgName;
                    }

                    // Refill the session variable to include new attendee
                    m = new EventManager.Business.AttendeeMethods();
                    Session["AttendeesList"] = m.GetAttendees(GetAssociationId());

                    ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('insert', '" + AttendeeId.ToString() + "', '" + AttendeeFullName + "', '" + OrgId.ToString() + "', '" + OrgName + "', '" + Title + "');", true);
                }
            }
            else if (action == "update")
            {
                m = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = m.GetAttendee(Convert.ToInt32(Request.QueryString["AttendeeId"]));

                int? OrgId = a.OrganizationId;
                string OrgName = "";

                // Get the org name
                if (OrgId != null && OrgId != 0)
                {
                    OrgName = a.Organization.OrgName;
                }

                if (a != null)
                {
                    //ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('update', '" + a.Id.ToString() + "', '" + a.FullName.Replace(",", "\\,") + "', '" + OrgId.ToString() + "', '" + OrgName + "', '" + a.Title + "');", true);
                    ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('update', " + a.Id.ToString() + ", '" + a.FullName + "', '" + OrgId.ToString() + "', '" + OrgName + "', '" + (String.IsNullOrEmpty(a.Title) ? "" : a.Title) + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind(0, '');", true);
            }

        }

        private int GetAssociationId()
        {
            return UserInfo.AssociationId;
        }

        protected void AttendeeDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            newAttendee = (EventManager.Model.Attendee)e.ReturnValue;
            if (newAttendee != null)
                newAttendeeId = ((EventManager.Model.Attendee)e.ReturnValue).Id;
        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind('cancel', '', '', '', '', '');", true);
            }
        }

        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["IsApproved"] = true;
            e.Values["AssociationId"] = GetAssociationId();

            // Get the org of the logged in user
            int? orgId = null;
            AttendeeMethods m = new AttendeeMethods();
            Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);

            if (att != null && att.OrganizationId != null)
            {
                orgId = att.OrganizationId;
            }
            e.Values["OrganizationId"] = orgId;

        }


        protected void StateRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (FormView1.CurrentMode == FormViewMode.Insert)
            {
                RadComboBox c = (RadComboBox)sender;
                if (c.Items.Count > 1)
                {
                    c.SelectedValue = UserInfo.DefaultStateCode;
                }
            }
        }

        protected void AttendeeDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            Attendee a = (Attendee)e.InputParameters[0];
            int? orgId = null;
            if (Request.QueryString["OrgId"] != null && Request.QueryString["OrgId"] != "" && Request.QueryString["OrgId"] != "0")
            {
                orgId = Convert.ToInt32(Request.QueryString["OrgId"]);
                OrganizationMethods m = new OrganizationMethods();
                Organization org = m.GetOrganization(Convert.ToInt32(orgId));
                if (org == null || org.AssociationId != UserInfo.AssociationId)
                { 
                    orgId = null;
                }
            }

            a.OrganizationId = orgId;
        }

        
    }
}