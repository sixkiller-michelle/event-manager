﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Facility
{
    public partial class EventDetailsForm : System.Web.UI.Page
    {
        private int _eventId;
        private Guid _userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Facility/EventsCalendar.aspx");
            }
            else
            {
                HyperLink registerHyperlink = (HyperLink)FormView1.FindControl("RegisterHyperlink");
                registerHyperlink.NavigateUrl = "EventRegistration.aspx?EventId=" + Request.QueryString["EventId"];
                _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                _userId = UserInfo.UserId;
            }

            if (!IsPostBack)
            {
                GetEventDesc();
            }
        }

        protected void GetEventDesc()
        {
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = em.GetEvent(_eventId);
            if (evt != null)
            {
                // Registration Status
                Label regStatusLabel = (Label)FormView1.FindControl("RegStatusLabel");
                Panel regUserPanel = (Panel)FormView1.FindControl("RegUserPanel");
                HyperLink registerHyperlink = (HyperLink)FormView1.FindControl("RegisterHyperlink");
                Label statusLabel = (Label)FormView1.FindControl("EventStatusLabel");

                if (evt.IsRegistrationOpen)
                {
                    statusLabel.Text = "Open";
                    statusLabel.ForeColor = System.Drawing.Color.Green;
            
                    EventManager.Business.RegistrationMethods rm = new  EventManager.Business.RegistrationMethods();
                    EventManager.Model.Registration r = rm.GetExistingRegistration(_eventId, _userId);
                    if (r != null)
                    {
                        // Fill in info on who did the registration
                        Label userIdLabel = (Label)FormView1.FindControl("EnteredByUserIdLabel");
                        Label userEmailLabel = (Label)FormView1.FindControl("EnteredByUserEmailLabel");
                        userIdLabel.Text = r.EnteredByUserId.ToString();
                        userEmailLabel.Text = r.aspnet_Users.aspnet_Membership.Email;

                        if (r.IsComplete)
                        {
                            //registerHyperlink.Enabled = false;
                            regStatusLabel.Text = "Complete";   
                        }
                        else
                        {
                            registerHyperlink.Enabled = true;
                            regStatusLabel.Text = "In-Progress";
                        }
                    }
                    else
                    {
                        registerHyperlink.Enabled = true;
                        regStatusLabel.Text = "Not Registered";
                    }
                }
                else
                {
                    statusLabel.Text = "Closed";
                    statusLabel.ForeColor = System.Drawing.Color.Red;
                    registerHyperlink.Enabled = false;
                }

                // Pay by Check
                Label payByCheckLabel = (Label)FormView1.FindControl("PayByCheckLabel");
                if (payByCheckLabel != null)
                {
                    if (evt.AllowPayByCheck)
                    {
                        payByCheckLabel.Text = "Pay-by-check allowed";
                        payByCheckLabel.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        payByCheckLabel.Text = "Pay-by-check NOT allowed";
                        payByCheckLabel.ForeColor = System.Drawing.Color.Red;
                    }
                }
                

                // Show/Hide board meeting info
                Panel boardMeetingPanel = (Panel)FormView1.FindControl("BoardMeetingPanel");
                if (boardMeetingPanel != null)
                { 
                    if (evt.BoardMeetingStartDateTime == null)
                        boardMeetingPanel.Visible = false;
                    else
                        boardMeetingPanel.Visible = true;
                }

                // Fill event description rad editor
                Telerik.Web.UI.RadEditor editor1 = (Telerik.Web.UI.RadEditor)FormView1.FindControl("EventDescRadEditor");
                if (editor1 != null)
                {
                    editor1.Content = evt.EventDesc;
                }
            }
        }
    }
}