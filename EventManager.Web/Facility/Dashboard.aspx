﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="EventManager.Web.Facility.FacilityDashboardForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <style type="text/css">
h2 {margin-bottom:0px; padding-bottom:0px;}
p {margin-top: 0px;}
p.instruct {font-size:12px; }
.label {white-space:nowrap; font-style:italic; width:80px;}
.value {width:250px;}
.eventName {}
.eventName h3 {margin-top:3px; margin-bottom:3px;}
.eventName h3.eventDate {margin-top:10px;}
.eventName h3.eventName {color:#00529e; font-size:20px; margin-bottom:10px;}
.eventDetailsTable {width:95%;}
.eventDescCell {padding:5px; vertical-align:top;}
.staffTable {}
.staffTable td {padding:0px;}
.RegButton {height:50px; width:120px; background-color: #ed9d47; color: White;}
.RegButton:hover {height:50px; width:120px; background-color: #ed9d47; color: Black; cursor: hand;}
</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
    Dashboard
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<telerik:RadScriptBlock ID="RadSCriptBlock1" runat="server">
<script type="text/javascript">
function RadSplitter2_OnClientLoad(splitter, args) {

    // Set height of the second (inner) splitter
    var pane1 = splitter.getPaneById('<%= LeftRadPane.ClientID %>');
    var pane2 = splitter.getPaneById('<%= RightRadPane.ClientID %>');
    var height1 = pane1.getContentElement().scrollHeight;
    var height2 = pane2.getContentElement().scrollHeight;
    if (height1 > height2) {
        splitter.set_height(height1);
        pane1.set_height(height1);
    }
    else {
        splitter.set_height(height2 + 50);
        pane2.set_height(height2 + 50);
    }

    // Now set height of outer splitter
    var splitter2 = $find("<%= RadSplitter1.ClientID %>");
    var bottomPane = splitter2.getPaneById('<%= BottomRadPane.ClientID %>');
    var bottomPaneHeight = bottomPane.getContentElement().scrollHeight;
    bottomPane.set_height(bottomPaneHeight);

}
</script>
    
</telerik:RadScriptBlock>

    <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal" Width="100%" OnClientLoad="RadSplitter2_OnClientLoad">

    <telerik:RadPane ID="ProfileRadPane" runat="server" Height="200" Visible="false">



    </telerik:RadPane>

    <telerik:RadPane ID="ManagedRegistrationsRadPane" runat="server" Height="200" Visible="false">
        <asp:Panel runat="server" ID="RegistrationsPanel" style="margin:10px;">
            <h2>Your Registrations</h2>
            <p class="instruct">The following registration(s) are ones that YOU created and manage. To see all past and future events that you are registered for, see "Your Attendance History" below  </p>
            <telerik:RadGrid ID="RegistrationsRadGrid" runat="server"
                AutoGenerateColumns="False" CellSpacing="0"
                DataSourceID="RegistrationsDataSource" GridLines="None" 
                onitemdatabound="RegistrationsRadGrid_ItemDataBound">
                <MasterTableView DataSourceID="RegistrationsDataSource" DataKeyNames="Id" >
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <Columns>

                        <telerik:GridTemplateColumn UniqueName="RegStatusTemplateColumn" HeaderText="Status" HeaderStyle-HorizontalAlign="Left" 
                            HeaderStyle-Width="80px" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Right" DataType="System.Decimal" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="StatusNote"></asp:Label>&nbsp;
                                <telerik:RadButton runat="server" ID="EditRegRadButton" Text="Complete Registration"></telerik:RadButton>
                                <telerik:RadButton runat="server" ID="ViewRegRadButton" Text="View Registration Summary"></telerik:RadButton>
                            </ItemTemplate>
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" />
                        </telerik:GridTemplateColumn>

                         <telerik:GridBoundColumn DataField="Event.StartDateTime" DataType="System.DateTime" UniqueName="EventStartDateGridBoundColumn" 
                            ItemStyle-Width="80" HeaderText="Date" HeaderStyle-Width="80">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Event.EventName" DataType="System.Int32" UniqueName="EventNameGridBoundColumn" HeaderText="Event" ItemStyle-Width="250">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="NumberOfRegistrants" DataType="System.Int32" UniqueName="NumberOfRegistrantsGridBoundColumn" ItemStyle-Width="80" HeaderText="Attendees" HeaderStyle-Width="80">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn UniqueName="TotalChargesTemplateColumn" DataField="ChargeTotal" HeaderText="Charges" HeaderStyle-HorizontalAlign="Left" 
                        AllowFiltering="true" FilterControlWidth="30px" AutoPostBackOnFilter="true" CurrentFilterFunction="GreaterThan" ShowFilterIcon="true"
                        HeaderStyle-Width="80px" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Right" DataType="System.Decimal" 
                        SortExpression="ChargeTotal" >
                        <ItemTemplate>
                            <asp:LinkButton ID="TotalChargesLinkButton" runat="server" Text='' CommandName="ViewCharges" />
                        </ItemTemplate>
                        <HeaderStyle Width="80px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridTemplateColumn>

                     <telerik:GridTemplateColumn UniqueName="TotalPaymentsTemplateColumn" DataField="PaymentTotal" HeaderText="Payments" HeaderStyle-HorizontalAlign="Left" 
                            HeaderStyle-Width="80px" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Right" DataType="System.Decimal" 
                            SortExpression="PaymentTotal">
                            <ItemTemplate>
                                <asp:LinkButton ID="TotalPaymentsLinkButton" runat="server" Text='<%# Eval("PaymentTotal", "{0:c}") %>' CommandName="ViewCharges" />
                            </ItemTemplate>
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" />
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn DataField="PaymentType" 
                            FilterControlAltText="Filter PaymentType column" HeaderStyle-Width="70" AllowFiltering="false" 
                            HeaderText="Payment Type" ReadOnly="True" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="70"
                            SortExpression="PaymentType" UniqueName="PaymentTypeGridBoundColumn">
                        </telerik:GridBoundColumn> 

                         <telerik:GridBoundColumn DataField="PaymentNumber" AllowFiltering="false" 
                            FilterControlAltText="Filter PaymentNumber column" HeaderStyle-Width="60" 
                            HeaderText="#" ReadOnly="True" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="60"
                            SortExpression="PaymentNumber" UniqueName="PaymentNumberGridBoundColumn">
                        </telerik:GridBoundColumn> 
                    
                        <telerik:GridBoundColumn DataField="Balance" CurrentFilterFunction="GreaterThan" FilterControlWidth="30"
                            FilterControlAltText="Filter Balance column" DataFormatString="{0:C}" HeaderStyle-Width="80" 
                            HeaderText="Balance" ReadOnly="True" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80"
                            SortExpression="Balance" UniqueName="BalanceGridBoundColumn">
                        </telerik:GridBoundColumn> 

                         

                    </Columns>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </asp:Panel>
    </telerik:RadPane>
        
    <telerik:RadPane ID="BottomRadPane" runat="server" Scrolling="None">
    
        <telerik:RadSplitter ID="RadSplitter2" runat="server" Orientation="Vertical" OnClientLoad="RadSplitter2_OnClientLoad">
        <telerik:RadPane ID="LeftRadPane" runat="server" Width="300">
            
            <asp:Panel runat="server" ID="StaffPanel" style="margin:10px;">
            <h2>Staff Members</h2>
            <asp:Label ID="OrgNameLabel" runat="server" Text="" />
            <p class="instruct">You can add, edit, or remove staff members <asp:HyperLink ID="EditStaffHyperlink" runat="server" NavigateUrl="~/Facility/Staff.aspx">here</asp:HyperLink></p>
            <%--<telerik:RadListView ID="StaffRadListView" runat="server" DataSourceID="StaffDataSource">
                <LayoutTemplate>
                    <div class="RadListView RadListView_Default">
                        <div ID="itemPlaceholder" runat="server">
                        </div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="rlvI staffTable">
                        <table>
                        <tr>
                            <td><b><asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' /></b></td>
                            <td><b><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>' /></b></td>
                        </tr>
                        <tr>
                            <td colspan="2"><i><asp:Label ID="Label2" runat="server" Text='<%# Eval("Title") %>' /></i></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Email") %>' /></td>
                        </tr>
                        </table>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div class="rlvA staffTable">
                        <table>
                        <tr>
                            <td><b><asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' /></b></td>
                            <td><b><asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>' /></b></td>
                        </tr>
                        <tr>
                            <td colspan="2"><i><asp:Label ID="Label2" runat="server" Text='<%# Eval("Title") %>' /></i></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Email") %>' /></td>
                        </tr>
                        </table>
                    </div>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div class="RadListView RadListView_Default">
                        <div class="rlvEmpty">
                            There are no staff members.</div>
                    </div>
                </EmptyDataTemplate>
            </telerik:RadListView>--%>
            </asp:Panel>

        </telerik:RadPane>
        <telerik:RadSplitBar ID="SplitterBar1" runat="server"></telerik:RadSplitBar>
        <telerik:RadPane ID="RightRadPane" runat="server">
    
            <asp:Panel runat="server" ID="EventsPanel" style="margin:10px;">
            <h2>Upcoming Events</h2>
            <telerik:RadListView ID="EventsRadListView" runat="server" DataKeyNames="Id" 
                DataSourceID="UpcomingEventsDataSource">
                <LayoutTemplate>
                    <div class="RadListView RadListView_Sitefinity">
                        <div ID="itemPlaceholder" runat="server">
                        </div>
                        <div style="display:none">
                            <telerik:RadCalendar ID="rlvSharedCalendar" runat="server" 
                                RangeMinDate="<%#new DateTime(1900, 1, 1) %>" Skin="<%#Container.Skin %>" />
                        </div>
                        <div style="display:none">
                            <telerik:RadTimeView ID="rlvSharedTimeView" runat="server" 
                                Skin="<%# Container.Skin %>" />
                        </div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="rlvI">
                        <div class="eventName">
                            <h3 class="eventDate"><asp:Label ID="Label4" runat="server" Text='<%# Eval("StartDateTime", "{0:D}") %>' /></h3>
                            <h3 class="eventName"><asp:Label ID="Label6" runat="server" Text='<%# Eval("EventName") %>' /></h3>
                        </div>
                        <table class="eventDetailsTable">
                        <tr>
                            <td class="label">Event Type:</td>
                            <td class="value"><asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("EventType.TypeName") %>' /></td>
                            <td rowspan="4" class="eventDescCell"><asp:Label ID="EventDescLabel" runat="server" Text='<%# Eval("ShortDesc") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Location:</td>
                            <td class="value"><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Dates:</td>
                            <td class="value"><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Eval("StartDateTime", "{0:g}") %>' />-
                                <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Eval("EndDateTime", "{0:g}") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" Checked='<%# Eval("IsRegistrationOpen") %>' Text="Registration is open" Enabled="false" />
                                <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" Checked='<%# Eval("AllowPayByCheck") %>' Text="Payment by check accepted" Enabled="false" />
                                <asp:Button ID="RegNowButton" runat="server" CssClass="RegButton" 
                                    Text="Register Now!" onclick="RegNowButton_Click" />
                            </td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div class="rlvA">
                        <div class="eventName">
                            <h3 class="eventDate"><asp:Label ID="Label4" runat="server" Text='<%# Eval("StartDateTime", "{0:D}") %>' /></h3>
                            <h3 class="eventName"><asp:Label ID="Label6" runat="server" Text='<%# Eval("EventName") %>' /></h3>
                        </div>
                        <table class="eventDetailsTable">
                        <tr>
                            <td class="label">Event Type:</td>
                            <td class="value"><asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("EventType.TypeName") %>' /></td>
                            <td rowspan="4" class="eventDescCell"><asp:Label ID="EventDescLabel" runat="server" Text='<%# Eval("ShortDesc") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Location:</td>
                            <td class="value"><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Dates:</td>
                            <td class="value"><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Eval("StartDateTime", "{0:g}") %>' />-
                                <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Eval("EndDateTime", "{0:g}") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" Checked='<%# Eval("IsRegistrationOpen") %>' Text="Registration is open" Enabled="false" />
                                <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" Checked='<%# Eval("AllowPayByCheck") %>' Text="Payment by check accepted" Enabled="false" />
                                <asp:Button ID="RegNowButton" runat="server" Text="Register Now!" CssClass="RegButton" PostBackUrl="~/Facility/RegWiz.aspx" />
                                </td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </div>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div class="RadListView RadListView_Sitefinity">
                        <div class="rlvEmpty">
                            There are no upcoming events.</div>
                    </div>
                </EmptyDataTemplate>
            </telerik:RadListView>
            </asp:Panel>
        </telerik:RadPane>
        </telerik:RadSplitter>

    </telerik:RadPane>
    </telerik:RadSplitter>

    <asp:ObjectDataSource ID="AttendeeDataSource" runat="server"></asp:ObjectDataSource>

<asp:ObjectDataSource ID="UpcomingEventsDataSource" runat="server" 
        onselecting="UpcomingEventsDataSource_Selecting" 
        SelectMethod="GetUpcomingEvents" TypeName="EventManager.Business.EventMethods">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="RegistrationsDataSource" runat="server" 
        onselecting="RegistrationsDataSource_Selecting" 
        SelectMethod="GetActiveRegistrationsByUser" 
        TypeName="EventManager.Business.RegistrationMethods">
        <SelectParameters>
            <asp:Parameter DbType="Guid" Name="UserId" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
