﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.Master" AutoEventWireup="true" CodeBehind="RegDetails.aspx.cs" Inherits="EventManager.Web.Facility.RegDetailsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
    Registration Summary
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:FormView ID="RegistrationFormView" runat="server" 
        DataSourceID="RegistrationDataSource">
        <ItemTemplate>
            Id:
            <asp:Label ID="IdLabel" runat="server" Text='<%# Bind("Id") %>' />
            <br />
            Event:
            <asp:Label ID="EventIdLabel" runat="server" Text='<%# Bind("Event.EventName") %>' />
            <br />
            Facility:
            <asp:Label ID="OrganizationIdLabel" runat="server" 
                Text='<%# Bind("Organization.OrgName") %>' />
            <br />
            
            
            Notes:
            <asp:Label ID="NotesLabel" runat="server" Text='<%# Bind("Notes") %>' />
            <br />
            Boot Request:
            <asp:Label ID="BoothNumberRequestLabel" runat="server" 
                Text='<%# Bind("BoothNumberRequest") %>' />
            <br />
            Booth:
            <asp:Label ID="BoothNumberLabel" runat="server" 
                Text='<%# Bind("BoothNumber") %>' />
            <br />
            Door Prize:
            <asp:Label ID="DoorPrizeLabel" runat="server" Text='<%# Bind("DoorPrize") %>' />
            <br />
            
            EnteredByUserId:
            <asp:Label ID="EnteredByUserIdLabel" runat="server" 
                Text='<%# Bind("EnteredByUserId") %>' />
            <br />
            EntryDateTime:
            <asp:Label ID="EntryDateTimeLabel" runat="server" 
                Text='<%# Bind("EntryDateTime") %>' />
            <br />
         
            Complete?:
            <asp:CheckBox ID="IsCompleteCheckBox" runat="server" 
                Checked='<%# Bind("IsComplete") %>' Enabled="false" />
            <br />
            Status:
            <asp:Label ID="StatusLabel" runat="server" Text='<%# Bind("Status") %>' />
            <br />
            TotalPrice:
            <asp:Label ID="TotalPriceLabel" runat="server" 
                Text='<%# Bind("TotalPrice") %>' />
            <br />
            Order Number:
            <asp:Label ID="OrderNumberLabel" runat="server" 
                Text='<%# Bind("OrderNumber") %>' />
            <br />
        </ItemTemplate>

    </asp:FormView>

<asp:ObjectDataSource ID="RegistrationDataSource" runat="server" 
    SelectMethod="GetRegistration" onselecting="RegistrationDataSource_Selecting"
    TypeName="EventManager.Business.RegistrationMethods">
    <SelectParameters>
            <asp:Parameter Name="registrationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>
