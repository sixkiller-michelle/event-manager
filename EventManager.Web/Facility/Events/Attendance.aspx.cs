﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;
using System.IO;

namespace EventManager.Web.Facility.Events
{
    public partial class Attendance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AttendanceDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            AttendeeMethods am = new AttendeeMethods();
            Attendee att = am.GetAttendeeByUserId(UserInfo.UserId);

            if (att != null)
                e.InputParameters["attendeeId"] = att.Id;
        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ViewCertificate")
            {
                int assnId = UserInfo.AssociationId;
                int eventId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EventId"]);
                int attendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AttendeeId"]);
                string certificateReportName = "";

                EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
                if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForEvent(eventId))))
                    certificateReportName = m.GetCertificateFileNameForEvent(eventId);
                else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForAssociation(assnId))))
                    certificateReportName = m.GetCertificateFileNameForAssociation(assnId);
                else
                    certificateReportName = m.GetCertificateFileNameForApplication();

                if (certificateReportName != "")
                {
                    Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=" + certificateReportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId);
                }
            }
        }
    }
}