﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace EventManager.Web.Facility.Events
{
    public partial class EventList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        protected void UpcomingEventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
            e.InputParameters["userId"] = UserInfo.UserId;
        }

       
        protected void EventsRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Register")
            { 
                Response.Redirect("~/Facility/Events/Register/ContactInfo.aspx?EventId=" + e.CommandArgument.ToString());
            }
        }
    }
}