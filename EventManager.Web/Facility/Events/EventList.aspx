﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.Master" AutoEventWireup="true" CodeBehind="EventList.aspx.cs" Inherits="EventManager.Web.Facility.Events.EventList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <style type="text/css">
h2 {margin-bottom:0px; padding-bottom:0px;}
p {margin-top: 0px;}
p.instruct {font-size:12px; }
.label {white-space:nowrap; font-style:italic; width:80px;}
.value {}
.eventName {}
.eventName h3 {margin-top:3px; margin-bottom:3px;}
.eventName h3.eventDate {margin-top:10px;}
.eventName h3.eventName {color:#00529e; font-size:20px; margin-bottom:10px;}
.eventDetailsTable {border-collapse:collapse; border:none;}
.eventDetailsTable td { border:none !important; padding:0;}
.eventDescCell {padding:5px; vertical-align:top;}
.staffTable {}
.staffTable td {padding:0px;}
.RegButton {height:50px; width:120px; background-color: #ed9d47; color: White;}
.RegButton:hover {height:50px; width:120px; background-color: #ed9d47; color: Black; cursor: hand;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
    Upcoming Events
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Panel runat="server" ID="MainPanel" style="padding:10px;">

    <telerik:RadGrid ID="EventsRadGrid" runat="server" AutoGenerateColumns="False"
        CellSpacing="0" DataSourceID="UpcomingEventsDataSource" GridLines="None" 
        onitemcommand="EventsRadGrid_ItemCommand">
        <MasterTableView DataSourceID="UpcomingEventsDataSource">
            <Columns>
                <telerik:GridTemplateColumn UniqueName="EventDetailsTemplateColumn" HeaderText="Event Details">
                    <ItemTemplate>
                        <table class="eventDetailsTable" border="0">
                        <tr>
                            <td  colspan="2">
                                <div class="eventName">
                                    <h3 class="eventDate"><asp:Label ID="Label2" runat="server" Text='<%# Eval("StartDateTime", "{0:D}") %>' /></h3>
                                    <h3 class="eventName"><asp:Label ID="Label1" runat="server" Text='<%# Eval("EventName") %>' /></h3>
                                </div>
                             </td>
                        </tr>
                        <tr>
                            <td class="label">Event Type:</td>
                            <td class="value"><asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("EventType.TypeName") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Location:</td>
                            <td class="value"><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Dates:</td>
                            <td class="value"><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Eval("StartDateTime", "{0:g}") %>' />-
                                <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Eval("EndDateTime", "{0:g}") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="EventNameLabel" runat="server" Text='<%# Eval("ShortDesc") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Label ID="EventVisibilityAssociationLabel" runat="server" Text="This event is only visible to association employees" Visible='<%# (string)Eval("Visibility") == "Association" ? true : false %>' ForeColor="Red" />
                            <asp:Label ID="EventVisibilityCreatorLabel" runat="server" Text="This event is only visible to you"  Visible='<%# (string)Eval("Visibility") == "Creator" ? true : false %>' ForeColor="Red" /></td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <%--<telerik:GridBoundColumn DataField="ShortDesc" DataType="System.String" UniqueName="ShortDescGridBoundColumn" HeaderText="Description" ItemStyle-Width="250">
                </telerik:GridBoundColumn>--%>
                <telerik:GridTemplateColumn UniqueName="EventDetailsTemplateColumn" HeaderText="Registration" HeaderStyle-Width="300px" ItemStyle-Width="300">
                    <ItemTemplate>
                        <table class="eventDetailsTable" border="0">
                        <tr>
                            <td colspan="2">
                            <ul>
                                <li><asp:Label  ID="IsRegistrationOpenLabel1" runat="server" Text="Registration is OPEN" Visible='<%# (bool)Eval("IsRegistrationOpen") %>' />
                                <asp:Label  ID="IsRegistrationOpenLabel2" runat="server" Text="Registration is CLOSED" Visible='<%# !(bool)Eval("IsRegistrationOpen") %>' ForeColor="Red" /></li>
                                 <li><asp:Label  ID="AllowPayByCheckLabel1" runat="server" Visible='<%# (bool)Eval("AllowPayByCheck") %>' Text="Payment by check accepted" />
                                <asp:Label  ID="AllowPayByCheckLabel2" runat="server" Visible='<%# !(bool)Eval("AllowPayByCheck") %>' Text="Payment by check NOT accepted" ForeColor="Red" /> </li>
                             </ul>   <br />
                             <asp:Button ID="RegNowButton" runat="server" CssClass="RegButton" 
                                    Text="Register Now!" CommandName="Register" CommandArgument='<%# Eval("Id") %>' Visible='<%# (bool)Eval("IsRegistrationOpen") %>' />
                            </td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <NoRecordsTemplate>
              <div>
                There are no upcoming events to display.  Please check back later!  </div>
            </NoRecordsTemplate>
        </MasterTableView>
    </telerik:RadGrid>
<%--
 <telerik:RadListView ID="EventsRadListView" runat="server" DataKeyNames="Id" 
                DataSourceID="UpcomingEventsDataSource">
                <LayoutTemplate>
                    <div>
                        <div ID="itemPlaceholder" runat="server">
                        </div>
                        <div style="display:none">
                            <telerik:RadCalendar ID="rlvSharedCalendar" runat="server" 
                                RangeMinDate="<%#new DateTime(1900, 1, 1) %>" />
                        </div>
                        <div style="display:none">
                            <telerik:RadTimeView ID="rlvSharedTimeView" runat="server"  />
                        </div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div>
                        <div class="eventName">
                            <h3 class="eventDate"><asp:Label ID="Label4" runat="server" Text='<%# Eval("StartDateTime", "{0:D}") %>' /></h3>
                            <h3 class="eventName"><asp:Label ID="Label6" runat="server" Text='<%# Eval("EventName") %>' /></h3>
                        </div>
                        <table class="eventDetailsTable">
                        <tr>
                            <td class="label">Event Type:</td>
                            <td class="value"><asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("EventType.TypeName") %>' /></td>
                            <td rowspan="4" class="eventDescCell"><asp:Label ID="EventDescLabel" runat="server" Text='<%# Eval("ShortDesc") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Location:</td>
                            <td class="value"><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Dates:</td>
                            <td class="value"><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Eval("StartDateTime", "{0:g}") %>' />-
                                <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Eval("EndDateTime", "{0:g}") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" Checked='<%# Eval("IsRegistrationOpen") %>' Text="Registration is open" Enabled="false" />
                                <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" Checked='<%# Eval("AllowPayByCheck") %>' Text="Payment by check accepted" Enabled="false" />
                                <asp:Button ID="RegNowButton" runat="server" CssClass="RegButton" 
                                    Text="Register Now!" onclick="RegNowButton_Click" />
                            </td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div>
                        <div class="eventName">
                            <h3 class="eventDate"><asp:Label ID="Label4" runat="server" Text='<%# Eval("StartDateTime", "{0:D}") %>' /></h3>
                            <h3 class="eventName"><asp:Label ID="Label6" runat="server" Text='<%# Eval("EventName") %>' /></h3>
                        </div>
                        <table class="eventDetailsTable">
                        <tr>
                            <td class="label">Event Type:</td>
                            <td class="value"><asp:Label ID="TypeNameLabel" runat="server" Text='<%# Eval("EventType.TypeName") %>' /></td>
                            <td rowspan="4" class="eventDescCell"><asp:Label ID="EventDescLabel" runat="server" Text='<%# Eval("ShortDesc") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Location:</td>
                            <td class="value"><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Dates:</td>
                            <td class="value"><asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Eval("StartDateTime", "{0:g}") %>' />-
                                <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Eval("EndDateTime", "{0:g}") %>' /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" Checked='<%# Eval("IsRegistrationOpen") %>' Text="Registration is open" Enabled="false" />
                                <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" Checked='<%# Eval("AllowPayByCheck") %>' Text="Payment by check accepted" Enabled="false" />
                                <asp:Button ID="RegNowButton" runat="server" Text="Register Now!" CssClass="RegButton" PostBackUrl="~/Facility/RegWiz.aspx" />
                                </td>
                        </tr>
                        </table>
                        <div style="clear:both;"></div>
                    </div>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div>
                        <div>
                            There are no upcoming events.</div>
                    </div>
                </EmptyDataTemplate>
            </telerik:RadListView>--%>

    <asp:ObjectDataSource ID="UpcomingEventsDataSource" runat="server" 
        onselecting="UpcomingEventsDataSource_Selecting" 
        SelectMethod="GetUpcomingEvents" 
        TypeName="EventManager.Business.EventMethods" 
        OldValuesParameterFormatString="original_{0}">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
        <asp:Parameter DbType="Guid" Name="userId" />
    </SelectParameters>
    </asp:ObjectDataSource>

</asp:Panel>

</asp:Content>
