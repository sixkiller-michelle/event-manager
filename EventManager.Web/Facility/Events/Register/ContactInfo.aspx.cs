﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;
using Telerik.Web.UI;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class ContactInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        protected void UserFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                am.SaveAllObjectChanges();
                UserFormView.DataBind();
            }
        }

        protected void OrganizationFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                EventManager.Business.OrganizationMethods am = new EventManager.Business.OrganizationMethods();
                am.SaveAllObjectChanges();

                OrganizationFormView.DataBind();
            }
        }

        protected void AttendeeDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["userId"] = UserInfo.UserId;
        }

        protected void OrganizationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a != null)
                if (a.OrganizationId != null)
                    e.InputParameters["orgId"] = a.OrganizationId;
        }


        private void UpdateAttendeeInfo()
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            AttendeeMethods m = new AttendeeMethods();
            EventManager.Model.Attendee a = m.GetAttendeeByUserId(UserInfo.UserId);
            a.FirstName = ((RadTextBox)UserFormView.FindControl("FirstNameTextBox")).Text;
            a.LastName = ((RadTextBox)UserFormView.FindControl("LastNameTextBox")).Text;
            a.MiddleInitial = ((RadTextBox)UserFormView.FindControl("MiddleInitialTextBox")).Text;
            a.Email = ((RadTextBox)UserFormView.FindControl("EmailTextBox")).Text;
            a.PhoneNumber = ((RadTextBox)UserFormView.FindControl("PhoneNumberTextBox")).Text;
            m.Update(a);
            m.SaveAllObjectChanges();
            UserFormView.ChangeMode(FormViewMode.ReadOnly);
        }

        private void UpdateOrgInfo()
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;
            int? orgId = 0;

            // Find org ID (if exists)
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);
            if (a != null)
                if (a.OrganizationId != null)
                    orgId = a.OrganizationId;

            if (orgId != null)
            {
                OrganizationMethods m = new OrganizationMethods();
                EventManager.Model.Organization org = m.GetOrganization(Convert.ToInt32(orgId));
                org.Address = ((RadTextBox)OrganizationFormView.FindControl("StreetTextBox")).Text;
                org.City = ((RadTextBox)OrganizationFormView.FindControl("CityTextBox")).Text;
                org.StateCode = ((RadComboBox)OrganizationFormView.FindControl("StateRadComboBox")).SelectedValue;
                org.Zip = ((RadTextBox)OrganizationFormView.FindControl("ZipTextBox")).Text;
                org.Phone = ((RadTextBox)OrganizationFormView.FindControl("OrgPhoneTextBox")).Text;
                org.Fax = ((RadTextBox)OrganizationFormView.FindControl("OrgFaxTextBox")).Text;
                m.Update(org);
                m.SaveAllObjectChanges();
                OrganizationFormView.ChangeMode(FormViewMode.ReadOnly);
            }
            //Response.Redirect("~Facility/Events/Register/Attendees.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void UserFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
           
        }

        protected void UserFormView_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "UpdateAttendeeInfo")
            {
                UpdateAttendeeInfo();
            }
        }

        protected void OrganizationFormView_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "UpdateFacilityInfo")
            {
                UpdateOrgInfo();
            }
        }

        protected void OrganizationFormView_ModeChanged(object sender, EventArgs e)
        {
            //if (OrganizationFormView.CurrentMode == FormViewMode.Edit)
            //{
            //    UserFormView.ChangeMode(FormViewMode.Edit);
            //}
            //else if (OrganizationFormView.CurrentMode == FormViewMode.ReadOnly)
            //{
            //    UserFormView.ChangeMode(FormViewMode.ReadOnly);
            //}
        }

        protected void NextRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            if (OrganizationFormView.CurrentMode == FormViewMode.Edit)
            {
                UpdateAttendeeInfo();
                UpdateOrgInfo();
            }
            Response.Redirect("~/Facility/Events/Register/Attendees.aspx?eventid=" + eventId + "&regid=" + regId);
        }
    }
}