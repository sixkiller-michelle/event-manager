﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class ErrorForm : System.Web.UI.Page
    {
        private List<Error> errors;

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeErrors();
            if (Request.QueryString["code"] != null)
            {
                ErrorMessageLabel.Text = errors.Where(r => r.ErrorCode.ToLower() == Request.QueryString["code"].ToLower()).FirstOrDefault().ErrorMessage;
            }
        }

        private void InitializeErrors()
        { 
            errors = new List<Error>();
            errors.Add(new Error { ErrorCode = "NoEvent", ErrorMessage = "You must select an event for which to register. " });
            errors.Add(new Error { ErrorCode = "ClosedReg", ErrorMessage = "Registration is currently closed for this event. " });

        }
    }

    public class Error
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}