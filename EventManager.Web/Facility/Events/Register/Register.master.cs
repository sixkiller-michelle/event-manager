﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Model;
using EventManager.Business;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class RegisterMaster : System.Web.UI.MasterPage
    {
        private int _eventId = 0;
        private int _regId = 0;
        private Guid _userId = UserInfo.UserId;

        public int EventId
        {
            get { return _eventId; }
        }

        public int RegId
        {
            get { return _regId; }
        }

        public Registration Registration
        {
            get { //return (Registration)RegistrationFormView.DataItem; 
                RegistrationMethods m = new RegistrationMethods();
                return m.GetRegistration(_regId);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            RegistrationMethods m = new RegistrationMethods();
            Registration reg = null;

            if (Request.QueryString["RegId"] != null && Int32.TryParse(Request.QueryString["RegId"], out _regId) && _regId > 0)
            {
                reg = m.GetRegistration(_regId);
                if (reg != null && reg.Event.AssociationId == UserInfo.AssociationId)
                {
                    _eventId = reg.EventId;
                    if (reg.IsComplete && !Request.ServerVariables["Path_Info"].Contains("Register/Summary.aspx"))
                    {
                        Response.Redirect("~/Facility/Events/Register/Summary.aspx?eventid=" + _eventId + "&regid=" + _regId);
                    }
                }
                else
                {
                    _eventId = 0;
                    _regId = 0;
                    Response.Redirect("~/Facility/Events/Register/Error.aspx?code=NoEvent");
                }
            }
            else if (Request.QueryString["EventId"] != null && Int32.TryParse(Request.QueryString["EventId"], out _eventId))
            {
                // See if we have a valid event and that the association matches the user's association
                EventMethods em = new EventMethods();
                Event evt = em.GetEvent(_eventId);
                if (evt != null && evt.AssociationId == UserInfo.AssociationId)
                {
                    reg = m.GetRegistrationByUser(_eventId, UserInfo.UserId);
                    if (reg != null)
                    {
                        _regId = reg.Id;
                        if (reg.IsComplete && !Request.ServerVariables["Path_Info"].Contains("Register/Summary.aspx"))
                        {
                            Response.Redirect("~/Facility/Events/Register/Summary.aspx?eventid=" + _eventId + "&regid=" + _regId);
                        }
                    }
                    else if (!evt.IsRegistrationOpen)
                    {
                        Response.Redirect("~/Facility/Events/Register/Error.aspx?code=ClosedReg");
                    }
                }
                else
                {
                    _eventId = 0;
                }
            }

            if (_eventId == 0)
            {
                _regId = 0;
                Response.Redirect("~/Facility/Events/Register/Error.aspx?code=NoEvent");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RefreshRegistrationSummary(RegId);
        }

        public void RefreshRegistrationSummary(int regId)
        {
            AttendeeMethods m = new AttendeeMethods();
            Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);

            Label orgNameLabel = (Label)OrgNameLabel;
            if (orgNameLabel != null)
            { 
                if (att != null && att.Organization != null)
                {
                    orgNameLabel.Text = att.Organization.OrgName;
                }
                else
                {
                    orgNameLabel.Text = "N/A (Individual Registration)";
                }
            }
                

            RegistrationMethods rm = new RegistrationMethods();
            Registration reg = null;
            if (regId > 0)
            {
                reg = rm.GetRegistration(regId);
            }
            
            if (reg != null)
            {
                // update org for registration to be user's org (if exists)
                if (att.OrganizationId != reg.OrganizationId)
                {
                    reg.OrganizationId = att.OrganizationId;
                }

                StatusLabel.Text = (reg.IsComplete ? "Complete" : "In-Progress");

                if (!string.IsNullOrEmpty(reg.Status) && reg.Status != "Complete" && reg.Status != "In-Progress")
                {
                    StatusLabel.Text += " (" + reg.Status + ")";
                }
            }
            else
            {
                StatusLabel.Text = "New Registration";
            }
        }


        protected void RegistrationFormView_DataBound(object sender, EventArgs e)
        {
            //RefreshRegistrationSummary();
        }

        protected void RegistrationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (_regId == 0)
            {
                e.InputParameters["registrationId"] = null;
                //if (RegistrationFormView.CurrentMode != FormViewMode.Insert)
                //    RegistrationFormView.ChangeMode(FormViewMode.Insert);
            }
            else
            {
                e.InputParameters["registrationId"] = _regId;
            }
        }

        protected void EventDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (_eventId > 0)
                e.InputParameters["eventId"] = _eventId;
        }

    }
}