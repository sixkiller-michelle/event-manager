﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.ErrorForm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Registration Error
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<br />
<asp:Label ID="ErrorMessageLabel" value="" runat="server" ForeColor="Red" Font-Size="Large"></asp:Label>
<br /><br />
<asp:Hyperlink runat="server" NavigateUrl="~/Facility/Events/EventList.aspx">Return to event list</asp:Hyperlink>

</asp:Content>
