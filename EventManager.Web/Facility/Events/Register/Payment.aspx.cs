﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using AuthorizeNet;
using EventManager.Model;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class PaymentForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FillYearsCombo();
            FillCheckInfo();
        }

        private void FillYearsCombo()
        {
            int startYear = DateTime.Today.Year;
            for (var i = startYear; i < startYear + 20; i++)
            {
                ExpYearRadComboBox.Items.Add(new Telerik.Web.UI.RadComboBoxItem(i.ToString(), i.ToString().Substring(2)));
            }
        }

        private void FillCheckInfo()
        {
            EventMethods em = new EventMethods();
            EventManager.Model.Event evt = em.GetEvent(Convert.ToInt32(Request.QueryString["eventId"]));
            if (evt != null)
            {
                if (!evt.AllowPayByCheck)
                {
                    PaymentMethodRadioButtonList.Items[1].Enabled = false;
                }
            }

            AssociationMethods m = new AssociationMethods();
            EventManager.Model.Association assn = m.GetAssociation(UserInfo.AssociationId);
            if (assn != null)
            {
                CheckPayableLabel.Text = assn.AssociationName;
                MailCheckAddressTextBox.Text = assn.FullLocation;
            }
        }


        protected void PreviousRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/Review.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void PaymentMethodRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PaymentMethodRadioButtonList.SelectedValue == "Check")
            {
                CheckPanel.Visible = true;
                CreditCardPanel.Visible = false;
            }
            else
            {
                CheckPanel.Visible = false;
                CreditCardPanel.Visible = true;
            }
        }

        protected void SubmitRadButton_Click(object sender, EventArgs e)
        {
            if (PaymentMethodRadioButtonList.SelectedValue == "Credit")
            {
                string cardNumber = CardNumberRadTextBox.Text;
                string cardName = NameRadTextBox.Text;
                string cardCode = CvcRadTextBox.Text;
                string expDate = ExpMonthRadComboBox.SelectedValue + ExpYearRadComboBox.SelectedValue;
                decimal amount = Convert.ToDecimal(AmountLabel.Text);

                string apiLogin = "8B4fgQ44p9w6";
                string transKey = "85at753dz5EC2Cm4";

                //step 1 - create the request
                var request = new AuthorizationRequest(cardNumber, expDate, amount, "Test Transaction");

                //These are optional calls to the API
                request.AddCardCode(cardCode);
                request.AddLineItem("123", "Group rate", "Group rate", 1, 450, false);
                request.AddLineItem("451", "Individual rate", "Individual rate", 1, 0, false);
                request.AddLineItem("41", "Individual rate", "Individual rate", 1, 0, false);


                //Customer info - this is used for Fraud Detection
                //request.AddCustomer("id", "first", "last", "address", "state", "zip");

                //order number
                //request.AddInvoice("invoiceNumber");

                //Custom values that will be returned with the response
                //request.AddMerchantValue("merchantValue", "value");

                //Shipping Address
                //request.AddShipping("id", "first", "last", "address", "state", "zip");


                //step 2 - create the gateway, sending in your credentials and setting the Mode to Test (boolean flag)
                //which is true by default
                //this login and key are the shared dev account - you should get your own if you 
                //want to do more testing
                var gate = new Gateway(apiLogin, transKey, true);

                //step 3 - make some money
                var response = gate.Send(request);

                //Console.WriteLine("{0}: {1}", response.ResponseCode, response.Message);
                //Console.Read();

                // Update registration status to Complete
                int regId = ((RegisterMaster)Master).RegId;
                int eventId = ((RegisterMaster)Master).EventId;
                RegistrationMethods m = new RegistrationMethods();
                Registration reg = m.GetRegistration(regId);
                if (reg != null)
                {
                    reg.IsComplete = true;
                    m.SaveAllObjectChanges();
                    Response.Redirect("~/Facility/Events/Register/Complete.aspx?eventid=" + eventId + "&regid=" + regId);
                }
            }
        }

    }
}