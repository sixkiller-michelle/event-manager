﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class Attendees : System.Web.UI.Page
    {
        int? orgId = null;

        private int RegId
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["RegId"]);
            }
        }

        private int EventId
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["EventId"]);
            }
        }
            
        protected void Page_Load(object sender, EventArgs e)
        {
            orgId = GetOrgForRegistration();

            if (!IsPostBack)
            { 
                AttendeesRadGrid.DataBind();
                LoadDeleteAttendeesDropDown();
            }
            
            // Set the click link for "Add Attendee" button
            NewAttendeeRadButton.Attributes["onclick"] = String.Format("return ShowAttendeeDialog('{0}', '{1}', '{2}');", "0", orgId, -1);
        }

        private void LoadDeleteAttendeesDropDown()
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a.OrganizationId != null)
            {
                AttendeeRadComboBox.Items.Clear();
                AttendeeRadComboBox.Items.Add(new RadComboBoxItem("", ""));
                AttendeeRadComboBox.DataSource = am.GetAttendeesByOrganization((int)a.OrganizationId);
                AttendeeRadComboBox.DataBind();

            }

        }

        protected void DeleteAttendeeButton_Click(object sender, EventArgs e)
        {
            if (AttendeeRadComboBox.SelectedIndex > 0)
            {
                int eventId = ((RegisterMaster)Master).EventId;
                int regId = ((RegisterMaster)Master).RegId;

                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = am.GetAttendee(Convert.ToInt32(AttendeeRadComboBox.SelectedValue));
                
                // Remove the person from the facility
                a.OrganizationId = null;

                // Remove the person from the registration (if they are there)
                RemoveAttendeeFromRegistration(a.Id);

                am.SaveAllObjectChanges();
                LoadDeleteAttendeesDropDown();
                AttendeesRadGrid.DataBind();
            }

        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            GridItem item = ((sender as CheckBox).NamingContainer as GridItem);
            int attendeeId = (int)item.OwnerTableView.DataKeyValues[item.ItemIndex]["Id"];

            // Select/unselect row based on checkbox
           item.Selected = (sender as CheckBox).Checked;

            // Update selected attendee count
            SelectedAttendeesCount.Text = AttendeesRadGrid.SelectedItems.Count.ToString();

            if (item.Selected)
            {
                AddAttendeeToRegistration(attendeeId);
            }
            else
            { 
                RemoveAttendeeFromRegistration(attendeeId);
            }

            // Set "Check all" box in header
            bool checkHeader = true;
            foreach (GridDataItem dataItem in AttendeesRadGrid.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("CheckBox1") as CheckBox).Checked)
                {
                    checkHeader = false;
                    return;
                }
            }
            GridHeaderItem headerItem = AttendeesRadGrid.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            CheckBox headerChkbox = (CheckBox)headerItem.FindControl("headerChkbox");
            if (headerChkbox != null)
                headerChkbox.Checked = checkHeader;

            ((RegisterMaster)Master).RefreshRegistrationSummary(RegId);
        }

        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            foreach (GridDataItem dataItem in AttendeesRadGrid.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as CheckBox).Checked = headerCheckBox.Checked;
                dataItem.Selected = headerCheckBox.Checked;
            }
            SelectedAttendeesCount.Text = AttendeesRadGrid.SelectedItems.Count.ToString();
           // ((RegisterMaster)Master).RefreshRegistrationSummary(RegId);
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            // See if ID is numeric.  If so, assume attendeeId and refresh that data item
            int attendeeId;
            if (Int32.TryParse(e.Argument, out attendeeId))
            {
                //RegistrationsRadGrid.Rebind();
                //GridDataItem item = (GridDataItem)AttendeesRadGrid.MasterTableView.FindItemByKeyValue("Id", attendeeId);
                //if (item != null)
                //{
                //    RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
                //    RegAttendeeRow regAtt = m.GetRegisteredAttendeeForGrid(attendeeId);
                //    if (regAtt != null)
                //    {
                //        item.DataItem = regAtt;
                //        item.DataBind();
                //    }
                //}
            }

        }

        protected void AddAttendeeToRegistration(int attendeeId)
        {
            AttendeeMethods m = new AttendeeMethods();
            Attendee att = m.GetAttendee(attendeeId);

            if (att != null)
            { 
                int eventId = ((RegisterMaster)Master).EventId;
                int regId = ((RegisterMaster)Master).RegId;
                Registration reg = null;
                if (regId > 0)
                {
                    RegistrationMethods rm = new RegistrationMethods();
                    reg = rm.GetRegistration(regId);
                }
                

                if (reg == null)
                {
                    RegistrationMethods rm = new RegistrationMethods();
                    reg = new Registration();
                    reg.EventId = eventId;
                    reg.OrganizationId = orgId;
                    reg.EnteredByUserId = UserInfo.UserId;
                    reg.EntryDateTime = DateTime.Now;
                    reg.IsWebRegistration = true;
                    reg.IsComplete = false;
                    reg.Status = "In-Progress";
                    reg = rm.AddRegistration(reg);
                }

                RegistrationAttendeesMethods m2 = new RegistrationAttendeesMethods();
                RegistrationAttendee ra = new RegistrationAttendee();
                ra.Registration = reg;
                ra.Attendee = att;
                ra.EntryDateTime = DateTime.Now;
                ra.Title = att.Title;
                m2.Add(ra);
                m2.SaveAllObjectChanges();
            }
           
        }

        protected void RemoveAttendeeFromRegistration(int attendeeId)
        {
                int eventId = ((RegisterMaster)Master).EventId;
                int regId = ((RegisterMaster)Master).RegId;
           
                RegistrationAttendeesMethods m2 = new RegistrationAttendeesMethods();
                RegistrationAttendee ra = m2.GetRegistrationAttendeeForEvent(eventId, attendeeId);
                if (ra != null)
                { 
                    m2.Delete(ra);
                    m2.SaveAllObjectChanges();
                    //AttendeesRadGrid.DataBind();
                }
        }

        protected void AttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a.OrganizationId != null)
                e.InputParameters["orgId"] = a.OrganizationId;
        }

        protected void AttendeesRadGrid_DataBound(object sender, EventArgs e)
        {
            //int eventId = ((RegisterMaster)Master).EventId;
            //int regId = ((RegisterMaster)Master).RegId;
            int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
            int regId = Convert.ToInt32(Request.QueryString["RegId"]);

            // Get all registered attendees and check them in the grid
            RegistrationAttendeesMethods m = new RegistrationAttendeesMethods();
            List<RegistrationAttendee> atts = m.GetRegisteredAttendeesForEvent(eventId);

            int attendeeId;
            foreach (GridDataItem item in AttendeesRadGrid.MasterTableView.Items)
            {
                attendeeId = Convert.ToInt32(item.OwnerTableView.DataKeyValues[item.ItemIndex]["Id"]);
                if (atts.Find(a => a.AttendeeId == attendeeId) != null)
                {
                    item.Selected = true;
                    CheckBox selectedCheckBox = (CheckBox)item.FindControl("CheckBox1");
                    if (selectedCheckBox != null)
                        selectedCheckBox.Checked = true;
                }
            }

            SelectedAttendeesCount.Text = AttendeesRadGrid.SelectedItems.Count.ToString();
           // ((RegisterMaster)Master).RefreshRegistrationSummary(RegId);
        }

        protected void AttendeesRadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int attendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);


                EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
                EventManager.Model.Attendee a = am.GetAttendee(attendeeId);
                a.OrganizationId = null;
                am.SaveAllObjectChanges();

                //List<Attendee> checkedAttendees = GetCheckedAttendees2();
                AttendeesRadGrid.DataBind();
                //CheckAttendees2(checkedAttendees);
            }
            else if (e.CommandName == "InitInsert")
            {
                e.Canceled = true;

                // Hide all edit items
                AttendeesRadGrid.MasterTableView.ClearEditItems();

                //Prepare an IDictionary with the predefined values  
                System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                //set initial checked state for the checkbox on init insert
                //newValues["IsActive"] = true;

                //Insert the item and rebind  
                e.Item.OwnerTableView.InsertItem(newValues);

                // Set focus to first name textbox
                GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
                RadTextBox fistNameTextBox = (RadTextBox)insertedItem.FindControl("FirstNameRadTextBox");
                fistNameTextBox.Focus();

            }
            else if (e.CommandName == "PerformInsert")
            {
                
                int eventId = ((RegisterMaster)Master).EventId;
                Registration reg = ((RegisterMaster)Master).Registration;

                try
                {
                    // Create the attendee
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    Attendee att = new Attendee();
                    att.AssociationId = UserInfo.AssociationId;
                    att.OrganizationId = orgId;
                    att.FirstName = ((RadTextBox)e.Item.FindControl("FirstNameRadTextBox")).Text;
                    att.LastName = ((RadTextBox)e.Item.FindControl("LastNameRadTextBox")).Text;
                    att.Title = ((RadComboBox)e.Item.FindControl("TitleRadComboBox")).Text;
                    att.LicenseNumber = ((RadTextBox)e.Item.FindControl("LicenseNumberRadTextBox")).Text;
                    att.LicenseExpDate = ((RadDatePicker)e.Item.FindControl("ExpDateRadDatePicker")).SelectedDate;
                    att.Birthdate = ((RadDatePicker)e.Item.FindControl("BirthdateRadDatePicker")).SelectedDate;
                    att.IsApproved = true;
                    att = m.AddAttendee(att);

                    // Add the attendee to the registration
                    AddAttendeeToRegistration(att.Id);
              
                    m.SaveAllObjectChanges();
                    AttendeesRadGrid.Rebind();
                }
                catch (Exception ex)
                {
                    e.Canceled = true;
                    CustomValidator v = ((CustomValidator)e.Item.FindControl("FormTemplateCustomValidator"));
                    v.ErrorMessage = ex.Message;
                    v.IsValid = false;
                    e.Item.OwnerTableView.IsItemInserted = true;
                }
               
            }
            else if (e.CommandName == "Update")
            {
                int attendeeId = Convert.ToInt32((e.Item as GridEditableItem).GetDataKeyValue("Id"));
                EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                Attendee att = m.GetAttendee(attendeeId);

                att.FirstName = ((RadTextBox)e.Item.FindControl("FirstNameRadTextBox")).Text;
                att.LastName = ((RadTextBox)e.Item.FindControl("LastNameRadTextBox")).Text;
                att.Title = ((RadComboBox)e.Item.FindControl("TitleRadComboBox")).Text;
                att.LicenseNumber = ((RadTextBox)e.Item.FindControl("LicenseNumberRadTextBox")).Text;
                att.LicenseExpDate = ((RadDatePicker)e.Item.FindControl("ExpDateRadDatePicker")).SelectedDate;
                att.Birthdate = ((RadDatePicker)e.Item.FindControl("BirthdateRadDatePicker")).SelectedDate;
                m.Update(att);

                m.SaveAllObjectChanges();
                AttendeesRadGrid.Rebind();
            }
        }

   
        protected void AttendeesRadGrid_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridEditFormInsertItem && e.Item.IsInEditMode)
            {
                GridEditableItem insertedItem = e.Item.OwnerTableView.GetInsertItem();
                RadTextBox fistNameTextBox = (RadTextBox)insertedItem.FindControl("FirstNameRadTextBox");
                fistNameTextBox.Focus();
            }
         
        }

        private int? GetOrgForRegistration()
        {
            AttendeeMethods m = new AttendeeMethods();
            Attendee att = m.GetAttendeeByUserId(UserInfo.UserId);
            if (att != null && att.Organization != null)
            {
                return Convert.ToInt32(att.OrganizationId);
            }
            else
            {
                return null;
            }
        }


        private void CheckAttendees(int[] checkedAttendees)
        {
            for (int i = 0; i < checkedAttendees.Count(); i++)
            {
                foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
                {
                    if ((int)r.OwnerTableView.DataKeyValues[r.ItemIndex]["Id"] == checkedAttendees[i])
                    {
                        CheckBox chk = (CheckBox)r.FindControl("CheckBox1");
                        chk.Checked = true;
                    }
                }
            }
        }

        private void CheckAttendees2(List<Attendee> checkedAttendees)
        {
            //for (int i = 0; i < checkedAttendees.Count(); i++)
            //{
                foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
                {
                    // Look for the attendee in our checkedAttendees list
                    Attendee att = checkedAttendees.Find(a => a.Id == (int)r.OwnerTableView.DataKeyValues[r.ItemIndex]["Id"]);
                    if (att != null)
                    {
                        CheckBox chk = (CheckBox)r.FindControl("CheckBox1");
                        chk.Checked = true;
                    }
                }
            //}
        }

        private List<Attendee> GetCheckedAttendees2()
        {
            int attendeeCount = 0;
            List<Attendee> checkedAttendees = new List<Attendee>();
            foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.SelectedItems)
            {
                checkedAttendees.Add((Attendee)r.DataItem);
                attendeeCount++;
            }
            return checkedAttendees;
        }


        private int[] GetCheckedAttendees()
        {
            int attendeeCount = 0;
            int[] checkedAttendees = new int[AttendeesRadGrid.Items.Count];
            foreach (Telerik.Web.UI.GridDataItem r in AttendeesRadGrid.Items)
            {
                CheckBox chk = (CheckBox)r.FindControl("CheckBox1");
                if (chk.Checked == true)
                {
                    checkedAttendees[attendeeCount] = (int)r.OwnerTableView.DataKeyValues[r.ItemIndex]["Id"];
                    attendeeCount++;
                }
            }
            return checkedAttendees;
        }

        protected void NewAttendeeRadButton_Click(object sender, EventArgs e)
        {
            AttendeesRadGrid.MasterTableView.IsItemInserted = true;
            AttendeesRadGrid.Rebind();
        }

        protected void AttendeesRadGrid_ItemUpdated(object sender, GridUpdatedEventArgs e)
        {
            if (e.Exception == null)
            {
                try
                {
                    int attendeeId = Convert.ToInt32((e.Item as GridEditableItem).GetDataKeyValue("Id"));
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    Attendee att = m.GetAttendee(attendeeId);
                    e.Item.UpdateValues(att);
                    m.Update(att);

                    m.SaveAllObjectChanges();
                    AttendeesRadGrid.Rebind();

                }
                catch (Exception ex)
                {
                    //if (ex.InnerException != null)
                    //{
                    //    CustomValidator1.ErrorMessage = ex.InnerException.Message;
                    //}
                    //else
                    //{
                    //    CustomValidator1.ErrorMessage = ex.Message;
                    //}
                    //CustomValidator1.IsValid = false;
                    e.KeepInEditMode = true;
                }

            }
            else
            {
                //CustomValidator1.ErrorMessage = e.Exception.Message;
                //CustomValidator1.IsValid = false;
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
            }


        }

        protected void PreviousRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/ContactInfo.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void NextRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            // Make sure at least one attendee is selected
            //if (AttendeesRadGrid.SelectedItems.Count == 0)
            //{
            //    CustomValidator1.ErrorMessage = "Please select at least one attendee.";
            //    CustomValidator1.IsValid = false;
            //    return;
            //}

            Response.Redirect("~/Facility/Events/Register/Addons.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void TitlesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = UserInfo.AssociationId;
        }

        protected void AttendeesRadGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a.OrganizationId != null)
            {
                AttendeesRadGrid.DataSource = am.GetAttendeesByOrganization(a.OrganizationId);
            }
            else
            {
                AttendeesRadGrid.DataSource = new List<Attendee> { a };
            }
        }
    }
}