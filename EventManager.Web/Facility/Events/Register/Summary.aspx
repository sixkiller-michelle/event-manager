﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.master" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.SummaryForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<h2 style="color:green;">Your registration is complete</h2>
<br />
<p>
For upcoming events, see the <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Facility/Events/EventList.aspx"> Events List</asp:HyperLink><br />
After the event, you can view your <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Facility/Events/Attendance.aspx">Attendance History</asp:HyperLink>&nbsp;to print your certificate
</p>

</asp:Content>
