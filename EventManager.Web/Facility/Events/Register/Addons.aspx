﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.master" AutoEventWireup="true" CodeBehind="Addons.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.Addons" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">



<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step3.jpg" /><br /><br />

<p class="instruct">Select the add-ons for each attendee.</p>

<h2>There are no add-ons for this event.</h2>

<div class="wizardButtons">
<div>
<telerik:RadButton ID="PreviousRadButton" runat="server" Text="Previous" 
        CausesValidation="False"  CommandName="Previous"
        onclick="PreviousRadButton_Click" ></telerik:RadButton>&nbsp;
<telerik:RadButton ID="NextRadButton" runat="server" Text="Next"
        CausesValidation="False"  CommandName="Next" onclick="NextRadButton_Click" ></telerik:RadButton>
    </div></div>
<br /><br />

</asp:Content>
