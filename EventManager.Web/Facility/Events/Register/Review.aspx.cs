﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class ReviewForm : System.Web.UI.Page
    {
        private int? orgId;
        private int eventId;
        private int regId;
       
        protected void Page_Init(object sender, EventArgs e)
        {
            orgId = null;
            Guid userId = UserInfo.UserId;
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(userId);

            if (a != null)
                if (a.OrganizationId != null)
                    orgId = a.OrganizationId;

            regId = ((RegisterMaster)Master).RegId;
            eventId = ((RegisterMaster)Master).EventId;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void OrganizationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["orgId"] = orgId;
        }

        protected void PreviousRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/Addons.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void NextRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/Payment.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void ChargesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["regId"] = regId;

        }

        protected void ChargesListView_NeedDataSource(object sender, Telerik.Web.UI.RadListViewNeedDataSourceEventArgs e)
        {
            //CultureInfo ci = new CultureInfo("en-us");
            List<RegistrationCharge> charges = new List<RegistrationCharge>();

            RegistrationMethods rm = new RegistrationMethods();
            charges = rm.GetCalculatedCharges(regId);

            RegistrationChargeMethods cm = new RegistrationChargeMethods();
            charges.AddRange(cm.GetChargesByRegistration(regId));

            ChargesListView.DataSource = charges;
        
        }

        protected void AttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["registrationId"] = regId;
        }

        protected void ChargesRadGrid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //CultureInfo ci = new CultureInfo("en-us");
            List<RegistrationCharge> charges = new List<RegistrationCharge>();

            RegistrationMethods rm = new RegistrationMethods();
            charges = rm.GetCalculatedCharges(regId);

            RegistrationChargeMethods cm = new RegistrationChargeMethods();
            charges.AddRange(cm.GetChargesByRegistration(regId));

            ChargesRadGrid.DataSource = charges;

            // Show charge total
            ChargeTotalLabel.Text = String.Format("{0:c}", charges.Sum(ch => ch.ChargeAmt));
        }
    }
}