﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.PaymentForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step5.jpg" /><br /><br />

<h2>Payment Amount</h2>
<asp:Label ID="AmountLabel" runat="server" Text="465.00" style="margin-left:25px;"></asp:Label>
<br />

<h2>Payment Method</h2>

<asp:RadioButtonList ID="PaymentMethodRadioButtonList" runat="server" Font-Size="Large" Autopostback="true"
        onselectedindexchanged="PaymentMethodRadioButtonList_SelectedIndexChanged">
        <asp:ListItem Value="Credit" Selected="True">Credit Card</asp:ListItem>
        <asp:ListItem Value="Check" >Check</asp:ListItem>
    
</asp:RadioButtonList>
<br />

<h2>Payment Info</h2>

<asp:Panel runat="server" ID="CheckPanel" Visible="false">
<table>
<tr>
    <td style="width:150px">Make check payable to:</td>
    <td>
        <asp:Label ID="CheckPayableLabel" runat="server" Text="" Font-Size="16"></asp:Label></td>
</tr>
<tr>
    <td style="vertical-align:top; width:150px">Mail check to:</td>
    <td><telerik:RadTextBox ID="MailCheckAddressTextBox" runat="server" Text="" Font-Size="Large" Wrap="true" TextMode="MultiLine" Height="100" Width="300" BorderStyle="None"  ReadOnly="true" /></td>
</tr>
</table>
</asp:Panel>

<asp:Panel runat="server" ID="CreditCardPanel">
<table>
<tr>
    <td>Name on card:</td>
    <td>
        <telerik:RadTextBox ID="NameRadTextBox" runat="server" MaxLength="100" Width="200">
        </telerik:RadTextBox></td>
</tr>
<tr>
    <td>Card Number</td>
    <td> <telerik:RadTextBox ID="CardNumberRadTextBox" runat="server" MaxLength="16" Width="200">
        </telerik:RadTextBox></td>
</tr>
<tr>
    <td>Expiration Date:</td>
    <td>
        <telerik:RadComboBox ID="ExpMonthRadComboBox" runat="server" Width="100">
        <Items>
            <telerik:RadComboBoxItem Text="01 - Jan" Value="01" />
            <telerik:RadComboBoxItem Text="02 - Feb" Value="02" />
            <telerik:RadComboBoxItem Text="03 - Mar" Value="03" />
            <telerik:RadComboBoxItem Text="04 - Apr" Value="04" />
            <telerik:RadComboBoxItem Text="05 - May" Value="05" />
            <telerik:RadComboBoxItem Text="06 - Jun" Value="06" />
            <telerik:RadComboBoxItem Text="07 - Jul" Value="07" />
            <telerik:RadComboBoxItem Text="08 - Aug" Value="08" />
            <telerik:RadComboBoxItem Text="09 - Sep" Value="09" />
            <telerik:RadComboBoxItem Text="10 - Oct" Value="10" />
            <telerik:RadComboBoxItem Text="11 - Nov" Value="11" />
            <telerik:RadComboBoxItem Text="12 - Dec" Value="12" />
        </Items>
        </telerik:RadComboBox>&nbsp;
        <telerik:RadComboBox ID="ExpYearRadComboBox" runat="server" Width="95">
        </telerik:RadComboBox>
    </td>
</tr>
<tr>
    <td>CVC:</td>
    <td> <telerik:RadTextBox ID="CvcRadTextBox" runat="server" MaxLength="3" Width="50">
        </telerik:RadTextBox></td>
</tr>
</table>
</asp:Panel>
<br />

    <telerik:RadButton ID="SubmitRadButton" runat="server" Text="Complete Registration" 
        onclick="SubmitRadButton_Click">
    </telerik:RadButton>
    <br />
    <br />

<div class="wizardButtons">
<div>
<telerik:RadButton ID="PreviousRadButton" runat="server" Text="Previous" 
        CausesValidation="False"  CommandName="Previous"
        onclick="PreviousRadButton_Click" ></telerik:RadButton>
</div></div>&nbsp;
<br /><br />



</asp:Content>
