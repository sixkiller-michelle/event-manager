﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Facility.Events.Register
{
    public partial class Addons : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PreviousRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/Attendees.aspx?eventid=" + eventId + "&regid=" + regId);
        }

        protected void NextRadButton_Click(object sender, EventArgs e)
        {
            int eventId = ((RegisterMaster)Master).EventId;
            int regId = ((RegisterMaster)Master).RegId;

            Response.Redirect("~/Facility/Events/Register/Review.aspx?eventid=" + eventId + "&regid=" + regId);
        }
    }
}