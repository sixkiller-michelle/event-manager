﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventManager.Web.Facility
{
    public partial class EventRegistrationForm : System.Web.UI.Page
    {
        private int _eventId;
        private Guid _userId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["EventId"] == null)
            {
                Response.Redirect("~/Facility/EventsCalendar.aspx");
            }
            else
            {
                RegWizardRadButton.PostBackUrl = "RegWiz.aspx?EventId=" + Request.QueryString["EventId"];
                _eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                _userId = UserInfo.UserId;
            }

            if (!IsPostBack)
            {
                GetRegistrationInfo();
                GetPricingInfo();
            }
        }

        protected void GetRegistrationInfo()
        {
            EventManager.Business.EventMethods em = new EventManager.Business.EventMethods();
            EventManager.Model.Event evt = em.GetEvent(_eventId);
            if (evt != null)
            {
                // Fill event description rad editor
                RegistrationInfoRadEditor.Content = evt.RegistrationInfo;
            }
        }

        protected void GetPricingInfo()
        {
            EventManager.Model.Entities ctx = new EventManager.Model.Entities();

            // Get current user's orgId
            EventManager.Business.AttendeeMethods am = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = am.GetAttendeeByUserId(_userId);
            //int organizationId = a.OrganizationId;
            
            //PricingGridView.DataSource = from emp in ctx.Employees.Include("ClinicLocation").Include("aspnet_Users").Include("aspnet_Membership")
            //                              where emp.ClinicLocation.ClinicId == ClinicId && emp.IsActive == true && emp.UserId != null && emp.aspnet_Users.aspnet_Membership.IsApproved == true && emp.aspnet_Users.aspnet_Roles.Count == 0                                     
            //                              select new
            //                              {
            //                                  EmployeeId = emp.EmployeeId,
            //                                  EmployeeName = emp.LastName + ", " + emp.FirstName,
            //                                  BariatrakUserName = emp.aspnet_Users.UserName,
            //                                  CreateDate = emp.aspnet_Users.aspnet_Membership.CreateDate
            //                              };

            //PricingGridView.DataBind();
        }
    }
}