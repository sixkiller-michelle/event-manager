﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace EventManager.Web
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            Exception exc = Server.GetLastError();

            if (exc == null)
                return;

            // Send email to dev
            ExceptionUtility.LogException(exc, Request.Path + "?" + Request.QueryString);
            ExceptionUtility.NotifySystemOps(exc, Request.Path + "?" + Request.QueryString);

            try
            { 
                Server.Transfer("~/GenericErrorPage.aspx");
            }
            catch(Exception)
            {
                Response.Redirect("~/GenericErrorPage.aspx");
            }
            
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
