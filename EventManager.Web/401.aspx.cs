﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace Inclusion.Web
{
    public partial class Error401Form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                AnonymousPanel.Visible = false;
                UnauthorizedPanel.Visible = true;

                LoginPanel.Visible = false;
                ProfilePanel.Visible = true;

                PermbooksPanel.Visible = Roles.IsUserInRole("Permbooks");
                HrPanel.Visible = Roles.IsUserInRole("HR");
                BillingPanel.Visible = Roles.IsUserInRole("Billing");
            }
            else
            {
                AnonymousPanel.Visible = true;
                UnauthorizedPanel.Visible = false;

                LoginPanel.Visible = true;
                ProfilePanel.Visible = false;
                PermbooksPanel.Visible = false;
                HrPanel.Visible = false;
                BillingPanel.Visible = false;
            }
        }
    }
}