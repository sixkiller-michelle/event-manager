﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EventManager.Web
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetSelectedMenuItem();
            SetAssociationPanelVisibility();
        }

        protected void SetSelectedMenuItem()
        {
            Menu mainMenu = (Menu)MenuContent.FindControl("MainMenu");

            if (mainMenu != null)
            { 
                if (Request.ServerVariables["Path_Info"].Contains("/Association/Dashboard"))
                    mainMenu.Items[0].Selected = true;
                else if (Request.ServerVariables["Path_Info"].Contains("/Association/Event") || Request.ServerVariables["Path_Info"].Contains("/Association/NewEvent") || Request.ServerVariables["Path_Info"].Contains("/Association/EditEvent"))
                    mainMenu.Items[1].Selected = true;
                else if (Request.ServerVariables["Path_Info"].Contains("/Association/Org"))
                    mainMenu.Items[2].Selected = true;
                else if (Request.ServerVariables["Path_Info"].Contains("/Association/Attendee"))
                    mainMenu.Items[3].Selected = true;
            }
            
        }

        private void SetAssociationPanelVisibility()
        {
            bool isAssociationEmp = false;
            MembershipUser u = Membership.GetUser();
            if (u != null)
            {
                EventManager.Business.AssociationEmployeeMethods aem = new EventManager.Business.AssociationEmployeeMethods();
                EventManager.Model.AssociationEmployee e = aem.GetEmployeeByUserId(this.UserId);
                if (e != null)
                {
                    isAssociationEmp = true;
                }
            }

            HyperLink assnModHyperlink = (HyperLink)LoginView1.FindControl("ModuleHyperLink");
            if (assnModHyperlink != null)
            {
                assnModHyperlink.NavigateUrl = (isAssociationEmp ? "~/Association/Dashboard.aspx" : "~/Facility/Events/EventList.aspx");
                assnModHyperlink.Text = (isAssociationEmp ? "Dashboard" : "Upcoming Events");
            }
        }

        private void GetAssociationInfo()
        {
            // See if user is logged in
            // If so, get association from database.  If not, get association from query string
            MembershipUser u = Membership.GetUser();
            if (u != null)
            {
                
            }

            //EventManager.Business.AssociationMethods am = new EventManager.Business.AssociationMethods();
            //EventManager.Model.Association a = am.GetAssociation(
        }

        protected void AssociationDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["associationId"] = Request.QueryString["AssociationId"];
        }

        protected void AssociationDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            EventManager.Model.Association a = (EventManager.Model.Association)e.ReturnValue;
            //AssociationNameLabel.Text = a.AssociationName;
            //AssociationAddressLabel.Text = a.FullLocation;
            //AssociationPhoneLabel.Text = a.PhoneNumber;
            //AssociationFaxLabel.Text = a.FaxNumber;

        }

        public Guid UserId
        {
            get
            {
                Guid g = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                return g;
            }
        }

        public Guid SessionUserId
        {
            get 
            {
                if (Session["UserId"] == null)
                {
                    MembershipUser user = System.Web.Security.Membership.GetUser();
                    if (user != null)
                        Session["UserId"] = user.ProviderUserKey.ToString();
                }
                return new Guid(Session["UserId"].ToString());
            }
        }

        public int SessionAssociationId
        {
            get
            {
                if (Session["AssociationId"] == null)
                {
                    EventManager.Business.UserAssociationMethods uam = new EventManager.Business.UserAssociationMethods();
                    Session["AssociationId"] = uam.GetAssociationIdForUser(SessionUserId);
                }
                return Convert.ToInt32(Session["AssociationId"]);
            }
        }
    }
}
