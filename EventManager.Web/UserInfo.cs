﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using EventManager.Business;
using EventManager.Model;

namespace EventManager.Web
{
    public static class UserInfo
    {
        public static Guid UserId
        {
            get
            {
                Guid userGuid = Guid.Empty;

                MembershipUser user = System.Web.Security.Membership.GetUser();

                if (user != null)
                {
                    userGuid = new Guid(user.ProviderUserKey.ToString());
                }

                return userGuid;
            }

        }

        public static string UserName
        {
            get
            {
                try
                {
                    return System.Web.Security.Membership.GetUser().UserName;
                }
                catch
                {
                    return "";
                }
                
            }
        }

        public static int AssociationId
        {
            get
            {
                //Get bariatrak employee from username
                int assnId = 0;
                UserAssociationMethods m = new UserAssociationMethods();
                Guid userId = UserId;

                if (userId != Guid.Empty)
                {
                    assnId = m.GetAssociationIdForUser(userId);
                }
                return assnId;
            }
        }

        public static bool UserIsAssociationEmployee
        {
            get
            {
                //Get bariatrak employee from username
                bool isEmployee = false;
                AssociationEmployeeMethods m = new AssociationEmployeeMethods();
                Guid userId = UserId;

                if (userId != Guid.Empty)
                {
                    AssociationEmployee emp = m.GetEmployeeByUserId(userId);
                    if (emp != null)
                    {
                        isEmployee = true;
                    }
                }
                return isEmployee;
            }
        }

        public static string DefaultStateCode
        {
            get
            {
                int assnId = AssociationId;
                ConfigMethods m = new ConfigMethods();
                Config c = m.GetConfigSetting(assnId, "DefaultState");
                if (c != null)
                {
                    return c.PropertyValue;
                }
                else
                {
                    return "AL";
                }
            }
        }
    }
}