﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;

namespace EventManager.Web.Facility
{
    public partial class PrintCertificateForm : System.Web.UI.Page
    {
        EventManager.Model.Attendee att;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                Response.Redirect("~/PrintCertSearch.aspx");

                //GetAssociations();

                //if (Request.QueryString["AttendeeId"] != null)
                //{
                //    // Navigate right to the attendance history step
                //    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();

                //    // Get the attendee 
                //    EventManager.Model.Attendee a = m.GetAttendee(Convert.ToInt32(Request.QueryString["AttendeeId"]));
                //    if (a != null)
                //    {
                //        AttendeeNameLabel.Text = a.FullNameFNF;
                //        AttendeeTitleLabel.Text = a.Title;
                //        EventHistoryRadGrid.DataSource = m.GetCertificatePrintList(a.Id);
                //        EventHistoryRadGrid.DataBind();
                //        Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(AttendanceHistoryWizardStep);
                //    }
                //    EventsPanel.Visible = true;
                //}
                //else
                //{
                //    EventsPanel.Visible = false;
                //}
                
            }
            else
            {
                EventsPanel.Visible = true;
            }
        }

        protected void ViewCertificateRadButton_Click(object sender, EventArgs e)
        {
            //EventsDataSource.DataBind();


            //Response.Redirect("~/Reports/ReportViewer.aspx?FileName=CertificateVertical.rpt&ParmNames=@EventId|@AttendeeId&ParmValues=70|2460");
        }

        protected void EventsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (String.IsNullOrEmpty(AttendeeIdRadTextBox.Text))
                e.Cancel = true;

            e.InputParameters[0] = AttendeeIdRadTextBox.Text;
        }

        protected void EventsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {
                //InvalidIdValidator.ErrorMessage = e.Exception.InnerException.Message;
                //InvalidIdValidator.IsValid = false;
                e.ExceptionHandled = true;
            }
        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "PrintCertificate")
            {
                int assnId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AssociationId"]);
                int eventId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EventId"]);
                int attendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AttendeeId"]);
                string certificateReportName = "";

                EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
                if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForEvent(eventId))))
                    certificateReportName = m.GetCertificateFileNameForEvent(eventId);
                else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForAssociation(assnId))))
                    certificateReportName = m.GetCertificateFileNameForAssociation(assnId);
                else
                    certificateReportName = m.GetCertificateFileNameForApplication();

                if (certificateReportName != "")
                { 
                    Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=" + certificateReportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId);
                }
            }
        }

        protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            if (dataItem is GridDataItem)
            {
                EventManager.Model.RptAttendeeCertificateList_Result r = (EventManager.Model.RptAttendeeCertificateList_Result)e.Item.DataItem;
                if (!r.IsDateValid)
                    dataItem.Cells[5].ForeColor = System.Drawing.Color.Red;

                if (!r.IsPaid && r.IsPaymentRequired)
                {
                    dataItem.Cells[6].ForeColor = System.Drawing.Color.Red;
                }

                if (!r.IsSurveyComplete && r.IsSurveyRequired)
                {
                    dataItem.Cells[7].ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink lnk = dataItem["SurveyHyperlinkColumn"].Controls[0] as HyperLink;
                    lnk.Visible = false;
                }

                if (!r.IsDateValid || (!r.IsPaid && r.IsPaymentRequired) || (!r.IsSurveyComplete && r.IsSurveyRequired))
                {
                    LinkButton btn = dataItem["PrintCertGridButtonColumn"].Controls[0] as LinkButton;
                    btn.Visible = false; 
                }
            }
        }

        protected void AttendeeSearchRadButton_Click(object sender, EventArgs e)
        {
            int assnId = Convert.ToInt32(AssociationRadComboBox.SelectedValue);
            int orgId = Convert.ToInt32(FacilityRadComboBox.SelectedValue);
            string lastName = LastNameRadTextBox.Text.Trim();
            string firstName = FirstNameRadTextBox.Text.Trim();
            
            // Store the user's assn in cookie
            Response.Cookies["assn"].Value = AssociationRadComboBox.SelectedValue;
            Response.Cookies["assn"].Expires = DateTime.Now.AddDays(60);

            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            List<EventManager.Model.Attendee> attendees = m.GetAttendeesByNameAndFacility(assnId, orgId, firstName, lastName, true);
            if (attendees.Count == 1)
            {
                att = attendees[0];
            }
            else if (attendees.Count == 0)
            {
                AttendeeSearchCustomValidator.ErrorMessage = "No matching person can be found.";
                AttendeeSearchCustomValidator.IsValid = false;
            }
            else if (attendees.Count > 1)
            {
                AttendeeSearchCustomValidator.ErrorMessage = "There were " + attendees.Count.ToString() + " people found matching search criteria.";
                AttendeeSearchCustomValidator.IsValid = false;
                
            }

        }

        private List<EventManager.Model.Attendee> GetAttendeesByName(int assnId, int? orgId, string firstName, string lastName)
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            List<EventManager.Model.Attendee> attendees = m.GetAttendeesByNameAndFacility(assnId, orgId, firstName, lastName, true);
 
            return attendees;
        }

        private EventManager.Model.Attendee GetAttendeeById(int attendeeId)
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee attendee = m.GetAttendee(attendeeId);
            return attendee;
        }

        protected void AttendeesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(LastNameRadTextBox.Text) && !String.IsNullOrWhiteSpace(FirstNameRadTextBox.Text))
            {
                e.InputParameters["associationId"] = AssociationRadComboBox.SelectedValue;
                e.InputParameters["fullName"] = LastNameRadTextBox.Text.Trim() + ", " + FirstNameRadTextBox.Text.Trim();
            }
            else
            {
                e.Cancel = true;
            }
            
        }

        protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
        {
            //if (Wizard1.ActiveStep.ID == "AttendanceHistoryWizardStep")
            //{
            //    if (att != null)
            //    { 
            //        EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            //        EventHistoryRadGrid.DataSource = m.GetCertificatePrintList(att.Id);
            //        EventHistoryRadGrid.DataBind();
            //    }
               
            //}
        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
            EventManager.Model.Attendee a = null;

            if (Wizard1.WizardSteps[e.CurrentStepIndex].ID == "EnterAttendeeId")
            {
                a = GetAttendeeById(Convert.ToInt32(AttendeeIdRadTextBox.Text));
                if (a != null)
                {
                    EventHistoryRadGrid.DataSource = m.GetCertificatePrintList(a.Id);
                    EventHistoryRadGrid.DataBind();
                }
                else
                {
                    AttendeeSearchCustomValidator.ErrorMessage = "No matching person found.";
                    AttendeeSearchCustomValidator.IsValid = false;
                }
            }
            else if (Wizard1.WizardSteps[e.CurrentStepIndex].ID == "AttendeeSearchWizardStep")
            {
                try
                {
                    int? orgId = null;
                    int assnId;
                    string firstName;
                    string lastName;

                    // Check for selected association
                    if (AssociationRadComboBox.SelectedIndex == 0)
                        throw new ArgumentException("Please select your association.");
                    else
                        assnId = Convert.ToInt32(AssociationRadComboBox.SelectedValue);

                    // Check for first and last name
                    if (string.IsNullOrEmpty(LastNameRadTextBox.Text) || string.IsNullOrEmpty(FirstNameRadTextBox.Text))
                    {
                        throw new ArgumentException("Please enter your first and last name.");
                    }
                    else
                    {
                        lastName = LastNameRadTextBox.Text.Trim();
                        firstName = FirstNameRadTextBox.Text.Trim();
                    }
                    
                    if (FacilityRadComboBox.SelectedValue != "")
                    {
                        orgId = Convert.ToInt32(FacilityRadComboBox.SelectedValue);
                    }

                    // Store the user's assn in cookie
                    Response.Cookies["assn"].Value = AssociationRadComboBox.SelectedValue;
                    Response.Cookies["assn"].Expires = DateTime.Now.AddDays(60);

                    List<EventManager.Model.Attendee> attendees = GetAttendeesByName(assnId, orgId, firstName, lastName);

                    if (attendees.Count == 1)
                    {
                        a = attendees[0];
                        AttendeeNameLabel.Text = a.FullNameFNF;
                        AttendeeTitleLabel.Text = a.Title;
                    }
                    else if (attendees.Count == 0)
                    {
                        AttendeeSearchCustomValidator.ErrorMessage = "No matching person can be found.";
                        AttendeeSearchCustomValidator.IsValid = false;
                    }
                    else if (attendees.Count > 1)
                    {
                        AttendeeSearchCustomValidator.ErrorMessage = "There were " + attendees.Count.ToString() + " people found matching search criteria.";
                        AttendeeSearchCustomValidator.IsValid = false;
                    }
                }
                catch(Exception ex)
                {
                    if (ex.InnerException == null)
                        AttendeeSearchCustomValidator.ErrorMessage = ex.Message;
                    else
                        AttendeeSearchCustomValidator.ErrorMessage = "Please select a facility and enter your first and last name.";
                    
                    AttendeeSearchCustomValidator.IsValid = false;
                }
                finally
                {}
            }
            
            if (a != null)
            {
                EventHistoryRadGrid.DataSource = m.GetCertificatePrintList(a.Id);
                EventHistoryRadGrid.DataBind();

                Wizard1.ActiveStepIndex = Wizard1.WizardSteps.IndexOf(AttendanceHistoryWizardStep);
            }
            else
            {
                e.Cancel = true;
            }
        }

        protected void AssociationRadComboBox_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (AssociationRadComboBox.SelectedIndex > 0)
            {
                FirstNameRadTextBox.Enabled = true;
                LastNameRadTextBox.Enabled = true;
                LoadFacilitiesforAssociation();
            }
            else
            {
                FirstNameRadTextBox.Enabled = false;
                LastNameRadTextBox.Enabled = false;
                FacilityRadComboBox.Items.Clear();
                FacilityRadComboBox.Enabled = false;
            }
            
        }

        private void LoadFacilitiesforAssociation()
        {
            if (AssociationRadComboBox.SelectedIndex > 0)
            {
                EventManager.Business.OrganizationMethods m = new EventManager.Business.OrganizationMethods();

                FacilityRadComboBox.Items.Clear();
                FacilityRadComboBox.Text = "";
                FacilityRadComboBox.DataSource = m.GetOrganizationsByAssociation(Convert.ToInt32(AssociationRadComboBox.SelectedValue));
                FacilityRadComboBox.DataBind();
                FacilityRadComboBox.Items.Insert(0, new RadComboBoxItem("", ""));
                FacilityRadComboBox.Enabled = true;
            }
            else
            {
                FacilityRadComboBox.Items.Clear();
            }
        }

        protected void AssociationRadComboBox_DataBound(object sender, EventArgs e)
        {
            if (AssociationRadComboBox.Items.Count > 0)
            {
                // Look for cookie stored for assn
                if (Request.Cookies["assn"] != null)
                {
                    string assnId = Request.Cookies["assn"].Value;
                    AssociationRadComboBox.SelectedValue = assnId;
                }
                else
                { 
                    AssociationRadComboBox.SelectedIndex = 0;
                }

                LoadFacilitiesforAssociation();
            }
        }

        private void Page_Error(object sender, EventArgs e)
        {
            // Get last error from the server
            Exception exc = Server.GetLastError();

            // Handle exceptions generated by Button 1
            if (exc is InvalidOperationException)
            {
                // Pass the error on to the Generic Error page
                Server.Transfer("GenericErrorPage.aspx", true);
            }

            // Handle exceptions generated by Button 2
            else if (exc is ArgumentOutOfRangeException)
            {
                // Give the user some information, but
                // stay on the default page
                PageCustomValidator.ErrorMessage = exc.InnerException.Message;
                PageCustomValidator.IsValid = false;

                // Log the exception and notify system operators
                ExceptionUtility.LogException(exc, "PrintCertificate.aspx");
                ExceptionUtility.NotifySystemOps(exc);

                // Clear the error from the server
                Server.ClearError();
            }
            else if (exc is System.Data.EntityCommandExecutionException)
            {
                if (exc.InnerException != null)
                {
                    if (exc.InnerException.Message.Contains("Timeout expired"))
                        PageCustomValidator.ErrorMessage = "The page is taking too long too load and has timed out.  Please try again later.";
                    else
                        PageCustomValidator.ErrorMessage = exc.InnerException.Message;
                }
                // Give the user some information, but
                // stay on the default page
                PageCustomValidator.IsValid = false;

                // Log the exception and notify system operators
                string lastName = LastNameRadTextBox.Text.Trim();
                string firstName = FirstNameRadTextBox.Text.Trim();
                ExceptionUtility.LogException(exc, "PrintCertificate.aspx - First name: " + firstName + ", Last name: " + lastName);
                ExceptionUtility.NotifySystemOps(exc, "PrintCertificate.aspx", "First name: " + firstName + ", Last name: " + lastName);

                // Clear the error from the server
                Server.ClearError();
            }
            else
            {
                // Pass the error on to the default global handler
            }
        }

        private void GetAssociations()
        {
            EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
            AssociationRadComboBox.DataSource = m.GetAssociations();
            AssociationRadComboBox.DataBind();
        }

    }
}