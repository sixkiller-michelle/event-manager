﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front/Front.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="EventManager.Web.Front._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<!--
    ============================================
    HOME PAGE "CONTENT AND IMAGE" SLIDER
    ============================================= -->
    <div id="MainSlider">
        <div class="container">
            <div class="row">
                <!-- ----------- Begin:ContentSlides ----------- -->
                <div class="col-md-4 col-sm-12 RemovePaddingRight">
                    <div class="MainSliderContent">
                        <div class="MainSliderEntryWrapper">
                            <a data-slide-index="0" href="">
                                <div class="MainSliderEntry">
                                    <h2>LOREM IPSUM DOLOR SIR AMET</h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <p class="AddPaddingTop" style="font-size:11px;">
                                        <i class="fa-clock"></i> 14th April 2013
                                    </p>
                                    <p style="font-size:11px;">
                                        <i class="fa-user"></i> Author Name
                                    </p>
                                </div>
                            </a>
                            <a data-slide-index="1" href="">
                                <div class="MainSliderEntry">
                                    <h2>HABITANT MORBI TRISTIQUE ET</h2>
                                    <p>
                                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum scelerisque.
                                    </p>
                                    <p class="AddPaddingTop" style="font-size:11px;">
                                        <i class="fa-clock"></i> 14th April 2013
                                    </p>
                                    <p style="font-size:11px;">
                                        <i class="fa-user"></i> Author Name
                                    </p>
                                </div>
                            </a>
                            <a data-slide-index="2" href="">
                                <div class="MainSliderEntry">
                                    <h2>SENECTUS ET NEYUS ET MALESUADA</h2>
                                    <p>
                                        Vestibulum scelerisque dignissim massa, sed gravida nisi sollicitudin vel. Sed nec erat feugiat orci hendrerit.
                                    </p>
                                    <p class="AddPaddingTop" style="font-size:11px;">
                                        <i class="fa-clock"></i> 14th April 2013
                                    </p>
                                    <p style="font-size:11px;">
                                        <i class="fa-user"></i> Author Name
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="MainSliderButtons">
                            <ul>
                                <li><a href="#">GET STARTED</a>
                                </li>
                                <li><a class="active" href="#">READ MORE</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ----------- Finish:ContentSlides ----------- -->

                <!-- ----------- Begin:ImageSlides ----------- -->
                <div class="col-md-8 col-sm-12 RemovePaddingLeft">
                    <div class="MainSliderImages">
                        <a href="#">
                            <img src="./images/slide-show/slide1a.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                        <a href="#">
                            <img src="./images/slide-show/slide2a.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                        <a href="#">
                            <img src="./images/slide-show/slide3.jpg" class="img-responsive" title="Your Image Caption to Go Here" alt="alternative information">
                        </a>
                    </div>
                </div>
                <!-- ----------- Finish:ImageSlides ----------- -->
            </div>
        </div>
    </div>

    <!--
    ============================================
    ANNOUCEMENT
    ============================================= -->
    <div class="container">
        <div class="AnnounceTable WhiteSkin clearfix">
            <!-- ----------- Begin:AnnounceSlider ----------- -->
            <div class="col-md-9">
                <div class="AnnounceSlider">
                    <li>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi tellus.
                        </p>
                    </li>
                    <li>
                        <p>
                            Pellentesque habitant morbi tristique senectus et netus et malesuada.
                        </p>
                    </li>
                    <li>
                        <p>
                            Vestibulum scelerisque dignissim massa, sed gravida nisi sollicitudin vel.
                        </p>
                    </li>
                </div>
            </div>
            <!-- ----------- Finish:AnnounceSlider ----------- -->

            <!-- ----------- Begin:AnnounceDropdown ----------- -->
            <%--<div class="col-md-3 hidden-sm hidden-xs">
                <ul class="AnnounceDropdown pull-right">
                    <li>WEBSITE DESIGN</li>
                    <li class="dropdown" style="border-right: 1px solid #544b4a;"><a href="#" class="ddown dropdown-toggle" data-toggle="dropdown"><i class="fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">GRAPHIC DESIGN</a>
                            </li>
                            <li><a href="#">PRINT</a>
                            </li>
                            <li><a href="#">MULTIMEDIA</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>--%>
            <!-- ----------- finish:AnnounceDropdown ----------- -->
        </div>
    </div>

    <!--
    ============================================
    SERVICES & NEW PROJECT
    ============================================= -->
    <div class="container" style="margin-bottom: 10px;">
        <div class="row">
            <!-- ----------- Begin:ServicesMosaic ----------- -->
            <div class="col-sm-12">
                <div class="row">
                    <!-- ----------- Begin:1stMosaic ----------- -->
                    <div class="col-sm-4">
                        <asp:HyperLink ID="PrintCertHyperlink" runat="server" NavigateUrl="~/PrintCertSearch.aspx">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-doc-text VeryLargeSize"></i>
                                <h3>PRINT CERTIFICATE</h3>
                                <p>Attended an event recently?  Click here 
                                to print your certificate.  </p>
                            </div>
                        </asp:HyperLink>
                    </div>
                    <!-- ----------- Finish:1stMosaic ----------- -->

                    <!-- ----------- Begin:2ndMosaic ----------- -->
                    <div class="col-sm-4">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/about/upcomingevents.aspx">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-calendar VeryLargeSize"></i>
                                <h3>UPCOMING EVENTS</h3>
                                <p>View upcoming events in your area or log in
                                to view your association's events
                                </p>
                            </div>
                        </asp:HyperLink>
                    </div>
                    <!-- ----------- Finish:2ndMosaic ----------- -->
                    
                    <!-- ----------- Begin:3rdMosaic ----------- -->
                    <div class="col-sm-4">
                        <a href="#">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa-help VeryLargeSize"></i>
                                <h3>SUPPORT</h3>
                                <p>Contact your association's administrators or the 
                                support staff at ceuavenue.com</p>
                            </div>
                        </a>
                    </div>
                    <!-- ----------- Finish:3rdMosaic ----------- -->
                </div>
            </div>
            <!-- ----------- Finish:ServicesMosaic ----------- -->
        </div>
    </div>

    <!--
    ============================================
    RECENT WORKS
    ============================================= -->
    <%--<div class="container hidden-xs">
        <div class="RecentWorks clearfix">
            <!-- ----------- Begin:RecentWorksHeader ----------- -->
            <div class="RecentWorksHeader clearfix">
                <h2>RECENT WORKS</h2>

                <div class="RecentWorksCarouselCtrls clearfix">
                    <a data-slide="next" href="#RecentWorks" class="RecentWorksCarouselBtns"><i class="fa-angle-right"></i></a>
                    <a data-slide="prev" href="#RecentWorks" class="RecentWorksCarouselBtns"><i class="fa-angle-left"></i></a>
                </div>
            </div>
            <!-- ----------- Finish:RecentWorksHeader ----------- -->

            <!-- ----------- Begin:RecentWorksCarousel ----------- -->
            <div class="RecentWorksCarousel">
                <div class="carousel slide" id="RecentWorks">
                    <div class="carousel-inner">
                        <div class="item active">
                            <!-- ----------- Begin:First4Thumbnails ----------- -->
                            <ul class="thumbnails">
                                <div class="row">
                                    <!-- ----------- Begin:1stThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent1.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent1.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:1stThumbnail ----------- -->

                                    <!-- ----------- Begin:2ndThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent2.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent2.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:2ndThumbnail ----------- -->

                                    <!-- ----------- Begin:3rdThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent3.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent3.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:3rdThumbnail ----------- -->

                                    <!-- ----------- Begin:4thThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent4.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent4.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:4thThumbnail ----------- -->
                                </div>
                            </ul>
                            <!-- ----------- Finish:First4Thumbnails ----------- -->
                        </div>
                        <div class="item">
                            <!-- ----------- Begin:Second4Thumbnails ----------- -->
                            <ul class="thumbnails">
                                <div class="row">
                                    <!-- ----------- Begin:1stThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent5.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent5.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:1stThumbnail ----------- -->

                                    <!-- ----------- Begin:2ndThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent6.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent6.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:2ndThumbnail ----------- -->

                                    <!-- ----------- Begin:3rdThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent7.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent7.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:3rdThumbnail ----------- -->

                                    <!-- ----------- Begin:4thThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent8.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent8.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:4thThumbnail ----------- -->
                                </div>
                            </ul>
                            <!-- ----------- Finish:Second4Thumbnails ----------- -->
                        </div>
                        <div class="item">
                            <!-- ----------- Begin:Third4Thumbnails ----------- -->
                            <ul class="thumbnails">
                                <div class="row">
                                    <!-- ----------- Begin:1stThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent9.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent9.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:1stThumbnail ----------- -->

                                    <!-- ----------- Begin:2ndThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent10.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent10.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:2ndThumbnail ----------- -->

                                    <!-- ----------- Begin:3rdThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent11.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent11.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:3rdThumbnail ----------- -->

                                    <!-- ----------- Begin:4thThumbnail ----------- -->
                                    <li class="col-sm-3 col-xs-3">
                                        <div class="ImageWrapper">
                                            <img class="img-responsive" src="./images/recent-works/recent12.jpg" title="Image title" alt="alternative information">
                                            <div class="ImageCaption">
                                                <div class="CaptionContent">
                                                    <h3>PROJECT TITLE</h3>
                                                    <small>Graphic Design</small>
                                                </div>
                                            </div>
                                            <div class="Buttons">
                                                <a href=""><i class="fa-link"></i></a>
                                                <a href="./images/recent-works/recent12.jpg" class="fancybox"><i class="fa-search"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- ----------- Finish:4thThumbnail ----------- -->
                                </div>
                            </ul>
                            <!-- ----------- Finish:Third4Thumbnails ----------- -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- ----------- Finish:RecentWorksCarousel ----------- -->
        </div>
    </div>--%>

    <!--
    ============================================
    TESTIMONIALS
    ============================================= -->
    <%--<div class="container">
        <div class="row">
            <div class="Testimonials clearfix">
                <!-- ----------- Begin:TestimonialsContent ----------- -->
                <div class="col-sm-8">
                    <div class="TestimonialsContent WhiteSkin tab-content">
                        <div class="TestimonialsEntry tab-pane" id="GRAPHICRIVER">
                            <img src="./img/envato-logos/griver.png" title="Image title" alt="alternative information">
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                John Doe, Director at Company.
                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane active" id="ENVATO">
                            <img src="./img/envato-logos/envato.png" title="Image title" alt="alternative information">
                            <blockquote>
                                consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                Lorem ipsum, Founder and Director at Company.
                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane" id="THEMEFOREST">
                            <img src="./img/envato-logos/themeforest.png" title="Image title" alt="alternative information">
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                John Doe, Director at Company.
                            </p>
                        </div>
                        <div class="TestimonialsEntry tab-pane" id="ACTIVEDEN">
                            <img src="./img/envato-logos/activeden.png" title="Image title" alt="alternative information">
                            <blockquote>
                                consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </blockquote>
                            <p>
                                Lorem ipsum, Founder and Director at Company.
                            </p>
                        </div>
                    </div>
                </div>
                <!-- ----------- Finish:TestimonialsContent ----------- -->

                <!-- ----------- Begin:TestimonialsBlocks ----------- -->
                <div class="col-sm-4">
                    <div class="TestimonialsBlocksWrapper">
                        <ul id="TestimonialsBlocks">
                            <a href="#GRAPHICRIVER" data-toggle="tab">
                                <li class="WhiteSkin">GRAPHICRIVER</li>
                            </a>
                            <a href="#ENVATO" data-toggle="tab">
                                <li class="WhiteSkin active">ENVATO</li>
                            </a>
                            <a href="#THEMEFOREST" data-toggle="tab">
                                <li class="WhiteSkin">THEMEFOREST</li>
                            </a>
                            <a href="#ACTIVEDEN" data-toggle="tab">
                                <li class="WhiteSkin">ACTIVEDEN</li>
                            </a>
                        </ul>
                    </div>
                </div>
                <!-- ----------- Finish:TestimonialsBlocks ----------- -->
            </div>
        </div>
    </div>--%>

    <!--
    ============================================
    OUR CLIENTS
    ============================================= -->
    <%--<div class="container hidden-xs">
        <div class="OurClients clearfix">
            <!-- ----------- Begin:OurClientsHeader ----------- -->
            <div class="OurClientsHeader">
                <h2>OUR CLIENTS</h2>

                <div class="OurClientsCarouselCtrls">
                    <a data-slide="next" href="#OurClients" class="OurClientsCarouselBtns"><i class="fa-angle-right"></i></a>
                    <a data-slide="prev" href="#OurClients" class="OurClientsCarouselBtns"><i class="fa-angle-left"></i></a>
                </div>
            </div>
            <!-- ----------- Finish:OurClientsHeader ----------- -->

            <!-- ----------- Begin:OurClientsCarousel ----------- -->
            <div class="OurClientsCarousel">
                <div class="col-sm-12">
                    <div class="carousel slide" id="OurClients">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/unityb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/enterprisesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/tvdigitalb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/changesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /Slide1 -->
                            <div class="item">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/enterprisesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/tvdigitalb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/unityb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/changesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /Slide2 -->
                            <div class="item">
                                <ul class="thumbnails">
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/unityb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/changesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/enterprisesb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                    <li class="col-sm-3">
                                        <a href="#">
                                            <img class="grayscale img-responsive" src="./images/clients/tvdigitalb.png" title="Image title" alt="alternative information" effect="mono" inverse="true">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ----------- Finish:OurClientsCarousel ----------- -->
        </div>
    </div>--%>
</asp:Content>