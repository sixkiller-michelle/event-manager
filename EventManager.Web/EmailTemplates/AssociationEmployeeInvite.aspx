﻿<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <table>
	<tr>
		<td colspan="6" style="background-color:#666666; padding:5px; border-bottom-size:1px;border-bottom-color:black;">
			<a href="http://www.ceuavenue.com"><img src="http://ceuavenue.com/images/logo2.png" /></a>
		</td>
	</tr>
    <tr>
        <td colspan="6">An account has been created for you on <a href="http://www.ceuavenue.com/account/login.aspx">ceuavenue.com</a>.  Your login credentials are:</td>
    </tr>
    <tr>
		<td>Username: </td>
		<td><%Username%></td>
    </tr>
	<tr>
		<td>Password: </td>
		<td><%Password%></td>
    </tr>
    <tr>
        <td style="font-style:italic;">Created on:</td>
		<td style="font-style:italic;"><%CreatedDateTime%></td>
    </tr>
	<tr>
        <td style="font-style:italic;">Created by:</td>
		<td style="font-style:italic;"><%CreatedByUsername%></td>
    </tr>
    </table>
</body>
</html>

