﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewAssociationRequest.aspx.cs" Inherits="EventManager.Web.EmailTemplates.NewAssociationRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table>
    <tr>
        <td colspan="6"><h1>Requested By</h1></td>
    </tr>
    <tr>
        <td colspan="6" style="vertical-align:top;">
            <%FirstName%>&nbsp;<%LastName%><br />
            <%PhoneNumber%><br />
            <%Email%>
        </td>
    </tr>
    <tr>
        <td colspan="6" style="font-style:italic;">Requested on: <%RequestDateTime%></td>
    </tr>
    <tr>
        <td colspan="6"><h1>Association Details</h1></td>
    </tr>
    <tr>
        <td colspan="6"><%AssociationName%></td>
    </tr>
    <tr>
        <td>Address:</td>
        <td colspan="5"><%Address%></td>
    </tr>
    <tr>
        <td>City:</td>
        <td><%City%></td>
        <td>State:</td>
        <td><%StateCode%></td>
        <td>Zip:</td>
        <td><%Zip%></td>
    </tr>
    <tr>
        <td>Phone:</td>
        <td colspan="5"><%AssociationPhoneNumber%></td>
    </tr>
     <tr>
        <td>Fax:</td>
        <td colspan="5"><%FaxNumber%></td>
    </tr>
    </table>
        
    </div>
    </form>
</body>
</html>
