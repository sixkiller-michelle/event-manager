﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tom.aspx.cs" Inherits="EventManager.Web.Tom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 183px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server" 
        DataSourceID="ObjectDataSource2" DataTextField="AssociationName" 
        DataValueField="AssociationId" 
        onselectedindexchanged="DropDownList1_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        SelectMethod="GetAssociations" 
        TypeName="EventManager.Business.AssociationMethods"></asp:ObjectDataSource>
    <br />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="ObjectDataSource3">
        <EditItemTemplate>
            FullLocation:
            <asp:TextBox ID="FullLocationTextBox" runat="server" 
                Text='<%# Bind("FullLocation") %>' />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="FullLocationTextBox"
                    runat="server" ErrorMessage="This is required"></asp:RequiredFieldValidator>
            <br />
            LatestSessionStartDateTime:
            <asp:TextBox ID="LatestSessionStartDateTimeTextBox" runat="server" 
                Text='<%# Bind("LatestSessionStartDateTime") %>' />
            <br />
            IsRegistrationOpen:
            <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" 
                Checked='<%# Bind("IsRegistrationOpen") %>' />
            <br />
            IsCertificatePrintingAllowed:
            <asp:CheckBox ID="IsCertificatePrintingAllowedCheckBox" runat="server" 
                Checked='<%# Bind("IsCertificatePrintingAllowed") %>' />
            <br />
            Id:
            <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>' />
            <br />
            EventName:
            <asp:TextBox ID="EventNameTextBox" runat="server" 
                Text='<%# Bind("EventName") %>' />
            <br />
            StartDateTime:
            <asp:TextBox ID="StartDateTimeTextBox" runat="server" 
                Text='<%# Bind("StartDateTime") %>' />
            <br />
            EndDateTime:
            <asp:TextBox ID="EndDateTimeTextBox" runat="server" 
                Text='<%# Bind("EndDateTime") %>' />
            <br />
            Location:
            <asp:TextBox ID="LocationTextBox" runat="server" 
                Text='<%# Bind("Location") %>' />
            <br />
            City:
            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
            <br />
            StateCode:
            <asp:TextBox ID="StateCodeTextBox" runat="server" 
                Text='<%# Bind("StateCode") %>' />
            <br />
            TypeId:
            <asp:TextBox ID="TypeIdTextBox" runat="server" Text='<%# Bind("TypeId") %>' />
            <br />
            AssociationId:
            <asp:TextBox ID="AssociationIdTextBox" runat="server" 
                Text='<%# Bind("AssociationId") %>' />
            <br />
            OnSiteRegistrationDateTime:
            <asp:TextBox ID="OnSiteRegistrationDateTimeTextBox" runat="server" 
                Text='<%# Bind("OnSiteRegistrationDateTime") %>' />
            <br />
            EventDesc:
            <asp:TextBox ID="EventDescTextBox" runat="server" 
                Text='<%# Bind("EventDesc") %>' />
            <br />
            EventStatusId:
            <asp:TextBox ID="EventStatusIdTextBox" runat="server" 
                Text='<%# Bind("EventStatusId") %>' />
            <br />
            OnlineRegEndDateTime:
            <asp:TextBox ID="OnlineRegEndDateTimeTextBox" runat="server" 
                Text='<%# Bind("OnlineRegEndDateTime") %>' />
            <br />
            Visibility:
            <asp:TextBox ID="VisibilityTextBox" runat="server" 
                Text='<%# Bind("Visibility") %>' />
            <br />
            AllowPayByCheck:
            <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" 
                Checked='<%# Bind("AllowPayByCheck") %>' />
            <br />
            RegistrationInfo:
            <asp:TextBox ID="RegistrationInfoTextBox" runat="server" 
                Text='<%# Bind("RegistrationInfo") %>' />
            <br />
            CertificatesAvailableStartDate:
            <asp:TextBox ID="CertificatesAvailableStartDateTextBox" runat="server" 
                Text='<%# Bind("CertificatesAvailableStartDate") %>' />
            <br />
            CertificatesAvailableEndDate:
            <asp:TextBox ID="CertificatesAvailableEndDateTextBox" runat="server" 
                Text='<%# Bind("CertificatesAvailableEndDate") %>' />
            <br />
            BoardMeetingStartDateTime:
            <asp:TextBox ID="BoardMeetingStartDateTimeTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingStartDateTime") %>' />
            <br />
            BoardMeetingEndDateTime:
            <asp:TextBox ID="BoardMeetingEndDateTimeTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingEndDateTime") %>' />
            <br />
            BoardMeetingRoom:
            <asp:TextBox ID="BoardMeetingRoomTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingRoom") %>' />
            <br />
            EventType:
            <asp:TextBox ID="EventTypeTextBox" runat="server" 
                Text='<%# Bind("EventType") %>' />
            <br />
            EventTypeReference:
            <asp:TextBox ID="EventTypeReferenceTextBox" runat="server" 
                Text='<%# Bind("EventTypeReference") %>' />
            <br />
            EventFeeSchedules:
            <asp:TextBox ID="EventFeeSchedulesTextBox" runat="server" 
                Text='<%# Bind("EventFeeSchedules") %>' />
            <br />
            EventFlags:
            <asp:TextBox ID="EventFlagsTextBox" runat="server" 
                Text='<%# Bind("EventFlags") %>' />
            <br />
            EventSessions:
            <asp:TextBox ID="EventSessionsTextBox" runat="server" 
                Text='<%# Bind("EventSessions") %>' />
            <br />
            Association:
            <asp:TextBox ID="AssociationTextBox" runat="server" 
                Text='<%# Bind("Association") %>' />
            <br />
            AssociationReference:
            <asp:TextBox ID="AssociationReferenceTextBox" runat="server" 
                Text='<%# Bind("AssociationReference") %>' />
            <br />
            ScannerDatas:
            <asp:TextBox ID="ScannerDatasTextBox" runat="server" 
                Text='<%# Bind("ScannerDatas") %>' />
            <br />
            EventStatu:
            <asp:TextBox ID="EventStatuTextBox" runat="server" 
                Text='<%# Bind("EventStatu") %>' />
            <br />
            EventStatuReference:
            <asp:TextBox ID="EventStatuReferenceTextBox" runat="server" 
                Text='<%# Bind("EventStatuReference") %>' />
            <br />
            Registrations:
            <asp:TextBox ID="RegistrationsTextBox" runat="server" 
                Text='<%# Bind("Registrations") %>' />
            <br />
            EntityState:
            <asp:TextBox ID="EntityStateTextBox" runat="server" 
                Text='<%# Bind("EntityState") %>' />
            <br />
            EntityKey:
            <asp:TextBox ID="EntityKeyTextBox" runat="server" 
                Text='<%# Bind("EntityKey") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            FullLocation:
            <asp:TextBox ID="FullLocationTextBox" runat="server" 
                Text='<%# Bind("FullLocation") %>' />
            <br />
            LatestSessionStartDateTime:
            <asp:TextBox ID="LatestSessionStartDateTimeTextBox" runat="server" 
                Text='<%# Bind("LatestSessionStartDateTime") %>' />
            <br />
            IsRegistrationOpen:
            <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" 
                Checked='<%# Bind("IsRegistrationOpen") %>' />
            <br />
            IsCertificatePrintingAllowed:
            <asp:CheckBox ID="IsCertificatePrintingAllowedCheckBox" runat="server" 
                Checked='<%# Bind("IsCertificatePrintingAllowed") %>' />
            <br />
            Id:
            <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>' />
            <br />
            EventName:
            <asp:TextBox ID="EventNameTextBox" runat="server" 
                Text='<%# Bind("EventName") %>' />
            <br />
            StartDateTime:
            <asp:TextBox ID="StartDateTimeTextBox" runat="server" 
                Text='<%# Bind("StartDateTime") %>' />
            <br />
            EndDateTime:
            <asp:TextBox ID="EndDateTimeTextBox" runat="server" 
                Text='<%# Bind("EndDateTime") %>' />
            <br />
            Location:
            <asp:TextBox ID="LocationTextBox" runat="server" 
                Text='<%# Bind("Location") %>' />
            <br />
            City:
            <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
            <br />
            StateCode:
            <asp:TextBox ID="StateCodeTextBox" runat="server" 
                Text='<%# Bind("StateCode") %>' />
            <br />
            TypeId:
            <asp:TextBox ID="TypeIdTextBox" runat="server" Text='<%# Bind("TypeId") %>' />
            <br />
            AssociationId:
            <asp:TextBox ID="AssociationIdTextBox" runat="server" 
                Text='<%# Bind("AssociationId") %>' />
            <br />
            OnSiteRegistrationDateTime:
            <asp:TextBox ID="OnSiteRegistrationDateTimeTextBox" runat="server" 
                Text='<%# Bind("OnSiteRegistrationDateTime") %>' />
            <br />
            EventDesc:
            <asp:TextBox ID="EventDescTextBox" runat="server" 
                Text='<%# Bind("EventDesc") %>' />
            <br />
            EventStatusId:
            <asp:TextBox ID="EventStatusIdTextBox" runat="server" 
                Text='<%# Bind("EventStatusId") %>' />
            <br />
            OnlineRegEndDateTime:
            <asp:TextBox ID="OnlineRegEndDateTimeTextBox" runat="server" 
                Text='<%# Bind("OnlineRegEndDateTime") %>' />
            <br />
            Visibility:
            <asp:TextBox ID="VisibilityTextBox" runat="server" 
                Text='<%# Bind("Visibility") %>' />
            <br />
            AllowPayByCheck:
            <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" 
                Checked='<%# Bind("AllowPayByCheck") %>' />
            <br />
            RegistrationInfo:
            <asp:TextBox ID="RegistrationInfoTextBox" runat="server" 
                Text='<%# Bind("RegistrationInfo") %>' />
            <br />
            CertificatesAvailableStartDate:
            <asp:TextBox ID="CertificatesAvailableStartDateTextBox" runat="server" 
                Text='<%# Bind("CertificatesAvailableStartDate") %>' />
            <br />
            CertificatesAvailableEndDate:
            <asp:TextBox ID="CertificatesAvailableEndDateTextBox" runat="server" 
                Text='<%# Bind("CertificatesAvailableEndDate") %>' />
            <br />
            BoardMeetingStartDateTime:
            <asp:TextBox ID="BoardMeetingStartDateTimeTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingStartDateTime") %>' />
            <br />
            BoardMeetingEndDateTime:
            <asp:TextBox ID="BoardMeetingEndDateTimeTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingEndDateTime") %>' />
            <br />
            BoardMeetingRoom:
            <asp:TextBox ID="BoardMeetingRoomTextBox" runat="server" 
                Text='<%# Bind("BoardMeetingRoom") %>' />
            <br />
            EventType:
            <asp:TextBox ID="EventTypeTextBox" runat="server" 
                Text='<%# Bind("EventType") %>' />
            <br />
            EventTypeReference:
            <asp:TextBox ID="EventTypeReferenceTextBox" runat="server" 
                Text='<%# Bind("EventTypeReference") %>' />
            <br />
            EventFeeSchedules:
            <asp:TextBox ID="EventFeeSchedulesTextBox" runat="server" 
                Text='<%# Bind("EventFeeSchedules") %>' />
            <br />
            EventFlags:
            <asp:TextBox ID="EventFlagsTextBox" runat="server" 
                Text='<%# Bind("EventFlags") %>' />
            <br />
            EventSessions:
            <asp:TextBox ID="EventSessionsTextBox" runat="server" 
                Text='<%# Bind("EventSessions") %>' />
            <br />
            Association:
            <asp:TextBox ID="AssociationTextBox" runat="server" 
                Text='<%# Bind("Association") %>' />
            <br />
            AssociationReference:
            <asp:TextBox ID="AssociationReferenceTextBox" runat="server" 
                Text='<%# Bind("AssociationReference") %>' />
            <br />
            ScannerDatas:
            <asp:TextBox ID="ScannerDatasTextBox" runat="server" 
                Text='<%# Bind("ScannerDatas") %>' />
            <br />
            EventStatu:
            <asp:TextBox ID="EventStatuTextBox" runat="server" 
                Text='<%# Bind("EventStatu") %>' />
            <br />
            EventStatuReference:
            <asp:TextBox ID="EventStatuReferenceTextBox" runat="server" 
                Text='<%# Bind("EventStatuReference") %>' />
            <br />
            Registrations:
            <asp:TextBox ID="RegistrationsTextBox" runat="server" 
                Text='<%# Bind("Registrations") %>' />
            <br />
            EntityState:
            <asp:TextBox ID="EntityStateTextBox" runat="server" 
                Text='<%# Bind("EntityState") %>' />
            <br />
            EntityKey:
            <asp:TextBox ID="EntityKeyTextBox" runat="server" 
                Text='<%# Bind("EntityKey") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <table style="width:100%;">
                <tr>
                    <td class="style1">
                        FullLocation</td>
                    <td>
                        <asp:Label ID="FullLocationLabel" runat="server" 
                            Text='<%# Bind("FullLocation") %>' />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style1">
                        LatestSessionStartDateTime</td>
                    <td>
                        <asp:Label ID="LatestSessionStartDateTimeLabel" runat="server" 
                            Text='<%# Bind("LatestSessionStartDateTime") %>' />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            : <br />
            : <br />
            IsRegistrationOpen:
            <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" 
                Checked='<%# Bind("IsRegistrationOpen") %>' Enabled="false" />
            <br />
            IsCertificatePrintingAllowed:
            <asp:CheckBox ID="IsCertificatePrintingAllowedCheckBox" runat="server" 
                Checked='<%# Bind("IsCertificatePrintingAllowed") %>' Enabled="false" />
            <br />
            Id:
            <asp:Label ID="IdLabel" runat="server" Text='<%# Bind("Id") %>' />
            <br />
            EventName:
            <asp:Label ID="EventNameLabel" runat="server" Text='<%# Bind("EventName") %>' />
            <br />
            StartDateTime:
            <asp:Label ID="StartDateTimeLabel" runat="server" 
                Text='<%# Bind("StartDateTime") %>' />
            <br />
            EndDateTime:
            <asp:Label ID="EndDateTimeLabel" runat="server" 
                Text='<%# Bind("EndDateTime") %>' />
            <br />
            Location:
            <asp:Label ID="LocationLabel" runat="server" Text='<%# Bind("Location") %>' />
            <br />
            City:
            <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>' />
            <br />
            StateCode:
            <asp:Label ID="StateCodeLabel" runat="server" Text='<%# Bind("StateCode") %>' />
            <br />
            TypeId:
            <asp:Label ID="TypeIdLabel" runat="server" Text='<%# Bind("TypeId") %>' />
            <br />
            AssociationId:
            <asp:Label ID="AssociationIdLabel" runat="server" 
                Text='<%# Bind("AssociationId") %>' />
            <br />
            OnSiteRegistrationDateTime:
            <asp:Label ID="OnSiteRegistrationDateTimeLabel" runat="server" 
                Text='<%# Bind("OnSiteRegistrationDateTime") %>' />
            <br />
            EventDesc:
            <asp:Label ID="EventDescLabel" runat="server" Text='<%# Bind("EventDesc") %>' />
            <br />
            EventStatusId:
            <asp:Label ID="EventStatusIdLabel" runat="server" 
                Text='<%# Bind("EventStatusId") %>' />
            <br />
            OnlineRegEndDateTime:
            <asp:Label ID="OnlineRegEndDateTimeLabel" runat="server" 
                Text='<%# Bind("OnlineRegEndDateTime") %>' />
            <br />
            Visibility:
            <asp:Label ID="VisibilityLabel" runat="server" 
                Text='<%# Bind("Visibility") %>' />
            <br />
            AllowPayByCheck:
            <asp:CheckBox ID="AllowPayByCheckCheckBox" runat="server" 
                Checked='<%# Bind("AllowPayByCheck") %>' Enabled="false" />
            <br />
            RegistrationInfo:
            <asp:Label ID="RegistrationInfoLabel" runat="server" 
                Text='<%# Bind("RegistrationInfo") %>' />
            <br />
            CertificatesAvailableStartDate:
            <asp:Label ID="CertificatesAvailableStartDateLabel" runat="server" 
                Text='<%# Bind("CertificatesAvailableStartDate") %>' />
            <br />
            CertificatesAvailableEndDate:
            <asp:Label ID="CertificatesAvailableEndDateLabel" runat="server" 
                Text='<%# Bind("CertificatesAvailableEndDate") %>' />
            <br />
            BoardMeetingStartDateTime:
            <asp:Label ID="BoardMeetingStartDateTimeLabel" runat="server" 
                Text='<%# Bind("BoardMeetingStartDateTime") %>' />
            <br />
            BoardMeetingEndDateTime:
            <asp:Label ID="BoardMeetingEndDateTimeLabel" runat="server" 
                Text='<%# Bind("BoardMeetingEndDateTime") %>' />
            <br />
            BoardMeetingRoom:
            <asp:Label ID="BoardMeetingRoomLabel" runat="server" 
                Text='<%# Bind("BoardMeetingRoom") %>' />
            <br />
            EventType:
            <asp:Label ID="EventTypeLabel" runat="server" Text='<%# Bind("EventType") %>' />
            <br />
            EventTypeReference:
            <asp:Label ID="EventTypeReferenceLabel" runat="server" 
                Text='<%# Bind("EventTypeReference") %>' />
            <br />
            EventFeeSchedules:
            <asp:Label ID="EventFeeSchedulesLabel" runat="server" 
                Text='<%# Bind("EventFeeSchedules") %>' />
            <br />
            EventFlags:
            <asp:Label ID="EventFlagsLabel" runat="server" 
                Text='<%# Bind("EventFlags") %>' />
            <br />
            EventSessions:
            <asp:Label ID="EventSessionsLabel" runat="server" 
                Text='<%# Bind("EventSessions") %>' />
            <br />
            Association:
            <asp:Label ID="AssociationLabel" runat="server" 
                Text='<%# Bind("Association") %>' />
            <br />
            AssociationReference:
            <asp:Label ID="AssociationReferenceLabel" runat="server" 
                Text='<%# Bind("AssociationReference") %>' />
            <br />
            ScannerDatas:
            <asp:Label ID="ScannerDatasLabel" runat="server" 
                Text='<%# Bind("ScannerDatas") %>' />
            <br />
            EventStatu:
            <asp:Label ID="EventStatuLabel" runat="server" 
                Text='<%# Bind("EventStatu") %>' />
            <br />
            EventStatuReference:
            <asp:Label ID="EventStatuReferenceLabel" runat="server" 
                Text='<%# Bind("EventStatuReference") %>' />
            <br />
            Registrations:
            <asp:Label ID="RegistrationsLabel" runat="server" 
                Text='<%# Bind("Registrations") %>' />
            <br />
            EntityState:
            <asp:Label ID="EntityStateLabel" runat="server" 
                Text='<%# Bind("EntityState") %>' />
            <br />
            EntityKey:
            <asp:Label ID="EntityKeyLabel" runat="server" Text='<%# Bind("EntityKey") %>' />
            <br />

        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
        SelectMethod="GetEvent" TypeName="EventManager.Business.EventMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="eventId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" 
        CellSpacing="0" DataSourceID="ObjectDataSource1" GridLines="None" 
        oniteminserted="RadGrid1_ItemInserted">
        <MasterTableView DataSourceID="ObjectDataSource1">
            <CommandItemSettings ExportToPdfText="Export to PDF">
            </CommandItemSettings>
            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="FullLocation" 
                    FilterControlAltText="Filter FullLocation column" HeaderText="FullLocation" 
                    ReadOnly="True" SortExpression="FullLocation" UniqueName="FullLocation">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EventName" 
                    FilterControlAltText="Filter EventName column" HeaderText="EventName" 
                    SortExpression="EventName" UniqueName="EventName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" 
                    FilterControlAltText="Filter StartDateTime column" HeaderText="StartDateTime" 
                    SortExpression="StartDateTime" UniqueName="StartDateTime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EndDateTime" DataType="System.DateTime" 
                    FilterControlAltText="Filter EndDateTime column" HeaderText="EndDateTime" 
                    SortExpression="EndDateTime" UniqueName="EndDateTime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Visibility" 
                    FilterControlAltText="Filter Visibility column" HeaderText="Visibility" 
                    SortExpression="Visibility" UniqueName="Visibility">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BoardMeetingRoom" 
                    FilterControlAltText="Filter BoardMeetingRoom column" 
                    HeaderText="BoardMeetingRoom" SortExpression="BoardMeetingRoom" 
                    UniqueName="BoardMeetingRoom">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EntityState" DataType="System.Int32" 
                    FilterControlAltText="Filter EntityState column" HeaderText="EntityState" 
                    ReadOnly="True" SortExpression="EntityState" UniqueName="EntityState">
                </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetEventsByMonth" TypeName="EventManager.Business.EventMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="associationId" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:QueryStringParameter Name="monthStart" QueryStringField="MonthStart" 
                Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
