﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Common;
using System.Web.UI;

/// <summary>
/// Summary description for CommonWebUtils
/// </summary>
public static class CommonWebUtils
{

    public static System.Web.UI.Control GetPostBackControl(Page page)
    {
        Control control = null;

        string ctrlname = page.Request.Params.Get("__EVENTTARGET");
        if (ctrlname != null && ctrlname != string.Empty)
        {
            control = page.FindControl(ctrlname);
        }
        else
        {
            foreach (string ctl in page.Request.Form)
            {
                if (ctl != null)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        control = c;
                        break;
                    }
                }
            }
        }
        return control;
    }

    public static int GetColumnIndex(System.Web.UI.WebControls.GridView g, string headerText)
    {
        int index = -1;
        foreach (System.Web.UI.WebControls.DataControlField c in g.Columns)
        {
            if (c.HeaderText == headerText)
            {
                index = g.Columns.IndexOf(c);
            }
        }
        return index;
    }

}