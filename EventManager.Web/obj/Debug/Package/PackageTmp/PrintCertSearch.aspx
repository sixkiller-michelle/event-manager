﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.Master" AutoEventWireup="true" CodeBehind="PrintCertSearch.aspx.cs" Inherits="EventManager.Web.PrintCertSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    

<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="AssociationRadComboBox">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="AssociationRadComboBox" />--%>
                    <telerik:AjaxUpdatedControl ControlID="FacilityRadComboBox" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

<div id="content" class="box">
	<h2 class="page-title">Print Certificate</h2>
	<div class="breadcrumb"><a href="Default.aspx" id="home">Home</a>  »  <a href="#">Certificates</a></div>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="ValidationGroup1" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="An error has occurred" Display="None" ValidationGroup="ValidationGroup1"></asp:CustomValidator>

    <table class="formview-table-layout">
        <tr>
            <td>Association:</td>
            <td>
                <telerik:RadComboBox ID="AssociationRadComboBox" Runat="server" AutoPostBack="true"  
                    DataTextField="AssociationName" AppendDataBoundItems="true" 
                    DataValueField="AssociationId"  Width="400px" 
                    OnSelectedIndexChanged="AssociationRadComboBox_SelectedIndexChanged" 
                    OnDataBound="AssociationRadComboBox_DataBound">
                    <Items>
                        <telerik:RadComboBoxItem Text="" Value="" />
                    </Items>
                </telerik:RadComboBox>
            </td>
        </tr> 
        <tr>
            <td>Facility:</td>
            <td>
                <telerik:RadComboBox ID="FacilityRadComboBox" Runat="server" AllowCustomText="true" MarkFirstMatch="true"  
                    DataTextField="OrgName" 
                    DataValueField="Id" Width="400px" AppendDataBoundItems="True">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>First Name:</td>
            <td><telerik:RadTextBox ID="FirstNameRadTextBox" runat="server" EmptyMessage="First name" CausesValidation="true" />
            <asp:RequiredFieldValidator ID="FirstNameRequiredValidator" runat="server" ErrorMessage="First name is required" ValidationGroup="ValidationGroup1" ControlToValidate="FirstNameRadTextBox" Display="None"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td><telerik:RadTextBox ID="LastNameRadTextBox" runat="server" EmptyMessage="Last name" />
            <asp:RequiredFieldValidator ID="LastNameRequiredValidator" runat="server" ErrorMessage="Last name is required" ValidationGroup="ValidationGroup1" ControlToValidate="LastNameRadTextBox" Display="None"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><telerik:RadButton ID="AttendeeSearchRadButton" runat="server" Text="Find Me" CausesValidation="true" ValidationGroup="ValidationGroup1" onclick="AttendeeSearchRadButton_Click" UseSubmitBehavior="true" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="AttendeeIdLabel" runat="server" Text=""></asp:Label></td>
        </tr>   
    </table>

    <telerik:radgrid id="AttendeesRadGrid" runat="server" autogeneratecolumns="False"
            cellspacing="0" gridlines="None" Visible="false" 
        onitemcommand="AttendeesRadGrid_ItemCommand">
    <mastertableview >
    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
    <HeaderStyle Width="20px"></HeaderStyle>
    </RowIndicatorColumn>

    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
    <HeaderStyle Width="20px"></HeaderStyle>
    </ExpandCollapseColumn>

        <Columns>
            <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" 
                FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" 
                UniqueName="Id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FullName" 
                FilterControlAltText="Filter FullName column" HeaderText="Name" ReadOnly="True" 
                SortExpression="FullName" UniqueName="FullName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Organization.OrgName" 
                FilterControlAltText="Filter OrgName column" HeaderText="Facility" 
                SortExpression="Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Title" 
                FilterControlAltText="Filter Title column" HeaderText="Title" 
                SortExpression="Title" UniqueName="Title">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="" CommandName="SelectAttendee" Text="This is me" UniqueName="SelectButtonGridButtonColumn"
                 ButtonType="PushButton">
            </telerik:GridButtonColumn>
        </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </mastertableview>
        <filtermenu enableimagesprites="False"></filtermenu>
        </telerik:radgrid>

</div>

</asp:Content>
