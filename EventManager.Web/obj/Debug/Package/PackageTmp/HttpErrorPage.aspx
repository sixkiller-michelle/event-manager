﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HttpErrorPage.aspx.cs" Inherits="EventManager.Web.HttpErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<%--
The following example shows the HttpErrorPage.aspx page. 
This page also creates a safe message that depends on the value of the error code, which it displays to remote users. 
For local users, the page displays a complete exception report. The Application_Error handler redirects HttpException errors to this page.
--%>

<div>
    <h2>
      Http Error Page</h2>
    <asp:Panel ID="InnerErrorPanel" runat="server" Visible="false">
      <asp:Label ID="innerMessage" runat="server" Font-Bold="true" 
        Font-Size="Large" /><br />
      <pre>
        <asp:Label ID="innerTrace" runat="server" />
      </pre>
    </asp:Panel>
    Error Message:<br />
    <asp:Label ID="exMessage" runat="server" Font-Bold="true" 
      Font-Size="Large" />
    <pre>
      <asp:Label ID="exTrace" runat="server" Visible="false" />
    </pre>
    <br />
    Return to the 
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Default Page</asp:HyperLink>
  </div>

<%--
<div style="width:400px; margin:0 auto; margin-top:100px; text-align:center;">
    <h2>We're Sorry!</h2>
    <p style="color:Red;">An error has occurred while processing your request. </p>

    <p>An email has been sent to us and we'll get it fixed ASAP.</p>
    return to: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Home Page</asp:HyperLink>

</div>--%>

</asp:Content>
