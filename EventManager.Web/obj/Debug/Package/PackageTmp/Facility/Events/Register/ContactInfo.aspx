﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.Master" AutoEventWireup="true" CodeBehind="ContactInfo.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.ContactInfo" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step1.jpg" /><br /><br />

<div class="margin:0 auto;">

<p class="instruct">Before starting registration, please verify your facility's contact information and 
your personal contact information:</p>

 <asp:FormView ID="UserFormView" runat="server" style="float:left; margin-right:20px;" 
        DataSourceID="AttendeeDataSource" DataKeyNames="Id" DefaultMode="ReadOnly"
        onitemcommand="UserFormView_ItemCommand">
       <HeaderTemplate><h2>Personal Contact Info</h2></HeaderTemplate>
        <EditItemTemplate>
            <asp:Panel runat="server" ID="AttendeeInfoPanel" DefaultButton="UpdateButton">
            <table style="width: 100%;" class="formview-table-layout">
                <tr>
                    <td class="label">First Name:</td>
                    <td><telerik:RadTextBox ID="FirstNameTextBox" runat="server"  Text='<%# Eval("FirstName") %>' MaxLength="30" /></td>
                </tr>
                    <tr>
                    <td class="label">Last Name:</td>
                    <td><telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Eval("LastName") %>' MaxLength="30" /></td>
                </tr>
                    <tr>
                    <td class="label">Middle Initial:</td>
                    <td><telerik:RadTextBox ID="MiddleInitialTextBox" runat="server"  Text='<%# Eval("MiddleInitial") %>' MaxLength="1" /></td>
                </tr>
                    <tr>
                    <td class="label">Email:</td>
                    <td><telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Eval("Email") %>' MaxLength="100" Width="200" /></td>
                </tr>
                <tr>
                    <td class="label">Phone:</td>
                    <td><telerik:RadTextBox ID="PhoneNumberTextBox" runat="server" Text='<%# Eval("PhoneNumber") %>' MaxLength="20" /></td>
                </tr>
            </table>
            </asp:Panel>
             <br /><br />
            <telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="UpdateAttendeeInfo" />
            <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False"  CommandName="Cancel" />
            <br /><br />

            <%--<telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="UpdateAttendeeInfo" />
            <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False"  CommandName="Cancel" />--%>
        </EditItemTemplate>
            
        <ItemTemplate>
            <table class="formview-table-layout">
                <tr>
                    <td class="label">Name:</td>
                    <td><asp:Label ID="FirstNameTextBox" runat="server"  Text='<%# Bind("FullName") %>' /></td>
                </tr>
                    <tr>
                    <td class="label">Email:</td>
                    <td><asp:Label ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' /></td>
                </tr>
                <tr>
                    <td class="label">Phone:</td>
                    <td><asp:Label ID="PhoneNumberTextBox" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                </tr>
            </table>
            <br />
           <telerik:RadButton ID="EditRadButton" runat="server" Text="Edit" CausesValidation="False"  CommandName="Edit" ButtonType="StandardButton" >
            </telerik:RadButton>
        </ItemTemplate>
    </asp:FormView>

    <asp:FormView ID="OrganizationFormView" runat="server" 
        DataSourceID="OrganizationDataSource" DataKeyNames="Id"
        onitemupdated="OrganizationFormView_ItemUpdated" 
        onmodechanged="OrganizationFormView_ModeChanged"
        onitemcommand="OrganizationFormView_ItemCommand">
         <HeaderTemplate><h2>Facility Contact Info</h2></HeaderTemplate>
        <EditItemTemplate>
            <asp:Panel runat="server" ID="AttendeeInfoPanel" DefaultButton="UpdateButton">
            <table  class="formview-table-layout">
                <tr>
                    <td colspan="2"><b><asp:Label ID="OrgNameTextBox" runat="server"  Text='<%# Eval("OrgName") %>' Enabled="false" /></b></td>
                </tr>
                    <tr>
                    <td class="label">Street:</td>
                    <td><telerik:RadTextBox ID="StreetTextBox" runat="server" Text='<%# Eval("Address") %>'  /></td>
                </tr>
                <tr>
                    <td class="label">City:</td>
                    <td><telerik:RadTextBox ID="CityTextBox" runat="server" Text='<%# Eval("City") %>' /></td>
                </tr>
                <tr>
                    <td class="label">State:</td>
                    <td><telerik:RadComboBox
                            ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                            DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Eval("StateCode") %>'
                            DropDownWidth="200" Height="100">
                        </telerik:RadComboBox></td>
                </tr>
                <tr>
                    <td class="label">Zip:</td>
                    <td><telerik:RadTextBox ID="ZipTextBox" runat="server" Text='<%# Eval("Zip") %>' /></td>
                </tr>
                <tr>
                    <td class="label">Phone:</td>
                    <td><telerik:RadTextBox ID="OrgPhoneTextBox" runat="server" Text='<%# Eval("Phone") %>' /></td>
                </tr>
                <tr>
                    <td class="label">Fax:</td>
                    <td><telerik:RadTextBox ID="OrgFaxTextBox" runat="server" Text='<%# Eval("Fax") %>' /></td>
                </tr>
            </table>
            </asp:Panel>
            <br /><br />
            <telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="UpdateFacilityInfo"  />
            <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False"  CommandName="Cancel" />
            <br /><br />
        </EditItemTemplate>
            
        <ItemTemplate>
            <table class="formview-table-layout">
                <tr>
                    <td class="label">Facility:</td>
                    <td><asp:Label ID="OrgNameTextBox" runat="server"  Text='<%# Eval("OrgName") %>' /></td>
                </tr>
                    <tr>
                    <td class="label">Address:</td>
                    <td><asp:Label ID="OrgFullAddressTextBox" runat="server" Text='<%# Eval("FullAddress") %>' /></td>
                </tr>
                <tr>
                    <td class="label">Phone:</td>
                    <td><asp:Label ID="OrgPhoneTextBox" runat="server" Text='<%# Eval("Phone") %>' /></td>
                </tr>
                <tr>
                    <td class="label">Fax:</td>
                    <td><asp:Label ID="OrgFaxTextBox" runat="server" Text='<%# Eval("Fax") %>' /></td>
                </tr>
            </table>
            <br />
            <telerik:RadButton ID="EditRadButton" runat="server" Text="Edit" CausesValidation="False"  CommandName="Edit" ButtonType="LinkButton" CssClass="wizardButton"></telerik:RadButton> 
            <br /><br />
        </ItemTemplate>
    </asp:FormView>
</div>
<div style="clear:both;"></div>
<br />
<div class="wizardButtons">
    <div>
        <telerik:RadButton ID="NextRadButton" runat="server" Text="Next" 
            CausesValidation="False"  CommandName="Next"
            onclick="NextRadButton_Click" ></telerik:RadButton>
    </div>
</div>

<asp:ObjectDataSource runat="server" ID="AttendeeDataSource" 
    onselecting="AttendeeDataSource_Selecting" SelectMethod="GetAttendeeByUserId" 
    TypeName="EventManager.Business.AttendeeMethods"
        DataObjectTypeName="EventManager.Model.Attendee">
    <SelectParameters>
        <asp:Parameter DbType="Guid" Name="userId" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="OrganizationDataSource" runat="server" 
    SelectMethod="GetOrganization"
    TypeName="EventManager.Business.OrganizationMethods" 
    DataObjectTypeName="EventManager.Model.Organization" 
    onselecting="OrganizationDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="StatesDataSource" runat="server" 
    SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
</asp:ObjectDataSource>

</asp:Content>
