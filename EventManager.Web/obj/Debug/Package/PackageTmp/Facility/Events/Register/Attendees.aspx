﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.Master" AutoEventWireup="true" CodeBehind="Attendees.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.Attendees" %>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step2.jpg" /><br /><br />

<p class="instruct">
Select the people that will be attending the event. To edit a person's information, click the "Edit" link. If a person is not listed, click "Add Attendee" to add them.</p>

<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ForeColor="Red" Display="Dynamic"></asp:CustomValidator>

<div>
    <telerik:RadButton runat="server" ID="NewAttendeeRadButton" Text="Add Attendee" onclick="NewAttendeeRadButton_Click"></telerik:RadButton>
    <div style="float:right;">
        <asp:Label ID="SelectedAttendeesLabel" runat="server" Text="Selected attendees: "></asp:Label>
        <asp:Label ID="SelectedAttendeesCount" runat="server" Text="0" ForeColor="Green" Font-Size="Large"></asp:Label>
    </div>
</div>
<div style="clear:both;"></div>
<br />

<%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
     <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="AttendeesRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AttendeesRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />--%>

 <telerik:RadGrid ID="AttendeesRadGrid" runat="server" AutoGenerateColumns="False" Width="900"  
                CellSpacing="0" Skin="Simple"
        GridLines="None"  AllowMultiRowSelection="true"
                OnItemCreated="AttendeesRadGrid_ItemCreated" 
                OnItemCommand="AttendeesRadGrid_ItemCommand" 
                OnDataBound="AttendeesRadGrid_DataBound" 
        onitemupdated="AttendeesRadGrid_ItemUpdated" 
        onneeddatasource="AttendeesRadGrid_NeedDataSource"  >
            <SelectedItemStyle CssClass="SelectedItem" />
            <%--<ClientSettings EnableRowHoverStyle="true">
                <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>--%>
            <MasterTableView DataKeyNames="Id">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>

            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>

                <Columns>

                    <%--<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn"></telerik:GridClientSelectColumn>--%>

                    <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn" ItemStyle-Width="50"
                        HeaderStyle-Width="50">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="ToggleRowSelection"
                            AutoPostBack="True" />
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle Width="50px"></HeaderStyle>
                        <ItemStyle Width="50px"></ItemStyle>
                    </telerik:GridTemplateColumn>

                    <%-- <telerik:GridTemplateColumn UniqueName="SelectAttendeeTemplateColumn" HeaderText="" ItemStyle-Width="30" HeaderStyle-Width="30">
                        <ItemTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:CheckBox ID="SelectedAttendeeCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="CheckedChanged" />
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>

                    <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn"
                        ItemStyle-Width="30px" HeaderText="" HeaderStyle-Width="30px" UpdateText="Update" ButtonType="LinkButton">
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle Width="30px" HorizontalAlign="Center"></ItemStyle>
                    </telerik:GridEditCommandColumn>

                    <telerik:GridBoundColumn DataField="FullName" ItemStyle-Width="300" HeaderStyle-Width="300"
                        FilterControlAltText="Filter FullName column" HeaderText="Name" 
                        ReadOnly="True" SortExpression="FullName" UniqueName="FullName">
<HeaderStyle Width="300px"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn DataField="Title" 
                        FilterControlAltText="Filter Title column" HeaderText="Title" 
                        SortExpression="Title" UniqueName="Title" ItemStyle-Width="150" HeaderStyle-Width="150">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle Width="150px"></ItemStyle>
                    </telerik:GridBoundColumn>

                     <telerik:GridBoundColumn DataField="LicenseNumber" 
                        FilterControlAltText="Filter LicenseNumber column" HeaderText="License Number" 
                        SortExpression="LicenseNumber" UniqueName="LicenseNumberGridBoundColumn" ItemStyle-Width="100" HeaderStyle-Width="100">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
                    </telerik:GridBoundColumn>

                    <telerik:GridDateTimeColumn DataField="LicenseExpDate" DataFormatString="{0:d}" PickerType="DatePicker" 
                        FilterControlAltText="Filter LicenseExpDate column" HeaderText="License Exp. Date" 
                        SortExpression="LicenseExpDate" UniqueName="LicenseExpDateGridDateTimeColumn" ItemStyle-Width="120" HeaderStyle-Width="120">
<HeaderStyle Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
                    </telerik:GridDateTimeColumn>

                    <telerik:GridDateTimeColumn DataField="Birthdate" DataFormatString="{0:d}" PickerType="DatePicker" 
                        FilterControlAltText="Filter Birthdate column" HeaderText="Birthdate" 
                        SortExpression="Birthdate" UniqueName="BirthdateGridDateTimeColumn" ItemStyle-Width="120" HeaderStyle-Width="120">
<HeaderStyle Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
                    </telerik:GridDateTimeColumn>

                </Columns>
                
                <EditFormSettings EditFormType="Template" >
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                <FormTemplate>
                    <asp:Panel runat="server" ID="FormTemplatePanel" DefaultButton="btnUpdate" >
                    <asp:ValidationSummary ID="FormTemplateValidationSummary" runat="server" ValidationGroup="FormTemplateValidationGroup" CssClass="errorMessage" />
                    <asp:CustomValidator ID="FormTemplateCustomValidator" runat="server" ErrorMessage="" CssClass="errorMessage" Display="None" ValidationGroup="FormTemplateValidationGroup"></asp:CustomValidator>
                    <table id="AttendeeEditTable" style="margin:5px; float:left;" class="formTable">
                        <tr>
                            <td colspan="2"><h3 style="margin-top:0px;"><asp:Label ID="AttendeeNameLabel" runat="server" Text='<%# Eval("FullName") %>'  /></h3></td>
                        </tr>
                        <tr>
                            <td>First Name:</td><td><telerik:RadTextBox ID="FirstNameRadTextBox" runat="server" Text='<%# Bind("FirstName") %>' Width="150" MaxLength="30" />
                                <asp:RequiredFieldValidator ID="FirstNameRequiredFieldValidator" runat="server" ErrorMessage="First name is required" ControlToValidate="FirstNameRadTextBox" CssClass="errorMessage"></asp:RequiredFieldValidator>
                            </td></tr>
                        <tr>
                            <td>Last Name:</td><td><telerik:RadTextBox ID="LastNameRadTextBox" runat="server" Text='<%# Bind("LastName") %>' Width="150" MaxLength="30" />
                            <asp:RequiredFieldValidator ID="LastNameRequiredFieldValidator" runat="server" ErrorMessage="Last name is required" ControlToValidate="LastNameRadTextBox" CssClass="errorMessage"></asp:RequiredFieldValidator>
                            </td></tr>
                       <%-- <tr>
                            <td>Title:</td><td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' Width="150" MaxLength="50" /></td></tr>--%>
                        <tr>
                            <td>Title:</td>
                            <td>
                                <telerik:RadComboBox ID="TitleRadComboBox" runat="server" DropDownAutoWidth="Enabled" Height="200" Skin="Simple" AllowCustomText="true"
                                    DataSourceID="TitlesDataSource" DataTextField="Title" DataValueField="Title" Text='<%# Bind("Title") %>'>
                                </telerik:RadComboBox>
                            </td></tr>
                        <tr>
                            <td>License Number:</td><td><telerik:RadTextBox ID="LicenseNumberRadTextBox" runat="server" Text='<%# Bind("LicenseNumber") %>' Width="150" MaxLength="50"  /></td></tr>
                         <tr>
                            <td>License Exp. Date:</td><td>
                                <telerik:RadDatePicker ID="ExpDateRadDatePicker" runat="server" DbSelectedDate='<%# Bind("LicenseExpDate") %>'>
                                </telerik:RadDatePicker>
                            </td></tr>
                         <tr>
                            <td>DOB:</td><td>
                                <telerik:RadDatePicker ID="BirthdateRadDatePicker" runat="server" 
                                    DbSelectedDate='<%# Bind("Birthdate") %>' Culture="en-US" MinDate="1910-01-01">
                                </telerik:RadDatePicker>
                            </td></tr>
                        <tr>
                            <td colspan="2" style="padding-top:3px;">
                                <telerik:RadButton ID="btnUpdate" runat="server" CausesValidation="true" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                &nbsp;
                                <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </FormTemplate>
            </EditFormSettings>

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>

<asp:Panel ID="Panel2" runat="server" style="float:left; width:200px;">
    <table class="formview-table-layout">
        <tr>
            <td colspan="2"><h3>Delete Staff Member</h3>
            <p class="instruct">If a staff member is no longer employed at your facility, remove them below:</p></td>
        </tr>
        <tr>
            <td>
            <telerik:RadComboBox ID="AttendeeRadComboBox" runat="server" AppendDataBoundItems="true" DropDownAutoWidth="Enabled"  
                DataTextField="FullNameWithId" DataValueField="Id" Enabled="true" Height="200">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="0" />
                </Items>
            </telerik:RadComboBox>
            </td>
            <td><telerik:RadButton ID="DeleteAttendeeButton" 
                runat="server" CommandName="Delete" Text="Delete"  
                OnClick="DeleteAttendeeButton_Click" /></td>
        </tr>
    </table>    
</asp:Panel>
<div style="clear:both;"></div>
<br />

<div class="wizardButtons">
<div>
<telerik:RadButton ID="PreviousRadButton" runat="server" Text="Previous" 
        CausesValidation="False"  CommandName="Previous"
        onclick="PreviousRadButton_Click" ></telerik:RadButton>&nbsp;
<telerik:RadButton ID="NextRadButton" runat="server" Text="Next"
        CausesValidation="False"  CommandName="Next" onclick="NextRadButton_Click" ></telerik:RadButton>
</div>
</div>
<br /><br />
<%--
<asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
    onselecting="AttendeesDataSource_Selecting" 
    SelectMethod="GetAttendeesByOrganization" 
    TypeName="EventManager.Business.AttendeeMethods" 
    DataObjectTypeName="EventManager.Model.Attendee">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>--%>

<asp:ObjectDataSource ID="TitlesDataSource" runat="server" 
    onselecting="TitlesDataSource_Selecting" 
    SelectMethod="GetTitles" 
    TypeName="EventManager.Business.AttendeeTitleMethods">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>
