﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Events/Register/Register.master" AutoEventWireup="true" CodeBehind="Review.aspx.cs" Inherits="EventManager.Web.Facility.Events.Register.ReviewForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step4.jpg" /><br /><br />

<p class="instruct" style="width:900px;">Please review your registration details before continuing on to the Payment step.  If you have questions about your registration charges, 
please contact your association BEFORE entering your payment information.  The following things MAY affect the final price of your registration: </p>
<ul class="instruct" style="margin-left:50px;">
<li>The timing of your registration (i.e. "Earlybird" or "Regular" registration period)</li>
<li>Your facility's category</li>
<li>The member/non-member status of your facility</li>
<li>The number of attendees that will be speakers at the event (this is not common)</li>
</ul>

<table>
<tr>
<td style="vertical-align:top;">
    <h3>Facility Information</h3>
    <asp:FormView ID="OrganizationFormView" runat="server" 
        DataSourceID="OrganizationDataSource">
        <ItemTemplate>
            <table>
            <tr>
                <td class="label">Facility:</td>
                <td> <asp:Label ID="OrgNameLabel" runat="server" Text='<%# Bind("OrgName") %>' /></td>
            </tr>
            <tr>
                <td class="label">Type:</td>
                <td> <asp:Label ID="OrgTypeLabel" runat="server" Text='<%# Bind("OrganizationType.OrgTypeName") %>' /></td>
            </tr>
            <tr>
                <td class="label">Fee Category:</td>
                <td> <asp:Label ID="OrgFeeCategoryLabel" runat="server" Text='<%# Bind("OrganizationFeeCategory.CategoryDesc") %>' /> &nbsp;
                <asp:Label ID="MemberLabel" runat="server" Text="(MEMBER)" Visible='<%# (bool)Eval("IsMember") %>' ForeColor="Green" />
                <asp:Label ID="NonMemberLabel" runat="server" Text="(NON-MEMBER)" Visible='<%# !(bool)Eval("IsMember") %>' ForeColor="Red" /></td>
            </tr>
            </table>
        </ItemTemplate>
        <EmptyDataTemplate>This is an individual registration</EmptyDataTemplate>
    </asp:FormView>
    <br />
</td>
<td style="width:50px;"></td>
<td style="vertical-align:top;">
    <h3>Charge Total</h3>
    <asp:Label ID="ChargeTotalLabel" runat="server" Text="" />
    <br />
</td>
</tr>
</table>


<h3>Charge Detail</h3>

    <telerik:RadGrid ID="ChargesRadGrid" runat="server" Width="900"
        onneeddatasource="ChargesRadGrid_NeedDataSource">
        <MasterTableView AutoGenerateColumns="False">
            <Columns>
                <telerik:GridBoundColumn DataField="ChargeDesc" 
                    HeaderText="Charge Desc" 
                    SortExpression="ChargeDesc" UniqueName="ChargeDescGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridNumericColumn DataField="ChargeAmt" DecimalDigits="2" NumericType="Currency" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Amount" UniqueName="ChargeAmtGridBoundColumn">
                </telerik:GridNumericColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

    <telerik:RadListView ID="ChargesListView" runat="server" Visible="false" 
        onneeddatasource="ChargesListView_NeedDataSource" Skin="Outlook">
        <ItemTemplate>
            <tr style="">
               <td>
                    <asp:Label ID="ChargeDescLabel" runat="server" 
                        Text='<%# Eval("ChargeDesc") %>' />
                </td><td style="text-align:right;">
                    <asp:Label ID="ChargeAmtLabel" runat="server" Text='<%# Eval("ChargeAmt", "{0:c}") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <div class="RadListView RadListView_Outlook">
            <table runat="server" border="0">
                <tr runat="server">
                    <td runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="0" style="text-align:left;">
                            <tr runat="server" style="">
                                
                                <th runat="server" >
                                    Charge</th>
                                <th runat="server" style="text-align:right;">
                                    Amt</th>
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style=""></td>
                </tr>
            </table>
            </div>
        </LayoutTemplate>
    </telerik:RadListView>
<br />


<h3>Attendees</h3>
    <telerik:RadGrid ID="AttendeesRadGrid" runat="server" CellSpacing="0" Width="900" 
        DataSourceID="AttendeesDataSource" GridLines="None">
        <MasterTableView AutoGenerateColumns="False" DataSourceID="AttendeesDataSource">
            <Columns>
                <telerik:GridBoundColumn DataField="Attendee.FullName" 
                    FilterControlAltText="Filter Attendee.FullName column" HeaderText="Name" 
                    SortExpression="Attendee.FullName" UniqueName="FullNameGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Title" 
                    FilterControlAltText="Filter Title column" HeaderText="Title" 
                    SortExpression="Title" UniqueName="Title">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FlagList" 
                    FilterControlAltText="Filter FlagList column" HeaderText="Add-Ons" 
                    ReadOnly="True" SortExpression="FlagList" UniqueName="FlagList">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsSpeaker" DataType="System.Boolean" ItemStyle-Width="50"
                    FilterControlAltText="Filter IsSpeaker column" HeaderText="Speaker" 
                    ReadOnly="True" SortExpression="IsSpeaker" UniqueName="IsSpeaker">
                </telerik:GridCheckBoxColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />

<div class="wizardButtons">
<div>
<telerik:RadButton ID="PreviousRadButton" runat="server" Text="Previous"
        CausesValidation="False"  CommandName="Previous" 
        onclick="PreviousRadButton_Click" ></telerik:RadButton>&nbsp;
<telerik:RadButton ID="NextRadButton" runat="server" Text="Next"  
        CausesValidation="False"  CommandName="Next" onclick="NextRadButton_Click" ></telerik:RadButton>
</div></div>
<br /><br />

    <asp:ObjectDataSource ID="ChargesDataSource" runat="server" 
        onselecting="ChargesDataSource_Selecting" SelectMethod="GetCalculatedCharges" 
        TypeName="EventManager.Business.RegistrationMethods" 
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:Parameter Name="regId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

<asp:ObjectDataSource ID="OrganizationDataSource" runat="server" 
    SelectMethod="GetOrganization"
    TypeName="EventManager.Business.OrganizationMethods" 
    DataObjectTypeName="EventManager.Model.Organization" 
    onselecting="OrganizationDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
    onselecting="AttendeesDataSource_Selecting" 
    SelectMethod="GetRegisteredAttendees" 
    TypeName="EventManager.Business.RegistrationAttendeesMethods">
    <SelectParameters>
        <asp:Parameter Name="registrationId" Type="Int32" DefaultValue="" />
        <asp:Parameter DefaultValue="false" Name="includeCancelled" Type="Boolean" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>
