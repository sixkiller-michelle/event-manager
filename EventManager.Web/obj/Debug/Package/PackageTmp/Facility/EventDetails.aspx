﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.master" AutoEventWireup="true" CodeBehind="EventDetails.aspx.cs" Inherits="EventManager.Web.Facility.EventDetailsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
#EventDetails table td { padding:3px;}
#BoardMeetingTable td { padding:3px;}
</style>

<script type="text/javascript">  
    //<![CDATA[
    var eventIdLabel = null;

    function NavigateToRegistration() {

        alert("Navigating...");
        if (eventIdLabel != null)
            window.location.href = "RegWiz.aspx?EventId=" + eventIdLabel.Text;
    }

    function pageLoad() {

       
    }

    function OnClientNavigationComplete(sender) {

        var selectedDate = sender.get_selectedDate();
  }
    //]]
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
    <h2>Event Details</h2>
    <asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource" DefaultMode="ReadOnly" Width="950">
     
        <ItemTemplate>
        <h3><asp:Label ID="EventNameLabel" runat="server" Text='<%# Bind("EventName") %>' /></h3>
        
        <div id="RegistrationDetails" style="width:500px; float:right; background-color:#ececec; border-style:solid; border-width:thin; border-color:#828282;">
        <table style="float:left; margin:5px;" border="0">
            <tr>
                <td colspan="2"><h3>Registration Information</h3></td>
            </tr>
            <tr>
                <td>Online registration is:</td>
                <td><b><asp:Label ID="EventStatusLabel" runat="server" Text='Closed' ForeColor="Red" /></b></td>
            </tr>
            <tr>
                <td>Close date:</td>
                <td><asp:Label ID="OnlineRegEndDateTimeLabel" runat="server" Text='<%# Bind("OnlineRegEndDateTime", "{0:MM/dd/yyyy h:mm tt}") %>' /></td>
            </tr>
            <tr>
                <td>Register on-site after:</td>
                <td><asp:Label ID="OnSiteRegistrationDateTimeLabel" runat="server" Text='<%# Bind("OnSiteRegistrationDateTime", "{0:MM/dd/yyyy h:mm tt}") %>' /></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Label ID="PayByCheckLabel" runat="server" Text="Pay-by-check is allowed"></asp:Label><br /><br /></td>
            </tr>
            <tr>
                <td>Certificates Available:</td>
                <td><asp:Label ID="CertificatesAvailableStartDateLabel" runat="server" Text='<%# Bind("CertificatesAvailableStartDate", "{0:MM/dd/yyyy h:mm tt}") %>' /> - <asp:Label ID="CertificatesAvailableEndDateLabel" runat="server" Text='<%# Bind("CertificatesAvailableEndDate", "{0:MM/dd/yyyy h:mm tt}") %>' /></td>
               
            </tr>   
        </table>
        <table style="margin-left:5px; margin:5px;">
            <tr>
                <td colspan="2"><h3>Your Registration</h3></td>
            </tr>
            <tr> 
                <td>Status:</td>
                <td><asp:Label ID="RegStatusLabel" runat="server" Text='' />
            </tr>
            <tr>
                <td>Manager:</td>
                <td><asp:Label ID="EnteredByUserIdLabel" runat="server" Text='' /><br />
                    <asp:Label ID="EnteredByUserEmailLabel" runat="server" Text='' />
                </td> 
            </tr>
            <tr>
                <td colspan="2" style="padding-top:5px;">
                    <asp:HyperLink ID="RegisterHyperlink" runat="server" NavigateUrl="EventRegistration.aspx" CssClass="button">Register</asp:HyperLink>
                </td>
            </tr>
        </table>
        <br />

       
         <%--
            
            Is Registration Open:
            <asp:CheckBox ID="IsRegistrationOpenCheckBox" runat="server" 
                Checked='<%# Bind("IsRegistrationOpen") %>' Enabled="false" />
            <br />
            Is Certificate Printing Allowed:
            <asp:CheckBox ID="IsCertificatePrintingAllowedCheckBox" runat="server" 
                Checked='<%# Bind("IsCertificatePrintingAllowed") %>' Enabled="false" />
            <br />
--%>
        </div>


        <div id="EventDetails" style="float:left; width:350px;">
        <table border="0">
            <tr>
                <td><b>Date</b></td>
                <td>:</td>
                <td> <asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Bind("StartDateTime") %>' /> - <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Bind("EndDateTime") %>' /></td>
             </tr>
            <tr>
                <td><b>Type</b></td>
                <td>:</td>
                <td><asp:Label ID="TypeIdLabel" runat="server" Text='<%# Bind("EventType.TypeName") %>' /></td>
            </tr>
            <tr>
                <td><b>Location</b></td>
                <td>:</td>
                <td><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Bind("FullLocation") %>' /></td>
            </tr>
            <tr>
                <td><b>Status</b></td>
                <td>:</td>
                <td><asp:Label ID="Label1" runat="server" Text='<%# Bind("EventStatu.StatusDesc") %>' /></td>
            </tr>
             <tr style="display:none;">
                <td><b>Id</b></td>
                <td>:</td>
                <td><asp:Label ID="EventIdLabel" runat="server" Text='<%# Bind("Id") %>' /></td>
            </tr>
        </table>
        </div>

        <div style="clear:both;"></div>
        
        
        <asp:Panel ID="BoardMeetingPanel" runat="server">
            <h2>Board Meeting</h2>
            <table id="BoardMeetingTable">
            <tr>
                <td><b>Date</b></td>
                <td>:</td>
                <td> <asp:Label ID="Label5" runat="server" Text='<%# Bind("BoardMeetingStartDateTime", "{0:d}") %>' /></td>
             </tr>
            <tr>
                <td><b>Time</b></td>
                <td>:</td>
                <td> <asp:Label ID="Label2" runat="server" Text='<%# Bind("BoardMeetingStartDateTime", "{0:t}") %>' /> - <asp:Label ID="Label3" runat="server" Text='<%# Bind("BoardMeetingEndDateTime", "{0:t}") %>' /></td>
             </tr>
            <tr>
                <td><b>Location</b></td>
                <td>:</td>
                <td><asp:Label ID="Label4" runat="server" Text='<%# Bind("BoardMeetingRoom") %>' /></td>
            </tr>
        </table>

        </asp:Panel>
        <br />
        
        <h2>About the Event</h2>
        <telerik:RadEditor ID="EventDescRadEditor" runat="server" Enabled="false" Width="98%">
        </telerik:RadEditor>
        <br />

        <h2>Sessions</h2>
        <telerik:RadGrid ID="SessionsRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="SessionsDataSource" GridLines="None">
            <MasterTableView DataSourceID="SessionsDataSource">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="StartDateTime" DataFormatString="{0:d}" 
                        DataType="System.DateTime" FilterControlAltText="Filter StartDateTime column" 
                        HeaderText="Day" SortExpression="StartDateTime" UniqueName="StartDateTime">
                        <ItemStyle Width="70px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDateTime" DataFormatString="{0:t}" 
                        DataType="System.DateTime" FilterControlAltText="Filter StartDateTime column" 
                        HeaderText="Start" SortExpression="StartDateTime" UniqueName="StartDateTime">
                        <ItemStyle Width="60px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EndDateTime" DataFormatString="{0:t}" 
                        DataType="System.DateTime" FilterControlAltText="Filter EndDateTime column" 
                        HeaderText="End" SortExpression="EndDateTime" UniqueName="EndDateTime">
                        <ItemStyle Width="60px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SessionName" 
                        FilterControlAltText="Filter SessionName column" HeaderText="Session" 
                        SortExpression="SessionName" UniqueName="SessionName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Audience" 
                        FilterControlAltText="Filter Audience column" HeaderText="Audience" 
                        SortExpression="Audience" UniqueName="Audience">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CEUHours" DataType="System.Decimal" 
                        FilterControlAltText="Filter CEUHours column" HeaderText="CEUs" 
                        SortExpression="CEUHours" UniqueName="CEUHours">
                        <ItemStyle Width="50px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <NoRecordsTemplate>
                    There are no sessions for this event
                </NoRecordsTemplate>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />

        <h2>Add Ons</h2>
        <telerik:RadGrid ID="FlagsRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="FlagsDataSource" GridLines="None">
            <MasterTableView DataSourceID="FlagsDataSource">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="FlagName" 
                        FilterControlAltText="Filter FlagName column" HeaderText="Name" 
                        SortExpression="FlagName" UniqueName="FlagName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Price" DataType="System.Decimal" 
                        FilterControlAltText="Filter Price column" HeaderText="Price" DataFormatString="{0:c}"  
                        SortExpression="Price" UniqueName="Price">
                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
               <NoRecordsTemplate>
                    There are no add-ons for this event
                </NoRecordsTemplate>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
<%--
            EventFeeSchedules:
            <asp:Label ID="EventFeeSchedulesLabel" runat="server" 
                Text='<%# Bind("EventFeeSchedules") %>' />
            <br />--%>

        </ItemTemplate>
    </asp:FormView>

    <asp:ObjectDataSource ID="FlagsDataSource" runat="server" 
        SelectMethod="GetFlagsByEvent" 
        TypeName="EventManager.Business.EventFlagMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SessionsDataSource" runat="server" 
        SelectMethod="GetSessionsByEvent" 
        TypeName="EventManager.Business.EventSessionMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="EventDataSource" runat="server" 
        SelectMethod="GetEvent" TypeName="EventManager.Business.EventMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>