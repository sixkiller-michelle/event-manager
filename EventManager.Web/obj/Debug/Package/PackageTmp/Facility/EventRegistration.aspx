﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.master" AutoEventWireup="true" CodeBehind="EventRegistration.aspx.cs" Inherits="EventManager.Web.Facility.EventRegistrationForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
<script type="text/javascript">  
    //<![CDATA[
    var eventIdLabel = null;

    function NavigateToRegWizard() {

        if (eventIdLabel != null)
            window.location.href = "RegWiz.aspx?EventId=" + eventIdLabel.Text;
    }

    function pageLoad() {

    
    }

    //]]
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Registration Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<h2>Registration</h2>

<asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource" DefaultMode="ReadOnly" Width="950">
     
    <ItemTemplate>
    <h3><b><asp:Label ID="EventNameLabel" runat="server" Text='<%# Bind("EventName") %>' /></b></h3>
        
    <div id="EventDetails">
    <table border="0">
        <tr>
            <td><b>Date</b></td>
            <td>:</td>
            <td> <asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Bind("StartDateTime") %>' /> - <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Bind("EndDateTime") %>' /></td>
            </tr>
        <tr>
            <td><b>Type</b></td>
            <td>:</td>
            <td><asp:Label ID="TypeIdLabel" runat="server" Text='<%# Bind("EventType.TypeName") %>' /></td>
        </tr>
        <tr>
            <td><b>Location</b></td>
            <td>:</td>
            <td><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Bind("FullLocation") %>' /></td>
        </tr>
        <tr style="display:none;">
            <td><b>Event Id</b></td>
            <td>:</td>
            <td><asp:Label ID="EventIdLabel" runat="server" Text='<%# Bind("Id") %>' /></td>
        </tr>
    </table>
    </div>
    <br />

    </ItemTemplate>
</asp:FormView>

    <h3>Registration Information</h3>
    <telerik:RadEditor ID="RegistrationInfoRadEditor" runat="server" Enabled="false" Width="98%">
    </telerik:RadEditor>
    <br />

    <h3>Pricing</h3>
    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/fees.jpg" />

    <br />
    <br />
    <telerik:RadButton ID="RegWizardRadButton" runat="server" Text="Start Registration" PostBackUrl="" AutoPostBack="true">
    </telerik:RadButton>
 
    <br />
    <br />

<asp:ObjectDataSource runat="server" ID="EventDataSource" SelectMethod="GetEvent" TypeName="EventManager.Business.EventMethods">
    <SelectParameters>
        <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>

