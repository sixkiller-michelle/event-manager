﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.master" AutoEventWireup="true" CodeBehind="RegWiz.aspx.cs" Inherits="EventManager.Web.Facility.RegWizForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
    <style type="text/css">
        .itemHighlight
        {
            color: #000000;
            background-color: #ffff99;
        }
        
        .altItemHighlight
        {
            color: #000000;
            background-color: #ffff99;
        }
        #RegDetailsPanel h3 { margin-bottom:0px;}
        .layoutTable {margin-top:0px; padding-top:0px;}
        .layoutTable td {padding:3px;}
    </style>
 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Event Registration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[
   var rowIndex;

    function ShowAttendeeDialog(attendeeId, orgId, gridRowIndex) {

        rowIndex = gridRowIndex;
        //alert("showing attendee dialog " + attendeeId + " " + orgId);
        var wnd = window.radopen("StaffMemberDialog.aspx?AttendeeId=" + attendeeId + "&OrgId=" + orgId, "AttendeeDetailsDialog");
        wnd.setSize(850, 450);
        return false;
    }

    function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

        alert(attendeeId);
        $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(attendeeId);
    }


    //function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {
    function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

    }

    
    function onRequestStart(sender, args) {
        alert(args.get_eventTarget());
//        if (args.get_eventTarget().indexOf("InsertTrans") >= 0) {
//            args.set_enableAjax(false);
//        }
    }

      //]]>
    </script>
</telerik:RadCodeBlock>

<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" 
        DefaultLoadingPanelID="RadAjaxLoadingPanel1"
        onajaxrequest="RadAjaxManager1_AjaxRequest">
     <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="UserFormView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="UserFormView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="OrganizationFormView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OrganizationFormView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="AttendeesRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AttendeesRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewAttendeeRadButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AttendeesRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="AttendeesRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegistrationFormView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AttendeesRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RegisteredAttendeesListView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegisteredAttendeesListView" />
                    <telerik:AjaxUpdatedControl ControlID="RegistrationFormView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
</telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />


<asp:Panel runat="server" ID="EventDetailsPanel" style="float:left; margin:10px;">
<asp:FormView ID="EventFormView" runat="server" DataSourceID="EventDataSource" DefaultMode="ReadOnly" Width="600">
    <ItemTemplate>
    <h3 style="margin-bottom:3px; margin-top:0px;"><asp:Label ID="EventNameLabel" runat="server" Text='<%# Bind("EventName") %>' /></h3>
        
    <div id="EventDetails">
    <table border="0" class="formview-table-layout">
        <tr>
            <td><b>Date</b></td>
            <td>:</td>
            <td> <asp:Label ID="StartDateTimeLabel" runat="server" Text='<%# Bind("StartDateTime") %>' /> - <asp:Label ID="EndDateTimeLabel" runat="server" Text='<%# Bind("EndDateTime") %>' /></td>
            </tr>
        <tr>
            <td><b>Type</b></td>
            <td>:</td>
            <td><asp:Label ID="TypeIdLabel" runat="server" Text='<%# Bind("EventType.TypeName") %>' /></td>
        </tr>
        <tr>
            <td><b>Location</b></td>
            <td>:</td>
            <td><asp:Label ID="FullLocationLabel" runat="server" Text='<%# Bind("FullLocation") %>' /></td>
        </tr>
        <tr style="display:none;">
            <td><b>Event Id</b></td>
            <td>:</td>
            <td><asp:Label ID="EventIdLabel" runat="server" Text='<%# Bind("Id") %>' /></td>
        </tr>
    </table>
   </ItemTemplate>
</asp:FormView>
</asp:Panel>

<asp:Panel runat="server" ID="RegDetailsPanel" style="float:left; margin:10px;">
<asp:FormView ID="RegistrationFormView" runat="server" 
        DataSourceID="RegistrationDataSource" 
        ondatabound="RegistrationFormView_DataBound">
        <ItemTemplate>
        <table style="" class="formview-table-layout">
            <tr>
                <td colspan="2"><h3>Your Registration Summary</h3></td>
            </tr>
            <tr>
                <td>Facility:</td>
                <td><asp:Label ID="OrgNameLabel" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        </ItemTemplate>
        <InsertItemTemplate>
        <table style="" class="formview-table-layout">
            <tr>
                <td colspan="2"><h3>Your Registration</h3></td>
            </tr>
            <tr>
                <td>Facility:</td>
                <td><asp:Label ID="OrgNameLabel" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr> 
                <td colspan="2"><asp:Label ID="NewRegLabel" runat="server" Text="You are creating a new registration" ForeColor="Green"></asp:Label></td> 
            </tr>
        </table>
        </InsertItemTemplate>
        <EditItemTemplate>
        <div style="background-color:#fafcc4; border-style:solid; border-color:#999999; border-width:1px;">
        <table style="margin:5px;" class="formview-table-layout">
            <tr>
                <td colspan="2"><h3 style="margin-bottom:0px; margin-top:0px;">Your Registration Summary</h3></td>
            </tr>
            <tr>
                <td><b>Facility:</b></td>
                <td><asp:Label ID="OrgNameLabel" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr> 
                <td><b>Attendees:</b></td>
                <td><asp:Label ID="AttendeeCountLabel" runat="server" Text="0" ForeColor="Green"></asp:Label></td> 
            </tr>
            <tr> 
                <td><b>Cost:</b></td>
                <td><asp:Label ID="RegCostLabel" runat="server" Text="$0.00"></asp:Label></td> 
            </tr>
        </table>
        </div>
        </EditItemTemplate>
</asp:FormView>
</asp:Panel>

<div style="clear:both;">&nbsp;</div>

<div style="width: 100%;">

<div id="wizard" style="width:900px; text-align:left;">
<asp:Wizard ID="Wizard1" runat="server" CellPadding="5" ActiveStepIndex="2" 
        DisplaySideBar="false" Width="900" 
        onnextbuttonclick="Wizard1_NextButtonClick" 
        onfinishbuttonclick="Wizard1_FinishButtonClick" 
        onactivestepchanged="Wizard1_ActiveStepChanged" >
    
 <WizardSteps>
        <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1: Verify your contact information">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Step1.jpg" /><br /><br />

        <div id="YourInformation" style="width:300px; float:left; text-align:left; margin-left:30px;">
        <b>Your Information</b>
        <asp:UpdatePanel ID="UserInfoUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:FormView ID="UserFormView" runat="server" 
                DataSourceID="AttendeeDataSource" DataKeyNames="Id" 
                onitemupdated="UserFormView_ItemUpdated">
                <EditItemTemplate>
                    <table style="width: 100%;" class="formview-table-layout">
                        <tr>
                            <td class="label">First Name:</td>
                            <td><telerik:RadTextBox ID="FirstNameTextBox" runat="server"  Text='<%# Bind("FirstName") %>' /></td>
                        </tr>
                         <tr>
                            <td class="label">Last Name:</td>
                            <td><telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' /></td>
                        </tr>
                         <tr>
                            <td class="label">Middle Initial:</td>
                            <td><telerik:RadTextBox ID="MiddleInitialTextBox" runat="server"  Text='<%# Bind("MiddleInitial") %>' /></td>
                        </tr>
                         <tr>
                            <td class="label">Email:</td>
                            <td><telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Phone:</td>
                            <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                        </tr>
                    </table>
                    <br /><br />

                    <telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="Update" />
                    <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False"  CommandName="Cancel" />
                </EditItemTemplate>
            
                <ItemTemplate>
                    <table class="formview-table-layout">
                        <tr>
                            <td class="label">Name:</td>
                            <td><asp:Label ID="FirstNameTextBox" runat="server"  Text='<%# Bind("FullName") %>' /></td>
                        </tr>
                         <tr>
                            <td class="label">Email:</td>
                            <td><asp:Label ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' /></td>
                        </tr>
                        <tr>
                            <td class="label">Phone:</td>
                            <td><asp:Label ID="TextBox1" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                        </tr>
                    </table>
                    <br />
                    <telerik:RadButton ID="EditRadButton" runat="server" Text="Edit" CausesValidation="False"  CommandName="Edit" >
                    </telerik:RadButton>
                </ItemTemplate>
            </asp:FormView>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        
        <div style="width:400px; float:left; margin-left:30px; text-align:left;">

        <b>Your Facility</b>
        <asp:FormView ID="OrganizationFormView" runat="server" DataSourceID="OrganizationDataSource" DataKeyNames="Id"
             onitemupdated="OrganizationFormView_ItemUpdated">

            <EditItemTemplate>
                <table  class="formview-table-layout">
                    <tr>
                        <td colspan="2"><b><asp:Label ID="OrgNameTextBox" runat="server"  Text='<%# Bind("OrgName") %>' /></b></td>
                    </tr>
                     <tr>
                        <td class="label">Street:</td>
                        <td><telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Address") %>'  /></td>
                    </tr>
                    <tr>
                        <td class="label">City:</td>
                        <td><telerik:RadTextBox ID="TextBox1" runat="server" Text='<%# Bind("City") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">State:</td>
                        <td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="200">
                            </telerik:RadComboBox></td>
                    </tr>
                    <tr>
                        <td class="label">Zip:</td>
                        <td><telerik:RadTextBox ID="TextBox3" runat="server" Text='<%# Bind("Zip") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><telerik:RadTextBox ID="TextBox4" runat="server" Text='<%# Bind("Phone") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Fax:</td>
                        <td><telerik:RadTextBox ID="TextBox5" runat="server" Text='<%# Bind("Fax") %>' /></td>
                    </tr>
                </table>
                <br /><br />
                <telerik:RadButton ID="UpdateButton" runat="server" Text="Update" CausesValidation="True"  CommandName="Update" />
                <telerik:RadButton ID="UpdateCancelButton" runat="server" Text="Cancel" CausesValidation="False"  CommandName="Cancel" />

            </EditItemTemplate>
            
            <ItemTemplate>
                <table class="formview-table-layout">
                    <tr>
                        <td class="label">Facility:</td>
                        <td><asp:Label ID="OrgNameTextBox" runat="server"  Text='<%# Bind("OrgName") %>' /></td>
                    </tr>
                     <tr>
                        <td class="label">Address:</td>
                        <td><asp:Label ID="EmailTextBox" runat="server" Text='<%# Bind("FullAddress") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><asp:Label ID="TextBox4" runat="server" Text='<%# Bind("Phone") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Fax:</td>
                        <td><asp:Label ID="TextBox5" runat="server" Text='<%# Bind("Fax") %>' /></td>
                    </tr>
                </table>
                <br />
                <telerik:RadButton ID="EditRadButton" runat="server" Text="Edit" CausesValidation="False"  CommandName="Edit" >
                </telerik:RadButton>
            </ItemTemplate>

        </asp:FormView>
        </div>

        </asp:WizardStep>


        <asp:WizardStep ID="WizardStep2" runat="server" Title="Step 2: Add the attendees" >
        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Step2.jpg" /><br /><br />

        <div id="AttendeesGrid" style="width:400px; margin-right:20px; float:left;" >

            <div style="float:left; width:300px; margin-bottom:0px;">
                <asp:Label ID="SelectedAttendeesLabel" runat="server" Text="Selected attendees: "></asp:Label>
                <asp:Label ID="SelectedAttendeesCount" runat="server" Text="0" ForeColor="Green" Font-Size="Large"></asp:Label>
            </div>
            <div style="float:left; width:100px; text-align:right;">
                <telerik:RadButton runat="server" ID="NewAttendeeRadButton" Text="Add Attendee" AutoPostBack="false" 
                    UseSubmitBehavior="false" Style="margin:5px;"></telerik:RadButton>
            </div>
            <div style="clear:both;"></div>
            <telerik:RadGrid ID="AttendeesRadGrid" runat="server" AutoGenerateColumns="False"  
                CellSpacing="0" DataSourceID="AttendeesDataSource" GridLines="None"  AllowMultiRowSelection="true"
                OnItemCreated="AttendeesRadGrid_ItemCreated" 
                OnItemCommand="AttendeesRadGrid_ItemCommand" 
                OnDataBound="AttendeesRadGrid_DataBound" >
             <ClientSettings EnableRowHoverStyle="true">
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <SelectedItemStyle CssClass="SelectedItem" />
            <MasterTableView DataSourceID="AttendeesDataSource" DataKeyNames="Id">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>

            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>

                <Columns>
                     <telerik:GridTemplateColumn UniqueName="SelectAttendeeTemplateColumn" HeaderText="Select">
                        <ItemTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:CheckBox ID="SelectedAttendeeCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="CheckedChanged" />
                            </asp:Panel>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="FullName" 
                        FilterControlAltText="Filter FullName column" HeaderText="Name" 
                        ReadOnly="True" SortExpression="FullName" UniqueName="FullName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Title" 
                        FilterControlAltText="Filter Title column" HeaderText="Title" 
                        SortExpression="Title" UniqueName="Title">
                    </telerik:GridBoundColumn>

                     <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ItemStyle-Width="30" HeaderStyle-Width="30"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Remove"
                        UniqueName="DeleteRegistrationColumn" 
                        ConfirmTextFields="FullName" 
                        ConfirmTextFormatString="Are you sure you want to remove {0} from your staff?">
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                    </telerik:GridButtonColumn>
                </Columns>
                

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>
            
       </div>
        <asp:Panel ID="Panel2" runat="server" style="float:left; width:200px;">
            <table class="formview-table-layout">
                <tr>
                    <td colspan="2"><h3>Delete Staff Member</h3>
                    <p class="instruct">If a staff member is no longer employed at your facility, remove them below:</p></td>
                </tr>
                <tr>
                    <td>
                    <telerik:RadComboBox ID="AttendeeRadComboBox" runat="server" AppendDataBoundItems="true" 
                        DataTextField="FullNameWithId" DataValueField="Id" Enabled="true">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="0" />
                        </Items>
                    </telerik:RadComboBox>
                    </td>
                    <td><telerik:RadButton ID="DeleteAttendeeButton" 
                        runat="server" CommandName="Delete" Text="Delete"  
                        OnClick="DeleteAttendeeButton_Click" /></td>
                </tr>
            </table>    
        </asp:Panel>
        <div style="clear:both;"></div>
        </asp:WizardStep>

        <asp:WizardStep ID="WizardStep3" runat="server" Title="Step 3: Select add-ons">
        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Step3.jpg" /><br /><br />

            <div style="width:200px; float:left; margin-right:20px;">
                <telerik:RadGrid ID="EventFlagsRadGrid" runat="server" AutoGenerateColumns="False"
                    DataSourceID="EventFlagsDataSource">
                    <MasterTableView DataSourceID="EventFlagsDataSource">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FlagName" FilterControlAltText="Filter FlagName column"
                                HeaderText="Add On" SortExpression="FlagName" UniqueName="FlagName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Price" DataType="System.Decimal" FilterControlAltText="Filter Price column"
                                DataFormatString="{0:C}" HeaderText="Price" SortExpression="Price" UniqueName="Price">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <NoRecordsTemplate>
                            There are no add-ons for this event</NoRecordsTemplate>
                    </MasterTableView>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </div>
            
            <div style="width:500px; float:left;">
            
                <p class="instruct">For each registered staff member, select which add-ons apply to them:</p>
                <telerik:RadListView ID="RegisteredAttendeesListView" runat="server" Enabled="true" AllowMultiItemEdit="true" 
                    DataSourceID="RegisteredAttendeesDataSource" DataKeyNames="Id" ItemPlaceholderID="itemPlaceholderContainer" 
                    OnItemDataBound="RegisteredAttendeesListView_ItemDataBound">
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td>You have not registered any staff members for this event.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                     <LayoutTemplate>
                        <table class="layoutTable">
                            <%--<tr>
                                <th>Name</th>
                                <th>Title</th>
                            </tr>--%>
                            <asp:PlaceHolder ID="itemPlaceholderContainer" runat="server"></asp:PlaceHolder>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Attendee.FullName") %>' Font-Size="Large" Font-Bold="true" />
                            </td>
                            <td>
                                <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' Font-Italic="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-left:20px;">
                              <asp:CheckBoxList ID="AttendeeFlagsCheckBoxList" runat="server" AutoPostBack="true" 
                                    DataTextField="FlagName" DataValueField="Id" 
                                    onselectedindexchanged="AttendeeFlagsCheckBoxList_SelectedIndexChanged" >
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <br />
                    </ItemTemplate>
                </telerik:RadListView>
            </div>
            <div style="clear:both;"></div>
            <asp:ObjectDataSource ID="RegisteredAttendeesDataSource" runat="server" 
                OnSelecting="RegisteredAttendeesDataSource_Selecting" 
                SelectMethod="GetRegisteredAttendees" 
                TypeName="EventManager.Business.RegistrationAttendeesMethods">
                <SelectParameters>
                    <asp:Parameter DefaultValue="" Name="registrationId" Type="Int32" />
                    <asp:Parameter DefaultValue="false" Name="includeCancelled" Type="Boolean" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <asp:ObjectDataSource ID="EventFlagsDataSource" runat="server" 
                OnSelecting="EventFlagsDataSource_Selecting" 
                SelectMethod="GetFlagsByEvent" 
                TypeName="EventManager.Business.EventFlagMethods">
                <SelectParameters>
                    <asp:Parameter DefaultValue="" Name="eventId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </asp:WizardStep>

        <asp:WizardStep ID="WizardStep4" runat="server" Title="Step 4: Review">
         <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Step4.jpg" /><br /><br />

            <h3>People Attending:</h3>
            <telerik:RadListView ID="ReviewAttendeesRadListView" runat="server" Enabled="true" Style="margin-top:0px;"  
                    DataSourceID="RegisteredAttendeesDataSource" DataKeyNames="Id" ItemPlaceholderID="itemPlaceholderContainer" >
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td>You have not registered any staff members for this event.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                     <LayoutTemplate>
                        <table class="layoutTable">
                            <tr>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Add-Ons</th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceholderContainer" runat="server"></asp:PlaceHolder>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Attendee.FullName") %>' />
                            </td>
                            <td>
                                <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' Font-Italic="true" />
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("FlagList") %>' />
                            </td>
                        </tr>
                        <br />
                    </ItemTemplate>
                </telerik:RadListView>
                <br />

                <h3>Cost Summary:</h3>
                <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="0" GridLines="None" ShowFooter="True"  
                    OnNeedDataSource="RadGrid1_NeedDataSource">
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="Id" TableLayout="Fixed">
                        <NoRecordsTemplate>
                            There are no charges for this registration
                        </NoRecordsTemplate>
                        <CommandItemSettings ExportToPdfText="Export to PDF">
                        </CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>
                        <Columns>
                            
                            <telerik:GridBoundColumn DataField="ChargeDesc" FilterControlAltText="Filter ChargeDesc column" ItemStyle-Width="100" HeaderStyle-Width="100"
                                HeaderText="Charge" ReadOnly="True" SortExpression="ChargeDesc" UniqueName="ChargeDesc">
                            </telerik:GridBoundColumn>
                            <telerik:GridNumericColumn Aggregate="Sum" FooterAggregateFormatString="{0:C}" FooterStyle-HorizontalAlign="Right" FooterStyle-Width="60" DataField="ChargeAmt"
                                HeaderText="Amt" DataType="System.Decimal" SortExpression="ChargeAmt" UniqueName="ChargeAmt"
                                DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60"
                                HeaderStyle-Width="60px" FilterControlWidth="60px">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                            </telerik:GridNumericColumn>
                           
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="false"  UseStaticHeaders="true" />
                        <Resizing AllowResizeToFit="True" EnableRealTimeResize="True" />
                    </ClientSettings>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>

        <p>&nbsp;</p>
        </asp:WizardStep>

        <asp:WizardStep ID="WizardStep5" runat="server" Title="Step 5: Payment">
         <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Step5.jpg" /><br /><br />
        <p>&nbsp;</p>
        </asp:WizardStep>

    </WizardSteps>
    <StartNavigationTemplate>
        <telerik:RadButton ID="NextButton" runat="server" Text="Next >>"
                    CausesValidation="true" ValidationGroup="CreateFacilityEmployeeValidationGroup"  
                    CommandName="MoveNext"  />&nbsp;

        <telerik:RadButton ID="CancelButton" runat="server" Text="Cancel"
                    CausesValidation="false"  
                    OnClientClicked="return confirm('Are you sure you want to cancel your registration?');" 
                    OnClick="CancelButton_Click"  />
    </StartNavigationTemplate>
    <StepNavigationTemplate>
        `<hr style="color:#e99719;width:5px; line-height:5px;" />
        <telerik:RadButton ID="PreviousButton" runat="server" Text="<< Previous"
                    CausesValidation="false" 
                    CommandName="MovePrevious"  />&nbsp;

        <telerik:RadButton ID="NextButton" runat="server" Text="Next >>"
                    CausesValidation="true" ValidationGroup="CreateFacilityEmployeeValidationGroup"  
                    CommandName="MoveNext"  />&nbsp;

        <telerik:RadButton ID="CancelButton" runat="server" Text="Cancel"
                    CausesValidation="false" 
                    OnClientClicked="return confirm('Are you sure you want to cancel your registration?');" 
                    OnClick="CancelButton_Click"  />
      
    </StepNavigationTemplate>
</asp:Wizard>
</div>
</div>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" EnableShadow="True">
    <Windows>
        <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
            Title="Edit Attendee" Height="500px"
            Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
            Modal="true" DestroyOnClose="True" Overlay="True" />
    </Windows>
</telerik:RadWindowManager>

<asp:ObjectDataSource ID="OrganizationDataSource" runat="server" 
    SelectMethod="GetOrganization" UpdateMethod="Update" 
    TypeName="EventManager.Business.OrganizationMethods" 
    DataObjectTypeName="EventManager.Model.Organization" 
    onselecting="OrganizationDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="RegistrationDataSource" runat="server" 
    SelectMethod="GetRegistration" onselecting="RegistrationDataSource_Selecting"
    TypeName="EventManager.Business.RegistrationMethods">
    <SelectParameters>
            <asp:Parameter Name="registrationId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
    onselecting="AttendeesDataSource_Selecting" 
    SelectMethod="GetAttendeesByOrganization" 
    TypeName="EventManager.Business.AttendeeMethods">
    <SelectParameters>
        <asp:Parameter Name="orgId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="AttendeeDataSource" 
    DataObjectTypeName="EventManager.Model.Attendee" 
    onselecting="AttendeeDataSource_Selecting" SelectMethod="GetAttendeeByUserId" 
    TypeName="EventManager.Business.AttendeeMethods" UpdateMethod="Update">
    <SelectParameters>
        <asp:Parameter DbType="Guid" Name="userId" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="EventDataSource" SelectMethod="GetEvent" 
    TypeName="EventManager.Business.EventMethods" onselecting="EventDataSource_Selecting">
<SelectParameters>
    <asp:Parameter Name="eventId" Type="Int32" />
</SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="StatesDataSource" runat="server" 
    SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
</asp:ObjectDataSource>

</asp:Content>
