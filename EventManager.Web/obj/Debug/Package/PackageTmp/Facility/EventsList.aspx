﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/Facility.master" AutoEventWireup="true" CodeBehind="EventsList.aspx.cs" Inherits="EventManager.Web.Facility.EventsForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">

<style type="text/css">
.rcEventDay
{ background-color:Yellow;
  font-weight:bold;
  color:Red;}
</style>

<script type="text/javascript">  
    //<![CDATA[
    var scheduler = null;

    function onAppointmentClick(sender, eventArgs) {

        //alert("Appointment click: " + eventArgs.get_appointment().get_id());
        window.location.href = "EventDetails.aspx?EventId=" + eventArgs.get_appointment().get_id().toString();
    }
   
    function pageLoad() {

        //scheduler = $find('<%=RadScheduler1.ClientID %>');
    }

    function OnClientNavigationComplete(sender) {

        var selectedDate = sender.get_selectedDate();

        //calendar1.navigateToDate([selectedDate.format("yyyy"), selectedDate.format("MM"), selectedDate.format("dd")]);
        //calendar2.navigateToDate([selectedDate.format("yyyy"), parseInt(selectedDate.format("MM")) + 1, selectedDate.format("dd")]);

    }

    //]]
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
Events List
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <telerik:RadSkinManager ID="RadSkinManager2" Runat="server">
    </telerik:RadSkinManager>

   <%-- <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadCalendar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadCalendar1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>--%>

    <h2>Upcoming Events</h2>
<%--
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <telerik:RadScheduler runat="server" ID="RadScheduler1" AllowDelete="False" EnableViewState="false" 
            AllowEdit="False" AllowInsert="False" OnClientNavigationComplete="OnClientNavigationComplete"
            DataStartField="StartDateTime" DataEndField="EndDateTime" 
            DataKeyField="Id" DataSubjectField="EventName" SelectedView="MonthView" 
            ShowViewTabs="False">
            <monthview showresourceheaders="False" />
        </telerik:RadScheduler>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br />
--%>
  <%--  <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>--%>

     <%--   <telerik:RadCalendar ID="RadCalendar1" runat="server" AutoPostBack="true" MultiViewColumns="3"
            MultiViewRows="2" EnableMultiSelect="false" OnPreRender="RadCalendar1_PreRender" OnDayRender="CustomizeDay" OnSelectionChanged="SelectedDateChange">
        </telerik:RadCalendar>
        <br />--%>

        <telerik:RadGrid ID="EventsRadGrid" runat="server" 
            OnItemCommand="EventsRadGrid_RowCommand" AutoGenerateColumns="False" 
            EnableViewState="False" CellSpacing="0" GridLines="None" >
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" 
                    HeaderText="Event Date" UniqueName="StartDateTime" HeaderStyle-Width="100" 
                    ItemStyle-Width="100" >
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EventName" DataType="System.String" 
                    HeaderText="Event Name" UniqueName="EventName" HeaderStyle-Width="400" 
                    ItemStyle-Width="400" >
                    <HeaderStyle Width="400px" />
                    <ItemStyle Width="400px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="EventTypeTemplateColumn" DataField="TypeId" HeaderText="Type" SortExpression="TypeId"  HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <%# Eval("EventType.TypeName") %>
                    </ItemTemplate>
                    <HeaderStyle Width="120px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridTemplateColumn>
                <telerik:GridHyperLinkColumn FilterControlAltText="Filter column1 column" Text="Details"  
                   UniqueName="column1" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="~/Facility/EventDetails.aspx?EventId={0}">
                </telerik:GridHyperLinkColumn>
            </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
<%--
    </ContentTemplate>
    </asp:UpdatePanel>
    <br />

    
--%>
</asp:Content>