﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="Associations.aspx.cs" Inherits="EventManager.Web.Association.Corporate.AssociationsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadGrid 
    ID="associations_RadGrid" 
    runat="server" 
    AllowAutomaticDeletes="True" 
    AllowAutomaticInserts="True" 
    AllowAutomaticUpdates="True" 
    CellSpacing="0" 
    DataSourceID="associations_ODS" 
    GridLines="None">

    <MasterTableView 
        AutoGenerateColumns="False" 
        DataSourceID="associations_ODS"
        CommandItemDisplay="Top">

        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridBoundColumn DataField="AssociationName" FilterControlAltText="Filter AssociationName column" HeaderText="AssociationName" SortExpression="AssociationName" UniqueName="AssociationName">
                </telerik:GridBoundColumn>

                <telerik:GridHyperLinkColumn 
                    DataTextFormatString="{0}" 
                    FilterControlAltText="Filter UserCount column"
                    HeaderText="User Count" 
                    SortExpression="UserCount" 
                    DataNavigateUrlFields="AssociationId"
                    DataNavigateUrlFormatString="~/Corporate/AssociationUsers.aspx?assnId={0}" 
                    UniqueName="UserCount" 
                    DataTextField="UserCount">

                </telerik:GridHyperLinkColumn>

                <telerik:GridTemplateColumn DataField="FullLocation" FilterControlAltText="Filter FullLocation column" HeaderText="Address" 
                    SortExpression="PhoneNumber" UniqueName="FullLocationGridTemplateColumn">
                    <ItemTemplate>
                        <asp:Label ID="FullLocationLabel" runat="server" Text='<%# Eval("FullLocation") %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn HeaderText="Phone/Fax"  UniqueName="PhoneGridTemplateColumn">
                    <ItemTemplate>
                        Phone: <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label><br />
                        Fax: <asp:Label ID="FaxNumberLabel" runat="server" Text='<%# Eval("FaxNumber") %>'></asp:Label><br />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ItemStyle-Width="30" HeaderStyle-Width="30"
                    ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                    UniqueName="DeleteCommandColumn" ConfirmTextFields="AssociationName" ConfirmTextFormatString="Are you sure you want to delete association '{0}'?">
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>


                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    <ItemStyle CssClass="MyImageButton"></ItemStyle>
                </telerik:GridEditCommandColumn>

            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <FilterMenu EnableImageSprites="False"></FilterMenu>
</telerik:RadGrid>

        <asp:ObjectDataSource 
            ID="associations_ODS" 
            runat="server" 
            DataObjectTypeName="EventManager.Model.Association" 
            DeleteMethod="Delete" 
            InsertMethod="Add" 
            SelectMethod="GetAssociations" 
            TypeName="EventManager.Business.AssociationMethods" 
            UpdateMethod="UpdateAssociation">
           
        </asp:ObjectDataSource>

</asp:Content>
 