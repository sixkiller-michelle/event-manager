﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Http404ErrorPage.aspx.cs" Inherits="EventManager.Web.Http404ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<%--
The following example shows the Http404ErrorPage.aspx page. ASP.NET redirects unhandled HTTP 404 (file not found) errors to this page. 
The page displays the same message to remote and local users.
--%>
<div>
    <h2>
      HTTP 404 Error Page</h2>
    Standard error message suitable for file not found errors. 
    The original exception object is not available, 
    but the original requested URL is in the query string.<br />
    <br />
    Return to the <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Default Page</asp:HyperLink>
  </div>


</asp:Content>
