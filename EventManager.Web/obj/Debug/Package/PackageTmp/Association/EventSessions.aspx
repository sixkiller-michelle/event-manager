﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventSessions.aspx.cs" Inherits="EventManager.Web.Association.EventSessions" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <style type="text/css">
        #<%= SessionsRadGridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        <!--
        var rowIndex;
        var speakersWindow;

//        function onRequestStart(sender, args) {
//            if (args.get_eventTarget().indexOf("Insert") >= 0 ||
//                    args.get_eventTarget().indexOf("Update") >= 0) {
//                args.set_enableAjax(false);
//            }
//        }

        function ShowSpeakersDialog(sessionId, gridRowIndex) {

            rowIndex = gridRowIndex;
            speakersWindow = window.radopen("EditSpeakersDialog.aspx?SessionId=" + sessionId, "EditSpeakersDialog");
            return false;
        }

        function ShowAttendeeDialog(attendeeId, gridRowIndex) {

            var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
            wnd.setSize(700, 450);
            return false;
        }

        function RefreshSessionGrid(speakerString) {

            // Update the session row with the new speaker string
            var editedDataItem = $find("<%= SessionsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            editedDataItem.get_cell("SpeakerStringTemplateColumn").children[0].innerText = speakerString;
        }

        function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

            // Add the new attendee to the RadListBox
            var listbox = $find("<%= SpeakersRadListBox.ClientID %>");
            var item = new Telerik.Web.UI.RadListBoxItem();
            item.set_text(fullNameWithId);
            item.set_value(attendeeId);
            listbox.trackChanges();
            listbox.get_items().add(item);
            item.select();
            listbox.commitChanges();

            // Set the attendee drop down index to 0

        }
        -->
    </script>

</telerik:RadCodeBlock>


<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxManager runat="server" ID="RadAjaxManager1" DefaultLoadingPanelID="RadAjaxLoadingPanel1" OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated">
<ClientEvents  />
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="SessionsRadGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SessionsRadGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
     <%--   <telerik:AjaxSetting AjaxControlID="CreateSessionRadButton">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="SessionsRadGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>--%>
        <telerik:AjaxSetting AjaxControlID="ActionsRadMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadSplitter1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CreateSessionRadButton">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadSplitter1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        
    </AjaxSettings>
</telerik:RadAjaxManager>

    <telerik:RadMenu ID="ActionsRadMenu" runat="server" Skin="Default" 
        onitemclick="ActionsRadMenu_ItemClick" Width="100%">
    <Items>
        <telerik:RadMenuItem runat="server" Text="New" PostBack="true"></telerik:RadMenuItem>
        <telerik:RadMenuItem runat="server" Text="Print Session List" PostBack="true"></telerik:RadMenuItem>
        <telerik:RadMenuItem runat="server" Text="Print Speaker Badges" PostBack="true"></telerik:RadMenuItem>
    </Items>
    </telerik:RadMenu>
    <br />

    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Horizontal">
    <telerik:RadPane ID="NewSessionRadPane" runat="server" Scrolling="Both" Height="270">
            <table style="border:none; margin:5px 0px 5px 10px;" class="formview-table-layout">
                <tr>
                    <td>
                    <asp:Label ID="Label10" runat="server" Text="Session Name:"></asp:Label>&nbsp;</td>
                    <td><asp:TextBox ID="SessionNameTextBox1" runat="server" Width="600px" />&nbsp;</td>
                </tr>
            </table>
            <table style="border:none; float:left; margin:5px 0px 5px 10px;" class="formview-table-layout">
                <tr>
                    <td><asp:Label ID="Label11" runat="server" Text="Start:"></asp:Label>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="StartDateRadDateTimePicker1" runat="server"
                            OnSelectedDateChanged="StartDateRadDateTimePicker_SelectedDateChanged" 
                            Culture="en-US">
                            <TimeView CellSpacing="-1" Columns="8" Interval="00:15:00" StartTime="04:00:00">
                            </TimeView>
                            <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                ViewSelectorText="x">
                            </Calendar>
                            <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                            </DateInput>
                            <DatePopupButton HoverImageUrl="" ImageUrl="" />
                        </telerik:RadDateTimePicker>
                    
                   <%-- <telerik:RadDateTimePicker ID="StartDateRadDateTimePicker1" runat="server" 
                        OnSelectedDateChanged="StartDateRadDateTimePicker_SelectedDateChanged" 
                            Culture="en-US">
                        <TimeView CellSpacing="-1" Interval="00:15:00" Visible="False">
                        </TimeView>
                        <TimePopupButton HoverImageUrl="" ImageUrl="" />
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDateTimePicker>--%>&nbsp;</td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label12" runat="server" Text="End:"></asp:Label>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="EndDateRadDateTimePicker1" runat="server" 
                            Culture="en-US">
                        <TimeView runat="server" CellSpacing="-1" Interval="00:15:00" Columns="8" 
                            StartTime="04:00:00">
                        </TimeView>
                        <TimePopupButton HoverImageUrl="" ImageUrl="" />
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDateTimePicker>&nbsp;</td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label13" runat="server" Text="Alt End:"></asp:Label>&nbsp;</td>
                    <td>
                        <telerik:RadDateTimePicker ID="AltEndDateRadDateTimePicker1" runat="server" 
                            Culture="en-US">
                        <TimeView runat="server" CellSpacing="-1" Interval="00:15:00">
                        </TimeView>
                        <TimePopupButton HoverImageUrl="" ImageUrl="" />
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                            ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDateTimePicker>&nbsp;</td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label14" runat="server" Text="CEUs:"></asp:Label>&nbsp;</td>
                    <td><telerik:RadNumericTextBox ID="CeusTextBox1" runat="server" Width="50">
                    </telerik:RadNumericTextBox></td>
                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><asp:CheckBox ID="OneScanCheckBox1" runat="server" Text="Only 1 scan required" /></td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td colspan="2">
                    <telerik:RadButton ID="CreateSessionRadButton" runat="server" Text="Create" UseSubmitBehavior="true" onclick="CreateSessionRadButton_Click" />&nbsp;
                    <telerik:RadButton ID="CancelSessionRadButton" runat="server" Text="Cancel" onclick="CancelSessionRadButton_Click" />
                    <telerik:RadButton ID="NewRegDoneRadButton" runat="server" Text="Close" OnClientClicked="CollapseNewSessionPane" style="margin-left:100px;" />
                    </td>
                </tr>
                </table>
            <table class="formview-table-layout" style="margin-left:20px; float:left;">
            <tr>
                <td>
                <asp:Label ID="Label15" runat="server" Text="Location:"></asp:Label>&nbsp;</td>
                <td><asp:TextBox ID="LocationTextBox1" runat="server" Width="150" />&nbsp;</td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="AudienceLabel" runat="server" Text="Audience:"></asp:Label>&nbsp;</td>
                <td><asp:TextBox ID="AudienceTextBox1" runat="server" Width="150" />&nbsp;</td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="Label17" runat="server" Text="Host:"></asp:Label>&nbsp;</td>
                <td><asp:TextBox ID="HostTextBox1" runat="server" Width="150" />&nbsp;</td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="Label18" runat="server" Text="Scanner Operator:"></asp:Label>&nbsp;</td>
                <td><asp:TextBox ID="ScannerTextBox1" runat="server" Width="150" />&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                        
                </td>
            </tr>
            </table>
            <table style="border:none;">
            <tr>
                <td colspan="2"><b>Speakers</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <telerik:RadListBox ID="SpeakersRadListBox" runat="server" Width="230" 
                        AllowDelete="true" AllowReorder="false" AllowTransfer="false" Height="70">
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox
                        ID="NewSpeakerRadComboBox" runat="server"
                        AllowCustomText="false" MarkFirstMatch="true"  
                        DataTextField="FullNameWithId" AppendDataBoundItems="true"
                        DataValueField="Id" EmptyMessage="Attendee name (last, first)..." 
                        Width="200px">
                        <Items>
                            <telerik:RadComboBoxItem Text="" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>&nbsp;
                    <telerik:RadButton ID="InsertSpeakerRadButton" runat="server" Text="Add" 
                        AutoPostBack="true" onclick="InsertSpeakerRadButton_Click" UseSubmitBehavior="true" ></telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="vertical-align:top;">
                    <asp:LinkButton ID="NewAttendeeLinkButton" runat="server" Text="Create new attendee" />
                </td>
            </tr>
            </table>
               
    </telerik:RadPane>

    <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>

    <telerik:RadPane ID="RegGridRadPane" runat="server" OnClientResized="SessionGridPaneResized" EnableViewState="true" Height="100%" Scrolling="None">
        <telerik:RadGrid ID="SessionsRadGrid" runat="server" AllowSorting="True" 
            AutoGenerateColumns="False" Height="100%"
                AllowAutomaticUpdates="True"
                OnSelectedIndexChanged="SessionsRadGrid_SelectedIndexChanged" allowmultirowselection="True"  
                onitemdatabound="SessionsRadGrid_ItemDataBound" 
                onitemcreated="SessionsRadGrid_ItemCreated" 
                ondeletecommand="SessionsRadGrid_DeleteCommand" 
                oninsertcommand="SessionsRadGrid_InsertCommand" 
                onneeddatasource="SessionsRadGrid_NeedDataSource" 
                onupdatecommand="SessionsRadGrid_UpdateCommand">
            <ClientSettings>
                <Selecting AllowRowSelect="False" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            </ClientSettings>
            <MasterTableView ClientDataKeyNames="Id" DataKeyNames="Id" >
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                        FooterText="EditCommand footer" HeaderStyle-Width="50px" HeaderText="Edit" 
                        ItemStyle-Width="50px" UniqueName="EditCommandColumn" UpdateText="Update">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" Width="50px" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="SessionName" DataType="System.String" 
                        FilterControlAltText="Filter column1 column" HeaderText="Session" 
                        ItemStyle-Width="300" UniqueName="SessionName" HeaderStyle-Width="300">
                        <ItemStyle Width="300px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDateTime" DataFormatString="{0:d}" 
                        DataType="System.DateTime" FilterControlAltText="Filter column1 column" 
                        HeaderText="Date" UniqueName="StartDate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDateTime" DataFormatString="{0:t}" 
                        DataType="System.DateTime" FilterControlAltText="Filter column1 column" 
                        HeaderText="Start Time" UniqueName="StartDateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EndDateTime" DataFormatString="{0:t}" 
                        DataType="System.DateTime" FilterControlAltText="Filter column2 column" 
                        HeaderText="End Time" UniqueName="EndDateTime">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EndDateTimeAlt" DataFormatString="{0:t}" 
                        DataType="System.DateTime" FilterControlAltText="Filter column2 column" 
                        HeaderText="End Time (alt)" UniqueName="EndDateTimeAlt">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="RequireOneScan" HeaderStyle-Width="50px" 
                        HeaderText="Scans" ItemStyle-Width="50" SortExpression="RequireOneScan" 
                        UniqueName="RequireOneScanTemplateColumn">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" 
                                Text='<%# Convert.ToBoolean(Eval("RequireOneScan")) == true ? "One" : "Two" %>'></asp:Label>
                            <HeaderStyle Width="50px" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="CEUHours" DataType="System.DateTime" 
                        FilterControlAltText="Filter column2 column" HeaderText="CEUs" 
                        UniqueName="CEUHours">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn DataField="SpeakerString" HeaderStyle-Width="150px" 
                        HeaderText="Speakers" ItemStyle-Width="150" SortExpression="SpeakerString" 
                        UniqueName="SpeakerStringTemplateColumn">
                        <ItemTemplate>
                            <asp:LinkButton ID="EditSpeakersLinkButton" runat="server" 
                                CommandName="EditSpeakers" Text='<%# Eval("SpeakerString") %>' />
                        </ItemTemplate>
                        <HeaderStyle Width="150px" />
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Location" DataType="System.DateTime" 
                        FilterControlAltText="Filter column2 column" HeaderText="Location" 
                        UniqueName="Location">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Audience" DataType="System.DateTime" 
                        FilterControlAltText="Filter column2 column" HeaderText="Audience" 
                        UniqueName="Audience">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SessionHost" DataType="System.DateTime" 
                        FilterControlAltText="Filter column2 column" HeaderText="Host" 
                        UniqueName="SessionHost">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Scanner" DataType="System.DateTime" 
                        FilterControlAltText="Filter column2 column" HeaderText="Scanner" 
                        UniqueName="Scanner">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" 
                        ConfirmDialogType="RadWindow" 
                        ConfirmText="Are you sure you want to delete this session?" 
                        ConfirmTitle="Delete" HeaderStyle-Width="50" ItemStyle-Width="50" Text="Delete" 
                        UniqueName="DeleteColumn">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                        <table border="0" style="border:none; margin:5px;">
                            <tr>
                                <td>
                                    <asp:Label ID="Label" runat="server" Text="Name:"></asp:Label>
                                </td>
                                <td colspan="4">
                                    <asp:TextBox ID="SessionNameTextBox" runat="server" 
                                        Text='<%# Bind("SessionName") %>' Width="500" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Start:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadDateTimePicker ID="StartDateRadDateTimePicker" runat="server" 
                                        SelectedDate='<%# Bind("StartDateTime") %>'>
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td align="right">
                                    <asp:Label ID="Label5" runat="server" Text="Location:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="LocationTextBox" runat="server" Text='<%# Bind("Location") %>' 
                                        Width="150" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text="End:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadDateTimePicker ID="EndDateRadDateTimePicker" runat="server" 
                                        SelectedDate='<%# Bind("EndDateTime") %>'>
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td align="right">
                                    <asp:Label ID="Label6" runat="server" Text="Audience:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="AudienceTextBox" runat="server" Text='<%# Bind("Audience") %>' 
                                        Width="150" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width:80px;">
                                    <asp:Label ID="Label3" runat="server" Text="Alt End:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadDateTimePicker ID="AltEndDateRadDateTimePicker" runat="server" 
                                        SelectedDate='<%# Bind("EndDateTimeAlt") %>'>
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td align="right">
                                    <asp:Label ID="Label7" runat="server" Text="Host:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="HostTextBox" runat="server" Text='<%# Bind("SessionHost") %>' 
                                        Width="150" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="CEUs:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CeusTextBox" runat="server" Text='<%# Bind("CEUHours") %>' 
                                        Width="50" />
                                </td>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Label ID="Label8" runat="server" Text="Scanner:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="ScannerTextBox" runat="server" Text='<%# Bind("Scanner") %>' 
                                        Width="150" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:CheckBox ID="OneScanCheckBox" runat="server" 
                                        Checked='<%# Bind("RequireOneScan") %>' Text="Only 1 scan required" />
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td align="right">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="5" style="padding-top:3px;">
                                    <asp:Button ID="btnUpdate" runat="server" 
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' 
                                        Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        <telerik:RadInputManager ID="RadInputManager1" runat="server">
                            <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior1" DecimalDigits="2" 
                                EmptyMessage="Ceus.." Type="Number">
                                <TargetControls>
                                    <telerik:TargetInput ControlID="CeusTextBox" />
                                </TargetControls>
                            </telerik:NumericTextBoxSetting>
                        </telerik:RadInputManager>
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False">
                <WebServiceSettings>
                    <ODataSettings InitialContainerName="">
                    </ODataSettings>
                </WebServiceSettings>
                </FilterMenu>

            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                <WebServiceSettings>
                    <ODataSettings InitialContainerName="">
                    </ODataSettings>
                </WebServiceSettings>
                </HeaderContextMenu>
            </telerik:radgrid>
    </telerik:RadPane>
    </telerik:RadSplitter>

    <telerik:RadCodeBlock ID="RadCodeBlock3" runat="server">

        <script type="text/javascript">
        //<![CDATA[

        function pageLoad() {

        }

        function CollapseNewSessionPane() {
            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var pane = splitter.getPaneById("<%= NewSessionRadPane.ClientID %>");
            var isCollapseSuccess = pane.collapse();

            // Clear all new session fields
            document.all("<%=SessionNameTextBox1.ClientID %>").value = "";
            document.all("<%=LocationTextBox1.ClientID %>").value = "";
            document.all("<%=AudienceTextBox1.ClientID %>").value = "";
            document.all("<%=HostTextBox1.ClientID %>").value = "";
            document.all("<%=ScannerTextBox1.ClientID %>").value = "";
            $find("<%= CeusTextBox1.ClientID %>").set_value("");
         
            // Clear speakers list box
            var list = $find("<%= SpeakersRadListBox.ClientID %>");
            var items = list.get_items();
            list.trackChanges();
            items.clear();
            list.commitChanges();
        }

        function SessionGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var sessionGrid = $find("<%= SessionsRadGrid.ClientID %>");
            sessionGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            sessionGrid.repaint();
        }

        //]]>
    </script>
    </telerik:RadCodeBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="EditSpeakersDialog" runat="server" Title="Edit Speakers" Height="400px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true" />
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="500px"
                Width="800px" Left="0" Top="0" ReloadOnShow="true" ShowContentDuringLoad="true" VisibleStatusbar="false"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

   <%-- <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter Name="EventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
--%>
    <asp:ObjectDataSource ID="SessionDataSource" runat="server" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" SelectMethod="GetSession" TypeName="EventManager.Business.EventSessionMethods" UpdateMethod="Update">
        <SelectParameters>
            <asp:ControlParameter ControlID="SessionsRadGrid" Name="id" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
