﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventFlags.aspx.cs" Inherits="EventManager.Web.Association.EventFlagsForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- custom head section -->
    <style type="text/css">
        .MyImageButton
        {
            cursor: hand;
        }
        .EditFormHeader td
        {
            font-size: 14px;
            padding: 4px !important;
            color: #0066cc;
        }
    </style>
    <!-- end of custom head section -->
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadMenu ID="ActionsRadMenu" runat="server" OnClientItemClicking="ActionsRadMenu_ItemClicking" Width="100%"  Skin="Default">
        <Items>
            <telerik:RadMenuItem runat="server" Text="New Flag" PostBack="false" ></telerik:RadMenuItem>
        </Items>
    </telerik:RadMenu>

    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Horizontal">
    <telerik:RadPane ID="NewFlagRadPane" runat="server" Scrolling="Both" Height="80" Collapsed="true">

        <asp:Panel ID="NewFlagPanel" runat="server" style="padding:5px;">
        <table class="formview-table-layout">
            <tr>
                <td>Flag Name:</td>
                <td>&nbsp;</td>
                <td>Code (for badge):</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td><telerik:RadTextBox ID="NewFlagNameTextBox" runat="server" MaxLength="50" Width="300" /></td>
                <td>&nbsp;</td>
                <td><telerik:RadTextBox ID="NewFlagCodeTextBox" runat="server" MaxLength="10" Width="100" /></td>
                <td><asp:CheckBox ID="NewFlagPrintBadgeCheckBox" runat="server" Text="Print on Badge" /></td>
                <td><telerik:RadButton ID="NewFlagAddButton" runat="server" Text="Save" onclick="NewFlagAddButton_Click" />&nbsp;
                    <telerik:RadButton ID="NewRegDoneRadButton" runat="server" Text="Close" AutoPostBack="false" OnClientClicked="HideNewFlagPane" style="margin-left:10px;" />
                </td>
            </tr>
        </table>
        </asp:Panel>

    </telerik:RadPane>
    <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>
    <telerik:RadPane ID="RegGridRadPane" runat="server" OnClientResized="FlagsGridPaneResized" EnableViewState="true" Scrolling="None">

        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" AutoGenerateEditColumn="True"
            Width="100%" Height="100%" CellSpacing="0" DataSourceID="FlagsDataSource" 
            GridLines="None" AllowAutomaticUpdates="True"
            AllowAutomaticDeletes="true" OnItemDeleted="RadGrid1_ItemDeleted" 
            OnItemUpdated="RadGrid1_ItemUpdated" ondatabound="RadGrid1_DataBound" 
            oninit="RadGrid1_Init" onprerender="RadGrid1_PreRender" 
            onitemdatabound="RadGrid1_ItemDataBound">
            <ClientSettings>
                <Scrolling AllowScroll="true" />
            </ClientSettings>
            <MasterTableView DataSourceID="FlagsDataSource" DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="FlagName" FilterControlAltText="Filter FlagName column"
                        HeaderText="Flag" SortExpression="FlagName" UniqueName="FlagName">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FlagCode" FilterControlAltText="Filter FlagCode column"
                        HeaderText="Code" SortExpression="FlagCode" UniqueName="FlagCode">
                        <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Period1Price" DataType="System.Decimal" ItemStyle-Width="50"
                        HeaderStyle-Width="50" FilterControlAltText="Filter Period1Price column" HeaderText="Price (1)"
                        DataFormatString="{0:c}" SortExpression="Period1Price" UniqueName="Period1PriceGridBoundColumn">
                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Period2Price" DataType="System.Decimal" ItemStyle-Width="50"
                        HeaderStyle-Width="50" FilterControlAltText="Filter Period2Price column" HeaderText="Price (2)"
                        DataFormatString="{0:c}" SortExpression="Period2Price" UniqueName="Period2PriceGridBoundColumn">
                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Period3Price" DataType="System.Decimal" ItemStyle-Width="50"
                        HeaderStyle-Width="50" FilterControlAltText="Filter Period3Price column" HeaderText="Price (3)"
                        DataFormatString="{0:c}" SortExpression="Period3Price" UniqueName="Period3PriceGridBoundColumn">
                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="PrintOnBadge" DataType="System.Boolean" FilterControlAltText="Filter PrintOnBadge column"
                        HeaderText="Print on Badge" SortExpression="PrintOnBadge" UniqueName="PrintOnBadge">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridButtonColumn ConfirmTextFields="FlagCode" ConfirmTextFormatString="Delete the flag '{0}'?" ConfirmDialogType="RadWindow"
                        ItemStyle-Width="50" HeaderStyle-Width="50" ConfirmTitle="Delete" ButtonType="ImageButton"
                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                        <table border="0" style="border:none; margin:5px;">
                            <tr>
                                <td>
                                    <asp:Label ID="Label" runat="server" Text="Flag:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="FlagNameTextBox" runat="server" Text='<%# Bind("FlagName") %>' Width="300" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Code:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="FlagCodeRadTextBox" runat="server" Text='<%# Bind("FlagCode") %>' Width="100" MaxLength="10" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Period1PriceLabel" runat="server" Text="Period 1:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="Period1PriceRadNumericTextBox" runat="server" Text='<%# Bind("Period1Price") %>' 
                                        Width="100" MaxLength="10" NumberFormat-DecimalDigits="2" MinValue="0" Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Period2PriceLabel" runat="server" Text="Period 2:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="Period2PriceRadNumericTextBox" runat="server" Text='<%# Bind("Period2Price") %>' 
                                        Width="100" MaxLength="10" NumberFormat-DecimalDigits="2" MinValue="0" Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Period3PriceLabel" runat="server" Text="Period 3:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="Period3PriceRadNumericTextBox" runat="server" Text='<%# Bind("Period3Price") %>' 
                                        Width="100" MaxLength="10" NumberFormat-DecimalDigits="2" MinValue="0" Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:CheckBox ID="PrintOnBadgeCheckBox" runat="server" Checked='<%# Bind("PrintOnBadge") %>' Text="Print on Badge" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="padding-top:3px;">
                                    <telerik:RadButton ID="btnUpdate" runat="server" 
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' 
                                        Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                    &nbsp;
                                    <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        <telerik:RadInputManager ID="RadInputManager1" runat="server">
                            <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior1" DecimalDigits="2" 
                                EmptyMessage="Ceus.." Type="Number">
                                <TargetControls>
                                    <telerik:TargetInput ControlID="CeusTextBox" />
                                </TargetControls>
                            </telerik:NumericTextBoxSetting>
                        </telerik:RadInputManager>
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>

    </telerik:RadPane>
    </telerik:RadSplitter>

    <telerik:RadCodeBlock ID="RadCodeBlock3" runat="server">
    <script type="text/javascript">
    //<![CDATA[
        function pageLoad() {

        }

        function ActionsRadMenu_ItemClicking(sender, args) {
            if (args.get_item().get_text() == "New Flag") {
                var splitter = $find("<%= RadSplitter1.ClientID %>");
                var pane = splitter.getPaneById("<%= NewFlagRadPane.ClientID %>");
                var isExpandSuccess = pane.expand(pane); // expand this pane
                var flagNameTextBox = $find("<%= NewFlagNameTextBox.ClientID %>");
                flagNameTextBox.focus();
            }
            else {
                args.set_cancel(true);
            }
        }

        function HideNewFlagPane() { 
            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var pane = splitter.getPaneById("<%= NewFlagRadPane.ClientID %>");
            var isExpandSuccess = pane.collapse(); // collapse this pane
        }


        
        function FlagsGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var flagGrid = $find("<%= RadGrid1.ClientID %>");
            flagGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            flagGrid.repaint();
        }
           //]]>
    </script>
    </telerik:RadCodeBlock>
    
    <asp:ObjectDataSource ID="FlagsDataSource" runat="server" SelectMethod="GetFlagsByEvent" TypeName="EventManager.Business.EventFlagMethods" DataObjectTypeName="EventManager.Model.EventFlag" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter Name="EventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>

</asp:Content>
