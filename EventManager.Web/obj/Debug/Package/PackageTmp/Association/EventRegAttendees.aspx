﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventRegAttendees.aspx.cs" Inherits="EventManager.Web.Association.EventRegAttendeesForm" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        .successMessage {color: Green; font-weight:bold; margin-top:5px; padding-top:10px;}
        #NewRegTable 
        {
            margin:5px;margin-top:10px; }

    </style>
    <script language="javascript" src="../js/DYMO.Label.Framework.latest.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   
    <telerik:radmenu ID="ActionsRadMenu" runat="server" Width="100%"  Skin="Default"
        OnClientItemClicking="OnClientItemClicking" 
        style="top: 0px; left: 0px; height: 23px"
        onitemclick="ActionsRadMenu_ItemClick">
        <Items>
            <telerik:RadMenuItem runat="server" Text="Event Registrations - Attendees" PostBack="false" Font-Bold="True" SelectedCssClass="">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="New" PostBack="true" >
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Print" PostBack="false">
            <Items>
                <telerik:RadMenuItem Width="200px">
                    <ItemTemplate>
                    <asp:Label ID="SortOrderLabel" runat="server" Text="Sort Order:"></asp:Label>
                    <asp:RadioButtonList ID="SortOrderRadioButtonList" runat="server" 
                            RepeatDirection="Horizontal" >
                        <asp:ListItem Text="Facility" Value="Facility" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Attendee" Value="Attendee"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:LinkButton runat="server" ID="PrintButton" Text="Print" OnClick="PrintButton_Click"></asp:LinkButton>
                    </ItemTemplate>
                </telerik:RadMenuItem>
            </Items>
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Selected Attendees..." Width="300px" PostBack="False" >
                <Items>
                
                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="PrintBadgesButton" Text="Print Badges" OnClick="PrintBadgesButton_Click"></asp:LinkButton>
                    </ItemTemplate>
                </telerik:RadMenuItem>

                <telerik:RadMenuItem Width="300px" Text="Print Single Badge" PostBack="false" />

                 
                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table style="spacing:0px;">
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="Label1" runat="server" Text="Assign Flag:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <asp:DropDownList ID="FlagMenuDropDownList" runat="server" Font-Size="Small" 
                                DataSourceID="EventFlagsDataSource" DataTextField="FlagName" Width="150" DropDownWidth="200" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" 
                                onselectedindexchanged="FlagMenuDropDownList_SelectedIndexChanged">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                </asp:DropDownList>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem>  

                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table style="spacing:0px;">
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="Label3" runat="server" Text="Grant CEUs for:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <asp:DropDownList ID="SessionsDropDownList" runat="server" Font-Size="Small" 
                                DataSourceID="SessionsDataSource" DataTextField="SessionName" Width="150" DropDownWidth="200" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" 
                                onselectedindexchanged="SessionsMenuDropDownList_SelectedIndexChanged">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                </asp:DropDownList>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem> 

                </Items>
            </telerik:RadMenuItem>
        </Items>
    </telerik:radmenu>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" CssClass="errorMessage" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="ValidationGroup1" Display="None" CssClass="errorMessage"></asp:CustomValidator>
    
    <asp:Panel ID="NewRegPanel" runat="server" style="margin-top:10px; clear:both; background-color:#cccccc;">
    <asp:UpdatePanel ID="NewRegUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
    <table id="NewRegTable" class="formview-table-layout">
        <tr>
            <td colspan="3">
            <br />
                <asp:ValidationSummary ID="NewRegValidationSummary" runat="server" ValidationGroup="NewRegValidationGroup" ForeColor="Red" CssClass="errorMessage" Style="margin-top:3px;" />
                <asp:CustomValidator ID="NewRegCustomValidator" runat="server" ErrorMessage="CustomValidator" ValidationGroup="NewRegValidationGroup" Display="None" CssClass="errorMessage"></asp:CustomValidator>
                <asp:CustomValidator ID="NewRegSuccessValidator" runat="server" ErrorMessage="" Display="Dynamic" CssClass="successMessage" ForeColor="Green"></asp:CustomValidator>
            </td>
        </tr>
            <tr>
                <td>Attendee Name:</td>
                <td>Facility:</td>
                <td>Title:</td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox
                        ID="NewAttendeeRadComboBox" runat="server"
                        AllowCustomText="false" MarkFirstMatch="false"  
                        DataTextField="FullNameWithId" AutoPostBack="true" 
                        DataValueField="Id" EmptyMessage="Attendee name (last, first)..." 
                        Width="200px" MaxHeight="200px" 
                        onselectedindexchanged="NewAttendeeRadComboBox_SelectedIndexChanged"
                        onitemsrequested="NewAttendeeRadComboBox_ItemsRequested" 
                        EnableLoadOnDemand="True" >
                    </telerik:RadComboBox>
                    <%--<asp:RequiredFieldValidator ID="AttendeeNameValidator" runat="server" ControlToValidate="NewAttendeeRadComboBox"
                        ErrorMessage="Attendee name is required."></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="NewRegFacilityRadComboBox" runat="server"
                        Width="300px" 
                        EmptyMessage="Facility..."
                        MarkFirstMatch="false"
                        DataTextField="OrgName" 
                        DataValueField="Id"
                        AllowCustomText="false"
                        AutoPostBack="false"
                        onitemsrequested="NewRegFacilityRadComboBox_ItemsRequested" 
                        EnableLoadOnDemand="True" SkinID="Vista" >
                    </telerik:RadComboBox></td>
                <td>
                    <telerik:radtextbox ID="NewRegTitleTextBox" runat="server" Text="" Width="150" />
                </td>
                <td>
                    <asp:Button ID="NewRegAddButton" runat="server" Text="Save" onclick="NewRegAddButton_Click" />&nbsp;
                    <a href="javascript:HideElement('<%=NewRegPanel.ClientID %>')">
                        <asp:Label ID="NewFlagDoneLabel" runat="server" Text="Done"></asp:Label>
                    </a>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;">
                    <asp:LinkButton ID="NewAttendeeLinkButton" runat="server" Text="Create new attendee" />
                </td>
                <td style="vertical-align:top;">
                    <asp:LinkButton ID="NewOrgLinkButton" runat="server" Text="Create new facility" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RegistrationsRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>


    <asp:UpdatePanel ID="RegGridUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="NewRegAddButton" EventName="Click" />
    </Triggers>
    <ContentTemplate>
    <telerik:radgrid id="RegistrationsRadGrid" runat="server" 
        autogeneratecolumns="False"
        allowfilteringbycolumn="True" allowmultirowselection="True" 
        cellspacing="0" datasourceid="AttendeesDataSource"
        gridlines="None" allowautomaticupdates="True" allowautomaticdeletes="True" allowpaging="True"
        pagerstyle-position="TopAndBottom" oninit="RegistrationsRadGrid_Init" onitemdeleted="RegistrationsRadGrid_ItemDeleted"
        onitemdatabound="RegistrationsRadGrid_ItemDataBound" onitemupdated="RegistrationsRadGrid_ItemUpdated" 
        PageSize="20" onitemcreated="RegistrationsRadGrid_ItemCreated" 
            onitemcommand="RegistrationsRadGrid_ItemCommand" >

        <groupingsettings casesensitive="False"></groupingsettings>
        <ClientSettings>
            <scrolling AllowScroll="False" UseStaticHeaders="True" />
            <selecting AllowRowSelect="True" />
           
            <%--<scrolling frozencolumnscount="4" usestaticheaders="True" />--%>
        </ClientSettings>
        <%--<ExportSettings IgnorePaging="true" OpenInNewWindow="true">
            <Pdf PageHeight="210mm" PageWidth="297mm" PageTitle="SushiBar menu" DefaultFontFamily="Arial Unicode MS"
                PageBottomMargin="20mm" PageTopMargin="20mm" PageLeftMargin="20mm" PageRightMargin="20mm" />
            <pdf defaultfontfamily="Arial Unicode MS" pagebottommargin="20mm" 
                pageheight="210mm" pageleftmargin="20mm" pagerightmargin="20mm" 
                pagetitle="SushiBar menu" pagetopmargin="20mm" pagewidth="297mm" />
            <pdf defaultfontfamily="Arial Unicode MS" pagebottommargin="20mm" 
                pageheight="210mm" pageleftmargin="20mm" pagerightmargin="20mm" 
                pagetitle="SushiBar menu" pagetopmargin="20mm" pagewidth="297mm" />
            <pdf defaultfontfamily="Arial Unicode MS" pagebottommargin="20mm" 
                pageheight="210mm" pageleftmargin="20mm" pagerightmargin="20mm" 
                pagetitle="SushiBar menu" pagetopmargin="20mm" pagewidth="297mm" />
            <pdf defaultfontfamily="Arial Unicode MS" pagebottommargin="20mm" 
                pageheight="210mm" pageleftmargin="20mm" pagerightmargin="20mm" 
                pagetitle="SushiBar menu" pagetopmargin="20mm" pagewidth="297mm" />
        </ExportSettings>--%>
        <mastertableview datasourceid="AttendeesDataSource" DataKeyNames="Id" ClientDataKeyNames="Id" PagerStyle-Position="TopAndBottom"  CommandItemDisplay="Top">
        
        <%--<commanditemsettings exporttopdftext="Export to PDF" showexporttoexcelbutton="True" showexporttopdfbutton="True" />

            <commanditemsettings exporttopdftext="Export to PDF" 
                showaddnewrecordbutton="False" showexporttoexcelbutton="True" 
                showexporttopdfbutton="True" />

            <commanditemsettings exporttopdftext="Export to PDF" 
                showaddnewrecordbutton="False" showexporttoexcelbutton="True" 
                showexporttopdfbutton="True" />

            <commanditemsettings exporttopdftext="Export to PDF" 
                showaddnewrecordbutton="False" showexporttoexcelbutton="True" 
                showexporttopdfbutton="True" />--%>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridClientSelectColumn UniqueName="CheckboxSelectColumn" 
                    FooterText="CheckBoxSelect footer" ItemStyle-Width="30"  
                    HeaderStyle-Width="40" >

<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle Width="40px" HorizontalAlign="Center" BackColor="#CCCCCC"></ItemStyle>
                </telerik:GridClientSelectColumn>

                 <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn"
                        ItemStyle-Width="30px" HeaderText="" HeaderStyle-Width="30px" UpdateText="Update" ButtonType="ImageButton">
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle Width="30px" HorizontalAlign="Center" CssClass="MyImageButton" 
                            BackColor="#CCCCCC"></ItemStyle>
                    </telerik:GridEditCommandColumn>

                <telerik:GridBoundColumn DataField="AttendeeId" DataType="System.Int32" UniqueName="AttendeeId" ItemStyle-Width="80" AllowSorting="false" 
                    HeaderText="Id" SortExpression="AttendeeId" HeaderStyle-Width="80" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                    AllowFiltering="true" FilterControlWidth="50" FilterControlAltText="Filter Attendee ID" CurrentFilterFunction="EqualTo">
                    <HeaderStyle Width="80px"></HeaderStyle>

                    <ItemStyle Width="70px" BackColor="#CCCCCC"></ItemStyle>
                </telerik:GridBoundColumn>

             <%--   <telerik:GridHyperLinkColumn FilterControlAltText="Filter attendee name" HeaderStyle-Width="130px" HeaderText="Attendee"
                            AllowFiltering="true" FilterControlWidth="100px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                            UniqueName="AttendeeNameHyperlink"
                            DataType="System.String"
                            DataTextField="Attendee.FullName"
                            DataNavigateUrlFields="AttendeeId"
                            DataNavigateUrlFormatString="AttendeeDetails.aspx?AttendeeId={0}">
                        <HeaderStyle Width="130px"></HeaderStyle>
                        </telerik:GridHyperLinkColumn>--%>

                    <telerik:GridTemplateColumn UniqueName="AttendeeNameTemplateColumn" DataField="Attendee.FullName" HeaderText="Attendee" HeaderStyle-HorizontalAlign="Left" 
                            AllowFiltering="true" FilterControlWidth="80px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                            HeaderStyle-Width="100px" ItemStyle-Width="130">
                            <ItemTemplate>
                                <asp:LinkButton ID="AttendeeNameLinkButton" runat="server" Text='<%# Eval("Attendee.FullName") %>' CommandName="ViewAttendeeDetails" />
                            </ItemTemplate>
                            <HeaderStyle Width="130px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" BackColor="#CCCCCC" />
                        </telerik:GridTemplateColumn>

                <telerik:GridHyperLinkColumn FilterControlAltText="Filter facility name" HeaderStyle-Width="250px" HeaderText="Facility" 
                    FilterControlWidth="200px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true" 
                    UniqueName="OrgNameHyperlink" DataType="System.String" DataTextField="Registration.Organization.OrgName" 
                    DataNavigateUrlFields="Registration.Organization.Id" DataNavigateUrlFormatString="OrgDetails.aspx?OrgId={0}">
                    <HeaderStyle Width="250px"></HeaderStyle>
                </telerik:GridHyperLinkColumn>
                
                <telerik:GridBoundColumn DataField="Title" FilterControlAltText="Filter Title column" HeaderText="Title" ItemStyle-Width="120" HeaderStyle-Width="120px" 
                    AllowSorting="false" UniqueName="Title" FilterControlWidth="75">
                    <HeaderStyle Width="120px"></HeaderStyle>
                    <ItemStyle Width="120px"></ItemStyle>
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="IsSpeakerTemplateColumn" DataField="IsSpeaker" HeaderText="Speaker" 
                    HeaderStyle-Width="60px" ItemStyle-Width="60">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSpeaker")) == true ? "Speaker" : "" %>'></asp:Label>
                    <HeaderStyle Width="60px"></HeaderStyle>
                    </ItemTemplate>
                    <HeaderStyle Width="60px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="Notes" ItemStyle-Width="150" HeaderStyle-Width="150" FilterControlWidth="100"
                    FilterControlAltText="Filter Notes column" HeaderText="Notes" CurrentFilterFunction="Contains" 
                    SortExpression="Notes" UniqueName="Notes">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="Price" DataType="System.Decimal" 
                    AllowFiltering="false" HeaderText="Reg Price" ItemStyle-Width="80" HeaderStyle-Width="80" 
                    SortExpression="Price" UniqueName="Price" DataFormatString="{0:C}">
                    <HeaderStyle Width="80px" />
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="FlagListTemplateColumn" DataField="FlagList" HeaderText="Flags" 
                    SortExpression="FlagList"  HeaderStyle-Width="150px" ItemStyle-Width="150">
                  <ItemTemplate>
                        <%# Eval("FlagList")%>
                    </ItemTemplate>
                    <HeaderStyle Width="150px"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="FlagsRadComboBox" DataSourceID="EventFlagsDataSource" DataTextField="FlagName"
                            DataValueField="FlagCode" Width="120" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("FlagListTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="FlagsComboBoxIndexChanged" DropDownWidth="200">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                            <script type="text/javascript">
                                function FlagsComboBoxIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("FlagListTemplateColumn", args.get_item().get_value(), "Contains");

                                }
                            </script>

                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="CeusTemplateColumn" 
                    DataField="CeuTotal" HeaderText="CEUs"
                    SortExpression="CeuTotal"  HeaderStyle-Width="90px" ItemStyle-Width="90" 
                    FilterControlWidth="45px">
                    <ItemTemplate>
                        <asp:Label ID="AttendanceLabel" runat="server" Text='<%# Eval("CeuTotal") %>' Style="margin-top:2px;" />&nbsp;&nbsp;
                        <telerik:RadButton ID="EditAttendanceRadButton" runat="server" Text="Edit" CommandName="EditAttendance" AutoPostBack="false"></telerik:RadButton>
                    </ItemTemplate>
                    <HeaderStyle Width="90px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="PrintCertificateTemplateColumn" HeaderText="Print Cert" 
                            AllowFiltering="false"
                             HeaderStyle-Width="90px" ItemStyle-Width="90">
                            <ItemTemplate>
                                <asp:LinkButton ID="PrintCertLinkButton" runat="server" Text="Print Cert" CommandName="PrintCertificate" />
                            </ItemTemplate>
                            <HeaderStyle Width="90px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridTemplateColumn>

                 <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ItemStyle-Width="30" HeaderStyle-Width="30"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Remove"
                        UniqueName="DeleteRegistrationColumn" 
                    ConfirmTextFields="Attendee.FullName" 
                    ConfirmTextFormatString="Are you sure you want to delete the registration for {0}?">
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
               
            </Columns>

            <EditFormSettings EditFormType="Template" >
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                <FormTemplate>
                    <table id="AttendeeEditTable" style="margin:5px; float:left;">
                        <tr>
                            <td colspan="2"><h3 style="margin-top:0px;"><asp:Label ID="AttendeeNameLabel" runat="server" Text='<%# Eval("Attendee.FullName") %>'  /></h3></td>
                        </tr>
                        <tr>
                            <td>Facility Registration:</td>
                            <td>
                                <asp:DropDownList ID="RegistrationsDropDownList" runat="server" DataTextField="RegistrationDesc" DataValueField="Id" 
                                    DataSourceID="RegistrationsDataSource"  SelectedValue='<%# Bind("RegistrationId") %>'>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Title:</td><td><asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>'  /></td></tr>
                        <tr>
                            <td>Notes:</td><td><asp:TextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>'  /></td></tr>
                        <tr>
                            <td colspan="2" style="padding-top:3px;">
                                <asp:Button ID="btnUpdate" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    
                    <asp:CheckBoxList ID="AttendeeFlagsCheckBoxList" runat="server" 
                        DataSourceID="EventFlagsDataSource" DataTextField="FlagName" 
                        DataValueField="Id" Style="float:left; margin-left:20px; margin-top:30px;">
                        </asp:CheckBoxList>
                     <br />
                    
                </FormTemplate>
            </EditFormSettings>

            <PagerStyle AlwaysVisible="True" />

        </mastertableview>
        <HeaderStyle HorizontalAlign="Left" />
        <pagerstyle position="TopAndBottom" ></pagerstyle>
        <filtermenu enableimagesprites="False" ></filtermenu>
    </telerik:radgrid>
    </ContentTemplate>
    </asp:UpdatePanel>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[
        function pageLoad() {

        }

        var regAttendeeId;
        var rowIndex;
        var attendeeComboClientId;

        function ShowPrintBadgeForm() {

            // Get the selected row's RegAttendeeId
            var firstDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_selectedItems()[0];
            regAttendeeId = firstDataItem.getDataKeyValue("Id");
            
            window.radopen("Reports2/PrintSingleBadge.aspx?RegAttendeeId=" + regAttendeeId, "BadgePrintDialog");
            return false;
        }

        function ShowCertificateReport(eventId, attendeeId, reportName) {

            // Get the selected row's RegAttendeeId
            //var firstDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[0];
          
            window.radopen("Reports2/ReportViewer.aspx?FileName=" + reportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId, "CertificateReportDialog");
            return false;
        }

        function ShowAttendanceDialog(eventId, registrationAttendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("EditAttendanceDialog.aspx?EventId=" + eventId + "&RegAttendeeId=" + registrationAttendeeId, "EditAttendanceDialog");
            return false;
        }

        function ShowAttendeeDialog(attendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function ShowOrgDialog(orgId) {

            //attendeeComboClientId =
            var wnd = window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "OrgDetailsDialog");
            wnd.setSize(850, 500);
            return false;
        }

        function RefreshRegistrationsGrid(ceuTotal) {

            // Update the session row with the new speaker string
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            editedDataItem.get_cell("CeusTemplateColumn").children[0].innerText = ceuTotal;
        }

        function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

            // Get the edited row
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];

            // Update the cells
            editedDataItem.get_cell("AttendeeNameTemplateColumn").children[0].innerText = fullName;
            editedDataItem.get_cell("Title").children[0].innerText = title;
        }

        function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

            if (attendeeId > 0) {

                // Insert new attendee into newAttendeeRadComboBox
                var combo = $find("<%= NewAttendeeRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(fullNameWithId);
                comboItem.set_value(attendeeId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();

                // Insert the facility into NewRegFacilityRadComboBox
                if (orgId != "0") { 
                    var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(orgName);
                    comboItem.set_value(orgId);
                    combo.trackChanges();
                    combo.get_items().add(comboItem);
                    comboItem.select();
                    combo.commitChanges();
                    comboItem.scrollIntoView();
                }

                if (title) { 
                    // Insert the title into
                    var textbox = $find("<%= NewRegTitleTextBox.ClientID %>");
                    textbox.set_text = title;
                }
            }
        }

        function RefreshOrgCombo(orgId, orgName) {

            if (orgId > 0) {

                // Insert new org into newFacilityRadComboBox
                var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(orgName);
                comboItem.set_value(orgId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();
            }
        }

        function ShowElement(elementID) {
            document.all(elementID).style.display = 'block';
            // Hide the 'new' button
            // Set focus to Flag Name textbox
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").focus();

        }
        function HideElement(elementID) {
            document.all(elementID).style.display = 'none';
            // Show the 'new' button
            // Clear all new flag fields
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegFacilityRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegTitleTextBox.ClientID %>").value = "";

        }
           //]]>
        function OnClientItemClicking(sender , args) 
        {
            if (args.get_item().get_text() == "Print Single Badge") {
                ShowPrintBadgeForm();
            }
            else if (args.get_item().get_text() == "Print Certificate") {
                ShowCertificateReport();
            }
        }

        function GetSelectedItems() {
            alert($find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_selectedItems().length);
        }

        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                args.set_enableAjax(false);
            }
        }


    </script>
    </telerik:RadCodeBlock>
       
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" 
        EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="BadgePrintDialog" runat="server" Title="Print Single Badge" Height="280px"
                Width="400px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="EditAttendanceDialog" runat="server" Title="Edit Attendance" Height="500px"
                Width="900px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="OrgDetailsDialog" runat="server" 
                Title="Edit Facility" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="CertificateReportDialog" runat="server" 
                Title="Certificate" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetRegisteredAttendeesForEvent" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods" 
            DataObjectTypeName="EventManager.Model.RegistrationAttendee" 
            DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update" 
        onselected="AttendeesDataSource_Selected" 
        onselecting="AttendeesDataSource_Selecting">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeeFlagsDataSource" runat="server" 
        SelectMethod="GetRegisteredAttendeeFlags" 
        TypeName="EventManager.Business.RegistrationAttendeeFlags">
        <SelectParameters>
            <asp:ControlParameter ControlID="RegistrationsRadGrid" 
                Name="registrationAttendeeId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>


    <asp:ObjectDataSource ID="EventFlagsDataSource" runat="server" 
            SelectMethod="GetFlagsByEvent" 
            TypeName="EventManager.Business.EventFlagMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
        </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="RegistrationsDataSource" runat="server" 
        SelectMethod="GetRegistrationsForEvent" 
        TypeName="EventManager.Business.RegistrationMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
