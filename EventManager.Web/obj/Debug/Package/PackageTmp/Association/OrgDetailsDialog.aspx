﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgDetailsDialog.aspx.cs" Inherits="EventManager.Web.Association.OrgDetailsDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Facility</title>

    <script type="text/javascript">
    //<![CDATA[

        function OrgAdded(organizationId, orgName) {
            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.OrgAdded(organizationId, orgName);
            oWindow.close();
        }

        function OrgUpdated(organizationId, orgName) {
            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.OrgUpdated(organizationId, orgName);
            oWindow.close();
        }

        function OrgDeleted(organizationId) {
            var oWindow = GetRadWindow();
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CloseWindow() {
            GetRadWindow().close();
        }
    //]]>
    </script>
   
    <style type="text/css">
       
        html, body, form
        {
            margin: 0px;
            padding: 0;
            height: 100%;
        }
      
        input[readonly] {border:none; background-color:Gray;}
        .label {white-space:nowrap;}
        td.buttons {padding:10px;}
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div style="width:95%; height:100%; border:0 none; margin: 0 auto; vertical-align:top;">

        <telerik:radscriptmanager runat="server" />
        <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server"  />

        <asp:ValidationSummary ID="OrgValidationSummary" runat="server" ValidationGroup="EditOrg" ForeColor="Red" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="None" ValidationGroup="EditOrg"></asp:CustomValidator>

        <asp:FormView ID="FormView1" runat="server" DataSourceID="OrgDataSource" Width="100%" Height="100%"
                DataKeyNames="Id" DefaultMode="Edit" onitemcommand="FormView1_ItemCommand" BorderStyle="None"
                oniteminserted="FormView1_ItemInserted" 
                onitemupdated="FormView1_ItemUpdated" 
                onitemdeleted="FormView1_ItemDeleted" 
                oniteminserting="FormView1_ItemInserting" 
            onitemupdating="FormView1_ItemUpdating" >
        <EditItemTemplate>
            <h1 style="margin-top:0px; padding-top:0px;"><asp:Label ID="Label1" runat="server" Text='<%# Eval("OrgName") %>' ViewStateMode="Disabled"></asp:Label></h1>
            <table style="float:left;" class="formview-table-layout">
                <tr>
                    <td colspan="2" class="buttons">
                    <telerik:RadButton ID="RadButton11" runat="server" Text="Update" CausesValidation="True" CommandName="Update" ValidationGroup="Insert" />&nbsp;
                    <telerik:RadButton ID="RadButton12" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                    </td>
                </tr>
				    <tr>
					    <td>Name:</td>
					    <td><telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Text='<%# Bind("OrgName") %>' Width="250" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorOrgName" runat="server" ControlToValidate="OrgNameRadTextBox"                         
                                ErrorMessage="Facility name is Required" ForeColor="Red" ValidationGroup="EditOrg">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
					    <td>Type:</td>
					    <td><telerik:RadComboBox
                                ID="OrgTypeRadComboBox" runat="server" DataSourceID="OrgTypesDataSource"  Width="250"
                                DataTextField="OrgTypeName" DataValueField="Id" SelectedValue='<%# Bind("OrgTypeId") %>'
                                DropDownWidth="300" AutoPostBack="true" AppendDataBoundItems="true"
                                onselectedindexchanged="OrgTypeRadComboBox_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                           </telerik:RadComboBox>
					    </td>
				    </tr>
                    <tr>
						    <td class="label">
							    Fee Category:</td>
						    <td>
                            <telerik:RadComboBox
                                    ID="FeeCategoryDropDownList" runat="server" 
                                    DataSourceID="FeeCategoriesDataSource" Width="250"
                                    DataTextField="CategoryDesc" DataValueField="Id" AutoPostBack="true"
                                    SelectedValue='<%# Bind("OrgFeeCategoryId") %>' 
                                    DropDownWidth="300"  AppendDataBoundItems="true"
                                    onitemsrequested="FeeCategoryDropDownList_ItemsRequested" 
                                    onselectedindexchanged="FeeCategoryDropDownList_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                                </telerik:RadComboBox>
						    </td>
					    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><asp:Label ID="IsMemberLabel" runat="server" Visible='<%# (bool)Eval("IsMember") == true ? true : false %>' ForeColor="Green" Text="Member Facility" />
                            <asp:Label ID="IsNonMemberLabel" runat="server" Visible='<%# (bool)Eval("IsMember") == false ? true : false %>' ForeColor="Red" Text="Non Member Facility" /></td>
                    </tr>
                    <tr>
					    <td class="label">Corporate Affiliation:</td>
					    <td>
						    <telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' Width="250" />
					    </td>
				    </tr>
                    <tr>
					    <td>Beds:</td>
					    <td><telerik:RadTextBox ID="BedCountRadTextBox" runat="server" Text='<%# Bind("BedCount") %>' Width="50"  />
                            <asp:CompareValidator ID="BedCountCompareFieldValidator" runat="server" ControlToValidate="BedCountRadTextBox" 
                            Operator="DataTypeCheck" Type="Integer" ErrorMessage="Input valid bed count" ValidationGroup="EditOrg">  
                        </asp:CompareValidator>
                        </td>
				    </tr>
                    <tr>
                        <td colspan="2" class="buttons">
                            <telerik:RadButton ID="RadButton13" runat="server" Text="Update" CausesValidation="True" CommandName="Update" ValidationGroup="EditOrg" />&nbsp;
                            <telerik:RadButton ID="RadButton14" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                        </td>
                    </tr>
                </table>
            <table style="float:left;margin-left:10px; margin-top:0px;">
                <tr>
                    <td colspan="2"><h3>Facility Contact Info</h3></td>
                </tr>
				<tr>
					<td >
						Street:</td>
					<td>
						<telerik:RadTextBox ID="AddressRadTextBox" runat="server" Text='<%# Bind("Address") %>' TextMode="MultiLine" Width="130" />
					</td>
				</tr>
				<tr>
					<td>
						City:</td>
					<td>
						<telerik:RadTextBox ID="CityRadTextBox" runat="server" Text='<%# Bind("City") %>' Width="130" />
					</td>
				</tr>
				<tr>
					<td >
						State:</td>
					<td><telerik:RadComboBox
                                ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" 
                                DataTextField="StateName" DataValueField="StateCode" Width="130" SelectedValue='<%# Bind("StateCode") %>'
                                DropDownWidth="200">
                            </telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td>
						Zip:
					</td>
					<td>
						<telerik:RadTextBox ID="ZipRadTextBox" runat="server" Text='<%# Bind("Zip") %>' Width="130" />
					</td>
				</tr>
				<tr>
					<td >
						Phone:</td>
					<td>
						<telerik:RadTextBox ID="PhoneRadTextBox" runat="server" Text='<%# Bind("Phone") %>' Width="130" />
					</td>
				</tr>
				<tr>
					<td >
						Fax:</td>
					<td>
						<telerik:RadTextBox ID="FaxRadTextBox" runat="server" Text='<%# Bind("Fax") %>' Width="130" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:Label ID="AdminEmailLabel" runat="server" Text='<%# Eval("Attendee.Email") %>' Width="130" />
					</td>
				</tr>
				</table>
			<div style="clear:both"></div>
            <br />
			
        </EditItemTemplate>

        <InsertItemTemplate>
            <table class="formview-table-layout" style="float:left; width:250px; margin-right:20px;" >
                <tr>
                    <td colspan="2"><h1 style="margin:3px;">New Facility</h1></td>
                </tr>
                <tr>
                    <td colspan="2">
                       <telerik:RadButton ID="InsertRadButton" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
                        <telerik:RadButton ID="InsertCancelRadButton" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" />
                    </td>
                </tr>
				    <tr>
					    <td>Name:</td>
					    <td><telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Text='<%# Bind("OrgName") %>' Width="200" MaxLength="100" /></td>
                    </tr>
                    <tr>
					    <td>Type:</td>
					    <td><telerik:RadComboBox
                                    ID="OrgTypeRadComboBox" runat="server" DataSourceID="OrgTypesDataSource" 
                                    DataTextField="OrgTypeName" DataValueField="Id" Width="200" SelectedValue='<%# Bind("OrgTypeId") %>'
                                    DropDownWidth="300" AppendDataBoundItems="true">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                    </Items>
                                </telerik:RadComboBox>
					    </td>
				    </tr>
                    <tr>
						    <td class="label">
							    Fee Category:</td>
						    <td>
                            <telerik:RadComboBox
                                    ID="FeeCategoryDropDownList" runat="server" DataSourceID="FeeCategoriesDataSource" 
                                    DataTextField="CategoryDesc" DataValueField="Id"  Width="200" AppendDataBoundItems="true"
                                    SelectedValue='<%# Bind("OrgFeeCategoryId") %>' DropDownWidth="300">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                    </Items>
                                </telerik:RadComboBox>
						    </td>
					    </tr>
                    <tr>
					    <td class="label">Corporate Affiliation:</td>
					    <td>
						    <telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' Width="200" MaxLength="100" />
					    </td>
				    </tr>
                    <tr>
					    <td>Beds:</td>
					    <td><telerik:RadTextBox ID="BedCountRadTextBox" runat="server" Text='<%# Bind("BedCount") %>' Width="50" MaxLength="3"  /></td>
				    </tr>
                </table>
                <table  class="formview-table-layout" style="float:left;margin-left:0px; margin-top:15px;">
                    <tr>
                        <td colspan="2"><h3>Contact Information</h3></td>
                    </tr>
				    <tr>
					    <td style="vertical-align:top; padding-top:12px;">
						    Street:</td>
					    <td>
						    <telerik:RadTextBox ID="AddressRadTextBox" runat="server" Text='<%# Bind("Address") %>' TextMode="MultiLine" Rows="2" Width="125" MaxLength="255" />
					    </td>
				    </tr>
				    <tr>
					    <td >
						    City:</td>
					    <td>
						    <telerik:RadTextBox ID="CityRadTextBox" runat="server" Text='<%# Bind("City") %>' Width="120" MaxLength="50" />
					    </td>
				    </tr>
				    <tr>
					    <td >
						    State:</td>
					    <td><telerik:RadComboBox
                                    ID="StateRadComboBox" runat="server" DataSourceID="StatesDataSource" Width="120"
                                    DataTextField="StateName" DataValueField="StateCode" SelectedValue='<%# Bind("StateCode") %>'
                                    DropDownWidth="120px" ondatabound="StateRadComboBox_DataBound" >
                                </telerik:RadComboBox>
					    </td>
				    </tr>
				    <tr>
					    <td>
						    Zip:
					    </td>
					    <td>
						    <telerik:RadTextBox ID="ZipRadTextBox" runat="server" Text='<%# Bind("Zip") %>' Width="120" MaxLength="10" />
					    </td>
				    </tr>
				    <tr>
					    <td>
						    &nbsp;</td>
					    <td>
						    &nbsp;</td>
				    </tr>
				    <tr>
					    <td>
						    Phone:</td>
					    <td>
						    <telerik:RadTextBox ID="PhoneRadTextBox" runat="server" Text='<%# Bind("Phone") %>' Width="120" MaxLength="20" />
					    </td>
				    </tr>
				    <tr>
					    <td>
						    Fax:</td>
					    <td>
						    <telerik:RadTextBox ID="FaxRadTextBox" runat="server" Text='<%# Bind("Fax") %>' Width="120" MaxLength="20" />
					    </td>
				    </tr>
					
				    </table>
                <div style="clear:both;"></div>
                <telerik:RadButton ID="RadButton7" runat="server" Text="Insert" CausesValidation="True" CommandName="Insert" />&nbsp;
                <telerik:RadButton ID="RadButton8" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel" /><br /><br />

        </InsertItemTemplate>

        <ItemTemplate>
            <table cellspacing="0" cellpadding="0" class="formview-table-layout">
                <tr>
                    <td colspan="3"><h1><asp:Label ID="Label1" runat="server" Text='<%# Eval("OrgName") %>' ViewStateMode="Disabled"></asp:Label></h1></td>
                </tr>
                <tr>
                    <td colspan="3">
                    <telerik:RadButton ID="RadButton4" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" />&nbsp;
                    <telerik:RadButton ID="RadButton5" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" />&nbsp;
                    <telerik:RadButton ID="RadButton6" runat="server" Text="New" CausesValidation="False" CommandName="New" />
                    </td>
                </tr>
                <tr>
					    <td>Id:</td>
					    <td colspan="2"><telerik:RadTextBox ID="RadTextBox1" runat="server" Text='<%# Bind("Id") %>' ReadOnly="true" Width="40" /></td>
                    </tr>
				    <tr>
					    <td>Name:</td>
					    <td colspan="2"><telerik:RadTextBox ID="OrgNameRadTextBox" runat="server" Text='<%# Bind("OrgName") %>' ReadOnly="true" Width="100%" /></td>
                    </tr>
                    <tr>
					    <td>Type:</td>
					    <td colspan="2"><telerik:RadTextBox ID="OrgTypeNameRadTextBox" runat="server" Text='<%# Bind("OrganizationType.OrgTypeName") %>' ReadOnly="true" Width="100%" />
					    </td>
				    </tr>
                    <tr>
					    <td>Corporate Affiliation:</td>
					    <td colspan="2">
						    <telerik:RadTextBox ID="CorporateAffiliationRadTextBox" runat="server" Text='<%# Bind("CorporateAffiliation") %>' ReadOnly="true" Width="100%" />
					    </td>
				    </tr>
                    <tr>
					    <td>Beds:</td>
					    <td><telerik:RadTextBox ID="BedCountDynamicControl" runat="server" Text='<%# Bind("BedCount") %>' ReadOnly="true" Width="50"  /></td>
				        <td align="right" class="style1"><asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMember") %>' Text="Member" /></td>
                    </tr>
                </table>
                <table style="float:left;margin-left:10px; width:300px;">
                    <tr>
                        <td colspan="2"><h3>Facility Contact Info</h3></td>
                    </tr>
				    <tr>
					    <td >
						    Address:</td>
					    <td>
						    <telerik:RadTextBox ID="AddressDynamicControl" runat="server" Text='<%# Bind("FullAddress") %>' TextMode="MultiLine" ReadOnly="true" />
					    </td>
				    </tr>
				
				    <tr>
					    <td >
						    Phone:</td>
					    <td>
						    <telerik:RadTextBox ID="PhoneDynamicControl" runat="server" Text='<%# Bind("Phone") %>' ReadOnly="true" />
					    </td>
				    </tr>
				    <tr>
					    <td >
						    Fax:</td>
					    <td>
						    <telerik:RadTextBox ID="FaxDynamicControl" runat="server" Text='<%# Bind("Fax") %>' ReadOnly="true" />
					    </td>
				    </tr>
			        <tr>
                        <td colspan="2"><h3>Administrative Contact</h3></td>
                    </tr>
				    <tr>
					    <td>
						    Name:</td>
					    <td>
						    <telerik:RadTextBox ID="AdminNameRadTextBox" runat="server" Text='<%# Bind("Attendee.FullName") %>' />
					    </td>
					
				    </tr>
				    <tr>
					    <td>
						    Email:</td>
					    <td>
						    <telerik:RadTextBox ID="AdminEmailRadTextBox" runat="server" Text='<%# Bind("Attendee.Email") %>' />
					    </td>
				    </tr>
					    <tr>
						    <td>
							    Fee Category:</td>
						    <td><telerik:RadTextBox ID="RadTextBox3" runat="server" Text='<%# Bind("OrganizationFeeCategory.CategoryDesc") %>' ReadOnly="true" />
						    </td>
					    </tr>
				    </table>
            <br />
            <telerik:RadButton ID="RadButton1" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" />&nbsp;
            <telerik:RadButton ID="RadButton2" runat="server" Text="Delete" CausesValidation="False" CommandName="Delete" />&nbsp;
            <telerik:RadButton ID="RadButton3" runat="server" Text="New" CausesValidation="False" CommandName="New" />
   
        </ItemTemplate>
        </asp:FormView>

     <asp:ObjectDataSource ID="OrgTypesDataSource" runat="server" 
        onselecting="OrgTypesDataSource_Selecting" SelectMethod="GetOrganizationTypes" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="FeeCategoriesDataSource" runat="server" 
        onselecting="FeeCategoriesDataSource_Selecting" SelectMethod="GetFeeCategories" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

     <asp:ObjectDataSource ID="OrgDataSource" runat="server" 
            SelectMethod="GetOrganization" TypeName="EventManager.Business.OrganizationMethods" 
            DataObjectTypeName="EventManager.Model.Organization" 
            InsertMethod="AddOrganization" DeleteMethod="Delete" 
            UpdateMethod="Update" 
            oninserted="OrgDataSource_Inserted" oninserting="OrgDataSource_Inserting" 
            onupdating="OrgDataSource_Updating">
        <SelectParameters>
            <asp:QueryStringParameter Name="orgId" QueryStringField="OrgId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="StatesDataSource" runat="server" 
        SelectMethod="GetStates" TypeName="EventManager.Business.StateMethods">
    </asp:ObjectDataSource>


    </div>
    </form>
</body>
</html>
