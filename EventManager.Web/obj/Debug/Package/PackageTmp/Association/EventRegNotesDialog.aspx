﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventRegNotesDialog.aspx.cs" Inherits="EventManager.Web.Association.EventRegNotesDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attendee Notes</title>

    <script type="text/javascript">
    //<![CDATA[

        function CloseAndRebind(regId) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.NotesUpdated(regId);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CloseWindow() {
            GetRadWindow().close();
        }

        function CancelRadButton_Clicked(sender, args) {
            CloseWindow();
        }
    //]]>
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%; border:1 solid black;">

        <telerik:radscriptmanager ID="Radscriptmanager1" runat="server" />
        <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server"  />

        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="None" ValidationGroup="ValidationGroup1"></asp:CustomValidator>

         <asp:Panel runat="server" ID="TopPanel" Style="padding-bottom:0px;">
            <telerik:RadButton ID="OkRadButton" runat="server" Text="Save and Close" onclick="OkRadButton_Click" />&nbsp;
            <telerik:RadButton ID="CancelRadButton" runat="server" Text="Cancel" onclientclicked="CancelRadButton_Clicked" AutoPostBack="false" />
        </asp:Panel>

        <h3><asp:Label ID="OrgNameLabel" runat="server" Text="" /></h3>

        <telerik:RadTextBox ID="RegNoteRadTextBox" runat="server" Wrap="true" TextMode="MultiLine" Width="100%" MaxLength="255" EmptyMessage="Registration note">
        </telerik:RadTextBox>
        <br /><br />

        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False"
                    CellSpacing="0" GridLines="None" 
            onitemdatabound="RadGrid1_ItemDataBound" 
            onneeddatasource="RadGrid1_NeedDataSource">
            <AlternatingItemStyle BackColor="#F8F8F8" />
        <MasterTableView  DataKeyNames="Id">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
               <telerik:GridBoundColumn DataField="Attendee.FullName" DataType="System.String" 
                    FilterControlAltText="Filter Attendee.FullName column" HeaderText="Name" 
                    SortExpression="Attendee.FullName" UniqueName="AttendeeNameGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EntryDateTime" DataType="System.DateTime" ItemStyle-Width="120" HeaderStyle-Width="120" 
                    FilterControlAltText="Filter EntryDateTime column" HeaderText="Reg Date" 
                    SortExpression="EntryDateTime" UniqueName="EntryDateTime" DataFormatString="{0:g}">
<HeaderStyle Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Notes" 
                    FilterControlAltText="Filter Notes column" HeaderText="Notes" 
                    SortExpression="Notes" UniqueName="Notes">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FlagList" 
                    FilterControlAltText="Filter FlagList column" HeaderText="Flags" 
                    ReadOnly="True" SortExpression="FlagList" UniqueName="FlagList">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsSpeaker" DataType="System.Boolean" 
                    FilterControlAltText="Filter IsSpeaker column" HeaderText="Spkr" 
                    ReadOnly="True" SortExpression="IsSpeaker" UniqueName="IsSpeaker">
                </telerik:GridCheckBoxColumn>
            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>

            <ItemStyle BackColor="#F8F8F8" />

        <FilterMenu EnableImageSprites="False"></FilterMenu>
                </telerik:RadGrid>

        <asp:ObjectDataSource ID="RegAttendeesDataSource" runat="server"
            SelectMethod="GetRegisteredAttendees" TypeName="EventManager.Business.RegistrationAttendeesMethods" 
            DataObjectTypeName="EventManager.Model.RegistrationAttendee" 
            UpdateMethod="Update" OldValuesParameterFormatString="original_{0}" 
            onselecting="RegAttendeesDataSource_Selecting">
            <SelectParameters>
                <asp:Parameter Name="registrationId" Type="Int32" />
                <asp:Parameter DefaultValue="true" Name="includeCancelled" Type="Boolean" />
            </SelectParameters>
        </asp:ObjectDataSource>

    
    </div>
    </form>
</body>
</html>
