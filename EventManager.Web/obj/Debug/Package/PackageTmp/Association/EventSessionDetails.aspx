﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventSessionDetails.aspx.cs" Inherits="EventManager.Web.Association.EventSessionDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript" language="javascript">
    function autoCompleteItemSelected(source, eventArgs) {

        //get the hidden field associated with this autoCompleteExtender:*/
        var assocHiddenField = document.getElementById(source.get_id() + '_hidden');

        //set the value of the hidden field to the pair's value (personID, etc):
        assocHiddenField.value = eventArgs.get_value();

    }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   
    <div style="padding:5px;">
    Select a session:
     <telerik:RadComboBox ID="SessionsRadComboBox" Runat="server" DataSourceID="SessionsDataSource" DataTextField="SessionName" Font-Size="Small" 
            DataValueField="Id" AutoPostBack="true" Width="400" DropDownWidth="600" OnDataBound="SessionsRadComboBox_DataBound">
        </telerik:RadComboBox>
    </div>
    <div id="content">

    <asp:FormView ID="SessionFormView" runat="server" DataSourceID="SessionDataSource" 
            DefaultMode="ReadOnly" Width="201px" >
    <ItemTemplate>
        <h2 style="margin-top:5px;"><asp:Label ID="StartDateTimeTextBox" runat="server" Text='<%# Bind("StartDateTime", "{0:g}") %>' />-<asp:Label ID="Label1" runat="server" Text='<%# Bind("EndDateTime", "{0:t}") %>' />
        <br /><asp:Label ID="Label2" runat="server" Text='<%# Bind("SessionName") %>' /></h2>
    </ItemTemplate>
    </asp:FormView>

    <div style="width:800px; margin-top:5px;">
    <h3>Speakers</h3>
    <telerik:RadGrid ID="SpeakersRadGrid" runat="server" CellSpacing="0" DataSourceID="SpeakersDataSource" GridLines="None" 
        AutoGenerateColumns="False" Width="300px" OnItemCommand="SpeakersRadGrid_ItemCommand">
        <MasterTableView DataSourceID="SpeakersDataSource" DataKeyNames="Id">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Attendee.FullName" DataType="System.Int32" FilterControlAltText="Filter AttendeeId column" HeaderText="Name" SortExpression="Attendee.FullName" UniqueName="AttendeeFullName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Title" FilterControlAltText="Filter Title column" HeaderText="Title" SortExpression="Title" UniqueName="Title">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OrgName" FilterControlAltText="Filter OrgName column" HeaderText="OrgName" SortExpression="OrgName" UniqueName="OrgName">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Delete" Text="Remove" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to delete this speaker?" />
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <br />
   <telerik:RadComboBox
                 ID="NewSpeakerRadComboBox" runat="server"
                 Width="300px" Height="140px"
                 EmptyMessage="Type attendee name..."
                 MarkFirstMatch="true"
                 AllowCustomText="true"
                  EnableLoadOnDemand="true"
                AutoPostBack="true"
                onselectedindexchanged="NewSpeakerRadComboBox_SelectedIndexChanged" onitemsrequested="NewSpeakerRadComboBox_ItemsRequested">
             </telerik:RadComboBox>
                

    <asp:Label ID="SpeakerNoteLabel" runat="server" Text="Note: removing speaker DOES NOT delete their registration record." style="margin-top:5px;" />
    </div>

    <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SpeakersDataSource" runat="server" DataObjectTypeName="EventManager.Model.RegistrationAttendee" DeleteMethod="Delete" SelectMethod="GetSpeakerRegistrations" TypeName="EventManager.Business.EventSessionMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="SessionsRadComboBox" Name="sessionId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SessionDataSource" runat="server" SelectMethod="GetSession" TypeName="EventManager.Business.EventSessionMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="SessionsRadComboBox" Name="id" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>


</div>
</asp:Content>
