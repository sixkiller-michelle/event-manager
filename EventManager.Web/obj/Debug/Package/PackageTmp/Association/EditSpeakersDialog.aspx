﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditSpeakersDialog.aspx.cs" Inherits="EventManager.Web.Association.EditSpeakersDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Speakers</title>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
    //<![CDATA[

    function CloseAndRebind(speakerList) {

        var oWindow = GetRadWindow();
        oWindow.BrowserWindow.RefreshSessionGrid(speakerList);
        oWindow.close();
    }

    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

        return oWindow;
    }

    function CloseWindow() {
        GetRadWindow().close();
    }

    function ShowAttendeeDialog(attendeeId, gridRowIndex) {

        var oBrowserWnd = GetRadWindow().BrowserWindow;
        setTimeout(function () {
            oBrowserWnd.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
        }, 0);

        // Close this dialog too (just insert speaker on sessions page)
        GetRadWindow.close();

        //var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
        //wnd.setSize(850, 550);
        //return false;
    }

    function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

        if (attendeeId > 0) {

            // Insert new attendee into newAttendeeRadComboBox
            var combo = $find("<%= NewSpeakerRadComboBox.ClientID %>");
            var comboItem = new Telerik.Web.UI.RadComboBoxItem();
            comboItem.set_text(fullNameWithId);
            comboItem.set_value(attendeeId);
            combo.trackChanges();
            combo.get_items().add(comboItem);
            comboItem.select();
            combo.commitChanges();
            comboItem.scrollIntoView();
        }
    }

    function SpeakerAdded() {
        alert("Speaker added");
    }

    //]]>
    </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     
        <telerik:radscriptmanager runat="server" />
      
    
        <telerik:radbutton ID="CloseWindowRadButton" runat="server" text="Close" 
            onclick="CloseWindowRadButton_Click"  />

        <br /><br />
        <b>Speakers for:</b>
        <h2 style="margin-bottom:6px;margin-top:3px;">
            <asp:Label ID="SessionStartTimeLabel" runat="server" Text="Start" ForeColor="RoyalBlue" />&nbsp;
            <asp:Label ID="SessionNameLabel" runat="server" Text="SessionName" />
        </h2>
    
        <table style="margin: 5px; float: left; border:none;">
            <tr>
                <td style="vertical-align:top;">
                    <telerik:RadComboBox
                        ID="NewSpeakerRadComboBox" runat="server"
                        AllowCustomText="false" MarkFirstMatch="true"  
                        DataTextField="FullNameWithId" 
                        DataValueField="Id" EmptyMessage="Attendee name (last, first)..." 
                        Width="200px">
                    </telerik:RadComboBox>
                </td>
                <td style="vertical-align:top;"><telerik:RadButton ID="InsertSpeakerRadButton" runat="server" Text="Add" 
                        AutoPostBack="true" onclick="InsertSpeakerRadButton_Click" ></telerik:RadButton>
                </td>
                <td>
                    <telerik:RadButton ID="NewAttendeeRadButton" runat="server" 
                        Text="New attendee..." onclick="NewAttendeeRadButton_Click">
                    </telerik:RadButton>
                    <asp:Panel runat="server" ID="NewAttendeePanel">
                        <asp:CustomValidator ID="NewAttendeeCustomValidator" runat="server" ErrorMessage="" Display="Dynamic" ForeColor="Red"></asp:CustomValidator>
                        <table>
                        <tr>
                            <td>First Name:</td>
                             <td><telerik:RadTextBox ID="NewAttendeeFirstNameRadTextBox" runat="server" EmptyMessage="First name...">
                            </telerik:RadTextBox></td>
                        </tr>
                        <tr>
                            <td>
                            Last Name:
                            </td>
                            <td><telerik:RadTextBox ID="NewAttendeeLastNameRadTextBox" runat="server" EmptyMessage="Last name...">
                            </telerik:RadTextBox></td>
                        </tr>
                        <tr>
                            <td>
                            Title:
                            </td>
                            <td><telerik:RadTextBox ID="NewAttendeeTitleRadTextBox" runat="server" EmptyMessage="Title...">
                            </telerik:RadTextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:right;"><telerik:RadButton ID="AddAttendeeRadButton" runat="server" 
                        Text="Create" onclick="AddAttendeeRadButton_Click" /></td>
                        </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
          <%--  <tr>
                    <td colspan="2" style="vertical-align:top;">
                        <asp:LinkButton ID="NewAttendeeLinkButton" runat="server" Text="Create new attendee" />
                    </td>
                </tr>--%>
        </table>

        <telerik:radgrid id="SpeakersRadGrid" runat="server" autogeneratecolumns="False"
            cellspacing="0" gridlines="None" datasourceid="SpeakersDataSource" 
            AllowAutomaticDeletes="True" AllowAutomaticInserts="True" 
            onitemdeleted="SpeakersRadGrid_ItemDeleted" >
            <mastertableview datasourceid="SpeakersDataSource" DataKeyNames="Id">
            <commanditemsettings exporttopdftext="Export to PDF" />
<CommandItemSettings ExportToPdfText="Export to PDF" ></CommandItemSettings>

            <rowindicatorcolumn filtercontrolalttext="Filter RowIndicator column" 
                visible="True">
                <HeaderStyle Width="20px" />
            </rowindicatorcolumn>
            <expandcollapsecolumn filtercontrolalttext="Filter ExpandColumn column" 
                visible="True">
                <HeaderStyle Width="20px" />
            </expandcollapsecolumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Attendee.FullName" DataType="System.String" 
                    FilterControlAltText="Filter AttendeeName column" HeaderText="Speaker" 
                    SortExpression="Attendee.FullName" UniqueName="SpeakerNameGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Attendee.Title" DataType="System.String" 
                    FilterControlAltText="Filter Title column" HeaderText="Title" 
                    SortExpression="Attendee.Title" UniqueName="TitleGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn CommandName="Delete" Text="Remove" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to remove this person as a speaker?" ItemStyle-Width="50" />
            </Columns>
            <EditFormSettings EditFormType="Template">
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </mastertableview>
            <filtermenu enableimagesprites="False">
        </filtermenu>
        </telerik:radgrid>


        
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:9000;" EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="500px"
                Width="800px" Left="0" Top="0" ReloadOnShow="true" ShowContentDuringLoad="true" VisibleStatusbar="false"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

        <asp:ObjectDataSource ID="SpeakersDataSource" runat="server" 
            DataObjectTypeName="EventManager.Model.SessionSpeaker" DeleteMethod="Delete" 
            InsertMethod="Add" SelectMethod="GetSpeakersBySession" 
            TypeName="EventManager.Business.SessionSpeakerMethods">
            <SelectParameters>
                <asp:QueryStringParameter Name="sessionId" QueryStringField="SessionId" 
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

    </div>
    </form>
</body>
</html>
