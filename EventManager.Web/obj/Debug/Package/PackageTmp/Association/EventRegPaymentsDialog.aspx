﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventRegPaymentsDialog.aspx.cs" Inherits="EventManager.Web.Association.EventRegPaymentsDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration Charges</title>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <style type="text/css">
    html
    {
	    overflow:hidden;
    }
    html, body, form, #<%= RadGrid1PanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>

    <script type="text/javascript">
        //<![CDATA[
        function CloseAndRebind(paymentsTotal, paymentMethod, paymentNumber, balance, status) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.UpdatePaymentTotal(paymentsTotal, paymentMethod, paymentNumber, balance, status);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }

        function CancelRadButton_Clicked(sender, args) {
            CancelEdit();
        }

         //]]>
    </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" />

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"
        OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" 
        DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

        <table style="width:100%; height:100%;" border="1">
        <tr>
            <td style="height:30px; padding:5px;">
            <telerik:radbutton runat="server" ID="CloseRadButton" text="Close" width="60" onclick="CloseRadButton_Click" />
            </td>
        </tr>
         <tr>
            <td>
                <asp:Panel runat="server" ID="GridPanel" style="width:100%; height:100%;">
                
                <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="0" GridLines="None" ShowFooter="True"
                    AllowAutomaticDeletes="true" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" 
                    OnNeedDataSource="RadGrid1_NeedDataSource" OnDeleteCommand="RadGrid1_DeleteCommand" Height="100%"
                    OnInsertCommand="RadGrid1_InsertCommand" OnUpdateCommand="RadGrid1_UpdateCommand"
                    OnItemCommand="RadGrid1_ItemCommand">
                    <MasterTableView AutoGenerateColumns="False" OnDataBinding="MasterTableView_DataBinding" DataKeyNames="Id"
                        CommandItemDisplay="Top">
                        <NoRecordsTemplate>
                            There are no payments for this registration
                        </NoRecordsTemplate>
                        <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="Add new payment">
                        </CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" ItemStyle-Width="80px"
                                HeaderText="Edit" HeaderStyle-Width="80px" UpdateText="Update" ButtonType="ImageButton">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemStyle Width="60px" HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                            </telerik:GridEditCommandColumn>

                            <telerik:GridBoundColumn DataField="PaymentDate" DataType="System.DateTime" DataFormatString="{0:d}" FilterControlAltText="Filter PaymentDate column"
                                HeaderText="Date" ItemStyle-Width="80" HeaderStyle-Width="80">
                            </telerik:GridBoundColumn>
                            
                            <telerik:GridNumericColumn Aggregate="Sum" FooterAggregateFormatString="{0:C}" FooterStyle-HorizontalAlign="Right" FooterStyle-Width="60" DataField="PaymentAmt"
                                HeaderText="Amt" DataType="System.Decimal" SortExpression="PaymentAmt" UniqueName="PaymentAmt"
                                DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60"
                                HeaderStyle-Width="60px" FilterControlWidth="60px">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                            </telerik:GridNumericColumn>

                            <telerik:GridBoundColumn DataField="PaymentType" FilterControlAltText="Filter PaymentType column" ItemStyle-Width="100" HeaderStyle-Width="100"
                                HeaderText="Method" ReadOnly="True" SortExpression="PaymentType" UniqueName="PaymentType">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="PaymentNumber" FilterControlAltText="Filter PaymentNumber column" ItemStyle-Width="50" HeaderStyle-Width="50"
                                HeaderText="#" ReadOnly="True" SortExpression="PaymentNumber" UniqueName="PaymentNumber">
                            </telerik:GridBoundColumn>
                            
                            <telerik:GridBoundColumn DataField="Notes" FilterControlAltText="Filter Notes column"
                                HeaderText="Notes" ReadOnly="True" SortExpression="Notes" UniqueName="Notes">
                            </telerik:GridBoundColumn>

                            <telerik:GridButtonColumn ConfirmText="Delete this payment?" ConfirmDialogType="RadWindow"
                                ItemStyle-Width="60" HeaderStyle-Width="60" ConfirmTitle="Delete" ButtonType="ImageButton"
                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                            </telerik:GridButtonColumn>
                        </Columns>
                        <EditFormSettings EditFormType="Template">
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                            <FormTemplate>
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="3" style="padding-top: 3px;">
                                            <asp:Button ID="Button1" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                                Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />&nbsp;
                                            <asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancel" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date:
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="RadDatePicker1" runat="server" DbSelectedDate='<%# Bind("PaymentDate") %>'>
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Amount
                                        </td>
                                        <td>
                                            $
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="PaymentAmtRadTextBox" runat="server" Text='<%# Bind("PaymentAmt") %>' NumberFormat-DecimalDigits="2" Type="Currency">
                                            </telerik:RadNumericTextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Method:
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="PaymentTypesRadComboBox" runat="server" SelectedValue='<%# Bind("PaymentType") %>' MarkFirstMatch="true" AllowCustomText="false">
                                            <Items>
                                                <telerik:RadComboBoxItem Text="Credit" Value="Credit" />
                                                <telerik:RadComboBoxItem Text="Check" Value="Check" />
                                                <telerik:RadComboBoxItem Text="Cash" Value="Cash" />
                                            </Items>
                                            </telerik:RadComboBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Card/Chk #:
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <telerik:RadTextBox ID="PaymentNumberRadTextBox" runat="server" Text='<%# Bind("PaymentNumber") %>'>
                                            </telerik:RadTextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Notes
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <telerik:RadTextBox ID="NotesRadTextBox" runat="server" Text='<%# Bind("Notes") %>'>
                                            </telerik:RadTextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table><br />
                            </FormTemplate>
                        </EditFormSettings>
                    </MasterTableView>
                    <ClientSettings>
                        <Scrolling AllowScroll="true"  UseStaticHeaders="true" />
                        <Resizing AllowResizeToFit="True" EnableRealTimeResize="True" />
                    </ClientSettings>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid></asp:Panel>
            </td>
        </tr>
        </table>
    </form>
</body>
</html>
