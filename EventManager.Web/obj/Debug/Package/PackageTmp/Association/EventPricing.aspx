﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventPricing.aspx.cs" Inherits="EventManager.Web.Association.EventPricingForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
.CustomValidator1, .CustomValidator2 {margin:8px; padding:8px; padding-top:8px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal">
<telerik:RadPane runat="server" ID="HeaderPane" Height="70" Scrolling="None">

    <asp:Panel runat="server" ID="HeaderPanel" Style="padding:5px;">
        <h2>Event Pricing</h2>
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="Dynamic" ForeColor="Red" CssClass="CustomValidator1"></asp:CustomValidator>
    </asp:Panel>
    
</telerik:RadPane>

<telerik:RadPane runat="server" ID="TopPane" Height="200">
    <telerik:RadGrid ID="FeeScheduleRadGrid" runat="server" 
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
        AllowAutomaticDeletes="true" AllowAutomaticUpdates="true" 
        OnNeedDataSource="FeeScheduleRadGrid_NeedDataSource"
        OnInsertCommand="FeeScheduleRadGrid_InsertCommand"
        OnUpdateCommand="FeeScheduleRadGrid_UpdateCommand"
        OnDeleteCommand="FeeScheduleRadGrid_DeleteCommand">
        <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top">
            <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="New Fee Period..."></CommandItemSettings>
            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                        FooterText="EditCommand footer" HeaderStyle-Width="50px" HeaderText="Edit" 
                        ItemStyle-Width="50px" UniqueName="EditCommandColumn" UpdateText="Update">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" Width="50px" />
                    </telerik:GridEditCommandColumn>

                <telerik:GridBoundColumn DataField="TimeframeName" FilterControlAltText="Filter TimeframeName column"
                    HeaderText="Name" ItemStyle-Width="80" HeaderStyle-Width="80" SortExpression="TimeframeName"
                    UniqueName="TimeframeNameGridBoundColumn">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="TimeframeDesc" FilterControlAltText="Filter TimeframeDesc column"
                    HeaderText="Desc" SortExpression="TimeframeDesc" UniqueName="TimeframeDesc">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PeriodNumber" DataType="System.Int16" ItemStyle-Width="80"
                    HeaderStyle-Width="80" FilterControlAltText="Filter PeriodNumber column" HeaderText="Period"
                    SortExpression="PeriodNumber" UniqueName="PeriodNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" DataFormatString="{0:d}"
                    FilterControlAltText="Filter StartDateTime column" HeaderText="Start"
                    ItemStyle-Width="100" HeaderStyle-Width="100" SortExpression="StartDateTime"
                    UniqueName="StartDateTime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EndDateTime" DataType="System.DateTime" DataFormatString="{0:d}"
                    FilterControlAltText="Filter EndDateTime column" HeaderText="End" ItemStyle-Width="100"
                    HeaderStyle-Width="100" SortExpression="EndDateTime" UniqueName="EndDateTime">
                </telerik:GridBoundColumn>

                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" 
                        ConfirmDialogType="RadWindow" 
                        ConfirmText="Are you sure you want to delete this fee period?" 
                        ConfirmTitle="Delete" HeaderStyle-Width="50" ItemStyle-Width="50" Text="Delete" 
                        UniqueName="DeleteColumn">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>

            </Columns>
            <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                        <asp:CustomValidator ID="FormTemplateCustomValidator" runat="server" ErrorMessage="" Display="Dynamic" ForeColor="Red" CssClass="CustomValidator2"></asp:CustomValidator>
                        <table border="0" style="border:none; margin:5px;">
                            <tr>
                                <td>
                                    <asp:Label ID="Label10" runat="server" Text="Period #:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <telerik:RadNumericTextBox ID="PeriodNumberRadNumericTextBox" runat="server" Width="100" MaxLength="2"
                                        NumberFormat-DecimalDigits="0" Type="Number" Text='<%# Bind("PeriodNumber") %>'></telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label" runat="server" Text="Timeframe Name:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <telerik:RadTextBox ID="TimeframeNameTextBox" runat="server" Text='<%# Bind("TimeframeName") %>' Width="100" MaxLength="20" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Timeframe Desc:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <telerik:RadTextBox ID="TimeframeDescTextBox" runat="server" Text='<%# Bind("TimeframeDesc") %>' Width="400" MaxLength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Start:" />
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="StartDateRadDateTimePicker" runat="server" Width="150" 
                                        DbSelectedDate='<%# Bind("StartDateTime") %>'>
                                    </telerik:RadDatePicker>
                                </td>
                                <td style="text-align:right;"><asp:Label ID="Label3" runat="server" Text="End:" /></td>
                                <td><telerik:RadDatePicker ID="EndDateRadDateTimePicker" runat="server" DbSelectedDate='<%# Bind("EndDateTime") %>' Width="150">
                                    </telerik:RadDatePicker></td>
                            </tr>
                            <tr>
                                <td colspan="4" style="padding-top:3px;">
                                    <telerik:RadButton ID="btnUpdate" runat="server" 
                                        CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' 
                                        Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                    &nbsp;
                                    <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </FormTemplate>
                </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>

</telerik:RadPane>
<telerik:RadSplitBar runat="server" ID="SplitterBar1"></telerik:RadSplitBar>
<telerik:RadPane runat="server" ID="BottomPane">

    <telerik:RadGrid ID="RegFeesRadGrid" runat="server" AutoGenerateColumns="False" 
        CellSpacing="0"  GridLines="None" 
        AllowAutomaticDeletes="true" AllowAutomaticUpdates="true" 
        onneeddatasource="RegFeesRadGrid_NeedDataSource" 
        OnInsertCommand="RegFeesRadGrid_InsertCommand"
        OnUpdateCommand="RegFeesRadGrid_UpdateCommand"
        OnDeleteCommand="RegFeesRadGrid_DeleteCommand"
        oninit="RegFeesRadGrid_Init" onitemcommand="RegFeesRadGrid_ItemCommand">
        <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top" EditMode="InPlace">
            <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="New Fee Category..." />
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                Visible="True">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                Visible="True">
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                
                <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                        FooterText="EditCommand footer" HeaderStyle-Width="50px" HeaderText="Edit" 
                        ItemStyle-Width="50px" UniqueName="EditCommandColumn" UpdateText="Update">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" Width="50px" />
                    </telerik:GridEditCommandColumn>

                <%--<telerik:GridBoundColumn DataField="OrganizationFeeCategory.CategoryDesc" 
                    DataType="System.String" 
                    FilterControlAltText="Filter OrganizationFeeCategory.CategoryDesc column" 
                    HeaderText="Fee Category" SortExpression="OrganizationFeeCategory.CategoryDesc" 
                    UniqueName="CategoryDescGridBoundColumn">
                </telerik:GridBoundColumn> --%>

                <telerik:GridTemplateColumn DataField="OrganizationFeeCategory.CategoryDesc"
                    HeaderText="Fee Category" SortExpression="OrganizationFeeCategory.CategoryDesc"
                    UniqueName="CategoryDescGridTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadComboBox ID="OrgFeeCategoryRadComboBox" runat="server" AppendDataBoundItems="true" 
                            DataSourceID="OrgFeeCategoriesDataSource" DataTextField="CategoryDesc" AutoPostBack="true"
                            DataValueField="Id" SelectedValue='<%# Bind("FeeCategoryId") %>' Width="300" 
                            onselectedindexchanged="OrgFeeCategoryRadComboBox_SelectedIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadComboBox ID="OrgFeeCategoryRadComboBox" runat="server" AppendDataBoundItems="true" 
                            DataSourceID="OrgFeeCategoriesDataSource" DataTextField="CategoryDesc" AutoPostBack="true"
                            DataValueField="Id"  SelectedValue='<%# Bind("FeeCategoryId") %>' Width="300" 
                            onselectedindexchanged="OrgFeeCategoryRadComboBox_SelectedIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="CategoryDescLabel" runat="server" Text='<%# Eval("OrganizationFeeCategory.CategoryDesc") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>

                <%--<telerik:GridCheckBoxColumn DataField="IsMemberRate" DataType="System.Boolean" 
                    FilterControlAltText="Filter IsMemberRate column" HeaderText="Member" 
                    SortExpression="IsMemberRate" UniqueName="IsMemberRate">
                </telerik:GridCheckBoxColumn>--%>

                <telerik:GridTemplateColumn DataField="IsMemberRate" ItemStyle-Width="100" HeaderStyle-Width="100"
                    HeaderText="Member Rate" SortExpression="IsMemberRate"
                    UniqueName="IsMemberRateGridTemplateColumn">
                    <InsertItemTemplate>
                        <asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMemberRate") %>' />
                        <asp:Label ID="IsMemberLabel" runat="server" Text='Member Rate' Visible="false" ForeColor="Green" />
                        <asp:Label ID="IsNonMemberLabel" runat="server" Text='Non-Member Rate' Visible="false" ForeColor="Red" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="IsMemberCheckBox" runat="server" Checked='<%# Bind("IsMemberRate") %>' />
                        <asp:Label ID="IsMemberLabel" runat="server" Text='Member Rate' Visible="false" ForeColor="Green" />
                        <asp:Label ID="IsNonMemberLabel" runat="server" Text='Non-Member Rate' Visible="false" ForeColor="Red" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="IsMemberLabel" runat="server" Text="Member Rate" ForeColor="Green" Visible='<%# (bool)Eval("IsMemberRate") %>' />
                        <asp:Label ID="IsNonMemberLabel" runat="server" Text="Non-Member Rate" ForeColor="Red" Visible='<%# !(bool)Eval("IsMemberRate") %>' />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="GroupMinCount" DataType="System.Int32"  
                    FilterControlAltText="Filter GroupMinCount column" HeaderText="Min" ItemStyle-Width="50" HeaderStyle-Width="50"  
                    SortExpression="GroupMinCount" UniqueName="GroupMinCount">
                    <HeaderStyle Width="50px" />
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="GroupMaxCount" DataType="System.Int32" 
                    FilterControlAltText="Filter GroupMaxCount column" HeaderText="Max"  ItemStyle-Width="50" HeaderStyle-Width="50"  
                    SortExpression="GroupMaxCount" UniqueName="GroupMaxCount">
                    <HeaderStyle Width="50px" />
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn DataField="Period1GroupPrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #1 Group Price" SortExpression="Period1GroupPrice"
                    UniqueName="Period1GroupPriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period1GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period1GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period1GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period1GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period1GroupPriceLabel" runat="server" Text='<%# Eval("Period1GroupPrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Period1AttendeePrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #1 Attendee Price" SortExpression="Period1AttendeePrice"
                    UniqueName="Period1AttendeePriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period1AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period1AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period1AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period1AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period1AttendeePriceLabel" runat="server" Text='<%# Eval("Period1AttendeePrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

               
                <telerik:GridTemplateColumn DataField="Period2GroupPrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #2 Group Price" SortExpression="Period2GroupPrice"
                    UniqueName="Period2GroupPriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period2GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period2GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period2GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period2GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period2GroupPriceLabel" runat="server" Text='<%# Eval("Period2GroupPrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Period2AttendeePrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #2 Attendee Price" SortExpression="Period2AttendeePrice"
                    UniqueName="Period2AttendeePriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period2AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period2AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period2AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period2AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period2AttendeePriceLabel" runat="server" Text='<%# Eval("Period2AttendeePrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Period3GroupPrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #3 Group Price" SortExpression="Period3GroupPrice"
                    UniqueName="Period3GroupPriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period3GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period3GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period3GroupPriceRadTextBox" runat="server" Text='<%# Bind("Period3GroupPrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period3GroupPriceLabel" runat="server" Text='<%# Eval("Period3GroupPrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn DataField="Period3AttendeePrice" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right"
                    HeaderText="Period #3 Attendee Price" SortExpression="Period3AttendeePrice"
                    UniqueName="Period3AttendeePriceTemplateColumn">
                    <InsertItemTemplate>
                        <telerik:RadNumericTextBox ID="Period3AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period3AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <telerik:RadNumericTextBox ID="Period3AttendeePriceRadTextBox" runat="server" Text='<%# Bind("Period3AttendeePrice") %>' Type="Currency" EnabledStyle-HorizontalAlign="Right" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Period3AttendeePriceLabel" runat="server" Text='<%# Eval("Period3AttendeePrice", "{0:C}") %>' EnabledStyle-HorizontalAlign="Right" />
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridTemplateColumn>

                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" 
                        ConfirmDialogType="RadWindow" 
                        ConfirmText="Are you sure you want to delete this fee?" 
                        ConfirmTitle="Delete" HeaderStyle-Width="50" ItemStyle-Width="50" Text="Delete" 
                        UniqueName="DeleteColumn">
                        <HeaderStyle Width="50px" />
                        <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                    </telerik:GridButtonColumn>

                <%--<telerik:GridBoundColumn DataField="Period1GroupPrice"  
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period1GroupPrice column" 
                    HeaderText="Period #1 Group Price" SortExpression="Period1GroupPrice" 
                    UniqueName="Period1GroupPrice">
                </telerik:GridBoundColumn> --%>
                <%--<telerik:GridBoundColumn DataField="Period1AttendeePrice" 
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100"  ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period1AttendeePrice column" 
                    HeaderText="Period #1 Att Price" SortExpression="Period1AttendeePrice" 
                    UniqueName="Period1AttendeePrice">
                </telerik:GridBoundColumn>--%>
                <%--<telerik:GridBoundColumn DataField="Period2GroupPrice" 
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100"  ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period2GroupPrice column" 
                    HeaderText="Period #2 Group Price" SortExpression="Period2GroupPrice" 
                    UniqueName="Period2GroupPrice">
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Period2AttendeePrice" 
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period2AttendeePrice column" 
                    HeaderText="Period #2 Att Price" SortExpression="Period2AttendeePrice" 
                    UniqueName="Period2AttendeePrice">
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridBoundColumn>--%>
                <%--<telerik:GridBoundColumn DataField="Period3GroupPrice" 
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100"  ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period3GroupPrice column" 
                    HeaderText="Period #3 Group Price" SortExpression="Period3GroupPrice" 
                    UniqueName="Period3GroupPrice">
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Period3AttendeePrice" 
                    DataType="System.Decimal" ItemStyle-Width="100" HeaderStyle-Width="100"  ItemStyle-HorizontalAlign="Right" DataFormatString="{0:C}" 
                    FilterControlAltText="Filter Period3AttendeePrice column" 
                    HeaderText="Period #3 Att Price" SortExpression="Period3AttendeePrice" 
                    UniqueName="Period3AttendeePrice">
                    <HeaderStyle Width="100px" />
                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                </telerik:GridBoundColumn>--%>

            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>

</telerik:RadPane>
</telerik:RadSplitter>

<asp:ObjectDataSource ID="OrgFeeCategoriesDataSource" runat="server" 
    SelectMethod="GetFeeCategories" 
    TypeName="EventManager.Business.OrganizationMethods" 
        OldValuesParameterFormatString="original_{0}" 
        onselecting="OrgFeeCategoriesDataSource_Selecting">
    <SelectParameters>
        <asp:Parameter Name="associationId" Type="Int32" />
    </SelectParameters>
    </asp:ObjectDataSource>


</asp:Content>
