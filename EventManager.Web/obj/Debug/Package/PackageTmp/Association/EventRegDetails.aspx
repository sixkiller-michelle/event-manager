﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventRegDetails.aspx.cs" Inherits="EventManager.Web.Association.EventRegDetailsForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
#AttendeeEditTable td {padding:3px;}
table td {padding:3px;}
.ErrorMessage {color:Red;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" />
    <script type="text/javascript">
        //<![CDATA[
        function openWin() {
            var oWnd = radopen("NewOrganization.aspx", "RadWindow1");
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                var orgId = arg.OrgId;
                var orgName = arg.OrgName;
                var orgTypeId = arg.OrgTypeId;
                var bedCount = arg.BedCount;
                var isMember = arg.IsMember;

                // Add an item to the Org ComboBox for the new org
                var formView =  $find("<%= RegistrationFormView.ClientID %>");
                var combo = $find("ctl00_ctl00_MainContent_MainContent_RegistrationFormView_OrgRadComboBox");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(orgName);
                comboItem.set_value(orgId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();  

                //$get("order").innerHTML = "You chose to fly to <strong>" + cityName + "</strong> on <strong>" + seldate + "</strong>";
            }
        }
        //]]>
    </script>

<div id="content">

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="RegValidationGroup" CssClass="ErrorMessage" />
<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="None" ForeColor="Red" ValidationGroup="RegValidationGroup"></asp:CustomValidator>

<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close" OnClientClose="OnClientClose"
                NavigateUrl="Dialog1.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" Width="650" Height="480" Modal="true"
                NavigateUrl="Dialog2.aspx">
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>


<table style="width:100%;">
<tr>
    <td style="width:700px; vertical-align:top;">

    <asp:FormView ID="RegistrationFormView" runat="server"  
            DataSourceID="RegistrationDataSource" DefaultMode="Edit" 
            Width="100%" onmodechanged="RegistrationFormView_ModeChanged" DataKeyNames="Id" 
            oniteminserted="RegistrationFormView_ItemInserted" 
            onitemdeleted="RegistrationFormView_ItemDeleted" 
            onitemupdated="RegistrationFormView_ItemUpdated" 
            oniteminserting="RegistrationFormView_ItemInserting" 
            onitemupdating="RegistrationFormView_ItemUpdating">
        <EditItemTemplate>

            <h2><asp:Label ID="Label1" runat="server" Text='<%# Eval("RegistrationDesc") %>'></asp:Label></h2>

            <table class="formview-table-layout">
            <tr>
                <td>Facility:</td>
                <td>
                    <telerik:RadComboBox
                        ID="OrgRadComboBox" runat="server" DataSourceID="OrganizationsDataSource" 
                        DataTextField="OrgName" DataValueField="Id" AppendDataBoundItems="true" MarkFirstMatch="true"   
                        Width="300px" OnSelectedIndexChanged="OrganizationRadComboBox_SelectedIndexChanged"
                        SelectedValue='<%# Bind("OrganizationId") %>' DropDownWidth="300">
                    <Items>
                    <telerik:RadComboBoxItem Text="" Value="" />
                    </Items>
                    </telerik:RadComboBox>
                    <asp:Label ID="OrgIdLabel" runat="server" Text='<%# Eval("OrganizationId") %>' Visible="false" />
                </td>
            </tr>
            <tr>
                <td>Order Number:</td>
                <td><asp:Label ID="OrderNumberLabel" runat="server" Text='<%# Bind("OrderNumber") %>' /></td>
            </tr>
            <tr>
                <td>Status:</td>
                <td>
                    <telerik:RadComboBox
                        ID="StatusRadComboBox" runat="server" MarkFirstMatch="true"   
                        Width="300px"  SelectedValue='<%# Bind("Status") %>' DropDownWidth="150">
                    <Items>
                                <telerik:RadComboBoxItem Text="" Value="" />
                                <telerik:RadComboBoxItem Text="Pending" Value="Pending" />
                                <telerik:RadComboBoxItem Text="Paid" Value="Paid" />
                                <telerik:RadComboBoxItem Text="Complete" Value="Complete" />
                            </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>Notes:</td>
                <td>
                    <telerik:radtextbox ID="NotesRadTextBox" runat="server" Text='<%# Bind("Notes") %>' 
                        TextMode="MultiLine" Width="300px" Height="100px" ></telerik:radtextbox></td>
            </tr>
            </table>
            <br />
            
            <asp:Panel ID="TradeShowPanel" runat="server" Visible="false">
                <h3>Trade Show</h3>
                <table class="formview-table-layout">
                    <tr>
                        <td>Booth Number (Request):</td>
                        <td><asp:TextBox ID="BoothNumberRequestTextBox" runat="server" Text='<%# Bind("BoothNumberRequest") %>' /></td>
                    </tr>
                    <tr>
                        <td>Booth Number:</td>
                        <td><asp:TextBox ID="BoothNumberTextBox" runat="server" Text='<%# Bind("BoothNumber") %>' /></td>
                     
                    </tr>
                    <tr>
                        <td>Door Prize:</td>
                        <td><asp:TextBox ID="DoorPrizeTextBox" runat="server" Text='<%# Bind("DoorPrize") %>' /></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="PaymentPanel" runat="server">
                <h3>Payment</h3>
                <table class="formview-table-layout">
                    <tr>
                        <td>Charges:</td>
                        <td><asp:Label ID="ChargesRadTextBox" runat="server" Text='<%# Eval("ChargeTotal", "{0:n}") %>' Width="50" /></td>
                    </tr>
                     <tr>
                        <td>Payments:</td>
                        <td><asp:Label ID="Radtextbox1" runat="server" Text='<%# Eval("PaymentTotal", "{0:n}") %>' Width="50" /></td>
                    </tr> 
                    <tr>
                        <td>Balance:</td>
                        <td><asp:Label ID="BalanceLabel" runat="server"  Text='<%# Eval("Balance", "{0:n}") %>' /></td>
                    </tr>
                    <tr>
                        <td>Payment Calculation:</td>
                        <td><asp:Label ID="PricingCalculationTextBox" runat="server" Text='<%# Eval("PricingCalculation") %>' /></td>
                    </tr>
                  
                </table>
            </asp:Panel>
            <br /><br />
            
            <telerik:radbutton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />&nbsp;
            <telerik:radbutton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
           
           <br /><br /><br />
            <table style="color:Gray;">
                <tr><td>Created By:</td><td><asp:Label ID="Label2" runat="server" Text='<%# Eval("aspnet_Users.UserName") %>' /></td></tr>
                <tr><td>On:</td><td><asp:Label ID="EntryDateTimeTextBox" runat="server"  Text='<%# Bind("EntryDateTime") %>' /></td></tr>
            </table>
            <br /><br />

        </EditItemTemplate>
        <InsertItemTemplate>

            <h2>New Registration</h2>

            <table class="formview-table-layout">
            <tr>
                <td>Organization:</td>
                <td>
                    <%--<telerik:RadComboBox
                        ID="OrganizationRadComboBox" runat="server" DataSourceID="OrganizationsDataSource" AutoPostBack="true" 
                        DataTextField="OrgName" DataValueField="Id" AppendDataBoundItems="true"  Width="200" OnSelectedIndexChanged="OrganizationRadComboBox_SelectedIndexChanged"
                        SelectedValue='<%# Bind("OrganizationId") %>' DropDownWidth="300">
                    <Items>
                    <telerik:RadComboBoxItem Text="" Value="" />
                    </Items>
                    </telerik:RadComboBox>--%>
                    <telerik:RadComboBox
                        ID="OrgRadComboBox" runat="server"
                        Width="300px" Height="140px" DropDownWidth="300"
                        EmptyMessage="Type facility name..."
                        MarkFirstMatch="true"
                        AllowCustomText="true"
                        EnableLoadOnDemand="true"
                        AutoPostBack="true"
                        onselectedindexchanged="OrgRadComboBox_SelectedIndexChanged" 
                        onitemsrequested="OrgRadComboBox_ItemsRequested">
                    </telerik:RadComboBox>
                    <button onclick="openWin(); return false;">New...</button>
                    <asp:Label ID="OrgIdLabel" runat="server" Text='<%# Eval("OrganizationId") %>' Visible="false" />
                </td>
            </tr>
            <tr>
                <td>Notes:</td>
                <td><asp:TextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>' Width="200" /></td>
            </tr>
            </table>
            <br />
            
            <asp:Panel ID="TradeShowPanel" runat="server" Visible="false">
                <h3>Trade Show</h3>
                <table class="formview-table-layout">
                    <tr>
                        <td>Booth Number (Request):</td>
                        <td><asp:TextBox ID="BoothNumberRequestTextBox" runat="server" Text='<%# Bind("BoothNumberRequest") %>' /></td>
                    </tr>
                    <tr>
                        <td>Booth Number:</td>
                        <td><asp:TextBox ID="BoothNumberTextBox" runat="server" Text='<%# Bind("BoothNumber") %>' /></td>
                     
                    </tr>
                    <tr>
                        <td>Door Prize:</td>
                        <td><asp:TextBox ID="DoorPrizeTextBox" runat="server" Text='<%# Bind("DoorPrize") %>' /></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="PaymentPanel" runat="server">
                <h3>Payment</h3>
                <table class="formview-table-layout">
                    <tr>
                        <td class="label">Base Price:</td>
                        <td><telerik:RadTextBox ID="BasePriceTextBox" runat="server" Text='<%# Bind("BasePrice", "{0:0.00}") %>' /></td>
                    </tr>
                     <tr>
                        <td class="label">Total Price:</td>
                        <td><telerik:RadTextBox ID="Label4" runat="server" Text='<%# Eval("PaymentAmount", "{0:c}") %>' ReadOnly="true" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("PricingCalculation") %>'  /></td>
                    </tr>
                    <tr>
                        <td class="label"></td>
                        <td><asp:CheckBox ID="IsPaidLabel" runat="server" Checked='<%# Bind("IsPaid")%>' Text="Paid" /></td>
                    </tr>
                    <tr>
                        <td class="label">Payment Date:</td>
                        <td><telerik:RadDatePicker ID="PaymentDateRadDatePicker" runat="server" SelectedDate='<%# Bind("PaymentDate") %>' >
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />

            <telerik:radbutton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />&nbsp;
            <telerik:radbutton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />

        </InsertItemTemplate>
        <ItemTemplate>
            
           <h2><asp:Label ID="Label3" runat="server" Text='<%# Bind("Organization.OrgName") %>' /></h2>
           <asp:Label ID="OrgIdLabel" runat="server" Text='<%# Eval("OrganizationId") %>' Visible="false" />

           <telerik:RadComboBox
                        ID="OrgRadComboBox" runat="server" DataSourceID="OrganizationsDataSource" 
                        DataTextField="OrgName" DataValueField="Id" AppendDataBoundItems="true"  
                        Width="300px" OnSelectedIndexChanged="OrganizationRadComboBox_SelectedIndexChanged"
                        SelectedValue='<%# Bind("OrganizationId") %>' DropDownWidth="300" />

           <h3>Notes</h3><hr />
            <%--<telerik:radtextbox ID="NotesRadTextBox" runat="server" Text='<%# Bind("Notes") %>' 
                        TextMode="MultiLine" Width="300px" Height="100px" ReadOnly="true">
                <readonlystyle borderstyle="None" />
            </telerik:radtextbox>--%>
                <asp:Label ID="Label7" runat="server" Text='<%# Bind("Notes") %>' ></asp:Label>

            <asp:Panel ID="TradeShowPanel" runat="server" Visible="false">
                <h3>Trade Show</h3><hr />
                <table class="formview-table-layout">
                    <tr>
                        <td class="label">Booth Number (Request):</td>
                        <td><asp:Label ID="BoothNumberRequestTextBox" runat="server" Text='<%# Bind("BoothNumberRequest") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Booth Number:</td>
                        <td><asp:Label ID="BoothNumberTextBox" runat="server" Text='<%# Bind("BoothNumber") %>' /></td>
                     
                    </tr>
                    <tr>
                        <td class="label">Door Prize:</td>
                        <td><asp:Label ID="DoorPrizeTextBox" runat="server" Text='<%# Bind("DoorPrize") %>' /></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="PaymentPanel" runat="server">
                <h3>Payment</h3><hr />
                <table class="formview-table-layout">
                    <tr>
                        <td class="label">Base Price:</td>
                        <td><asp:Label ID="BasePriceTextBox" runat="server" Text='<%# Bind("BasePrice", "{0:c}") %>' /></td>
                    </tr>
                     <tr>
                        <td class="label">Total Price:</td>
                        <td><asp:Label ID="PaymentAmountLabel" runat="server" Text='<%# Bind("PaymentAmount", "{0:c}") %>' /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><asp:Label ID="PricingCalculationLabel" runat="server" Text='<%# Bind("PricingCalculation") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Paid:</td>
                        <td><asp:Label ID="IsPaidLabel" runat="server" Text='<%# Eval("IsPaid").ToString() == "1" ? "Yes": "No" %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Payment Date:</td>
                        <td><asp:Label ID="Label6" runat="server" Text='<%# Bind("PaymentDate", "{0:d}") %>' /></td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            
            

            <table class="formview-table-layout">
            <tr>
                <td class="label">Created By:</td><td><asp:Label ID="EnteredByUserIdTextBox" runat="server" Text='<%# Bind("aspnet_Users.UserName") %>' /></td>
            </tr>
            <tr>
                <td class="label">Date:</td><td><asp:Label ID="EntryDateTimeTextBox" runat="server"  Text='<%# Bind("EntryDateTime") %>' /></td>
            </tr>
            </table>

            <br />
            <br />
            
            <telerik:RadButton ID="EditButton" runat="server" CausesValidation="False"  CommandName="Edit" Text="Edit" />&nbsp;
            <telerik:RadButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" />&nbsp;
            <telerik:RadButton ID="NewButton" runat="server" CausesValidation="False"  CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>
 </td>
 <td style="vertical-align:top; padding-left:20px;">
    <asp:Panel ID="RegistrantsPanel" runat="server">
    
        <asp:ValidationSummary ID="AttendeesValidationSummary" runat="server" ForeColor="Red" ValidationGroup="AttendeesValidationGroup" CssClass="ErrorMessage" />
        <asp:CustomValidator ID="AttendeesCustomValidator" runat="server" ErrorMessage="" Display="None" ForeColor="Red" ValidationGroup="AttendeesValidationGroup"></asp:CustomValidator>

        <h3>Registrants</h3>
        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" 
            Width="100%"  AllowAutomaticUpdates="true" 
            AllowAutomaticInserts="True" AllowAutomaticDeletes="true"
            CellSpacing="0" DataSourceID="AttendeesDataSource" GridLines="None" 
            onitemdeleted="RadGrid1_ItemDeleted" onitemupdated="RadGrid1_ItemUpdated" 
            onitemdatabound="RadGrid1_ItemDataBound">
            <MasterTableView DataSourceID="AttendeesDataSource" CommandItemDisplay="Top"  DataKeyNames="Id">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>

            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>

                <Columns>
                    <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn"
                        ItemStyle-Width="80px" HeaderText="Edit" HeaderStyle-Width="80px" UpdateText="Update" ButtonType="ImageButton">
                        <HeaderStyle Width="60px"></HeaderStyle>
                        <ItemStyle Width="60px" HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridEditCommandColumn>

                    <telerik:GridBoundColumn DataField="AttendeeId" DataType="System.Int32" ItemStyle-Width="50" 
                        FilterControlAltText="Filter attendeeId column" HeaderText="Id" SortExpression="AttendeeId" HeaderStyle-Width="50" 
                        UniqueName="AttendeeId">
                    </telerik:GridBoundColumn>
                    <telerik:GridHyperLinkColumn FilterControlAltText="Filter attendee name" HeaderStyle-Width="150px" HeaderText="Attendee"
                                FilterControlWidth="120px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                                UniqueName="AttendeeNameHyperlink"
                                DataType="System.String"
                                DataTextField="Attendee.FullName"
                                DataNavigateUrlFields="AttendeeId"
                                DataNavigateUrlFormatString="AttendeeDetails.aspx?AttendeeId={0}">
                            </telerik:GridHyperLinkColumn>
                   
                    <telerik:GridBoundColumn DataField="Title" ItemStyle-Width="200" HeaderStyle-Width="200"
                        FilterControlAltText="Filter Title column" HeaderText="Title" 
                        SortExpression="Title" UniqueName="Title">
                    </telerik:GridBoundColumn>
                     <telerik:GridBoundColumn DataField="IsSpeaker"  ItemStyle-Width="50" HeaderStyle-Width="50"
                        FilterControlAltText="Filter speaker column" HeaderText="Speaker" 
                        SortExpression="Speaker" UniqueName="Speaker">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Price" DataType="System.Decimal"  ItemStyle-Width="50" HeaderStyle-Width="50"
                        FilterControlAltText="Filter Price column" HeaderText="Reg Price" ItemStyle-HorizontalAlign="Right"
                        SortExpression="Price" UniqueName="Price" DataFormatString="{0:C}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FlagList" DataType="System.String" 
                        HeaderText="Flags" 
                        SortExpression="FlagList" UniqueName="FlagListGridBoundColumn">
                    </telerik:GridBoundColumn>
                
                    <telerik:GridButtonColumn ConfirmText="Remove this attendee?" ConfirmDialogType="RadWindow" ItemStyle-Width="50" HeaderStyle-Width="50"
                        ConfirmTitle="Remove" ButtonType="ImageButton" CommandName="Delete" Text="Remove"
                        UniqueName="RemoveAttendeeColumn">
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
         </telerik:GridButtonColumn>
                </Columns>

            <EditFormSettings EditFormType="Template">
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                <FormTemplate>
                    <table id="AttendeeEditTable" style="margin:10px;">
                        <tr>
                            <td>Title:</td><td><asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>'  /></td></tr>
                        <tr>
                            <td>Notes:</td><td><asp:TextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>'  /></td></tr>
                        <tr>
                            <td>&nbsp;</td><td><asp:CheckBox ID="IsCancelledCheckBox" runat="server" Checked='<%# Bind("IsCancelled") %>' Text=" Cancelled Registration"  /></td></tr>
                        <tr>
                            <td colspan="2" style="padding-top:3px;">
                                <asp:Button ID="btnUpdate" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                     <br />
                     <br />
                    
                </FormTemplate>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False">
            <WebServiceSettings>
            <ODataSettings InitialContainerName=""></ODataSettings>
            </WebServiceSettings>
            </FilterMenu>

            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
            <WebServiceSettings>
            <ODataSettings InitialContainerName=""></ODataSettings>
            </WebServiceSettings>
            </HeaderContextMenu>
        </telerik:RadGrid>
        <br />

        <telerik:RadComboBox
            ID="NewAttendeeRadComboBox" runat="server"
            Width="300px" Height="140px"
            EmptyMessage="Type attendee name..."
            MarkFirstMatch="true"
            AllowCustomText="true"
            EnableLoadOnDemand="true"
            AutoPostBack="false"
            onitemsrequested="NewAttendeeRadComboBox_ItemsRequested">
        </telerik:RadComboBox>&nbsp;
        <telerik:radbutton ID="AddAttendeeRadButton" runat="server" text="Add" 
            onclick="AddAttendeeRadButton_Click" />

        <br /><br />


        <h3>Create New Attendee</h3>

      <%--  <asp:FormView ID="FormView2" runat="server" DataSourceID="AttendeesDataSource">
            <EditItemTemplate>
                Id:
                <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>' />
                <br />
                RegistrationId:
                <asp:TextBox ID="RegistrationIdTextBox" runat="server" 
                    Text='<%# Bind("RegistrationId") %>' />
                <br />
                AttendeeId:
                <asp:TextBox ID="AttendeeIdTextBox" runat="server" 
                    Text='<%# Bind("AttendeeId") %>' />
                <br />
                First Name:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("Attendee.FirstName") %>' />
                <br />
                Last Name:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("Attendee.LastName") %>' />
                <br />
                Title:
                <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                <br />
                Price:
                <asp:TextBox ID="PriceTextBox" runat="server" Text='<%# Bind("Price") %>' />
                <br />
                Notes:
                <asp:TextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>' />
                <br />
                Attendee:
                <asp:TextBox ID="AttendeeTextBox" runat="server" 
                    Text='<%# Bind("Attendee") %>' />
                <br />
                AttendeeReference:
                <asp:TextBox ID="AttendeeReferenceTextBox" runat="server" 
                    Text='<%# Bind("AttendeeReference") %>' />
                <br />
                Registration:
                <asp:TextBox ID="RegistrationTextBox" runat="server" 
                    Text='<%# Bind("Registration") %>' />
                <br />
                RegistrationReference:
                <asp:TextBox ID="RegistrationReferenceTextBox" runat="server" 
                    Text='<%# Bind("RegistrationReference") %>' />
                <br />
                RegistrationAttendeeFlags:
                <asp:TextBox ID="RegistrationAttendeeFlagsTextBox" runat="server" 
                    Text='<%# Bind("RegistrationAttendeeFlags") %>' />
                <br />
                SessionAttendances:
                <asp:TextBox ID="SessionAttendancesTextBox" runat="server" 
                    Text='<%# Bind("SessionAttendances") %>' />
                <br />
                FlagList:
                <asp:TextBox ID="FlagListTextBox" runat="server" 
                    Text='<%# Bind("FlagList") %>' />
                <br />
               
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                Id:
                <asp:TextBox ID="IdTextBox" runat="server" Text='<%# Bind("Id") %>' />
                <br />
                RegistrationId:
                <asp:TextBox ID="RegistrationIdTextBox" runat="server" 
                    Text='<%# Bind("RegistrationId") %>' />
                <br />
                AttendeeId:
                <asp:TextBox ID="AttendeeIdTextBox" runat="server" 
                    Text='<%# Bind("AttendeeId") %>' />
                <br />
                Title:
                <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                <br />
                Price:
                <asp:TextBox ID="PriceTextBox" runat="server" Text='<%# Bind("Price") %>' />
                <br />
                Notes:
                <asp:TextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>' />
                <br />
                Attendee:
                <asp:TextBox ID="AttendeeTextBox" runat="server" 
                    Text='<%# Bind("Attendee") %>' />
                <br />
                AttendeeReference:
                <asp:TextBox ID="AttendeeReferenceTextBox" runat="server" 
                    Text='<%# Bind("AttendeeReference") %>' />
                <br />
                <br />
                <br />
                RegistrationAttendeeFlags:<br />&nbsp;
                <asp:TextBox ID="RegistrationAttendeeFlagsTextBox" runat="server" 
                    Text='<%# Bind("RegistrationAttendeeFlags") %>' />
                    
                <br />
                <br />
                <br />
                <br />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                Id:
                <asp:Label ID="IdLabel" runat="server" Text='<%# Bind("Id") %>' />
                <br />
                RegistrationId:
                <asp:Label ID="RegistrationIdLabel" runat="server" 
                    Text='<%# Bind("RegistrationId") %>' />
                <br />
                AttendeeId:
                <asp:Label ID="AttendeeIdLabel" runat="server" 
                    Text='<%# Bind("AttendeeId") %>' />
                <br />
                Attendee Name:
                <asp:Label ID="FirstNameTextBox" runat="server" Text='<%# Bind("Attendee.FullName") %>' />
                <br />
                Title:
                <asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' />
                <br />
                Price:
                <asp:Label ID="PriceLabel" runat="server" Text='<%# Bind("Price") %>' />
                <br />
                Notes:
                <asp:Label ID="NotesLabel" runat="server" Text='<%# Bind("Notes") %>' />
                <br />
                Flags:
                <asp:Label ID="FlagLabel" runat="server" Text='<%# Bind("FlagList") %>' />
                <br />
                RegistrationAttendeeFlags:
                <asp:Label ID="RegistrationAttendeeFlagsLabel" runat="server" 
                    Text='<%# Bind("RegistrationAttendeeFlags") %>' /><br />

                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSource='<%# Bind("RegistrationAttendeeFlags") %>' DataTextField="RegistrationAttendeeFlags.EventFlag.FlagName">
                    </asp:CheckBoxList>
                <br />
               

                <br />
                
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                    CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>--%>

        <asp:FormView ID="NewAttendeeFormView" runat="server" DataSourceID="AttendeeDataSource" DataKeyNames="Id"
            DefaultMode="Insert" oniteminserting="NewAttendeeFormView_ItemInserting"  
            onitemcommand="NewAttendeeFormView_ItemCommand" 
            oniteminserted="NewAttendeeFormView_ItemInserted" >
            <InsertItemTemplate>
                <table>
                    <tr>
                       <td>First:</td><td><asp:TextBox ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' Width="100" /></td>
                       <td>Last:</td><td><asp:TextBox ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' Width="100" /></td>
                       <td>MI:</td><td><asp:TextBox ID="MiddleInitialLabel" runat="server" Text='<%# Bind("MiddleInitial") %>' Width="30" /></td>
                    </tr>
                    <tr>
                        <td>Title:</td><td colspan="5"><asp:TextBox ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' Width="200" /></td>
                    </tr>
                    <tr>
                        <td>Email:</td><td colspan="5"><asp:TextBox ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' Width="200" /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td><td colspan="5"><asp:TextBox ID="PhoneLabel" runat="server" Text='<%# Bind("PhoneNumber") %>' Width="200" /></td>
                    </tr>
                </table><br />
                <telerik:RadButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" /> &nbsp;
                <telerik:RadButton ID="InsertCancelButton" runat="server"   CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                
                <table>
                    <tr>
                       <td>First:</td><td><asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' /></td>
                       <td>Last:</td><td><asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' /></td>
                       <td>MI:</td><td><asp:Label ID="MiddleInitialLabel" runat="server" Text='<%# Bind("MiddleInitial") %>' /></td>
                    </tr>
                    <tr>
                        <td>Title:</td><td colspan="5"><asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' /></td>
                    </tr>
                    <tr>
                        <td>Email:</td><td colspan="5"><asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' /></td>
                    </tr>
                    <tr>
                        <td>Phone:</td><td colspan="5"><asp:Label ID="Label2" runat="server" Text='<%# Bind("PhoneNumber") %>' /></td>
                    </tr>
                </table>
                           
                <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>

    </asp:Panel>
 </td>
 </tr>
 </table>

    <asp:ObjectDataSource ID="RegistrationDataSource" runat="server" 
        DataObjectTypeName="EventManager.Model.Registration" DeleteMethod="Delete" 
        InsertMethod="AddRegistration" SelectMethod="GetRegistration" 
        TypeName="EventManager.Business.RegistrationMethods" UpdateMethod="Update" 
        oninserted="RegistrationDataSource_Inserted">
        <SelectParameters>
            <asp:QueryStringParameter Name="registrationId" QueryStringField="RegId" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetRegisteredAttendees" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods" 
        DataObjectTypeName="EventManager.Model.RegistrationAttendee" 
        DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:ControlParameter ControlID="RegistrationFormView" Name="registrationId" PropertyName="SelectedValue" Type="Int32" />
            <asp:Parameter Name="includeCancelled" Type="Boolean" DefaultValue="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="OrganizationsDataSource" runat="server" 
        SelectMethod="GetOrganizationsByAssociation" OnSelecting="OrganizationsDataSource_Selecting" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeeDataSource" runat="server" 
            DataObjectTypeName="EventManager.Model.Attendee" InsertMethod="AddAttendee"
            SelectMethod="GetAttendee" 
            TypeName="EventManager.Business.AttendeeMethods"
            oninserted="AttendeeDataSource_Inserted">
        <SelectParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="attendeeId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="AttendeeFlagsDataSource" runat="server" 
        DataObjectTypeName="EventManager.Model.RegistrationAttendeeFlag" 
        DeleteMethod="Delete" InsertMethod="Add" 
        SelectMethod="GetRegisteredAttendeeFlags" 
        TypeName="EventManager.Business.RegistrationAttendeeFlags">
        <SelectParameters>
            <asp:ControlParameter ControlID="RadGrid1" Name="registrationAttendeeId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
</asp:Content>
