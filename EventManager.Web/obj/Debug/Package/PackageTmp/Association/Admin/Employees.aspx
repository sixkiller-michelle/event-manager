﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="EventManager.Web.EmployeesForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="None" BorderSize="0" Orientation="Horizontal">
    <telerik:RadPane ID="TopRadPane" runat="server" Scrolling="None" Height="80">
    
        <asp:Panel ID="HeaderPanel" runat="server" style="padding:10px;">
            <h2 style="margin-top:0px;">Employees</h2>

            <asp:CheckBox ID="InactiveCheckBox" AutoPostBack="true" Checked="false" runat="server" Text="Include inactive employees" OnCheckedChanged="InactiveCheckBox_CheckedChanged"/><br /><br />
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ForeColor="Red" Display="Dynamic"></asp:CustomValidator><br />
        </asp:Panel>

    </telerik:RadPane>
    <telerik:RadPane ID="BottomRadPane" runat="server" Scrolling="None">

        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" CellSpacing="0"
            OnNeedDataSource="RadGrid1_NeedDataSource" GridLines="Horizontal" Height="98%"
            Width="100%" OnItemCommand="RadGrid1_ItemCommand" OnUpdateCommand="RadGrid1_UpdateCommand"
            OnItemCreated="RadGrid1_ItemCreated" OnDataBound="RadGrid1_DataBound" OnItemDataBound="RadGrid1_ItemDataBound"
            OnInsertCommand="RadGrid1_InsertCommand">
            <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top">
                <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="Add new employee">
                </CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn"
                        ItemStyle-Width="80px" HeaderText="" HeaderStyle-Width="80px" UpdateText="Update"
                        ButtonType="ImageButton">
                        <HeaderStyle Width="80px"></HeaderStyle>
                        <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="MyImageButton">
                        </ItemStyle>
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter Id column"
                        HeaderText="Id" SortExpression="Id" UniqueName="Id">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullName column"
                        HeaderText="Name" SortExpression="FullName" UniqueName="FullName">
                    </telerik:GridBoundColumn>

                    <telerik:GridTemplateColumn
                        DataField="aspnet_Users.UserName"
                        DataType="System.String"
                        FilterControlAltText="Filter UserName column"
                        HeaderText="User Name"
                        SortExpression="aspnet_Users.UserName"
                        UniqueName="UserNameGridBoundColumn">
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn
                        UniqueName="UserIdGridTemplateColumn" 
                        HeaderText="User Id"
                        SortExpression="IsEmployee" 
                        ItemStyle-Width="70" 
                        HeaderStyle-Width="70" 
                        ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center">

                        <ItemTemplate>
                            <asp:Label 
                                ID="EmailLabel" 
                                runat="server" 
                                Text='<%# Eval("UserName") %>'>

                            </asp:Label>

                            <telerik:RadButton 
                                CommandName="Invite" 
                                ID="InviteButton" 
                                Text="Invite" 
                                runat="server">

                            </telerik:RadButton>
                        </ItemTemplate>

                        <HeaderStyle 
                            HorizontalAlign="Center" 
                            Width="70px">

                        </HeaderStyle>
                        <ItemStyle 
                            HorizontalAlign="Center"
                            Width="70px">

                        </ItemStyle>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="IsEmployeeGridTemplateColumn" HeaderText="Employee"
                        SortExpression="IsEmployee" ItemStyle-Width="70" HeaderStyle-Width="70" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Image ID="EmployeeImage" runat="server" ImageUrl="~/Images/arrow2.gif" Width="10"
                                Visible='<%# Eval("IsEmployee") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="EmployeeRadTextBox" runat="server" Text='<%# Bind("IsEmployee") %>'>
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn UniqueName="IsAdminGridTemplateColumn" HeaderText="Admin"
                        SortExpression="IsAdmin" ItemStyle-Width="50" HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Image ID="AdminImage" runat="server" ImageUrl="~/Images/arrow2.gif" Width="10"
                                Visible='<%# (bool)Eval("IsAdmin") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="IsAdminRadTextBox" runat="server" Text='<%# Bind("IsAdmin") %>'>
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
                    </telerik:GridTemplateColumn>
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                
                        <div style="clear: both; width: 100%; height: 100%; padding:5px;"  class="RadGridEditForm">
                            <table class="formview-table-layout">
                                <tr>
                                    <td colspan="2">
                                        <asp:ValidationSummary ID="FormTemplateValidationSummary" runat="server" ValidationGroup="FormTemplateValidationGroup"
                                            ForeColor="Red" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ValidationGroup="FormTemplateValidationGroup"
                                            Display="None"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h3 style="margin-top: 0px;">
                                            <asp:Label ID="EmployeeName" runat="server" Text='<%# Eval("FullName") %>' /></h3>
                                    </td>
                                    <td rowspan="6" style="vertical-align: top;">
                                        <asp:Panel ID="RolesPanel" runat="server" Style="width: 200px; float: left; margin-top: 20px;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="IsEmployeeCheckBox" runat="server" Text=" Employee" /><br />
                                                        <asp:CheckBox ID="IsAdminCheckBox" runat="server" Text=" Admin" /><br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>First Name:
                                    </td>
                                    <td>
                                        <telerik:RadTextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>'
                                            CausesValidation="true" Width="200" MaxLength="30" />
                                        <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" ErrorMessage="First name is required"
                                            ControlToValidate="FirstNameTextBox" ValidationGroup="FormTemplateValidationGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last Name:
                                    </td>
                                    <td>
                                        <telerik:RadTextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' CausesValidation="true"
                                            Width="200" MaxLength="30" />
                                        <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" ErrorMessage="Last name is required"
                                            ControlToValidate="LastNameTextBox" ValidationGroup="FormTemplateValidationGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email:
                                    </td>
                                    <td>
                                        <telerik:RadTextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' CausesValidation="true"
                                            Width="200" MaxLength="30" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email is required"
                                            ControlToValidate="LastNameTextBox" ValidationGroup="FormTemplateValidationGroup">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:CheckBox ID="IsActiveCheckBox" runat="server" Checked='<%# Bind("IsActive") %>' Text="Active" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-top: 3px;">
                                        <telerik:RadButton ID="btnUpdate" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                            Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                        &nbsp;
                                        <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </telerik:RadPane>
</telerik:RadSplitter>

    <asp:ObjectDataSource ID="EmployeesDataSource" runat="server" SelectMethod="GetActiveEmployees" 
        TypeName="IndepthSolutions.Business.EmployeeMethods" 
        OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SupervisorsDataSource" runat="server" 
        SelectMethod="GetSupervisorsAndExecutives" 
        TypeName="IndepthSolutions.Business.EmployeeMethods" 
        OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>


</asp:Content>
