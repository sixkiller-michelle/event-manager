﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventRegAttFlagsDialog.aspx.cs" Inherits="EventManager.Web.Association.EventRegAttFlagsDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attendee Flags</title>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <style type="text/css">
    html
    {
	    overflow:hidden;
    }
    html, body, form { height:100%; min-height:100%; padding:0px; margin:0px;}
    </style>

    <script type="text/javascript">
        //<![CDATA[

        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("Insert") >= 0 ||
                    args.get_eventTarget().indexOf("Update") >= 0) {
                args.set_enableAjax(false);
            }
        }

        function CloseAndRebind(flagsList, regAttendeeId) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.FlagStringUpdated(flagsList, regAttendeeId);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }

        function CancelRadButton_Clicked(sender, args) {
            CancelEdit();
        }

         //]]>
    </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" BackColor="AntiqueWhite" />

    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" Orientation="Horizontal">
    <telerik:RadPane runat="server" ID="TopRadPane" Height="50" BackColor="White">
        <asp:Panel runat="server" ID="TopPanel" Style="padding:10px;">
            <telerik:RadButton ID="OkRadButton" runat="server" Text="Save and Close" onclick="OkRadButton_Click" />&nbsp;
            <telerik:RadButton ID="CancelRadButton" runat="server" Text="Cancel" onclientclicked="CancelRadButton_Clicked" AutoPostBack="false" />
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane runat="server" ID="BottomRadPane" Scrolling="Both">

         <asp:CheckBoxList ID="AttendeeFlagsCheckBoxList" runat="server" Width="98%"
            DataSourceID="EventFlagsDataSource" DataTextField="FlagName" Style="margin-left:10px; margin-bottom:10px;" 
            DataValueField="Id" ondatabound="AttendeeFlagsCheckBoxList_DataBound">
         </asp:CheckBoxList>

    </telerik:RadPane>
    </telerik:RadSplitter>

     <asp:ObjectDataSource ID="EventFlagsDataSource" runat="server"
            SelectMethod="GetFlagsByEvent" 
            TypeName="EventManager.Business.EventFlagMethods" 
            onselecting="EventFlagsDataSource_Selecting">
        <SelectParameters>
            <asp:Parameter Name="eventId" Type="Int32" />
        </SelectParameters>
     </asp:ObjectDataSource>

    </form>
</body>
</html>
