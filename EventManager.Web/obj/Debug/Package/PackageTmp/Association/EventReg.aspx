﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventReg.aspx.cs" Inherits="EventManager.Web.Association.EventRegForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <style type="text/css">
    html
    {
	    overflow:hidden;
    }
    #<%= RegGridRadGridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"
    OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
    DefaultLoadingPanelID="RadAjaxLoadingPanel1">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RegistrationsRadGrid">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="UpdateStatusRadComboBox">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="UpdateStatusRadComboBox" />
                <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />


    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" BorderStyle="Solid" Orientation="Horizontal">
    <telerik:RadPane runat="server" ID="NewRegRadPane" Scrolling="Both" Height="100" >

        <%--<telerik:RadMenu runat="server" ID="ActionsRadMenu" 
            OnClientItemClicking="OnClientItemClicking" 
            OnItemClick="ActionsRadMenu_ItemClick">
            <Items>
                <telerik:RadMenuItem runat="server" Text="New" PostBack="true" 
                    NavigateUrl="~/Association/EventRegDetails.aspx" Width="200px"></telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Update to 'Paid'" PostBack="true" ></telerik:RadMenuItem>
            </Items>
        </telerik:RadMenu>
        <br />--%>

        <h2 style="margin-left:5px;">Event Registrations - Facility Summary</h2>

        <table>
        <tr>
            <td><telerik:radbutton ID="ClearFilterRadButton" runat="server" text="Clear Filter" AutoPostBack="true" CommandName="ClearFilter" onclick="ClearFilterRadButton_Click" />&nbsp;
                <telerik:radbutton ID="NewRegRadButton" runat="server" text="New Registration" PostBackUrl="~/Association/EventRegDetails.aspx" /></td>
            <td>Update Status:&nbsp;</td>
            <td>
                <telerik:RadComboBox ID="UpdateStatusRadComboBox" Width="90" Height="150px" 
                    runat="server" DropDownWidth="90" AutoPostBack="true"
                    onselectedindexchanged="UpdateStatusRadComboBox_SelectedIndexChanged">
                    <Items>
                        <telerik:RadComboBoxItem Text="" Value="" />
                        <telerik:RadComboBoxItem Text="[None]" Value="None" />
                        <telerik:RadComboBoxItem Text="Pending" Value="Pending" />
                        <telerik:RadComboBoxItem Text="Paid" Value="Paid" />
                        <telerik:RadComboBoxItem Text="Complete" Value="Complete" />
                    </Items>
                </telerik:RadComboBox></td>
        </tr>
        </table>
        
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" ValidationGroup="ValidationGroup1" Display="None"></asp:CustomValidator>

    </telerik:RadPane>
    <telerik:RadSplitBar runat="server"></telerik:RadSplitBar>
    <telerik:RadPane ID="RegGridRadPane" runat="server" OnClientResized="RegGridPaneResized" EnableViewState="true" Scrolling="None" >
    
        <telerik:RadGrid ID="RegistrationsRadGrid" runat="server"
                AutoGenerateColumns="False" CellSpacing="0" Height="100%" Width="100%"
                GridLines="None" AllowSorting="true" AllowFilteringByColumn="true" 
               EnableLinqExpressions="false"  
                AllowAutomaticDeletes="True" AllowMultiRowSelection="True" 
                onneeddatasource="RegistrationsRadGrid_NeedDataSource" 
            onitemcreated="RegistrationsRadGrid_ItemCreated" 
            onitemdatabound="RegistrationsRadGrid_ItemDataBound" 
            ondeletecommand="RegistrationsRadGrid_DeleteCommand">
                <groupingsettings casesensitive="False"></groupingsettings>
                <ClientSettings>
                    <scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                    <selecting AllowRowSelect="True" />
                </ClientSettings>
                <MasterTableView DataKeyNames="Id" ClientDataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="100px"></HeaderStyle>
                </RowIndicatorColumn>

                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="100px"></HeaderStyle>
                </ExpandCollapseColumn>

                <Columns>
                <telerik:GridBoundColumn DataField="OrderNumber" ItemStyle-Width="50" HeaderStyle-Width="50" 
                    FilterControlAltText="Filter OrderNumber column" CurrentFilterFunction="EqualTo" FilterControlWidth="30" 
                    HeaderText="Order #" SortExpression="OrderNumber" ShowFilterIcon="false" 
                    UniqueName="OrderNumberGridBoundColumn">
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="OrgNameTemplateColumn" DataField="RegistrationDesc" HeaderText="Facility/Individual" HeaderStyle-HorizontalAlign="Left" 
                    AllowFiltering="true" FilterControlWidth="230px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                    SortExpression="RegistrationDesc" 
                    ItemStyle-Width="280" HeaderStyle-Width="280">
                    <ItemTemplate>
                        <asp:Label ID="OrgIdLabel" runat="server" Text='<%# Eval("OrganizationId") %>' Visible="false"></asp:Label>
                        <asp:LinkButton ID="OrgNameLinkButton" runat="server" Text='<%# Eval("RegistrationDesc") %>' CommandName="ViewOrgDetails" />
                    </ItemTemplate>
                    <HeaderStyle Width="280px"></HeaderStyle>
                </telerik:GridTemplateColumn>

           <%--     <telerik:GridHyperLinkColumn FilterControlAltText="Filter facility name" HeaderText="Facility/Individual" ItemStyle-Width="280" HeaderStyle-Width="280"
                                    FilterControlWidth="230px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AllowSorting="true"
                                    UniqueName="RegistrationDescHyperlink"
                                    DataType="System.String"
                                    DataTextField="RegistrationDesc"
                                    DataNavigateUrlFields="Id, EventId"
                                    DataNavigateUrlFormatString="EventRegDetails.aspx?RegId={0}&EventId={1}">
                                <HeaderStyle Width="280px"></HeaderStyle>
                                </telerik:GridHyperLinkColumn>
--%>
                <telerik:GridHyperLinkColumn HeaderStyle-Width="50px" HeaderText="Attendees"
                                    FilterControlWidth="50px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                                    UniqueName="NumberOfRegistrantsHyperlink" AllowSorting="true"
                                    DataType="System.String" AllowFiltering="false"
                                    DataTextField="NumberOfRegistrants"
                                    DataNavigateUrlFields="Id, EventId"
                                    DataNavigateUrlFormatString="EventRegDetails.aspx?RegId={0}&EventId={1}">
        <HeaderStyle Width="50px"></HeaderStyle>
                                </telerik:GridHyperLinkColumn>
               

                <%--Booth Request--%>
               <telerik:GridBoundColumn DataField="BoothNumberRequest" 
                    FilterControlAltText="Filter BoothNumberRequest column" AllowFiltering="false"
                    HeaderText="Booth Number (Req)" SortExpression="BoothNumberRequest" 
                    UniqueName="BoothNumberRequest">
                </telerik:GridBoundColumn>
                
                <%--Booth Number--%>
                <telerik:GridBoundColumn DataField="BoothNumber" AllowFiltering="false"
                    FilterControlAltText="Filter BoothNumber column" HeaderText="Booth Number" 
                    SortExpression="BoothNumber" UniqueName="BoothNumber">
                </telerik:GridBoundColumn>

                <%--DoorPrize--%>
                <telerik:GridBoundColumn DataField="DoorPrize" AllowFiltering="false"
                    FilterControlAltText="Filter DoorPrize column" HeaderText="Door Prize" 
                    SortExpression="DoorPrize" UniqueName="DoorPrize">
                </telerik:GridBoundColumn>

                  <telerik:GridTemplateColumn UniqueName="TotalChargesTemplateColumn" DataField="ChargeTotal" HeaderText="Charges" HeaderStyle-HorizontalAlign="Left" 
                        AllowFiltering="true" FilterControlWidth="30px" AutoPostBackOnFilter="true" CurrentFilterFunction="GreaterThan" ShowFilterIcon="true"
                        HeaderStyle-Width="80px" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Right" DataType="System.Decimal" 
                        SortExpression="ChargeTotal" >
                        <ItemTemplate>
                            <asp:LinkButton ID="TotalChargesLinkButton" runat="server" Text='' CommandName="ViewCharges" />
                        </ItemTemplate>
                        <HeaderStyle Width="80px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridTemplateColumn>

                 <telerik:GridTemplateColumn UniqueName="TotalPaymentsTemplateColumn" DataField="PaymentTotal" HeaderText="Payments" HeaderStyle-HorizontalAlign="Left" 
                        AllowFiltering="true" FilterControlWidth="30px" AutoPostBackOnFilter="true" CurrentFilterFunction="GreaterThan" ShowFilterIcon="true"
                        HeaderStyle-Width="80px" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Right" DataType="System.Decimal" 
                        SortExpression="PaymentTotal">
                        <ItemTemplate>
                            <asp:LinkButton ID="TotalPaymentsLinkButton" runat="server" Text='<%# Eval("PaymentTotal", "{0:c}") %>' CommandName="ViewCharges" />
                        </ItemTemplate>
                        <HeaderStyle Width="80px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="PaymentType" 
                        FilterControlAltText="Filter PaymentType column" HeaderStyle-Width="70" AllowFiltering="false" 
                        HeaderText="Payment Type" ReadOnly="True" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="70"
                        SortExpression="PaymentType" UniqueName="PaymentTypeGridBoundColumn">
                    </telerik:GridBoundColumn> 

                     <telerik:GridBoundColumn DataField="PaymentNumber" AllowFiltering="false" 
                        FilterControlAltText="Filter PaymentNumber column" HeaderStyle-Width="60" 
                        HeaderText="#" ReadOnly="True" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="60"
                        SortExpression="PaymentNumber" UniqueName="PaymentNumberGridBoundColumn">
                    </telerik:GridBoundColumn> 
                    
                    <telerik:GridBoundColumn DataField="Balance" CurrentFilterFunction="GreaterThan" FilterControlWidth="30"
                        FilterControlAltText="Filter Balance column" DataFormatString="{0:C}" HeaderStyle-Width="80" 
                        HeaderText="Balance" ReadOnly="True" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80"
                        SortExpression="Balance" UniqueName="BalanceGridBoundColumn">
                    </telerik:GridBoundColumn> 
                    
                  <%--  <telerik:GridBoundColumn DataField="PricingCalculation" AllowFiltering="false"
                    FilterControlAltText="Filter PricingCalculation column" 
                    HeaderText="Pricing Calculation" ReadOnly="True" 
                    SortExpression="PricingCalculation" UniqueName="PricingCalculation">
                </telerik:GridBoundColumn>--%>

                <telerik:GridTemplateColumn UniqueName="StatusTemplateColumn" DataField="Status" HeaderText="Status" 
                    SortExpression="Status"  HeaderStyle-Width="100px" ItemStyle-Width="100">
                  <ItemTemplate>
                        <%# Eval("Status")%>
                    </ItemTemplate>
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="StatusRadComboBox" Width="90" Height="150px" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("StatusTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="StatusRadComboBoxIndexChanged" DropDownWidth="90">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" Value="" />
                                <telerik:RadComboBoxItem Text="Pending" Value="Pending" />
                                <telerik:RadComboBoxItem Text="Paid" Value="Paid" />
                                <telerik:RadComboBoxItem Text="Complete" Value="Complete" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                            <script type="text/javascript">
                                function StatusRadComboBoxIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("StatusTemplateColumn", args.get_item().get_value(), "EqualTo");
                                }
                            </script>
                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>
             
             <%-- <telerik:GridBoundColumn DataField="Notes" AllowFiltering="false"
                    FilterControlAltText="Filter Notes column" HeaderText="Notes" 
                    SortExpression="Notes" UniqueName="Notes">
                </telerik:GridBoundColumn>--%>

                <telerik:GridTemplateColumn UniqueName="NotesTemplateColumn" DataField="Notes" HeaderText="Notes"
                        AllowFiltering="false" SortExpression="Notes" ItemStyle-Width="150" HeaderStyle-Width="150" >
                        <ItemTemplate>
                            <%# Eval("Notes")%>
                            <asp:LinkButton ID="AttendeeNotesLinkButton" runat="server" Text='[*]' Visible='<%# Eval("HasAttendeeNotes")%>' CommandName="ViewAttendeeNotes" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                 </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="EntryDateTime" DataType="System.DateTime" CurrentFilterFunction="GreaterThan" 
                    ItemStyle-Width="130" HeaderStyle-Width="130" ItemStyle-HorizontalAlign="Right" FilterControlWidth="80" 
                    FilterControlAltText="Filter EntryDateTime column" HeaderText="Created" 
                    SortExpression="EntryDateTime" UniqueName="EntryDateTime" 
                    DataFormatString="{0:g}">
        <ItemStyle HorizontalAlign="Right" Width="130"></ItemStyle>
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="aspnet_Users.UserName" DataType="System.String" 
                    FilterControlAltText="Filter EnteredByUserId column" AllowFiltering="false" 
                    HeaderText="Entered By" SortExpression="EnteredByUserId" ItemStyle-Width="80" HeaderStyle-Width="80" 
                    UniqueName="EnteredByUserId">
                </telerik:GridBoundColumn>

               
                <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this registration?  This will also delete all the associated attendee registrations. " ConfirmDialogType="RadWindow" ItemStyle-Width="50" HeaderStyle-Width="50"
                                ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Remove"
                                UniqueName="DeleteRegistrationColumn">
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>

        <FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>

    </telerik:RadPane>
    </telerik:RadSplitter>

    <script type="text/javascript">
                //<![CDATA[

        var rowIndex;

        function CollapseNewRegPane() {
            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
            var isCollapseSuccess = pane.collapse();

        }

        function RegGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= RadSplitter1.ClientID %>");
            var regGrid = $find("<%= RegistrationsRadGrid.ClientID %>");
            regGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            regGrid.repaint();
        }

        function OnClientItemClicking(sender, args) {
            if (args.get_item().get_text() == "Print Single Badge") {
                ShowPrintBadgeForm();
            }
            else if (args.get_item().get_text() == "Print Certificate") {
                ShowCertificateReport();
            }
        }

        function ShowOrgDialog(orgId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "EditOrgDialog");
            window.setSize(850, 450);
            return false;
        }


        function ShowChargesDialog(regId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("EventRegChargesDialog.aspx?RegId=" + regId, "RegChargesDialog");
            wnd.setSize(700, 400);
            return false;
        }

        function ShowCalculatedChargesDialog(regId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("EventRegCalcDialog.aspx?RegId=" + regId, "RegCalculatedChargesDialog");
            wnd.setSize(500, 500);
            return false;
        }

        function ShowPaymentsDialog(regId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("EventRegPaymentsDialog.aspx?RegId=" + regId, "RegPaymentsDialog");
            wnd.setSize(600, 400);
            return false;
        }

        function ShowNotesDialog(regId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("EventRegNotesDialog.aspx?RegId=" + regId, "AttendeeNotesDialog");
            wnd.setSize(700, 400);
            return false;
        }

        function OrgUpdated(orgId, orgName) {

            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(editedDataItem.getDataKeyValue("Id"));
        }

        function UpdateChargeTotal(chargeTotal) {

            // Update the reg row with the new charge total
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            editedDataItem.get_cell("TotalChargesTemplateColumn").children[0].innerText = "$" + formatCurrency(chargeTotal);
        }

       

        function UpdatePaymentTotal(paymentTotal, paymentMethod, paymentNumber, balance, status) {

            // Update the reg row with the new payment total
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            editedDataItem.get_cell("TotalPaymentsTemplateColumn").children[0].innerText = "$" + formatCurrency(paymentTotal);
            editedDataItem.get_cell("PaymentTypeGridBoundColumn").innerText = paymentMethod;
            editedDataItem.get_cell("PaymentNumberGridBoundColumn").innerText = paymentNumber;
            editedDataItem.get_cell("BalanceGridBoundColumn").innerText = "$" + formatCurrency(balance);
            if (balance == 0) {
                editedDataItem.get_cell("BalanceGridBoundColumn").style.backgroundColor = "transparent";
                editedDataItem.get_cell("BalanceGridBoundColumn").style.color = "black";
            }
            else {
                editedDataItem.get_cell("BalanceGridBoundColumn").style.backgroundColor = "red";
                editedDataItem.get_cell("BalanceGridBoundColumn").style.color = "white";
            }
            editedDataItem.get_cell("StatusTemplateColumn").innerText = status;
        }

        function NotesUpdated(regId) {

            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(regId);
        }

        function formatCurrency(num) {
            num = isNaN(num) || num === '' || num === null ? 0.00 : num;
            return parseFloat(num).toFixed(2);
        }

        //]]>
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="AttendeesDialog" runat="server" Title="Attendees" Height="250px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true" VisibleStatusbar="false"
                Modal="true" />
            <telerik:RadWindow ID="EditOrgDialog" runat="server" Title="Edit Facility" Height="460px"
                Width="850px" Left="100px" ReloadOnShow="true" ShowContentDuringLoad="false" VisibleStatusbar="true"
                Modal="true" />
            <telerik:RadWindow ID="RegChargesDialog" runat="server" Title="Registration Charges" Height="500px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true" VisibleStatusbar="false"
                Modal="true" />
            <telerik:RadWindow ID="RegCalculatedChargesDialog" runat="server" Title="Registration Charges" Height="500px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true" VisibleStatusbar="false"
                Modal="true" />
            <telerik:RadWindow ID="RegPaymentsDialog" runat="server" 
                Title="Registration Payments" Height="500px" VisibleStatusbar="false"
                Width="600px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="AttendeeNotesDialog" runat="server" 
                Title="Attendee Notes" Height="500px" VisibleStatusbar="false"
                Width="700px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="RegistrationsDataSource" runat="server" 
        DataObjectTypeName="EventManager.Model.Registration" DeleteMethod="Delete" 
        SelectMethod="GetRegistrationsForEvent" 
        TypeName="EventManager.Business.RegistrationMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
