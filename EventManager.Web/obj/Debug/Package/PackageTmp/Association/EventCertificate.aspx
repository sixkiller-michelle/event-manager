﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventCertificate.aspx.cs" Inherits="EventManager.Web.Association.EventCertificate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadButton ID="SaveRadButton" runat="server" Text="Save" 
        onclick="SaveRadButton_Click">
    </telerik:RadButton>

    <br />

    <telerik:RadEditor ID="HeaderTextRadEditor" runat="server">
    <ImageManager UploadPaths="~Association/Documents/Events/" ViewPaths="~Association/Documents/Events/" />
    <CssFiles>
        <telerik:EditorCssFile Value="~/Association/Styles/layout.css" />
        <telerik:EditorCssFile Value="~/Association/Styles/style.css" />
        <telerik:EditorCssFile Value="~/Association/radeditor.css" />
    </CssFiles>
    </telerik:RadEditor>

    <telerik:RadEditor ID="FooterTextRadEditor" runat="server">
    </telerik:RadEditor>

</asp:Content>
