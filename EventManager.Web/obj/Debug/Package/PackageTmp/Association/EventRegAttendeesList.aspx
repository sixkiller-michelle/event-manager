﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.Master" AutoEventWireup="true" CodeBehind="EventRegAttendeesList.aspx.cs" Inherits="EventManager.Web.Association.EventRegAttendeesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

 <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <style type="text/css">
    #<%= RegRadGridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1" OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated">
     <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GridListView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridListView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewRegDoneButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="NewRegRadPane" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewRegAddButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridListView" />
                    <telerik:AjaxUpdatedControl ControlID="NewRegPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewAttendeeRadComboBox">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="NewAttendeeRadComboBox" />
                    <telerik:AjaxUpdatedControl ControlID="NewRegPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
           
        </AjaxSettings>
    </telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />

  <telerik:RadMenu runat="server" ID="ActionsRadMenu"  Skin="Default"
         OnClientItemClicking="ActionsRadMenu_ItemClicking" 
         OnItemClick="ActionsRadMenu_ItemClick" OnClientItemClosing="ActionsRadMenu_ItemClosing">
        <Items>
            <telerik:RadMenuItem runat="server" Text="" PostBack="false" />
            <telerik:RadMenuItem runat="server" Text="Clear Filters" Width="100" ImageUrl="~/Images/filterCancel.gif" PostBack="true" >
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="New" PostBack="false" >
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Selected Attendees..." Width="300px" PostBack="False" >
                <Items>
        
                <telerik:RadMenuItem Width="300px" Text="Print Single Badge" PostBack="false" />
                <telerik:RadMenuItem IsSeparator="true"></telerik:RadMenuItem>

                <telerik:RadMenuItem Width="300px" Text="Print">
                    <ItemTemplate>
                        <table>
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="PrintMenuLabel" runat="server" Text="Print:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                               
                                <telerik:RadComboBox ID="PrintMenuRadComboBox" Width="150" AutoPostBack="true" 
                                       Font-Size="Small" Style="z-index: 10000"
                                       runat="server" 
                                       DropDownWidth="200" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                       OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                       onselectedindexchanged="PrintMenuRadComboBox_SelectedIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="Badge Sheet (Avery 5392)" Value="PrintBadgeSheet" />
                                        <telerik:RadComboBoxItem Text="Registrations" Value="PrintRegistrations" />
                                        <telerik:RadComboBoxItem Text="Contact Sheet" Value="PrintContactSheet" />
                                    </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem> 
                 
                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table>
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="FlagMenuLabel" runat="server" Text="Assign Flag:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <telerik:RadComboBox ID="FlagMenuDropDownList" runat="server" Font-Size="Small"  Style="z-index: 10000"
                                DataSourceID="EventFlagsDataSource" DataTextField="FlagName" Width="150" DropDownWidth="200" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                onselectedindexchanged="FlagMenuDropDownList_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem>  

                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table >
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="CeusMenuLabel" runat="server" Text="Grant CEUs for:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <telerik:RadComboBox ID="SessionsDropDownList" runat="server" Font-Size="Small"  Style="z-index: 10000"
                                DataSourceID="SessionsDataSource" DataTextField="SessionName" Width="150" DropDownWidth="300" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                onselectedindexchanged="SessionsMenuDropDownList_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem> 

                <telerik:RadMenuItem IsSeparator="true"></telerik:RadMenuItem>
                <telerik:RadMenuItem Width="300px" Text="Cancel Registration" Value="CancelReg" PostBack="true" />

                </Items>
            </telerik:RadMenuItem>
        </Items>
    </telerik:RadMenu>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" CssClass="errorMessage" />
<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="ValidationGroup1" Display="None" CssClass="errorMessage"></asp:CustomValidator>

<telerik:RadSplitter runat="server" ID="EventRadSplitter" Width="100%" Height="100%" BorderStyle="Solid" BorderColor="Red" BorderSize="1" Orientation="Horizontal" EnableViewState="true">
    <telerik:RadPane ID="NewRegRadPane" runat="server" Scrolling="Both" Height="120">
     
        <asp:Panel runat="server" ID="NewRegPanel">
        <table id="NewRegTable" class="formview-table-layout">
            <tr>
                <td colspan="3">
                <br />
                    <asp:ValidationSummary ID="NewRegValidationSummary" runat="server" ValidationGroup="NewRegValidationGroup" ForeColor="Red" CssClass="errorMessage" Style="margin-top:3px;" />
                    <asp:CustomValidator ID="NewRegCustomValidator" runat="server" ErrorMessage="CustomValidator" ValidationGroup="NewRegValidationGroup" Display="None" CssClass="errorMessage"></asp:CustomValidator>
                    <asp:CustomValidator ID="NewRegSuccessValidator" runat="server" ErrorMessage="" Display="Dynamic" CssClass="successMessage" ForeColor="Green"></asp:CustomValidator>
                </td>
            </tr>
                <tr>
                    <td>Attendee Name:</td>
                    <td>Facility:</td>
                    <td>Title:</td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox
                            ID="NewAttendeeRadComboBox" runat="server"
                            AllowCustomText="true" MarkFirstMatch="False"  
                            DataTextField="FullNameWithId" AutoPostBack="true" IsCaseSensitive="false" 
                            DataValueField="Id" EmptyMessage="Attendee name (last, first)..." 
                            Width="200px" MaxHeight="200px" EnableLoadOnDemand="true" 
                            onselectedindexchanged="NewAttendeeRadComboBox_SelectedIndexChanged" 
                            onitemsrequested="NewAttendeeRadComboBox_ItemsRequested">
                        </telerik:RadComboBox>
               
                    </td>
                    <td>
                        <telerik:RadComboBox
                            ID="NewRegFacilityRadComboBox" runat="server"
                            Width="300px" 
                            EmptyMessage="Facility..."
                            MarkFirstMatch="false"
                            DataTextField="OrgName" DataValueField="Id"
                            AllowCustomText="false" EnableLoadOnDemand="true"
                            AutoPostBack="false" IsCaseSensitive="false"
                            onitemsrequested="NewRegFacilityRadComboBox_ItemsRequested" >
                        </telerik:RadComboBox></td>
                    <td>
                        <telerik:RadTextBox ID="NewRegTitleTextBox" runat="server" Text="" Width="150" />
                    </td>
                    <td>
                        <telerik:RadButton ID="NewRegAddButton" runat="server" Text="Save" onclick="NewRegAddButton_Click" />&nbsp;
                        <telerik:RadButton ID="NewRegDoneRadButton" runat="server" Text="Close" OnClientClicked="CollapseNewRegPane" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;">
                        <asp:LinkButton ID="NewAttendeeLinkButton" runat="server" Text="Create new attendee" />
                    </td>
                    <td style="vertical-align:top;">
                        <asp:LinkButton ID="NewOrgLinkButton" runat="server" Text="Create new facility" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            </asp:Panel>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </telerik:RadPane>

    <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>

    <telerik:RadPane ID="RegGridRadPane" runat="server" OnClientResized="RegGridPaneResized" EnableViewState="true" Height="100%">

        <telerik:RadListView runat="server" ID="GridListView" AllowPaging="True" AllowMultiItemSelection="true" 
            DataKeyNames="Id" onneeddatasource="GridListView_NeedDataSource">
                    <ItemTemplate>
                        <tr class="rlvI">
                            <td>&nbsp;</td>
                            <td><asp:Label ID="IdLabel" runat="server" Text='<%# Eval("AttendeeId") %>'></asp:Label></td>
                            <td><asp:Label ID="AttendeeNameLabel" runat="server" Text='<%# Eval("AttendeeFullName") %>'></asp:Label></td>
                            <td><asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("OrgName") %>'></asp:Label></td>
                            <td><asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>'></asp:Label></td>
                            <td><asp:Label ID="Label2" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSpeaker")) == true ? "Speaker" : "" %>'></asp:Label></td>
                            <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("Notes") %>'></asp:Label></td>
                            <td><asp:Label ID="Label4" runat="server" Text='<%# Eval("FlagList") %>'></asp:Label></td>
                            <td><asp:Label ID="Label5" runat="server" Text='<%# Eval("EntryDateTime") %>'></asp:Label></td>
                            <td><asp:Label ID="Label6" runat="server" Text='<%# Eval("CeuTotal") %>'></asp:Label></td>
                            <td><asp:HyperLink ID="PrintCertHyperlink" runat="server" Text="Print Cert" CommandName="PrintCertificate" Target="_blank" /></td>
                            <td>
                                <telerik:RadButton ID="DeleteRadButton" runat="server" Text="Delete">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <div class="RadListView RadListView_<%# Container.Skin %>">
                            <div class="rlvEmpty">
                                There are no registered attendees to be displayed.</div>
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="RadListView RadListView_<%# Container.Skin %>">
                            <table cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr class="rlvHeader">
                                        <th>Edit</th>
                                        <th>Id</th>
                                        <th>Attendee</th>
                                        <th>Facility</th>
                                        <th>Title</th>
                                        <th>Speaker</th>
                                        <th>Notes</th>
                                        <th>Flags</th>
                                        <th>Created</th>
                                        <th>CEUs</th>
                                        <th>Print Cert</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <telerik:RadDataPager ID="RadDataPager1" runat="server">
                                                <Fields>
                                                    <telerik:RadDataPagerButtonField FieldType="Numeric"></telerik:RadDataPagerButtonField>
                                                </Fields>
                                            </telerik:RadDataPager>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </LayoutTemplate>
                </telerik:RadListView>

    </telerik:RadPane>

</telerik:RadSplitter>

<telerik:RadCodeBlock runat="server" ID="RadCodeBlock1">

    <script type="text/javascript">
    //<![CDATA[
    
        function CollapseNewRegPane() {
            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
            var isCollapseSuccess = pane.collapse();

        }

        function RegGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var regGrid = $find("<%= GridListView.ClientID %>");
            regGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            regGrid.repaint();
        }
        function pageLoad() {

        }

        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("Insert") >= 0 || args.get_eventTarget().indexOf("Update") >= 0 || args.get_eventTarget().indexOf("Cancel") >= 0) {
                args.set_enableAjax(false);
            }
        }

        var regAttendeeId;
        var rowIndex;
        var attendeeComboClientId;

        function ShowPrintBadgeForm() {

            

        }

        function PrintBadgesWindowClosed() {

            var masterTable = $find("<%= GridListView.ClientID %>").get_masterTableView();
            var row = masterTable.get_dataItems();

            for (var i = 0; i < row.length; i++) {
                masterTable.get_dataItems()[i].set_selected(false);
            }
        }

        function ShowCertificateReport(eventId, attendeeId, reportName) {

            var wnd = window.radopen("Reports2/ReportViewer.aspx?FileName=" + reportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId, "CertificateReportDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function ShowBadgeReport() {

            var wnd = window.radopen("Reporting/TelerikReportViewer.aspx?method=push", "BadgeReportDialog");
            wnd.setSize(850, 550);
            return false;
        }


        function ShowAttendanceDialog(eventId, registrationAttendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("EditAttendanceDialog.aspx?EventId=" + eventId + "&RegAttendeeId=" + registrationAttendeeId, "EditAttendanceDialog");
            return false;
        }

        function ShowAttendeeDialog(attendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function ShowOrgDialog(orgId) {

            //attendeeComboClientId =
            var wnd = window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "OrgDetailsDialog");
            wnd.setSize(850, 500);
            return false;
        }

        function RefreshRegistrationsGrid(ceuTotal) {

            
        }

        function ReportViewerErrorOccurred(errorMsg) {

            alert(errorMsg);
        }

        //function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {
        function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

            
        }

        function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

            if (attendeeId > 0) {

                // Insert new attendee into newAttendeeRadComboBox
                var combo = $find("<%= NewAttendeeRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(fullNameWithId);
                comboItem.set_value(attendeeId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();

                // Insert the facility into NewRegFacilityRadComboBox
                if (orgId != "0") {
                    var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(orgName);
                    comboItem.set_value(orgId);
                    combo.trackChanges();
                    combo.get_items().add(comboItem);
                    comboItem.select();
                    combo.commitChanges();
                    comboItem.scrollIntoView();
                }

                if (title) {
                    // Insert the title into
                    var textbox = $find("<%= NewRegTitleTextBox.ClientID %>");
                    textbox.set_text = title;
                }
            }
        }

        function OrgAdded(orgId, orgName) {

            if (orgId > 0) {

                // Insert new org into newFacilityRadComboBox
                var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(orgName);
                comboItem.set_value(orgId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();
            }
        }

        function OrgUpdated(orgId, orgName) {


        }

        function ShowElement(elementID) {
            document.all(elementID).style.display = 'block';
            // Hide the 'new' button
            // Set focus to Flag Name textbox
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").focus();

        }

        function ctl65_Clicked(sender, args) {
            //Add JavaScript handler code here
            CollapseNewRegPane();
        }

        function ExpandNewRegPane() {

        }

        function HideElement(elementID) {
            document.all(elementID).style.display = 'none';
            // Show the 'new' button
            // Clear all new flag fields
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegFacilityRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegTitleTextBox.ClientID %>").value = "";

        }

        function PrintMenuRadComboBox_SelectedIndexChanged(sender, args) {

            var menu = $find("<%=ActionsRadMenu.ClientID %>");
            var menuItem = menu.findItemByText("Print");
            var printCombo = menuItem.findControl("PrintMenuRadComboBox");
            var value = printCombo.get_selectedItem().get_value();
            var item = printCombo.findItemByValue("");
            //item.select();
            if (value == "PrintSingleBadge")
                ShowPrintBadgeForm();
            else if (value == "PrintBadgeSheet" || value == "PrintRegistrations")
                __doPostBack('ActionsRadMenu', value);

        }


        function ActionsRadMenu_ItemClicking(sender, args) {
            if (args.get_item().get_text() == "Print Single Badge") {
                ShowPrintBadgeForm();
            }
            else if (args.get_item().get_text() == "New") {
                var splitter = $find("<%= EventRadSplitter.ClientID %>");
                var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
                var isExpandSuccess = pane.expand(pane); // expand this pane
            }
            else if (args.get_item().get_text() != "Clear Filters" && args.get_item().get_text() != "Cancel Registration") {
                args.set_cancel(true);
            }
        }

        function GetSelectedItems() {
           
        }

        var flag = false;

        function ActionsRadMenu_ItemClosing(sender, args) {
            args.set_cancel(flag);
        }

        function MenuRadComboBox_DropDownOpening(sender) {
            flag = true;
        }

        function MenuRadComboBox_DropDownClosed(sender) {
            setTimeout(function () { flag = false; }, 200);

        }

      //]]>
    </script>
    </telerik:RadCodeBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" 
        EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="BadgePrintDialog" runat="server" Title="Print Single Badge" Height="250px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="EditAttendanceDialog" runat="server" Title="Edit Attendance" Height="500px"
                Width="900px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="OrgDetailsDialog" runat="server" 
                Title="Edit Facility" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="CertificateReportDialog" runat="server" 
                Title="Certificate" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
             <telerik:RadWindow ID="BadgeReportDialog" runat="server" 
                Title="Attendee Badges" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="EventFlagsDataSource" runat="server"
            SelectMethod="GetFlagsByEvent" 
            TypeName="EventManager.Business.EventFlagMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
     </asp:ObjectDataSource>

     <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
