﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanAttendeesOffline.aspx.cs" Inherits="EventManager.Web.Association.ScanAttendeesOffline" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body {font-family:Arial; font-size:12px;}
        .assnName
        {
            color: #0055A4;
            font-family: arial;
            font-size: 16px;
            font-weight:bold;
        }
        .bigName 
        {
            font-size:64px;
            font-family:Arial;
            color:Black;
            margin:0 auto;
        }
        .time {font-size:20px; color: #0055A4;}
        .date {font-size:20px; color: #0055A4;}
        .location {font-size:20px; color: #0055A4;}
        .sessionName {font-size:24px; color:#0055A4; font-weight:bold;}
        .label { font-style: Italic;}
        .PleaseScan {color:Red; font-size:28px;}
        .ScanInstructions {font-style:italic; color:Red; font-size:14px;}
        
        #SessionsListContainer {margin: 0 auto; text-align:center;}
        #AttendeeNameContainer {margin: 0 auto; text-align:center;}
        
        #AttendeeIdTextBox {}
        table {margin: 0 auto;}
        table td {text-align:left;}
        table .header {padding:10px;}
    </style>

</head>
<body>
    
    <script type="text/javascript" language="javascript" src="../js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var attendeeNameLabel = document.all("<%=AttendeeNameLabel.ClientID %>");
            if (attendeeNameLabel != null) { 
                if (attendeeNameLabel.value != "") {
                    //$(".bigName").show();
                    $(".bigName").delay(1000);
                    $(".bigName").fadeOut(1000);
                }
            }
            

        });

        function keyPress(sender) {

            //alert(attendeeTextBox.value);
            var attendeeTextBox = document.all("<%=AttendeeIdTextBox2.ClientID %>");
            if (window.event.keyCode == 13) {
                if (attendeeTextBox.value != "") { 
                    attendeeTextBox.disabled = true;
                    __doPostBack('AttendeeIdTextBox2', attendeeTextBox.value);
                }
                
            }
        }

    </script>

    <form id="form1" runat="server">

    <telerik:radscriptmanager runat="server"></telerik:radscriptmanager>

    <div style="width:98%; margin:0 auto;">
    
        <table class="header">
            <tr>
                <td>
                    <asp:Image ID="AssociationLogoImage" runat="server" /></td>
            </tr>
        </table>
        <br />
        
        <div id="SessionsDetailsContainer" >
            
            <asp:FormView ID="SessionFormView" runat="server" 
                DataSourceID="SessionDataSource" Width="626px">
                
                <ItemTemplate>

                    <table>
                        <tr>
                            <td style="text-align:center;"><asp:Label ID="StartTimeLabel" runat="server" Text='<%# Bind("StartDateTime", "{0:dddd}") %>' CssClass="date" />&nbsp;
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("StartDateTime", "{0:t}") %>' CssClass="time" />&nbsp;-&nbsp;
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("EndDateTime", "{0:t}") %>' CssClass="time" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <asp:Label ID="SessionNameLabel" runat="server" Text='<%# Eval("SessionName") %>' CssClass="sessionName" /></td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td class="label"></td>
                            <td><asp:Label ID="LocationLabel" runat="server" Text='<%# Bind("Location") %>' /></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:FormView>

        </div>

        <br />

        <asp:Panel ID="AttendeeNameContainer" runat="server">
        
            <br />
             <asp:Label ID="PleaseScanLabel" runat="server" Text="Please scan your badge" CssClass="PleaseScan" /><br />
            <asp:Label ID="ScanInstructionsLabel" runat="server" Text='Always remember to scan IN and OUT to get credit for session attendance' CssClass="ScanInstructions" /><br /><br />

            <asp:Label ID="AttendeeNameLabel" runat="server" Text="" CssClass="bigName" />
            <br />
            <br />
            <%--<telerik:RadTextBox ID="AttendeeIdTextBox" runat="server"
                Wrap="False" Font-Size="24pt" Height="50px"
                ontextchanged="AttendeeIdTextBox_TextChanged" BackColor="#FFFFCC" 
                Width="200px" BorderStyle="None"  >
            </telerik:RadTextBox>--%>

             <asp:TextBox ID="AttendeeIdTextBox2" runat="server" Wrap="false" Height="50" ontextchanged="AttendeeIdTextBox_TextChanged" />
                
             <telerik:RadInputManager ID="RadInputManager1" runat="server">
                <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="" Type="Number" DecimalDigits="0" ClientEvents-OnKeyPress="keyPress" >
                    <TargetControls>
                        <telerik:TargetInput ControlID="AttendeeIdTextBox2" />
                    </TargetControls>
                </telerik:NumericTextBoxSetting>
            </telerik:RadInputManager>
<%--

               <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" />
               <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" Display="None" ValidationGroup="ValidationGroup1"></asp:CustomValidator>
               <asp:CompareValidator id="valRequired" runat="server" ControlToValidate="AttendeeIdTextBox" ValidationGroup="ValidationGroup1" Display="Dynamic"
                    ValueToCompare="0"
                    Type="Integer"
                    Operator="GreaterThan"
                    ErrorMessage="* You must enter an ID greater than 0">*
                </asp:CompareValidator>--%>

            <br />

        </asp:Panel>

        <br /><br /><br /><br /><br /><br /><br /><br />
        <div id="SessionsListContainer" style="background-color:white;">
        
            <asp:Label ID="ChangeSessionLabel" runat="server" Text="Change Session:" /><br />
              <telerik:radcombobox runat="server" ID="SessionsRadComboBox" Width="400" AutoPostBack="true" 
                DataSourceID="SessionsDataSource" DropDownWidth="600" AppendDataBoundItems="true" DataTextField="StartTimeAndName" DataValueField="Id"  
                onselectedindexchanged="SessionsRadComboBox_SelectedIndexChanged">
                <Items>
                <telerik:RadComboBoxItem Text="" Value="" />
                </Items>
            </telerik:radcombobox>

        </div>

        <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
            <SelectParameters>
                <asp:QueryStringParameter Name="EventId" QueryStringField="EventId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

         <asp:ObjectDataSource ID="SessionDataSource" runat="server" 
            SelectMethod="GetSession" TypeName="EventManager.Business.EventSessionMethods" 
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="SessionsRadComboBox" Name="id" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>


    </div>
    </form>
</body>
</html>
