﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgStaffDialog.aspx.cs" Inherits="EventManager.Web.Association.OrgStaffDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Facility</title>

    <script type="text/javascript">
    //<![CDATA[

        function StaffUpdated(organizationId, staffCount) {
            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.StaffUpdated(organizationId, staffCount);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CloseWindow() {
            GetRadWindow().close();
        }
    //]]>
    </script>
    <style type="text/css">
    .label {white-space:nowrap;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%; border:1 solid black;">

        <telerik:radscriptmanager runat="server" />
        <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server"  />

        <asp:ValidationSummary ID="OrgValidationSummary" runat="server" ValidationGroup="EditOrg" ForeColor="Red" />
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" Display="None" ValidationGroup="EditOrg"></asp:CustomValidator>
        
        <table style="width:100%;">
        <tr>
            <td>
                <h2 style="margin:0px; padding:0px;"><asp:Label ID="OrgNameLabel" runat="server"  /></h2>
                <h3 style="margin-top:3px;"><asp:Label ID="Label1" runat="server" Text="Staff Members: " />
                    <asp:Label ID="StaffCountLabel" runat="server" Text="0" ForeColor="Blue" /></h3>
            </td>

            <td style="text-align:right;"> 
                <telerik:RadButton ID="SaveCloseRadButton" runat="server" Text="Save and Close" onclick="SaveCloseRadButton_Click"></telerik:RadButton>
                <br /><br />
                Administrator:&nbsp;
                <telerik:RadComboBox
                    ID="AdminRadComboBox" runat="server" DataSourceID="AttendeesDataSource"
                    DataTextField="FullName" DataValueField="Id" EnableViewState="true" AppendDataBoundItems="true"
                    DropDownWidth="150" ondatabound="AdminRadComboBox_DataBound">
                    <Items>
                        <telerik:RadComboBoxItem Text="" Value="System.DBNull.Value" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td>
               
            </td>
        </tr>
        </table>
        
        

        <telerik:RadGrid ID="AttendeesRadGrid" runat="server"  
            AutoGenerateColumns="False" CellSpacing="0" DataSourceID="AttendeesDataSource" 
            OnDeleteCommand="AttendeesRadGrid_DeleteCommand" Height="200"
            GridLines="None" AllowPaging="false"
            PagerStyle-Position="Bottom" ondatabound="AttendeesRadGrid_DataBound">
            <ClientSettings>
                <Scrolling AllowScroll="true" />
            </ClientSettings>
        <MasterTableView DataSourceID="AttendeesDataSource" DataKeyNames="Id" >
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" 
                    FilterControlAltText="Filter Id column" HeaderText="Id" SortExpression="Id" 
                    UniqueName="Id">
                </telerik:GridBoundColumn>
                <%--<telerik:GridHyperLinkColumn FilterControlAltText="Filter attendee name" HeaderStyle-Width="150px" HeaderText="Attendee"
                                FilterControlWidth="120px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                                UniqueName="AttendeeNameHyperlink"
                                DataType="System.String"
                                DataTextField="FullName"
                                DataNavigateUrlFields="Id"
                                DataNavigateUrlFormatString="AttendeeDetails.aspx?AttendeeId={0}">
                            </telerik:GridHyperLinkColumn>--%>
                 <telerik:GridBoundColumn DataField="FullName" 
                    FilterControlAltText="Filter FullName column" HeaderText="Name" 
                    SortExpression="FullName" UniqueName="FullName">
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="Title" 
                    FilterControlAltText="Filter Title column" HeaderText="Title" 
                    SortExpression="Title" UniqueName="Title">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Email" 
                    FilterControlAltText="Filter Email column" HeaderText="Email" 
                    SortExpression="Email" UniqueName="Email">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PhoneNumber" 
                    FilterControlAltText="Filter PhoneNumber column" HeaderText="Phone" 
                    SortExpression="PhoneNumber" UniqueName="PhoneNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UserId" DataType="System.Guid" 
                    FilterControlAltText="Filter UserId column" HeaderText="User Id" 
                    SortExpression="UserId" UniqueName="UserId">
                </telerik:GridBoundColumn>
                 <telerik:GridButtonColumn ConfirmText="Remove this staff member?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Remove" ButtonType="ImageButton" CommandName="Delete" ConfirmDialogHeight="100px"
                            ConfirmDialogWidth="220px" />

            </Columns>

            <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
            </MasterTableView>

            <FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>

             <br />
            <telerik:RadComboBox
                 ID="NewAttendeeRadComboBox" runat="server"
                 Width="300px" Height="140px"
                 EmptyMessage="Type attendee name..."
                 MarkFirstMatch="true"
                 AllowCustomText="true"
                 EnableLoadOnDemand="true"
                AutoPostBack="true"
                onselectedindexchanged="NewAttendeeRadComboBox_SelectedIndexChanged" onitemsrequested="NewAttendeeRadComboBox_ItemsRequested">
             </telerik:RadComboBox>

             <br /><br />

    <asp:ObjectDataSource ID="AttendeesDataSource" runat="server" 
        SelectMethod="GetAttendeesByOrganization" 
        TypeName="EventManager.Business.AttendeeMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="orgId" QueryStringField="OrgId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    </div>
    </form>
</body>
</html>
