﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventRegAttendees2.aspx.cs" Inherits="EventManager.Web.Association.EventRegAttendeesForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <style type="text/css">
    #<%= RegRadGridPanelClientID %> { height:100%; padding:0px; margin:0px;}
    </style>
</telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1" OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
     <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RegistrationsRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
                    <telerik:AjaxUpdatedControl ControlID="ActionsRadMenu" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewRegDoneButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="NewRegRadPane" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewRegAddButton">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
                    <telerik:AjaxUpdatedControl ControlID="NewRegPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewAttendeeRadComboBox">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="NewAttendeeRadComboBox" />
                    <telerik:AjaxUpdatedControl ControlID="NewRegPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ActionsRadMenu">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ActionsRadMenu" />
                    <telerik:AjaxUpdatedControl ControlID="RegistrationsRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    
    <telerik:RadMenu runat="server" ID="ActionsRadMenu" Skin="Default" 
         OnClientItemClicking="ActionsRadMenu_ItemClicking" Width="100%" 
         OnItemClick="ActionsRadMenu_ItemClick" OnClientItemClosing="ActionsRadMenu_ItemClosing">
        <Items>
            <telerik:RadMenuItem runat="server" Text="" PostBack="false" ForeColor="Red" />
            <telerik:RadMenuItem runat="server" Text="Clear Filters" Width="120" ImageUrl="~/Images/filterCancel.gif" PostBack="true" >
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="New" PostBack="false" >
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Selected Attendees..." Width="300px" PostBack="False" >
                <Items>
        
                <telerik:RadMenuItem Width="300px" Text="Print Single Badge" PostBack="false" />
                <telerik:RadMenuItem IsSeparator="true"></telerik:RadMenuItem>

                <telerik:RadMenuItem Width="300px" Text="Print">
                    <ItemTemplate>
                        <table>
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="PrintMenuLabel" runat="server" Text="Print:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <%--<asp:DropDownList ID="PrintMenuDropDownList" runat="server" Font-Size="Small" 
                                    Width="150" DropDownWidth="200" 
                                    AutoPostBack="true" onselectedindexchanged="PrintMenuDropDownList_SelectedIndexChanged">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Badge Sheet (Avery 5392)" Value="PrintBadgeSheet"></asp:ListItem>
                                    <asp:ListItem Text="Single Badge (DYMO)" Value="PrintSingleBadge"></asp:ListItem>
                                    <asp:ListItem Text="Registrations" Value="PrintRegistrations"></asp:ListItem>
                                </asp:DropDownList>--%>
                                <telerik:RadComboBox ID="PrintMenuRadComboBox" Width="150" AutoPostBack="true" 
                                       Font-Size="Small" Style="z-index: 10000"
                                       runat="server" 
                                       DropDownWidth="200" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                       OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                       onselectedindexchanged="PrintMenuRadComboBox_SelectedIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="Attendee Badges (Sheet of 6)" Value="PrintBadgeSheet" />
                                        <telerik:RadComboBoxItem Text="Exhibitor Badges (Sheet of 6)" Value="PrintExhibitorBadgeSheet" />
                                        <telerik:RadComboBoxItem Text="Registrations" Value="PrintRegistrations" />
                                        <telerik:RadComboBoxItem Text="Contact Sheet" Value="PrintContactSheet" />
                                    </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem> 
                 
                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table>
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="FlagMenuLabel" runat="server" Text="Assign Flag:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <telerik:RadComboBox ID="FlagMenuDropDownList" runat="server" Font-Size="Small"  Style="z-index: 10000"
                                DataSourceID="EventFlagsDataSource" DataTextField="FlagName" Width="150" DropDownWidth="200" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                onselectedindexchanged="FlagMenuDropDownList_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem>  

                <telerik:RadMenuItem Width="300px">
                    <ItemTemplate>
                        <table >
                           <tr><td style="white-space:nowrap;padding:0px;">
                            <asp:Label ID="CeusMenuLabel" runat="server" Text="Grant CEUs for:" Width="100"></asp:Label>
                           </td><td style="white-space:nowrap;padding:0px;">
                                <telerik:RadComboBox ID="SessionsDropDownList" runat="server" Font-Size="Small"  Style="z-index: 10000"
                                DataSourceID="SessionsDataSource" DataTextField="SessionName" Width="150" DropDownWidth="300" 
                                DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="true" OnClientDropDownOpening="MenuRadComboBox_DropDownOpening" 
                                OnClientDropDownClosed="MenuRadComboBox_DropDownClosed" 
                                onselectedindexchanged="SessionsMenuDropDownList_SelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="" Value="" />
                                </Items>
                                </telerik:RadComboBox>
                           </td></tr>
                        </table>
                    </ItemTemplate>
                </telerik:RadMenuItem> 

                <telerik:RadMenuItem IsSeparator="true"></telerik:RadMenuItem>
                <telerik:RadMenuItem Width="300px" Text="Cancel Registration" Value="CancelReg" PostBack="true" />

                </Items>
            </telerik:RadMenuItem>

            <telerik:RadMenuItem runat="server" Text="Mode:" PostBack="false" Enabled="false" >
            </telerik:RadMenuItem>

            <telerik:RadMenuItem runat="server" Text="Normal" PostBack="False" >
                <Items>
                <telerik:RadMenuItem runat="server" Text="" >
                    <ItemTemplate>
                    <asp:RadioButtonList ID="ModeRadioButtonList" runat="server" AutoPostBack="true" Width="80" 
                           onselectedindexchanged="ModeRadioButtonList_SelectedIndexChanged">
                        <asp:ListItem Text="Normal" Value="Normal" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Onsite" Value="Onsite"></asp:ListItem>
                    </asp:RadioButtonList>
                    </ItemTemplate>
                </telerik:RadMenuItem>
                </Items>
            </telerik:RadMenuItem>
        </Items>
    </telerik:RadMenu>

  
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidationGroup1" ForeColor="Red" CssClass="errorMessage" />
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="ValidationGroup1" Display="None" CssClass="errorMessage"></asp:CustomValidator>

    <telerik:RadSplitter runat="server" ID="EventRadSplitter" Width="100%" Height="100%" BorderStyle="Solid" BorderColor="Red" BorderSize="1" Orientation="Horizontal" EnableViewState="true">
    <telerik:RadPane ID="NewRegRadPane" runat="server" Scrolling="Both" Height="120">
        <%--<asp:UpdatePanel ID="NewRegUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="ActionsRadMenu" />
        </Triggers>
        <ContentTemplate>--%>
        <asp:Panel runat="server" ID="NewRegPanel" Style="padding:10px;">
        <table id="NewRegTable" class="formview-table-layout">
            <tr>
                <td colspan="3">
                    <asp:ValidationSummary ID="NewRegValidationSummary" runat="server" ValidationGroup="NewRegValidationGroup" ForeColor="Red" CssClass="errorMessage" Style="margin-top:3px;" />
                    <asp:CustomValidator ID="NewRegCustomValidator" runat="server" ErrorMessage="CustomValidator" ValidationGroup="NewRegValidationGroup" Display="None" CssClass="errorMessage"></asp:CustomValidator>
                    <asp:CustomValidator ID="NewRegSuccessValidator" runat="server" ErrorMessage="" Display="Dynamic" CssClass="successMessage" ForeColor="Green"></asp:CustomValidator>
                </td>
            </tr>
                <tr>
                    <td>Attendee Name:</td>
                    <td>Facility:</td>
                    <td>Title:</td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadComboBox
                            ID="NewAttendeeRadComboBox" runat="server"
                            AllowCustomText="true" MarkFirstMatch="False"  
                            DataTextField="FullNameWithId" AutoPostBack="true" IsCaseSensitive="false" 
                            DataValueField="Id" EmptyMessage="Attendee name (last, first)..." 
                            Width="200px" MaxHeight="200px" EnableLoadOnDemand="true" 
                            onselectedindexchanged="NewAttendeeRadComboBox_SelectedIndexChanged" 
                            onitemsrequested="NewAttendeeRadComboBox_ItemsRequested">
                        </telerik:RadComboBox>
               
                    </td>
                    <td>
                        <telerik:RadComboBox
                            ID="NewRegFacilityRadComboBox" runat="server"
                            Width="300px" 
                            EmptyMessage="Facility..."
                            MarkFirstMatch="false"
                            DataTextField="OrgName" DataValueField="Id"
                            AllowCustomText="false" EnableLoadOnDemand="true"
                            AutoPostBack="false" IsCaseSensitive="false"
                            onitemsrequested="NewRegFacilityRadComboBox_ItemsRequested" >
                        </telerik:RadComboBox></td>
                    <td>
                        <telerik:RadTextBox ID="NewRegTitleTextBox" runat="server" Text="" Width="150" />
                    </td>
                    <td>
                        <telerik:RadButton ID="NewRegAddButton" runat="server" Text="Save" onclick="NewRegAddButton_Click" />&nbsp;
                        <telerik:RadButton ID="NewRegDoneRadButton" runat="server" Text="Close" OnClientClicked="CollapseNewRegPane" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;">
                        <asp:LinkButton ID="NewAttendeeLinkButton" runat="server" Text="Create new attendee" />
                    </td>
                    <td style="vertical-align:top;">
                        <asp:LinkButton ID="NewOrgLinkButton" runat="server" Text="Create new facility" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            </asp:Panel>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </telerik:RadPane>

    <telerik:RadSplitBar runat="server"></telerik:RadSplitBar>

    <telerik:RadPane ID="RegGridRadPane" runat="server" OnClientResized="RegGridPaneResized" EnableViewState="true" Height="100%">
    <%--<asp:UpdatePanel ID="RegGridUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" Style="Height:100%;">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="NewRegAddButton" EventName="Click" />
    </Triggers>
    <ContentTemplate>--%>

    <telerik:radgrid id="RegistrationsRadGrid" runat="server" Height="98%"
        AutoGenerateColumns="False" EnableLinqExpressions="false" EnableViewState="true"
        AllowFilteringByColumn="True" AllowMultiRowSelection="True"
        AllowAutomaticUpdates="True" AllowPaging="True" AllowSorting="true"
        PageSize="50" PagerStyle-Position="TopAndBottom" PagerStyle-AlwaysVisible="true"
        oninit="RegistrationsRadGrid_Init"
        onitemdatabound="RegistrationsRadGrid_ItemDataBound"
        onitemcreated="RegistrationsRadGrid_ItemCreated"
        onpagesizechanged="RegistrationsRadGrid_PageSizeChanged" 
        onitemevent="RegistrationsRadGrid_ItemEvent" 
        onitemcommand="RegistrationsRadGrid_ItemCommand" 
        onneeddatasource="RegistrationsRadGrid_NeedDataSource" 
        ondeletecommand="RegistrationsRadGrid_DeleteCommand" 
        onupdatecommand="RegistrationsRadGrid_UpdateCommand">

        <groupingsettings casesensitive="False"></groupingsettings>
        <ClientSettings>
            <scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="4" />
            <selecting AllowRowSelect="True" />
        </ClientSettings>
     
        <MasterTableView DataKeyNames="Id, AttendeeId" EnableViewState="true" ClientDataKeyNames="Id" PagerStyle-Position="TopAndBottom"  CommandItemDisplay="None">
     
            <CommandItemSettings ExportToPdfText="Export to PDF" />
     
        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>
                <telerik:GridClientSelectColumn UniqueName="CheckboxSelectColumn"  
                    FooterText="CheckBoxSelect footer" ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="#CCCCCC"  
                    HeaderStyle-Width="40" >
                    
                    <HeaderStyle Width="40px" />
                    <ItemStyle BackColor="#CCCCCC" HorizontalAlign="Center" Width="30px" />
                    
                </telerik:GridClientSelectColumn>

                 <telerik:GridEditCommandColumn FooterText="EditCommand footer" UniqueName="EditCommandColumn"
                        ItemStyle-Width="30px" HeaderText="" HeaderStyle-Width="30px" UpdateText="Update" ButtonType="ImageButton">
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <ItemStyle Width="30px" HorizontalAlign="Center" 
                            BackColor="#CCCCCC"></ItemStyle>
                    </telerik:GridEditCommandColumn>

                <telerik:GridBoundColumn DataField="AttendeeId" DataType="System.Int32" UniqueName="AttendeeId" ItemStyle-Width="80" HeaderText="Id" HeaderStyle-Width="80"
                    AllowSorting="true" SortExpression="AttendeeId" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                    AllowFiltering="true" FilterControlWidth="50" FilterControlAltText="Filter Attendee ID" CurrentFilterFunction="EqualTo">
                    <HeaderStyle Width="80px"></HeaderStyle>
                    <ItemStyle Width="70px" BackColor="#CCCCCC"></ItemStyle>
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="AttendeeNameTemplateColumn" DataField="AttendeeFullName" HeaderText="Attendee" HeaderStyle-HorizontalAlign="Left" 
                    AllowFiltering="true" FilterControlWidth="80px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true"
                    SortExpression="AttendeeFullName" 
                    HeaderStyle-Width="100px" ItemStyle-Width="130">
                    <ItemTemplate>
                        <asp:LinkButton ID="AttendeeNameLinkButton" runat="server" Text='<%# Eval("AttendeeFullName") %>' CommandName="ViewAttendeeDetails" />
                    </ItemTemplate>
                    <HeaderStyle Width="130px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" BackColor="#CCCCCC" />
                </telerik:GridTemplateColumn>

                <telerik:GridHyperLinkColumn FilterControlAltText="Filter facility name" HeaderStyle-Width="250px" HeaderText="Facility" 
                    FilterControlWidth="200px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true" 
                    UniqueName="OrgNameHyperlink" DataType="System.String" DataNavigateUrlFields="OrgId" DataTextField="OrgName"
                    DataNavigateUrlFormatString="OrgDetails.aspx?OrgId={0}"
                    AllowSorting="true">
                    <HeaderStyle Width="250px"></HeaderStyle>
                </telerik:GridHyperLinkColumn>
                
                <telerik:GridBoundColumn DataField="Title" FilterControlAltText="Filter Title column" HeaderText="Title" ItemStyle-Width="120" HeaderStyle-Width="120px" 
                    AllowSorting="true" UniqueName="TitleGridBoundColumn" FilterControlWidth="75" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                    <HeaderStyle Width="120px"></HeaderStyle>
                    <ItemStyle Width="120px"></ItemStyle>
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="IsSpeakerTemplateColumn" DataField="IsSpeaker" HeaderText="Speaker" 
                    HeaderStyle-Width="60px" ItemStyle-Width="60">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSpeaker")) == true ? "Speaker" : "" %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="60px" />
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridTemplateColumn>

                <telerik:GridBoundColumn DataField="Notes" ItemStyle-Width="150" HeaderStyle-Width="150" FilterControlWidth="100"
                    FilterControlAltText="Filter Notes column" HeaderText="Notes" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" 
                    SortExpression="Notes" UniqueName="Notes">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>

                <telerik:GridTemplateColumn UniqueName="FlagListTemplateColumn" DataField="FlagList" HeaderText="Flags" 
                    SortExpression="FlagList"  HeaderStyle-Width="150px" ItemStyle-Width="150">
                  <ItemTemplate>
                        <asp:LinkButton ID="FlagListLinkButton" runat="server" Text='<%# (Eval("FlagList") == "" ? "[no flags]" : Eval("FlagList")) %>' ForeColor='<%# (Eval("FlagList") == "" ? System.Drawing.Color.Gray : System.Drawing.Color.Blue) %>' CommandName="ViewAttendeeFlags" />
                    </ItemTemplate>
                    <HeaderStyle Width="150px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="FlagsRadComboBox" DataSourceID="EventFlagsDataSource" DataTextField="FlagName"
                            DataValueField="FlagCode" Width="120" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("FlagListTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="FlagsComboBoxIndexChanged" DropDownWidth="200">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" Value="" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                            <script type="text/javascript">
                                function FlagsComboBoxIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("FlagListTemplateColumn", args.get_item().get_value(), "Contains");
                                }
                            </script>
                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

                 <telerik:GridBoundColumn DataField="EntryDateTime" DataType="System.DateTime" 
                    ItemStyle-Width="120" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Right" 
                    FilterControlAltText="Filter EntryDateTime column" FilterControlWidth="80" CurrentFilterFunction="GreaterThan" HeaderText="Created" 
                    SortExpression="EntryDateTime" UniqueName="EntryDateTime" 
                    DataFormatString="{0:g}">
                    <ItemStyle HorizontalAlign="Right" Width="120px"></ItemStyle>
                </telerik:GridBoundColumn>

                <%--<telerik:GridTemplateColumn UniqueName="CeusTemplateColumn" 
                    DataField="CeuTotal" HeaderText="CEUs"
                    SortExpression="CeuTotal"  HeaderStyle-Width="90px" ItemStyle-Width="90" 
                    FilterControlWidth="45px">
                    <ItemTemplate>
                        <asp:Label ID="AttendanceLabel" runat="server" Text='<%# Eval("CeuTotal") %>' Style="margin-top:2px;" />&nbsp;&nbsp;
                        <telerik:RadButton ID="EditAttendanceRadButton" runat="server" Text="Edit" CommandName="EditAttendance" AutoPostBack="false"></telerik:RadButton>
                    </ItemTemplate>
                    <HeaderStyle Width="90px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridTemplateColumn>--%>

                <telerik:GridTemplateColumn UniqueName="CeusTemplateColumn" 
                    DataField="CeuTotal" HeaderText="CEUs"
                    SortExpression="CeuTotal"  HeaderStyle-Width="55px" ItemStyle-Width="55" 
                    FilterControlWidth="25px">
                    <ItemTemplate>
                        <asp:LinkButton ID="EditAttendanceLinkButton" runat="server" Text='<%# Eval("CeuTotal") %>' CommandName="EditAttendance" />
                    </ItemTemplate>
                    <HeaderStyle Width="55px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>

                <telerik:GridTemplateColumn UniqueName="PrintCertificateTemplateColumn" HeaderText="Print Cert" 
                        AllowFiltering="false"
                        HeaderStyle-Width="90px" ItemStyle-Width="90">
                    <ItemTemplate>
                        <%--<asp:LinkButton ID="PrintCertLinkButton" runat="server" Text="Print Cert" CommandName="PrintCertificate" />--%>
                        <asp:HyperLink ID="PrintCertHyperlink" runat="server" Text="Print Cert" CommandName="PrintCertificate" Target="_blank" />
                    </ItemTemplate>
                    <HeaderStyle Width="90px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" />
                </telerik:GridTemplateColumn>

                 <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ItemStyle-Width="30" HeaderStyle-Width="30"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Remove"
                        UniqueName="DeleteRegistrationColumn" 
                    ConfirmTextFields="AttendeeFullName" 
                    ConfirmTextFormatString="Are you sure you want to delete the registration for {0}?">
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                </telerik:GridButtonColumn>
               
            </Columns>

            <EditFormSettings EditFormType="Template" >
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                <FormTemplate>
                    <table id="AttendeeEditTable" style="margin:5px; float:left;">
                        <tr>
                            <td colspan="2"><h3 style="margin-top:0px;"><asp:Label ID="AttendeeNameLabel" runat="server" Text='<%# Eval("AttendeeFullName") %>'  /></h3></td>
                        </tr>
                        <tr>
                            <td>Facility:</td>
                            <td>
                                <telerik:RadComboBox ID="FacilitiesRadComboBox" runat="server" DataTextField="OrgName" DataValueField="Id" Width="300" MarkFirstMatch="true" AllowCustomText="false">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Title:</td><td><telerik:RadTextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' Width="300" MaxLength="50" /></td></tr>
                        <tr>
                            <td>Notes:</td><td><telerik:RadTextBox ID="NotesTextBox" runat="server" Text='<%# Bind("Notes") %>' Width="300" MaxLength="100"  /></td></tr>
                        <tr>
                            <td colspan="2" style="padding-top:3px;">
                                <telerik:RadButton ID="btnUpdate" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />
                                &nbsp;
                                <telerik:RadButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    
                </FormTemplate>
            </EditFormSettings>

            <PagerStyle AlwaysVisible="True" />

        </MasterTableView>
        <HeaderStyle HorizontalAlign="Left" />
        <pagerstyle position="TopAndBottom" ></pagerstyle>
        <filtermenu enableimagesprites="False" ></filtermenu>
    </telerik:radgrid>
  <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
    </telerik:RadPane>
    </telerik:RadSplitter>


    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[

        function CollapseNewRegPane() {
            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
            var isCollapseSuccess = pane.collapse();

        }

        function RegGridPaneResized(sender, eventArgs) {

            var splitter = $find("<%= EventRadSplitter.ClientID %>");
            var regGrid = $find("<%= RegistrationsRadGrid.ClientID %>");
            regGrid.get_element().style.height = (sender.get_height() - 30) + "px";
            regGrid.repaint();
        }

        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("Insert") >= 0 || args.get_eventTarget().indexOf("Update") >= 0 || args.get_eventTarget().indexOf("Cancel") >= 0) {
                args.set_enableAjax(false);
            }
        }

        var regAttendeeId;
        var rowIndex;
        var attendeeComboClientId;

        function ShowPrintBadgeForm() {

            var attendeeIdString = "";
            var dataItem;
            var rowCount = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_selectedItems().length;

            if (rowCount > 0) {

                // Get the selected row's RegAttendeeId
                for (var i = 0; i < rowCount; i++) {
                    dataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_selectedItems()[i];
                    if (i == 0)
                        attendeeIdString = dataItem.getDataKeyValue("Id");
                    else
                        attendeeIdString = attendeeIdString + "," + dataItem.getDataKeyValue("Id");
                    //regAttendeeId = dataItem.getDataKeyValue("Id");
                }

                window.radopen("Reports2/PrintSingleBadge.aspx?RegAttendeeId=" + attendeeIdString, "BadgePrintDialog");
                return false;
            }
            else {
                alert("Please select at least one attendee.");
            }
            
        }

        function PrintBadgesWindowClosed() {

            var masterTable = $find("<%= RegistrationsRadGrid.ClientID %>").get_masterTableView();
            var row = masterTable.get_dataItems();

            for (var i = 0; i < row.length; i++) {
                masterTable.get_dataItems()[i].set_selected(false);
            }
        }

        function ShowCertificateReport(eventId, attendeeId, reportName) {

            // Get the selected row's RegAttendeeId
            //var firstDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[0];

            var wnd = window.radopen("Reports2/ReportViewer.aspx?FileName=" + reportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId, "CertificateReportDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function ShowBadgeReport() {

            var wnd = window.radopen("Reporting/TelerikReportViewer.aspx?method=push", "BadgeReportDialog");
            wnd.setSize(850, 550);
            return false;
        }


        function ShowAttendanceDialog(eventId, registrationAttendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            window.radopen("EditAttendanceDialog.aspx?EventId=" + eventId + "&RegAttendeeId=" + registrationAttendeeId, "EditAttendanceDialog");
            return false;
        }

        function ShowAttendeeDialog(attendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("AttendeeDetailsDialog.aspx?AttendeeId=" + attendeeId, "AttendeeDetailsDialog");
            wnd.setSize(700, 450);
            return false;
        }

        function ShowOrgDialog(orgId) {

            //attendeeComboClientId =
            var wnd = window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "OrgDetailsDialog");
            wnd.setSize(850, 500);
            return false;
        }

        function ShowAttendeeFlagsDialog(registrationAttendeeId, gridRowIndex) {

            rowIndex = gridRowIndex;
            var wnd = window.radopen("EventRegAttFlagsDialog.aspx?id=" + registrationAttendeeId, "AttendeeFlagsDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function RefreshRegistrationsGrid(ceuTotal) {

            // Update the session row with the new speaker string
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];
            editedDataItem.get_cell("CeusTemplateColumn").children[0].innerText = ceuTotal;
        }

        function ReportViewerErrorOccurred(errorMsg) {

            alert(errorMsg);
        }

        function FlagStringUpdated(flagString, regAttendeeId) {

            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];

            // Update the flag cell
            //editedDataItem.get_cell("FlagListTemplateColumn").children[0].innerText = flagString;

            //alert("Reg att ID: " + regAttendeeId);
            $find("<%=RadAjaxManager1.ClientID%>").ajaxRequest(regAttendeeId);
        }

        //function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {
        function AttendeeUpdated(attendeeId, fullName, orgId, orgName, title) {

            // Get the edited row
            var editedDataItem = $find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_dataItems()[rowIndex];

            // Update the cells
            editedDataItem.get_cell("AttendeeNameTemplateColumn").children[0].innerText = fullName;
            //editedDataItem.get_cell("TitleGridBoundColumn").innerText = title;
        }

        function AttendeeAdded(attendeeId, fullNameWithId, orgId, orgName, title) {

            if (attendeeId > 0) {

                // Insert new attendee into newAttendeeRadComboBox
                var combo = $find("<%= NewAttendeeRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(fullNameWithId);
                comboItem.set_value(attendeeId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();

                // Insert the facility into NewRegFacilityRadComboBox
                if (orgId != "0") {
                    var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                    var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                    comboItem.set_text(orgName);
                    comboItem.set_value(orgId);
                    combo.trackChanges();
                    combo.get_items().add(comboItem);
                    comboItem.select();
                    combo.commitChanges();
                    comboItem.scrollIntoView();
                }

                if (title) {
                    // Insert the title into
                    var textbox = $find("<%= NewRegTitleTextBox.ClientID %>");
                    textbox.set_text = title;
                }
            }
        }

        function OrgAdded(orgId, orgName) {
            
            if (orgId > 0) {

                // Insert new org into newFacilityRadComboBox
                var combo = $find("<%= NewRegFacilityRadComboBox.ClientID %>");
                var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                comboItem.set_text(orgName);
                comboItem.set_value(orgId);
                combo.trackChanges();
                combo.get_items().add(comboItem);
                comboItem.select();
                combo.commitChanges();
                comboItem.scrollIntoView();
            }
        }

        function OrgUpdated(orgId, orgName) {
            
            
        }

        function ShowElement(elementID) {
            document.all(elementID).style.display = 'block';
            // Hide the 'new' button
            // Set focus to Flag Name textbox
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").focus();

        }

        function ctl65_Clicked(sender, args) {
            //Add JavaScript handler code here
            CollapseNewRegPane();
        }

        function ExpandNewRegPane() {

        }

        function HideElement(elementID) {
            document.all(elementID).style.display = 'none';
            // Show the 'new' button
            // Clear all new flag fields
            document.all("<%=NewAttendeeRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegFacilityRadComboBox.ClientID %>").value = "";
            document.all("<%=NewRegTitleTextBox.ClientID %>").value = "";

        }

        function PrintMenuRadComboBox_SelectedIndexChanged(sender, args) {

            var menu = $find("<%=ActionsRadMenu.ClientID %>");
            var menuItem = menu.findItemByText("Print");
            var printCombo = menuItem.findControl("PrintMenuRadComboBox");
            var value = printCombo.get_selectedItem().get_value();
            var item = printCombo.findItemByValue("");
            //item.select();
            if (value == "PrintSingleBadge")
                ShowPrintBadgeForm();
            else if (value == "PrintBadgeSheet" || value == "PrintRegistrations")
                __doPostBack('ActionsRadMenu', value);
            
        }


        function ActionsRadMenu_ItemClicking(sender, args) {
            console.log(args.get_item().get_text());
            if (args.get_item().get_text() == "Print Single Badge") {
                ShowPrintBadgeForm();
            }
            else if (args.get_item().get_text() == "New") {
                var splitter = $find("<%= EventRadSplitter.ClientID %>");
                var pane = splitter.getPaneById("<%= NewRegRadPane.ClientID %>");
                var isExpandSuccess = pane.expand(pane); // expand this pane
            }
            else if (args.get_item().get_text() != "Clear Filters" && args.get_item().get_text() != "Cancel Registration" && args.get_item().get_text() != "Mode") {
                args.set_cancel(true);
            }
        }

        function GetSelectedItems() {
            alert($find("<%= RegistrationsRadGrid.MasterTableView.ClientID %>").get_selectedItems().length);
        }

        var flag = false;

        function ActionsRadMenu_ItemClosing(sender, args) {
            args.set_cancel(flag);
        }

        function MenuRadComboBox_DropDownOpening(sender) {
            flag = true;
        }

        function MenuRadComboBox_DropDownClosed(sender) {
            setTimeout(function () { flag = false; }, 200);

        }

      //]]>
    </script>
    </telerik:RadCodeBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" 
        EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="BadgePrintDialog" runat="server" Title="Print Single Badge" Height="250px"
                Width="700px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="EditAttendanceDialog" runat="server" Title="Edit Attendance" Height="500px"
                Width="900px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" />
            <telerik:RadWindow ID="AttendeeDetailsDialog" runat="server" 
                Title="Edit Attendee" Height="450px"
                Width="700px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="OrgDetailsDialog" runat="server" 
                Title="Edit Facility" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="CertificateReportDialog" runat="server" 
                Title="Certificate" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
             <telerik:RadWindow ID="BadgeReportDialog" runat="server" 
                Title="Attendee Badges" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
            <telerik:RadWindow ID="AttendeeFlagsDialog" runat="server" 
                Title="Attendee Flags" Height="500px" VisibleStatusbar="false"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="false"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="EventFlagsDataSource" runat="server"
            SelectMethod="GetFlagsByEvent" 
            TypeName="EventManager.Business.EventFlagMethods">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" 
                Type="Int32" />
        </SelectParameters>
     </asp:ObjectDataSource>

     <asp:ObjectDataSource ID="SessionsDataSource" runat="server" SelectMethod="GetSessionsByEvent" TypeName="EventManager.Business.EventSessionMethods" DataObjectTypeName="EventManager.Model.EventSession" DeleteMethod="Delete" InsertMethod="Add" UpdateMethod="Update">
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
