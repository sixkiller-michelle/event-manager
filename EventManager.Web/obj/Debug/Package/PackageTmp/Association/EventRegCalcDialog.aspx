﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventRegCalcDialog.aspx.cs" Inherits="EventManager.Web.Association.EventRegCalcDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration Calculation</title>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <style type="text/css">
    html
    {
	    overflow:hidden;
    }
    html, body, form, #<%= AutoChargesRadGridPanelClientID %>, #<%= ManualChargesRadGridPanelClientID %> { height:100%; min-height:100%; padding:0px; margin:0px;}
    </style>

    <script type="text/javascript">
        //<![CDATA[

        function onRequestStart(sender, args) {
            //alert("ajax started " + args.get_eventTarget().toString());
            if (args.get_eventTarget().indexOf("Update") >= 0) {
                args.set_enableAjax(false);
            }
        }

        function CloseAndRebind(chargesTotal) {

            var oWindow = GetRadWindow();
            oWindow.BrowserWindow.UpdateChargeTotal(chargesTotal);
            oWindow.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }

        function CancelRadButton_Clicked(sender, args) {
            CancelEdit();
        }

         //]]>
    </script>
    </telerik:RadCodeBlock>
</head>

<body>
    <form id="form1" runat="server">
    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <telerik:RadFormDecorator ID="RadFormDecorator1" DecoratedControls="All" runat="server" />

   <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"
        OnAjaxSettingCreated="RadAjaxManager1_AjaxSettingCreated" 
        DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="AutoChargesRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AutoChargesRadGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
        
    <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal" Width="100%" Height="100%">
    <telerik:RadPane ID="TopRadPane" runat="server" Height="40">
        <asp:Panel ID="HeaderPanel" runat="server" Style="padding:5px;">
            <telerik:radbutton runat="server" ID="CloseRadButton" text="Close" width="50" onclick="OkRadButton_Click"></telerik:radbutton>
            <asp:Label runat="server" ID="TotalLabel" Text="Total: $0.00" Font-Size="16px" Style="float:right; margin-top:5px;"  />
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane ID="AutoChargesRadPane" runat="server">
       
        <telerik:RadGrid ID="AutoChargesRadGrid" runat="server" CellSpacing="0" 
            GridLines="None" ShowFooter="True" 
            OnNeedDataSource="AutoChargesRadGrid_NeedDataSource" Height="100%" 
            style="border:0;outline:none" ondatabound="AutoChargesRadGrid_DataBound">
            <MasterTableView AutoGenerateColumns="False" OnDataBinding="AutoChargesMasterTableView_DataBinding" DataKeyNames="Id" TableLayout="Fixed" >
                <NoRecordsTemplate>
                    There are no charges for this registration
                </NoRecordsTemplate>
                <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="Add new charge">
                </CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                         
                    <telerik:GridBoundColumn DataField="ChargeDate" DataType="System.DateTime" DataFormatString="{0:d}" FilterControlAltText="Filter ChargeDate column"
                        HeaderText="Date" ItemStyle-Width="80" HeaderStyle-Width="80">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ChargeDesc" FilterControlAltText="Filter ChargeDesc column" ItemStyle-Width="100" HeaderStyle-Width="100"
                        HeaderText="Charge" ReadOnly="True" SortExpression="ChargeDesc" UniqueName="ChargeDesc">
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn Aggregate="Sum" FooterAggregateFormatString="{0:C}" FooterStyle-HorizontalAlign="Right" FooterStyle-Width="60" DataField="ChargeAmt"
                        HeaderText="Amt" DataType="System.Decimal" SortExpression="ChargeAmt" UniqueName="ChargeAmt"
                        DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60"
                        HeaderStyle-Width="60px" FilterControlWidth="60px">
                        <HeaderStyle Width="60px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                    </telerik:GridNumericColumn>
                            
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="false"  UseStaticHeaders="true" />
                <Resizing AllowResizeToFit="True" EnableRealTimeResize="True" />
            </ClientSettings>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </telerik:RadPane>
    <telerik:RadSplitBar ID="SplitterBar1" runat="server"></telerik:RadSplitBar>
    <telerik:RadPane ID="ManualChargesRadPane" runat="server">
        <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="0" GridLines="None" ShowFooter="True"
            AllowAutomaticDeletes="true" AllowAutomaticInserts="true" AllowAutomaticUpdates="true"  
            OnNeedDataSource="RadGrid1_NeedDataSource"
            OnDeleteCommand="RadGrid1_DeleteCommand" Height="100%" style="border:0;outline:none"
            OnInsertCommand="RadGrid1_InsertCommand" OnUpdateCommand="RadGrid1_UpdateCommand"
            OnItemCommand="RadGrid1_ItemCommand" ondatabound="RadGrid1_DataBound">
            <MasterTableView AutoGenerateColumns="False" OnDataBinding="MasterTableView_DataBinding" DataKeyNames="Id" TableLayout="Fixed" CommandItemDisplay="Top">
                <NoRecordsTemplate>
                    There are no charges for this registration
                </NoRecordsTemplate>
                <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText="Add new charge">
                </CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridEditCommandColumn UniqueName="EditCommandColumn" ItemStyle-Width="80px"
                        HeaderText="Edit" HeaderStyle-Width="80px" UpdateText="Update" ButtonType="ImageButton">
                        <HeaderStyle Width="60px"></HeaderStyle>
                        <ItemStyle Width="60px" HorizontalAlign="Center" CssClass="MyImageButton"></ItemStyle>
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="ChargeDate" DataType="System.DateTime" DataFormatString="{0:d}" FilterControlAltText="Filter ChargeDate column"
                        HeaderText="Date" ItemStyle-Width="80" HeaderStyle-Width="80">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ChargeDesc" FilterControlAltText="Filter ChargeDesc column" ItemStyle-Width="100" HeaderStyle-Width="100"
                        HeaderText="Charge" ReadOnly="True" SortExpression="ChargeDesc" UniqueName="ChargeDesc">
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn Aggregate="Sum" FooterAggregateFormatString="{0:C}" FooterStyle-HorizontalAlign="Right" FooterStyle-Width="60" DataField="ChargeAmt"
                        HeaderText="Amt" DataType="System.Decimal" SortExpression="ChargeAmt" UniqueName="ChargeAmt"
                        DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60"
                        HeaderStyle-Width="60px" FilterControlWidth="60px">
                        <HeaderStyle Width="60px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                    </telerik:GridNumericColumn>
                    <telerik:GridBoundColumn DataField="Notes" FilterControlAltText="Filter Notes column"
                        HeaderText="Notes" ReadOnly="True" SortExpression="Notes" UniqueName="Notes">
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this charge?" ConfirmDialogType="RadWindow"
                        ItemStyle-Width="60" HeaderStyle-Width="60" ConfirmTitle="Delete" ButtonType="ImageButton"
                        CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                        <HeaderStyle Width="60px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="3" style="padding-top: 3px;">
                                    <asp:Button ID="InsertButton" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                        Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' />&nbsp;
                                    <asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancel" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Date
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server" DbSelectedDate='<%# Bind("ChargeDate") %>'>
                                    </telerik:RadDatePicker>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Charge
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="ChargeDescRadTextBox" runat="server" Text='<%# Bind("ChargeDesc") %>'>
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Amount
                                </td>
                                <td>
                                    $
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="ChargeAmtRadTextBox" runat="server" Text='<%# Bind("ChargeAmt") %>' NumberFormat-DecimalDigits="2" Type="Currency">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Notes
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="NotesRadTextBox" runat="server" Text='<%# Bind("Notes") %>'>
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table><br />
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="false"  UseStaticHeaders="true" />
                <Resizing AllowResizeToFit="True" EnableRealTimeResize="True" />
            </ClientSettings>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </telerik:RadPane>
    </telerik:RadSplitter>

    </form>
</body>
</html>
