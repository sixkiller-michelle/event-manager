﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventOverview.aspx.cs" Inherits="EventManager.Web.Association.EventOverviewForm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

<style type="text/css">
.RadSplitter_[Vista].pane,   
.RadSplitter_[Vista].paneHorizontal  
{  
    border: 0px;  
} 

.RadSplitter_[Vista]  
{     
    border: 0px;     
} 

.RadSplitter .rspResizeBarHorizontal
        {
            border-style: none !important;
        }
</style>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="100%" Orientation="Horizontal" BorderSize="0" PanesBorderSize="0">
    <telerik:RadPane runat="server" ID="HeaderRadPane" Height="50" Scrolling="None" BorderWidth="0" BorderStyle="None">

        <asp:Panel runat="server" ID="HeaderPanel" Style="padding:5px;">
        <h1>Event Overview</h1>
        </asp:Panel>

    </telerik:RadPane>
    <telerik:RadPane runat="server" ID="EventDetailsRadPane" Scrolling="Both" Height="200">
   
        <telerik:RadSplitter ID="RadSplitter2" runat="server" Width="100%" Height="98%" BorderStyle="None" Orientation="Vertical"  BorderSize="0" PanesBorderSize="0">
        <telerik:RadPane runat="server" ID="EventFormRadPane" Width="280" Scrolling="None" BorderStyle="None">

            <asp:Panel runat="server" ID="EventFormViewPanel" Style="float:left; padding:10px;">
            <asp:FormView ID="FormView1" runat="server" DataSourceID="EventDataSource">
                <ItemTemplate>
            
                   <table class="formview-table-layout">
                   <tr>
                    <td>Facilities Registered:</td>
                    <td>
                        <asp:HyperLink ID="FacilityCountHyperlink" runat="server" Text='<%# Eval("FacilityCount") %>' Font-Size="Larger" NavigateUrl='<%# "~/Association/EventReg.aspx?EventId=" + Request.QueryString["EventId"] %>'></asp:HyperLink></td>
                   </tr>
                   <tr>
                    <td>Attendees Registered:</td>
                    <td><asp:HyperLink ID="AttendeeCountHyperLink" runat="server" Text='<%# Eval("AttendeeCount") %>' Font-Size="Larger" NavigateUrl='<%# "~/Association/EventRegAttendees2.aspx?EventId=" + Request.QueryString["EventId"] %>'></asp:HyperLink></td>
                   </tr>
                   <tr>
                    <td>Number of Sessions:</td>
                    <td><asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("SessionCount") %>' Font-Size="Larger" NavigateUrl='<%# "~/Association/EventSessions.aspx?EventId=" + Request.QueryString["EventId"] %>'></asp:HyperLink>
                   </td>
                   </tr>
                    <tr>
                    <td>Number of Speakers:</td>
                    <td><asp:HyperLink ID="HyperLink2" runat="server" Text='<%# Eval("SpeakerCount") %>' Font-Size="Larger" NavigateUrl='<%# "~/Association/EventSessions.aspx?EventId=" + Request.QueryString["EventId"] %>'></asp:HyperLink>
                    </td>
                   </tr>
                   <tr>
                    <td>Online Registration:</td>
                    <td><asp:Label runat="server" ID="RegOpenLabel" Text="OPEN" Visible='<%# (bool)Eval("IsRegistrationOpen") %>' ForeColor="Green"></asp:Label>
                        <asp:Label runat="server" ID="RegClosedLabel" Text="CLOSED" Visible='<%# !(bool)Eval("IsRegistrationOpen") %>' ForeColor="Red"></asp:Label></td>
                   </tr>
                   <tr>
                    <td>Certificate Printing:</td>
                    <td><asp:Label runat="server" ID="CertPrintLabel" Text="AVAILABLE" Visible='<%# (bool)Eval("IsCertificatePrintingAllowed") %>' ForeColor="Green"></asp:Label>
                            <asp:Label runat="server" ID="CertNoPrintLabel" Text="NOT AVAILABLE" Visible='<%# !(bool)Eval("IsCertificatePrintingAllowed") %>' ForeColor="Red"></asp:Label></td>
                   </tr>
                   </table>
                </ItemTemplate>
            </asp:FormView>
        </asp:Panel>

        </telerik:RadPane>
        <telerik:RadPane runat="server" ID="TabPageRadPane" Scrolling="None" BorderStyle="None">
            
            <asp:Panel runat="server" ID="FlagPanel" Width="100%" Height="180">
            
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1" SelectedIndex="0" Width="100%">
            <Tabs>
                <telerik:RadTab Text="Attendees Per Flag">
                </telerik:RadTab>
                <telerik:RadTab Text="Unpaid Registrations">
                </telerik:RadTab>
            </Tabs>
            </telerik:RadTabStrip>

             <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="100%" Height="160">
                <telerik:RadPageView ID="RadPageView1" runat="server" Width="98%" Height="98%" BorderStyle="Solid" BorderWidth="1" BorderColor="LightGray">
                    
                    <telerik:RadGrid ID="FlagsRadGrid" runat="server" CellSpacing="0" AutoGenerateColumns="false"
                        Height="100%" Width="400" DataSourceID="FlagsDataSource" GridLines="None" OnItemDataBound="FlagsRadGrid_ItemDataBound">
                        <clientsettings>
                        <Scrolling AllowScroll="true" />
                        </clientsettings>
                        <mastertableview datakeynames="Id" datasourceid="FlagsDataSource" showheader="false">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                            Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                            Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FlagName"  ShowFilterIcon="false" 
                                UniqueName="FlagNameGridBoundColumn" HeaderStyle-Width="300px" ItemStyle-Width="300">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="AttendeeCountTemplateColumn" HeaderText="# Attendees" HeaderStyle-HorizontalAlign="Left" 
                                HeaderStyle-Width="40px" ItemStyle-Width="40" ItemStyle-HorizontalAlign="Center" DataType="System.Int32">
                                <ItemTemplate>
                                    <asp:Label ID="AttendeeCountLabel" runat="server" Text="0" />
                                </ItemTemplate>
                                <HeaderStyle Width="40px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </mastertableview>
                        <filtermenu enableimagesprites="False">
                        </filtermenu>
                    </telerik:RadGrid>

                </telerik:RadPageView>

                <telerik:RadPageView ID="RadPageView2" runat="server">
                </telerik:RadPageView>

            </telerik:RadMultiPage>

        </asp:Panel>

        </telerik:RadPane>
        </telerik:RadSplitter>
    </telerik:RadPane>
    <telerik:RadPane runat="server" ID="EventTimeframeRadPane" Scrolling="Both" BorderStyle="None">
        
        <asp:Panel runat="server" ID="EventTimeframePanel" Style="clear:both; width:100%;">

            <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%" ExpandMode="SingleExpandedItem" >
            <Items>
                <telerik:RadPanelItem Text="Before the Event" >
                <ContentTemplate>
                <asp:Panel runat="server" ID="BeforeEventPanel" Style="margin:10px;">
                
                <h2><asp:Label runat="server" ID="BeforeEventDaysLabel" Text="Before the Event" Font-Size="Large" /></h2>

                <asp:Panel runat="server" ID="Over2WeeksPanel">
                    <h3>Prepare for the Event</h3>
                    <ul>
                        <li>If you will need a custom badge or certificate for this event, be sure to contact support to get these created.  THE SOONER
                        THE BETTER.</li>
                        <li>If you have an interface between ceuavenue.com and an external registration system, make sure that the registrations
                        are transferring over.</li>
                        <li>Double check that the <asp:HyperLink ID="PricingHyperLink" runat="server" Target="_blank" NavigateUrl='<%# string.Format("~/Association/EventPricing.aspx?EventId={0}", Request.QueryString["EventId"]) %>'>registration timeframes and fee matrix</asp:HyperLink> are set up accurately.</li>
                    </ul>
                    
                </asp:Panel>

                <asp:Panel runat="server" ID="Under2WeeksPanel">
                    
                    <h3>Prepare the hardware</h3>
                    <ul>
                        <li>If you will be accepting onsite registrations, make sure the DYMO printer drivers are installed on each laptop that you will use to 
                            register attendees onsite.  Verify that you can print a DYMO badge label from each of these laptops.</li>
                        <li>If you will be using the offline scanning application to scan attendees, make sure it 
                            is installed on all netbooks/laptops that will be scanning attendees in/out of each session.  The offline
                            scanning application can be installed from 
                            <asp:HyperLink ID="OfflineScannerInstallHyperlink" runat="server" NavigateUrl="http://www.ceuavenue.com/installers/offlinescanner" Target="_blank">here.</asp:HyperLink></li>
                        <li>Within 2 days of the event start, finalize all 
                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="http://www.ceuavenue.com/installers/offlinescanner" Target="_blank">session information.</asp:HyperLink> 
                            Pay particular attention to the start/end times of each session, and whether each session requires one or two scans for
                            an attendee to get CEU credit.</li>
                        <li>Within 2 days of the event start, use the offline scanning application to download the 
                            event, session, and attendee lists to each netbook/laptop.  This will allow each netbook to 
                            scan attendees in/out of sessions without the need for internet access.</li>
                    </ul>

                    <h3>Test the event processes</h3>
                    <ul>
                        <li>Print a single sheet of 6 attendee badges (From the 'Registered Attendees' page, select the "Selected Attendees / Print / Badge Sheet")</li>
                        <li>Print a DYMO badge (From the 'Registered Attendees' page, select the "Selected Attendees / Print Single Badge")</li>
                        <li><b>Offline Scanning</b>: Start up each netbook/laptop that will be used to scan attendees and attach a scanner to each one.
                            Launch the offline scanning application and scan barcodes from both the badge sheet and the DYMO badge.
                            Upload the scan file and then check the 'Scanner Data' (Scanner Interface / Scanner Data) page in ceuavenue.com to view the scans.</li>
                        <li><b>Real-Time Scanning</b>: Go to the live scanning page on ceuavenue.com (Scanner Interface / Scan Attendees) and scan barcodes from both the badge sheet and the DYMO badge.
                            Verify that the scans show up on the 'Scanner Data' page</li>
                    </ul>

                    <h3>Print reports and reference materials to be used at the event</h3>
                    <ul>
                        <li>Print the “Attendee Barcodes” report twice, sorted first by facility and then by attendee</li>
                        <li>Print the “Session List” for time and room reference</li>
                        <li>Print the “Speaker List”</li>
                    </ul>

                </asp:Panel>
                </asp:Panel>
                </ContentTemplate>    
                </telerik:RadPanelItem>

                <telerik:RadPanelItem Text="After the Event">
                <ContentTemplate>   
                    <asp:Panel runat="server" ID="AfterEventPanel" Style="padding:10px;">
                    
                    
                    <h2><asp:Label runat="server" ID="AfterEventDaysLabel" Text="After the Event" Font-Size="Large" /></h2>
                    <h3>Process Attendance and Grant CEUs</h3>
                    <ul>
                        <li>Adjust session start/end times for those sessions that started or ended earlier than originally scheduled.</li>
                        <li>Upload all scan files from the Offline Scanning application on each netbook/laptop</li>
                        <li>Process attendance</li>
                    </ul>
                    <h3>Printing Certificates</h3>
                    <ul>
                        <li>Attendees can print their own certifcates from the 
                        <asp:HyperLink ID="PrintCertPageHyperLink" runat="server" NavigateUrl="~/printcertSearch.aspx" Target="_blank">"Print Certificate"</asp:HyperLink> page.
                        In order to print, the attendee's registration must have a status of "Paid" (if required) and they must have taken the event survey (if required).
                        Also, the dates for certificate printing must be valid.  You can verify the printing dates from the 
                        <asp:HyperLink ID="EventSetupHyperlink" runat="server" NavigateUrl='<%# "~/Association/EditEvent.aspx?EventId=" + Request.QueryString["EventId"] %>'  Target="_blank">Edit Event</asp:HyperLink> page.</li>
                        <li>If an attendee is having problems finding their attendance history (error "No matching person can be found"), make 
                        sure they are entering the correct facility.  This will be the facility listed on their attendee profile (accessible from the 
                        main "Attendees" menu item) and NOT necessarily the facility under which they attended the event.  In most cases, these
                        will be the same, but not always.</li>
                        <li></li>
                    </ul>
                    </asp:Panel>
                    </ContentTemplate>   
                </telerik:RadPanelItem>
            </Items>
            </telerik:RadPanelBar>

        </asp:Panel>

    </telerik:RadPane>
</telerik:RadSplitter>

    <asp:ObjectDataSource ID="EventDataSource" runat="server" DataObjectTypeName="EventManager.Model.Event" 
        DeleteMethod="Delete" SelectMethod="GetEvent" 
        TypeName="EventManager.Business.EventMethods" >
        <SelectParameters>
            <asp:QueryStringParameter Name="eventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="FlagsDataSource" runat="server" SelectMethod="GetFlagsByEvent" TypeName="EventManager.Business.EventFlagMethods" 
        DataObjectTypeName="EventManager.Model.EventFlag">
        <SelectParameters>
            <asp:QueryStringParameter Name="EventId" QueryStringField="EventId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

<%--    The Overview should include all of this information in a preview format as well as the following high level details:
•	Total number of facilities/organizations registered
•	Total Attendees
•	Total for each flag (including “speaker”)
•	Total number of sessions
•	Unpaid registrations (facility name)--%>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
       <!--

       -->
    </script>

</telerik:RadCodeBlock>

</asp:Content>