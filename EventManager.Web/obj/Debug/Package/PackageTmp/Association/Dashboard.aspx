﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="EventManager.Web.Association.Dashboard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadSplitter runat="server" ID="RadSplitter1" Width="100%" Height="100%" BorderStyle="Solid" BorderSize="1" Orientation="Horizontal" >
   <%--  <telerik:RadPane ID="TopRadPane" runat="server" Scrolling="None" Height="80" >

        <asp:Panel ID="HeaderPanel" runat="server" Style="margin:10px;">
            <h2>Event List</h2>
            <telerik:RadButton ID="RadButton1" runat="server" Text="New Event" UseSubmitBehavior="true" PostBackUrl="~/Association/NewEvent.aspx" NavigateUrl="~/Association/NewEvent.aspx" />
        </asp:Panel>

    </telerik:RadPane>--%>
    <telerik:RadPane ID="ConventionRadPane" runat="server" Scrolling="None" Height="85">
        <asp:Panel runat="server" ID="ConventionPanel" Style="margin:10px;">
        <i>Convention:&nbsp;&nbsp;</i><asp:Label ID="ConventionNameLabel" runat="server" Text="" Font-Bold="true" Font-Size="Larger" /><br />
        <asp:Label ID="ConventionDateLabel" runat="server" Text="" />&nbsp;(<asp:Label ID="ConventionLagTimeLabel" runat="server" Text="" />)<br />
        <asp:Panel runat="server" ID="ConventionButtonsPanel" Style="margin:10px;">
        <telerik:RadButton ID="OverviewRadButton1" runat="server" Text="Overview" PostBackUrl="~/Association/EventOverview.aspx" />&nbsp;
        <telerik:RadButton ID="AttendeesRadButton1" runat="server" Text="Attendees" PostBackUrl="~/Association/EventRegAttendees2.aspx" />&nbsp;
        <telerik:RadButton ID="FacilitiesRadButton1" runat="server" Text="Facilities" PostBackUrl="~/Association/EventReg.aspx" />
        </asp:Panel>
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane ID="NextEventRadPane" runat="server" Scrolling="None" Height="85">
        <asp:Panel runat="server" ID="NextEventPanel" Style="margin:10px;">
        <i>Next Event:&nbsp;&nbsp;</i><asp:Label ID="NextEventNameLabel" runat="server" Text="" Font-Bold="true" Font-Size="Larger" /><br />
        <asp:Label ID="NextEventDateLabel" runat="server" Text="" />&nbsp;(<asp:Label ID="NextEventLagTimeLabel" runat="server" Text="" />)<br />
        <asp:Panel runat="server" ID="NextEventButtonsPanel" Style="margin:10px;">
        <telerik:RadButton ID="OverviewRadButton2" runat="server" Text="Overview" PostBackUrl="~/Association/EventOverview.aspx" />&nbsp;
        <telerik:RadButton ID="AttendeesRadButton2" runat="server" Text="Attendees" PostBackUrl="~/Association/EventRegAttendees2.aspx" />&nbsp;
        <telerik:RadButton ID="FacilitiesRadButton2" runat="server" Text="Facilities" PostBackUrl="~/Association/EventReg.aspx" />
        </asp:Panel>
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane ID="LastEventRadPane" runat="server" Scrolling="None" Height="85">
        <asp:Panel runat="server" ID="LastEventPanel" Style="margin:10px;">
        <i>Most Recent Event:&nbsp;&nbsp;</i><asp:Label ID="LastEventNameLabel" runat="server" Text="" Font-Bold="true" Font-Size="Larger" /><br />
        <asp:Label ID="LastEventDateLabel" runat="server" Text="" />&nbsp;(<asp:Label ID="LastEventLagTimeLabel" runat="server" Text="" />)<br />
        <asp:Panel runat="server" ID="LastEventButtonsPanel" Style="margin:10px;">
        <telerik:RadButton ID="OverviewRadButton3" runat="server" Text="Overview" PostBackUrl="~/Association/EventOverview.aspx" />&nbsp;
        <telerik:RadButton ID="AttendeesRadButton3" runat="server" Text="Attendees" PostBackUrl="~/Association/EventRegAttendees2.aspx" />&nbsp;
        <telerik:RadButton ID="FacilitiesRadButton3" runat="server" Text="Facilities" PostBackUrl="~/Association/EventReg.aspx" />
        </asp:Panel>
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane ID="UpcomingEventsHeaderPane" runat="server" Scrolling="None" Height="30" BackColor="#e1dac7">
        <asp:Panel runat="server" ID="UpcomingEventsHeaderPanel" Style="margin:10px;">
            <b>Upcoming Events</b>
        </asp:Panel>
    </telerik:RadPane>
    <telerik:RadPane ID="UpcomingEventsRadPane" runat="server" Scrolling="None">
    
        <telerik:RadGrid ID="UpcomingEventsRadGrid" runat="server" AutoGenerateColumns="False"
            CellSpacing="0" GridLines="None" AllowPaging="True" PageSize="5" DataSourceID="UpcomingEventsDataSource"
            MasterTableView-PagerStyle-Width="300">
            <MasterTableView DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridDateTimeColumn DataField="StartDateTime" HeaderText="Date" UniqueName="StartDateTime"
                        SortExpression="StartDateTime" HeaderStyle-Width="70px" FilterControlWidth="100px"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo" ShowFilterIcon="false"
                        DataFormatString="{0:d}">
                        <HeaderStyle Width="70px"></HeaderStyle>
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn HeaderText="Event" DataField="EventName" UniqueName="EventName"
                        SortExpression="EventName" HeaderStyle-Width="300px" FilterControlWidth="300px"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true">
                        <HeaderStyle Width="300px"></HeaderStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="EventStatusTemplateColumn" SortExpression="EventStatusId"
                        InitializeTemplatesFirst="false" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <%# Eval("EventStatu.StatusDesc") %>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridHyperLinkColumn FilterControlAltText="Filter column column" UniqueName="Edit"
                        Text="Edit" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="EventRegAttendees2.aspx?EventId={0}">
                    </telerik:GridHyperLinkColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>

    </telerik:RadPane>
    </telerik:RadSplitter>

    <asp:ObjectDataSource ID="UpcomingEventsDataSource" runat="server" 
        SelectMethod="GetUpcomingEvents" TypeName="EventManager.Business.EventMethods" 
        onselecting="UpcomingEventsDataSource_Selecting" 
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
            <asp:Parameter DbType="Guid" Name="userId" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content>
