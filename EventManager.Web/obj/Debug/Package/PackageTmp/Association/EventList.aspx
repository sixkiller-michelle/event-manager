﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.master" AutoEventWireup="true" CodeBehind="EventList.aspx.cs" Inherits="EventManager.Web.Association.EventListForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
td.hyperlinkColumn a {color: Blue;}
.hyperlinkColumn a {color: Blue;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<telerik:RadSplitter runat="server" ID="EventRadSplitter" Width="100%" Height="100%" BorderStyle="None" BorderSize="0" Orientation="Horizontal" >
    
    <telerik:RadPane ID="TopRadPane" runat="server" Scrolling="None" Height="80" >

        <asp:Panel ID="HeaderPanel" runat="server" Style="margin:10px;">
            <h2>Event List</h2>
            <asp:HyperLink ID="NewEventHyperLink" runat="server" NavigateUrl="~/Association/NewEvent.aspx">New Event...</asp:HyperLink>
        </asp:Panel>

    </telerik:RadPane>

    <telerik:RadPane ID="FutureEventsHeaderPane" runat="server" Scrolling="None" Height="30" BackColor="#e1dac7">
        <asp:Panel runat="server" ID="UpcomingEventsHeaderPanel" Style="margin:10px;">
            <b>Upcoming Events</b>
        </asp:Panel>
    </telerik:RadPane>

    <telerik:RadPane ID="MiddleRadPane" runat="server" Scrolling="None" Height="200">

        <telerik:RadGrid ID="FutureEventsRadGrid" runat="server" AllowFilteringByColumn="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
             GridLines="None" AllowPaging="True" PagerStyle-Position="TopAndBottom" 
            PageSize="20" Height="100%" onneeddatasource="FutureEventsRadGrid_NeedDataSource" 
            onitemdatabound="FutureEventsRadGrid_ItemDataBound">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>
        <MasterTableView DataKeyNames="Id" >
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>

                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" HeaderText="Month" DataFormatString="{0:Y}" 
                    SortExpression="StartDateTime" UniqueName="StartDateTime" HeaderStyle-Width="120" ItemStyle-Width="100">
                    <FilterTemplate>Clear filters
                        <asp:ImageButton ID="ShowAllFutureEventsImageButton" runat="server" ImageUrl="../Images/filterCancel.gif" AlternateText="Show All" ToolTip="Show All" OnClick="ShowAllFutureEventsImageButton_Click"
                            Style="vertical-align: middle" />
                    </FilterTemplate>
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" HeaderText="Start Date" DataFormatString="{0:d}"  
                    SortExpression="StartDateTime" UniqueName="StartDateTime" HeaderStyle-Width="120" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Right">
                </telerik:GridBoundColumn>

                <telerik:GridHyperLinkColumn FilterControlAltText="Filter event name" HeaderStyle-Width="350px" HeaderText="Event" SortExpression="EventName" AllowSorting="true"
                            FilterControlWidth="300px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true" ItemStyle-CssClass="hyperlinkColumn"
                            UniqueName="EventNameHyperlink"
                            DataType="System.String"
                            DataTextField="EventName"
                            DataNavigateUrlFields="Id"
                            DataNavigateUrlFormatString="EventRegAttendees2.aspx?EventId={0}">
                        </telerik:GridHyperLinkColumn>
       
                <telerik:GridTemplateColumn UniqueName="EventTypeTemplateColumn" DataField="TypeId" HeaderText="Type" SortExpression="TypeId" ItemStyle-Width="150" HeaderStyle-Width="120px">
                  <ItemTemplate>
                        <%# Eval("EventType.TypeName") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="RadComboBoxEventType" DataSourceID="EventTypesDataSource" DataTextField="TypeName" Width="100" DropDownWidth="150"
                            DataValueField="Id" Height="200px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("EventTypeTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="EventTypeIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

                            <script type="text/javascript">
                                function EventTypeIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("EventTypeTemplateColumn", args.get_item().get_value(), "EqualTo");

                                }
                            </script>

                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

<%--
                <telerik:GridTemplateColumn UniqueName="EventStatusTemplateColumn" DataField="EventStatusId" HeaderText="Status" SortExpression="EventStatusId" ItemStyle-Width="120"  HeaderStyle-Width="120px">
                  <ItemTemplate>
                        <%# Eval("EventStatu.StatusDesc") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="RadComboBoxEventStatus" DataSourceID="EventStatiDataSource" DataTextField="StatusDesc" Width="100"
                            DataValueField="EventStatusId" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("EventStatusTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="EventStatusIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                            <script type="text/javascript">
                                function EventStatusIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("EventStatusTemplateColumn", args.get_item().get_value(), "EqualTo");

                                }
                            </script>

                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>--%>

                <telerik:GridBoundColumn DataField="FullLocation" AllowFiltering="false" HeaderText="Location" ReadOnly="True" SortExpression="FullLocation" UniqueName="FullLocation">
                </telerik:GridBoundColumn>
            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False"></FilterMenu>

        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>

    </telerik:RadPane>

    <telerik:RadPane ID="PastEventsRadPane" runat="server" Scrolling="None" Height="30" BackColor="#e1dac7">
        <asp:Panel runat="server" ID="Panel1" Style="margin:10px;">
            <b>Past Events</b>
        </asp:Panel>
    </telerik:RadPane>

    <telerik:RadPane ID="BottomRadPane" runat="server" Scrolling="None">
    
        <telerik:RadGrid ID="PastEventsRadGrid" runat="server" AllowFilteringByColumn="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
             GridLines="None" AllowPaging="True" PagerStyle-Position="TopAndBottom" 
            PageSize="20" Height="100%" onneeddatasource="PastEventsRadGrid_NeedDataSource" 
            onitemdatabound="PastEventsRadGrid_ItemDataBound">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
        </ClientSettings>
        <MasterTableView DataKeyNames="Id" >
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

            <Columns>

                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" HeaderText="Month" DataFormatString="{0:Y}" 
                    SortExpression="StartDateTime" UniqueName="StartDateTime" HeaderStyle-Width="120" ItemStyle-Width="100">
                    <FilterTemplate>Clear filters
                        <asp:ImageButton ID="ShowAllPastEventsImageButton" runat="server" ImageUrl="../Images/filterCancel.gif" AlternateText="Show All" ToolTip="Show All" OnClick="ShowAllPastEventsImageButton_Click"
                            Style="vertical-align: middle" />
                    </FilterTemplate>
                </telerik:GridBoundColumn>

                <telerik:GridBoundColumn DataField="StartDateTime" DataType="System.DateTime" HeaderText="Start Date" DataFormatString="{0:d}" 
                    SortExpression="StartDateTime" UniqueName="StartDateTime" HeaderStyle-Width="120" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Right">
                </telerik:GridBoundColumn>

                <telerik:GridHyperLinkColumn FilterControlAltText="Filter event name" HeaderStyle-Width="350px" HeaderText="Event" SortExpression="EventName" AllowSorting="true"
                            FilterControlWidth="300px" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" ShowFilterIcon="true" ItemStyle-CssClass="hyperlinkColumn"
                            UniqueName="EventNameHyperlink"
                            DataType="System.String"
                            DataTextField="EventName"
                            DataNavigateUrlFields="Id"
                            DataNavigateUrlFormatString="EventRegAttendees2.aspx?EventId={0}">
                        </telerik:GridHyperLinkColumn>
       
                <telerik:GridTemplateColumn UniqueName="EventTypeTemplateColumn" DataField="TypeId" HeaderText="Type" SortExpression="TypeId" ItemStyle-Width="150" HeaderStyle-Width="120px">
                  <ItemTemplate>
                        <%# Eval("EventType.TypeName") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="RadComboBoxEventType" DataSourceID="EventTypesDataSource" DataTextField="TypeName" Width="100" DropDownWidth="150"
                            DataValueField="Id" Height="200px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("EventTypeTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="EventTypeIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

                            <script type="text/javascript">
                                function EventTypeIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("EventTypeTemplateColumn", args.get_item().get_value(), "EqualTo");

                                }
                            </script>

                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>

<%--
                <telerik:GridTemplateColumn UniqueName="EventStatusTemplateColumn" DataField="EventStatusId" HeaderText="Status" SortExpression="EventStatusId" ItemStyle-Width="120"  HeaderStyle-Width="120px">
                  <ItemTemplate>
                        <%# Eval("EventStatu.StatusDesc") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                    <FilterTemplate>
                        <telerik:RadComboBox ID="RadComboBoxEventStatus" DataSourceID="EventStatiDataSource" DataTextField="StatusDesc" Width="100"
                            DataValueField="EventStatusId" Height="150px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("EventStatusTemplateColumn").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="EventStatusIndexChanged">
                            <Items>
                                <telerik:RadComboBoxItem Text="All" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">

                            <script type="text/javascript">
                                function EventStatusIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("EventStatusTemplateColumn", args.get_item().get_value(), "EqualTo");

                                }
                            </script>

                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridTemplateColumn>--%>

                <telerik:GridBoundColumn DataField="FullLocation" AllowFiltering="false" HeaderText="Location" ReadOnly="True" SortExpression="FullLocation" UniqueName="FullLocation">
                </telerik:GridBoundColumn>
            </Columns>

        <EditFormSettings>
        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False"></FilterMenu>

        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>

    </telerik:RadPane>
</telerik:RadSplitter>

    <%--<asp:ObjectDataSource ID="EventsDataSource" runat="server" onselecting="EventsDataSource_Selecting" SelectMethod="GetEventList" TypeName="EventManager.Business.EventMethods">
        <SelectParameters>
            <asp:Parameter Name="associationId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
--%>
    <asp:ObjectDataSource ID="EventTypesDataSource" runat="server" SelectMethod="GetEventTypes" TypeName="EventManager.Business.EventMethods"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="EventStatiDataSource" runat="server" SelectMethod="GetEventStati" TypeName="EventManager.Business.EventMethods"></asp:ObjectDataSource>


</asp:Content>
