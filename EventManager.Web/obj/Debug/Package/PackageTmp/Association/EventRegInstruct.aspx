﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/Event2.master" AutoEventWireup="true" CodeBehind="EventRegInstruct.aspx.cs" Inherits="EventManager.Web.Association.EventRegInstructForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content">

    <h2>Registration Instructions</h2>

    <p>Enter the registration information here.  This text will be displayed to all users when they begin the 
            registration process.  You might want to include items such as:</p>
            <ul>
                <li>Physical address of event</li>
                <li>Highlight important registration dates and deadlines</li>
                <li>Instructions for paying by check (if applicable)</li>
                <li>Summary of registration costs and fees</li>
                <li>Cancellation policy</li>
            </ul>
             
    <telerik:RadButton ID="SaveRadButton" runat="server" Text="Save" onclick="SaveRadButton_Click">
    </telerik:RadButton>

    <telerik:radeditor ID="RadEditor1" runat="server" SkinID="DefaultSetOfTools" ToolbarMode="Default" Skin="Default" BackColor="White" ContentAreaCssFile="" ContentAreaMode="Div">
    <Content>
        This is a test.
    </Content>
    <ImageManager ViewPaths="~/Association/Images"
                            UploadPaths="~/Association/Images"
                            DeletePaths="~/Association/Images">
                        </ImageManager>

    </telerik:radeditor>
    </div>

</asp:Content>
