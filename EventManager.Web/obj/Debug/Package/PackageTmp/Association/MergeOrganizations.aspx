﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Association/AssociationMaster2.Master" AutoEventWireup="true" CodeBehind="MergeOrganizations.aspx.cs" Inherits="EventManager.Web.Association.MergeOrganizationsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
.content {width:95%; margin: 0 auto;}
.EditAttendeeButton {margin-top:170px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <telerik:RadSplitter ID="RadSplitter1" runat="server" Orientation="Horizontal" Width="100%" Height="100%">
    <telerik:RadPane runat="server" Height="50">
    
    <div style="margin:10px;">
            <telerik:RadButton ID="MergeButton" runat="server" Text="Merge Facilities" 
                Enabled="false" onclick="MergeButton_Click">
            </telerik:RadButton>
        &nbsp;&nbsp;
        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="jhlkjhkjhkjhlkj" ForeColor="Red" Font-Size="Large" Display="Dynamic" CssClass="errorMessage"></asp:CustomValidator>
    </div>

    </telerik:RadPane>
    <telerik:RadSplitBar runat="server"></telerik:RadSplitBar>
     <telerik:RadPane ID="RadPane1" runat="server">
     
        <telerik:RadSplitter ID="RadSplitter2" runat="server" Orientation="Vertical">
        <telerik:RadPane ID="DeleteOrganizationRadPane" runat="server">
    
        <div class="content">
            <table class="formview-table-layout">
            <tr>
                <td colspan="2">
                    <h2 style="color:Red;">This facility will be DELETED</h2>
                </td>
            </tr>
            <tr>
                <td>Facility ID:</td>
                <td>
                    <telerik:RadTextBox ID="DeleteOrgRadTextBox" runat="server" AutoPostBack="true">
                    </telerik:RadTextBox></td>
            </tr>
            </table>

            <asp:FormView ID="DeleteOrgFormView" runat="server" 
                DataSourceID="OldOrgDataSource"  DataKeyNames="Id"
                ondatabound="DeleteOrgFormView_DataBound">
                
                <ItemTemplate>
                    <table class="formview-table-layout">
                    <tr>
                        <td colspan="2"><h3>Facility Profile</h3><hr /></td>
                    </tr>
                    <tr>
                        <td class="label">Name: </td>
                        <td> <asp:Label ID="Label1" runat="server" Text='<%# Bind("OrgName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Type:</td>
                        <td><asp:Label ID="Label3" runat="server" Text='<%# Bind("OrganizationType.OrgTypeName") %>' /><br />
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("OrganizationFeeCategory.CategoryDesc") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Address:</td>
                        <td><asp:Label ID="Label4" runat="server" Text='<%# Bind("FullAddress") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><asp:Label ID="Label5" runat="server" Text='<%# Bind("Phone") %>' /></td>
                    </tr>
                    </table>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <asp:Label ID="EmptyDataLabel" runat="server" Text='This facility does not exist.' ForeColor="Red" />
                </EmptyDataTemplate>
            </asp:FormView>
            <br />
            <%--<h3>Attendance History</h3><hr />
            <telerik:RadGrid ID="DeleteRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="OldRegHistoryDataSource" GridLines="None">
                <MasterTableView DataSourceID="OldRegHistoryDataSource">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Registration.Event.EventName" 
                            FilterControlAltText="Filter Event column" HeaderText="Event" 
                            SortExpression="Registration.Event.EventName" UniqueName="EventNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Registration.Organization.OrgName" 
                            FilterControlAltText="Filter Facility column" HeaderText="Facility" 
                            SortExpression="Registration.Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Title" 
                            FilterControlAltText="Filter Title column" HeaderText="Title" 
                            SortExpression="Title" UniqueName="Title">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CeuTotal" DataType="System.Decimal" 
                            FilterControlAltText="Filter CeuTotal column" HeaderText="CeuTotal" 
                            ReadOnly="True" SortExpression="CeuTotal" UniqueName="CeuTotal">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>--%>
        </div>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="RadSplitBar1" runat="server"></telerik:RadSplitBar>
        <telerik:RadPane ID="KeepOrgRadPane" runat="server">
        <div class="content">
         <table class="formview-table-layout">
         <tr>
                <td colspan="2">
                    <h2 style="color:Green;">Keep this facility</h2>
                </td>
            </tr>
            <tr>
                <td>Facility ID:</td>
                <td>
                    <telerik:RadTextBox ID="KeepOrgRadTextBox" runat="server" AutoPostBack="true">
                    </telerik:RadTextBox></td>
            </tr>
            </table>

            <asp:FormView ID="KeepOrgFormView" runat="server" 
                DataSourceID="NewOrgDataSource" DataKeyNames="Id" 
                ondatabound="KeepOrgFormView_DataBound">
                
                <ItemTemplate>
                    <table class="formview-table-layout" style="float:left;">
                    <tr>
                        <td colspan="2"><h3>Facility Profile</h3><hr /></td>
                    </tr>
                    <tr>
                        <td class="label">Name: </td>
                        <td> <asp:Label ID="Label1" runat="server" Text='<%# Bind("OrgName") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Type:</td>
                        <td><asp:Label ID="Label3" runat="server" Text='<%# Bind("OrganizationType.OrgTypeName") %>' /><br />
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("OrganizationFeeCategory.CategoryDesc") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Address:</td>
                        <td><asp:Label ID="Label4" runat="server" Text='<%# Bind("FullAddress") %>' /></td>
                    </tr>
                    <tr>
                        <td class="label">Phone:</td>
                        <td><asp:Label ID="Label5" runat="server" Text='<%# Bind("Phone") %>' /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><telerik:RadButton ID="EditOrgRadButton" runat="server" Text="Edit facility profile" AutoPostBack="false" OnClientClicked="ShowOrgDialog" CssClass="EditOrgButton">
                    </telerik:RadButton></td>
                    </tr>
                    </table>
                    

                </ItemTemplate>
            </asp:FormView>
            <div style="clear:both;"></div>
            <br />
            <%--<h3>Attendance History</h3><hr />
            <telerik:RadGrid ID="KeepRadGrid" runat="server" AutoGenerateColumns="False" 
                CellSpacing="0" DataSourceID="NewRegHistoryDataSource" GridLines="None">
                <MasterTableView DataSourceID="NewRegHistoryDataSource">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Registration.Event.EventName" 
                            FilterControlAltText="Filter Event column" HeaderText="Event" 
                            SortExpression="Registration.Event.EventName" UniqueName="EventNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Registration.Organization.OrgName" 
                            FilterControlAltText="Filter Facility column" HeaderText="Facility" 
                            SortExpression="Registration.Organization.OrgName" UniqueName="OrgNameGridBoundColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Title" 
                            FilterControlAltText="Filter Title column" HeaderText="Title" 
                            SortExpression="Title" UniqueName="Title">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CeuTotal" DataType="System.Decimal" 
                            FilterControlAltText="Filter CeuTotal column" HeaderText="CeuTotal" 
                            ReadOnly="True" SortExpression="CeuTotal" UniqueName="CeuTotal">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
--%>
        </div>
        </telerik:RadPane>
        </telerik:RadSplitter>

     </telerik:RadPane>
    </telerik:RadSplitter>

    
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
    //<![CDATA[
        function ShowOrgDialog() {

            // Get the attendeeID from textbox
            var orgTextBox = $find("<%= KeepOrgRadTextBox.ClientID %>");
            var orgId = orgTextBox.get_value();
            var wnd = window.radopen("OrgDetailsDialog.aspx?OrgId=" + orgId, "OrgDetailsDialog");
            wnd.setSize(850, 550);
            return false;
        }

        function OrgUpdated(orgId, orgName) {

            var orgTextBox = document.all("<%=KeepOrgRadTextBox.ClientID %>");
            __doPostBack('KeepOrgRadTextBox', orgTextBox.value);
      
        }
      //]]>
    </script>
</telerik:RadCodeBlock>

    <telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="Facility ID..."
            Type="Number">
            <TargetControls>
                <telerik:TargetInput ControlID="DeleteOrgRadTextBox" />
                <telerik:TargetInput ControlID="KeepOrgRadTextBox" />
            </TargetControls>
        </telerik:NumericTextBoxSetting>
    </telerik:RadInputManager>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Style="z-index:8000;" 
        EnableShadow="True">
        <Windows>
            <telerik:RadWindow ID="OrgDetailsDialog" runat="server" 
                Title="Edit Facility" Height="500px"
                Width="800px" Left="150px" Top="50" ReloadOnShow="true" ShowContentDuringLoad="true"
                Modal="true" DestroyOnClose="True" Overlay="True" />
        </Windows>
    </telerik:RadWindowManager>

    <asp:ObjectDataSource ID="OldOrgDataSource" runat="server" 
        SelectMethod="GetOrganization" TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="DeleteOrgRadTextBox" Name="orgId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="NewOrgDataSource" runat="server" 
        SelectMethod="GetOrganization" 
        TypeName="EventManager.Business.OrganizationMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="KeepOrgRadTextBox" Name="orgId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

   <%-- <asp:ObjectDataSource ID="OldRegHistoryDataSource" runat="server" 
        SelectMethod="GetRegistrationsByAttendee" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="DeleteAttendeeRadTextBox" Name="orgId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="NewRegHistoryDataSource" runat="server" 
        SelectMethod="GetRegistrationsByAttendee" 
        TypeName="EventManager.Business.RegistrationAttendeesMethods">
        <SelectParameters>
            <asp:ControlParameter ControlID="KeepAttendeeRadTextBox" Name="orgId" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>

</asp:Content>
