﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="403.aspx.cs" Inherits="Inclusion.Web.Error403Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="contentInner">
<h2>I'm sorry, but you do not have access to the requested page.</h2>

<p>Try one of these pages instead:</p>

<asp:Panel ID="LoginPanel" runat="server">
    <asp:HyperLink ID="LoginHyperLink" runat="server" NavigateUrl="~/Account/Login.aspx">Login</asp:HyperLink><br /><br />
</asp:Panel>

<asp:Panel ID="ProfilePanel" runat="server">
    <asp:HyperLink ID="ProfileHyperLink" runat="server" NavigateUrl="~/Profile.aspx">Profile</asp:HyperLink><br /><br />
</asp:Panel>

<asp:Panel ID="PermbooksPanel" runat="server">
    <b>Permbooks</b><br />
    <asp:HyperLink ID="PermbooksHyperLink" runat="server" NavigateUrl="~/Permbooks/ParticipantList.aspx">Participant List</asp:HyperLink><br /><br />
</asp:Panel>

<asp:Panel ID="HrPanel" runat="server">
    <b>HR</b><br />
    <asp:HyperLink ID="HrHyperLink" runat="server" NavigateUrl="~/Permbooks/ParticipantList.aspx">Employee List</asp:HyperLink><br /><br />
</asp:Panel>

<asp:Panel ID="BillingPanel" runat="server">
    <b>Billing</b><br />
    <asp:HyperLink ID="BillingHyperLink" runat="server" NavigateUrl="~/Billing/Default.aspx">Dashboard</asp:HyperLink><br /><br />
</asp:Panel>

</div>
</asp:Content>
