﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Facility/FacilityMaster.master" AutoEventWireup="true" CodeBehind="PrintCertHistory.aspx.cs" Inherits="EventManager.Web.PrintCertHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
tr.rgRow td, tr.rgAltRow td {font-size:10pt; line-height:normal;}
tr.rgRow td a, tr.rgAltRow td a {color:blue; text-decoration:underline;}
th.rgHeader {font-size:10pt;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="ErrorPanel" runat="server" style="padding:10px;" Visible="false">
    <p>Sorry, but your attendance history could not be found.  Please return to 
    the <asp:HyperLink ID="SearchPageHyperlink" NavigateUrl="~/PrintCertSearch.aspx" runat="server">attendee search page</asp:HyperLink> and try again.</p>

    </asp:Panel>

    <asp:Panel ID="EventsPanel" runat="server" style="padding:10px;">
        <asp:Label ID="AttendeeNameLabel" runat="server" Text="Name" Font-Size="Large" ForeColor="#D2912C"></asp:Label><br />
        <asp:Label ID="AttendeeTitleLabel" runat="server" Text="Title"></asp:Label><br />
        <br />
        <h3 style="font-weight: bold;">
            Attendance History</h3>
        <p style="font-size: smaller;">
            In order to print your certificate, ALL of the following must be true:</p>
        <ul style="font-size: smaller;">
            <li>Your registration must be fully <b>paid</b></li>
            <li>You must have completed the event <b>survey</b></li>
            <li>Today's date must fall in the range of the "Certificate Available" dates</li>
            <li style="color: red;">Note: Your certificate may be more than one page- to view multiple
                pages, use the toolbar menu at the top of the report</li>
        </ul>
        <telerik:RadGrid ID="EventHistoryRadGrid" runat="server" AutoGenerateColumns="False"
            CellSpacing="0" GridLines="None" OnItemCommand="RadGrid1_ItemCommand" Width="900px"
            OnItemDataBound="RadGrid1_ItemDataBound">
            <MasterTableView DataKeyNames="AssociationId, EventId, AttendeeId">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="EventStartDate" DataFormatString="{0:d}" DataType="System.DateTime"
                        FilterControlAltText="Filter StartDateTime column" HeaderText="Date" SortExpression="StartDateTime"
                        UniqueName="StartDateTime" ItemStyle-Width="80">
                        <ItemStyle Width="80px"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EventName" FilterControlAltText="Filter EventName column"
                        HeaderText="Event" SortExpression="EventName" UniqueName="EventName" ItemStyle-Width="300">
                        <ItemStyle Width="300px"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CeuHours" FilterControlAltText="Filter CeuHours column"
                        HeaderText="CEUs" SortExpression="CeuHours" UniqueName="CeuHoursBoundColumn"
                        ItemStyle-Width="50">
                        <ItemStyle Width="50px"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CertificateDateRange" FilterControlAltText="Filter CertificateDateRange column"
                        HeaderText="Certificate Available" SortExpression="CertificateDateRange" UniqueName="CertificateDateRangeBoundColumn"
                        ItemStyle-Width="150">
                        <ItemStyle Width="150px"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="IsPaidTemplateColumn" DataField="IsPaid"
                        HeaderText="Paid" ItemStyle-Width="50">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToBoolean(Eval("IsPaid")) == true ? "Yes" : "No" %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="50px"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="IsSurveyCompleteTemplateColumn" DataField="IsSurveyComplete"
                        HeaderText="Survey" ItemStyle-Width="50">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "Yes" : "No" %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="50px"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridHyperLinkColumn UniqueName="SurveyHyperlinkColumn" HeaderText="" DataType="System.String"
                        Text="Take Survey..." DataNavigateUrlFields="AssociationId, EventId, AttendeeId"
                        DataNavigateUrlFormatString="Survey.aspx?AssnId={0}&EventId={1}&AttendeeId={2}">
                    </telerik:GridHyperLinkColumn>
                    <%-- <telerik:GridTemplateColumn UniqueName="IsSurveyCompleteTemplateColumn" DataField="IsSurveyComplete" HeaderText="Survey" ItemStyle-Width="50">
                            <ItemTemplate>
                                <asp:HyperLink ID="IsSurveyCompleteHyperlink" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "" : "Take Survey..." %>'></asp:HyperLink>
                                <asp:Label ID="IsSurveyCompleteLabel" runat="server" Text='<%# Convert.ToBoolean(Eval("IsSurveyComplete")) == true ? "Yes" : "" %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    --%>
                    <telerik:GridButtonColumn UniqueName="PrintCertGridButtonColumn" CommandName="PrintCertificate"
                        HeaderText="Print" Text="Print Certificate" ItemStyle-Width="150">
                        <ItemStyle Width="150px"></ItemStyle>
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br /><br />
    </asp:Panel>
</asp:Content>
