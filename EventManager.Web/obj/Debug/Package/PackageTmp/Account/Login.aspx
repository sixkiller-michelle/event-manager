﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="EventManager.Web.Account.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
    table td {padding: 3px; vertical-align:middle;}
</style>
</asp:Content>
<asp:Content ID="MenuContent" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

<div id="content" class="box">

	<h2 class="page-title">Log In</h2>
	<div class="breadcrumb"><a href="../Default.aspx" id="home">Home</a>  »  <a href="#">Log in</a></div>

    <p>Please enter your username and password.<br />
    <asp:HyperLink ID="RegisterHyperLink" runat="server" EnableViewState="false">Register</asp:HyperLink> &nbsp;if you don't have an account.</p>

    <p>
    <asp:Login ID="LoginUser" runat="server" InstructionText="Enter your user name and password to log in."
         style="text-align: left; width:400px; float:none;"
         OnLoggedIn="LoginUser_OnLoggedIn" onloggingin="LoginUser_LoggingIn" 
        onloginerror="LoginUser_LoginError">
        <InstructionTextStyle Font-Bold="True" ForeColor="#E0E0E0" BackColor="Gray"></InstructionTextStyle>
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup" Width="348px"/>
            <div class="accountInfo">
                <fieldset class="login" style="width:100%;">
                    <legend>Account Information</legend>
                        <table class="formview-table-layout">
                            <tr>
                                <td><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label></td>
                                <td><asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                     CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                                     ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label></td>
                                <td><asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                     CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                                     ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><asp:CheckBox ID="RememberMe" runat="server"/>
                                <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me logged in</asp:Label></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="LoginUserValidationGroup" Width="80" Height="28" />
                                
                                <br />
                                <br />
                                Forgot your password? <asp:HyperLink ID="ResetPasswordHyperlink" runat="server" NavigateUrl="~/Account/PasswordReset.aspx" Text="Reset it"></asp:HyperLink><br />
                                </td>
                                
                                <td>&nbsp;</td>
                            </tr>
                        </table>
       
                </fieldset>
                <p class="submitButton">
                    
                </p>
            </div>
            
        </LayoutTemplate>
    </asp:Login>
    </p>
	</div><!-- end #content -->
    <div class="clear"></div>

</asp:Content>
