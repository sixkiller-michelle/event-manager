﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="EventManager.Web.Account.PasswordResetForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
table td {padding:5px; text-align:left;}
</style>
</asp:Content>
<asp:Content ID="MenuContent" runat="server" ContentPlaceHolderID="MenuContent">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Password Reset</h1>

    <div style="width:400px; margin: 0 auto; text-align:left;">
        <asp:PasswordRecovery ID="RecoverPwd" runat="server" 
            onsendingmail="RecoverPwd_SendingMail" 
            style="text-align: left; font-size: large;" BackColor="#EFF3FB" 
            BorderColor="#B5C7DE" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
            Font-Names="Verdana" Font-Size="0.8em">
            <SubmitButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" 
                HorizontalAlign="Left" />
            <MailDefinition BodyFileName="~/EmailTemplates/PasswordReset.txt"></MailDefinition>
            <SuccessTextStyle Font-Bold="True" Font-Size="Medium" ForeColor="#507CD1" />
            <TextBoxStyle Font-Size="0.8em" Width="250px" />
            <TitleTextStyle BackColor="#e99719" Font-Bold="True" Font-Size="0.9em" 
                ForeColor="White" HorizontalAlign="Left" />
        </asp:PasswordRecovery>
        <br />
        <br />
        <br />
        <br />
    </div>


</asp:Content>
