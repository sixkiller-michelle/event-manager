﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Eula.aspx.cs" Inherits="EventManager.Web.EulaForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="color: rgb(0, 0, 0); font-family: Verdana; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
        End User License Agreement</h1>
    <h3 style="color: rgb(0, 0, 0); font-family: Verdana; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
        Read Carfully</h3>
    <div style="color: rgb(0, 0, 0); font-family: Verdana; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-align: left;">
        <p>
            A. Agreement. By accessing or otherwise using this website, you agree to be 
            bound by the terms of this End User License Agreement (this &quot;EULA&quot;).<span 
                class="Apple-converted-space">&nbsp;</span><strong>If you do not agree to the 
            terms of this EULA, do not use or access this website.</strong></p>
        <p>
            B. What this EULA Covers. This EULA covers the CeuAvenue™ Web-based software 
            program and may include associated media, hardware, printed materials, and 
            electronic documentation (collectively, the &quot;Web-based Program&quot;). The Web-based 
            Program also includes any updates and supplements to the original Web-based 
            Program provided to you by Sixkiller Software, LLC. (&quot;Developer&quot;) through this 
            site.</p>
        <p>
            C. Meaning of &quot;You.&quot; If the person accessing this Web-based Program is an 
            individual using it for his or her own purposes, &quot;you&quot; means the person using 
            this software. If the person accessing this Web-based Program is doing so in 
            furtherance of the purposes of another person, including an organization 
            (whether or not the employer or the person) then &quot;you&quot; means both the person 
            accessing this software and the other person, including an organization. No 
            person accessing the software for another may do so unless the person accessing 
            the software is authorized to bind the other person, including an organization, 
            and failure to be so authorized will result in unauthorized use of the software 
            subject to civil and criminal consequences.</p>
        <p>
            D. Meaning of &quot;Association&quot;.&nbsp;
        </p>
        <p>
            E. The Parties to this EULA. This EULA is a legal agreement between you and 
            Sixkiller Software, LLC, an Idaho company with offices at 223 N 6th St., Suite 
            200, Boise, ID 83702, USA.</p>
        <p>
            F. Nature of this EULA. The Web-based Program is protected by copyright laws and 
            international copyright treaties, as well as other intellectual property laws 
            and treaties. The Web-based Program is licensed, not sold.</p>
        <p style="text-align: center;">
            1. GRANT OF LICENSE.</p>
        <p>
            Developer grants to you a nonexclusive license to use the Web-based Program as 
            provided in the accompanying publications or documentation. This license permits 
            one association to access and use the Web-based Program. This is not a seat- or 
            session-based license. Once an association begins use of the Web-based Program, 
            the license is permanently associated with that named association and no other 
            association may use the Web-based Program without procuring from Developer 
            another license for that other association.</p>
        <p>
            YOU MAY NOT REGISTER TO USE THIS WEB-BASED PROGRAM, OR ALLOW ANY OTHER PERSON TO 
            USE THE WEB-BASED PROGRAM UNDER YOUR REGISTRATION, IF YOU OR SUCH OTHER PERSON 
            OWN(S) OR REPRESENT(S) A WEBSITE THAT COMPETES WITH THE WEB-BASED PROGRAM. AS 
            USED HEREIN, THE TERM &quot;REPRESENT&quot; MEANS THAT YOU (OR SUCH OTHER PERSON) ARE/IS 
            AN EMPLOYEE OF, CONSULTANT TO, OR HAVE/HAS A CONTRACTUAL RELATIONSHIP TO PROVIDE 
            GOODS OR SERVICES TO THE PERSON OR ENTITY THAT OWNS OR MANAGES THE COMPETING 
            WEBSITE. A COMPETING WEBSITE IS ONE THAT ALLOWS USERS TO TRADE REAL ESTATE FOR 
            OTHER REAL ESTATE ONLINE.</p>
        <p>
            You may make the URL and Web-based Program available to natural persons, 
            provided that one such license is bound to one person and that each person who 
            uses the Web-based Program made available by you agrees to this EULA. If any 
            person to whom you make available the Web-based Program is not bound by the 
            EULA, you will be responsible for any violations of this EULA or of applicable 
            law by that person as though that person was your agent acting on your express 
            instructions.</p>
        <p>
            If you distribute the URL or Web-based Program to natural persons using a 
            centralized server, web application, or otherwise, you must have in place 
            measures reasonably calculated to limit the number of natural persons using the 
            Web-based Program to the number of licenses granted by LAT. In any case, you 
            must make available your books, records, facilities, and systems for audit by 
            LAT and/or its agents so that LAT can verify your compliance with the terms of 
            this EULA.</p>
        <p style="text-align: center;">
            2. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.</p>
        <p>
            Individual User Accounts. Every person who accesses the Web-based Program must 
            do so with a single named user account that is personally associated and only 
            used by him or her.</p>
        <p>
            Registration Requirements. In consideration of your use of this website and the 
            Web-based Program, you agree to (a) provide true, accurate, current and complete 
            information about yourself as prompted by our registration form (such 
            information being &quot;Your Data&quot;). If you provide any information that is untrue, 
            inaccurate, not current or incomplete, or LAT has reasonable grounds to suspect 
            that such information is untrue, inaccurate, not current or incomplete, LAT has 
            the right to suspend or terminate your account and refuse any and all current or 
            future use of the website or Web-based Program, or any portion thereof.</p>
        <p>
            Limitations on Reverse Engineering, Decompilation, and Disassembly. You may not 
            reverse engineer, decompile, or disassemble the Web-based Program.</p>
        <p>
            Separation of Components. The Web-based Program is licensed as a single product. 
            Its component parts may not be separated for use on more than one computer and 
            must be presented to the user in the whole form as distributed by LAT or its 
            designated suppliers.</p>
        <p>
            Rental or Sale. Without exception, you may not rent, sell, lease, lend or 
            provide commercial or fee-based hosting services for the Web-based Program or 
            otherwise charge any type of access fee to users in exchange for access to the 
            Web-based Program.</p>
        <p>
            Trademarks. This EULA does not grant you any rights in connection with any 
            trademarks or service marks of LAT.</p>
        <p>
            No Support. LAT shall have no obligation to provide any product support for the 
            Web-based Program unless LAT expressly agrees in writing to provide such 
            support.</p>
        <p>
            Termination. Without prejudice to any other rights, LAT may terminate this EULA 
            if you fail to comply with the terms and conditions of this EULA. In such event, 
            you must immediately stop accessing the Web-based Program and destroy all copies 
            of the Web-based Program and all of its component parts in your possession or 
            under your control.</p>
        <p style="text-align: center;">
            3. COPYRIGHT.</p>
        <p>
            Except for the limited license rights granted in this EULA, all right, title, 
            and interest in and to the Web-based Program (including, but not limited to, any 
            images, photographs, animations, video, audio, music, text, computer code or 
            applets incorporated into the Web-based Program), the accompanying printed 
            materials, if any, and any copies of the Web-based Program are owned by LAT. All 
            rights in and to the content that may be accessed through use of the Web-based 
            Program is the property of the respective content owners and may be protected by 
            applicable copyright or other intellectual property laws and treaties. This EULA 
            grants no rights to use such content. All rights not expressly granted are 
            reserved by LAT and/or the owner(s) of the Web-based Program or other materials.</p>
        <p style="text-align: center;">
            4. U.S. GOVERNMENT RESTRICTED RIGHTS.</p>
        <p>
            The Web-based Program and documentation are provided with RESTRICTED RIGHTS. 
            Use, duplication, or disclosure by the Government is subject to restrictions as 
            set forth is subparagraph (c)(1)(ii) of the Rights in Technical Data and 
            Computer Software clause at DFARS 252.227-7013 or subparagraphs (c)(1) and (2) 
            of the Commercial Computer Software-Restricted Rights at 48 CFR 52.227-19, as 
            applicable. Manufacturer is LAT, Inc., 112 South Broadway, Suite B, Manhattan, 
            MT 59741, USA.</p>
        <p style="text-align: center;">
            5. EXPORT RESTRICTIONS.</p>
        <p>
            You may not export or re-export the Web-based Program, any part thereof, or any 
            process or service that is the direct product of the Web-based Program (the 
            foregoing collectively referred to as the &quot;Restricted Components&quot;), to any 
            country, person, entity, or end user subject to U.S. export restrictions. You 
            specifically agree not to export or re-export any of the Restricted Components 
            (i) to any country to which the U.S. has embargoed or restricted the export of 
            goods or services or to any national of any such country, wherever located, who 
            intends to transmit or transport the Restricted Components back to such country; 
            (ii) to any end user who you know or have reason to know will utilize the 
            Restricted Components in the design, development, or production of nuclear, 
            chemical, or biological weapons; or (iii) to any end user who has been 
            prohibited from participating in U.S. export transactions by any federal agency 
            of the U.S. Government. You represent and warrant that no governmental authority 
            has suspended, revoked, or denied your export privileges.</p>
        <p style="text-align: center;">
            6. LIMITED WARRANTY AND EXCLUSIVE REMEDIES</p>
        <p>
            Limited Warranty. LAT warrants solely to the person who directly acquires each 
            license from LAT (and not to any transferee of the person who directly acquires 
            each license from LAT) that the Web-based Program will perform substantially in 
            accordance with the accompanying written materials for a period of 30 days after 
            the date of receipt by you. Where the Web-based Program is provided to you for 
            use by others, the warranty period begins to run when you first receive access 
            to the Web-based Program, regardless of whether and/or when any other person 
            uses the Web-based Program.</p>
        <p>
            Limited Remedies. LAT and its suppliers&#39; entire liability and your exclusive 
            remedy shall be, at LAT&#39;s option, either (a) return of the price paid, if any, 
            or (b) repair or replacement of the Web-based Program that does not meet the 
            limited warranty. This limited warranty is void if failure of the Web-based 
            Program has resulted from accident, abuse, misapplication, or failure to follow 
            the instructions or documentation. Any replacement Web-based Program will be 
            warranted for the remainder of the original warranty period or 30 days after 
            receipt by you, whichever is longer. Outside the United States, neither these 
            remedies nor any product support services offered by LAT are available without 
            proof of purchase from an authorized international source.</p>
        <p>
            No Other Warranties. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, EXCEPT 
            AS OTHERWISE EXPRESSLY PROVIDED IN THIS EULA, LAT PROVIDES THE WEB-BASED PROGRAM 
            AND ANY SUPPORT SERVICES WITH ALL FAULTS; THE ENTIRE RISK AS TO SATISFACTORY 
            QUALITY, PERFORMANCE, ACCURACY, AND EFFORT IS WITH YOU; AND LAT MAKES NO 
            WARRANTY AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF THE WEB-BASED PROGRAM OR 
            SUPPORT SERVICES, AGAINST INFRINGEMENT, OR OF FITNESS FOR ANY PARTICULAR PURPOSE 
            OR OF MERCHANTABILITY.</p>
        <p>
            Limitation of Liability. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN 
            NO EVENT SHALL LAT OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, 
            INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, 
            DAMAGES FOR OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE 
            THE WEB-BASED PROGRAM OR THE FAILURE TO PROVIDE SUPPORT SERVICES, EVEN IF LAT 
            HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, LAT&#39;S ENTIRE 
            LIABILITY UNDER ANY PROVISION OF THIS EULA SHALL BE LIMITED TO THE GREATER OF 
            THE AMOUNT ACTUALLY PAID BY YOU FOR THE WEB-BASED PROGRAM LICENSE OR $5.00;</p>
        <p>
            Magnuson-Moss Warranty Act and Similar Law. It is the understanding of LAT that 
            the Web-based Program, as a software product, is not subject to the 
            Magnuson-Moss Warranty Act or any other similar state or federal law. Solely in 
            the case that such law is held to apply to the Web-based Program:</p>
        <ol>
            <li>This limited warranty gives you specific legal rights. You may have other rights 
                that vary from state/jurisdiction to state/jurisdiction;</li>
            <li>Some states/jurisdictions do not allow limitations on duration of an implied 
                warranty, so the limitations above may not apply to you;</li>
            <li>Because some states/jurisdictions do not allow the exclusion or limitation of 
                certain finds of liability, some or all of the above limitations on liability 
                may not apply to you.</li>
        </ol>
        <p style="text-align: center;">
            7. GOVERNING LAW; VENUE</p>
        <p>
            This EULA is governed by the laws of the State of Montana and the federal law of 
            the United States of America, without regard for their conflict of law 
            principles. If you bring any action or claim arising under or related to this 
            EULA, you may do so only in the Montana state courts sitting in Gallatin County, 
            Montana or in the United States District Court for the District of Montana.</p>
        <p style="text-align: center;">
            8. ENTIRE AGREEMENT; SEVERABILITY</p>
        <p>
            This EULA (including any addendum or amendment to this EULA that is included 
            with the Web-based Program) is the entire agreement between you and LAT relating 
            to the Web-based Program and the support services (if any), and they supersede 
            all prior or contemporaneous oral or written communications, proposals and 
            representations with respect to the Web-based Program or any other subject 
            matter covered by this EULA. To the extent the terms of any LAT policies or 
            programs for support services conflict with the terms of this EULA, the terms of 
            this EULA shall control. If any provision of this EULA is held to be void, 
            invalid, unenforceable or illegal, the other provisions shall continue in full 
            force and effect.</p>
        <p>
            LAT reserves the right to change this licensing agreement at any time. The 
            current version of this agreement shall be posted at all times at 
            http://www.tradehome.com/eula.php.</p>
        <p>
            YOU ACKNOWLEDGE THAT YOU HAVE READ THIS AGREEMENT, YOU UNDERSTAND THIS 
            AGREEMENT, AND YOU UNDERSTAND THAT BY USING THE WEB-BASED PROGRAM, YOU AGREE TO 
            BE BOUND BY THE TERMS AND CONDITIONS OF THIS EULA. YOU FURTHER AGREE THAT, 
            EXCEPT FOR SEPARATE WRITTEN AGREEMENTS, IF ANY, BETWEEN LAT, INC. AND YOU SIGNED 
            BY BOTH LAT, INC. AND YOU, THIS AGREEMENT IS A COMPLETE AND EXCLUSIVE STATEMENT 
            OF THE RIGHTS AND LIABILITIES OF THE PARTIES.</p>
        <p>
            © 2009 LAT, INC. ALL RIGHTS RESERVED.</p>
    </div>
</asp:Content>
