namespace EventManager.Web.Association.Reporting
{
    partial class AttendeeBadges117
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttendeeBadges117));
            this.detail = new Telerik.Reporting.DetailSection();
            this.attendeeFirstNameDataTextBox = new Telerik.Reporting.TextBox();
            this.orgNameDataTextBox = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
            this.objectDataSource2 = new Telerik.Reporting.ObjectDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.ColumnSpacing = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.attendeeFirstNameDataTextBox,
            this.orgNameDataTextBox,
            this.pictureBox1,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.barcode1});
            this.detail.Name = "detail";
            // 
            // attendeeFirstNameDataTextBox
            // 
            this.attendeeFirstNameDataTextBox.CanGrow = false;
            this.attendeeFirstNameDataTextBox.CanShrink = false;
            this.attendeeFirstNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D));
            this.attendeeFirstNameDataTextBox.Name = "attendeeFirstNameDataTextBox";
            this.attendeeFirstNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.39999982714653015D));
            this.attendeeFirstNameDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.attendeeFirstNameDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.attendeeFirstNameDataTextBox.Value = "=Fields.AttendeeFirstName";
            // 
            // orgNameDataTextBox
            // 
            this.orgNameDataTextBox.CanGrow = false;
            this.orgNameDataTextBox.CanShrink = false;
            this.orgNameDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.399842381477356D));
            this.orgNameDataTextBox.Name = "orgNameDataTextBox";
            this.orgNameDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.orgNameDataTextBox.Style.Font.Bold = true;
            this.orgNameDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.orgNameDataTextBox.Value = "=Fields.OrgName";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.099921070039272308D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.99992126226425171D));
            this.textBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(35)))), ((int)(((byte)(24)))));
            this.textBox1.Style.Font.Name = "Stencil";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Value = "BOOT CAMP\r\nFOR \r\nLONG-TERM\r\nCARE";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(1.5999212265014648D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "= IIF(Trim(Fields.Title) = \"\", Fields.AttendeeNameFNF, Fields.AttendeeNameFNF + \"" +
                ", \" + Fields.Title)";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Segoe Script";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox3.Value = "... transforming the profession";
            // 
            // barcode1
            // 
            this.barcode1.Checksum = true;
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D), Telerik.Reporting.Drawing.Unit.Inch(2.2000787258148193D));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.69992160797119141D));
            this.barcode1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.barcode1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.barcode1.Symbology = Telerik.Reporting.Barcode.SymbologyType.Code39;
            this.barcode1.Value = "= Fields.AttendeeId";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "Attendee";
            this.objectDataSource1.DataSource = typeof(EventManager.Web.Association.Reports2.AttendeeBadgesDataSet);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.Name = "objectDataSource2";
            // 
            // AttendeeBadges117
            // 
            this.DataSource = this.objectDataSource1;
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "AttendeeBadges117";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.99960631132125854D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0.25D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0.24960629642009735D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(4.0103774070739746D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox attendeeFirstNameDataTextBox;
        private Telerik.Reporting.TextBox orgNameDataTextBox;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.ObjectDataSource objectDataSource2;
        private Telerik.Reporting.Barcode barcode1;

    }
}