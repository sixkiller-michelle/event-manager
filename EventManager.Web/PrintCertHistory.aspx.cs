﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Web.UI;

namespace EventManager.Web
{
    public partial class PrintCertHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int attendeeId = 0;
                if (Int32.TryParse(Request.QueryString["id"], out attendeeId))
                {
                    EventManager.Business.AttendeeMethods m = new EventManager.Business.AttendeeMethods();
                    EventManager.Model.Attendee a = m.GetAttendee(attendeeId);

                    if (a != null)
                    {
                        AttendeeNameLabel.Text = a.FullNameFNF;
                        AttendeeTitleLabel.Text = a.Title;

                        EventHistoryRadGrid.DataSource = m.GetCertificatePrintList(a.Id);
                        EventHistoryRadGrid.DataBind();

                        ErrorPanel.Visible = false;
                        EventsPanel.Visible = true;
                    }
                    else
                    {
                        ErrorPanel.Visible = true;
                        EventsPanel.Visible = false;
                    }
                }
                else
                {
                    ErrorPanel.Visible = true;
                    EventsPanel.Visible = false;
                }
            }
        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "PrintCertificate")
            {
                int assnId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AssociationId"]);
                int eventId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EventId"]);
                int attendeeId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["AttendeeId"]);
                string certificateReportName = "";

                EventManager.Business.AssociationMethods m = new EventManager.Business.AssociationMethods();
                if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForEvent(eventId))))
                    certificateReportName = m.GetCertificateFileNameForEvent(eventId);
                else if (File.Exists(Server.MapPath("~/Association/Reports2/" + m.GetCertificateFileNameForAssociation(assnId))))
                    certificateReportName = m.GetCertificateFileNameForAssociation(assnId);
                else
                    certificateReportName = m.GetCertificateFileNameForApplication();

                if (certificateReportName != "")
                {
                    Response.Redirect("~/Association/Reports2/ReportViewer.aspx?FileName=" + certificateReportName + "&ParmNames=@EventId|@AttendeeId&ParmValues=" + eventId + "|" + attendeeId);
                }
            }
        }

        protected void RadGrid1_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            if (dataItem is GridDataItem)
            {
                EventManager.Model.RptAttendeeCertificateList_Result r = (EventManager.Model.RptAttendeeCertificateList_Result)e.Item.DataItem;
                if (!r.IsDateValid)
                    dataItem.Cells[5].ForeColor = System.Drawing.Color.Red;

                if (!r.IsPaid && r.IsPaymentRequired)
                {
                    dataItem.Cells[6].ForeColor = System.Drawing.Color.Red;
                }

                if (!r.IsSurveyComplete && r.IsSurveyRequired)
                {
                    dataItem.Cells[7].ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink lnk = dataItem["SurveyHyperlinkColumn"].Controls[0] as HyperLink;
                    lnk.Visible = false;
                }

                if (!r.IsDateValid || (!r.IsPaid && r.IsPaymentRequired) || (!r.IsSurveyComplete && r.IsSurveyRequired))
                {
                    LinkButton btn = dataItem["PrintCertGridButtonColumn"].Controls[0] as LinkButton;
                    btn.Visible = false;
                }
            }
        }
    }
}