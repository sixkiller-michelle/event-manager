﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EventManager.Model;

namespace EventManager.Model
{
	[MetadataType(typeof(EventFlagMetadata))]
	public partial class EventFlag
	{
        //partial void OnFlagNameChanging(global::System.String value)
        //{
        //    if (string.IsNullOrEmpty(value))
        //        throw new ValidationException("Flag name is required.");
        //}

        //partial void OnFlagCodeChanging(string value)
        //{
        //    if (string.IsNullOrEmpty(value))
        //        throw new ValidationException("Flag code is required.");
        //}
	}

	public class EventFlagMetadata
	{

		[Required(ErrorMessage = "Flag name is required.")]
		[StringLength(10, ErrorMessage = "Flag name must be 50 characters or less.")]
		public String FlagName { get; set; }

		[Required(ErrorMessage = "Flag code is required.")]
		[StringLength(10, ErrorMessage="Flag code must be 10 characters or less.")]
		public String FlagCode { get; set; }
	} 
}
