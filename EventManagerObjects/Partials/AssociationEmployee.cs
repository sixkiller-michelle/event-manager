﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventManager.Model
{
    public partial class AssociationEmployee
    {
        public string FullName
        {
            get
            {
                string n = this.LastName + ", " + this.FirstName;
                return n;
            }
        }

        public string UserName
        {
            get
            {
                string n = "";
                if (this.aspnet_Users != null)
                    n = this.aspnet_Users.UserName;
                return n;
            }
        }

        public bool IsEmployee
        {
            get
            {
                bool hasRole = false;
                if (this.UserId != null)
                {
                    if (this.aspnet_Users.aspnet_Roles.Where(r => r.RoleName == "AssociationEmployee").Count() > 0)
                    {
                        hasRole = true;
                    }
                }
                return hasRole;
            }
        }

        public bool IsAdmin
        {
            get
            {
                bool hasRole = false;
                if (this.UserId != null)
                {
                    if (this.aspnet_Users.aspnet_Roles.Where(r => r.RoleName == "AssociationAdmin").Count() > 0)
                    {
                        hasRole = true;
                    }
                }
                return hasRole;
            }
        }

    }
}
