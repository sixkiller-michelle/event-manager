﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model
{
    [MetadataType(typeof(EventMetadata))]
	public partial class Event
	{
		public string FullLocation
		{
			get
			{
				string n = this.Location + " - " + this.City + ", " + this.StateCode;
				return n;
			}
		}

        public DateTime LatestSessionStartDateTime
        {
            get
            {
                if (this.EventSessions.Count() > 0)
                    return this.EventSessions.Max(e => (DateTime)e.StartDateTime);
                else
                    return new DateTime(this.StartDateTime.Year, this.StartDateTime.Month, this.StartDateTime.Day, 8, 0, 0);
            }
        }

        public bool IsRegistrationOpen
        {
            // Registration is open today's date falls between OnlineRegStartDateTime AND OnlineRegEndDateTime
            get
            {
                bool isOpen = false;

                if (DateTime.Now <= this.OnlineRegEndDateTime && DateTime.Now >= this.OnlineRegStartDateTime)
                {
                    isOpen = true;
                }
                
                return isOpen;
            }
        }

        public int SessionCount
        { 
            get
            {
                return this.EventSessions.Count;
            }
            
        }

        public int FacilityCount
        {
            get
            {

                return this.Registrations.Where(r => r.OrganizationId != null).Count();
            }

        }


        public int AttendeeCount
        {
            get
            {

                return this.Registrations.Sum(r => r.NumberOfRegistrants);
            }

        }

        public int SpeakerCount
        {
            get
            {

                return this.EventSessions.Sum(r => r.SpeakerCount);
            }

        }

        public bool IsCertificatePrintingAllowed
        {
            // Printing is allowed if today falls between cert print dates
            get
            {
                bool print = false;
                if (DateTime.Now > this.CertificatesAvailableStartDate && DateTime.Now < this.CertificatesAvailableEndDate)
                {
                    print = true;
                }
              
                return print;
            }
        }

        public int LagDays
        {
            get
            {
                return this.StartDateTime.Subtract(DateTime.Today).Days;
            }
        }

        public string LagTimeString
        {
            get
            {
                int days = Math.Abs(LagDays);
                if (days < 14)
                {
                    return days.ToString() + " days";
                }
                else if (days >= 14 && days < 49)
                {
                    return (days / 7).ToString() + " weeks";
                }
                else if (days >= 49 && days < 340)
                {
                    return (days / 30).ToString() + " months";
                }
                else if (days >= 340 && days < 400)
                {
                    return "1 year";
                }   
                else
                {
                    return "over a year";
                }
            }
        }

        public bool IsValid()
        {
            return (GetRuleViolations().Count() == 0);
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {

            if (String.IsNullOrEmpty(EventName))
                yield return new RuleViolation("Event Name is required", "EventName");
            if (StartDateTime == DateTime.MinValue)
                yield return new RuleViolation("Start date is required.", "StartDateTime");
            if (EndDateTime == DateTime.MinValue)
                yield return new RuleViolation("End date is required.", "EndDateTime");
            if (DateTime.Compare(Convert.ToDateTime(StartDateTime), Convert.ToDateTime(EndDateTime)) == 0)
                yield return new RuleViolation("Start date must be before end date.", "StartDateTime");

            yield break;
        }
         
	}

    public class EventMetadata
    {
        [DisplayName("Event Name")] 
        [Required(ErrorMessage = "Event name is required.")]
        [StringLength(100, ErrorMessage = "Event name must be less than 100 characters.")]
        public string EventName { get; set; }

        [DisplayName("Location")]
        [Required(ErrorMessage = "Location is required.")]
        [StringLength(50, ErrorMessage = "Location must be less than 50 characters.")]
        public string Location { get; set; }

        [DisplayName("City")]
        [Required(ErrorMessage = "City is required.")]
        [StringLength(40, ErrorMessage = "City must be less than 40 characters.")]
        public string City { get; set; }

        [DisplayName("State")]
        [Required(ErrorMessage = "State is required.")]
        [StringLength(2, ErrorMessage = "State code must be 2 characters.")]
        public string StateCode { get; set; }

        [DisplayName("Type")]
        [Required(ErrorMessage = "Event type is required.")]
        public int TypeId { get; set; }

        [DisplayName("Status")]
        [Required(ErrorMessage = "Event status is required.")]
        public int EventStatusId { get; set; }

        [DisplayName("Board Meeting Room")]
        [StringLength(50, ErrorMessage = "Board meeting room must be less than 50 characters.")]
        public string BoardMeetingRoom { get; set; }

       
    } 

}
