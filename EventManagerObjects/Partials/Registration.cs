﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model
{
    [MetadataType(typeof(RegistrationMetadata))]
    public partial class Registration
    {
        public string PricingCalculation
        {
            get
            {
                string c = "";
                int attendeeFlagCount;

                // Base price
                //c = "Base: " + String.Format("{0:C}", this.BasePrice);

                //// Get flag (add-on) prices 
                //foreach (EventFlag f in this.Event.EventFlags)
                //{
                //    attendeeFlagCount = 0;
                //    if (f.Price != 0)
                //    { 
                //        foreach (RegistrationAttendee ra in this.RegistrationAttendees)
                //        {
                //            if (ra.HasFlag(f.Id))
                //                attendeeFlagCount = attendeeFlagCount + 1;
                //        }
                //    }
                //    c = c + ", " + f.FlagCode + ": " + String.Format("{0:C}", (attendeeFlagCount * f.Price)) + " (" + attendeeFlagCount.ToString() + " @ " + String.Format("{0:C}", f.Price) + ")";
                //}

                //// Get extra attendee prices
                //foreach (RegistrationAttendee a in this.RegistrationAttendees)
                //{
                //    foreach (RegistrationAttendeeFlag f in a.RegistrationAttendeeFlags)
                //    {
                //        c = c + ", " + a.Attendee.FirstName + " " + a.Attendee.LastName + ": " + String.Format("{0:C}", a.Price) + "(" + a.FlagListWithPrices + "), ";
                //    }
                //}
                return c;
            }
        }

        public bool HasAttendeeNotes
        { 
            get 
            {
                int attendeesWithNotes = 0;
                attendeesWithNotes = this.RegistrationAttendees.Where(n => !string.IsNullOrEmpty(n.Notes)).Count();
                return (attendeesWithNotes > 0);
            }
            
        }

        public decimal ChargeTotal
        {
            get
            {
                decimal c = 0;

                foreach (RegistrationCharge a in this.RegistrationCharges)
                {
                    c = c + a.ChargeAmt;
                }
                return c;
            }
        }



        public decimal PaymentTotal
        {
            get
            {
                decimal c = 0;

                foreach (RegistrationPayment a in this.RegistrationPayments)
                {
                    c = c + a.PaymentAmt;
                }
                return c;
            }
        }

        public decimal Balance
        {
            get
            {
                decimal c = 0;

                c = ChargeTotal - PaymentTotal;
                return c;
            }
        }

        public string PaymentType
        {
            get
            {
                if (this.RegistrationPayments.Count == 1)
                    return this.RegistrationPayments.FirstOrDefault().PaymentType;
                else if (this.RegistrationPayments.Count == 0)
                    return "";
                else
                    return "[multiple]";
            }
        }

        public string PaymentNumber
        {
            get
            {
                if (this.RegistrationPayments.Count == 1)
                    return this.RegistrationPayments.FirstOrDefault().PaymentNumber;
                else
                    return "";
            }
        }

        public string RegistrationDesc
        {
            get
            {
                if (this.OrganizationId == null)
                {
                    if (RegistrationAttendees.Count > 0)
                    {
                        return "* Individual registration (" + this.RegistrationAttendees.First().Attendee.FullName + ")";
                    }
                    else
                    {
                        return "* Individual registration (no attendees)";
                    }
                }
                else
                {
                    return Organization.OrgName;
                }
            }
        }

        public int NumberOfRegistrants
        {
            get
            {
                return this.RegistrationAttendees.Where(a => a.IsCancelled == false).ToList().Count;
            }
        }

        public bool IsIndividualRegistration
        {
            get
            {
                if (this.OrganizationId == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public class RegistrationMetadata
    {
        //[DisplayFormat(DataFormatString = "{0:D}", ApplyFormatInEditMode = true)]
        //public Int32 BedCount { get; set; }

        //[Required(ErrorMessage = "Facility name is required.")]
        //[StringLength(100, ErrorMessage = "Facility name must be less than 100 characters.")]
        //public String OrgName { get; set; }

        //[Required(ErrorMessage = "Last name is required.")]
        //public String LastName { get; set; }
    } 
}
