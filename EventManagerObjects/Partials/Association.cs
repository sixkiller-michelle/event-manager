﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventManager.Model
{
    public partial class Association
    {
        public string FullLocation
        {
            get
            {
                string n = this.Address + "\n" + this.City + ", " + this.StateCode + " " + this.Zip;
                return n;
            }
        }

        public int UserCount
        {
            get
            {
                return this.UserAssociations.Count;
            }
        }

        public int FacilityUserCount
        {
            get
            {
                return this.Attendees.Where(a => a.UserId != null).Count();
            }
        }

        public int AssociationUserCount
        {
            get
            {
                return this.AssociationEmployees.Where(a => a.UserId != null).Count();
            }
        }
    }
}
