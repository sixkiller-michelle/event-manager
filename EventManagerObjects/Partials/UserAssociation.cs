﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventManager.Model
{
    public partial class UserAssociation
    {
        public string Email
        {
            get
            {
                string e = "";
                if (this.aspnet_Users != null)
                    e = this.aspnet_Users.aspnet_Membership.Email;
                return e;
            }
        }

        public string UserName
        {
            get
            {
                string u = "";
                if (this.aspnet_Users != null)
                    u = this.aspnet_Users.UserName;
                return u;
            }
        }

        public bool UsernameEqualsEmail
        { 
            get
            {
                if (this.aspnet_Users != null)
                    return this.aspnet_Users.UserName == this.aspnet_Users.aspnet_Membership.Email;
                else
                    return false;
            }
            
        }


        public bool IsApproved
        {
            get
            {
                bool u = false;
                if (this.aspnet_Users != null)
                    u = this.aspnet_Users.aspnet_Membership.IsApproved;
                return u;
            }
        }

        public bool IsLockedOut
        {
            get
            {
                bool u = false;
                if (this.aspnet_Users != null)
                    u = this.aspnet_Users.aspnet_Membership.IsLockedOut;
                return u;
            }
        }

        public DateTime? CreationDate
        {
            get
            {
                DateTime? u = null;
                if (this.aspnet_Users != null)
                    u = this.aspnet_Users.aspnet_Membership.CreateDate;
                return u;
            }
        }

        public DateTime? LastLoginDate
        {
            get
            {
                DateTime? u = null;
                if (this.aspnet_Users != null)
                    u = this.aspnet_Users.aspnet_Membership.LastLoginDate;
                return u;
            }
        }
    }
}
