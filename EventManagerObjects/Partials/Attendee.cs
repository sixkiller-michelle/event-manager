﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model
{
	[MetadataType(typeof(AttendeeMetadata))]
	public partial class Attendee
	{
       
		public string FullName
		{
			get
			{
				string n;
				if (string.IsNullOrEmpty(this.MiddleInitial))
					n = this.LastName + ", " + this.FirstName;
				else
					n = this.LastName + ", " + this.FirstName + " " + this.MiddleInitial;
				return n;
			}
		}

        public string FullNameWithId
        {
            get
            {
                string n;
                if (string.IsNullOrEmpty(this.MiddleInitial))
                    n = this.LastName + ", " + this.FirstName + " (" + this.Id.ToString() + ") ";
                else
                    n = this.LastName + ", " + this.FirstName + " " + this.MiddleInitial + " (" + this.Id.ToString() + ") ";
                return n;
            }
        }

		public string FullNameFNF
		{
			get
			{
				string n = "";

				// Prefix (Mrs., Mr., Dr., etc)
                if (!string.IsNullOrEmpty(this.Prefix) && this.Prefix.Trim() != "")
                {
					n = this.Prefix + " ";
				}
				if (string.IsNullOrEmpty(this.MiddleInitial) || this.MiddleInitial.Trim() == "")
					n = n + this.FirstName + " " + this.LastName;
				else
					n = n + this.FirstName + " " + this.MiddleInitial + ". " + this.LastName;

				// Credentials (MD, CNP, etc)
				if (!string.IsNullOrEmpty(this.Credentials))
				{
					n = n + ", " + this.Credentials;
				}
				return n;
			}
		}

        public string FullAddress
        {
            get
            {
                string a = "";
                // street
                if (!string.IsNullOrEmpty(this.Address))
                    a = this.Address + "\n";
                if (!string.IsNullOrEmpty(this.City))
                    a = a + this.City + ", ";
                if (!string.IsNullOrEmpty(this.StateCode))
                    a = a + this.StateCode + " ";
                if (!string.IsNullOrEmpty(this.Zip))
                    a = a + this.Zip;

                //a = this.Address + "\n" + this.City + ", " + this.StateCode + " " + this.Zip;
                return a;
            }
        }
		
        //partial void OnFirstNameChanging(string value)
        //{
        //    //if (string.IsNullOrEmpty(value))
        //    //    throw new ValidationException("First Name is required.");
        //}

        //partial void OnLastNameChanging(string value)
        //{
        //    //if (string.IsNullOrEmpty(value))
        //    //    throw new ValidationException("Last Name is required.");
        //}

	}
	
	public class AttendeeMetadata
	{
		//[DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
		//public DateTime EnrollmentDate { get; set; }

		[Required(ErrorMessage = "First name is required.")]
		public String FirstName { get; set; }

		[Required(ErrorMessage = "Last name is required.")]
		public String LastName { get; set; }
	} 

}
