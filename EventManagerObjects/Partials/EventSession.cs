﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventManager.Model;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model
{
    public partial class EventSession
    {
        public string SpeakerString
        {
            get
            {
                string s = "";

                foreach (SessionSpeaker speaker in this.SessionSpeakers)
                {
                    if (s == "")
                        s = speaker.Attendee.FirstName.Trim().Substring(0, 1) + ". " + speaker.Attendee.LastName;
                    else
                        s = s + ", " + speaker.Attendee.FirstName.Trim().Substring(0, 1) + ". " + speaker.Attendee.LastName;
                }
                if (s == "")
                {
                    s = "* None *";
                }
                return s;
            }
        }

        public int SpeakerCount
        {
            get
            {
                return this.SessionSpeakers.Count;
            }

        }

        public string StartTimeAndName
        { 
            get
            {
                return this.StartDateTime.ToString("dddd") + " " + this.StartDateTime.ToString("t") + " - " + this.SessionName;
            }
             
        }

        public string StartTimeDayOfWeek
        {
            get
            {
                return this.StartDateTime.ToString("dddd");
            }
        }
    }
}
