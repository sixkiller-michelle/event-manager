﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model
{
	[MetadataType(typeof(OrganizationMetadata))]
	public partial class Organization
	{
        public string FullAddress
        {
            get
            {
                string a;
                a = this.Address + "\n" + this.City + ", " + this.StateCode + " " + this.Zip;
                return a;
            }
        }

        public bool IsMember
        { 
            get {
                if (this.OrganizationFeeCategory != null)
                    return this.OrganizationFeeCategory.IsMemberCategory;
                else
                    return false;
            }
            
        }

        public string AdminName
        {
            get
            {
                string a = "";
                if (this.Attendee != null)
                    a = this.Attendee.FullName;
                return a;
            }
        }

        public string AdminEmail
        {
            get
            {
                string a = "";
                if (this.Attendee != null)
                    a = this.Attendee.Email;
                return a;
            }
        }

        public string AdminPhone
        {
            get
            {
                string a = "";
                if (this.Attendee != null)
                    a = this.Attendee.PhoneNumber;
                return a;
            }
        }
        public int StaffCount
        {
            get
            {
                int c;
                c = this.Attendees.Count;
                return c;
            }
        }
	}

	public class OrganizationMetadata
	{
		[DisplayFormat(DataFormatString = "{0:D}", ApplyFormatInEditMode = true)]
		public Int32 BedCount { get; set; }

		[Required(ErrorMessage = "Facility name is required.")]
		[StringLength(100, ErrorMessage="Facility name must be less than 100 characters.")]
		public String OrgName { get; set; }

		//[Required(ErrorMessage = "Last name is required.")]
		//public String LastName { get; set; }
	} 
}
