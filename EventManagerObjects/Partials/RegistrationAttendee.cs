﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EventApp;
using System.Data.Objects;

namespace EventManager.Model
{
	public partial class RegistrationAttendee
	{
		public string FlagList
		{
			get
			{
				string s = "";
				using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
				{
					var regFlags = entities.RegistrationAttendeeFlags;

					var query =
					from flag in regFlags
					where flag.RegistrationAttendeeId == this.Id
					select flag;

                    foreach (RegistrationAttendeeFlag flag in query)
					{
						s = (s == "" ? flag.EventFlag.FlagCode.Trim() : s + ", " + flag.EventFlag.FlagCode.Trim());
					}
				}
				return s;
			}

		}

        public string FlagListForBadge
        {
            get
            {
                string s = "";
                using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
                {
                    var regFlags = entities.RegistrationAttendeeFlags;

                    var query =
                    from flag in regFlags
                    where flag.RegistrationAttendeeId == this.Id && flag.EventFlag.PrintOnBadge == true
                    select flag;

                    foreach (RegistrationAttendeeFlag flag in query)
                    {
                        s = (s == "" ? flag.EventFlag.FlagCode.Trim() : s + ", " + flag.EventFlag.FlagCode.Trim());
                    }
                }
                return s;
            }

        }

        public string FlagListWithPrices
        {
            get
            {
                string s = "";
                using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
                {
                    var regFlags = entities.RegistrationAttendeeFlags;

                    var query =
                    from flag in regFlags
                    where flag.RegistrationAttendeeId == this.Id
                    select flag;

                    foreach (RegistrationAttendeeFlag flag in query)
                    {
                        if (String.IsNullOrWhiteSpace(s))
                            s = flag.EventFlag.FlagName.Trim() + "(" + string.Format("{0:C}", flag.EventFlag.Price) + ")";
                        else
                            s = s + ", " + flag.EventFlag.FlagName.Trim() + "(" + string.Format("{0:C}", flag.EventFlag.Price) + ")";
                    }
                }
                return s;
            }

        }

        public decimal CeuTotal
        {
            get
            {
                //decimal? total = 0;
                if (this.SessionAttendances.Count == 0)
                    return 0;
                else
                    return this.SessionAttendances.Sum(f => f.EventSession.CEUHours);

                //using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
                //{
                //    foreach (SessionAttendance a in this.SessionAttendances)
                //    {
                //        total = total + a.EventSession.CEUHours;
                //    }
                //}
                //return total;
            }
        }

        public bool HasFlag(int flagId)
        {
            if (this.RegistrationAttendeeFlags.Where(f => f.EventFlagId == flagId).Count() > 0)
                return true;
            else
                return false;
        }

        public bool IsSpeakerOld
        {
            get
            {
                using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
                {
                    int eventId = this.Registration.EventId;
                    int attendeeId = this.AttendeeId;

                    // Look for an attendance record for person that is flagged as speaker
                    var attendance = entities.SessionAttendances;

                    var query =
                    from att in attendance
                    where att.RegistrationAttendeeId == this.Id && att.IsSpeaker == true
                    select att.Id;

                    return (query.Count() > 0);
                }
                
            }
        }

        public bool IsSpeaker
        {
            get
            {
                if (this.Registration != null)
                {
                    int eventId = this.Registration.EventId;
                    int attendeeId = this.AttendeeId;

                    using (EventManager.Model.Entities entities = new EventManager.Model.Entities())
                    {
                        // Look for an attendance record for person that is flagged as speaker
                        //var sessions = this.Registration.Event.EventSessions;

                        var query =
                        from sp in entities.SessionSpeakers.Where(sp => sp.SpeakerId == attendeeId && sp.EventSession.EventId == eventId)
                        select sp.Id;

                        return (query.Count() > 0);
                    }
                }
                else
                { 
                     return false;
                }
            }
        }
	}
}