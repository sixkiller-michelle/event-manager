﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventApp.Security
{
    public class UserSession
    {
        public UserSession()
        {    }

        public String UserName
        {
            get { return _userName; }
            set { _userName = value; } 
        }

        #region Private Properties

        private string _userName = "";

        #endregion

    }
}