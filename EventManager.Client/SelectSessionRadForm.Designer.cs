﻿namespace EventManager.Client
{
    partial class SelectSessionRadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.OkRadButton = new Telerik.WinControls.UI.RadButton();
            this.label12 = new System.Windows.Forms.Label();
            this.SessionsDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.EventNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OkRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SessionsDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelRadButton
            // 
            this.CancelRadButton.Location = new System.Drawing.Point(91, 141);
            this.CancelRadButton.Name = "CancelRadButton";
            this.CancelRadButton.Size = new System.Drawing.Size(68, 24);
            this.CancelRadButton.TabIndex = 31;
            this.CancelRadButton.Text = "Cancel";
            this.CancelRadButton.Click += new System.EventHandler(this.CancelRadButton_Click);
            // 
            // OkRadButton
            // 
            this.OkRadButton.Location = new System.Drawing.Point(17, 141);
            this.OkRadButton.Name = "OkRadButton";
            this.OkRadButton.Size = new System.Drawing.Size(68, 24);
            this.OkRadButton.TabIndex = 30;
            this.OkRadButton.Text = "Ok";
            this.OkRadButton.Click += new System.EventHandler(this.OkRadButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Session:";
            // 
            // SessionsDropDownList
            // 
            this.SessionsDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionsDropDownList.DropDownAnimationEnabled = true;
            this.SessionsDropDownList.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.SessionsDropDownList.Location = new System.Drawing.Point(17, 94);
            this.SessionsDropDownList.MaxDropDownItems = 0;
            this.SessionsDropDownList.Name = "SessionsDropDownList";
            this.SessionsDropDownList.ShowImageInEditorArea = true;
            this.SessionsDropDownList.Size = new System.Drawing.Size(759, 23);
            this.SessionsDropDownList.TabIndex = 28;
            // 
            // EventNameLabel
            // 
            this.EventNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventNameLabel.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.EventNameLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.EventNameLabel.Location = new System.Drawing.Point(17, 13);
            this.EventNameLabel.Name = "EventNameLabel";
            this.EventNameLabel.Size = new System.Drawing.Size(759, 62);
            this.EventNameLabel.TabIndex = 32;
            this.EventNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SelectSessionRadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 188);
            this.Controls.Add(this.EventNameLabel);
            this.Controls.Add(this.CancelRadButton);
            this.Controls.Add(this.OkRadButton);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.SessionsDropDownList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SelectSessionRadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Select Session";
            this.ThemeName = "ControlDefault";
            this.Load += new System.EventHandler(this.SelectSessionRadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OkRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SessionsDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton CancelRadButton;
        private Telerik.WinControls.UI.RadButton OkRadButton;
        private System.Windows.Forms.Label label12;
        private Telerik.WinControls.UI.RadDropDownList SessionsDropDownList;
        private System.Windows.Forms.Label EventNameLabel;
    }
}
