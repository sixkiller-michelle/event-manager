﻿namespace EventManager.Client
{
    partial class ScanAttendees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EventNameLabel = new System.Windows.Forms.Label();
            this.SessionNameLabel = new System.Windows.Forms.Label();
            this.SessionTimeLabel = new System.Windows.Forms.Label();
            this.OnlineLabel = new System.Windows.Forms.Label();
            this.ScanTextBox = new Telerik.WinControls.UI.RadTextBoxControl();
            this.AttendeeNameLabel = new System.Windows.Forms.Label();
            this.LocationLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ScanNumericTextBox = new Telerik.WinControls.UI.RadSpinEditor();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.ChangeSessionMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ConfigFilesMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.HeaderImageMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.UploadScansMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.ScanTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScanNumericTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // EventNameLabel
            // 
            this.EventNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventNameLabel.Location = new System.Drawing.Point(10, 142);
            this.EventNameLabel.Name = "EventNameLabel";
            this.EventNameLabel.Size = new System.Drawing.Size(813, 17);
            this.EventNameLabel.TabIndex = 0;
            this.EventNameLabel.Text = "Event Name";
            this.EventNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SessionNameLabel
            // 
            this.SessionNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionNameLabel.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SessionNameLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.SessionNameLabel.Location = new System.Drawing.Point(10, 211);
            this.SessionNameLabel.Name = "SessionNameLabel";
            this.SessionNameLabel.Size = new System.Drawing.Size(813, 63);
            this.SessionNameLabel.TabIndex = 2;
            this.SessionNameLabel.Text = "Session Name";
            this.SessionNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SessionTimeLabel
            // 
            this.SessionTimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionTimeLabel.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SessionTimeLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.SessionTimeLabel.Location = new System.Drawing.Point(10, 168);
            this.SessionTimeLabel.Name = "SessionTimeLabel";
            this.SessionTimeLabel.Size = new System.Drawing.Size(813, 24);
            this.SessionTimeLabel.TabIndex = 3;
            this.SessionTimeLabel.Text = "Wednesday 8:00 AM - 2:00 PM";
            this.SessionTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OnlineLabel
            // 
            this.OnlineLabel.BackColor = System.Drawing.Color.LightYellow;
            this.OnlineLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.OnlineLabel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnlineLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.OnlineLabel.Location = new System.Drawing.Point(0, 0);
            this.OnlineLabel.Name = "OnlineLabel";
            this.OnlineLabel.Size = new System.Drawing.Size(829, 21);
            this.OnlineLabel.TabIndex = 4;
            this.OnlineLabel.Text = "Mode: Offline";
            this.OnlineLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ScanTextBox
            // 
            this.ScanTextBox.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.ScanTextBox.Location = new System.Drawing.Point(185, 449);
            this.ScanTextBox.Name = "ScanTextBox";
            this.ScanTextBox.Size = new System.Drawing.Size(167, 45);
            this.ScanTextBox.TabIndex = 6;
            this.ScanTextBox.Visible = false;
            this.ScanTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScanTextBox_KeyDown);
            this.ScanTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ScanTextBox_KeyPress);
            // 
            // AttendeeNameLabel
            // 
            this.AttendeeNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.AttendeeNameLabel.Font = new System.Drawing.Font("Calibri", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendeeNameLabel.Location = new System.Drawing.Point(10, 287);
            this.AttendeeNameLabel.Name = "AttendeeNameLabel";
            this.AttendeeNameLabel.Size = new System.Drawing.Size(807, 45);
            this.AttendeeNameLabel.TabIndex = 7;
            this.AttendeeNameLabel.Text = "Attendee Name";
            this.AttendeeNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LocationLabel
            // 
            this.LocationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LocationLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationLabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.LocationLabel.Location = new System.Drawing.Point(10, 192);
            this.LocationLabel.Name = "LocationLabel";
            this.LocationLabel.Size = new System.Drawing.Size(813, 19);
            this.LocationLabel.TabIndex = 8;
            this.LocationLabel.Text = "Location";
            this.LocationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(829, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // ScanNumericTextBox
            // 
            this.ScanNumericTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ScanNumericTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ScanNumericTextBox.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.ScanNumericTextBox.Location = new System.Drawing.Point(227, 335);
            this.ScanNumericTextBox.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.ScanNumericTextBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ScanNumericTextBox.Name = "ScanNumericTextBox";
            // 
            // 
            // 
            this.ScanNumericTextBox.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.ScanNumericTextBox.ShowBorder = true;
            this.ScanNumericTextBox.ShowUpDownButtons = false;
            this.ScanNumericTextBox.Size = new System.Drawing.Size(367, 37);
            this.ScanNumericTextBox.TabIndex = 10;
            this.ScanNumericTextBox.TabStop = false;
            this.ScanNumericTextBox.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.ScanNumericTextBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ScanNumericTextBox.ValueChanged += new System.EventHandler(this.ScanNumericTextBox_ValueChanged);
            this.ScanNumericTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ScanNumericTextBox_KeyPress);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "png";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "*.png|*.gif|*.jpg|*.bmp";
            // 
            // radMenu1
            // 
            this.radMenu1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radMenu1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ChangeSessionMenuItem,
            this.radMenuItem2});
            this.radMenu1.Location = new System.Drawing.Point(0, 425);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(829, 23);
            this.radMenu1.TabIndex = 12;
            this.radMenu1.Text = "radMenu1";
            // 
            // ChangeSessionMenuItem
            // 
            this.ChangeSessionMenuItem.AccessibleDescription = "Change Session...";
            this.ChangeSessionMenuItem.AccessibleName = "Change Session...";
            this.ChangeSessionMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ChangeSessionMenuItem.Name = "ChangeSessionMenuItem";
            this.ChangeSessionMenuItem.Text = "Change Session";
            this.ChangeSessionMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.ChangeSessionMenuItem.Click += new System.EventHandler(this.ChangeSessionMenuItem_Click);
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Options";
            this.radMenuItem2.AccessibleName = "Options";
            this.radMenuItem2.Alignment = System.Drawing.ContentAlignment.TopRight;
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ConfigFilesMenuItem,
            this.HeaderImageMenuItem,
            this.UploadScansMenuItem});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Options...";
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ConfigFilesMenuItem
            // 
            this.ConfigFilesMenuItem.AccessibleDescription = "radMenuItem3";
            this.ConfigFilesMenuItem.AccessibleName = "ConfigFilesMenuItem";
            this.ConfigFilesMenuItem.Name = "ConfigFilesMenuItem";
            this.ConfigFilesMenuItem.Text = "Configure Data Files";
            this.ConfigFilesMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.ConfigFilesMenuItem.Click += new System.EventHandler(this.ConfigFilesMenuItem_Click);
            // 
            // HeaderImageMenuItem
            // 
            this.HeaderImageMenuItem.AccessibleDescription = "Set Header Image";
            this.HeaderImageMenuItem.AccessibleName = "Set Header Image";
            this.HeaderImageMenuItem.Name = "HeaderImageMenuItem";
            this.HeaderImageMenuItem.Text = "Set Header Image";
            this.HeaderImageMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.HeaderImageMenuItem.Click += new System.EventHandler(this.HeaderImageMenuItem_Click);
            // 
            // UploadScansMenuItem
            // 
            this.UploadScansMenuItem.AccessibleDescription = "Upload Scans";
            this.UploadScansMenuItem.AccessibleName = "Upload Scans";
            this.UploadScansMenuItem.Name = "UploadScansMenuItem";
            this.UploadScansMenuItem.Text = "Upload Scans";
            this.UploadScansMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.UploadScansMenuItem.Click += new System.EventHandler(this.UploadScansMenuItem_Click);
            // 
            // ScanAttendees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(829, 448);
            this.Controls.Add(this.AttendeeNameLabel);
            this.Controls.Add(this.radMenu1);
            this.Controls.Add(this.ScanNumericTextBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LocationLabel);
            this.Controls.Add(this.ScanTextBox);
            this.Controls.Add(this.OnlineLabel);
            this.Controls.Add(this.SessionTimeLabel);
            this.Controls.Add(this.SessionNameLabel);
            this.Controls.Add(this.EventNameLabel);
            this.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "ScanAttendees";
            this.Text = "Scan Attendees";
            ((System.ComponentModel.ISupportInitialize)(this.ScanTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScanNumericTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label EventNameLabel;
        private System.Windows.Forms.Label SessionNameLabel;
        private System.Windows.Forms.Label SessionTimeLabel;
        private System.Windows.Forms.Label OnlineLabel;
        private Telerik.WinControls.UI.RadTextBoxControl ScanTextBox;
        private System.Windows.Forms.Label AttendeeNameLabel;
        private System.Windows.Forms.Label LocationLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadSpinEditor ScanNumericTextBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem ChangeSessionMenuItem;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem ConfigFilesMenuItem;
        private Telerik.WinControls.UI.RadMenuItem HeaderImageMenuItem;
        private Telerik.WinControls.UI.RadMenuItem UploadScansMenuItem;
    }
}

