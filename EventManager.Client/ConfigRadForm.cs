﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Deployment.Application;
using EventManager.Model;
using System.Linq;
using EventManager.Client.Properties;
using System.Xml.Linq;
using Telerik.WinControls.UI;

namespace EventManager.Client
{
    public partial class ConfigRadForm : Telerik.WinControls.UI.RadForm
    {
        private EventManager.Model.Association _assn;
        private EventManager.Model.Event _evt;
        private EventManager.Model.EventSession _session;
        private string _dataDirectory;
        private string _mode;

        EventManager.Model.Entities ctx;

        public ConfigRadForm(EventManager.Model.Event evt, EventManager.Model.Association assn)
        {
            ctx = new EventManager.Model.Entities();

            InitializeComponent();
            _evt = evt;
            _assn = assn;
        }

        private void ConfigRadForm_Load(object sender, EventArgs e)
        {
            // Load Association combo 
            LoadAssnCombo();

            //Load events combo
            //LoadEventsCombo();

            DataDirectoryTextBox.Text = (Properties.Settings.Default.DataDirectory != "" ? Properties.Settings.Default.DataDirectory : GetDefaultDataPath());
            SetFilePaths();
           
        }

        private void LoadAssnCombo()
        {
            // Load Association combo 
            IQueryable<Association> assnQuery =

                  from assn in ctx.Associations
                  where assn.IsApproved == true
                  orderby assn.AssociationName
                  select assn;

            List<EventManager.Model.Association> associations = assnQuery.ToList();
            AssnDropDownList.ValueMember = "AssociationId";
            AssnDropDownList.DisplayMember = "AssociationName";
            //AssnDropDownList.DataSource = null;
            AssnDropDownList.DataSource = associations;

            // If association was sent in, select it
            //if (_assn != null)
            //{
            //    // Find the drop down item that matches the assn ID sent in
            //    Telerik.WinControls.UI.RadListDataItem assnItem = AssnDropDownList.Items.First(a => Convert.ToInt32(a.Value) == _assn.AssociationId);
            //    if (assnItem != null)
            //        AssnDropDownList.SelectedItem = assnItem;
            //}
            //else
            //{
            //    AssnDropDownList.SelectedIndex = 0;
            //}

        }

        private void LoadEventsCombo()
        {
            //Load events combo
            int assnId = Convert.ToInt32(AssnDropDownList.SelectedValue);
            IQueryable<Event> query =

                  from evt in ctx.Events
                  where evt.AssociationId == assnId
                  orderby evt.StartDateTime descending
                  select evt;

            List<EventManager.Model.Event> events = query.ToList();
            
            EventsDropDownList.DataSource = null;
            EventsDropDownList.ValueMember = "Id";
            EventsDropDownList.DisplayMember = "EventName";
            EventsDropDownList.DataSource = events;

            
        }

        private EventManager.Model.Event GetTodaysEvent()
        {
            EventManager.Business.EventMethods m = new EventManager.Business.EventMethods();
            List<EventManager.Model.Event> events = m.GetEventList(1);

            EventManager.Model.Event todaysEvent = events.Find(e => e.StartDateTime.Date >= DateTime.Now && DateTime.Now <= e.EndDateTime);
            if (todaysEvent != null)
            {
                return todaysEvent;
            }
            else
                return null;
        }

        private string GetDefaultDataPath()
        {
            string dataPath;

            // Load the location of the data files (from install path)
            if (ApplicationDeployment.IsNetworkDeployed)
                dataPath = ApplicationDeployment.CurrentDeployment.DataDirectory;
            else
                dataPath = System.Windows.Forms.Application.StartupPath;

            return dataPath;
        }

        private void SetFilePaths()
        {
            int eventId;
            string computerName;
            string fileDirectory;
            string sessionsFileName;
            string sessionsFilePath;
            string attendeesFileName;
            string attendeesFilePath;
            string scansFileName;

            // Get eventId and data directory from drop down and textbox
            eventId = Convert.ToInt32(EventsDropDownList.SelectedValue);
            fileDirectory = DataDirectoryTextBox.Text;

            // Get the computer name (for scans file path)
            computerName = SystemInformation.ComputerName;

            sessionsFileName = "Sessions" + eventId + ".xml";
            attendeesFileName = "Attendees" + eventId + ".xml";
            scansFileName = "Scans" + eventId + "_" + computerName + ".xml";

            SessionsTextBox.Text = sessionsFileName;
            sessionsFilePath = fileDirectory + "\\" + sessionsFileName;
            if (System.IO.File.Exists(sessionsFilePath))
            {
                SessionsFileDateLabel.Text = System.IO.File.GetLastWriteTime(sessionsFilePath).ToShortDateString() + " " + System.IO.File.GetLastWriteTime(sessionsFilePath).ToShortTimeString();
                SessionsFileRecordsLabel.Text = GetXmlNodeCount(sessionsFilePath, "/Sessions/Session").ToString();
            }
            else
            {
                SessionsFileDateLabel.Text = "Never";
                SessionsFileRecordsLabel.Text = "0";
            }
            
            AttendeesTextBox.Text = attendeesFileName;
            attendeesFilePath = fileDirectory + "\\" + attendeesFileName;
            if (System.IO.File.Exists(attendeesFilePath))
            {
                AttendeesFileDateLabel.Text = System.IO.File.GetLastWriteTime(attendeesFilePath).ToShortDateString() + " " + System.IO.File.GetLastWriteTime(attendeesFilePath).ToShortTimeString();
                AttendeesFileRecordsLabel.Text = GetXmlNodeCount(attendeesFilePath, "/Attendees/Attendee").ToString();
            }
            else
            {
                AttendeesFileDateLabel.Text = "Never";
                AttendeesFileRecordsLabel.Text = "0";
            }
            
            ScansTextBox.Text = scansFileName;

        }

        private int GetXmlNodeCount(string filePath, string nodeName)
        {
            string xml = System.IO.File.ReadAllText(filePath);
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.LoadXml(xml);
            System.Xml.XmlNodeList nodes = xmlDoc.SelectNodes(nodeName);

            return nodes.Count;
        }

        private void UpdateFileInfo()
        {
            string computerName;
            string fileDirectory;
            string sessionsFilePath;
            string attendeesFilePath;
            string scansFilePath;

            // Load the location of the data files (from install path)
            if (Properties.Settings.Default.DataDirectory != "")
                fileDirectory = Properties.Settings.Default.DataDirectory;
            else
                fileDirectory = GetDefaultDataPath();

            // Get the computer name (for scans file path)
            computerName = SystemInformation.ComputerName;

            sessionsFilePath = System.IO.Path.Combine(_dataDirectory, "\\Sessions" + _evt.Id + ".txt");
            attendeesFilePath = System.IO.Path.Combine(_dataDirectory, "\\Attendees" + _evt.Id + ".txt");
            scansFilePath = System.IO.Path.Combine(_dataDirectory, "\\Scans" + _evt.Id + "_" + computerName + ".txt");

            // See if file exists, otherwise create it
            if (!System.IO.File.Exists(sessionsFilePath))
            {
                System.IO.File.Create(sessionsFilePath);
            }
            if (!System.IO.File.Exists(attendeesFilePath))
            {
                System.IO.File.Create(attendeesFilePath);
            }
            if (!System.IO.File.Exists(scansFilePath))
            {
                System.IO.File.Create(scansFilePath);
            }

            // if created successfully, add file info
            try
            {
                SessionsTextBox.Text = sessionsFilePath;
                SessionsFileDateLabel.Text = System.IO.File.GetLastWriteTime(sessionsFilePath).ToShortDateString() + " " + System.IO.File.GetLastWriteTime(sessionsFilePath).ToShortTimeString();
                SessionsFileRecordsLabel.Text = System.IO.File.ReadAllLines(sessionsFilePath).Length.ToString();

                AttendeesTextBox.Text = attendeesFilePath;
                AttendeesFileDateLabel.Text = System.IO.File.GetLastWriteTime(attendeesFilePath).ToShortDateString() + " " + System.IO.File.GetLastWriteTime(attendeesFilePath).ToShortTimeString();
                AttendeesFileRecordsLabel.Text = System.IO.File.ReadAllLines(attendeesFilePath).Length.ToString();
            }
            finally
            {

            }
        }

        public EventManager.Model.Association Assn
        {
            get
            {
                return _assn;
            }
            set
            {
                _assn = value;
            }
        }

        public EventManager.Model.Event Evt
        {
            get
            {
                return _evt;
            }
            set {
                _evt = value;
            }
        }

        public EventManager.Model.EventSession Session
        {
            get
            {
                return _session;
            }
            set
            {
                _session = value;
            }
        }

        public string DataDirectory
        {
            get {
                return _dataDirectory;
            }
        }

        private void EventsDropDownList_DataBindingComplete(object sender, Telerik.WinControls.UI.ListBindingCompleteEventArgs e)
        {
            // If event was sent in, select it
            if (_evt != null)
            {
                // Find the drop down item that matches the event ID sent in
                if (EventsDropDownList.Items.Where(i => Convert.ToInt32(i.Value) == _evt.Id).Count() > 0)
                {
                    Telerik.WinControls.UI.RadListDataItem eventItem = EventsDropDownList.Items.First(evt => Convert.ToInt32(evt.Value) == _evt.Id);
                    if (eventItem != null)
                        EventsDropDownList.SelectedItem = eventItem;
                }
            }
            else // select an event happening today (or in a few days)
            {
                foreach (RadListDataItem item in EventsDropDownList.Items)
                {
                    Event evt = (Event)item.DataBoundItem;
                    if (evt.StartDateTime.AddDays(-3) <= DateTime.Now && evt.EndDateTime >= DateTime.Now)
                    {
                        EventsDropDownList.SelectedItem = item;
                        return;
                    }
                }
            }

            if (EventsDropDownList.SelectedItem == null && EventsDropDownList.Items.Count > 0)
                EventsDropDownList.SelectedIndex = 0;

            SetFilePaths();

        }

        private void EventsDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.ActiveControl != null && this.ActiveControl.Name == "EventsDropDownList")
                SetFilePaths();
        }

        private void SessionsDropDownList_DataBindingComplete(object sender, Telerik.WinControls.UI.ListBindingCompleteEventArgs e)
        {
            // Select first item
            //if (SessionsDropDownList.Items.Count > 0)
            //    SessionsDropDownList.SelectedIndex = 0;

            //if (_session == null)
            //{
            //    if (SessionsDropDownList.Items.Count > 0)
            //        SessionsDropDownList.SelectedIndex = 0;
            //}
            //else
            //{
            //    // Find the drop down item that matches the session ID sent in
            //    Telerik.WinControls.UI.RadListDataItem sessionItem = SessionsDropDownList.Items.First(evt => Convert.ToInt32(evt.Value) == _session.Id);
            //    if (sessionItem != null)
            //        SessionsDropDownList.SelectedItem = sessionItem;
            //    else if (SessionsDropDownList.Items.Count > 0)
            //        SessionsDropDownList.SelectedIndex = 0;
            //}

            //if (SessionsDropDownList.SelectedItem != null)
            //{
            //    _session = (EventManager.Model.EventSession)SessionsDropDownList.SelectedItem.DataBoundItem;
            //}
        }

        private void OkRadButton_Click(object sender, EventArgs e)
        {
            if (EventsDropDownList.SelectedItem != null)
                Evt = (EventManager.Model.Event)EventsDropDownList.SelectedItem.DataBoundItem;
            else
                Evt = null;

            Properties.Settings.Default.DataDirectory = DataDirectoryTextBox.Text;
            Properties.Settings.Default.Save();

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void CancelRadButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private List<EventManager.Model.Event> GetEvents()
        {
            // Get all events from database
            IQueryable<Event> query =

                  from evt in ctx.Events
                  where evt.AssociationId == 1
                  orderby evt.StartDateTime descending
                  select evt;

            return query.ToList();
        }

        private List<EventManager.Model.EventSession> GetSessions(int eventId)
        {
            //Load sessions combo
            IQueryable<EventSession> query =

                  from s in ctx.EventSessions
                  where s.EventId == eventId
                  orderby s.StartDateTime, s.EndDateTime
                  select s;

            return query.ToList();
        }

        private void CreateFilesRadButton_Click(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(EventsDropDownList.SelectedValue);
            int assnId = Convert.ToInt32(AssnDropDownList.SelectedValue);

            // Make sure we have a directory and that it exist
            if (System.IO.Directory.Exists(DataDirectoryTextBox.Text))
            {
                // Set hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                try
                {
                    CreateEventFile();
                    CreateSessionFile(eventId);
                    CreateAttendeesFile(eventId);
                    CreateScansFile(eventId);

                    if (AssnDropDownList.SelectedItem != null)
                        Assn = (EventManager.Model.Association)AssnDropDownList.SelectedItem.DataBoundItem;
                    else
                        Assn = null;

                    if (EventsDropDownList.SelectedItem != null)
                        Evt = (EventManager.Model.Event)EventsDropDownList.SelectedItem.DataBoundItem;
                    else
                        Evt = null;

                    Properties.Settings.Default.DataDirectory = DataDirectoryTextBox.Text;
                    Properties.Settings.Default.EventId = eventId;
                    Properties.Settings.Default.Event = _evt;
                    Properties.Settings.Default.AssnId = assnId;
                    Properties.Settings.Default.Assn = _assn;
                    Properties.Settings.Default.Save();
                    
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                finally
                { 
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                MessageBox.Show("Please select a directory.  The current directory '" + DataDirectoryTextBox.Text + "' does not exist.", "Invalid Directory");
            }
        }

        private void CreateEventFile()
        {
            // Get all sessions for the selected event
            IQueryable<Event> query =

              from evt in ctx.Events
              where evt.AssociationId == 1
              orderby evt.StartDateTime, evt.EndDateTime
              select evt;

            List<Event> events = query.ToList();

            var xEle = new XElement("Events",
            from s in events
            select new XElement("Event",
                         new XAttribute("Id", s.Id),
                           new XElement("EventName", s.EventName)
                       ));

            xEle.Save(DataDirectoryTextBox.Text + "\\" + EventsTextBox.Text);
        }

        private void CreateSessionFile(int eventId)
        {
            // Get all sessions for the selected event
            IQueryable<EventSession> query =

              from evt in ctx.EventSessions
              where evt.EventId == eventId
              orderby evt.StartDateTime, evt.EndDateTime
              select evt;

            List<EventSession> sessions = query.ToList();

            var xEle = new XElement("Sessions",
            from s in sessions
            select new XElement("Session",
                         new XAttribute("Id", s.Id),
                           new XElement("SessionName", s.SessionName),
                           new XElement("Location", s.Location),
                           new XElement("StartDateTime", s.StartDateTime),
                           new XElement("EndDateTime", s.EndDateTime),
                           new XElement("RequireOneScan", s.RequireOneScan)
                       ));

            xEle.Save(DataDirectoryTextBox.Text + "\\" + SessionsTextBox.Text);
        }

        private void CreateAttendeesFile(int eventId)
        {
            // Get all sessions for the selected event
            IQueryable<RegistrationAttendee> query =

              from evt in ctx.RegistrationAttendees
              where evt.Registration.EventId == eventId
              orderby evt.Attendee.LastName, evt.Attendee.FirstName
              select evt;

            List<RegistrationAttendee> attendees = query.ToList();

            var xEle = new XElement("Attendees",
            from s in attendees
            select new XElement("Attendee",
                         new XAttribute("Id", s.AttendeeId),
                           new XElement("FullName", s.Attendee.LastName + ", " + s.Attendee.FirstName),
                           new XElement("LastName", s.Attendee.LastName),
                           new XElement("FirstName", s.Attendee.FirstName)
                       ));

            xEle.Save(DataDirectoryTextBox.Text + "\\" + AttendeesTextBox.Text);
        }

        private void CreateScansFile(int eventId)
        {
            // See if the file exists, if not create it.
            string computerName = SystemInformation.ComputerName;
            string fileName = "Scans" + eventId + "_" + computerName + ".xml";
            string filePath = DataDirectoryTextBox.Text + "\\" + fileName;

            if (!System.IO.File.Exists(filePath))
            {
                var xEle = new XElement("Scans");
                xEle.Save(filePath);
            }
        }

        private void SessionsFolderButton_Click(object sender, EventArgs e)
        {
            
        }

        private void DataFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.SelectedPath = GetDefaultDataPath();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                DataDirectoryTextBox.Text = folderDialog.SelectedPath;
            }
            folderDialog.Dispose();

            SetFilePaths();
        }

        private void AssnDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.ActiveControl != null && this.ActiveControl.Name == "AssnDropDownList")
                LoadEventsCombo();
        }

        private void AssnDropDownList_DataBindingComplete(object sender, ListBindingCompleteEventArgs e)
        {
            // If association was sent in, select it
            if (_assn != null)
            {
                // Find the drop down item that matches the assn ID sent in
                if (AssnDropDownList.Items.Where(i => Convert.ToInt32(i.Value) == _assn.AssociationId).Count() > 0)
                {
                    Telerik.WinControls.UI.RadListDataItem assnItem = AssnDropDownList.Items.First(evt => Convert.ToInt32(evt.Value) == _assn.AssociationId);
                    if (assnItem != null)
                        AssnDropDownList.SelectedItem = assnItem;
                }
                else
                {
                    AssnDropDownList.SelectedIndex = 0;
                }
            }
            else // select the first
            {
                AssnDropDownList.SelectedIndex = 0;
                //foreach (RadListDataItem item in EventsDropDownList.Items)
                //{
                //    Event evt = (Event)item.DataBoundItem;
                //    if (evt.StartDateTime <= DateTime.Now && evt.EndDateTime >= DateTime.Now)
                //    {
                //        EventsDropDownList.SelectedItem = item;
                //        return;
                //    }
                //}

                //if (EventsDropDownList.SelectedItem == null)
                //    EventsDropDownList.SelectedIndex = 0;
            }

            LoadEventsCombo();
        }

    }
}
