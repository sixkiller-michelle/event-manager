﻿namespace EventManager.Client
{
    partial class ConfigRadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SessionsTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.AttendeesTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.CreateFilesRadButton = new Telerik.WinControls.UI.RadButton();
            this.SessionsFileDateLabel = new System.Windows.Forms.Label();
            this.AttendeesFileDateLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SessionsFileRecordsLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AttendeesFileRecordsLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.EventsDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.label11 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.DataFolderButton = new System.Windows.Forms.Button();
            this.DataDirectoryTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ScansTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.EventsFileRecordsLabel = new System.Windows.Forms.Label();
            this.EventsFileDateLabel = new System.Windows.Forms.Label();
            this.EventsTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.AssnDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.SessionsTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttendeesTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreateFilesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventsDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataDirectoryTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScansTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventsTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssnDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 246);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Session list:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(405, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Last Updated:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Attendee list:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(12, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "Data Files";
            // 
            // SessionsTextBox
            // 
            this.SessionsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionsTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.SessionsTextBox.Location = new System.Drawing.Point(112, 242);
            this.SessionsTextBox.Name = "SessionsTextBox";
            this.SessionsTextBox.ReadOnly = true;
            this.SessionsTextBox.Size = new System.Drawing.Size(204, 20);
            this.SessionsTextBox.TabIndex = 8;
            this.SessionsTextBox.TabStop = false;
            this.SessionsTextBox.Text = "Sessions[event id].txt";
            // 
            // AttendeesTextBox
            // 
            this.AttendeesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.AttendeesTextBox.Location = new System.Drawing.Point(112, 268);
            this.AttendeesTextBox.Name = "AttendeesTextBox";
            this.AttendeesTextBox.ReadOnly = true;
            this.AttendeesTextBox.Size = new System.Drawing.Size(204, 20);
            this.AttendeesTextBox.TabIndex = 9;
            this.AttendeesTextBox.TabStop = false;
            this.AttendeesTextBox.Text = "Attendees[event id].txt";
            // 
            // CreateFilesRadButton
            // 
            this.CreateFilesRadButton.Location = new System.Drawing.Point(112, 330);
            this.CreateFilesRadButton.Name = "CreateFilesRadButton";
            this.CreateFilesRadButton.Size = new System.Drawing.Size(87, 24);
            this.CreateFilesRadButton.TabIndex = 10;
            this.CreateFilesRadButton.Text = "Create";
            this.CreateFilesRadButton.Click += new System.EventHandler(this.CreateFilesRadButton_Click);
            // 
            // SessionsFileDateLabel
            // 
            this.SessionsFileDateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionsFileDateLabel.AutoSize = true;
            this.SessionsFileDateLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SessionsFileDateLabel.Location = new System.Drawing.Point(483, 245);
            this.SessionsFileDateLabel.Name = "SessionsFileDateLabel";
            this.SessionsFileDateLabel.Size = new System.Drawing.Size(113, 13);
            this.SessionsFileDateLabel.TabIndex = 11;
            this.SessionsFileDateLabel.Text = "12/24/1900 12:00 AM";
            this.SessionsFileDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AttendeesFileDateLabel
            // 
            this.AttendeesFileDateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AttendeesFileDateLabel.AutoSize = true;
            this.AttendeesFileDateLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendeesFileDateLabel.Location = new System.Drawing.Point(483, 271);
            this.AttendeesFileDateLabel.Name = "AttendeesFileDateLabel";
            this.AttendeesFileDateLabel.Size = new System.Drawing.Size(113, 13);
            this.AttendeesFileDateLabel.TabIndex = 13;
            this.AttendeesFileDateLabel.Text = "12/24/1900 10:00 AM";
            this.AttendeesFileDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(405, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Last Updated:";
            // 
            // SessionsFileRecordsLabel
            // 
            this.SessionsFileRecordsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SessionsFileRecordsLabel.AutoSize = true;
            this.SessionsFileRecordsLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SessionsFileRecordsLabel.Location = new System.Drawing.Point(374, 245);
            this.SessionsFileRecordsLabel.Name = "SessionsFileRecordsLabel";
            this.SessionsFileRecordsLabel.Size = new System.Drawing.Size(13, 13);
            this.SessionsFileRecordsLabel.TabIndex = 14;
            this.SessionsFileRecordsLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(322, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Records:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(322, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Records:";
            // 
            // AttendeesFileRecordsLabel
            // 
            this.AttendeesFileRecordsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AttendeesFileRecordsLabel.AutoSize = true;
            this.AttendeesFileRecordsLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttendeesFileRecordsLabel.Location = new System.Drawing.Point(374, 271);
            this.AttendeesFileRecordsLabel.Name = "AttendeesFileRecordsLabel";
            this.AttendeesFileRecordsLabel.Size = new System.Drawing.Size(13, 13);
            this.AttendeesFileRecordsLabel.TabIndex = 16;
            this.AttendeesFileRecordsLabel.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(12, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 21);
            this.label10.TabIndex = 18;
            this.label10.Text = "Association / Event";
            // 
            // EventsDropDownList
            // 
            this.EventsDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventsDropDownList.DropDownAnimationEnabled = true;
            this.EventsDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.EventsDropDownList.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.EventsDropDownList.Location = new System.Drawing.Point(22, 88);
            this.EventsDropDownList.MaxDropDownItems = 0;
            this.EventsDropDownList.Name = "EventsDropDownList";
            this.EventsDropDownList.ShowImageInEditorArea = true;
            this.EventsDropDownList.Size = new System.Drawing.Size(782, 27);
            this.EventsDropDownList.TabIndex = 19;
            this.EventsDropDownList.DataBindingComplete += new Telerik.WinControls.UI.ListBindingCompleteEventHandler(this.EventsDropDownList_DataBindingComplete);
            this.EventsDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.EventsDropDownList_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(22, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(233, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(407, 15);
            this.label13.TabIndex = 28;
            this.label13.Text = "Note: In order to generate these files, you must be connected to the internet";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 20);
            this.label12.TabIndex = 29;
            this.label12.Text = "Save files here:";
            // 
            // DataFolderButton
            // 
            this.DataFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DataFolderButton.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.DataFolderButton.Location = new System.Drawing.Point(810, 180);
            this.DataFolderButton.Name = "DataFolderButton";
            this.DataFolderButton.Size = new System.Drawing.Size(30, 23);
            this.DataFolderButton.TabIndex = 31;
            this.DataFolderButton.Text = "...";
            this.DataFolderButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.DataFolderButton.UseVisualStyleBackColor = true;
            this.DataFolderButton.Click += new System.EventHandler(this.DataFolderButton_Click);
            // 
            // DataDirectoryTextBox
            // 
            this.DataDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DataDirectoryTextBox.Location = new System.Drawing.Point(112, 181);
            this.DataDirectoryTextBox.Name = "DataDirectoryTextBox";
            this.DataDirectoryTextBox.ReadOnly = true;
            this.DataDirectoryTextBox.Size = new System.Drawing.Size(692, 20);
            this.DataDirectoryTextBox.TabIndex = 30;
            this.DataDirectoryTextBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 298);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Scans:";
            // 
            // ScansTextBox
            // 
            this.ScansTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ScansTextBox.Location = new System.Drawing.Point(112, 294);
            this.ScansTextBox.Name = "ScansTextBox";
            this.ScansTextBox.ReadOnly = true;
            this.ScansTextBox.Size = new System.Drawing.Size(204, 20);
            this.ScansTextBox.TabIndex = 33;
            this.ScansTextBox.TabStop = false;
            this.ScansTextBox.Text = "Scans[event id]_[computer name].txt";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(322, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Records:";
            // 
            // EventsFileRecordsLabel
            // 
            this.EventsFileRecordsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EventsFileRecordsLabel.AutoSize = true;
            this.EventsFileRecordsLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EventsFileRecordsLabel.Location = new System.Drawing.Point(374, 219);
            this.EventsFileRecordsLabel.Name = "EventsFileRecordsLabel";
            this.EventsFileRecordsLabel.Size = new System.Drawing.Size(13, 13);
            this.EventsFileRecordsLabel.TabIndex = 38;
            this.EventsFileRecordsLabel.Text = "0";
            // 
            // EventsFileDateLabel
            // 
            this.EventsFileDateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EventsFileDateLabel.AutoSize = true;
            this.EventsFileDateLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EventsFileDateLabel.Location = new System.Drawing.Point(483, 219);
            this.EventsFileDateLabel.Name = "EventsFileDateLabel";
            this.EventsFileDateLabel.Size = new System.Drawing.Size(113, 13);
            this.EventsFileDateLabel.TabIndex = 37;
            this.EventsFileDateLabel.Text = "12/24/1900 12:00 AM";
            this.EventsFileDateLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // EventsTextBox
            // 
            this.EventsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventsTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.EventsTextBox.Location = new System.Drawing.Point(112, 216);
            this.EventsTextBox.Name = "EventsTextBox";
            this.EventsTextBox.ReadOnly = true;
            this.EventsTextBox.Size = new System.Drawing.Size(204, 20);
            this.EventsTextBox.TabIndex = 36;
            this.EventsTextBox.TabStop = false;
            this.EventsTextBox.Text = "Events.txt";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(405, 219);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Last Updated:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 220);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Event list:";
            // 
            // AssnDropDownList
            // 
            this.AssnDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.AssnDropDownList.DropDownAnimationEnabled = true;
            this.AssnDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AssnDropDownList.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.AssnDropDownList.Location = new System.Drawing.Point(22, 55);
            this.AssnDropDownList.MaxDropDownItems = 0;
            this.AssnDropDownList.Name = "AssnDropDownList";
            this.AssnDropDownList.ShowImageInEditorArea = true;
            this.AssnDropDownList.Size = new System.Drawing.Size(782, 27);
            this.AssnDropDownList.TabIndex = 40;
            this.AssnDropDownList.DataBindingComplete += new Telerik.WinControls.UI.ListBindingCompleteEventHandler(this.AssnDropDownList_DataBindingComplete);
            this.AssnDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AssnDropDownList_SelectedIndexChanged);
            // 
            // ConfigRadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 378);
            this.Controls.Add(this.AssnDropDownList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.EventsFileRecordsLabel);
            this.Controls.Add(this.EventsFileDateLabel);
            this.Controls.Add(this.EventsTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ScansTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DataFolderButton);
            this.Controls.Add(this.DataDirectoryTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.EventsDropDownList);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AttendeesFileRecordsLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SessionsFileRecordsLabel);
            this.Controls.Add(this.AttendeesFileDateLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.SessionsFileDateLabel);
            this.Controls.Add(this.CreateFilesRadButton);
            this.Controls.Add(this.AttendeesTextBox);
            this.Controls.Add(this.SessionsTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(600, 350);
            this.Name = "ConfigRadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Settings";
            this.ThemeName = "ControlDefault";
            this.Load += new System.EventHandler(this.ConfigRadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SessionsTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttendeesTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreateFilesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventsDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataDirectoryTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScansTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventsTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AssnDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadTextBox SessionsTextBox;
        private Telerik.WinControls.UI.RadTextBox AttendeesTextBox;
        private Telerik.WinControls.UI.RadButton CreateFilesRadButton;
        private System.Windows.Forms.Label SessionsFileDateLabel;
        private System.Windows.Forms.Label AttendeesFileDateLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label SessionsFileRecordsLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label AttendeesFileRecordsLabel;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadDropDownList EventsDropDownList;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button DataFolderButton;
        private Telerik.WinControls.UI.RadTextBox DataDirectoryTextBox;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadTextBox ScansTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label EventsFileRecordsLabel;
        private System.Windows.Forms.Label EventsFileDateLabel;
        private Telerik.WinControls.UI.RadTextBox EventsTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Telerik.WinControls.UI.RadDropDownList AssnDropDownList;
    }
}
