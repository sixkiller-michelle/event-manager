﻿namespace EventManager.Client
{
    partial class UploadScanFileRadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FileNameTextBox = new Telerik.WinControls.UI.RadTextBoxControl();
            this.label1 = new System.Windows.Forms.Label();
            this.SelectFileRadButton = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.FileNameTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectFileRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // FileNameTextBox
            // 
            this.FileNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FileNameTextBox.Location = new System.Drawing.Point(46, 24);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.Size = new System.Drawing.Size(618, 20);
            this.FileNameTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File:";
            // 
            // SelectFileRadButton
            // 
            this.SelectFileRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectFileRadButton.Location = new System.Drawing.Point(668, 22);
            this.SelectFileRadButton.Name = "SelectFileRadButton";
            this.SelectFileRadButton.Size = new System.Drawing.Size(30, 24);
            this.SelectFileRadButton.TabIndex = 2;
            this.SelectFileRadButton.Text = "...";
            this.SelectFileRadButton.Click += new System.EventHandler(this.SelectFileRadButton_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(46, 60);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 24);
            this.radButton1.TabIndex = 3;
            this.radButton1.Text = "Upload";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // UploadScanFileRadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 113);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.SelectFileRadButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FileNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "UploadScanFileRadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Upload Scan File";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.FileNameTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectFileRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBoxControl FileNameTextBox;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton SelectFileRadButton;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}
