﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Xml.Linq;
using EventManager.Model;
using System.Linq;
using EventManager.Business;

namespace EventManager.Client
{
    public partial class UploadScanFileRadForm : Telerik.WinControls.UI.RadForm
    {
        private string scansFilePath;

        public UploadScanFileRadForm(int eventId)
        {
            InitializeComponent();

            string computerName = SystemInformation.ComputerName;
            string scansFileName = "Scans" + eventId + "_" + computerName + ".xml";
            scansFilePath = Properties.Settings.Default.DataDirectory + "\\" + scansFileName;

            FileNameTextBox.Text = scansFilePath;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            // Get the data from the scans file
            XDocument xdoc1 = XDocument.Load(scansFilePath);
            XElement rootElem = xdoc1.Element("Scans");

            // Create object list
            ScannerData objScan = new ScannerData();
            List<ScannerData> lstScans
               = (from _scan in xdoc1.Element("Scans").Elements("Scan")
                  select new ScannerData
                  {
                      EventId = Convert.ToInt32(_scan.Element("EventId").Value),
                      ScannerSerialNumber = _scan.Element("ScannerSerialNumber").Value,
                      ScanDate = _scan.Element("ScanDate").Value,
                      ScanTime = _scan.Element("ScanTime").Value,
                      SessionId = Convert.ToInt32(_scan.Element("SessionId").Value),
                      AttendeeId = Convert.ToInt32(_scan.Element("AttendeeId").Value)
                  }).ToList();

            ScannerDataMethods m = new ScannerDataMethods();
            foreach (var _s in lstScans)
            {
                ScannerData newScan = new ScannerData();
                newScan.AttendeeId = _s.AttendeeId;
                newScan.EventId = _s.EventId;
                newScan.ScanDate = _s.ScanDate;
                newScan.ScanTime = _s.ScanTime;
                newScan.SessionId = _s.SessionId;
                newScan.ScannerSerialNumber = null;
                try
                {
                    m.Add(newScan);
                }
                catch { }
            }
            m.SaveAllObjectChanges();

            // Copy the scan file to an "archive" and clear it out
            string archiveFilePath = scansFilePath.Replace(".xml", "") + "_" + DateTime.Now.ToString().Replace("/", ".").Replace(":", ".").Replace(" ", "_") + ".xml";
            System.IO.File.Copy(scansFilePath, archiveFilePath);

            // Clear out the scans file
            rootElem.RemoveAll();
            xdoc1.Save(scansFilePath);

            MessageBox.Show("Scan file uploaded successfully");
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void SelectFileRadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog selectFileDialog = new OpenFileDialog();
            selectFileDialog.DefaultExt = "xml";
            if (selectFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                scansFilePath = selectFileDialog.FileName;
                FileNameTextBox.Text = scansFilePath;
            }
            selectFileDialog.Dispose();
        }



    }
}
