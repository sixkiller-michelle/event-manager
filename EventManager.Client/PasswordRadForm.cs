﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace EventManager.Client
{
    public partial class PasswordRadForm : Telerik.WinControls.UI.RadForm
    {
        private string password = "ihca-ical";
       

        public PasswordRadForm()
        {
            InitializeComponent();
        }

        public bool IsAuthenticated
        {
            get
            { 
                return PasswordTextBox.Text.Trim() == password;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void PasswordRadForm_Load(object sender, EventArgs e)
        {
            PasswordTextBox.Focus();
        }
    }
}
