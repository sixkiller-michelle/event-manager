﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using EventManager.Model;
using System.Linq;
using System.Xml.Linq;

namespace EventManager.Client
{
    public partial class SelectSessionRadForm : Telerik.WinControls.UI.RadForm
    {
        private EventManager.Model.Event _evt;
        private EventManager.Model.EventSession _session;
        EventManager.Model.Entities ctx;

        public SelectSessionRadForm(EventManager.Model.Event evt, EventManager.Model.EventSession session)
        {
            ctx = new EventManager.Model.Entities();

            InitializeComponent();
            _evt = evt;
            _session = session;
        }

        public EventManager.Model.Event Evt
        {
            get
            {
                return _evt;
            }
            set
            {
                _evt = value;
            }
        }

        public EventManager.Model.EventSession Session
        {
            get
            {
                return _session;
            }
            set
            {
                _session = value;
            }
        }

        private void SelectSessionRadForm_Load(object sender, EventArgs e)
        {
            // If event was sent in, show name as header
            if (_evt != null)
            {
                // Find the drop down item that matches the event ID sent in
                EventNameLabel.Text = _evt.EventName;

                LoadSessions();

                // If session sent in, select it
                if (_session != null)
                {
                    // Find the drop down item that matches the session ID sent in
                    Telerik.WinControls.UI.RadListDataItem sessionItem = SessionsDropDownList.Items.First(evt => Convert.ToInt32(evt.Value) == _session.Id);
                    if (sessionItem != null)
                        SessionsDropDownList.SelectedItem = sessionItem;
                }

            }
            else
            { 
                
            }
        }

        private List<EventSession> GetSessionListFromXmlFile()
        {
            // Load sessions from file
            string filePath = Properties.Settings.Default.DataDirectory + "\\Sessions" + _evt.Id.ToString() + ".xml";
            List<EventSession> sessionList = new List<EventSession>();

            if (System.IO.File.Exists(filePath))
            {
                XDocument xdoc1 = XDocument.Load(filePath);
                EventSession s = new EventSession();
                sessionList = (from _student in xdoc1.Element("Sessions").Elements("Session")
                      select new EventSession
                      {
                          Id = Convert.ToInt32(_student.Attribute("Id").Value),
                          SessionName = _student.Element("SessionName").Value,
                          Location = _student.Element("Location").Value,
                          StartDateTime = Convert.ToDateTime(_student.Element("StartDateTime").Value),
                          EndDateTime = Convert.ToDateTime(_student.Element("EndDateTime").Value),
                          RequireOneScan = Convert.ToBoolean(_student.Element("RequireOneScan").Value)
                      }).ToList();
            }

            return sessionList;
        }

        private void LoadSessions()
        {
            if (_evt != null)
            {
                // Load sessions from file
                List<EventSession> sessionList = GetSessionListFromXmlFile();

                //foreach (EventSession s in sessionList)
                //{ 
                //    SessionsDropDownList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(s.StartDateTime.ToShortTimeString() + " " + s.SessionName, s.Id));
                //}

                SessionsDropDownList.ValueMember = "Id";
                SessionsDropDownList.DisplayMember = "StartTimeAndName";
                SessionsDropDownList.DataSource = sessionList;
            }
            else
            {
                SessionsDropDownList.Items.Clear();
            }
        }

        private void OkRadButton_Click(object sender, EventArgs e)
        {
            _session = (EventSession)SessionsDropDownList.SelectedItem.DataBoundItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelRadButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }
}
