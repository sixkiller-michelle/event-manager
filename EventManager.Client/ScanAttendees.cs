﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EventManager.Model;
using System.Xml.Linq;
using System.Drawing.Imaging;

namespace EventManager.Client
{
    public partial class ScanAttendees : Form
    {
        private EventManager.Model.Association _assn;
        private EventManager.Model.Event _evt;
        private EventManager.Model.EventSession _session;
        private string _mode;
        private bool nonNumberEntered = false;

        public ScanAttendees()
        {
            InitializeComponent();

            Assn = null;
            Evt = null;
            Session = null;
            Mode = "Offline";

            // Check if an event is stored
            if (Properties.Settings.Default.Event != null)
                Evt = Properties.Settings.Default.Event;

            // Check if an event is stored
            if (Properties.Settings.Default.Assn != null)
                Assn = Properties.Settings.Default.Assn;

            if (System.IO.File.Exists(Properties.Settings.Default.HeaderImagePath))
                pictureBox1.ImageLocation = Properties.Settings.Default.HeaderImagePath;
        }

        private EventManager.Model.Association Assn
        {
            get
            {
                return _assn;
            }
            set
            {
                _assn = value;
            }
        }

        private EventManager.Model.Event Evt
        {
            get
            {
                return _evt;
            }
            set
            {
                if (value == null)
                {
                    EventNameLabel.Text = "You need to configure this scanning utility for the event.  Click the 'Settings' button.";
                    EventNameLabel.ForeColor = System.Drawing.Color.Red;
                    ScanNumericTextBox.Enabled = false;

                    ChangeSessionMenuItem.Enabled = false;
                    UploadScansMenuItem.Enabled = false;
                }
                else
                {
                    EventNameLabel.Text = value.EventName;
                    EventNameLabel.ForeColor = System.Drawing.Color.DarkBlue;
                    ChangeSessionMenuItem.Enabled = true;
                    UploadScansMenuItem.Enabled = true;
                    if (Session != null)
                        ScanNumericTextBox.Enabled = true;
                }
                _evt = value;
            }
        }

        private EventManager.Model.EventSession Session
        {
            get
            {
                return _session;
            }
            set
            {
                if (value == null)
                {
                    SessionNameLabel.ForeColor = System.Drawing.Color.Red;
                    SessionNameLabel.Text = "Click the 'Change Session' button to select a session.";
                    SessionTimeLabel.Text = "";
                    LocationLabel.Text = "";
                    ScanNumericTextBox.Enabled = false;
                }
                else
                {
                    SessionNameLabel.ForeColor = System.Drawing.Color.DarkBlue;
                    SessionNameLabel.Text = value.SessionName;
                    SessionTimeLabel.Text = value.StartDateTime.ToString("ddd") + " " + value.StartDateTime.ToShortTimeString() + " - " + value.EndDateTime.ToShortTimeString();
                    LocationLabel.Text = value.Location;
                    if (Evt != null)
                        ScanNumericTextBox.Enabled = true;
                }
                _session = value;
            }
        }

        private string Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                if (value == "Online")
                {
                    OnlineLabel.BackColor = System.Drawing.Color.LightYellow;
                    OnlineLabel.ForeColor = System.Drawing.Color.Green;
                    OnlineLabel.Text = "Mode: Online";
                }
                else
                {
                    OnlineLabel.BackColor = System.Drawing.Color.LightGray;
                    OnlineLabel.ForeColor = System.Drawing.Color.Blue;
                    OnlineLabel.Text = "Mode: Offline";
                }
                _mode = value;
            }
        }

        private bool HasPassword()
        { 
            bool isAuthenticated = false;
            PasswordRadForm passwordDialog = new PasswordRadForm();
            passwordDialog.StartPosition = FormStartPosition.CenterParent;
            if (passwordDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                isAuthenticated = passwordDialog.IsAuthenticated;
            }
            return isAuthenticated;
        }

       
       
        private void ScanTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 13 && nonNumberEntered == true)
            {
                // Stop the character from being entered into the control since it is non-numerical.
                //ScanTextBox.Text = ScanTextBox.Text.Substring(0, ScanTextBox.Text.Length - 1);
                e.Handled = true;
            } 
            else if (e.KeyChar == 13)
            {
                string scanText = ScanTextBox.Text.Trim();
                int attendeeId;
                if (Int32.TryParse(scanText, out attendeeId))
                {
                    // Write the scan to the file
                    string filePath = Properties.Settings.Default.DataDirectory + "\\Scans" + _evt.Id.ToString() + "_" + SystemInformation.ComputerName + ".xml";
                    XDocument xdoc1 = XDocument.Load(filePath);
                    XElement xElem = xdoc1.Element("Scans");
                    xElem.Add(new XElement("Scan",
                          new XElement("EventId", _evt.Id.ToString()),
                          new XElement("ScannerSerialNumber", ""),
                          new XElement("ScanDate", DateTime.Today.Date.ToShortDateString()),
                          new XElement("ScanTime", DateTime.Now.ToShortTimeString()),
                          new XElement("SessionId", _session.Id.ToString()),
                          new XElement("AttendeeId", attendeeId.ToString())));
                    xElem.Save(filePath);

                    // Get attendee from file and display name (if found)
                    string attendeeFilePath = Properties.Settings.Default.DataDirectory + "\\Attendees" + _evt.Id.ToString() + ".xml";
                    XDocument xdoc2 = XDocument.Load(attendeeFilePath);
                    XElement rootElem = xdoc2.Element("Attendees");
                   

                    XElement attElem = (from a in rootElem.Elements("Attendee")
                      where a.Attribute("Id").Value.Equals(scanText)
                      select a).FirstOrDefault();

                    if (attElem != null)
                    {
                        AttendeeNameLabel.Text = attElem.Element("FullName").Value;
                        AttendeeNameLabel.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        AttendeeNameLabel.Text = "Attendee not found: " + scanText;
                        AttendeeNameLabel.ForeColor = System.Drawing.Color.Red;
                    }
                    ScanTextBox.Text = "";
                    ScanTextBox.Focus();
                    ScanTextBox.SelectAll();
                    
                }
            }
           
        }

        private void ScanTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Initialize the flag to false.
            nonNumberEntered = false;

            // Determine whether the keystroke is a number from the top of the keyboard. 
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad. 
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace. 
                    if (e.KeyCode != Keys.Back)
                    {
                        // A non-numerical keystroke was pressed. 
                        // Set the flag to true and evaluate in KeyPress event.
                        nonNumberEntered = true;
                    }
                }
            }
            //If shift key was pressed, it's not a number. 
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }
          
        }

        private void ScanNumericTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                string scanText = ScanNumericTextBox.Text.Trim();
                int attendeeId;
                if (Int32.TryParse(scanText, out attendeeId))
                {
                    // Write the scan to the file
                    string filePath = Properties.Settings.Default.DataDirectory + "\\Scans" + _evt.Id.ToString() + "_" + SystemInformation.ComputerName + ".xml";
                    XDocument xdoc1 = XDocument.Load(filePath);
                    XElement xElem = xdoc1.Element("Scans");
                    xElem.Add(new XElement("Scan",
                          new XElement("EventId", _evt.Id.ToString()),
                          new XElement("ScannerSerialNumber", ""),
                          new XElement("ScanDate", DateTime.Today.Date.ToShortDateString()),
                          new XElement("ScanTime", DateTime.Now.ToShortTimeString()),
                          new XElement("SessionId", _session.Id.ToString()),
                          new XElement("AttendeeId", attendeeId.ToString())));
                    xElem.Save(filePath);

                    // Get attendee from file and display name (if found)
                    string attendeeFilePath = Properties.Settings.Default.DataDirectory + "\\Attendees" + _evt.Id.ToString() + ".xml";
                    XDocument xdoc2 = XDocument.Load(attendeeFilePath);
                    XElement rootElem = xdoc2.Element("Attendees");


                    XElement attElem = (from a in rootElem.Elements("Attendee")
                                        where a.Attribute("Id").Value.Equals(scanText)
                                        select a).FirstOrDefault();

                    if (attElem != null)
                    {
                        AttendeeNameLabel.Text = attElem.Element("FullName").Value;
                        AttendeeNameLabel.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        AttendeeNameLabel.Text = "Attendee not found: " + scanText;
                        AttendeeNameLabel.ForeColor = System.Drawing.Color.Red;
                    }
                    ScanNumericTextBox.Text = "";
                    ScanNumericTextBox.Value = 1;
                    ScanNumericTextBox.Focus();
                    ScanNumericTextBox.Select();

                }
            }
           
        }

      
        private OpenFileDialog SetImageFilterString(OpenFileDialog d)
        { 
            d.Filter = "";
            ImageCodecInfo[] codecs  = ImageCodecInfo.GetImageEncoders();
            string sep = String.Empty;
            foreach (ImageCodecInfo c in codecs)
            {
                string codecName = c.CodecName.Substring(8).Replace("Codec", "Files").Trim();
                d.Filter = String.Format("{0}{1}{2} ({3})|{3}", d.Filter, sep, codecName, c.FilenameExtension);
                sep = "|";
            }
          
            d.Filter = String.Format("{0}{1}{2} ({3})|{3}", d.Filter, sep, "All Files", "*.*");
            return d;
        }

        private void ChangeSessionMenuItem_Click(object sender, EventArgs e)
        {
            if (HasPassword())
            {
                SelectSessionRadForm sessionsForm = new SelectSessionRadForm(_evt, _session);
                sessionsForm.StartPosition = FormStartPosition.CenterParent;
                if (sessionsForm.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    Session = sessionsForm.Session;
                }
                AttendeeNameLabel.Text = "";
                ScanNumericTextBox.Focus();
                sessionsForm.Dispose();
            }
            else
            {
                MessageBox.Show("Password not entered, or incorrect", "Authentication Failed");
            }
        }

        private void ConfigFilesMenuItem_Click(object sender, EventArgs e)
        {
            if (HasPassword())
            {
                ConfigRadForm configForm = new ConfigRadForm(_evt, _assn);
                configForm.StartPosition = FormStartPosition.CenterParent;
                if (configForm.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    Evt = configForm.Evt;
                    Session = configForm.Session;
                }
                //ScanTextBox.Focus();
                ScanNumericTextBox.Focus();
                configForm.Dispose();
            }
            else
            {
                MessageBox.Show("Password not entered, or incorrect", "Authentication Failed");
            }
        }

        private void HeaderImageMenuItem_Click(object sender, EventArgs e)
        {
            if (HasPassword())
            {
                OpenFileDialog headerFileDialog = new OpenFileDialog();
                headerFileDialog = SetImageFilterString(headerFileDialog);
                if (headerFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pictureBox1.ImageLocation = headerFileDialog.FileName;
                    Properties.Settings.Default.HeaderImagePath = headerFileDialog.FileName;
                    Properties.Settings.Default.Save();
                }
                headerFileDialog.Dispose();
            }
            else
            {
                MessageBox.Show("Password not entered, or incorrect", "Authentication Failed");
            }
        }

        private void UploadScansMenuItem_Click(object sender, EventArgs e)
        {
            if (HasPassword())
            {
                UploadScanFileRadForm uploadForm = new UploadScanFileRadForm(_evt.Id);
                uploadForm.StartPosition = FormStartPosition.CenterParent;
                if (uploadForm.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                }
                ScanNumericTextBox.Focus();
                uploadForm.Dispose();
            }
            else
            {
                MessageBox.Show("Password not entered, or incorrect", "Authentication Failed");
            }
        }

        private void ScanNumericTextBox_ValueChanged(object sender, EventArgs e)
        {

        }



        

    }
}
